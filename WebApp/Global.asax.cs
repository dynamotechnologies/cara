﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hosting;

using log4net;
using log4net.Config;

using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "ProjectPhaseReport", // Route name
                "Project/{projectId}/Phase/{phaseId}/Report/{action}/{id}", // URL with parameters
                new
                {
                    controller = "PhaseReport",
                    action = "List",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhaseLetterAuthor", // Route name
                "Project/{projectId}/Phase/{phaseId}/Letter/{letterId}/Author/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Author",
                    action = "Input",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    letterId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhaseFormSet", // Route name
                "Project/{projectId}/Phase/{phaseId}/Letter/Form/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Form",
                    action = "List",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhaseLetter", // Route name
                "Project/{projectId}/Phase/{phaseId}/Letter/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Letter",
                    action = "List",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhaseComment", // Route name
                "Project/{projectId}/Phase/{phaseId}/Comment/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Comment",
                    action = "List",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhaseConcern", // Route name
                "Project/{projectId}/Phase/{phaseId}/Concern/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Concern",
                    action = "List",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhaseReadingRoom", // Route name
                "Project/{projectId}/Phase/{phaseId}/ReadingRoom/{action}/{id}", // URL with parameters
                new
                {
                    controller = "ReadingRoom",
                    action = "List",
                    projectId = UrlParameter.Optional,
                    phaseId = UrlParameter.Optional,
                    id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectPhase", // Route name
                "Project/{projectId}/Phase/{phaseId}/{action}", // URL with parameters
                new { controller = "Phase", action = "Details", projectId = UrlParameter.Optional, phaseId = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Project", // Route name
                "Project/{action}/{projectId}", // URL with parameters
                new { controller = "Project", action = "Details", projectId = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "AdminUser", // Route name
                "Admin/User/{action}/{id}", // URL with parameters
                new { controller = "User", action = "Details", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "AdminCode", // Route name
                "Admin/Code/{action}/{id}", // URL with parameters
                new { controller = "Code", action = "Details", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "AdminTermList", // Route name
                "Admin/TermList/{action}/{id}", // URL with parameters
                new { controller = "TermList", action = "Details", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                new string[] { "Cara.WebApp.Controllers" } // Constraints
            );
        }

        protected void Application_Start()
        {
            //for signalr hubs
            RouteTable.Routes.MapHubs();
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);

            // Force log4net to load the settings file before any call to
            // external APIs. This is because log4net uses assembly attributes
            // for loading the configuration. Since the webapp doesn't use
            // assembly attributes, need to configure the logger manually
            string log4netFile = ConfigurationManager.AppSettings["log4NetFile"];
            XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netFile));
            LogManager.GetLogger(typeof(MvcApplication));

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("ETFV-FT6C-6R7H-ACFA");
        }
        
        protected void Session_End(Object sender, EventArgs e)
        {
			var secToken = Session["SecurityToken"] as SecurityToken;

			if (secToken != null)
			{
                using (var securityTokenManager = ManagerFactory.CreateInstance<SecurityTokenManager>())
                {
                    securityTokenManager.UnitOfWork.Context.AttachTo("SecurityTokens", secToken);
                    securityTokenManager.Invalidate(secToken, "Expired");
                }
			}

            Session.Clear();
        }
    }
}