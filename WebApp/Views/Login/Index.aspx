﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Login 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding: 0px 0px 0px 15px;">
        <div class="pageTitle">Your Session Has Expired</div>
        <div class="text" style="padding: 0px 0px 0px 15px;">
	        <p>Due to security concerns, your session has been closed and you have been logged off due to inactivity.</p>
	        <%--<p>If you wish to continue working on <%: Utilities.GetResourceString("AppNameAbbv") %> activities, please log back in using the following link:<br />--%>
            <%--<a href="<%: ConfigurationManager.AppSettings["palsLoginUrl"]%>"><%: ConfigurationManager.AppSettings["palsLoginUrl"]%></a></p>--%>
            <a id="btnBackToCara" class="button" href="http://localhost:53755/Authentication/Login?token="><%: ViewData["user"] %> Back to CARA</a>
            <a id="btnBackToPals" class="button" href="<%: ConfigurationManager.AppSettings["palsLoginUrl"]%>">Back To PALS</a>
            <%-- <p>Thank you,<br />
            <%: Utilities.GetResourceString("AppNameAbbv") %> Support team</p> --%>
        </div>
    </div>
</asp:Content>