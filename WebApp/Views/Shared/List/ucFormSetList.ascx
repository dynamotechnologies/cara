﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.FormSetListViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.FormSetSearchResult>()
            .Name(Constants.LIST)
            .BindTo(Model.FormSets)
            .Columns(columns =>
            {
                columns.Bound(fs => fs.FormSetName).Template(fs =>
                        { %>
                            <%:Html.ActionLink(fs.FormSetName, "LetterList", "Form", new
                                {
                                    FormSet = fs.FormSetId.HasValue? fs.FormSetId : fs.FormSetName == "Unique" ? -1 : -2,
                                    PhaseId = Model.PhaseId,
                                    ProjectId = Model.ProjectId
                                },
                                null)%><%
                        }).Title("Name");
                columns.Bound(fs => fs.LetterSequence).Template(fs =>
                    {
                        if (fs.FormSetId.HasValue && fs.FormSetId.Value >= 0)
                        {%>
                            <%= Html.ActionLink(fs.LetterSequence.ToString(), Constants.CODING, Constants.LETTER, new { id = fs.MasterFormId, useDefaultFilter = true }, null)%>
                        <%}
                    }).Title("Master Form");
                columns.Bound(fs => fs.LastName).Template(fs =>
                    {
                        if (fs.FormSetId.HasValue && fs.FormSetId.Value >= 0)
                        {%>
                            <%=String.Format("{0}, {1}{2}", fs.LastName, fs.FirstName, 
                            string.IsNullOrEmpty(fs.OrganizationName)?"":"<br>"+fs.OrganizationName)%>
                        <%}
                    }).Title("Author Name and Organization");
                columns.Bound(fs => fs.Size).Title("Master Form Size");
                columns.Bound(fs => fs.LetterCount).Title("Letter Count");

                columns.Bound(fs => fs.FormSetId).Template(fs =>
                    {
                        if (Model.CanDownloadLetters)
                            { %>
                                <a class="button" href="#" onclick="requestFormLetters(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>, 
                                    <%=fs.FormSetId.HasValue? fs.FormSetId : fs.FormSetName == "Unique" ? -1: -2 %>)">Download</a>
                            <% }
                    }).Title("Action").Sortable(false);                    
                    
                columns.Bound(fs => fs.FormSetId).Template(fs =>
                    {

                        if (Model.CanEdit && Utilities.UserHasPrivilege("MLTR", Constants.PHASE) &&
                               fs.FormSetId != null)
                        { %>
                           <a class="button" href="#" onclick="openFormSetEditWindow(<%=fs.FormSetId%>);return false;"><%:Constants.RENAME%></a>
                        <% }
                    }).Title("Action").Sortable(false);
            })
            .EnableCustomBinding(true)
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Pageable(paging => paging.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
            .Sortable()
            .Render();
    } %>