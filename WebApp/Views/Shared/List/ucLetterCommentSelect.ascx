﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>

<div id="letterCommentSelectArea">
<script type="text/javascript">
<!--
    // close the window and refresh the letter input on parent
    function linkToComment() {
        var commentId = $("input[name=rbCommentId]:checked").val();
        var currComment = $("#currentComment").text();
        var letterId = $("#LetterId").val();
        var phaseId = $("#PhaseId").val();

        if (currComment != "Pending Comment") {
            alertWindow("Please select some text from the letter.");
            return false;
        }

        if (phaseId != null && phaseId != "" && letterId != null & letterId != "" && commentId != null & commentId != "") {
            closeWindow("WindowLinkText");

            addCommentText(phaseId, letterId, commentId);
        }
        else {
            alertWindow("Please select a comment.");
        }

        return false;
    }

    function getComments(parms) {
        // ajax action call to populate comment select area

        $.post("/Letter/SearchComment",
            parms,
            function (data) {
                $("#letterCommentSelectArea").html(data);
            },
            "html");
    }

    function showAllComments() {
        var letterId = $("#LetterId").val();
        var phaseId = $("#PhaseId").val();

        // set up parameters
        var parms = {
            phaseId: phaseId,
            letterId: letterId,
            text: ""
        };

        getComments(parms);
    }

    function searchComments() {
        var text = $("#txtCommentId").val();
        var letterId = $("#LetterId").val();
        var phaseId = $("#PhaseId").val();

        // set up parameters
        var parms = {
            phaseId: phaseId,
            letterId: letterId,
            text: text
        };

        getComments(parms);
    }
-->
</script>
<div id="inputArea">
    <div class="fieldLabel">Please select a comment to link the text to:</div>
    <br />
    <span class="text">Comment #:</span>&nbsp;<%: Html.TextBox("txtCommentId") %>&nbsp;
    <a id="btnSearchComment" class="button" href="#" onclick="searchComments()"><%=Constants.SEARCH %></a>
    <a id="btnAllComments" class="button" href="#" onclick="showAllComments()"><%=Constants.CLEAR %></a>
    <br /><br />
    <%  if (Model.Comments != null)
        {
            Html.Telerik().Grid(Model.Comments.OrderBy(x => x.CommentId))
                .Name(Constants.LIST)
                .Columns(columns =>
                {
                    if (Utilities.UserHasPrivilege("CODE", Constants.PHASE))
                    {
                        columns.Bound(comment => comment.CommentId).Template(comment =>
                            {%><input type="radio" name="rbCommentId" value="<%=comment.CommentId %>" /><%})
                                .Title(Constants.SELECT)
                                .Width(50)
                                .Sortable(false);
                    }
                    columns.Bound(comment => comment.LetterCommentNumber)
                        .Template(comment =>
                            {%><span id="CommentNumber_<%=comment.CommentId%>"><%=comment.LetterCommentNumber%></span><%})
                        .Width(50);
                    columns.Bound(comment => comment.CommentText).Sortable(false).Title("Text");
                })
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .Footer(false)
                .Render();
        } %>
    <br />
    <% if (Model.Comments != null && Model.Comments.Count > 0)
        { %>
        <input type="button" value="<%=Constants.LINK %>" name="save" class="button" onclick="linkToComment()" />
    <% } %>
    <input type="button" value="<%=Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('WindowLinkText')" />
    <%: Html.HiddenFor(model => model.LetterId) %>
</div>
</div>