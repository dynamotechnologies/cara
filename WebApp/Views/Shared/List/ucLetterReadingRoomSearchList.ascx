﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterReadingRoomListViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.LetterReadingRoomSearchResult>()
                .BindTo(Model.Letters)
                .Name(Constants.LIST)
                .ToolBar(commands => commands.Custom().HtmlAttributes(new { style = "color: black;", id = "btnCheckAll", onclick = "onClickCheckAll(this, true);return false;" })
                    .Text("Check All"))
                .Columns(columns =>
                {
                    // show checkbox if user has privilege to publish
                    if (Utilities.UserHasPrivilege("PURR", Constants.PHASE))
                    {
                        columns.Bound(letter => letter.PublishToReadingRoom).Template(letter =>
                        {%><input type="checkbox" name="chkPublish_<%=letter.LetterId %>" id="chkPublish_<%=letter.LetterId %>" value="<%=letter.LetterId %>" <%=letter.Checked %> onclick="onClickCheckbox(this);" /><%})
                            .ClientTemplate("<input type=\"checkbox\" name=\"chkPublish_<#=LetterId #>\" id=\"chkPublish_<#=LetterId #>\" value=\"<#=LetterId #>\" <#=Checked #> onclick=\"onClickCheckbox(this);\" />")
                            .Title(Constants.PUBLISH)
                            .Width(50)
                            .HtmlAttributes(new { style = "text-align: center;" })
                            .Sortable(false);
                        
                        columns.Bound(letter => letter.LetterId).Template(letter =>
                        { %>
                            <%: Html.Telerik().DropDownList()
                                        .Name("ddlUnpublishedReason_" + letter.LetterId)
                                        .BindTo(Model.LookupUnpublishedReason)
                                        .Value(letter.UnpublishedReasonId.HasValue ? letter.UnpublishedReasonId.ToString() : string.Empty)
                                        .ClientEvents(events => events.OnChange("onChangeDdl"))
                                        .HtmlAttributes(new { style = "width:220px;" }) %><br />
                            <%: Html.TextBox("txtUnpublishReasonOther_" + letter.LetterId, letter.UnpublishedReasonOther, new { onkeyup = "onKeyUpTextbox(this)", style = "width:220px;" + (!string.IsNullOrEmpty(letter.UnpublishedReasonOther) ? string.Empty : "display:none;"), maxlength = 50 })%>
                        <% })
                           .ClientTemplate(Html.Telerik().DropDownList()
                                        .Name("ddlUnpublishedReason_" + "<#= LetterId #>")
                                        .BindTo(Model.LookupUnpublishedReason)
                                        .ClientEvents(events => events.OnChange("onChangeDdl"))
                                        .HtmlAttributes(new { style = "width:220px;" }).ToHtmlString() + "<br />" +
                                        "<input type='text' id='txtUnpublishReasonOther_<#= LetterId #>' name='txtUnpublishReasonOther_<#= LetterId #>' value='<#= UnpublishedReasonOther #>' onkeyup='onKeyUpTextbox(this)' style='width:220px;<#= UnpublishedReasonOtherDisplay #>' maxlength='50' />")
                           .Title("Unpublished Reason")
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Sortable(false)
                        .Width(220);
                    }
                    else
                    {
                        columns.Bound(letter => letter.PublishToReadingRoomName)
                            .Title(Constants.PUBLISH)
                            .Width(150)
                            .Sortable(false);

                        columns.Bound(letter => letter.UnpublishedReasonName).Template(letter =>
                            {%>
                                <%: letter.UnpublishedReasonName + (!string.IsNullOrEmpty(letter.UnpublishedReasonOther) ? "\n" + letter.UnpublishedReasonOther : string.Empty) %>
                            <% })
                            .ClientTemplate("<#= UnpublishedReasonName #><br /><#= UnpublishedReasonOther #>")
                            .Title("Unpublished Reason")
                            .Width(220)
                            .Sortable(false);                        
                    }

                    columns.Bound(letter => letter.LetterSequence)
                        .Template(letter =>
                        {%>
                        <%= Html.ActionLink(letter.LetterSequence.ToString(), Constants.CODING, Constants.LETTER, new { id = letter.LetterId, useDefaultFilter = true }, null)%>
                        <%}).ClientTemplate("<a href=" + Url.Action(Constants.CODING, Constants.LETTER, new { id = "<#= LetterId #>", useDefaultFilter = true }) + "><#= LetterSequence #></a >")
                              .Width(50).Title("Letter\n#").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.FullNameLastFirst).Title("Author Name");
                    columns.Bound(letter => letter.OrganizationName).Width(300);

                    columns.Bound(letter => letter.LetterTypeName)
                        .Width(120).Title("Letter\nType").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE })
                        .HeaderTemplate(() => 
                        {%>Letter Type&nbsp;<img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Update Master Form Tooltip" title="Updating the publishing status of a Master Form will also update all of the letters in the Form Set." /> <%});

                    
                    columns.Bound(letter => letter.DateSubmitted).Format("{0:MM/dd/yyyy}")
                        .Width(100).Title("Date\nSubmitted").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.Size).HtmlAttributes(new { style = "text-align: right;" })
                        .Width(75).Title("Size\n(bytes)").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });                    
                })
                .ClientEvents(events => events.OnDataBound("onDataBound").OnRowDataBound("onRowDataBound"))
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                //.DataBinding(dataBinding => dataBinding.Ajax().Select("ReadingRoomListBinding", Constants.READING_ROOM))
                .EnableCustomBinding(true)
                .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
                .Sortable()
                .Render();
    } %>