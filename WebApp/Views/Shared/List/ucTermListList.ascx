﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.TermList>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model)
            .Name(Constants.TERM_LIST)
            .DataKeys(keys => keys.Add(termList => termList.TermListId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(termList => termList.Name).Width(200);
                columns.Bound(termList => termList.Description);
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)            
            .HtmlAttributes(new { style = "width:650px;"})
            .Footer(false)
            .Render();
    } %>