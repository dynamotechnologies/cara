﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DataMartProjectViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.DataMart.DataMartProject>(Model.Projects)
                .Name(Constants.DATAMART_PROJECT + Constants.LIST)
                .DataKeys(keys => keys.Add(proj => proj.ProjectId).RouteKey("id"))
                .Columns(columns =>
                {
                    if (Utilities.UserHasPrivilege("MPRJ", Constants.SYSTEM))
                    {
                        columns.Bound(proj => proj.ProjectId).Template(proj =>
                            {%>
                                <%if (!proj.ExistsInCara || Model.IsObjection) { %>
                                <%: Html.RadioButton("rbProjectId", proj.ProjectId, (!string.IsNullOrEmpty(Model.ProjectId) ? proj.ProjectId.Equals(Model.ProjectId) : false))%>
                                <% } else { %>
                                    <div class="text">Already Exists<br />in CARA (<%: Html.ActionLink(Constants.VIEW, Constants.DETAILS + "ByPalsId", Constants.PROJECT, new { palsProjectId = proj.ProjectId }, null)%>)</div>
                                <% } %>
                            <%})
                            .ClientTemplate("<# if (!ExistsInCara || IsObjection) { #><input id='rbProjectId' name='rbProjectId' type='radio' value='<#= ProjectId #>' <#= ProjectId == '" + Model.ProjectId + "' ? 'checked' : '' #> /><# } " +
                                "else { #> Already Exists<br />in CARA (<a href='/Project/DetailsByPalsId?palsProjectId=<#= ProjectId #>'>View</a>) <# } #>")
                            .Title(Constants.SELECT)
                            .Sortable(false);
                    }
                    columns.Bound(proj => proj.ProjectId).Title(string.Format("{0} ID", Utilities.GetResourceString("PALS"))).Width(50);
                    columns.Bound(proj => proj.Name).Width(100);
                    columns.Bound(proj => proj.LeadManagementUnitName).Title(Utilities.GetResourceString("LeadMgmtUnit")).Width(125);
                    columns.Bound(proj => proj.NepaInfo.AnalysisTypeName).Title("Analysis\nType").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(proj => proj.NepaInfo.ProjectStatusName).Title("PALS\nProject\nStatus").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(proj => proj.ActivityNameList).Title("Activities").Width(200);
                    columns.Bound(proj => proj.EstimatedDecisionDate).Title("Estimated\nDecision\nDate")
                        .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                })                
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .DataBinding(dataBinding => dataBinding.Ajax().Select("DataMartProjectListBinding", Constants.PROJECT))
                .EnableCustomBinding(true)
                .DetailView(details => details.Template(proj => {%>
                <%= Html.Telerik().Grid(new string[] {proj.Description})
                    .Name("Description_" + proj.ProjectId)
                    .Columns(columns =>
                    {
                        columns.Bound(p => p).Width(500).Title("Description");
                    })
                    .Footer(false)
                %><%})
                    .ClientTemplate("<table cellspacing='0'><colgroup><col style='width:500px' /></colgroup><thead class='t-grid-header'><tr><th class='t-last-header t-header' scope='col'>Description</th></tr></thead>" +
                        "<tbody><tr><td class='t-last'><#= Description #></td></tr></tbody></table><hr />"))
                .Pageable(settings => settings.PageSize(Constants.DEFAULT_PAGE_SIZE).Total((int)ViewData["totalProjects"]).PageTo((int)ViewData["pageToProjects"]))
                .Render();
    } %>