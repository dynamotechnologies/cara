﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DmdDocumentViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.Documents)
            .Name(Constants.DOCUMENT + Constants.LIST)
            .DataKeys(keys => keys.Add(doc => doc.DmdDocumentId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(doc => doc.DmdDocumentId).Template(doc =>
                    {%><a target="_blank" href="<%= doc.WwwLink %>"><%: ViewData["Document" + doc.DmdDocumentId].ToString()%></a ><%})
                        .Title("Documento");
                columns.Bound(doc => doc.Size).Width(60).HtmlAttributes(new { style = "text-align: right;" }).Title("Tamaño");
                if (Model.ShowButtons && Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
                {
                    columns.Bound(doc => doc.DmdDocumentId).Template(doc =>
                        { %><a class="button" href="#"
                                onclick="unassociateDocument('<%= Url.Action(Constants.DELETE + Constants.DOCUMENT, new { id = doc.DmdDocumentId, mode = Model.Mode }) %>')"><%=Constants.REMOVE%></a ><% })
                        .Title(Constants.ACTION)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Width(100);}
                })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .Footer(false)
        .HtmlAttributes(new { style = "width: 700px;" })
        .Render();
    } %>
