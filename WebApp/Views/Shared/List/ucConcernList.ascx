﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ConcernResponseListViewModel>" %>

<%  if (Model.ConcernResponses != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.ConcernResponseSearchResult>(Model.ConcernResponses)
            .Name(Constants.LIST)
            .ToolBar(commands => commands.Custom().HtmlAttributes(new { style = "color: black;", id = "btnExpand", onclick = "onClickExpandAll(this);return false;" })
            .Text("Expand All"))
            .Columns(columns =>
            {
                if (Constants.EDIT.Equals(Model.Mode))
                {
                    columns.Bound(concern => concern.Sequence)
                        .Width(360)
                        .Template(concern => {%>
                            <%=Html.TextBox("txtSequence" + concern.ConcernResponseId, concern.Sequence, new { style = "width:25px;", maxlength = 5 })%>&nbsp;&nbsp;
                            <a href="<%=Url.Action(Constants.FIRST, new { id = concern.ConcernResponseId })%>"><img src="<%= ResolveUrl("~/Content/Images/arrow_first.png")%>" alt="Move to First" title="Move to First" /></a>
                            <a href="<%=Url.Action(Constants.PREV, new { id = concern.ConcernResponseId })%>"><img src="<%= ResolveUrl("~/Content/Images/arrow_prev.png")%>" alt="Move to Previous" title="Move to Previous" /></a>
                            <a href="<%=Url.Action(Constants.NEXT, new { id = concern.ConcernResponseId })%>"><img src="<%= ResolveUrl("~/Content/Images/arrow_next.png")%>" alt="Move to Next" title="Move to Next" /></a>
                            <a href="<%=Url.Action(Constants.LAST, new { id = concern.ConcernResponseId })%>"><img src="<%= ResolveUrl("~/Content/Images/arrow_last.png")%>" alt="Move to Last" title="Move to Last" /></a>
                            <%}).ClientTemplate("<input type='text' name='txtSequence<#= ConcernResponseId #>' value='<#= Sequence #>' style='width:25px;' maxlength='5' />&nbsp;&nbsp;" +
                                "<a href=" + Url.Action(Constants.FIRST, new { id = "<#= ConcernResponseId #>" }) + "><img src='" + Url.Content("~/Content/Images/arrow_first.png") + "' alt='Move to First' title='Move to First' /></a >" +
                                "<a href=" + Url.Action(Constants.PREV, new { id = "<#= ConcernResponseId #>" }) + "><img src='" + Url.Content("~/Content/Images/arrow_prev.png") + "' alt='Move to Previous' title='Move to Previous' /></a >" +
                                "<a href=" + Url.Action(Constants.NEXT, new { id = "<#= ConcernResponseId #>" }) + "><img src='" + Url.Content("~/Content/Images/arrow_next.png") + "' alt='Move to Next' title='Move to Next' /></a >" +
                                "<a href=" + Url.Action(Constants.LAST, new { id = "<#= ConcernResponseId #>" }) + "><img src='" + Url.Content("~/Content/Images/arrow_last.png") + "' alt='Move to Last' title='Move to Last' /></a >");
                }
                else
                {
                    columns.Bound(concern => concern.Sequence);
                }
                columns.Bound(concern => concern.ConcernResponseNumber)
                    .Title("C/R #")
                    .HeaderHtmlAttributes(new { title = "Concern/Response #" })
                    .Template(concern =>
                    {%>
                        <%= Html.ActionLink(concern.ConcernResponseNumber.ToString(), Constants.INPUT, Constants.CONCERN, new { id = concern.ConcernResponseId, trackBackPage = Constants.CONCERN_RESPONSE + Constants.LIST }, null)%>
                  <%}).ClientTemplate("<a href=" + Url.Action(Constants.INPUT, Constants.CONCERN, new { id = "<#= ConcernResponseId #>" }) + "><#= ConcernResponseNumber #></a >");
                columns.Bound(concern => concern.Label);
                columns.Bound(concern => concern.FormattedCode).Width(440).Title("C/R Code").HeaderHtmlAttributes(new { title = "Concern/Response Code" });
                columns.Bound(concern => concern.CommentCount).Title("Comment<br>Count");
                columns.Bound(concern => concern.ConcernResponseStatusName).Width(440);
                columns.Bound(concern => concern.LastUpdated).Template(concern =>
                {%>
                    <%=concern.LastUpdateText%><br />(<%=concern.LastUpdatedBy%>)
                    <%}).ClientTemplate("<#= LastUpdateText #><br />(<#= LastUpdatedBy #>)");
                columns.Bound(concern => concern.UserId).Template(concern =>
                {
                    if (!Utilities.UserHasPrivilege("WRSP", Constants.PHASE))//get user priviledge code
                    {
                        %><%=Model.TeamMembers[Model.SelectedIndex(concern.ConcernResponseId, concern.UserId)].Text%><%
                    }
                    else
                    {%>
                        <%: Html.Telerik().DropDownList()
                        .BindTo(Model.TeamMembers)
                        .Name(Model.UserDropDownName + concern.ConcernResponseId.ToString())
                        .ClientEvents(events => events.OnChange("tuserOnChange"))
                        .SelectedIndex(Model.SelectedIndex(concern.ConcernResponseId, concern.UserId))%>
                    <%}
                })
                  .ClientTemplate("<select id=\"user_<#=ConcernResponseId#>\" name=\"user_<#=ConcernResponseId#>\" onchange=\"userOnChange(<#=ConcernResponseId#>)\">" + 
                    Model.TeamMemberSelectionString +
                    "</select>")
                  .Title("Assigned To");
            })
			.ClientEvents(events => events.OnDataBound("onDataBound"))
			.NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            //.DataBinding(dataBinding => dataBinding.Ajax().Select("ConcernListBinding", Constants.CONCERN))
            .EnableCustomBinding(true)
            .DetailView(details => details.Template(c => {%>
                <%= Html.Telerik().Grid(new string[] { "<b>Concern Statement:</b> " + c.ConcernText + "<br /><b>Response:</b> " + c.ResponseText })
                    .Name("ConcernText_" + c.ConcernResponseId)
                    .Columns(columns =>
                    {
                        columns.Bound(t => t).Width(600).Encoded(false);
                    })
                    .HtmlAttributes(new { @class = "concern-grid" })
                    .ClientEvents(events => events.OnLoad("hideHeader"))
                    .Footer(false)
                %><%})
            .ClientTemplate("<div style='width:600px;'><b>Concern:</b> <#= ConcernText #><br /><b>Response:</b> <#= ResponseText #></div>"))
            .Pageable(settings => settings.PageSize(Constants.EDIT.Equals(Model.Mode) ? Constants.MAX_ROW_COUNT : Utilities.GetUserSession().SearchResultsPerPage)
                .Total((int)ViewData["total"])
                .PageTo((int)ViewData["pageNum"])
                .Style(GridPagerStyles.NextPreviousAndNumeric | GridPagerStyles.PageInput))
            .Sortable(sorting => sorting.Enabled(!Constants.EDIT.Equals(Model.Mode)))
            .HtmlAttributes(new { style = "width: 900px;" })
            .Render();
    } %>

    <script type="text/javascript">
        function hideHeader() {
            $('.concern-grid .t-header').hide();
        }
    </script>
