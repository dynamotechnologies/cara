﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterReadingRoomListViewModel>" %>
<script type="text/javascript">
    function ucPublicReadingRoomSearchListOpenWindow(url) {

        var pdfWindow = window.open(url, 'ucPublicReadingRoomSearchList', 'toolbar=1,scrollbars=1,location=1,status=1,menubar=1, width=' + screen.width + ',height=' + screen.height + ',resizable=1');
        if (pdfWindow != null) {

            pdfWindow.document.onreadystatechange = function () {
                if (pdfWindow.document.readyState == "interactive") {
                    if (pdfWindow.document.title.indexOf("Error") != -1) {
                        pdfWindow.close();
                        alert("The document is not available. Please try again later.");
                    } else {
                        pdfWindow.focus();
                    }
                }
            };
        }
        //alert(url);
        return false;
    }
</script>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.LetterReadingRoomSearchResult>()
                .Name(Constants.LIST)
                .BindTo(Model.Letters)
                .ToolBar(commands => 
                {
                    commands.Template(() =>
                    { %>                        
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" width="72%">
                                    <a class="t-button" href="#" id="btnCheckAll" onclick="onClickCheckAll(this, false);return false;" style="color: black;">Check All</a>
                                </td>
                                <td align="right">
                                    <table border="0" cellpadding="0" cellspacing="0" width="28%">
                                        <tr>
                                            <td>Results Per Page:</td>
                                            <td><%: Html.Telerik().DropDownListFor(model => model.SearchResultsPerPage).BindTo(Model.Rows)
                                                    .HtmlAttributes(new { style = "width:40px;" })
                                                    .ClientEvents(events => events.OnChange("onChangeSearchResultsPerPage")) %></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <% });
                })
                .Columns(columns =>
                {
                    columns.Bound(l => l.LetterId).Template(l =>
                        {%><input type="checkbox" name="chkDownload_<%=l.LetterId %>" id="chkDownload_<%=l.LetterId %>" value="<%=l.LetterId %>" <%= l.PublishToReadingRoom && l.DmdId != null ? string.Empty : "disabled" %> onclick="onClickCheckboxDmd(this);" /><%})
                            .Title(Constants.DOWNLOAD)
                            .Width(75)
                            .HtmlAttributes(new { style = "text-align: center;" })
                            .Sortable(false);
                    
                    columns.Bound(l => l.FullNameLastFirst).Title("Author Name").Template(l =>
                        {%>
                            <% // If the letter is unpublished, display the information message
                                if (l.PublishToReadingRoom)
                                {
                                    if (l.DmdId == null)
                                    {%>
                                       <a href="#" onclick="return openUnavailableWindow();"><%: l.FullNameLastFirst%></a>
                                    <%}
                                    else
                                    {
                                        // If there are attachments, take them to a page that displays the letter information and all of the attachments
                                        if (l.HasAttachments)
                                        { %>
                                        <%= Html.ActionLink(l.FullNameLastFirst, Constants.LETTER, Constants.PUBLIC, new { id = l.LetterId, project = Model.ProjectNumber }, null)%>
                                        <% }
                                        else
                                        {%>
                                        <%= Html.ActionLink(l.FullNameLastFirst, string.Format("{0}{1}", Constants.DOWNLOAD, "CommentFile"), Constants.PUBLIC, new { dmdId = l.DmdId, project = Model.ProjectNumber }, new { onclick = "ucPublicReadingRoomSearchListOpenWindow(this.href);return false;" })%>
                                        <% }
                                    }
                                }
                                else
                                { %>
                                   <a href="#" onclick="return openUnpublishedWindow();"><%: l.FullNameLastFirst%></a>
                               <% } %>
                        <%})
                        .Width(220);
                    columns.Bound(l => l.OrganizationName).Width(223);
                    columns.Bound(l => l.DateSubmitted).Format("{0:MM/dd/yyyy}")
                        .Width(100).Title("Date\nSubmitted").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(l => l.Size).HtmlAttributes(new { style = "text-align: right;" })
                        .Width(75).Title("Size\n(bytes)").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
          
                    // -----------------------------------------------------------------------------------------------
                    // hidden column needs to be put at the end due to a known IE defect on Telerik Grid control
                    // -----------------------------------------------------------------------------------------------
                    columns.Bound(l => l.ReadingRoomKeyCount)
                        .Aggregate(agg => agg.Count())
                        .GroupHeaderTemplate(result =>
                        {%><%: FormUtilities.ParseElementFromKey(result.Key.ToString(), Constants.NAME, Constants.LETTERS) %>
                        <%})
                        .Hidden();
                        %>
                <% })
                .Groupable(grouping => grouping.Groups(groups =>
                {
                    groups.Add(fs => fs.ReadingRoomKeyCount);
                }).Visible(false))
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .EnableCustomBinding(true)
                .Pageable(settings => settings.PageSize(Model.SearchResultsPerPage).Total((int)ViewData["total"]))
                .TableHtmlAttributes(new { style="width:723px;table-layout:fixed" })
                .Sortable()
                .Render();
    } %>