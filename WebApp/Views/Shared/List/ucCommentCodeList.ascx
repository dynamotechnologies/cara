﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.CommentCode>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.OrderBy(x => x.Sequence))
            .Name(Constants.COMMENT_CODE)
            .DataKeys(keys => keys.Add(code => code.PhaseCodeId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(code => code.PhaseCode.CodeNumberDisplay).Width(150).Title("Code Number");
                columns.Bound(code => code.PhaseCode.CodeName);
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Footer(false)
            .HtmlAttributes(new { style = "width: 600px;" })
            .Render();
    } %>