﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.CodingTemplate>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model)
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(template => template.CodingTemplateId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(template => template.Name).Template(template =>
                    {%><a href="<%= Url.Action(Constants.DETAILS, new { id = template.CodingTemplateId }) %>"><%= template.Name%></a ><%})
                        .Width(100)
                        .Title("Template Name");
                    columns.Bound(template => template.CodingTemplateId).Template(template =>
                    {%><a class="button" href="<%= Url.Action(Constants.INPUT, new { id = template.CodingTemplateId }) %>"><%=Constants.EDIT%></a >
                        <% })
                        .Title(Constants.ACTION)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" }
                    )
                .Width(75).Sortable(false);
            })
        .Footer(false)
        .HtmlAttributes(new { style = "width: 400px;" })
        .Render();
    } %>