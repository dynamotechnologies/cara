﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.ConcernResponseByCommenterReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.ConcernResponseByCommenterReportData>(Model.ReportData)
                .Name("CocnernResponseByCommenter")
                .Columns(columns =>
                {
                    columns.Bound(x => x.LastName);
                    columns.Bound(x => x.FirstName);
                    columns.Bound(x => x.OrganizationName);
                    columns.Bound(x => x.LetterSequence).Title("Letter #");
                    columns.Bound(x => x.ConcernResponseNumber).Title("Concern Response Sequence"); ;
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("ConcernResponseByCommenterBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>

      