﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.CommenterOrganizationTypeData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.CommenterOrganizationTypeData>(Model.ReportData)
                .Name("UntimelyComments")
                .Columns(columns =>
                {
                        columns.Bound(x => x.OrgTypeName).Title("Orgnanization Type");
                        columns.Bound(x => x.OrgName).Title("Organization");
                        columns.Bound(x => x.AuthorName);
                        columns.Bound(x => x.LetterSequence).Title("Letter #");
                          
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("CommenterOrgTypeBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>