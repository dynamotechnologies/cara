﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.StandardReportWithGroupingViewModel>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.ResponseStatusReportData>()
                .Name("Grid")
                .ToolBar(commands => 
                {
                    commands.Template(() =>
                    { %>                        
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                  <a class="t-button" href="#" id="btnExpand" onclick="onClickExpandAll(this);return false;" style="color: black;">Collapse All</a>
                                </td>
                                <td align="right">
                                    <span>Total Response Status Letters: <%= ViewData["total"]%></span>
                                </td>
                            </tr>
                        </table>                                             
                        <% });
                })
               .BindTo(Model.ReportData)
               .Columns(columns =>
                {                   
                    columns.Bound(x => x.LetterCommentNumber).Title("Comment #");
                    columns.Bound(x => x.CommentText).Width(400).Title("Comment\nText")
                           .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });  
                    columns.Bound(x => x.CodeName).Width(200).Title("Code").Encoded(false)
                          .ClientTemplate("<span title='<#= CodeName #>'><#= CodeName #></span>");
             
 // -----------------------------------------------------------------------------------------------
                // Code to display the Group header and Count
                // -----------------------------------------------------------------------------------------------
                        columns.Bound(x => x.GroupBy1)
                          .Aggregate(aggregates => aggregates.Count())
                          .GroupHeaderTemplate(result =>
                          {%>
                                       
                        <%= result.Key%> 
                     <% })
                    .Hidden();
                           
                   
                    
               })
               .Groupable(grouping => grouping.Groups(groups =>
               {
                   groups.Add(x => x.GroupBy1);

               }).Visible(false))
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               .Render();
    
       
     %>

      