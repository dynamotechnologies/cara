﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.MailingListReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.MailingListReportData>(Model.ReportData)
                .Name("MailingList")
                .Columns(columns =>
                {
                   
                        columns.Bound(x => x.FirstName);
                        columns.Bound(x => x.LastName);
                        columns.Bound(x => x.OrgName).Title("Organization Name");
                        columns.Bound(x => x.Address1).Title("Address");
                        columns.Bound(x => x.City);
                        columns.Bound(x => x.StateId).Title("State");
                        columns.Bound(x => x.ZipPostal).Title("Zip Code");
                        columns.Bound(x => x.Email);
                        columns.Bound(x => x.ContactMethod);
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("MailingListBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>

      