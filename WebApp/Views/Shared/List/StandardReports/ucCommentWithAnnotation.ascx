﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.CommentWithAnnotationReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.CommentWithAnnotationReportData>(Model.ReportData)
                .Name("CommentWithAnnotation")
                .Columns(columns =>
                {
                   columns.Bound(x => x.LetterNumber).Title("Letter");
                   columns.Bound(x => x.CommentNumber).Title("Comment");
                   columns.Bound(x => x.CommentText).Title("Text");
                   columns.Bound(x => x.Annotation).Title("Annotation");
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("CommentWithAnnotationBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>