﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.StandardReportWithGroupingViewModel>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.CommentReportData>()
                .Name("Grid")
                .ToolBar(commands => 
                {
                    commands.Template(() =>
                    { %>                        
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <a class="t-button" href="#" id="btnExpand" onclick="onClickExpandAll(this);return false;" style="color: black;">Collapse All</a>
                                </td>
                                <td align="right">
                                    <span>Total Comments: <%= ViewData["total"]%></span>
                                </td>
                            </tr>
                        </table>                                             
                        <% });
                })
                .BindTo(Model.ReportData)
                .Columns(columns =>
                {
                    
                        columns.Bound(x => x.CommentCodeText).Title("Code");
                        columns.Bound(x => x.LetterCommentNumber).Title("Comment #");
                        columns.Bound(x => x.CommentText);
                        
                              
                
                // -----------------------------------------------------------------------------------------------
                // Code to display the Group header and Count
                // -----------------------------------------------------------------------------------------------
                  columns.Bound(x => x.CodeCategory)
                    .Aggregate(aggregates => aggregates.Count())        
                    .GroupHeaderTemplate(result =>        
                    {%>                
                        <%= result.Key %> 
                     <% })
                    .Hidden();
       
                    
               })
               .Groupable(grouping => grouping.Groups(groups =>
               {
                   groups.Add(x => x.CodeCategory);
           
               }).Visible(false))
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
              // .Sortable()
               .Render();
    
       
     %>

      