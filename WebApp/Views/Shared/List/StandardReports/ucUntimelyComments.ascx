﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.UntimelyCommentsReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.UntimelyCommentsReportData>(Model.ReportData)
                .Name("UntimelyComments")
                .Columns(columns =>
                {
                        columns.Bound(x => x.AuthorName).Title("Name");
                        columns.Bound(x => x.OrgName).Title("Organization");
                        columns.Bound(x => x.Address1).Title("Address");
                        columns.Bound(x => x.Phone);
                        columns.Bound(x => x.Email);
                        columns.Bound(x => x.LetterSequence).Title("Letter #");
                        columns.Bound(x => x.DateSubmitted).Title("Submitted");
                    
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("UntimelyCommentsBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               // .Sortable()
               .Render();
%>