﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.StandardReportWithGroupingViewModel>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.EarlyActionReportData>()
                .Name("Grid")
                .ToolBar(commands => 
                {
                    commands.Template(() =>
                    { %>                        
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <a class="t-button" href="#" id="btnExpand" onclick="onClickExpandAll(this);return false;" style="color: black;">Expand All</a>
                                </td>
                                <td align="right">
                                    <span>Total Early Attention Items: <%= ViewData["total"]%></span>
                                </td>
                            </tr>
                        </table>                                             
                        <% });
                })
                .BindTo(Model.ReportData)
                .Columns(columns =>
                {
                        columns.Bound(x => x.DateSubmitted).Title("Date \nSubmitted").Format("{0:MM/dd/yyyy}")
                            .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                        columns.Bound(x => x.LetterSequence).Title("Letter #");
                        columns.Bound(x => x.CommentNumber).Title("Comment #");
                        columns.Bound(x => x.Name).Title("Name");
                        columns.Bound(x => x.Address1).Title("Address");
                        columns.Bound(x => x.City);
                        columns.Bound(x => x.StateId).Title("State");
                        columns.Bound(x => x.ZipPostal).Title("Zip Code");
                              
                
                // -----------------------------------------------------------------------------------------------
                // Code to display the Group header and Count
                // -----------------------------------------------------------------------------------------------
                        columns.Bound(x => x.TermListName)
                          .Aggregate(aggregates => aggregates.Count())
                          .GroupHeaderTemplate(result =>
                          {%>                
                        <%= result.Key%> 
                     <% })
                    .Hidden();
       
                    
               })
               .Groupable(grouping => grouping.Groups(groups =>
               {
                   groups.Add(x => x.TermListName);
           
               }).Visible(false))
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .EnableCustomBinding(true)
               .DetailView(details => details.Template(x => {%>
                <%= Html.Telerik().Grid(new string[] { x.CommentText })
                    .Name("CommentText_" + x.CommentId)
                    .Columns(columns =>
                    {
                        columns.Bound(p => p).Width(500).Title("Comment Text");
                        
                    })
                    .Footer(false)
                %><%})
				.ClientTemplate("<div class=\"t-widget t-grid\" id=\"CommentText_<#= CommentId #>\"><table cellspacing=\"0\"><colgroup><col style=\"width:500px\" /></colgroup><thead class=\"t-grid-header\"><tr><th class=\"t-header t-last-header\" scope=\"col\">Full " + Utilities.GetResourceString(Constants.COMMENT) + " Text</th></tr></thead><tbody><tr><td class=\"t-last\"><#= CommentText #></td></tr></tbody></table></div>")
				)
            .CellAction(cell => {
                if (cell != null && cell.Column.Title == "Codes")
                {
                    cell.HtmlAttributes["title"] = cell.DataItem.CommentText;
                }
            })
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
    
       
     %>

      