﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.TeamMemberReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.TeamMemberReportData>(Model.ReportData)
                .Name("TeamMember")
                .Columns(columns =>
                {
                   columns.Bound(x => x.TeamMemberName).Title("Name");
                   columns.Bound(x => x.TeamMemberPhone).Title("Phone");
                   columns.Bound(x => x.TeamMemberEmail).Title("Email");
                   columns.Bound(x => x.TeamMemberStatus).Title("Status");
                                          
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("TeamMemberBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>