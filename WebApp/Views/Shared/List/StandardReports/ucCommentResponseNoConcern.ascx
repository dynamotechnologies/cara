﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.CommentResponseNoConcernReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.CommentResponseNoConcernReportData>(Model.ReportData)
                .Name("CommentResponseNoConcern")
                .Columns(columns =>
                {
                   columns.Bound(x => x.UniqueNumber).Title("Comment #");
                   columns.Bound(x => x.ConcatenatedCommentText).Title("Comment Text");
                   columns.Bound(x => x.ResponseNumber).Title("Response<br/>Sequence #");
                   columns.Bound(x => x.ResponseTextNonHtml).Title("Response Text");
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("CommentResponseNoConcernBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>