﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.ConcernResponseWithAssignedToReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.ConcernResponseWithAssignedToReportData>(Model.ReportData)
                .Name("ConcernResponseWithAssignedTo")
                .Columns(columns =>
                {
                   columns.Bound(x => x.ConcernResponseNumber).Title("C/R Number");
                   columns.Bound(x => x.ConcernText).Title("Concern Text");
                   columns.Bound(x => x.ResponseText).Title("Response Text");                   
                   columns.Bound(x => x.CRAssignedTo).Title(" CR Assigned To");
                                          
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("ConcernResponseWithAssignedToBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>