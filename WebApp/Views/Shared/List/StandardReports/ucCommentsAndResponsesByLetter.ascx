﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.CommentsAndResponsesByLetterReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.CommentsAndResponsesByLetterReportData>(Model.ReportData)
                .Name("CommentsAndResponesByLetter")
                .Columns(columns =>
                {
                        columns.Bound(x => x.AuthorList).Title("Author(s)").Width("10%").Template(x => x.AuthorList.ReplaceLineBreaks());
                        columns.Bound(x => x.CommentText).Title("Comment").Width("40%");
                        columns.Bound(x => x.ResponseText).Title("Response").Width("50%").Template(x => x.ResponseText);
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("CommentsAndResponsesByLetterBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]).Enabled(Model.HasPaging))
               .Render();
%>

      