﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.ResponseToCommentReportData>>" %>

<%
    Html.Telerik().Grid<Aquilent.Cara.Domain.Report.ResponseToCommentReportData>(Model.ReportData)
                .Name("ResponseToComment")
                .Columns(columns =>
                {
                    columns.Bound(x => x.ResponseSequence).Title("Sequence #");
                    columns.Bound(x => x.ResponseNumber).Title("ID #");
                    columns.Bound(x => x.ConcernTextNonHtml).Title("Concern Text");
                    columns.Bound(x => x.ResponseTextNonHtml).Title("Response Text");
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               //.DataBinding(dataBinding => dataBinding.Ajax().Select("ResponseToCommentBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               .DetailView(details => details.Template(x =>
                    {%>
                        <%= Html.Telerik().Grid(x.Comments)
                                .Name("Response_" + x.ResponseId)
                                .Columns(columns =>
                                    {
                                        columns.Bound(p => p.UniqueNumber).Title(Utilities.GetResourceString(Constants.COMMENT) + " #");
                                        columns.Bound(p => p.ConcatenatedCommentText).Title(Utilities.GetResourceString(Constants.COMMENT).Pluralize());
                                    })
                                .Footer(false)
                                                
                %><%})).Render();
                      
%>