﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.CommentWithCRInfoReportData>>" %>

<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.CommentWithCRInfoReportData>(Model.ReportData)
                .Name("CommentWithCRInfo")
                .Columns(columns =>
                {
                   columns.Bound(x => x.CRNumber).Title("C/R #");
                   columns.Bound(x => x.CRCodes).Title("C/R Codes");
                   columns.Bound(x => x.CommentNumber).Title("Comment #");
                   columns.Bound(x => x.CommentCodes).Title("Comment Codes");
                   columns.Bound(x => x.CommentText).Title("Text");
                   columns.Bound(x => x.SampleStatement).Title("Sample Comment");
                   columns.Bound(x => x.AssignedTo).Title("Assigned To");
                                          
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("CommentWithCRInfoBinding", Constants.PHASE_REPORT))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>