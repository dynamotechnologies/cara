﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DocumentViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.Documents)
            .Name(Constants.DOCUMENT + Constants.LIST)
            .DataKeys(keys => keys.Add(doc => doc.DocumentId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(doc => doc.Name).Template(doc =>
                    {%><a href="<%= Url.Action(Constants.DOWNLOAD, Constants.DOCUMENT, new { id = doc.DocumentId }) %>"><%= doc.Name%></a ><%})
                        .Title(Constants.DOCUMENT);
                columns.Bound(doc => doc.File.Size).Width(100).Title("Size (bytes)").HtmlAttributes(new { style = "text-align: right;" });
                //if (Model.ShowButtons && Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
                if ( Utilities.UserHasPrivilege("MLTR", Constants.PHASE) && Model.ShowActions)
                {
                    columns.Bound(doc => doc.DocumentId).Template(doc =>
                        {
                            if (Model.ShowAddContentButton)
                            {
                                if (Aquilent.Framework.Documents.TextExtractorFactory.IsExtractable(doc.File.ContentType))
                                { %>
                                   <%: Html.ActionLink("Add Content To " + Constants.LETTER, "AddDocumentText", new { id = Model.LetterId, documentId = doc.DocumentId }, new { @class = "button" })%>
                               <%}
                                else
                                { %>
                                     N/A 
                                <% }
                             }
                            
                             if (Model.ShowDeleteButton)
                             { 
                                 if (Model.Deletable(doc.DocumentId)) {
                                 %>
                                    <input type="button" name="btnDeleteAttachment<%=doc.DocumentId%>" value="Delete" onclick="javascript:deleteAttachment(<%=Model.LetterId %>,<%=doc.DocumentId%>);" style="height:auto;" class="button"/>
                                 <% }
                                 else{ %>
                                     N/A
                                 <% }  
                              }  
                        })
                        .HeaderTemplate(() => {%>
                         <%= Constants.ACTION %>&nbsp;
                             <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Action" title="Click on Add Content to Letter to preview and append the text of an attached document to this letter. This functionality only supports PDF (extension .PDF) and Text (extension .TXT) documents." />
                        
                         <%})
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Width(195);
                }
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .HtmlAttributes(new { style = "width: 765px;" })
        .Footer(false)
        .Render();
        %>
        <br />
        <% if (!Model.HideDownloadAllButton && Model.Documents != null && Model.Documents.Count > 0)
           { %>
            <%=Html.ActionLink(Constants.DOWNLOAD + " All", Constants.DOWNLOAD + "All",
                Constants.DOCUMENT, new { ids = Model.DocumentIds }, new { @class = "button" })%>
        <% } %>
        <br />
    <% } %>


    <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.letterdetails.js")); %>