﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Framework.Security.User>>" %>
<%@ Import Namespace="Aquilent.Framework.Security" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Framework.Security.User>(Model.OrderBy(usr => usr.Username))
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(usr => usr.UserId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(usr => usr.Username);
                columns.Bound(usr => usr.LastName);
                columns.Bound(usr => usr.FirstName);
                columns.Bound(usr => usr.Email);
                if (Utilities.UserHasPrivilege("MUC", Constants.SYSTEM))
                {
                    columns.Bound(usr => usr.UserId)
                        .Template(usr =>
                        {%>
                            <% if (usr.UserId <= 0)
                               { %>
                                <%= Html.ActionLink(string.Format("{0} to {1} and {2} {3}", Constants.ADD, Utilities.GetResourceString("AppNameAbbv"), Constants.MANAGE, Constants.ROLE.Pluralize()), Constants.CREATE, Constants.USER, new { id = usr.Username }, new { @class = "button", id = "btnUpdate_" + usr.Username.ToString() })%>
                            <% }
                            else { %>
                                <%: Html.ActionLink(Constants.MANAGE + " " + Constants.ROLE.Pluralize(), "Roles", "User", new { id = usr.UserId }, new { @class = "button" }) %>
                            <% } %>
                        <%})
                        .ClientTemplate("<# if (UserId <= 0) { #><a id=\"btnUpdate_<#= Username #>\" class=\"button\" href=\"/User/Create/<#= Username #>?roles=None\">" + string.Format("{0} to {1}", Constants.ADD, Utilities.GetResourceString("AppNameAbbv")) +
                            "</a > <# } else { #> Already Exists in CARA <# } #>")
                        .Title(Constants.ACTION)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Sortable(false);
                }
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .DataBinding(dataBinding => dataBinding.Ajax().Select("DataMartUserListBinding", Constants.USER))
            .EnableCustomBinding(true)
            .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]))            
            .Render();
    } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.datamartuserlist.js"));
%>