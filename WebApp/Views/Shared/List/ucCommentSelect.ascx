﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CommentListViewModel>" %>

<%  if (Model.CommentDetails != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.CommentDetails>(Model.CommentDetails)    
            .Name(Constants.LIST)
            .ToolBar(commands => commands.Custom().HtmlAttributes(new { style = "color: black;", id = "btnExpand", onclick = "onClickExpandAll(this);return false;" })
                .Text("Expand All"))
            .Columns(columns =>
            {
                if (Model.CanEdit && Utilities.UserHasPrivilege("WRSP", Constants.PHASE))
                {
                    columns.Bound(comment => comment.CommentId).Template(comment =>
                        {
                            string chkComment = "";
                            if (!String.IsNullOrWhiteSpace(Model.CommentIdList))
                            {
                                if (Model.CommentIdList.Split(',').Contains(comment.CommentId.ToString()))
                                {
                                    chkComment = "checked = \"checked\"";
                                }
                            }%>
                            <input type="checkbox" id="chkCommentId_<%=comment.CommentId %>" value="<%=comment.CommentId %>" <%=comment.Disabled%> onclick = "updateCommentIdList(this);" <%=chkComment %>/><%})
                            .ClientTemplate("<input type=\"checkbox\" id=\"chkCommentId_<#= CommentId #>\" value=\"<#= CommentId #>\" <#=Disabled #> onclick = \"updateCommentIdList(this);\" />")
                            .Title(Constants.SELECT)
                            .Width(50)
                            .Sortable(false)
                            .HtmlAttributes(new { style = "width:30px;" });
                }
                columns.Bound(comment => comment.ConcernResponseNumber)
                    .Template(comment =>
                        {%>
                            <%= comment.ConcernResponseId != null ? Html.ActionLink(comment.ConcernResponseNumber.ToString(), Constants.INPUT, Constants.CONCERN, new { id = comment.ConcernResponseId, trackbackpage = Constants.COMMENT + Constants.LIST }, null) : null%>
                        <%})
                    .ClientTemplate("<a href=" + Url.Action(Constants.INPUT, Constants.CONCERN, new { id = "<#= ConcernResponseId #>" }) + "><#= ConcernResponseNumber #></a >")
                    .Title("C/R #")
                    .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE, title = "Concern/Response #" });
                columns.Bound(comment => comment.FormattedConcernResponseCode)
                    .Title("C/R Codes")
                    .Encoded(false)
                    .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE, title = "Concern/Response Code" })
                    .HtmlAttributes(new { style = "width:140px;" });
                columns.Bound(comment => comment.LetterCommentNumber).Template(comment =>
                    {%><a href="<%= Url.Action(Constants.CODING, Constants.LETTER, new { id = comment.LetterId, commentId = comment.LetterCommentNumber, useDefaultFilter = true }) %>"><%= comment.LetterCommentNumber%></a ><%})
                    .ClientTemplate("<a href=" + Url.Action(Constants.CODING, Constants.LETTER, new { id = "<#= LetterId #>", commentId = "<#= LetterCommentNumber #>", useDefaultFilter = "<#= true #>" }) + "><#= LetterCommentNumber #></a >")
                    .Title("Comment #")
                    .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });              
                columns.Bound(comment => comment.CommentCodeText)
                    .Title("Comment Codes")
                    .Encoded(false)
                    .ClientTemplate("<span title='<#= CommentCodeText #>'><#= CommentCodeText #></span>")
                    .HtmlAttributes(new { style = "width:140px;" });
                columns.Bound(comment => comment.CommentText)
                    .Template(comment =>
                        {
                            if (!String.IsNullOrWhiteSpace(comment.CommentText))
                            {
                                int charCount = comment.CommentText.Length;
                                string shortText = string.Empty;
                                string shorterText = string.Empty;
                                int characterLimit = 680;
                                if (charCount > characterLimit)
                                {
                                    shortText = comment.CommentText.Substring(0, characterLimit);
                                    int lastSpace = shortText.LastIndexOf(" ") > 300 ? shortText.LastIndexOf(" ") : characterLimit;
                                    shorterText = comment.CommentText.Substring(0, lastSpace);
                                %> 
                                     <%= shorterText%><span class="t-hierarchy-cell">...<a id="lnkEntireCommentText" class="t-icon t-plus" href="#"></a >&nbsp;&nbsp;</span>
                                <%
}
                                else
                                {%>
                                    <%= comment.CommentText %>
                                <%}
                            }
                        })
                    .Sortable(true)
                    .Title("Text");
                columns.Bound(comment => comment.SampleStatementDisplay)
                    .Title("Sample\nComment")
                    .HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });               
                columns.Bound(comment => comment.UserId).Template(comment =>
                {
                    if (comment.ConcernResponseId != null || !Utilities.UserHasPrivilege("WRSP", Constants.PHASE))//get user priviledge code
                    {
                        %><%=Model.TeamMembers[Model.SelectedIndex(comment.CommentId, comment.UserId)].Text%><%
}
                    else
                    {%>
                        <%: Html.Telerik().DropDownList()
                        .BindTo(Model.TeamMembers)
                        .Name("user_" + comment.CommentId.ToString())
                        .ClientEvents(events => events.OnChange("tuserOnChange"))
                        .SelectedIndex(Model.SelectedIndex(comment.CommentId, comment.UserId))%>
                    <%}

                })
                  .ClientTemplate("<select id=\"user_<#=CommentId#>\" name=\"user_<#=CommentId#>\" onchange=\"userOnChange(<#=CommentId#>)\">" +
                    Model.TeamMemberSelectionString +
                    "</select>")
                  .Title("Assigned To")
                  .Sortable(true);
            })
            .ClientEvents(events => events.OnDataBound("onDataBound"))
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .EnableCustomBinding(true)
            .DetailView(details => details.Template(comment =>
            {%>
                <%= Html.Telerik().Grid(new string[] { comment.CommentText })
                    .Name("CommentText_" + comment.CommentId)
                    .Columns(columns =>
                    {
                        columns.Bound(p => p).Width(500).Title("Full " + Utilities.GetResourceString(Constants.COMMENT) + " Text");
                    })
                    .Footer(false)
                %><%})
                .ClientTemplate("<div class=\"t-widget t-grid\" id=\"CommentText_<#= CommentId #>\"><table cellspacing=\"0\"><colgroup><col style=\"width:500px\" /></colgroup><thead class=\"t-grid-header\"><tr><th class=\"t-header t-last-header\" scope=\"col\">Full " + Utilities.GetResourceString(Constants.COMMENT) + " Text</th></tr></thead><tbody><tr><td class=\"t-last\"><#= CommentText #></td></tr></tbody></table></div>")
                )
            .CellAction(cell =>
            {
                if (cell != null && cell.Column.Title == "Codes")
                {
                    cell.HtmlAttributes["title"] = cell.DataItem.CommentCodeText;
                }
            })
            .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
            //CARA-956 removed sorting parameters from this page sorting will be handled by spscomments stored procedure
            .Sortable()
            .HtmlAttributes(new { style = "width: 90%;" })
            .Render();
    } %>

<!-- variable to keep track of the checked comments -->
<%: Html.HiddenFor(model => model.CommentIdList) %>
<!-- variable to keep track of tack assignment changes -->
<%: Html.HiddenFor(m => m.TaskAssignmentContext)%>
<%: Html.HiddenFor(m => m.TaskAssignmentUsers)%>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.commentselect.js"));
%>