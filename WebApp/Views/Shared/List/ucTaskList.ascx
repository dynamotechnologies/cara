﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.TaskListViewModel>" %>

<% Html.Telerik().Grid<Aquilent.Cara.Domain.TaskItem>(Model.TaskItems)
       .Name("TaskList")
       .Columns(columns =>
           {
               columns.Bound(task => task.ProjectName)
                   .Template(task =>
                       {%>
                            <%=String.Format("{0} ({1})", task.ProjectName, task.ProjectNumber)%>
                    <% }).Title("Project Name (ID)").Sortable(false);

               columns.Bound(task => task.PhaseName).Title("Comment Period Name").Sortable(false);
                              
               columns
                   .Bound(task => task.CommentCount)
                   .Title("My Comments")
                   .Template(t => 
                   {%>
                    <% if (t.CommentCount > 0) 
                       { %>
                           <a href="<%= Url.Action(Constants.LIST, Constants.COMMENT, new { projectId = t.ProjectId, phaseId = t.PhaseId, filter="AssignedTo|" + t.UserId }) %>"><%= t.CommentCount%></a ><%
                       }
                       else
                       {
                         %> 0 <%
                       }
                   })
                   .ClientTemplate("<# if (CommentCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.COMMENT, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "AssignedTo|<#=UserId #>" }) + "><#= CommentCount #></a ><# } else { #> 0 <# } #>")
                   .Sortable(false)
                   .Visible(Model.Mode == TaskListViewModel.TaskMode.CommentsConcerns);
                   
               columns
                   .Bound(task => task.ConcernCount)
                   .Title("My Concerns")
                   .Template(t => 
                   {%>
                    <% if (t.ConcernCount > 0) 
                       { %>
                           <a href="<%= Url.Action(Constants.LIST, Constants.CONCERN, new { projectId = t.ProjectId, phaseId = t.PhaseId, filter="AssignedTo|" + t.UserId }) %>"><%= t.ConcernCount%></a ><%
                       }
                       else
                       {
                         %> 0 <%
                       }
                   })
                   .ClientTemplate("<# if (ConcernCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.CONCERN, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "AssignedTo|<#=UserId #>" }) + "><#= CommentCount #></a ><# } else { #> 0 <# } #>")
                   .Sortable(false)
                   .Visible(Model.Mode == TaskListViewModel.TaskMode.CommentsConcerns);
               
               columns
                   .Bound(task => task.EarlyAttentionCount)
                   .Title("Early Attention Review")
                   .Template(t => 
                   {%>
                    <% if (t.EarlyAttentionCount > 0) 
                       { %>
                           <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = t.ProjectId, phaseId = t.PhaseId, filter = "NeedsReview" }) %>"><%= t.EarlyAttentionCount%></a ><%
                       }
                       else
                       {
                         %> 0 <%
                       }
                   })
                   .ClientTemplate("<# if (EarlyAttentionCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "NeedsReview" }) + "><#= EarlyAttentionCount #></a ><# } else { #> 0 <# } #>")
                   .Sortable(false)
                   .Visible(Model.Mode == TaskListViewModel.TaskMode.ReviewableLetters);
                   
               columns
                   .Bound(task => task.UnpublishedLetterCount)
                   .Title("Reading Room Review")
                   .Template(t => 
                   {%>
                    <% if (t.UnpublishedLetterCount > 0) 
                       { %>
                           <a href="<%= Url.Action(Constants.LIST, Constants.READING_ROOM, new { projectId = t.ProjectId, phaseId = t.PhaseId, filter="Unpublished"}) %>"><%= t.UnpublishedLetterCount%></a ><%
                       }
                       else
                       {
                         %> 0 <%
                       }
                   })
                   .ClientTemplate("<# if (UnpublishedLetterCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.READING_ROOM, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "Unpublished" }) + "><#= UnpublishedLetterCount #></a ><# } else { #> 0 <# } #>")
                   .Sortable(false)
                   .Visible(Model.Mode == TaskListViewModel.TaskMode.ReviewableLetters);
                   
           })
           .NoRecordsTemplate(Model.CouldFetchTasks ? Constants.NO_RECORD_DISPLAY : "<center><em>An error occurred attempting to retrieve the task list. Please contact the helpdesk.</em></center>")
           .HtmlAttributes(new { style = "width:1200px" })
           .Render(); %>
