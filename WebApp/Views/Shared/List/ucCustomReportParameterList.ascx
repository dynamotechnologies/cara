﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>
<%Aquilent.Cara.Domain.CustomReportParameters parameters = Model.MetaData.parameters; %>
<%List<Aquilent.Cara.Domain.CustomReportDataItem> projectList = 
      parameters.DataItems.Where(x=>x.FieldType == Aquilent.Cara.Domain.CustomReportFieldType.Project).ToList();
List<Aquilent.Cara.Domain.CustomReportDataItem> commenterList = 
      parameters.DataItems.Where(x=>x.FieldType == Aquilent.Cara.Domain.CustomReportFieldType.Commenter).ToList();
List<Aquilent.Cara.Domain.CustomReportDataItem> letterList = 
      parameters.DataItems.Where(x=>x.FieldType == Aquilent.Cara.Domain.CustomReportFieldType.Letter).ToList();%>
Your Search Parameters:<br />
<table class="customReportSummaryTable">
<%if (projectList.Any()){ %><tr><td class="customReportSummaryFieldType">Project</td></tr><%} %>
<%foreach (Aquilent.Cara.Domain.CustomReportDataItem project in projectList)
  {
      
      %><tr><td class="customReportSummaryField"><%=project.FieldName%></td></tr>
      <%
      foreach (Aquilent.Cara.Domain.CustomReportValueItem value in project.Values.Where(x => !x.Not))
      {
          %><tr>
            <td class="customReportSummaryItem"><span class="customReportSummaryColor"><%=value.Name %></span></td>
            <td class="customReportSummaryButton">
                <a href="javascript:RemoveItem(<%=value.ListId%>)">
                    <img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove <%=value.Name%> filter criteria" />
                </a>
            </td>
            </tr><%
      }
      foreach (Aquilent.Cara.Domain.CustomReportValueItem value in project.Values.Where(x => x.Not))
      {
          %><tr>
            <td class="customReportSummaryItem"><span class="customReportSummaryColor"><i>Excluding </i><%=value.Name %></span></td>
            <td class="customReportSummaryButton">
                <a href="javascript:RemoveItem(<%=value.ListId%>)">
                    <img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove <%=value.Name%> filter criteria" />
                </a>
            </td>
            </tr><%
      }
  }%>

 <%if (commenterList.Any()){ %><tr><td class="customReportSummaryFieldType">Commenter</td></tr><%} %>
  <%foreach (Aquilent.Cara.Domain.CustomReportDataItem commenter in commenterList)
  {
      %><tr><td class="customReportSummaryField"><%=commenter.FieldName%></td></tr>
      <%
      foreach (Aquilent.Cara.Domain.CustomReportValueItem value in commenter.Values.Where(x => !x.Not))
      {
          %><tr>
            <td class="customReportSummaryItem"><span class="customReportSummaryColor"><%=value.Name%></span></td>
            <td class="customReportSummaryButton">
                <a href="javascript:RemoveItem(<%=value.ListId%>)">
                    <img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove <%=value.Name%> filter criteria" />
                </a>
            </td>
            </tr><%
      }
      foreach (Aquilent.Cara.Domain.CustomReportValueItem value in commenter.Values.Where(x => x.Not))
      {
          %><tr>
            <td class="customReportSummaryItem"><span class="customReportSummaryColor"><i>Excluding </i><%=value.Name %></span></td>
            <td class="customReportSummaryButton">
                <a href="javascript:RemoveItem(<%=value.ListId%>)">
                    <img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove <%=value.Name%> filter criteria" />
                </a>
            </td>
            </tr><%
      }
  } %>

<%if (letterList.Any()){ %><tr><td class="customReportSummaryFieldType">Letter</td></tr><%} %>
  <%foreach (Aquilent.Cara.Domain.CustomReportDataItem letter in letterList)
    {
      %><tr><td class="customReportSummaryField"><%=letter.FieldName%></td></tr>
      <%
      foreach (Aquilent.Cara.Domain.CustomReportValueItem value in letter.Values.Where(x => !x.Not))
      {
          %><tr>
            <td class="customReportSummaryItem"><span class="customReportSummaryColor"><%=value.Name %></span></td>
            <td class="customReportSummaryButton">
                <a href="javascript:RemoveItem(<%=value.ListId%>)">
                    <img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove <%=value.Name%> filter criteria" />
                </a>
            </td>
            </tr><%
      }
      foreach (Aquilent.Cara.Domain.CustomReportValueItem value in letter.Values.Where(x => x.Not))
      {
          %><tr>
            <td class="customReportSummaryItem"><span class="customReportSummaryColor"><i>Excluding </i><%=value.Name %></span></td>
            <td class="customReportSummaryButton">
                <a href="javascript:RemoveItem(<%=value.ListId%>)">
                    <img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove <%=value.Name%> filter criteria" />
                </a>
            </td>
            </tr><%
      }
  } %>
    <tr style="padding-bottom: 6px">
        <td class="customReportSummaryFieldType"></td>
        <td style="text-align:right">
            <input type="button" class="button" value="Clear All" onclick="ClearAllValues()" />
        </td>
    </tr>
  </table><br />
  Your Report Presentation:<br />
  <table class="customReportSummaryTable">
  <tr><td class="customReportSummaryFieldType">Columns</td></tr>
  <%
      int columnCount = 0;
      foreach (Aquilent.Cara.Domain.CustomReportColumnDataItem column in parameters.ColumnDataItems)
    {
        columnCount++;%>
            <tr>
            <td class="customReportSummaryItem">
            <span class="customReportSummaryColor">
            <%=column.ColumnName + ((columnCount > 20)? "*":string.Empty) %>
            </span></td>
        <td class="customReportSummaryButton">
            <a href="javascript:RemoveItem(<%=column.ListId%>)"><img src="<%= ResolveUrl("~/Content/Images/orangeRemove.png")%>" title="Remove Column" /></a>
            <a href="javascript:MoveUp(<%=column.ListId%>)"><img src="<%= ResolveUrl("~/Content/Images/orangeUp.png")%>" title="Move Column Up" /></a>
            <a href="javascript:MoveDown(<%=column.ListId%>)"><img src="<%= ResolveUrl("~/Content/Images/orangeDown.png")%>" title="Move Column Down" /></a>
        </td>
        </tr><%
    } %>
    <tr><td class="customReportSummaryFieldType"></td>
    <td style="text-align:right">
        <input type="button" class="button" value="Clear All" onclick="ClearAllColumns()" /></td></tr>
    </table>
    <div class="customReportSummaryTable" style="margin-top:0px">
    Columns marked with an asterisk * will not be displayed in the Preview but will be included in the exported report.
    </div>
    
