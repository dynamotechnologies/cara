﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.MemberViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.Members.OrderBy(m => m.RoleName).ThenBy(m => m.User.LastName))
            .Name(Constants.MEMBER + Constants.LIST)
            .DataKeys(keys => keys.Add(usr => usr.UserId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(usr => usr.User.FirstName);
                columns.Bound(usr => usr.User.LastName);
                columns.Bound(usr => usr.User.Username);
                columns.Bound(usr => usr.User.Email);
                columns.Bound(usr => usr.Status);
                columns.Bound(usr => usr.IsPointOfContact).Title("POC").Visible(!Model.IsPalsProject);
                columns.Bound(usr => usr.IsEarlyAttentionNewsletterSubscriber).Title("Early-Attention Newsletter").Template(usr =>
 		        {
 			        if (usr.UserId == Utilities.GetUserSession().UserId)
 			        {
 				        %><input type="checkbox"<% 
	 				        if(usr.IsEarlyAttentionNewsletterSubscriber)
 				        {
 					        %>checked="checked"<%
 				        }
 				        %> disabled="disabled" /><%
 			        }
 		        });                 
                if (Model.ShowButtons)
                {
                    columns.Bound(usr => usr.UserId).Template(usr =>
                        {
                            if (usr.Active && !usr.IsPointOfContact && Utilities.UserHasPrivilege("MTM", Constants.PHASE))
                            { %>
                                <a class="button" href="#" onclick="updateMember(<%=usr.PhaseId %>, <%=usr.UserId %>, '<%=Constants.DEACTIVATE%>')"><%=Constants.DEACTIVATE%></a >&nbsp;
                            <% } %>
                            <% if (!usr.Active && Utilities.UserHasPrivilege("MTM", Constants.PHASE))
                            { %>
                                <a class="button" href="#" onclick="updateMember(<%=usr.PhaseId %>, <%=usr.UserId %>, '<%=Constants.ACTIVATE%>')"><%=Constants.ACTIVATE%></a >&nbsp;
                            <% } %>
                            <% if (usr.CanRemove && !usr.IsPointOfContact && Utilities.UserHasPrivilege("MTM", Constants.PHASE))
                            { %>
                                <a class="button" href="#" onclick="confirmWindow('Member will be removed. Do you wish to continue', 'deleteMember(<%=usr.PhaseId %>, <%=usr.UserId %>)')"><%=Constants.REMOVE%></a >
                            <% } %>
                            <% if (!Model.IsPalsProject && !usr.IsPointOfContact && Utilities.UserHasPrivilege("MTM", Constants.PHASE))
                            { %>
                                <a class="button" href="#" onclick="confirmWindow('Member will be made the point of contact on the public comment input page. Do you wish to continue', 'makePointOfContact(<%=usr.PhaseId %>, <%=usr.UserId %>)')">Make Point Of Contact</a >
                            <% }%>
                            <% if (Model.IsPalsProject)
                            { %>
                                <a class="button" href="#" style="display:none;" onclick="confirmWindow('Member will be made the point of contact on the public comment input page. Do you wish to continue', 'makePointOfContact(<%=usr.PhaseId %>, <%=usr.UserId %>)')">Make Point Of Contact</a >
                            <% } %>
 		                     <% if (!usr.IsEarlyAttentionNewsletterSubscriber && usr.UserId == Utilities.GetUserSession().UserId)
 		                    { %>
 			                    <a class="button" href="#" onclick="changeEarlyAttentionNewsletterSubscription(<%=usr.PhaseId %>, <%=usr.UserId %>, <%=usr.IsEarlyAttentionNewsletterSubscriber.ToString().ToLower() %>)">Subscribe to Newsletter</a >
 		                    <% } %>                         
 		                     <% if (usr.IsEarlyAttentionNewsletterSubscriber && usr.UserId == Utilities.GetUserSession().UserId)
 		                    { %>
	 			                    <a class="button" href="#" onclick="changeEarlyAttentionNewsletterSubscription(<%=usr.PhaseId %>, <%=usr.UserId %>, <%=usr.IsEarlyAttentionNewsletterSubscriber.ToString().ToLower() %>)">Unsubscribe from Newsletter</a >                           
                            <% }                            
                        })
                        .Title(Constants.ACTION)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: left;" })
                        .Sortable(false);
                }
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .Footer(false)
        .HtmlAttributes(new { style = "width: 950px;" })
        .Render();
    } %>