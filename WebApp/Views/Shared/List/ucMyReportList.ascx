﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.Report.Personal.MyReport>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.OrderByDescending(r => r.DateCreated).ThenBy(r => r.Name))
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(r => r.MyReportId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(rpt => rpt.Name).Width(175);
                columns.Bound(rpt => rpt.Username).Width(125).Title("Created by");
                columns.Bound(rpt => rpt.DateCreated).Width(50);
                columns.Bound(rpt => rpt.Type).Width(75);
                if (Utilities.UserHasPrivilege("RRPT", Constants.SYSTEM))
                {
                    columns.Bound(rpt => rpt.MyReportId).Template(rpt =>
                        { %>
                        <%: Html.ActionLink(Constants.RUN, "RunMyReport",
                                new { myReportId = rpt.MyReportId}, new { @class = "button" })%>
                        <% if (rpt.UserId == Utilities.GetUserSession().UserId)
                           { %>
                        <%: Html.ActionLink(string.Format("{0} {1}", Constants.MODIFY, Constants.SEARCH), "UpdateMyReport",
                                new { myReportId = rpt.MyReportId}, new { @class = "button" })%>
                        <a class="button" href="#" onclick="confirmWindowSize('The Saved Report will be deleted. If it is shared other users will no longer be able to run it.  Do you wish to continue', 'deleteMyReport(<%=rpt.MyReportId %>)', '', 300, 85)"><%=Constants.DELETE%></a >
                        <% } %>
                      <%})
                        .Title(Constants.ACTION)
                        .Width(200)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Sortable(false);
                }
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .Footer(false)
        //.HtmlAttributes(new { style = "width: 575px;" })
        .Render();
    } %>