﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.UserListViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Framework.Security" %>

<%  if (Model.Users != null)
    {
        Html.Telerik().Grid<UserSearchResult>(Model.Users.OrderBy(usr => usr.UserName))
            .Name(Constants.LIST)
            .Columns(columns =>
            {
                columns.Bound(usr => usr.UserName);
                columns.Bound(usr => usr.LastName);
                columns.Bound(usr => usr.FirstName);
                columns.Bound(usr => usr.Email);
                columns.Bound(usr => usr.UnitNameText).Width(400).Title("Roles").Sortable(false)
                    .Template(usr => {%><%= usr.RoleUnitList %><% })
                    .ClientTemplate("<#= RoleUnitList #>");
                if (Utilities.UserHasPrivilege("MUC", Constants.SYSTEM))
                {
                    columns.Bound(usr => usr.UserId)
                        .Template(usr =>
                        {%>
                            <%: Html.ActionLink(Constants.MANAGE + " " + Constants.ROLE.Pluralize(), "Roles", "User", new { id = usr.UserId }, new { @class = "button" }) %>
                        <%}).ClientTemplate("<a class=\"button\" href=\"/Admin/User/Roles/<#= UserId #>\">" + Constants.MANAGE + " " + Constants.ROLE.Pluralize() + "</a >")
                        .Title(Constants.ACTION)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Sortable(false);
                }
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .DataBinding(dataBinding => dataBinding.Ajax().Select("UserListBinding", Constants.USER))
            .EnableCustomBinding(true)
            .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]))
            .Sortable()
            .HtmlAttributes(new { style = "width:1050px;" })
            .Render();
    } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.userlist.js"));
%>