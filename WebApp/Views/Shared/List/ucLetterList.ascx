﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.Letter>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.OrderBy(letter => letter.LetterSequence))
                .Name(Constants.LIST)
                .DataKeys(keys => keys.Add(letter => letter.LetterId).RouteKey("id"))
                .Columns(columns =>
                {
                    columns.Bound(letter => letter.LetterSequence).Template(letter =>
                    {%><a href="<%= Url.Action(Constants.CODING, Constants.LETTER, new { id = letter.LetterId }) %>"><%= letter.LetterSequence%></a ><%});
                    columns.Bound(letter => letter.Sender.FullName);
                    //columns.Bound(letter => letter.Sender.Organization.OrganizationTypeName).Width(100);
                    columns.Bound(letter => letter.Sender.Organization.Name).Width(175);
                    columns.Bound(letter => letter.DateSubmitted).Format("{0:MM/dd/yyyy}");
                    columns.Bound(letter => letter.LetterTypeName);
                    columns.Bound(letter => letter.LetterStatusName);
                    columns.Bound(letter => letter.Size);
                })
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .Footer(false)
                .HtmlAttributes(new { style = "width: 875px;" })
                .Render();
    } %>