﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ChangeLogListViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.FormManagementChangeLog>(Model.ChangeLogs)
                .Name(Constants.CHANGE_LOG + Constants.LIST)
                .Columns(columns =>
                {
                    columns.Bound(cl => cl.Date).Width(160).Title("Log Date");
                    columns.Bound(cl => cl.Description);
                    columns.Bound(cl => cl.Username).Title("Updated by");
                })
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .DataBinding(dataBinding => dataBinding.Ajax().Select("ChangeLogListBinding", Constants.FORM))
                .EnableCustomBinding(true)
                .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
                .HtmlAttributes(new { style = "width: 775px;" })
                .Render();
    } %>