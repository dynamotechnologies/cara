﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.Project>>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model)
                .Name(Constants.LIST)
                .DataKeys(keys => keys.Add(proj => proj.ProjectId).RouteKey("id"))
                .Columns(columns =>
                {
                    columns.Bound(proj => proj.ProjectId).Template(proj =>
                    {%><%: Html.RadioButton("rbProjectId", proj.ProjectId)%><%})
                        .Width(50)
                        .Title(Constants.SELECT);
                    columns.Bound(proj => proj.ProjectNameNumber).Template(proj =>
                        {%><a href="<%= Url.Action(Constants.DETAILS, Constants.PROJECT, new { projectId = proj.ProjectId }) %>"><%= string.Format("{0}", proj.Name)%></a ><br />(<%= proj.ProjectNumber%>)<%})
                        .Width(250);
                    columns.Bound(proj => proj.ProjectId).Template(proj =>
                    {%><%= Html.Telerik().DropDownList()
                            .Name(string.Format("ddlPhase_{0}", proj.ProjectId))
                            .BindTo(Utilities.ToSelectList(proj.Phases.OrderBy(p => p.PhaseTypeName).ThenBy(p => p.Name).ToLookup(p => p.PhaseId.ToString(), p => p.PhaseTypeDesc), string.Empty))
                            .HtmlAttributes(new { style = "width:300px" })%>
                    <%}).Title(Utilities.GetResourceString(Constants.PHASE));
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .HtmlAttributes(new { style = "width: 600px;" })
               .Footer(false)
               .Render();
    } %>