﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DataMartUserViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Framework.Security.User>(Model.Users)
            .Name(Constants.USER + Constants.LIST)
            .DataKeys(keys => keys.Add(usr => usr.UserId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(usr => usr.UserId).Template(usr =>
                {%><%: Html.RadioButton("rbUsername", usr.Username, (usr.Username == Model.Username))%><%})
                    .Width(75)
                    .ClientTemplate("<input id='rbUsername' name='rbUsername' type='radio' value='<#= Username #>' <#= Username == '" + Model.Username + "' ? 'checked' : '' #> />")
                    .Title(Constants.SELECT);
                columns.Bound(usr => usr.FirstName);
                columns.Bound(usr => usr.LastName);
                columns.Bound(usr => usr.Username);
                columns.Bound(usr => usr.Email);
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .DataBinding(dataBinding => dataBinding.Ajax().Select("DataMartUserListBinding", Constants.PROJECT))
            .EnableCustomBinding(true)
            .Pageable(settings => settings.PageSize(Constants.DEFAULT_PAGE_SIZE).Total((int)ViewData["total"]).PageTo((int)ViewData["pageTo"]))
            .Render();
    } %>