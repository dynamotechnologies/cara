﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>


<% if (Model != null)
   {
       LetterCodingViewModel lcvModel = new LetterCodingViewModel();

       int j = 0;
       %>
       <table  style="border-style: outset; border-width: thin; border-color: inherit; width:240px; ">
        <%
        foreach (var termGroup in Model.TermGroups)
        { %>
         <tr><td colspan="2"><b><%=termGroup.Key%>:</b>
         </td></tr>
        <%
        foreach (Aquilent.Cara.Domain.LetterTermConfirmation term in termGroup)
        {
            
        %>
              <tr><td align="right" style = "width:80px;"  class="text"><%=term.Term.Name%>:</td>
              <td valign="top" align="left"  class="text">
              <%:Html.Telerik().DropDownListFor(model => term.StatusId)
                      .Name("statusId_" + j)
                      .BindTo(lcvModel.LookupLetterTermConfirmationStatus)
                      .HtmlAttributes(new { style = "width:120px;" })%>
               
               <%: Html.Hidden("termId_"+j, term.TermId) %>
               <%: Html.Hidden("letterId", term.LetterId) %>
               <%: Html.Hidden("termListId_"+j, term.Term.TermListId)%>
           
                </td>
               </tr>
               <tr>
               <td align="right">
              <label id="Annotation"  class="text">Annotation:</label>
               </td>
               <td  class="text">
               <div id="show_<%=j%>">
               <input type="button" name="btn_Annotation_show_<%=j%>" value="Show" onclick="javascript:showControlsOnLetter(<%=j%>,'show');" style="width:40px;" class="button"/>
               </div>
               <div class="invisible" id="hide_<%=j%>">
               <input type="button" name="btn_Annotation_hide_<%=j%>" value="Hide" onclick="javascript:showControlsOnLetter(<%=j%>,'hide');" style="width:40px;" class="button"/>
               </div>
               </td>
               </tr>
               <tr><td colspan="2" align="right"  class="text">
               <div id="annotationTextarea_<%=j%>" class="invisible">
                     <%: Html.TextArea("annotation_" + j, term.Annotation, 6, 50, new { style = "width:200px; readonly" }) %>
               </div>
               <hr />
               </td></tr>
               
          <%j = j + 1;
        } //End of foreach inner%>
          <% } //End of foreach outer%>
        <%:Html.Hidden("ltcListCount",j) %>

         <%if (Utilities.UserHasPrivilege("CODE", Constants.PHASE) && j>0)%>
         <%{ %>
        <tr><td colspan="2" align="left"><input type="button" value="Save" name="save" class="button" onclick="javascript:updateLetterTerm();" /></td></tr>
        <% } %>
      </table>
 
<% } %>