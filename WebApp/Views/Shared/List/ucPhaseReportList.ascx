﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseReportViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.Reports.OrderBy(x => x.Name))
            .Name(Constants.REPORT + Constants.LIST)
            .DataKeys(keys => keys.Add(rpt => rpt.ReportId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(rpt => rpt.Name);
                if (Utilities.GetUserSession().HasPrivilege("RRPT"))
                {
                    columns.Bound(rpt => rpt.ReportId).Template(rpt =>
                        { %>
                        <% if (!rpt.HasReportOptions)
                           {
                               if (Model.Locked && rpt.ArtifactTypeId.HasValue)
                               { %>
                                   <a class="button" target="_blank" href="<%= Url.Action(Constants.VIEW + Constants.REPORT, new { id = rpt.ReportId, artifactTypeId = rpt.ArtifactTypeId }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                                <% }
                                else if (rpt.ReportId == 2) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 4) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.EARLYACTION_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }

                               else if (rpt.ReportId == 5) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTERORGTYPE_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 7) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.RESPONSESTATUS_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 8) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.MAILINGLIST_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 9) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.UNTIMELYCOMMENTS_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 10) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.TEAMMEMBER_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 12) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTSRESPONSESBYLETTER_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 13) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.CONCERNRESPONSEBYCOMMENTER_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else if (rpt.ReportId == 14) 
                               { %>
                                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTRESPONSENOCONCERN_REPORT, new {id=rpt.ReportId}) %>"><%=Constants.RUN%></a >
                                 <% }
                               else
                               { %>
                                 <a class="button" target="_blank" href="<%= Url.Action(Constants.RUN, new { id = rpt.ReportId }) %>"><%=Constants.RUN%></a >
                                <% }
                     
                           } %>
                        <% })
                    .Title(Constants.ACTION)
                    .HeaderHtmlAttributes(new { style = "text-align: center;" })
                    .HtmlAttributes(new { style = "text-align: center;" })
                    .Width(150);
                }
            })
            .DetailView(reportOptions => reportOptions.Template(rpt =>
            { %>
                <% Html.Telerik().Grid(rpt.ReportOptions)
                        .Name("ReportOptions_" + rpt.ReportId)
                        .Columns(columns =>
                        {
                            columns.Bound(ro => ro.Name).Title(" ");
                            if (Utilities.GetUserSession().HasPrivilege("RRPT"))
                            {
                                columns.Bound(ro => ro.Value).Template(ro =>
                                    { %>
                                    <% if (Model.Locked && ro.IsArtifact.HasValue && ro.IsArtifact.Value)
                                       { %>
                                        <a class="button" href="<%= Url.Action(Constants.VIEW, new { id = ro.ReportId, option = ro.Value }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                                    <% }
                                       else if (ro.ReportId == 13) 
                                       { %>
                                        <a class="button" target="_self" href="<%= Url.Action(Constants.CONCERNRESPONSEBYCOMMENTER_REPORT, new { id = ro.ReportId, option = ro.Value }) %>"><%=Constants.RUN%></a >
                                    <% }
                                       else if (rpt.ReportId == 6) 
                                       { %>
                                        <a class="button" target="_self" href="<%= Url.Action(Constants.RESPONSETOCOMMENT_REPORT, new {id=rpt.ReportId, option = ro.Value}) %>"><%=Constants.RUN%></a >
                                    <% }
                                       else 
                                       { %>
                                        <a class="button" target="_blank" href="<%= Url.Action(Constants.RUN, new { id = ro.ReportId, option = ro.Value }) %>"><%=Constants.RUN%></a >
                                    <% } %>
                                <% })
                                .Title(" ")
                                .HeaderHtmlAttributes(new { style = "text-align: center;" })
                                .HtmlAttributes(new { style = "text-align: center;" })
                                .Width(Model.ActionCellWidth);
                            }
                        })
                        .ClientEvents(events => events.OnLoad("hideHeader"))
                        .Footer(false)
                        .HtmlAttributes(new { style = "width: 451px;" })
                        .Render(); %>
                <% }))
            .ClientEvents(events => events.OnLoad("hideExpandIcons"))
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .RowAction(row =>
                {
                    row.DetailRow.Expanded = row.DataItem.HasReportOptions;
                })
            .Footer(false)
            .HtmlAttributes(new { style = "width: 502px;" })
            .Render();
    } %>