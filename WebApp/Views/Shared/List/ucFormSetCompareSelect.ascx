﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.FormSetListViewModel>" %>

<%  if (Model != null)
    { %>
    <div class="text filterArea" style="vertical-align:middle;padding: 10px 0px 10px 0px;">
        <table>
            <tr>
                <td><label for="SourceFormSetId"><%: Constants.RESORT %> <%: Utilities.GetResourceString(Constants.FORM_SET) %>:</label></td>
                <td>
                <%: Html.Telerik().DropDownList()
                    .Name("SourceFormSetId")
                    .BindTo(Model.LookupFormSetAndUnique)
                    .HtmlAttributes(new { style = "width:200px;" })%>
                </td>
                <td><label for="CompareToFormSetId">against <b><%: Utilities.GetResourceString("MasterForm") %></b> of <%: Utilities.GetResourceString(Constants.FORM_SET) %>:</label></td>
                <td>
                <%: Html.Telerik().DropDownList()
                    .Name("CompareToFormSetId")
                    .BindTo(Model.LookupFormSet)
                    .HtmlAttributes(new { style = "width:200px;" })%>
                </td>
                <td>
                    <a class="button" id="btnResort" href="#" onclick="confirmWindow('You are about to re-sort the selected Form Set, which could take several minutes to complete. Do you want to continue', 'resortFormSet()');return false;"><%: Constants.RESORT %></a>
                </td>
            </tr>
        </table>
    </div>
  <% } %>