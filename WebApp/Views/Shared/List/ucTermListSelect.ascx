﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.TermList>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model)
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(termList => termList.TermListId).RouteKey("id"))
            .Columns(columns =>
            {
                if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
                {
                    columns.Bound(termList => termList.TermListId).Template(termList =>
                        {%><input type="checkbox" name="chkTermListId" value="<%=termList.TermListId %>" <%=(termList.DefaultOn ? "checked" : "")%> onclick="return onClickCheckbox(<%: termList.Required ? "true" : "false" %>);" /><%})
                            .Width(50)
                            .Title(Constants.SELECT);
                }
                columns.Bound(termList => termList.Name).Width(200);
                columns.Bound(termList => termList.Description).Width(350);
                columns.Bound(termList => termList.TermListId).Template(termList =>
                    { %><a id="btnView" class="button" href="#" onclick="javascript:openWindow('ModalWindow', '/Admin/TermList/View/<%=termList.TermListId%>')"><%=Constants.VIEW%></a ><%} )
                        .Title(string.Format("{0} of {1}", Constants.LIST, Utilities.GetResourceString(Constants.TERM).Pluralize()))
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                    .Width(50)
                    .Sortable(false);
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Footer(false)
            .Render();
    }

    Html.Telerik().Window()
        .Name("ModalWindow")
        .Title(Utilities.GetResourceString(Constants.TERM).Pluralize())
        .Draggable(true)
        .Resizable(resizing => resizing
                        .Enabled(true)
                        .MinHeight(250)
                        .MinWidth(250)
                        .MaxHeight(800)
                        .MaxWidth(1000)
                    )
        .Modal(true)
        .Buttons(b => b.Maximize().Close())
        .Width(400)
        .Height(500)
        .Visible(false)
        .Render();
 %>

<!-- This scripts are necessary for the terms to be displayed in the modal window -->
<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")); %>