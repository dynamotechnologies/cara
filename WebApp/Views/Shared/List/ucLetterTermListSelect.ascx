﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>

<script type="text/javascript">
<!--
    function save() {
        // save the selected term lists into session, refresh the letter text
        saveTermList(<%=Model.LetterId %>, $("#chkTermListId:checked").val());
        
        // close the window
        closeWindow("WindowTermList");
    }
-->
</script>

<%  if (Model.TermLists != null && Model.TermLists.Count > 0)
    {
        Html.Telerik().Grid(Model.TermLists)
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(termList => termList.TermListId).RouteKey("id"))
            .Columns(columns =>
            {
                if (Utilities.UserHasPrivilege("CODE", Constants.PHASE))
                {
                    columns.Bound(termList => termList.TermListId).Template(termList =>
                        {%><input type="checkbox" id="chkTermListId" value="<%=termList.TermListId %>" <%=(Utilities.TermListContains(termList.TermListId) ? "checked" : string.Empty)%> /><%})
                            .Width(50)
                            .Title(Constants.SELECT);
                }
                columns.Bound(termList => termList.Name).Width(200);
                columns.Bound(termList => termList.Description).Width(350);
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Footer(false)
            .Render();
    }
    else
    { %>
        <p>No additional <%=Utilities.GetResourceString(Constants.TERM_LIST) %> was found.</p>
    <% } %>

<% if (Utilities.UserHasPrivilege("CODE", Constants.PHASE))
   { %>
    <br />
    <% if (Model.TermLists != null && Model.TermLists.Count > 0)
       { %>
    <input type="button" value="<%=Constants.SAVE %>" name="save" class="button" onclick="save()" />
    <% } %>
    <input type="button" value="<%=Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('WindowTermList')" />
<% } %>
