﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.CodingTemplate>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model)
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(template => template.CodingTemplateId).RouteKey("id"))
            .Columns(columns =>
            {
                if (Utilities.UserHasPrivilege("MPRJ", Constants.SYSTEM))
                {
                    columns.Bound(template => template.CodingTemplateId).Template(template =>
                    {%><%: Html.RadioButton("rbCodingTemplateId", template.CodingTemplateId)%><%})
                        .Width(50)
                        .Title(Constants.SELECT);
                }
                columns.Bound(template => template.Name).Title("Template Name");
                columns.Bound(template => template.CodingTemplateId).Template(template =>
                    {%><a class="button" onclick="javascript:openWindow('ModalWindow', '/Admin/Code/View/<%=template.CodingTemplateId%>')">View Coding Structure</a ><%})
                    .Title(Constants.ACTION)
                    .HeaderHtmlAttributes(new { style = "text-align: center;" })
                    .HtmlAttributes(new { style = "text-align: center;" })
                    .Width(215);
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Footer(false)
            .HtmlAttributes(new { style = "width: 525px;" })
            .Render();

        Html.Telerik().Window()
            .Name("ModalWindow")
            .Title(Utilities.GetResourceString("CodingStructure"))
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(true)
                            .MinHeight(250)
                            .MinWidth(250)
                            .MaxWidth(1000)
                        )
            .Modal(true)
            .Buttons(b => b.Maximize().Close())
            .Width(600)
            .Height(600)
            .Visible(false)
            .Render();
    }
%>

<!-- This scripts are necessary for the treeview to be displayed in the modal window -->
<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.treeview.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")); %>