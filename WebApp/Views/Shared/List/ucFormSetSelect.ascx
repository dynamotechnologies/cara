﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.FormSetSelectViewModel>" %>

<script type="text/javascript">
<!--
    // close the window and refresh the form set list on parent
    function update() {
        var formSetId = $("input[name=rbFormSetId]:checked").val();
        var letterId = $("#LetterId").val();

        if (letterId != null & letterId != "" && formSetId != null & formSetId != "") {
            closeWindow("WindowSelectFormSet");

            updateLetterFormSet(letterId, formSetId);
        }
        else {
            alertWindow("Please select a Form Set.");
        }

        return false;
    }
-->
</script>

<div>
    <div class="fieldLabel">Please select a <%:Utilities.GetResourceString(Constants.FORM_SET) %> to send the <%: Constants.LETTER %> to:</div>
    <br /><br />
    <%  if (Model.FormSets != null)
        {
            Html.Telerik().Grid(Model.FormSets.OrderBy(x => x.Name))
                .Name(Constants.FORM_SET + Constants.LIST)
                .Columns(columns =>
                {
                    if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
                    {
                        columns.Bound(fs => fs.FormSetId).Template(fs =>
                            {%><input type="radio" name="rbFormSetId" value="<%=fs.FormSetId %>" /><%})
                                .Title(Constants.SELECT)
                                .Width(50)
                                .Sortable(false);
                    }
                    columns.Bound(fs => fs.Name).Title(Constants.NAME);
                })
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .Footer(false)
                .Render();
        } %>
    <br />
    <% if (Model.FormSets != null && Model.FormSets.Count > 0)
        { %>
        <input type="button" value="<%=Constants.UPDATE %>" name="save" class="button" onclick="update()" />
    <% } %>
    <input type="button" value="<%=Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('WindowSelectFormSet')" />    
    <%: Html.HiddenFor(model => model.LetterId) %>
</div>