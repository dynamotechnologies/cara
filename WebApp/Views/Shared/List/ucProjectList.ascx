﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProjectListViewModel>" %>
<% Html.RenderPartial("ucProjectListHeader"); %>
<% Html.Telerik().Grid<Aquilent.Cara.Domain.ProjectSearchResult>(Model.Projects)
                .Name(Constants.PROJECT + Constants.LIST)
                .Columns(columns =>
                {
                    columns.Bound(proj => proj.ProjectName)
                        .Template(proj => {%>
                            <%= Html.ActionLink(proj.ProjectName, Constants.DETAILS, Constants.PROJECT, new { projectId = proj.ProjectId }, null) %><br />(<%=proj.ProjectNumber%>)
                            <%}).ClientTemplate("<a href=" + Url.Action(Constants.DETAILS, Constants.PROJECT, new { projectId = "<#= ProjectId #>" }) + ">" + string.Format("{0}", "<#= ProjectName #>") + "</a ><br />" + "<#= ProjectNumber #>")
                              .Width(150).Title("Project Name\n(ID)").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(proj => proj.UnitName).Width(100);
                    columns.Bound(proj => proj.AnalysisTypeName).Width(50).Title("Analysis\nType").HeaderHtmlAttributes(new { style=Constants.GRID_HEADER_STYLE });
                    columns.Bound(proj => proj.ProjectStatusName).Width(110).Title("PALS Project\nStatus").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(proj => proj.CommentStart).Width(175)
                        .Template(proj => {%>
                            <%=proj.CommentStartShortDate%> - <%=proj.CommentEndShortDate %><br /><%= Html.ActionLink(proj.PhaseTypeName, Constants.DETAILS, Constants.PHASE, new { projectId = proj.ProjectId, phaseId = proj.PhaseId }, null)%><br /><%= proj.PhaseName%>
                            <%}).ClientTemplate("<#= CommentStartShortDate #> - <#= CommentEndShortDate #><br /><a href=" + Url.Action(Constants.DETAILS, Constants.PHASE, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>" }) + ">" + string.Format("{0}", "<#= PhaseTypeName #>") + "</a><br /><#= PhaseName #>")
                            .Title("Comment Period").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(proj => proj.PhaseNewLetterCount).Template(proj =>
                        {%><% if (proj.PhaseNewLetterCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="New" }) %>"><%= proj.PhaseNewLetterCount%></a ><%
                            } else {%><%=proj.PhaseNewLetterCount%><%}})
                        .ClientTemplate("<# if (PhaseNewLetterCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "New" }) + "><#= PhaseNewLetterCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("New\nLetters").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(proj => proj.PhaseEarlyActionLetterCount).Template(proj =>
                        {%><% if (proj.PhaseEarlyActionLetterCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="RequiresEarlyAttention" }) %>"><%= proj.PhaseEarlyActionLetterCount%></a ><%
                            } else {%><%=proj.PhaseEarlyActionLetterCount%><%}})
                        .ClientTemplate("<# if (PhaseEarlyActionLetterCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "RequiresEarlyAttention" }) + "><#= PhaseEarlyActionLetterCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("Early\nAttention").Width(105).HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(proj => proj.PhaseCodingInProgressCount).Template(proj =>
                        {%><% if (proj.PhaseCodingInProgressCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="CodingInProgress" }) %>"><%= proj.PhaseCodingInProgressCount%></a ><%
                            } else {%><%=proj.PhaseCodingInProgressCount%><%}})
                        .ClientTemplate("<# if (PhaseCodingInProgressCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "CodingInProgress" }) + "><#= PhaseCodingInProgressCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("Coding In\nProgress").Width(90).HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(proj => proj.PhaseCodingCompleteCount).Template(proj =>
                        {%><% if (proj.PhaseCodingCompleteCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="CodingComplete" }) %>"><%= proj.PhaseCodingCompleteCount%></a ><%
                            } else {%><%=proj.PhaseCodingCompleteCount%><%}})
                        .ClientTemplate("<# if (PhaseCodingCompleteCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "CodingComplete" }) + "><#= PhaseCodingCompleteCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("Coding\nComplete").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(proj => proj.PhaseUniqueLettersCount).Template(proj =>
                        {%><% if (proj.PhaseUniqueLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="Unique" }) %>"><%= proj.PhaseUniqueLettersCount%></a ><%
                            } else {%><%=proj.PhaseUniqueLettersCount%><%}})
                        .ClientTemplate("<# if (PhaseUniqueLettersCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "Unique" }) + "><#= PhaseUniqueLettersCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("Unique").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    
                    columns.Bound(proj => proj.PhaseDuplicateLettersCount).Template(proj =>
                        {%><% if (proj.PhaseDuplicateLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="Duplicate" }) %>"><%= proj.PhaseDuplicateLettersCount%></a ><%
                            } else {%><%=proj.PhaseDuplicateLettersCount%><%}})
                        .ClientTemplate("<# if (PhaseDuplicateLettersCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "Duplicate" }) + "><#= PhaseDuplicateLettersCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("Duplicate").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(proj => proj.PhaseFormLettersCount).Template(proj =>
                        {%><% if (proj.PhaseFormLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="Forms" }) %>"><%= proj.PhaseFormLettersCount%></a ><%
                            } else {%><%=proj.PhaseFormLettersCount%><%}})
                        .ClientTemplate("<# if (PhaseFormLettersCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "Form" }) + "><#= PhaseFormLettersCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("All\nForms").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(proj => proj.PhaseTotalLettersCount).Template(proj =>
                        {%><% if (proj.PhaseTotalLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = proj.ProjectId, phaseId = proj.PhaseId, filter="Total" }) %>"><%= proj.PhaseTotalLettersCount%></a ><%
                            } else {%><%=proj.PhaseTotalLettersCount%><%}})
                        .ClientTemplate("<# if (PhaseTotalLettersCount > 0) { #><a href=" + Url.Action(Constants.LIST, Constants.LETTER, new { projectId = "<#= ProjectId #>", phaseId = "<#= PhaseId #>", filter = "Total" }) + "><#= PhaseTotalLettersCount #></a ><# } else { #> 0 <# } #>")
                        .Width(20).Title("Total").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                })
                .ClientEvents(events =>
                {
                    events.OnLoad("setColumnHeaderGroups");
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("ProjectListBinding", Constants.PROJECT))
               .EnableCustomBinding(true)
               .HtmlAttributes(new { style = "width:1270px" })
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               .Sortable(sorting => sorting.OrderBy(Model.SortDescriptor))
               .Render(); %>