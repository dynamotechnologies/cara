﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.StandardReportViewModel>" %>
<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.Reports.OrderBy(x => x.Name))
            .Name(Constants.REPORT + Constants.LIST)
            .DataKeys(keys => keys.Add(rpt => rpt.ReportId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(rpt => rpt.ReportId).Template(rpt =>
                    {
                        if (!rpt.HasReportOptions)
                        {
                            if (Utilities.GetUserSession().HasPrivilege("RRPT"))
                            {%>
                                <%=Html.RadioButton("ReportSelect", rpt.ReportId)%>
                            <%} %>
                            <%=Html.Label(rpt.Name)%>
                      <%}
                        else
                        {%>
                            <%=Html.Label(rpt.Name)%>
                        <%}%>
                    <%});
            })
            .DetailView(reportOptions => reportOptions.Template(rpt =>
            { %>
                <% Html.Telerik().Grid(rpt.ReportOptions)
                        .Name("ReportOptions_" + rpt.ReportId)
                        .Columns(columns =>
                        {
                            if (Utilities.GetUserSession().HasPrivilege("RRPT"))
                            {
                                columns.Bound(ro => ro.Value).Template(ro =>
                                    { %>
                                    <%=Html.RadioButton("ReportSelect", ro.ReportId) %>
                                    <%=Html.Label(ro.Name) %>
                                <% })
                                .Title(" ")
                                .Width(Model.ActionCellWidth);
                            }
                        })
                        .ClientEvents(events => events.OnLoad("hideHeader"))
                        .Footer(false)
                        .HtmlAttributes(new { style = "width: 451px;" })
                        .Render(); %>
                <% }))
            .ClientEvents(events => events.OnLoad("hideExpandIcons"))
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .RowAction(row =>
                {
                    row.DetailRow.Expanded = row.DataItem.HasReportOptions;
                })
            .Footer(false)
            .HtmlAttributes(new { style = "width: 502px;" })
            .Render();
    } %>

