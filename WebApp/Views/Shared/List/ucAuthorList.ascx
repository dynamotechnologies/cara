﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.AuthorListViewModel>" %>

<%  if (Model != null && Model.Authors != null)
    {
        Html.Telerik().Grid(Model.Authors.OrderByDescending(au => au.Sender).ThenBy(au => au.FullName))
            .Name(Constants.AUTHOR + Constants.LIST)
            .DataKeys(keys => keys.Add(au => au.AuthorId).RouteKey("id"))
            .Columns(columns =>
            {
                columns.Bound(au => au.FullName)
                    .Template(au =>
                    {%><%= au.FullName%>&nbsp;<%=(au.Sender ? "(Sender)" : string.Empty)%><%});
                columns.Bound(au => au.Organization.Name);
                columns.Bound(au => au.Title).Width((Constants.LIST.Equals(Model.Mode) ? 125 : 200));
                if (Constants.LIST.Equals(Model.Mode))
                {
                    columns.Bound(au => au.FullAddress).Title("Address");
                    columns.Bound(au => au.Email);
                    columns.Bound(au => au.Phone).Width(100);
                }
                else if (Constants.EDIT.Equals(Model.Mode))
                {
                    columns.Bound(au => au.AuthorId).Template(au =>
                        {
                            if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
                            { %>
                                <a class="button" href="<%= Url.Action(Constants.EDIT, Constants.AUTHOR, new { letterId = au.LetterId, id = au.AuthorId }) %>"><%=Constants.EDIT%></a >
                                <%
                                if (!au.Sender)
                                { %>
                                    <!--<a class="button" href="<%= Url.Action(Constants.DELETE, Constants.AUTHOR, new { letterId = au.LetterId, authorId = au.AuthorId }) %>"><%=Constants.DELETE%></a >-->
                                    <a class="button" href="javascript:void(0)" onclick = "return confirmWindow('Author will be deleted. Do you wish to continue?', 'deleteAuthor(<%=au.Letter.Phase.ProjectId %>, <%=au.Letter.PhaseId %>, <%=au.LetterId %>, <%=au.AuthorId %>)')"><%=Constants.DELETE%></a >
                             <% }
                            }
                        })
                    .Title(Constants.ACTION)
                    .HeaderHtmlAttributes(new { style = "text-align: center;" })
                    .HtmlAttributes(new { style = "text-align: center;" });
                }
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .TableHtmlAttributes(new { style = Constants.LIST.Equals(Model.Mode) ? "table-layout:fixed" : string.Empty })
        .Footer(false)
        .Render();
    } %>