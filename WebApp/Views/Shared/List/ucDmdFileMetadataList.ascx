﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PublicLetterViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.DmdFileMetaDataList.OrderBy(x => x.Filename))
            .Name(Constants.DOCUMENT + Constants.LIST)
            .Columns(columns =>
            {
                columns.Bound(doc => doc.Filename).Template(doc =>
                    {%><%: Html.ActionLink(Model.DmdFilenameForDisplay(doc.Filename), string.Format("{0}{1}", Constants.DOWNLOAD, "CommentFile"), new { dmdId = doc.DmdId }, new { onclick = "ucDmdFileMetaDataListOpenWindow(this.href);return false;" })%><%})
                        .Title(Constants.DOCUMENT);
                columns.Bound(doc => doc.FileSize).Title("Size (bytes)")
                    .Width(60).HtmlAttributes(new { style = "text-align: right;" });
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .Footer(false)
        .HtmlAttributes(new { style = "width: 700px;" })
        .Render();
         %>
        <br />        
        <%=Html.ActionLink(Constants.DOWNLOAD + " All", Constants.DOWNLOAD + "CommentFiles",
                                Constants.PUBLIC, new { letterId = Model.LetterId, project = Model.ProjectNumber }, new { @class = "button" })%>
<% } %>
<script type="text/javascript">
    function ucDmdFileMetaDataListOpenWindow(url) {

        var pdfWindow = window.open(url, 'ucDmdFileMetaDataListPrint', 'toolbar=1,scrollbars=1,location=1,status=1,menubar=1, width=' + screen.width + ',height=' + screen.height + ',resizable=1');
        if (pdfWindow != null) {
            
            pdfWindow.document.onreadystatechange = function () {
                if (pdfWindow.document.readyState == "interactive") {
                    if (pdfWindow.document.title.indexOf("Error") != -1) {
                        pdfWindow.close();
                        alert("The document is not available. Please try again later.");
                    } else {
                        pdfWindow.focus();
                    }
                }
            };
        }
        //alert(url);
        return false;
    }
</script>
