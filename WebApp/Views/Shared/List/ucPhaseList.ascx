﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.PhaseSearchResult>>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.PhaseSearchResult>(Model)
                .Name(Constants.LIST)
                .Columns(columns =>
                {
                    columns.Bound(phase => phase.PhaseTypeName).Template(phase =>
                        {%><a href="<%= Url.Action(Constants.DETAILS, Constants.PHASE, new { phaseId = phase.PhaseId }) %>"><%= phase.PhaseTypeName%></a >&nbsp;<%= (phase.Current > 0 ? "(Current)" : string.Empty) %>
                        <%})
                        .Width(75)
                        .Title(Constants.COMMENT_PERIOD);
                    columns.Bound(phase => phase.Name);
                    columns.Bound(phase => phase.CommentStart).Template(phase =>
                    {%><%=phase.CommentStart.ToShortDateString()%>&nbsp;-<br /><% if (!phase.AlwaysOpen)
                                                                                  { %><%= phase.CommentEnd.ToShortDateString()%><% }
                                                                                  else
                                                                                  { %> N/A <% } %><%}).Title("Start/End Date").Width(50);
                    columns.Bound(phase => phase.NewLetterCount).Template(phase =>
                        {%><% if (phase.NewLetterCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="New" }) %>"><%= phase.NewLetterCount%></a ><%
                            } else {%><%=phase.NewLetterCount%><%}})
                      .Width(20).Title("New\nLetters").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(phase => phase.EarlyActionLetterCount).Template(phase =>
                        {%><% if (phase.EarlyActionLetterCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="RequiresEarlyAttention" }) %>"><%=phase.EarlyActionLetterCount%></a ><%
                            } else {%><%=phase.EarlyActionLetterCount%><%}})
                      .Width(20).Title("Early\nAttention").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    
                    columns.Bound(phase => phase.CodingInProgressCount).Template(phase =>
                        {%><% if (phase.CodingInProgressCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="CodingInProgress" }) %>"><%= phase.CodingInProgressCount%></a ><%
                            } else {%><%=phase.CodingInProgressCount%><%}})
                      .Width(20).Title("Coding\nIn\nProgress").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(phase => phase.CodingCompleteCount).Template(phase =>
                        {%><% if (phase.CodingCompleteCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="CodingComplete" }) %>"><%= phase.CodingCompleteCount%></a ><%
                            } else {%><%=phase.CodingCompleteCount%><%}})
                      .Width(20).Title("Coding\nComplete").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(phase => phase.UniqueLettersCount).Template(phase =>
                        {%><% if (phase.UniqueLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="Unique" }) %>"><%= phase.UniqueLettersCount%></a ><%
                            } else {%><%=phase.UniqueLettersCount%><%}})
                      .Width(20).Title("Unique\nLetters").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });   
                    
                    columns.Bound(phase => phase.DuplicateLettersCount).Template(phase =>
                        {%><% if (phase.DuplicateLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="Duplicate" }) %>"><%= phase.DuplicateLettersCount%></a ><%
                            } else {%><%=phase.DuplicateLettersCount%><%}})
                      .Width(20).Title("Duplicate\nLetters").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });        
           
                    columns.Bound(phase => phase.FormLettersCount).Template(phase =>
                        {%><% if (phase.FormLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="Form" }) %>"><%= phase.FormLettersCount%></a ><%
                            } else {%><%=phase.FormLettersCount%><%}})
                      .Width(20).Title("Form\nLetters").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });

                    columns.Bound(phase => phase.TotalLettersCount).Template(phase =>
                        {%><% if (phase.TotalLettersCount > 0) { %>
                            <a href="<%= Url.Action(Constants.LIST, Constants.LETTER, new { projectId = phase.ProjectId, phaseId = phase.PhaseId, filter="Total" }) %>"><%= phase.TotalLettersCount%></a ><%
                            } else {%><%=phase.TotalLettersCount%><%}})
                      .Width(20).Title("Total\nLetters").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    
                    columns.Bound(phase => phase.StatusName).Title("Comment\nAnalysis\nStatus").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(phase => phase.AccessStatusName).Title("Viewing\nStatus").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                })                
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .HtmlAttributes(new { style = "width: 970px;" })
               .Footer(false)
               .Render();
    } %>