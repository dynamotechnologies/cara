﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ObjectionDocumentViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.Documents)
            .Name(Constants.DOCUMENT + Constants.LIST)
            .Columns(columns =>
            {
                columns.Bound(doc => doc.Name).Template(doc =>
                    {%><a href="<%= Url.Action(Constants.DOWNLOAD, Constants.DOCUMENT, new { id = doc.DocumentId }) %>"><%= doc.Name%></a ><%})
                        .Title(Constants.DOCUMENT);
                
                columns.Bound(doc => doc.File.Size).Width(60).HtmlAttributes(new { style = "text-align: right;" });
                
                if (Model.ShowButtons && Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
                {
                    columns.Bound(doc => doc.DocumentId).Template(doc =>
                        {%>
                        <%: Html.CheckBox("DocumentId_" + doc.DocumentId) %><%})
                        .Title("Delete")
                        .Width(50);
                }
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .Footer(false)
        .HtmlAttributes(new { style = "width: 460px;" })
        .Render();
    } %>

