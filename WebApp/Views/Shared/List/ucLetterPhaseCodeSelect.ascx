﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterPhaseCodeViewModel>" %>

<%  if (Model != null)
    {
        
        //Code for Treeview control
        Html.Telerik().TreeView()
        .Name("TreeView")
        .ShowCheckBox(true)
        .ExpandAll(Model.Expand)
        .ClientEvents(events => events.OnLoad("CodeTree_OnLoad"))
        .BindTo(Model.PhaseCodeTree, mappings =>
        {
            mappings.For<Aquilent.Cara.Domain.PhaseCodeSelect>(
                binding => binding.ItemDataBound((item, code) =>
                {
                    item.Checked = false;
                    item.Enabled = true;
                    if (code.CodeId == 0)
                    {
                        item.Checkable = false;
                    }
                    item.Text = code.CodeNumberDisplay + " " + code.CodeName;
                    item.Value = code.PhaseCodeId.ToString();
                    item.LinkHtmlAttributes["title"] = code.CodeNumberDisplay + " " + code.CodeName;
                })
                .Children(code => code.ChildCodes));

            mappings.For<Aquilent.Cara.Domain.PhaseCodeSelect>(binding => binding.ItemDataBound((item, code) => { item.Text = code.CodeName; }));
        })
        .HtmlAttributes(new { style = "position: relative;overflow: hidden;" })
        .Render();
      
    }
%>