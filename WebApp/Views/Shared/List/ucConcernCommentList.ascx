﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ConcernResponseInputViewModel>" %>

<%  if (Model.Comments != null)
    {
        // set the phase locked flag
        bool assignedToOther = Model.UserId > 0 && Model.UserId != Utilities.GetUserSession().UserId;
        bool locked = Model.Locked || Model.Comments.Count == 0 || assignedToOther;
        string disabled = (locked ? "disabled" : string.Empty);
        
        Html.Telerik().Grid(Model.Comments.OrderBy(x => x.Letter.LetterSequence).ThenBy(y => y.CommentNumber))
                .Name(Constants.LIST)
                .DataKeys(keys => keys.Add(comment => comment.CommentId).RouteKey("id"))
                .Columns(columns =>
                {
                    columns.Bound(comment => comment.CommentId).Template(comment =>
                    {%><a href="<%= Url.Action(Constants.CODING, Constants.LETTER, new { id = comment.LetterId, commentId = comment.LetterCommentNumber, useDefaultFilter = true }) %>"><%= comment.LetterCommentNumber%></a ><%}).Width(100);
					columns.Bound(comment => comment.CommentCodeText).Template(comment => comment.CommentCodeText.Replace("|", "<br />"));
                    columns.Bound(comment => comment.SampleStatement).Template(comment =>
                        {%><input type="radio" name="<%=string.Format("rbSampleStatement_{0}", comment.CommentId) %>" <%=comment.SampleStatement ? "checked" : string.Empty %> <%=disabled %> value="True" />Yes&nbsp
                        <input type="radio" name="<%=string.Format("rbSampleStatement_{0}", comment.CommentId) %>" <%=comment.SampleStatement ? string.Empty : "checked" %> <%=disabled %> value="False" />No<%})
                        .Sortable(false).Width(150);
                    if (!locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE))
                    {
                        columns.Bound(comment => comment.CommentId).Template(comment => 
                            { %>
                                <a id="btnUpdate_<%=comment.CommentId %>" href="#" class="button" onclick="javascript:updateComment(<%=Model.ProjectId %>, <%=Model.PhaseId %>, <%=comment.CommentId %>);"><%=Constants.UPDATE%></a>
                                <% if (Model.Comments.Count > 1)
                                    { %>
                                    <a class="button" href="#" onclick="confirmWindow('Remove this entry', 'deleteComment(<%=comment.ConcernResponseId %>, <%=comment.CommentId %>, <%=Model.PhaseId %>)')"><%=Constants.REMOVE%></a >
                                <% } %>
                            <% })
                            .Title(Constants.ACTION)
                            .HeaderHtmlAttributes(new { style = "text-align: center;" })
                            .HtmlAttributes(new { style = "text-align: center;" })
                            .Width(139);
                    }
                })
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .DetailView(details => details.Template(comment => {%>
                <%= Html.Telerik().Grid(new string[] { comment.CommentText })
                    .Name("CommentText_" + comment.CommentId)
                    .Columns(columns =>
                    {
                        columns.Bound(p => p).Width(500).Title(Utilities.GetResourceString(Constants.COMMENT).Pluralize());
                    })
                    .Footer(false)
                %><%}))
               .Pageable(settings => settings.PageSize(25))
               .HtmlAttributes(new { style = "width: 750px;" })
               .Render();
    } %>