﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterListViewModel>" %>
  
<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.LetterSearchResult>(Model.Letters)
                .Name(Constants.LIST)
                .ClientEvents(events => events.OnLoad("resetGridPager")) 
                .Columns(columns =>
                {
                    columns.Bound(letter => letter.LetterSequence)
                    .Template(letter => {%>
                        <%= Html.ActionLink(letter.LetterSequence.ToString(), Constants.CODING, Constants.LETTER, new { id = letter.LetterId }, null)%>
                        <%}).ClientTemplate("<a href=" + Url.Action(Constants.CODING, Constants.LETTER, new { id = "<#= LetterId #>" }) + "><#= LetterSequence #></a >")
                          .Width(50).Title("Letter\n#").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.AuthorName);
                    //columns.Bound(letter => letter.OrganizationTypeName).Width(125);
                    columns.Bound(letter => letter.OrganizationName).Width(175);
                    columns.Bound(letter => letter.DateSubmitted)
                        .Width(100).Title("Date\nSubmitted").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE })
                        .ClientTemplate("<#= DateSubmittedStr #>").Template(letter => {%><%= letter.DateSubmittedStr %><%});
                    columns.Bound(letter => letter.LetterTypeName)
                        .Width(120).Title("Letter\nType").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.LetterStatusName)
                        .Width(100).Title("Coding\nStatus").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.EarlyActionStatus)
                        .Width(100).Title("Early\nAttention").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.Size).HtmlAttributes(new { style = "text-align: right;" })
                        .Width(75).Title("Size\n(bytes)").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                    columns.Bound(letter => letter.HasAttachment)
                        .Template(letter => {%>
                        <%= letter.HasAttachment == 1 ? "<img src='/Content/Images/paper_clip.png' alt='Has Attachment' title='Has Attachment' />" : "<img src='/Content/Images/spacer.gif' alt='No Attachment' title='No Attachment' />" %>
                        <%})
                      .ClientTemplate("<#= HasAttachment ? \"<img src='/Content/Images/paper_clip.png' alt='Has Attachment' title='Has Attachment' />\" : \"<img src='/Content/Images/spacer.gif' alt='No Attachment' title='No Attachment' />\" #>")
                      .Width(3).Title("").Sortable(false);
                })
                .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
                .DataBinding(dataBinding => dataBinding.Ajax().Select("LetterListBinding", Constants.LETTER))
                .EnableCustomBinding(true)
                .Pageable(settings => settings
                    .PageSize(Utilities.GetUserSession().SearchResultsPerPage)
                    .Total((int)ViewData["total"])
                    .PageTo((int)ViewData["pageNum"])
                    .Style(GridPagerStyles.NextPreviousAndNumeric | GridPagerStyles.PageInput))
                .Sortable()
                .Render();
    } %>