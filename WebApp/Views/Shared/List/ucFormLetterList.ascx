﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.FormSetListViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid<Aquilent.Cara.Domain.FormSetLetterSearchResult>()
            .Name(Constants.LIST)
            .BindTo(Model.FormSets)
            .Columns(columns =>
            {
                columns.Bound(fs => fs.LetterSequence)
                .Template(fs =>
                {%>
                    <%= Html.ActionLink(fs.LetterSequence.ToString(), Constants.CODING, Constants.LETTER, new { id = fs.LetterId, useDefaultFilter = true }, null)%>
                    <%}).Width(50).Title("Letter\n#").HeaderHtmlAttributes(new { style = Constants.GRID_HEADER_STYLE });
                columns.Bound(fs => fs.LetterType).Title("Form Type");
                columns.Bound(fs => fs.LastName).Template(fs =>
                    {%>
                        <%=String.Format("{0}, {1}{2}", fs.LastName, fs.FirstName, 
                        string.IsNullOrEmpty(fs.OrganizationName)?"":"<br>"+fs.OrganizationName)%>
                    <%}).Title("Author Name and Organization");
                columns.Bound(fs => fs.StatusName).Title("Coding Status");
                columns.Bound(fs => fs.Size);
                columns.Bound(fs => fs.Confidence).Template(fs => 
                    { %>

                        <%= Model.LookupConfidenceGrade(fs.Confidence) %>

                    <%}).Title("Confidence").Sortable(true);
                columns.Bound(fs => fs.LetterId).Template(fs =>
                { %>
                   <% if (!(fs.LetterId == fs.MasterFormId))
                      { %>
                         <a href="#" onclick="openCompareMasterWindow(<%=fs.LetterId%>, <%=fs.MasterFormId %>, true);return false;"><img src="<%= ResolveUrl("~/Content/Images/icon_compare.png")%>" alt="Compare to Master" title="Compare to Master" /></a >
                   <% } %>
             <% })
					.Title(Constants.COMPARE)
                    .HeaderHtmlAttributes(new { style = "text-align: center;" })
                    .HtmlAttributes(new { style = "text-align: center;" })
                    .Sortable(false)
                    .Width(75);
				if (Model.CanEdit && Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
				{
                    columns.Bound(fs => fs.LetterId).Aggregate(aggregates => aggregates.Count())
                        .Template(fs =>
                        { %>
                        <% if (!(fs.LetterId == fs.MasterFormId) || Model.Count == 1)
                           { %>
                           <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <%: Html.Telerik().DropDownList()
                                        .Name("ddlLetterAction_" + fs.LetterId)
                                        .BindTo(Model.LookupLetterAction(fs, Model.Count))
                                        .HtmlAttributes(new { style = "width:175px;" })%>
                                </td>
                                <td>
                                    <a class="button" href="#" onclick="doLetterAction('<%=fs.LetterId %>', '<%=fs.FormSetId %>');return false;"><%: Constants.GO %></a>
                                </td>
                            </tr>
                           </table>
                        <% } %>
                        <% })
                        .Title(Constants.REDESIGNATE)
                        .HeaderHtmlAttributes(new { style = "text-align: center;" })
                        .HtmlAttributes(new { style = "text-align: center;" })
                        .Sortable(false)
                        .Width(240);
                }
            })
            .EnableCustomBinding(true)
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Pageable(paging => paging.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
            .Sortable()
            .Render();
    } %>