﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DmdDocumentViewModel>" %>

<%  if (Model != null)
    {
        Html.Telerik().Grid(Model.ProjectDocuments)
            .Name(Constants.LIST)
            .DataKeys(keys => keys.Add(doc => doc.DmdDocumentId).RouteKey("id"))
            .Columns(columns =>
            {
                if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
                {
                    columns.Bound(doc => doc.DmdDocumentId).Template(doc =>
                        {%><input type="checkbox" name="chkDocumentId" value="<%=doc.DmdDocumentId %>" /><%})
                            .Width(50)
                            .Title(Constants.ASSOCIATE);
                }
				columns.Bound(doc => doc.Name).Template(doc =>
					{%><a target="_blank" href="<%= doc.WwwLink %>"><%: doc.Name %></a ><%})
						.Title(Constants.DOCUMENT);
                columns.Bound(doc => doc.Size).Width(60);
            })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .Footer(false)
        .HtmlAttributes(new { style = "width: 700px;" })
        .Render();
    } %>