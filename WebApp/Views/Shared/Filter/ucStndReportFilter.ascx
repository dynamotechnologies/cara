﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.StndReportViewModel>" %>

<div class="filterArea">
    <table>
        <tr>
            <td class="fieldLabel"><label for="ProjectNameOrNumber">Search <%:Utilities.GetResourceString(Constants.PROJECT).Pluralize()%> by ID or Name</label></td>
            <td class="text">
                <%: Html.TextBoxFor(model => model.ProjectNameOrNumber)%>
                <input type="submit" class="button" name="action" value="Search Projects" />
            </td>
        </tr>
        <tr><td colspan="2">
        <% if (Model.Projects != null)
           {
               Html.RenderPartial("~/Views/Shared/List/ucProjectPhaseSelect.ascx", Model.Projects);
           } %>
        </td></tr>
    </table>
</div>