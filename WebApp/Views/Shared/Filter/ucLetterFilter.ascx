﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterListViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<table class="filterArea">
    <tr>
    <td class="fieldLabel"><label for="LetterSequence"><%:Utilities.GetResourceString(Constants.LETTER) %> #</label></td>
    <td><%: Html.TextBoxFor(model => model.LetterSequence)%></td>
    <td class="fieldLabel"><label for="FirstName">Author Name</label></td>
    <td><%: Html.TextBoxFor(model => model.LastName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="LetterTypeId"><%:Utilities.GetResourceString(Constants.LETTER) %> Type</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.LetterTypeId)
                             .BindTo(Model.LookupLetterType)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><label for="DateReceivedFrom">Date Submitted</label></td>
        <td class="text">
            <%= Html.Telerik().DatePickerFor(model => model.DateReceivedFrom).ShowButton(true) %>
            &nbsp;To&nbsp;
            <%= Html.Telerik().DatePickerFor(model => model.DateReceivedTo).ShowButton(true)%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="LetterStatusId">Coding Status</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.LetterStatusId)
                             .BindTo(Model.LookupLetterStatus)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><label for="Keyword"><%: Utilities.GetResourceString("Keyword") %></label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.Keyword) %>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by original letter text and coded letter text." />
        </td>
    </tr>
    <tr>
    <td class="fieldLabel"><label for="EarlyActionStatusId">Early Attention</label></td>
    <td class="text">
    <%: Html.Telerik().DropDownListFor(model => model.EarlyActionStatusId)
                             .BindTo(Model.LookupEarlyActionStatus)
                             .HtmlAttributes(new { style = "width:200px;" })%>
    </td>
    <td class="fieldLabel"><label for="WithinCommentPeriod">Received</label></td>
        <td class="text">
            <%: Html.Telerik().DropDownListFor(model => model.WithinCommentPeriod)
                .BindTo(Model.LookupCommentPeriodStatus)
                .HtmlAttributes(new { style = "width:200px;" })%>
        </td>
    </tr>
     <tr>
        <td class="text" colspan="4">
            <%: Html.CheckBoxFor(model => model.ViewAll)%><label for="ViewAll">Exclude Forms and Duplicates</label>
        </td>
        
    </tr>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%:Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%:string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString(Constants.LETTER).Pluralize())%>" />
        </td>
    </tr>
</table>