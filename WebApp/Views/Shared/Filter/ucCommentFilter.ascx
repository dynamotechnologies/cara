﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CommentListViewModel>" %>

<table class="filterArea">
<tr>
<td class="fieldLabel" valign="top"><label for="LetterSequence">Comment #</label></td>
<td class="text"><%: Html.TextBoxFor(model => model.LetterSequence)%></td>
<td class="fieldLabel" valign="top"><label for="CodeCategoryId"><%: Constants.COMMENT + " " + Constants.CODE %> Type</label></td>
<td class="text" valign="top"><%: Html.Telerik().DropDownListFor(model => model.CodeCategoryId)
                             .BindTo(Model.LookupCodeCategory)
                             .HtmlAttributes(new { style = "width:230px;" })%></td>
</tr>
    <tr>
        <td class="fieldLabel" valign="top"><label for="CodeSearchText"><%: Constants.CODE.Pluralize() %></label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.CodeSearchText, new { style = "width:200px;" })%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Code search" title="Search comments by code description, code number, and code numbers by range." /><br />
            <a href="#" onclick="javascript:openWindow('WindowCodeFilter', '/Project/<%=Model.ProjectId %>/Phase/<%=Model.PhaseId %>/Comment/SearchCode');">Advanced Search</a>
        </td>
        <td class="fieldLabel"><label for="Association"><%: Utilities.GetResourceString(Constants.CONCERN) %></label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Association)
                             .BindTo(Model.LookupYesNo)
                             .HtmlAttributes(new { style = "width:230px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="Keyword"><%: Utilities.GetResourceString("Keyword").Pluralize() %></label></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Keyword) %></td>
         <td class="fieldLabel"><label for="SampleStatement">Display Sample Comments Only</label></td>
        <td class="text"><%: Html.CheckBoxFor(model => model.SampleStatement) %></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="NoResponseRequired">No Further Response Required</label></td>
        <td class="text"><%: Html.CheckBoxFor(model => model.NoResponseRequired, new { onclick = "onCheckedNoResponse(this)" })%></td>
        <td class="fieldLabel"><label for="Annotation">Display Annotated Comments Only</label></td>
        <td class="text"><%: Html.CheckBoxFor(model => model.Annotation) %></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><label for="FilterUserId">Assigned To</label></td>
        <td class="text" valign="top"><%: Html.Telerik().DropDownListFor(model => model.FilterUserId)
                             .BindTo(Model.TeamMembers)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><label for="NoResponseReasonId" id="lblNoResponseReasonId" style="display:none">No Further Response Reason</label></td>
        <td class="text">
            <div id="ddlNoResponseReasonId" style="display:none">
                <%: Html.Telerik().DropDownListFor(model => model.NoResponseReasonId)
                             .BindTo(Model.LookupNoResponseReason)
                             .HtmlAttributes(new { style = "width:200px;" })%>
            </div>
        </td> 
    </tr>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%:Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%:string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString(Constants.COMMENT).Pluralize())%>" />
        </td>
    </tr>
</table>
<%: Html.HiddenFor(model => model.PhaseId) %>

<% Html.Telerik().Window()
    .Name("WindowCodeFilter")
    .Title(string.Format("{0} {1}", Constants.SEARCH, Constants.CODE.Pluralize()))
    .Draggable(true)
    .Modal(true)
    .ClientEvents(events => events.OnOpen("setWindowFocus"))
    .Buttons(b => b.Close())
    .Width(700)
    .Height(325)
    .Visible(false)
    .Render(); %>