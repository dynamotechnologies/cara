﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<br />
<div class="fieldLabel">Define the search criteria:</div>
<br />
<table class="filterArea">
    <tr>
        <td class="fieldLabel"><label for="codeName">Code Description</label></td>
        <td class="text">
            <%: Html.TextBox("codeName", string.Empty, new { maxlength = "50" })%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Code Description" title="You may use asterisk (*) to perform a wildcard search." />
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="codeNumber">Code Number (Single)</label></td>
        <td class="text">
            <%: Html.TextBox("codeNumber", string.Empty, new { maxlength = "15" })%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Code Number" title="You may use asterisk (*) to perform a wildcard search." />
        </td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><label for="codeRangeFrom1">Code Number (Range)</label></td>
        <td class="text">
            <% for (int i = 1; i <= int.Parse(ConfigurationManager.AppSettings["codeRangeCount"]); i++)
            { %>
                <div id="codeRange<%=i.ToString() %>" style="display: block;">
                    <input type="text" name="codeRangeFrom<%=i.ToString() %>" id="codeRangeFrom<%=i.ToString() %>" size="10" maxlength="15" />&nbsp;-&nbsp;
                    <input type="text" name="codeRangeTo<%=i.ToString() %>" id="codeRangeTo<%=i.ToString() %>" size="10" maxlength="15" />
                    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Code Number Range" title="You may use asterisk (*) to perform a wildcard search." />
                    <% if (i < int.Parse(ConfigurationManager.AppSettings["codeRangeCount"]))
                        { %>
                        <a class="button" id="show<%=i.ToString() %>" href="javascript:void(0);">&nbsp;+&nbsp;</a>
                    <% } %>
                    <% if (i > 1)
                        { %>
                        <a class="button" id="hide<%=i.ToString() %>" href="javascript:void(0);">&nbsp;-&nbsp;</a>
                    <% } %>
                    <br /><br />
                </div>
            <% } %>
        </td>
    </tr>
</table>
<br />
<input type="button" value="<%=Constants.OK %>" id="btnSave" name="save" class="button" onclick="saveFilter()" />
<input type="button" value="<%=Constants.CLEAR %>" id="btnClear" name="clear" class="button" onclick="clearFilter()" />
<input type="button" value="<%=Constants.CANCEL %>" id="btnCancel" name="cancel" class="button" onclick="closeWindow('WindowCodeFilter')" />

<script type="text/javascript" src="<%= ResolveUrl("~/Scripts/cara.codefilter.js")%>"></script>
<script type="text/javascript" src="<%= ResolveUrl("~/Scripts/telerik.window.dialog.js")%>"></script>
<script type="text/javascript">
    initialize();
</script>