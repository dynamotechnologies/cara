﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ConcernResponseListViewModel>" %>

<% if (!Constants.EDIT.Equals(Model.Mode))
{
%>
<table class="filterArea">
    <tr>
        <td class="fieldLabel"><label for="ConcernResponseNumber">Concern/Response #</label></td>
        <td class="text"><%=Html.TextBoxFor(model => model.ConcernResponseNumber) %></td>
        <td class="fieldLabel" valign="top"><label for="CodeCategoryId">C/R <%: Constants.CODE %> Type</label></td>
        <td class="text" valign="top"><%: Html.Telerik().DropDownListFor(model => model.CodeCategoryId)
                             .BindTo(Model.LookupCodeCategory)
                             .HtmlAttributes(new { style = "width:230px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="ConcernResponseStatusId">Status</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.ConcernResponseStatusId)
                             .BindTo(Model.LookupConcernResponseStatus)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel" valign="top"><label for="CodeSearchText"><%: Constants.CODE.Pluralize() %></label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.CodeSearchText, new { style = "width:200px;" })%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Code search" title="Search comments by code description, code number, and code numbers by range." /><br />
            <a href="#" onclick="javascript:openWindow('WindowCodeFilter', '/Project/<%=Model.ProjectId %>/Phase/<%=Model.PhaseId %>/Comment/SearchCode');">Advanced Search</a>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="DateUpdatedFrom">Last Updated From</label></td>
        <td class="text"><%= Html.Telerik().DatePickerFor(model => model.DateUpdatedFrom).ShowButton(true) %></td>
        <td class="fieldLabel"><label for="DateUpdatedTo">Last Updated To</label></td>
        <td class="text"><%= Html.Telerik().DatePickerFor(model => model.DateUpdatedTo).ShowButton(true) %></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><label for="FilterUserId">Assigned To</label></td>
        <td class="text" valign="top"><%: Html.Telerik().DropDownListFor(model => model.FilterUserId)
                             .BindTo(Model.TeamMembers)
                             .HtmlAttributes(new { style = "width:200px;" } )%></td>
        <td class="fieldLabel"><label for="Keyword"><%: Utilities.GetResourceString("Keyword") + "s" %></label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.Keyword) %>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by concern text and response text." />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%=Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%=string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString("Concerns"))%>" />
        </td>
    </tr>
</table>
<% Html.Telerik().Window()
    .Name("WindowCodeFilter")
    .Title(string.Format("{0} {1}", Constants.SEARCH, Constants.CODE.Pluralize()))
    .Draggable(true)
    .Modal(true)
    .ClientEvents(events => events.OnOpen("setWindowFocus"))
    .Buttons(b => b.Close())
    .Width(700)
    .Height(325)
    .Visible(false)
    .Render(); %>
<%
}
%>
<%: Html.HiddenFor(model => model.PhaseId) %>
<%: Html.HiddenFor(model => model.ProjectId) %>