﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.UserListViewModel>" %>

<table class="filterArea">
    <tr>
        <td class="fieldLabel"><label for="Username">Username</label></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Username) %></td>
        <td class="fieldLabel"><label for="UnitId"><%: Utilities.GetResourceString("LeadMgmtUnit") %></label></td>
        <td class="text">
                <%: Html.TextBoxFor(model => model.UnitName, new { disabled = "disabled", size = "40" }) %>
                <% Html.RenderPartial("~/Views/Shared/Util/ucUnitTreeSelector.ascx"
                       , new UnitTreeSelectorModel
                       {
                           Units = Model.Units,
                           Visible = false,
                           Closeable = true,
                           Modal = true,
                           UnitIdInputId = "UnitId",
                           UnitNameInputId = "UnitName"
                       }); %>
                <%: Html.HiddenFor(model => model.UnitId) %>
                <a id="btnChooseUnit" class="button" onclick="$('#UnitSelectorWindow').data('tWindow').center().open()">Choose Unit</a>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="LastName">Last Name</label></td>
        <td class="text"><%:Html.TextBoxFor(model => model.LastName)%></td>
        <td class="fieldLabel"><label for="FirstName">First Name</label></td>
        <td class="text"><%:Html.TextBoxFor(model => model.FirstName)%></td>
    </tr>
    <tr>
        <td colspan="4" class="fieldLabel">
            <label for="ShowOnlyNonProjectRoles">Only Show System Admins and Planning Contributors</label>&nbsp;     
            <%: Html.CheckBoxFor(model => model.ShowOnlyNonProjectRoles) %>&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%=Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%=string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString("Users"))%>" />
        </td>
    </tr>
</table>