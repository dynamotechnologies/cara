﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProjectListViewModel>" %>

<table class="filterArea">
    <tr>
        <td class="fieldLabel"><label for="Keyword"><%: Utilities.GetResourceString("Keyword")%></label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.Keyword, new { style = "width:225px;" })%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by project number, project name, and description." />
        </td>
        <td class="fieldLabel"><label for="AnalysisTypeId"><%: Utilities.GetResourceString("AnalysisType") %></label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.AnalysisTypeId)
                             .BindTo(Model.LookupAnalysisType)
                             .HtmlAttributes(new { style = "width:225px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="ProjectActivityId"><%: Utilities.GetResourceString("Activity") %></label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.ProjectActivityId)
                             .BindTo(Model.LookupActivity)
                             .HtmlAttributes(new { style = "width:225px;" })%></td>
        <td class="fieldLabel"><label for="ProjectStatusId">Status</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.ProjectStatusId)
                             .BindTo(Model.LookupProjectStatus)
                             .HtmlAttributes(new { style = "width:225px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="UnitId"><%:Utilities.GetResourceString("LeadMgmtUnit") %></label></td>
        <td class="text">
                <%: Html.TextBoxFor(model => model.UnitName, new { disabled = "disabled", size = "40" }) %>
                <% Html.RenderPartial("~/Views/Shared/Util/ucUnitTreeSelector.ascx"
                       , new UnitTreeSelectorModel
                       {
                           Units = Model.Units,
                           Visible = false,
                           Closeable = true,
                           Modal = true,
                           UnitIdInputId = "UnitId",
                           UnitNameInputId = "UnitName"
                       }); %>
                <%: Html.HiddenFor(model => model.UnitId) %>
                <a id="btnChooseUnit" class="button" onclick="$('#UnitSelectorWindow').data('tWindow').center().open()">Choose Unit</a>
        </td>
        <td class="fieldLabel"><label for="StateId">State</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.StateId)
                             .BindTo(Model.LookupState)
                             .HtmlAttributes(new { style = "width:225px;" })%></td>
    </tr>
    <tr>  
        <td class="fieldLabel"><label for="CommentPeriodTypeId">Comment Period Type</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.PhaseTypeId)
                             .BindTo(Model.LookupPhaseType)
                             .HtmlAttributes(new { style = "width:225px;" })%></td>
    </tr>
    <tr>
        <td colspan="4" class="fieldLabel">
            <label for="MyProjects">Only Show <%:Utilities.GetResourceString("MyProjects")%></label>&nbsp;     
            <%: Html.CheckBoxFor(model => model.MyProjects) %>&nbsp;&nbsp;
            <label for="Archived">Include Archived <%:Utilities.GetResourceString(Constants.PROJECT).Pluralize()%></label>&nbsp;
            <%: Html.CheckBoxFor(model => model.Archived) %>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%:Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%:string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString(Constants.PROJECT).Pluralize())%>" />
        </td>
    </tr>
</table>