﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.FormSetListViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<table class="filterArea">
    <tr>
        <td class="fieldLabel"><label for="LetterTypeId"><%: Utilities.GetResourceString(Constants.LETTER) %> Type</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.LetterTypeId)
                             .BindTo(Model.LookupLetterType)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="LetterStatusId">Status</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.LetterStatusId)
                             .BindTo(Model.LookupLetterStatus)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%=Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%=string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString(Constants.LETTER).Pluralize())%>" />
        </td>
    </tr>
</table>