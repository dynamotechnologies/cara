﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CustomReportListViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<div>
    Please add the analysis criteria and select the appropriate output information: Submitter information, <%:Constants.LETTER %>/<%:Constants.COMMENT%>, <%:Constants.PROJECT %>. 
    <br />For new searches, please click <%:Constants.CLEAR %>.
</div>
<br />
<div class="text">
    <b>Include data from:</b>&nbsp;
    <%: Html.CheckBoxFor(model => model.SubmitterView, new { id = "SubmitterView1", onclick = "updateCheckboxValue(this)" })%><label for="SubmitterView1">Submitter</label>&nbsp;
    <%: Html.CheckBoxFor(model => model.LetterView, new { id = "LetterView1", onclick = "updateCheckboxValue(this)" })%><label for="LetterView1"><%:Constants.LETTER %>/<%:Constants.COMMENT %></label>&nbsp;
    <%: Html.CheckBoxFor(model => model.ProjectView, new { id = "ProjectView1", onclick = "updateCheckboxValue(this)" })%><label for="ProjectView1"><%:Constants.PROJECT %></label>
</div>
<br />
<div class="filterAreaTitle">Submitter</div>
<div class="filterArea">
    <table>        
        <tr>
            <td class="fieldLabel"><label for="LastName">Last Name</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.LastName)%></td>
            <td class="fieldLabel"><label for="FirstName">First Name</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.FirstName)%></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="City">City</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.City)%></td>
            <td class="fieldLabel"><label for="State">State</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.StateId)
                                 .BindTo(Model.LookupState)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="ZipList">Zip/Postal</label></td>
            <td class="text">
                <%: Html.TextBoxFor(model => model.ZipList, new { maxlength = "100" })%>
                <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Zip/Postal search" title="Search more than one Zip/Postal codes separated by commas (e.g. 10012, 10025)." />
            </td>
            <td class="fieldLabel"><label for="CountryId">Country</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.CountryId)
                                 .BindTo(Model.LookupCountry)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="OrganizationId">Organization</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.OrganizationId)
                                 .BindTo(Model.LookupOrganization)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
            <td class="fieldLabel"><label for="OrganizationTypeId">Organization Type</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.OrganizationTypeId)
                                 .BindTo(Model.LookupOrganizationType)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="OfficialRepresentativeTypeId">Official Representative Type</label></td>
            <td class="text" colspan="3"><%: Html.Telerik().DropDownListFor(model => model.OfficialRepresentativeTypeId)
                                             .BindTo(Model.LookupOfficialRepresentativeType)
                                             .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
    </table>
</div>
<br />
<div class="filterAreaTitle"><%:Constants.LETTER%>/<%:Constants.COMMENT %></div>
<div class="filterArea">
    <table>
        <tr>
            <td class="fieldLabel" width="180"><label for="DeliveryTypeId">Delivery Type</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.DeliveryTypeId)
                                 .BindTo(Model.LookupDeliveryType)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
            <td class="fieldLabel">Submitted Date</td>
            <td class="text">
                From:<%= Html.Telerik().DatePickerFor(model => model.DateSubmittedFrom).ShowButton(true) %>&nbsp;
                To:<%= Html.Telerik().DatePickerFor(model => model.DateSubmittedTo).ShowButton(true) %>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="LetterSequence"><%:Constants.LETTER %> #</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.LetterSequence)%></td>
            <td class="fieldLabel"><label for="LetterText"><%:Constants.LETTER %> Text</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.LetterText)%></td>
        </tr>
        <tr>
            <td class="fieldLabel" valign="top"><label for="CodeSearchText"><%:Constants.COMMENT %> <%: Utilities.GetResourceString(Constants.CODE) %></label></td>
            <td class="text" colspan="3">
                <%: Html.TextBoxFor(model => model.CodeSearchText, new { style = "width:200px;" })%>
                <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Comment Code search" title="Search comments by code description, code number, and code numbers by range." /><br />
                <a href="#" onclick="javascript:openWindow('WindowCodeFilter', '/Report/SearchCode');">Advanced Search</a>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="ConcernText">Concern Text</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.ConcernText)%></td>
            <td class="fieldLabel"><label for="ResponseText">Response Text</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.ResponseText)%></td>
        </tr>
    </table>
</div>
<br />
<br />
<div class="filterAreaTitle"><%:Constants.PROJECT %> Information</div>
<div class="filterArea">
    <table>
        <tr>
            <td class="fieldLabel"><label for="ProjectNameOrId"><%:Constants.PROJECT %> Name or ID</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.ProjectNameOrId)%></td>
            <td class="fieldLabel"><label for="ProjectActivityId"><%: Utilities.GetResourceString("Activity") %></label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.ProjectActivityId)
                                 .BindTo(Model.LookupActivity)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="UnitCode">Region/Forest/District</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.UnitCode)%></td>
            <td class="fieldLabel"><label for="ProjectStateId">State</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.ProjectStateId)
                                 .BindTo(Model.LookupState)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="ProjectCountyName">County</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.ProjectCountyName)%></td>
            <td class="fieldLabel"><label for="ProjectTypeId">Project Type</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.ProjectTypeId)
                                 .BindTo(Model.LookupProjectType)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="AnalysisTypeId"><%: Utilities.GetResourceString("AnalysisType") %></label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.AnalysisTypeId)
                                 .BindTo(Model.LookupAnalysisType)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
            <td class="fieldLabel"><label for="PhaseTypeId"><%:Utilities.GetResourceString(Constants.PHASE) %></label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(model => model.PhaseTypeId)
                                 .BindTo(Model.LookupPhaseType)
                                 .HtmlAttributes(new { style = "width:200px" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="PhaseName"><%:Utilities.GetResourceString(Constants.PHASE) %> Description</label></td>
            <td class="text" colspan="3"><%: Html.TextBoxFor(model => model.PhaseName)%></td>
        </tr>
    </table>
</div>
<br />
<div class="text">
    <b>Include data from:</b>&nbsp;
    <%: Html.CheckBoxFor(model => model.SubmitterView, new { onclick = "updateCheckboxValue(this)" })%><label for="SubmitterView">Submitter</label>&nbsp;
    <%: Html.CheckBoxFor(model => model.LetterView, new { onclick = "updateCheckboxValue(this)" })%><label for="LetterView"><%=Constants.LETTER %>/<%=Constants.COMMENT %></label>&nbsp;
    <%: Html.CheckBoxFor(model => model.ProjectView, new { onclick = "updateCheckboxValue(this)" })%><label for="ProjectView"><%=Constants.PROJECT %></label>
    <%: Html.HiddenFor(model => model.ReportUserId)%>
</div>

<% Html.Telerik().Window()
    .Name("WindowCodeFilter")
    .Title(string.Format("{0} {1}", Constants.SEARCH, Constants.CODE + "s"))
    .Draggable(true)
    .Modal(true)
    .ClientEvents(events => events.OnOpen("setWindowFocus"))
    .Buttons(b => b.Close())
    .Width(700)
    .Height(325)
    .Visible(false)
    .Render(); %>

<% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/telerik.window.dialog.js"));
    %>