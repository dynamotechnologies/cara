﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterReadingRoomListViewModel>" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<table class="filterArea">
    <tr>
        <td class="fieldLabel"><label for="FirstName">First Name</label></td>
        <td class="text"><%: Html.TextBoxFor(m => m.FirstName) %></td>
        <td class="fieldLabel"><label for="OrganizationId">Organization</label></td>
        <td class="text"><%: Html.TextBoxFor(m=>m.OrganizationId) %></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="LastName">Last Name</label></td>
        <td class="text"><%: Html.TextBoxFor(m => m.LastName) %></td>
        <td class="fieldLabel"><label for="Keyword"><%: Utilities.GetResourceString("Keyword") %></label></td>
        <td class="text">
            <%: Html.TextBoxFor(m => m.Keyword) %>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by original letter text." />
        </td>
    </tr>
    <% if (!Model.IsPublicMode)
       { %>
        <tr>
            <td class="fieldLabel"><label for="LetterTypeId"><%:Utilities.GetResourceString(Constants.LETTER)%> Type</label></td>
            <td class="text" colspan="3"><%: Html.Telerik().DropDownListFor(m => m.LetterTypeId)
                                 .BindTo(Model.LookupLetterType)
                                 .HtmlAttributes(new { style = "width:200px;" })%></td>
        </tr>
        <tr>
            <td class="fieldLabel" colspan="4">
                <%: Html.RadioButtonFor(m => m.PublishToReadingRoom, true, new { id = "rbPublishToReadingRoom1" })%><label for="rbPublishToReadingRoom1">Only Show Published <%:Utilities.GetResourceString(Constants.LETTER).Pluralize()%></label>&nbsp;
                <%: Html.RadioButtonFor(m => m.PublishToReadingRoom, false, new { id = "rbPublishToReadingRoom2" })%><label for="rbPublishToReadingRoom2">Only Show Un-published <%:Utilities.GetResourceString(Constants.LETTER).Pluralize()%></label>
            </td>
        </tr>
    <% }
       else
       { %>
        <tr>
            <td class="fieldLabel" colspan="4">
                <%: Html.RadioButtonFor(m => m.ShowForms, true, new { id = "rbShowForms1" })%><label for="rbShowForms1">Show Form <%:Utilities.GetResourceString(Constants.LETTER).Pluralize()%></label>&nbsp;
                <%: Html.RadioButtonFor(m => m.ShowForms, false, new { id = "rbShowForms2" })%><label for="rbShowForms2">Do Not Show Form <%:Utilities.GetResourceString(Constants.LETTER).Pluralize()%></label>
                <%: Html.HiddenFor(m => m.LetterTypeId) %>
                <%: Html.HiddenFor(m => m.ProjectId) %>
            </td>
        </tr>
    <% } %>
    <tr>
        <td colspan="4">
            <input type="submit" class="button" name="action" value="<%:Constants.FILTER%>" />
            <input type="submit" class="button" name="action" value="<%:string.Format("{0} {1}", Constants.SHOW_ALL, Utilities.GetResourceString(Constants.LETTER).Pluralize())%>" />
        </td>
    </tr>
</table>