﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.RecaptchaViewModel>" %>

<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=loadRecaptcha&render=explicit" async defer></script>

<script type="text/javascript">
    function loadRecaptcha() {
        grecaptcha.render("recaptcha", 
            { 
                "sitekey": "<%= ConfigurationManager.AppSettings["reCaptchaPublicKey"] %>"
            });
    }
</script>

<div class="text"><%: Model.Description %></div>
<br />
<div id="recaptcha"></div>
<br />
<a id="btnVerify" class="button" href="#" onclick="javascript:return submitRecaptcha();">Submit</a>
<a id="btnClose" class="button" href="#" onclick="javascript:return closeWindowRecaptcha();"><%: Constants.CANCEL %></a>

