﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DiffPlex.DiffBuilder.Model.DiffPaneModel>" %>

<%
    string unchangedSymbol = "&nbsp;";
    string insertedSymbol = "+";
    string deletedSymbol = "-";
    string modifiedSymbol = "~";
    string symbol = string.Empty;
    string title = string.Empty;
 %>

<div class="diffPane">
    <table cellpadding="0" cellspacing="0" class="diffTable">
        <% foreach (var diffLine in Model.Lines)
           {
               switch (diffLine.Type)
               {
                   case DiffPlex.DiffBuilder.Model.ChangeType.Deleted:
                       title = "Deleted";
                       symbol = deletedSymbol;
                       break;
                   case DiffPlex.DiffBuilder.Model.ChangeType.Imaginary:
                       title = "Deleted";
                       symbol = deletedSymbol;
                       break;
                   case DiffPlex.DiffBuilder.Model.ChangeType.Inserted:
                       title = "Inserted";
                       symbol = insertedSymbol;
                       break;
                   case DiffPlex.DiffBuilder.Model.ChangeType.Modified:
                       title = "Modified";
                       symbol = modifiedSymbol;
                       break;
                   default:
                       title = "Unchanged";
                       symbol = unchangedSymbol;
                       break;
               }
        %>
        <tr>
            <td class="lineNumber">
                <span title="<%=string.Format("{0} Line", title) %>">
                    <%= diffLine.Position.HasValue? diffLine.Position.ToString() : "&nbsp;"  %><%= symbol%>
                </span>
            </td>
            <td class="line <%=diffLine.Type.ToString() %>Line">
                    <% Html.RenderPartial("~/Views/Shared/Util/ucDiffLine.ascx", diffLine); %>                
            </td>
        </tr>
        <%} %>
    </table>
</div>
