﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DiffPlex.DiffBuilder.Model.DiffPiece>" %>

<% if (!string.IsNullOrEmpty(Model.Text))
   {
       string spaceValue = "&nbsp;";
       string tabValue = "&nbsp;&nbsp;&nbsp;";

       if (Model.Type == DiffPlex.DiffBuilder.Model.ChangeType.Imaginary || Model.Type == DiffPlex.DiffBuilder.Model.ChangeType.Deleted ||
            Model.Type == DiffPlex.DiffBuilder.Model.ChangeType.Inserted || Model.Type == DiffPlex.DiffBuilder.Model.ChangeType.Unchanged)
       {
           %>
            <%= Html.Encode(Model.Text).Replace("\t", tabValue)%>
           <%
       }
       else if (Model.Type == DiffPlex.DiffBuilder.Model.ChangeType.Modified)
       {
           foreach (var character in Model.SubPieces)
           {
               if (character.Type == DiffPlex.DiffBuilder.Model.ChangeType.Imaginary) continue;
               %>
               <span class="<%= character.Type.ToString() %>Character"><%= character.Text%></span>           
           <%
           }
       }
   } %>
