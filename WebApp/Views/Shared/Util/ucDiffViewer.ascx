﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CompareMasterViewModel>" %>
<div id="viewerArea">
    <div class="pageTitle">
    <table width="90%">
      <tr>
       <td align="right">
       <%if (Model.MasterLetter != null && Model.MasterLetter.FormSet != null)
         { %><span style="float:left;"><%: Model.MasterLetter.FormSet.Name%></span><%} %>
        <%: Constants.LETTER%>&nbsp;#<a href="<%: Url.Action(Constants.CODING, Constants.LETTER, new { id = Model.Letter.LetterId, useDefaultFilter = true }) %>"><%: Model.Letter.LetterSequence%></a >&nbsp;
        (<%: (Model.Letter.FormSet != null ? Model.Letter.FormSet.Name : Model.Letter.LetterTypeName) %>)
        &nbsp;
       </td>
       <% if (Model.CanNavigate)
          { %>
       <td align="right">
        <%if (Model.PrevLetter != null)
          { %>   
        <a class="button" href="#" onclick="openCompareMasterWindow(<%: Model.PrevLetter.LetterId %>,<%= Model.MasterLetter.FormSetId.HasValue ? Model.MasterLetter.LetterId : -1 %>, true);return false;">< Previous</a>&nbsp;
        <%} %>
    
        <%if (Model.NextLetter != null)
          { %>
           <a class="button" href="#" onclick="openCompareMasterWindow(<%: Model.NextLetter.LetterId %>,<%= Model.MasterLetter.FormSetId.HasValue ? Model.MasterLetter.LetterId : -1 %>, true);return false;">Next > </a>
        <%} %>
        </td>
        <% } %>
      </tr>
    </table>
    </div>
    <div id="compareArea" class="filterArea">
    <table>
        <tr>
            <td class="text" width="26%"><label for="ddlFormSet"><%: Constants.COMPARE %> with <%: Utilities.GetResourceString("MasterForm")%> from Set:</label></td>
            <td width="24%"><%: Html.Telerik().DropDownList()
                                .Name("ddlFormSet")
                                .BindTo(Model.LookupFormSet)
                                .Value(Model.MasterLetter != null ? Model.MasterLetter.LetterId.ToString() : "")
                                .HtmlAttributes(new { style = "width:200px;" })%>
            </td>
            <td>
                <a class="button" href="#" onclick="compareFormSetMaster();return false;"><%: Constants.GO %></a>
            </td>
        <% if (Model.CanEdit && Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
       { %>
            <td align="center">
                <label for="ddlAction"><%: Constants.REDESIGNATE %>:</label>
            </td>
            <td class="text" width="26%">
                
                <%: Html.Telerik().DropDownList()
                                .Name("ddlAction")
                                .BindTo(Model.LookupAction)
                                .HtmlAttributes(new { style = "width:220px;" })%>
            </td>
            <td>
                <a class="button" href="#" onclick="redesignateLetter();return false;"><%: Constants.GO %></a>
            </td>
        <% } %>
        </tr>
    </table>
    </div>
    <% if (Model.SideBySideDiffModel != null)
       { %>
        <div id="diffBox">
            <div id="leftPane">
                <% if (Model.MasterLetter != null && Model.MasterLetter.LetterId > 0)
                   { %>
                    <div class="diffHeader">Master <%: Constants.LETTER%> Text&nbsp;<%: Model.MasterLetter != null && Model.MasterLetter.FormSet != null ? "(" + Model.MasterLetter.FormSet.Name + ")" : string.Empty%></div>
                    <% Html.RenderPartial("~/Views/Shared/Util/ucDiffPane.ascx", Model.SideBySideDiffModel.OldText); %>
                <% }
                   else
                   { %>
                    <div class="fieldLabel" style="text-align:center">Please select a <%: Utilities.GetResourceString(Constants.FORM_SET) %> to compare against this letter</div>
                <% } %>
            </div>
            <div id="rightPane">
                <div class="diffHeader"><%: Constants.LETTER %> Text</div>
                <% Html.RenderPartial("~/Views/Shared/Util/ucDiffPane.ascx", Model.SideBySideDiffModel.NewText); %>
            </div>
            <div class="clear">
            </div>
        </div>
    <% } %>
    <div class="filterArea"><b>Legend:</b>&nbsp;+: Added,&nbsp;-: Deleted,&nbsp;~: Modified
    <div style="float:right"><label for="syncScroll">Synchronize Scrollbars&nbsp;&nbsp;&nbsp;</label>
    <input id="syncScroll" checked = "checked" type="checkbox" /></div></div>
    
    <script type="text/javascript"><!--
        $(function () {
            InitializeDiffPanes();
        });
    // -->
    </script>
    <!-- hidden variables -->
    <%: Html.Hidden("SourceLetterId", Model.Letter.LetterId)%>
    <%: Html.HiddenFor(m=>m.CanNavigate) %>
    <% if (Model.MasterLetter != null)
       { %>
       <%: Html.Hidden("MasterFormSetId", Model.MasterLetter.FormSetId)%>
    <% } %>
</div>