﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UnitTreeSelectorModel>" %>


<% 
    // If not visible, use the following javascript in the parent page to open the window:
    //     $('#UnitSelectorWindow').data('tWindow').center().open();
    //
    // If the parent wants to use the selected values, you must write a javascript method:
    //     function handleUnitSelection(selectedUnitId, selectedUnitText)

    Html.Telerik().Window()
        .Name("UnitSelectorWindow")
        .Title("Choose a Unit")
        .Visible(Model.Visible)
        .Draggable(false)
        .Scrollable(false)
        .Modal(Model.Modal)
        .Content(() =>
        {%>
            <% if (Model.Closeable)
               { %>
            <br />
            <a href="#" onclick="closeUnitTreeSelector()" id="SelectUnitTop" class="button">Select Unit</a>
            <br /><br />
            <% } %>
            <div style="overflow:auto;max-height:220px;margin-bottom:5px">
            <% Html.Telerik().TreeView()
                .Name("UnitSelectorTreeView")
                .ClientEvents(events => events.OnSelect("selectUnit"))
                .BindTo(Model.Units, mappings =>
                {
                    mappings.For<Aquilent.Cara.Reference.Unit>(binding =>
                        binding.ItemDataBound((item, unit) =>
                            {
                                item.Text = unit.Name;
                                item.Value = unit.UnitId;
                            })
                            .Children(unit => unit.Children));
                }).Render();
            %>
            </div>
            <% if (Model.Closeable)
               { %>
            <br />
            <a href="#" onclick="closeUnitTreeSelector()" id="SelectUnitBottom" class="button">Select Unit</a>
            <br /><br />
            <% } %>
        <%})
        .Width(Model.Width)
        .Height(Model.Height)
        .Render();
%>

<script type="text/javascript">
    var selectedUnitText = null;
    var selectedUnitId = null;

    function treeView() {
        return $('#UnitSelectorTreeView').data('tTreeView');
    }

    function unitSelectorWindow() {
        return $('#UnitSelectorWindow').data('tWindow');
    }

    function closeUnitTreeSelector() {
        if (selectedUnitId != null) {
            $('#<%= Model.UnitNameInputId %>').val(selectedUnitText);
            $('#<%= Model.UnitIdInputId %>').val(selectedUnitId);
        }

        unitSelectorWindow().close();
    }

    function selectUnit(e) {
        selectedUnitText = treeView().getItemText(e.item);
        selectedUnitId = treeView().getItemValue(e.item);
    }
</script>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/jquery.treeview.js")
            .Add("~/Scripts/telerik.controls.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/telerik.common.min.js")
            .Add("~/Scripts/telerik.treeview.min.js"));
%>

