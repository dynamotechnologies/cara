﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.Domain.Project>" %>

<% if (Model != null) 
   { %>
<table width="900">
    <tr><td class="sectionTitle"><%=Utilities.GetResourceString(Constants.PROJECT)%></td></tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.ProjectNumber) %>:</td>
        <td class="text" valign="top"><%: Html.DisplayFor(proj => proj.ProjectNumber) %></td>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.Name) %>:</td>
        <td class="text" valign="top"><%: Html.DisplayFor(proj => proj.Name) %></td>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.UnitName) %>:</td>
        <td class="text" valign="top"><%: Html.DisplayFor(proj => proj.UnitName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.AnalysisTypeName)%>:</td>
        <td class="text" valign="top"><%: Html.DisplayFor(proj => proj.AnalysisTypeName) %></td>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.ProjectStatusName)%>:</td>
        <td class="text" valign="top"><%: Html.DisplayFor(proj => proj.ProjectStatusName) %></td>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.ProjectActivityNameList)%>:</td>
        <td class="text" valign="top"><%: Html.DisplayFor(proj => proj.ProjectActivityNameList) %></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(proj => proj.Description)%>:</td>
        <td class="text markdown" colspan="5"><%: Html.DisplayFor(proj => proj.Description) %></td>
    </tr>
</table>

    <% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/Markdown.Converter.js")
                .Add("~/Scripts/cara.markdown.js")); %>

<% } %>