﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.Domain.Phase>" %>

<% if (Model != null) 
   { %>
<table>
    <tr>
        <td class="sectionTitle" colspan="4">
            <%: Utilities.GetResourceString(Constants.PHASE)%>&nbsp;
            <span class="text"><%: Html.ActionLink(Constants.EDIT, Constants.INPUT, Constants.PHASE,
                        new { projectId = Model.ProjectId, phaseId = Model.PhaseId, mode = Request.Params[Constants.MODE] }, new { @class = "button" })%></span>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.PhaseTypeName) %>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.PhaseTypeName) %></td>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.Name)%>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.Name)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.CommentStart)%>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.CommentStart)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.CommentEnd)%>:</td>
        <td class="text">
            <% if (Model.AlwaysOpen)
               { %> 
                    N/A (Indefinite)<% 
               }
               else
               { %>
                <%: Html.DisplayFor(phase => phase.CommentEnd)%>
            <% } %>
        </td>
        <td class="fieldLabel">Time Zone: </td>
        <td class="text">
            <span id="timeZone"><%:Html.DisplayFor(phase => phase.TimeZone)%></span>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="TimeZoneToolTip" title="Comments close at midnight in the selected time zone." />
        </td>
    </tr>
</table>
<br />
<% Html.Telerik().TabStrip()
    .Name("Section")
    .Items(tabstrip =>
    {
        tabstrip.Add()
            .Text(Utilities.GetResourceString("TeamMember").Pluralize())
            .Content(() =>
            {%>
            <div class="tabArea">
            <% Html.RenderPartial("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel { Members = Model.PhaseMemberRoles, ShowButtons = false }); %>
            <br />
            <% if (Utilities.UserHasPrivilege("MTM", Constants.PHASE))
                { %>
            <%: Html.ActionLink(Constants.EDIT, "MemberInput", Constants.PHASE,
                            new { projectId = Model.ProjectId, phaseId = Model.PhaseId, mode = Request.Params[Constants.MODE] }, new { @class = "button" })%>
            <% } %>
            </div>
        <%}).Selected(true);
        
        tabstrip.Add()
            .Text(Utilities.GetResourceString("CodingStructure"))
            .Content(() =>
            {%>
            <div class="tabArea" style="width:685px">
            <% Html.RenderPartial("~/Views/Shared/Details/ucPhaseCodesDetails.ascx", new PhaseCodesViewModel { PhaseCodes = Model.PhaseCodes }); %>
            <br />
            <%: Html.ActionLink(Constants.EDIT, "CodeInput", Constants.PHASE,
                            new { projectId = Model.ProjectId, phaseId = Model.PhaseId, mode = Request.Params[Constants.MODE] }, new { @class = "button" })%>
            </div>
        <%});
        
        tabstrip.Add()
            .Text(Utilities.GetResourceString(Constants.DOCUMENT).Pluralize())
            .Content(() =>
            {%>
            <div class="tabArea">
            <% Html.RenderPartial("~/Views/Shared/List/ucDmdDocumentList.ascx", new DmdDocumentViewModel { Documents = Model.Documents, ShowButtons = false }); %>
            <br />
            <%: Html.ActionLink(Constants.EDIT, "DocumentInput", Constants.PHASE,
                            new { projectId = Model.ProjectId, phaseId = Model.PhaseId, mode = Request.Params[Constants.MODE] }, new { @class = "button" })%>
            </div>
        <%});
        
        tabstrip.Add()
            .Text(Utilities.GetResourceString(Constants.TERM_LIST))
            .Content(() =>
            {%>
            <div class="tabArea">
            <% Html.RenderPartial("~/Views/Shared/List/ucTermListList.ascx", Model.TermLists); %>
            <br />
            <%: Html.ActionLink(Constants.EDIT, "TermListInput", Constants.PHASE,
                            new { projectId = Model.ProjectId, phaseId = Model.PhaseId, mode = Request.Params[Constants.MODE] }, new { @class = "button" })%>
            </div>
        <%});
    }).Render();%>
<% } %>