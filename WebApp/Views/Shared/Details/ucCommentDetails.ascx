﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.Domain.Comment>" %>

<table width="700">
    <tr>
        <td class="fieldLabel" width="25%"><%: Html.LabelFor(model => model.CommentId) %>:</td>
        <td><%: Html.DisplayFor(model => model.CommentId) %></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.LetterSequence)%>:</td>
        <td><%: Html.ActionLink(Model.Letter.LetterSequence.ToString(), Constants.CODING, Constants.LETTER, new { id = Model.Letter.LetterId, commentId = Model.CommentId, useDefaultFilter = true }, null)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ConcernResponseId)%>:</td>
        <td>
            <% if (Model.ConcernResponseId.HasValue)
               { %>
                <%: Html.ActionLink(Model.ConcernResponseId.ToString(), Constants.INPUT, Constants.CONCERN, new { id = Model.ConcernResponseId }, null)%>
            <% } %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.SampleStatement)%>:</td>
        <td><%=Model.SampleStatement.ConvertToYesNo()%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.NoResponseReasonName)%>:</td>
        <td>
            <%: Html.DisplayFor(model => model.NoResponseReasonName)%>
            <% if (!string.IsNullOrEmpty(Model.NoResponseReasonOther))
               { %>
               &nbsp;&nbsp;(<%: Html.DisplayFor(model => model.NoResponseReasonOther)%>)
            <% } %>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="fieldLabel">Codes</td>
    </tr>
    <tr>
        <td colspan="2" class="text">
            <% Html.RenderPartial("~/Views/Shared/List/ucCommentCodeList.ascx", Model.CommentCodes); %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.CommentText)%>:</td>
        <td><%: Html.DisplayFor(model => model.CommentText)%></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Annotation)%>:</td>
        <td><%: Html.DisplayFor(model => model.Annotation)%></td>
    </tr>
</table>
