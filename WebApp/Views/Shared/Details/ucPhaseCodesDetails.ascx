﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseCodesViewModel>" %>

<% Html.Telerik().TreeView()
    .Name(Model.TreeViewName)
    .ShowCheckBox(false)
    .ExpandAll(Model.ExpandAll)
    .BindTo(Model.PhaseCodeTree, mappings =>
    {
        mappings.For<Aquilent.Cara.Domain.PhaseCode>(
            binding => binding.ItemDataBound((item, code) =>
            {
                // expand the type level nodes
                if (string.IsNullOrEmpty(code.CodeName) && code.PhaseCodeId == 0)
                {
                    item.Checkable = false;
                    item.Expanded = true;
                }
                item.Text = code.CodeNumberDisplay + " " + code.CodeName;
                item.Value = code.PhaseCodeId.ToString();
            })
            .Children(code => code.ChildCodes));

        mappings.For<Aquilent.Cara.Domain.PhaseCode>(binding => binding.ItemDataBound((item, code) => { item.Text = code.CodeName; }));
    })
    .Render(); %>
