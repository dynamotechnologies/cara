﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProjectDetailsViewModel>" %>
<%@ Import Namespace="Aquilent.Cara.Domain" %>

<% if (Model != null) 
   { %>
<% Html.RenderPartial("~/Views/Shared/List/ucPhaseList.ascx", Model.Phases); %>
<br />
<a class="button" id="showPalsProject" href="javascript:void(0);">Show Project Information</a>&nbsp;
<% if (Model.CanDelete) { %>
    <a class="button" href="#" onclick="confirmWindow('Project will be deleted. Do you wish to continue', 'deleteProject(<%=Model.Project.ProjectId %>)')"><%=Constants.DELETE%></a >
<% } %>
<div id="palsProject" style="display: none">
<br /><br />
<table width="925">
    <tr>
        <% if (Model.Project.IsPalsProject)
           { %>
        <td class="fieldLabel" width="15%" valign="top"><%=Utilities.GetResourceString("PALS")%>&nbsp;<%=Utilities.GetResourceString(Constants.PROJECT)%>:</td>
        <td class="text" width="12%" valign="top"><a target="_blank" href="<%=Model.PalsProjectUrl %>"><%=Constants.PROJECT + " " + Constants.DETAILS%></a></td>
        <td class="fieldLabel" width="18%" valign="top"><%: Html.LabelFor(model => model.Project.ProjectStatusName)%>:</td>
        <td class="text" width="20%" valign="top"><%: Html.DisplayFor(model => model.Project.ProjectStatusName)%></td>
        <% } %>
        <td class="fieldLabel" width="15%" valign="top"><%: Html.LabelFor(model => model.Project.UnitName) %>:</td>
        <td class="text" width="20%" valign="top"><%: Html.DisplayFor(model => model.Project.UnitName) %></td>        
        <% if (!Model.Project.IsPalsProject)
           { %>
        <td class="text" colspan="4">&nbsp;</td>
        <% } %>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.Description) %>:</td>
        <td class="text markdown" colspan="5"><%: Html.DisplayFor(model => model.Project.Description)%></td>
    </tr>
    <% if (Model.Project.IsPalsProject)
       { %>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Activities)%>:</td>
        <td class="text" colspan="5"><%: Html.DisplayFor(model => model.Activities)%></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Purposes)%>:</td>
        <td class="text" colspan="5"><%: Html.DisplayFor(model => model.Purposes)%></td>
    </tr>
    <% } %>
    <tr>
        <% if (Model.Project.IsPalsProject)
           { %>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Project.AnalysisTypeName) %>:</td>
        <td class="text"><%: Html.DisplayFor(model => model.Project.AnalysisTypeName)%></td>
        <% } %>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Project.LastUpdated)%>:</td>
        <td class="text"><%: Html.DisplayFor(model => model.Project.LastUpdated)%></td>
        <td colspan="2"></td>
        <% if (!Model.Project.IsPalsProject)
           { %>
        <td class="text" colspan="2">&nbsp;</td>
        <% } %>
    </tr>
    <% if (Model.Project.IsPalsProject)
       { %>
    <tr>
        <td class="fieldLabel" valign="top">Location:</td>
        <td class="text" colspan="5">
        <% ICollection<string> areaIds = new List<string>(); %>
        <!-- First display all regions -->
        <% foreach (Location region in Model.Locations.Where(l => l.AreaTypeSpecific == Location.AreaTypeEnum.Region).OrderBy(x => x.Area))
           {
               areaIds.Add(region.AreaId);%>
            <div style="border: 1px solid gray;padding: 2px;"><div style="padding-bottom:2px;"><%= region.Area%></div>
            <% foreach (Location forest in Model.Locations.Where(l => l.AreaTypeSpecific == Location.AreaTypeEnum.Forest && l.AreaId.StartsWith(region.AreaId)).OrderBy(x => x.Area)) 
               {
                   areaIds.Add(forest.AreaId);%>
                <div style="padding-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;<%= forest.Area%></div>
                <% foreach (Location district in Model.Locations.Where(l => l.AreaTypeSpecific == Location.AreaTypeEnum.District && l.AreaId.StartsWith(forest.AreaId)).OrderBy(x => x.Area)) 
                   {
                       areaIds.Add(district.AreaId);%>
                    <div style="padding-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= district.Area%></div>
                <% } %>
            <% } %>
            </div><br />
        <% } %>
        <!-- Then find all orphan forests -->
        <% ICollection<Location> orphans = Model.Locations.Where(l => !areaIds.Contains(l.AreaId)).ToList();
           foreach (Location forest in orphans.Where(l => l.AreaTypeSpecific == Location.AreaTypeEnum.Forest).OrderBy(x => x.Area))
           {
               areaIds.Add(forest.AreaId);%>
                <div style="border: 1px solid gray;padding: 2px;"><div style="padding-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;<%= forest.Area%></div>
                <% foreach (Location district in orphans.Where(l => l.AreaTypeSpecific == Location.AreaTypeEnum.District && l.AreaId.StartsWith(forest.AreaId)).OrderBy(x => x.Area)) 
                   {
                       areaIds.Add(district.AreaId);%>
                    <div style="padding-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= district.Area%></div>
                <% } %>
                </div><br />
        <% } %>

        <!-- Then find all orphan forests -->
        <% orphans = Model.Locations.Where(l => !areaIds.Contains(l.AreaId)).ToList();
           foreach (Location district in orphans.Where(l => l.AreaTypeSpecific == Location.AreaTypeEnum.District).OrderBy(x => x.Area))
           {
               areaIds.Add(district.AreaId);%>
               <div style="border: 1px solid gray;padding: 2px;"><div style="padding-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= district.Area%></div></div>
               <br />
        <% } %>
        </td>
    </tr>
    <% } %>
</table>
</div>

<% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/Markdown.Converter.js")
                .Add("~/Scripts/cara.markdown.js")); %>
<% } %>