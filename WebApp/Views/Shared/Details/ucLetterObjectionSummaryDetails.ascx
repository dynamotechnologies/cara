﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>
<%
if (Model.IsObjection)
{ %>
    <div style="width=100%">&nbsp;</div>
    <div class="t-widget t-grid">
        <table>
            <tr>
                <td class="t-header" colspan="4">Objection Response</td>
            </tr>
            <tr>
                <td class="fieldLabel" width="20%">Reviewing Officer:</td>
                <td width="30%">
                    <%: Html.DisplayFor(m => m.Letter.LetterObjection.SignerFirstName) %>
                    <%: Html.DisplayFor(m => m.Letter.LetterObjection.SignerLastName) %>,
                    <%: Html.DisplayFor(m => m.Letter.LetterObjection.ObjectionDecisionMakerName)%>
                </td>
                <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.LetterObjection.ResponseDate) %>:</td>
                <td><%: Html.DisplayFor(m => m.Letter.LetterObjection.ResponseDate) %></td>
            </tr>
            <tr class="t-alt">
                <td class="fieldLabel" width="20%">Attachments:</td>
                <td colspan="3">
                    <% Html.RenderPartial("~/Views/Shared/List/ucObjectionDocumentList.ascx", new ObjectionDocumentViewModel { Documents = Model.Letter.LetterObjection.Documents }); %>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel" width="20%">Public Response:</td>
                <td colspan="3">
                    <% if (!string.IsNullOrEmpty(Model.Letter.LetterObjection.DmdId))
                       { %>
                    <%: Html.ActionLink("Download", "DownloadDmd", "Document", new { id = Model.Letter.LetterObjection.DmdId }, null) %>
                    <% }
                       else
                       { %> Not Available <% } %>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <a id="lnkObjectionResponse" class="button" href="#" onclick="javascript:openWindow('WindowObjectionResponse', '/Project/<%=Model.Letter.Phase.ProjectId%>/Phase/<%=Model.Letter.PhaseId%>/Letter/ObjectionResponse/<%=Model.Letter.LetterObjection.LetterObjectionId %>')">Edit Objection Response</a >
    <br />&nbsp;
                    
    <br />
<% 
} %>

<h3>Responses</h3>
<table>
    <tr>
        <td align="right"><%: Html.DropDownList("ExportOptions", Model.ObjectionSummaryReport.ExportOptions) %>
            &nbsp;
            <a class="button" onclick="exportReport('<%= Url.Action("ExportReports", "PhaseReport", new {id = 12, option = Model.LetterId, exportType="~"} ) %>');">Export</a >
        </td>
    </tr>
    <tr>
        <td><% Html.RenderPartial("~/Views/Shared/List/StandardReports/ucCommentsAndResponsesByLetter.ascx", Model.ObjectionSummaryReport); %></td>
    </tr>
</table>