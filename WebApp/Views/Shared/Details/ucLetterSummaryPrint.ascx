﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>

<% if (Model != null)
   { %>
    <a class="button" href="#" onclick="printSummary()"><%=Constants.PRINT %></a>
    <a class="button" href="#" onclick="closeWindow('WindowSummary')"><%=Constants.CLOSE%></a>
    <br /><br />
    <div id="letterSummaryPrint">
        <table width="900">
            <tr><td class="sectionTitle" colspan="2"><%=Constants.LETTER%></td></tr>
            <tr>
                <td valign="top" width="25%">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.AuthorNameList)%>:</span><br />
                    <span class="text"><%: Html.DisplayFor(model => model.Letter.AuthorNameList)%></span>
                </td>
                <td class="text" valign="top" width="25%">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.Sender.Organization.Name)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.Sender.Organization.Name)%>
                    <% if (Model.Letter.Sender.OfficialRepresentativeTypeId.HasValue)
                       { %>
                        <br />(<%: Html.DisplayFor(model => model.Letter.Sender.OfficialRepresentativeTypeName)%>)
                    <% } %>
                </td>
                <td class="text" valign="top" width="25%">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.Sender.Organization.OrganizationTypeName)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.Sender.Organization.OrganizationTypeName)%>
                </td>
                <td class="text" valign="top">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.LetterTypeName)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.LetterTypeName)%>
                </td>
            </tr>
            <tr>
                <td class="text" valign="top" width="25%">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.Sender.Email)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.Sender.Email)%>
                </td>
                <td class="text" valign="top" width="25%">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.Sender.FullAddress)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.Sender.FullAddress)%>
                </td>
                <td class="text" valign="top" width="25%">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.EarlyActionStatus)%>:</span><br />
                    <%=Model.Letter.EarlyActionStatus%>
                </td>
                <td class="text" valign="top">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DateSubmitted)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.DateSubmitted)%>
                </td>
                <td valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="text" valign="top">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.LetterStatusName)%>:</span><br />
                    <div id="LetterStatusName"><%: Html.DisplayFor(model => model.Letter.LetterStatusName)%></div>
                </td>
                <td class="text" valign="top">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DeliveryTypeName)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.DeliveryTypeName)%>
                </td>
                <td class="text" valign="top">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.CommonInterestClassName)%>:</span><br />
                    <%: Html.DisplayFor(model => model.Letter.CommonInterestClassName)%>
                </td>
                <td class="text" valign="top">
                    <span class="fieldLabel"><%: Html.LabelFor(model => model.Letter.WithinCommentPeriod)%>:</span><br />
                    <%= Model.Letter.WithinCommentPeriod.ConvertToYesNo()%>
                </td>
            </tr>
        </table>
        <br />
        <div style="width: 500px">
            <span class="fieldLabel">&nbsp;&nbsp;Attachments</span>
            <% Html.RenderPartial("~/Views/Shared/List/ucDocumentList.ascx", new DocumentViewModel
               {
                   Documents = Model.Letter.Documents,
                   ShowButtons = false,
                   HideDownloadAllButton = true,
                   LetterId = Model.LetterId,
                   ShowActions = false
               }); %>
        </div>
    </div>
    <br />
    <a class="button" href="#" onclick="printSummary()"><%=Constants.PRINT %></a>
    <a class="button" href="#" onclick="closeWindow('WindowSummary')"><%=Constants.CLOSE%></a> 
    <br /><br />
<% } %>