﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseDetailsViewModel>" %>

<% if (Model != null)
   { %>
<div class="t-widget t-grid">
    <table>
        <tr>
            <td class="fieldLabel" width="225px">Date:</td>
            <td class="text"><%:Html.DisplayFor(model => model.Phase.CommentStart)%>&nbsp;-&nbsp;<% if (!Model.Phase.AlwaysOpen)
                                                                                                    { %><%=Html.DisplayFor(model => model.Phase.CommentEnd)%><% }
                                                                                                    else
                                                                                                    { %> N/A <% } %>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">Time Zone: </td>
            <td class="text">
                <span id="timeZone"><%:Html.DisplayFor(model => model.PhaseTimeZone)%></span>
                <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="TimeZoneToolTip" title="Comments close at midnight in the selected time zone." />                
            </td>
        </tr>

      <% if (Model.IsObjection)
         { %>
        <tr>
            <td class="fieldLabel"><%:Html.LabelFor(model => model.Phase.ObjectionResponseDue)%>:</td>
            <td class="text"><span id="responseDueDate"><%:Html.DisplayFor(model => model.Phase.ObjectionResponseDue)%></span></td>
        </tr>
        <tr>
            <td class="fieldLabel"><%:Html.LabelFor(model => model.Phase.ObjectionResponseExtendedBy)%>:</td>
            <td class="text">
                <span id="extendedInDays"><%:Html.DisplayFor(model => model.Phase.ObjectionResponseExtendedBy)%></span> Days
                <%--<a class="button" href="#" onclick="$('#extendResponseBy').val($('#extendedInDays').text());openWindow('ExtendResponseDueDate')"><%:Constants.EDIT%></a > --%>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">New Response Due Date:</td>
            <td class="text"><span id="newResponseDueDate"><%:Html.DisplayFor(model => model.NewObjectionResponseDue)%></span></td>
        </tr>
      <% } %>
        <tr>
            <td class="fieldLabel"><%:Html.LabelFor(model => model.Phase.Name)%>:</td>
            <td class="text"><%:Html.DisplayFor(model => model.Phase.PhaseTypeDesc)%></td>
        </tr>
        <tr>
            <td class="fieldLabel"><%:Html.LabelFor(model => model.Phase.StatusName)%>:</td>
            <td class="text">
                <%:Html.DisplayFor(model => model.Phase.StatusName)%>&nbsp;
                <% if (Utilities.UserHasPrivilege("ARPH", Constants.PHASE))
                   { %>
                   <% if (Constants.ARCHIVED.Equals(Model.Phase.StatusName))
                      { %>
                       <a class="button" href="#" onclick="confirmWindow('Activate the Comment Period', 'activatePhase(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')"><%:Constants.ACTIVATE%></a >
                   <% } %>
                   <% if (Constants.ACTIVE.Equals(Model.Phase.StatusName))
                      { %>
                        <a class="button" href="#" onclick="confirmWindowSize('An archived comment period signals the end and acceptance of all activities associated with this comment period.  All associated comment and response information, including annotations, will become read-only and will be saved as part of the project record.  The Viewing Status of the Comment period will be \'All CARA Users\', even if the current setting is \'Team Only\'. Are you sure you want to archive the comment period', 'archivePhase(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)', '', 520, 100)"><%=Constants.ARCHIVE%></a >
                   <% } %>
                <% } %>
                <% if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE) && Model.CanDelete)
                   { %>
                    <a class="button" href="#" onclick="confirmWindow('Comment Period will be deleted. Do you wish to continue', 'deletePhase(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')"><%:Constants.DELETE%></a >
                <% } %>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><%:Html.LabelFor(model => model.Phase.AccessStatusName)%>:</td>
            <td class="text">
                <%:Html.DisplayFor(model => model.Phase.AccessStatusName)%>&nbsp;
                <% if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                   <% if ("All CARA Users".Equals(Model.Phase.AccessStatusName))
                      { %>
                       <a class="button" href="#" onclick="confirmWindow('Set to Team Only', 'setToTeamOnly(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')">Set to Team Only</a >
                   <% } %>
                   <% if ("Team Only".Equals(Model.Phase.AccessStatusName))
                      { %>
                       <a class="button" href="#" onclick="confirmWindow('Set to All CARA Users', 'setToAll(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')">Set to All CARA <%:Utilities.GetResourceString(Constants.USER).Pluralize() %></a >
                   <% } %>
                <% } %>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><%:Utilities.GetResourceString(Constants.READING_ROOM)%> Status:</td>
            <td class="text">
                <%:Html.DisplayFor(model => model.Phase.ReadingRoomActiveName)%>&nbsp;
                <% if (Utilities.UserHasPrivilege("ACRR", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                   <% if (!Model.Phase.ReadingRoomActive)
                      { %>
                        <a class="button" href="#" onclick="confirmWindow('Activate the Reading Room', 'activateReadingRoom(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')"><%:Constants.ACTIVATE %></a >
                    <% }
				      else
				      { %>
					    <i>(<%: Html.ActionLink(Utilities.GetResourceString(Constants.PUBLIC_READING_ROOM), Constants.READING_ROOM, Constants.PUBLIC, new { project = Model.ProjectNumber }, new { target = "_blank" })%>)</i>&nbsp;
                        <a class="button" href="#" onclick="confirmWindow('Deactivate the Reading Room', 'deactivateReadingRoom(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')"><%:Constants.DEACTIVATE %></a >
                    <% } %>
                <% } %>            
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><%:Utilities.GetResourceString(Constants.PUBLIC_COMMENT_INPUT)%> Status:</td>
            <td class="text">
                <%:Html.DisplayFor(model => model.Phase.PublicCommentActiveName)%>&nbsp;
                <% if (Utilities.UserHasPrivilege("ACPC", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                   <% if (!Model.Phase.PublicCommentActive)
                      { %>
                        <a class="button" href="#" onclick="confirmWindowSize('Please make sure to double check your comment period dates when activating your webform.<br/><br/>Are you sure you wish to continue', 'activateCommentInput(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)', null, 300, 90)"><%:Constants.ACTIVATE %></a >
                    <% }
				      else
				      { %>
					    <i>(<%: Html.ActionLink(Utilities.GetResourceString(Constants.PUBLIC_COMMENT_INPUT), Constants.COMMENT_INPUT, Constants.PUBLIC, new { project = Model.ProjectNumber }, new { target = "_blank" })%>)</i>&nbsp;
                        <a class="button" href="#" onclick="confirmWindow('Deactivate the Public Comment Form', 'deactivateCommentInput(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)')"><%:Constants.DEACTIVATE %></a >
                    <% } %>
                    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="When the status is Active, the Public Comment Form will only be visible between <%:Html.DisplayFor(model => model.Phase.CommentStart)%> - <%=Html.DisplayFor(model => model.Phase.CommentEnd)%>." title="When the status is Active, the Public Comment Form will only be visible between <%:Html.DisplayFor(model => model.Phase.CommentStart)%> - <%=Html.DisplayFor(model => model.Phase.CommentEnd)%>." />
                <% } %>            
            </td>
        </tr>
        <% if (Model.CanDownloadLetters)
           { %>
        <tr>
            <td class="fieldLabel">Download All Letters:</td>
            <td><a class="button" href="#" onclick="requestAllLetters(<%=Model.Phase.ProjectId %>, <%=Model.Phase.PhaseId %>)">Download</a></td>
        </tr>
        <% } %>
     </table>
 </div>
 <br />
  <% Html.Telerik().TabStrip()
        .Name("Section")
        .Items(tabstrip =>
        {
            tabstrip.Add()
                .Text(Utilities.GetResourceString("TeamMember").Pluralize())
                .Content(() =>
                {%>
                <div class="tabArea">
                <% Html.RenderPartial("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel { Members = Model.Phase.PhaseMemberRoles, ShowButtons = false, IsPalsProject = !Model.ProjectNumber.StartsWith("NP") }); %>
                <br />
                <% if (Utilities.UserHasPrivilege("MTM", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                <%: Html.ActionLink(Constants.MANAGE, "MemberInput", Constants.PHASE, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = PageMode.Edit }, new { @class = "button" })%>
                <% } %>
                </div>
            <%}).Selected(true);

            tabstrip.Add()
                .Text(Utilities.GetResourceString(Constants.LETTER).Pluralize())
                .Content(() =>
                {%>
                <div class="tabArea">
					<%
			        Html.Telerik().Grid(Model.LetterStatus)
						.Name(Constants.LETTER + Constants.STATUS)
						.Columns(columns =>
							{
                                //columns.Bound(info => info.Label).Title("By " + Utilities.GetResourceString(Constants.LETTER) + " " + Constants.STATUS);
                                columns.Bound(info => info.Label).Title("By " + Constants.CODING + " " + Constants.STATUS);                                
								columns.Bound(info => info.Count).Title("Count").HtmlAttributes(new { align = "right" }).Template(info =>
									{
										if (info.Count == 0)
										{
											%><%: info.Count %><%
										}
										else
										{
											%><a href="<%: info.ActionLink %>"><%: info.Count %></a><%
										}
									}
								);
							}
						)
						.HtmlAttributes(new { style = "width:300px;" })
						.Footer(false)
						.Render();
					%>
                    	<br />
					<%
			        Html.Telerik().Grid(Model.EarlyActionStatus)
                        .Name(Constants.LETTER + Constants.STATUS)
						.Columns(columns =>
							{
                                columns.Bound(info => info.Label).Title("By Early Attention");
								columns.Bound(info => info.Count).Title("Count").HtmlAttributes(new { align = "right" }).Template(info =>
									{
										if (info.Count == 0)
										{
											%><%: info.Count %><%
										}
										else
										{
											%><a href="<%: info.ActionLink %>"><%: info.Count %></a><%
										}
									}
								);
							}
						)
						.HtmlAttributes(new { style = "width:300px;" })
						.Footer(false)
						.Render();
					%>
					<br />
					<%
			        Html.Telerik().Grid(Model.LetterType)
						.Name(Constants.LETTER + "Type")
						.Columns(columns =>
							{
								columns.Bound(info => info.Label).Title("By " + Utilities.GetResourceString(Constants.LETTER) + " Type");
								columns.Bound(info => info.Count).Title("Count").HtmlAttributes(new { align = "right" }).Template(info =>
									{
										if (info.Count == 0)
										{
											%><%: info.Count %><%
										}
										else
										{
											%><a href="<%: info.ActionLink %>"><%: info.Count %></a><%
										}
									}
								);
							}
						)
						.HtmlAttributes(new { style = "width:300px;" })
						.Footer(false)
						.Render();
					%>
                <br />
                <%: Html.ActionLink("View All " + Utilities.GetResourceString(Constants.LETTER).Pluralize(), Constants.LIST, Constants.LETTER, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, filter = "All" }, new { @class = "button" })%>&nbsp;
                <% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE) && Model.Phase.IsActive)
                   { %>                    
                    <%: Html.ActionLink("Enter New " + Utilities.GetResourceString(Constants.LETTER), Constants.CREATE, Constants.LETTER, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId }, new { @class = "button" })%>&nbsp;
                    <%: Html.ActionLink(Utilities.GetPageTitle(Constants.LETTER + Constants.UPLOAD + Constants.LETTERS), Constants.UPLOAD + Constants.LETTERS, Constants.LETTER, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId }, new { @class = "button" })%>
                <% } %>
                </div>
            <%});

            tabstrip.Add()
                .Text(Utilities.GetResourceString(Constants.COMMENT).Pluralize())
                .Content(() =>
                {%>
                <div class="tabArea">
					<%
			        Html.Telerik().Grid(Model.CommentCodeCategories)
						.Name(Constants.CODE + "Category")
						.Columns(columns =>
							{
								columns.Bound(info => info.Label).Title("By " + Utilities.GetResourceString(Constants.COMMENT) + " Code Category");
								columns.Bound(info => info.Count).Title("Count").HtmlAttributes(new { align = "right" }).Template(info =>
									{
										if (info.Count == 0)
										{
											%><%: info.Count %><%
										}
										else
										{
											%><a href="<%: info.ActionLink %>"><%: info.Count %></a><%
										}
									}
								);
							}
						)
						.HtmlAttributes(new { style = "width:300px;" })
						.Footer(false)
						.Render();
					%>
                <br />
                <%: Html.ActionLink("View All " + Utilities.GetResourceString(Constants.COMMENT).Pluralize(), Constants.LIST, Constants.COMMENT, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = Constants.CREATE }, new { @class = "button" })%>
                </div>
            <%});

            tabstrip.Add()
                .Text(Utilities.GetResourceString("Concerns"))
                .Content(() =>
                {%>
                <div class="tabArea">
					<%
			        Html.Telerik().Grid(Model.ConcernResponseStatus)
						.Name(Utilities.GetResourceString(Constants.CONCERN) + "Status")
						.Columns(columns =>
							{
								columns.Bound(info => info.Label).Title("By " + Utilities.GetResourceString(Constants.CONCERN) + " Status");
								columns.Bound(info => info.Count).Title("Count").HtmlAttributes(new { align = "right" }).Template(info =>
									{
										if (info.Count == 0)
										{
											%><%: info.Count %><%
										}
										else
										{
											%><a href="<%: info.ActionLink %>"><%: info.Count %></a><%
										}
									}
								);
							}
						)
						.HtmlAttributes(new { style = "width:300px;" })
						.Footer(false)
						.Render();
					%>
                <br />
                <%: Html.ActionLink("View All " + Utilities.GetResourceString("Concerns"), Constants.LIST, Constants.CONCERN, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = Constants.CREATE }, new { @class = "button" })%>
                <% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && Model.Phase.IsActive)
                   { %>
                <%: Html.ActionLink(Constants.CREATE + " " + Utilities.GetResourceString("Concern"), Constants.LIST, Constants.COMMENT, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = Constants.CREATE }, new { @class = "button" })%>
                <% } %>
                </div>
            <%});

            tabstrip.Add()
                .Text(Utilities.GetResourceString("CodingStructure"))
                .Content(() =>
                {%>
                <div class="tabArea" style="width:685px">
                <% Html.RenderPartial("~/Views/Shared/Details/ucPhaseCodesDetails.ascx", new PhaseCodesViewModel { PhaseCodes = Model.Phase.PhaseCodes }); %>
                <br />
                <% if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                <%: Html.ActionLink(Constants.MANAGE, "CodeInput", Constants.PHASE, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = PageMode.Edit }, new { @class = "button" })%>
                <% } %>
                </div>
            <%});

            tabstrip.Add()
                .Text(Utilities.GetResourceString(Constants.DOCUMENT).Pluralize())
                .Content(() =>
                {%>
                <div class="tabArea">
                <% Html.RenderPartial("~/Views/Shared/List/ucDmdDocumentList.ascx", new DmdDocumentViewModel { Documents = Model.Phase.Documents, ShowButtons = false }); %>
                <br />
                <% if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                <%: Html.ActionLink(Constants.MANAGE, "DocumentInput", Constants.PHASE, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = PageMode.Edit }, new { @class = "button" })%>
                <% } %>
                </div>
            <%});

            tabstrip.Add()
                .Text(Utilities.GetResourceString(Constants.TERM_LIST))
                .Content(() =>
                {%>
                <div class="tabArea" style="width:685px">
                <% Html.RenderPartial("~/Views/Shared/List/ucTermListList.ascx", Model.Phase.TermLists); %>
                <br />
                <% if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE) && !Model.Phase.Locked)
                   { %>
                <%: Html.ActionLink(Constants.MANAGE, "TermListInput", Constants.PHASE, new { projectId = Model.Phase.ProjectId, phaseId = Model.Phase.PhaseId, mode = PageMode.Edit }, new { @class = "button" })%>
                <% } %>
                </div>
            <%});
        }).Render();%>
<% } %>

    <% Html.Telerik().Window()
        .Name("ExtendResponseDueDate")
        .Title("Extend Response Due Date")
        .Draggable(true)
        .Modal(true)
        .Content(() =>
                {
                    %>
                    <br />
                    <div id="ExtensionError" class="error">&nbsp;</div>
                    <table>
                        <tr>
                            <td><span class="fieldLabel">Response Due:</span></td>
                            <td>
                                <%:Html.DisplayFor(model => model.Phase.ObjectionResponseDue)%>                                
                            </td>
                        </tr>
                        <tr>
                            <td><span class="fieldLabel">Extend Response By:</span></td>
                            <td><input type="text" id="extendResponseBy" value="" size="5" style="width: 40px"/> Day(s)</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input type="button" class="button" value="Update" onclick="updateObjectionResponseExtensionDate(<%:Model.Phase.ProjectId %>, <%:Model.Phase.PhaseId %>)" />
                                <input type="button" class="button" value="Cancel" onclick="closeWindow('ExtendResponseDueDate')" />
                            </td>
                        </tr>
                    </table>
                    <%
                })
        .Buttons(b => b.Close())
        .Width(325)
        .Height(150)
        .Visible(false)
        .Render(); %>
