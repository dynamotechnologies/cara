﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>
<br />
<div style="width=100%">&nbsp;</div>
<div class="t-widget t-grid">
    <table>
        <tr>
            <td class="t-header" colspan="4">Letter Details</td>
        </tr>
        <tr>
            <td class="fieldLabel" width="20%"><%: Html.LabelFor(m => m.Letter.LetterSequence) %>:</td>
            <td width="30%"><%: Html.DisplayFor(m => m.Letter.LetterSequence) %></td>
            <td class="fieldLabel" width="20%"><%: Html.LabelFor(m => m.Letter.DateSubmitted)%>:</td>
            <td width="30%"><%: Html.DisplayFor(m => m.ConvertedDateSubmitted)%></td>
        </tr>
        <tr class="t-alt">
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.LetterStatusName) %>:</td>
            <td><span id="LetterStatusNameDetails"><%: Html.DisplayFor(m => m.Letter.LetterStatusName) %></span></td>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.LetterTypeName) %>:</td>
            <td>
                <%: Html.DisplayFor(m => m.Letter.LetterTypeName) %>
                <% if (Model.Letter.LetterTypeId != 1 && Model.Letter.LetterTypeId != 3 && Model.Letter.LetterTypeId != 9)
                   { %>&nbsp;<a class="button" href="#" onclick="javascript:openWindow('MakeDuplicate')">Make&nbsp;Duplicate</a> <% } %>
                <br />                                               

                <% if (Model.Letter.FormSetId.HasValue)
                   { %> (<a href="../Form/LetterList?FormSet=<%: Model.Letter.FormSetId %>"><%: Model.FormSet.Name %></a>) <% } %>
                <% if (Model.Letter.MasterDuplicateId.HasValue)
                   { %> (<a href="<%= Url.Action(Constants.CODING, Constants.LETTER, new { id = Model.Letter.MasterDuplicateId }) %>">Original Letter #<%: Model.Letter.MasterDuplicate.LetterSequence %></a>) <% } %>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.PublishToReadingRoomName)%>:</td>
            <td>
                <%: Html.DisplayFor(m => m.Letter.PublishToReadingRoomName) %>
                <% if (!string.IsNullOrWhiteSpace(Model.Letter.UnpublishedReasonName))
                   { %>
                       (<%: Html.DisplayFor(m => m.Letter.UnpublishedReasonName)%><% if (!string.IsNullOrWhiteSpace(Model.Letter.UnpublishedReasonOther))
                                                                                  { %>: <%: Html.DisplayFor(m => m.Letter.UnpublishedReasonOther)%><% } %>)
                <% } %>
                <% if (Model.Publishable) { %>&nbsp;<a class="button" href="#" onclick="confirmUpdatePublishStatus('<%=!Model.Letter.PublishToReadingRoom %>', '<%= Model.Letter.IsMasterForm %>')"><%: Model.Letter.PublishToReadingRoom ? Constants.UNPUBLISH : Constants.PUBLISH%></a><% } %>
            </td>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.EarlyActionStatusId) %>:</td>
            <td><%: Html.DisplayFor(m => m.Letter.EarlyActionStatus) %></td>
        </tr>
        <tr class="t-alt">
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.DeliveryTypeName)%></td>
            <td><%: Html.DisplayFor(m => m.Letter.DeliveryTypeName)%></td>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.WithinCommentPeriod) %>:</td>
            <td><%: Model.Letter.WithinCommentPeriod.ConvertToYesNo()%></td>
        </tr>
    </table>
</div>

<br />

<div class="t-widget t-grid">
    <table>
        <tr>
            <td class="t-header" colspan="4">Author Details</td>
        </tr>
        <tr>
            <td class="fieldLabel" width="20%"><%: Html.LabelFor(m => m.Sender.FullName)%>:</td>
            <td width="30%">
                <%: Html.DisplayFor(m => m.Sender.FullName) %><br />
            </td>
            <td class="fieldLabel" width="20%"><%: Html.LabelFor(m => m.Sender.Organization.Name)%>:</td>
            <td width="30%"><%: Html.DisplayFor(m => m.Sender.Organization.Name)%></td>
        </tr>
        <tr class="t-alt">
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Sender.FullAddress)%>:</td>
            <td><%: Html.DisplayFor(m => m.Sender.FullAddress) %></td>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Sender.Title)%>:</td>
            <td><%: Html.DisplayFor(m => m.Sender.Title)%></td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Sender.Email)%>:</td>
            <td><%: Html.DisplayFor(m => m.Sender.Email) %></td>
            <td class="fieldLabel">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>
<br />
<a id="lnkAuthorList" class="button" href="#" onclick="javascript:openWindow('WindowAuthors', '/Project/<%=Model.Letter.Phase.ProjectId%>/Phase/<%=Model.Letter.PhaseId%>/Letter/AuthorList/<%=Model.Letter.LetterId%>')">View Complete Author List</a >
<br />&nbsp;

<% if (Model.Duplicates != null && Model.Duplicates.Count > 0)
{
%>   
<br />
<div class="t-widget t-grid">
    <table>
        <tr>
            <td class="t-header">Duplicates</td>
        </tr>
        <tr>
          <td>
        <% Html.Telerik().Grid(Model.Duplicates)
            .Name(Constants.DUPLICATE + Constants.LIST)
            .Columns(columns =>
            {
                columns.Bound(dup => dup.LetterSequence).Template(dup =>
                    {%><a href="<%= Url.Action(Constants.CODING, Constants.LETTER, new { id = dup.LetterId }) %>"><%= dup.LetterSequence%></a ><%});

                columns.Bound(dup => dup.LetterId).Template(dup =>
                { %>
                       <a href="#" onclick="javascript:openWindow('WindowCompare', '/Project/<%=Model.Letter.Phase.ProjectId%>/Phase/<%=Model.Letter.PhaseId%>/Letter/CompareMaster/<%=dup.LetterId%>?targetLetterId=<%=Model.Letter.LetterId%>&navigate=false')" onclick-temp="openCompareMasterWindow(<%=dup.LetterId%>, <%=dup.MasterDuplicate%>, true);return false;"><img src="<%= ResolveUrl("~/Content/Images/icon_compare.png")%>" alt="Compare to Master Duplicate" title="Compare to Master Duplicate" /></a >
             <% }).Title(Constants.COMPARE).HtmlAttributes(new { align = "center" });

                columns.Bound(dup => dup.LetterId).Template(dup =>
                { %>
                   <%: Html.ActionLink(Constants.DELETE, Constants.DELETE, new { id = dup.LetterId }, new { @class = "button", id = "btnDeleteDuplicate" + dup.LetterId, onclick = "return confirmWindow(\"Are you sure you want to permanently delete this duplicate letter\", \"deleteLetter(" + Model.Letter.LetterId + ", " + dup.LetterId +")\")" }) %>
             <% }).Title(Constants.DELETE).HtmlAttributes(new { align = "center" });
            })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
            .Footer(false)
            .HtmlAttributes(new { style = "width: 700px;" })
            .Render(); %>
          </td>
        </tr>
    </table>
</div>
<br />
<%: Html.ActionLink("Delete All Duplicates", Constants.DELETE + Constants.DUPLICATE.Pluralize(), new { id = Model.Letter.LetterId }, new { @class = "button", id = "btnDeleteDuplicates", onclick = "return confirmWindow(\"Are you sure you want to permanently delete all duplicates of this letter\", \"deleteDuplicates()\")" }) %>
<br />&nbsp;
<%
}
%>
<br />

<% if (Model.IsObjection && Model.Letter.LetterObjectionId.HasValue)
{
%>   

<div class="t-widget t-grid">
    <table>
        <tr>
            <td class="t-header" colspan="2">Objection Information</td>
        </tr>
        <tr>
            <td class="fieldLabel" width="30%"><%: Html.LabelFor(m => m.ObjectionInfo.ObjectionId)%>:</td>
            <td><%: Html.DisplayFor(m => m.ObjectionInfo.ObjectionId)%></td>
        </tr>
        <tr class="t-alt">
            <td class="fieldLabel"><%: Html.LabelFor(m => m.ObjectionInfo.ReviewStatusId)%>:</td>
            <td><%: Html.DisplayFor(m => m.ObjectionInfo.ReviewStatusName)%></td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.ObjectionInfo.OutcomeId)%>:</td>
            <td><%: Html.DisplayFor(m => m.ObjectionInfo.OutcomeName)%></td>
        </tr>
        <tr class="t-alt">
            <td class="fieldLabel"><%: Html.LabelFor(m => m.ObjectionInfo.MeetingInfo)%>:</td>
            <td><%: Html.DisplayFor(m => m.ObjectionInfo.MeetingInfo)%></td>
        </tr>
    </table>
    <%: Html.HiddenFor(m => m.ObjectionInfo.SignerFirstName) %>
    <%: Html.HiddenFor(m => m.ObjectionInfo.SignerLastName) %>
    <%: Html.HiddenFor(m => m.ObjectionInfo.ObjectionDecisionMakerId) %>
    <%: Html.HiddenFor(m => m.ObjectionInfo.ResponseDate) %>
    <%: Html.HiddenFor(m => m.NextLetterIds) %>
    <%: Html.HiddenFor(m => m.LettersRemaining) %>
</div>
    <% if (Model.Editable && Model.Letter.LetterObjectionId != null)
    {
    %>
        <br />
        <a class="button" href="#" onclick="javascript:openWindow('WindowObjectionInfo', '/Project/<%=Model.Letter.Phase.ProjectId%>/Phase/<%=Model.Letter.PhaseId%>/Letter/ObjectionInfo/<%=Model.Letter.LetterObjection.LetterObjectionId%>')">Edit Objection Information</a>
        <br />&nbsp;
    <%
    }
    %>
<%
}
%>

<%        Html.Telerik().Window()
            .Name("WindowAuthors")
            .Title(string.Format("{0} {1}", Constants.AUTHOR, Constants.LIST))
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(true)
                            .MinHeight(250)
                            .MinWidth(250)
                            .MaxWidth(1200)
                        )
            .Modal(true)
            .Buttons(b => b.Maximize().Close())
            .Width(975)
            .Height(300)
            .Visible(false)
            .Render();
 %>

<%        Html.Telerik().Window()
            .Name("WindowObjectionInfo")
            .Title(string.Format("{0} {1}", Constants.EDIT, "Objection Information"))
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(false)
                        )
            .Modal(true)
            .Buttons(b => b.Maximize().Close())
            .Width(600)
            .Height(325)
            .Visible(false)
            .Render();
 %>

 <%        Html.Telerik().Window()
            .Name("WindowUnpublish")
            .Title(string.Format("{0} {1}", Constants.UNPUBLISH, Constants.LETTER))
            .Draggable(false)
            .Modal(true)
            .Buttons(b => b.Maximize().Close())
            .Width(500)
            .Height(200)
            .Visible(false)
            .Render();
 %>

 <%        Html.Telerik().Window()
            .Name("PleaseWait")
            .Title("Please Wait")
            .Draggable(false)
            .Modal(true)
            .Content(() =>
                {
                    %>
                    <div style="position: absolute; top: 50%; left: 50%; margin-right: -50%; transform: translate(-50%, -50%)">
                      <img src="/Content/Images/pleasewait.gif" alt="Please Wait" />
                    </div>
                    <%
                })
            .Width(100)
            .Height(100)
            .Visible(false)
            .Render();
%>

<%        Html.Telerik().Window()
            .Name("MakeDuplicate")
            .Title("Make Duplicate")
            .Draggable(false)
            .Modal(true)
            .LoadContentFrom("MakeDuplicate", "Letter", new { projectId = Model.ProjectId, phaseId = Model.PhaseId, id = Model.LetterId, nextLetterNums = Model.NextLetterIds, numberOfLettersRemaining = Model.LettersRemaining })
            .Width(400)
            .Height(100)
            .Visible(false)
            .Render();
 %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.letterdetails.js")
            .Add("~/Scripts/cara.objectioninfoinput.js")
            .Add("~/Scripts/util.inputcountdown.js")
            .Add("~/Scripts/telerik.window.dialog.js")); %>