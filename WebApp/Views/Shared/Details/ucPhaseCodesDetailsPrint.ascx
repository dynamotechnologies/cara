﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseCodesViewModel>" %>

<script type="text/javascript">
<!--
    // print the summary
    function printCodeTree() {
        $("#codeTreePrint").printElement();

        return false;
    }
-->
</script>

<% if (Model != null)
   { %>
    <a class="button" href="#" onclick="printCodeTree()"><%=Constants.PRINT %></a>
    <a class="button" href="#" onclick="closeWindow('WindowPreview')"><%=Constants.CLOSE%></a>
    <br /><br />
    <div id="codeTreePrint">
        <% Html.RenderPartial("~/Views/Shared/Details/ucPhaseCodesDetails.ascx", Model); %>
    </div>
    <br />
    <a class="button" href="#" onclick="printCodeTree()"><%=Constants.PRINT %></a>
    <a class="button" href="#" onclick="closeWindow('WindowPreview')"><%=Constants.CLOSE%></a> 
    <br /><br />
<% } %>
