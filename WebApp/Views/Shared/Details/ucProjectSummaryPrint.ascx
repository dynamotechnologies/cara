﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.SummaryViewModel>" %>

<script type="text/javascript">
<!--
    // print the summary
    function printSummary() {
        $("#projectSummaryPrint").printElement();

        return false;
    }
-->
</script>

<% if (Model != null)
   { %>
    <a class="button" href="#" onclick="printSummary()"><%=Constants.PRINT %></a>
    <a class="button" href="#" onclick="closeWindow('WindowSummary')"><%=Constants.CLOSE%></a>
    <br /><br />
    <div id="projectSummaryPrint">
        <table width="900">
            <tr><td class="sectionTitle"><%=Utilities.GetResourceString(Constants.PROJECT)%></td></tr>
            <tr>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.ProjectNumber) %>:</td>
                <td class="text" valign="top"><%: Html.DisplayFor(model => model.Project.ProjectNumber)%></td>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.Name)%>:</td>
                <td class="text" valign="top"><%: Html.DisplayFor(model => model.Project.Name)%></td>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.UnitName)%>:</td>
                <td class="text" valign="top"><%: Html.DisplayFor(model => model.Project.UnitName)%></td>
            </tr>
            <tr>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.AnalysisTypeName)%>:</td>
                <td class="text" valign="top"><%: Html.DisplayFor(model => model.Project.AnalysisTypeName)%></td>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.ProjectStatusName)%>:</td>
                <td class="text" valign="top"><%: Html.DisplayFor(model => model.Project.ProjectStatusName)%></td>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.ProjectActivityNameList)%>:</td>
                <td class="text" valign="top"><%: Html.DisplayFor(model => model.Project.ProjectActivityNameList)%></td>
            </tr>
            <tr>
                <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.Description)%>:</td>
                <td class="text markdown" colspan="5"><%: Html.DisplayFor(model => model.Project.Description)%></td>
            </tr>
        </table>
        <br />
        <% Html.RenderPartial("~/Views/Shared/Details/ucPhaseSummaryPrint.ascx", Model.Phase); %>
    </div>
    <br />
    <a class="button" href="#" onclick="printSummary()"><%=Constants.PRINT %></a>
    <a class="button" href="#" onclick="closeWindow('WindowSummary')"><%=Constants.CLOSE%></a> 
    <br /><br />

    <% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/Markdown.Converter.js")
                .Add("~/Scripts/cara.markdown.js")); %>
<% } %>