﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PublicLetterViewModel>" %>

<% if (Model.Letter != null)
   { %>
    <table width="710" style="table-layout:fixed">
        <tr>
            <td colspan="2"><h1><%: Html.DisplayFor(m => m.Letter.Phase.Project.ProjectNameNumber) %></h1></td>
        </tr>
        <tr>
            <td class="fieldLabel" width="150" valign="top"><%: Html.LabelFor(m => m.Letter.Sender.FullName)%>:</td>
            <td class="text"><%: Html.DisplayFor(m => m.Letter.Sender.FullName)%></td>
        </tr>
        <tr>
            <td class="fieldLabel" valign="top"><%: Html.LabelFor(m => m.Letter.Sender.Organization.Name)%>:</td>
            <td class="text"><%: Html.DisplayFor(m => m.Letter.Sender.Organization.Name)%></td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.DateSubmitted)%>:</td>
            <td class="text"><%: Html.DisplayFor(m => m.Letter.DateSubmitted)%></td>
        </tr>
    </table>
<% }
   else
   { %>
    <div class="error"><%: string.Format("{0} cannot be found.", Utilities.GetResourceString(Constants.LETTER))%></div>
<% } %>