﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.TemplateCodeViewModel>" %>

<table>
    <tr>
        <td class="fieldLabel">Template Name:</td>
        <td class="text"><%: Html.DisplayFor(template => template.TemplateName) %></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top">List of Codes:</td>
        <td class="text">
        <% Html.Telerik().TreeView()
            .Name("TreeView")
            .ShowCheckBox(false)
            .ExpandAll(false)
            .BindTo(Model.TemplateCodeTree, mappings =>
                {
                    mappings.For<Aquilent.Cara.Domain.TemplateCodeSelect>(
                        binding => binding.ItemDataBound((item, code) =>
                        {
                            // expand the type level nodes
                            if (string.IsNullOrEmpty(code.Name) && code.CodeId == 0)
                            {
                                item.Checkable = false;
                                item.Expanded = true;
                            }
                            item.Text = code.CodeNumberDisplay + " " + code.Name;
                            item.Value = code.CodeId.ToString();
                        })
                        .Children(code => code.ChildCodes));

                    mappings.For<Aquilent.Cara.Domain.TemplateCodeSelect>(binding => binding.ItemDataBound((item, code) => { item.Text = code.Name; }));
                })
            .Render(); %>
        </td>
    </tr>
</table>