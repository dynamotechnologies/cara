﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.SupportViewModel>" %>

<table width="700" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" class="fieldLabel" valign="top">
            <table>
                <tr>
                    <td>
                        <img src="<%= ResolveUrl("~/Content/Images/icon_link.png")%>" alt="Links" title="Links" />&nbsp;&nbsp;Links
                        <p>
                            <a target="_blank" href="<%=Model.PalsLoginUrl %>"><%=Utilities.GetResourceString("PALS") %></a><br />
                            <a target="_blank" href="https://usdagcc.sharepoint.com/sites/fs-nepa-nsg/emnepa/cara/default.aspx">Support Materials</a><br />
                            <%-- %><a target="_blank" href="https://ems-team.usda.gov/sites/fs-nepa-nsg/emnepa/cara/default.aspx">Support Materials</a><br />--%>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<%= ResolveUrl("~/Content/Images/icon_problem.png")%>" alt="Report a Problem" title="Report a Problem" />&nbsp;&nbsp;Report a Problem
                        <p class="text">
                            Report a problem at <a href="<%=Model.SupportEmailAddress %>" target="_blank"><%=Model.SupportEmailAddress %> </a>
                        </p>
                    </td>                        
                </tr>
            </table>
        </td>
    </tr>
</table>