﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.Domain.Phase>" %>

<% if (Model != null) 
   { %>
    <table width="800">
        <tr>
            <td class="fieldLabel" width="20%" valign="top"><%: Html.LabelFor(model => model.Project.ProjectNameNumber) %>:</td>
            <td class="text" width="35%" valign="top"><%: Html.DisplayFor(model => model.Project.ProjectNameNumber)%></td>
            <td class="fieldLabel" width="20%" valign="top"><%: Html.LabelFor(model => model.PhaseTypeName)%>:</td>
            <td class="text" valign="top"><%: Html.DisplayFor(model => model.PhaseTypeDesc)%></td>
        </tr>
        <tr>
    <% if (Model.Project.IsPalsProject)
       { %>
            <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.UnitNameList)%>:</td>
            <td class="text" colspan="3" valign="top"><%: Html.DisplayFor(model => model.Project.UnitNameList)%></td>
    <% }
       else
       { %>
            <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Project.UnitName)%>:</td>
            <td class="text" colspan="3" valign="top"><%: Html.DisplayFor(model => model.Project.UnitName)%></td>
    <% } %>
        </tr>
    </table>
<% } %>
