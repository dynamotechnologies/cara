﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.Domain.Phase>" %>

<% if (Model != null) 
   { %>
<table>
    <tr>
        <td class="sectionTitle" colspan="4">
            <%: Utilities.GetResourceString(Constants.PHASE)%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.PhaseTypeName) %>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.PhaseTypeName) %></td>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.Name)%>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.Name)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.CommentStart)%>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.CommentStart)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(phase => phase.CommentEnd)%>:</td>
        <td class="text"><%: Html.DisplayFor(phase => phase.CommentEnd)%></td>
    </tr>
</table>
<br />
<div class="tabArea">
    <div class="sectionTitle"><%: Utilities.GetResourceString("TeamMember").Pluralize()%></div>
    <% Html.RenderPartial("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel { Members = Model.PhaseMemberRoles, ShowButtons = false }); %>
</div>
<br />
<div class="tabArea" style="width:685px">
    <div class="sectionTitle"><%: Utilities.GetResourceString("CodingStructure")%></div>
    <% Html.RenderPartial("~/Views/Shared/Details/ucPhaseCodesDetails.ascx", new PhaseCodesViewModel
       {
           PhaseCodes = Model.PhaseCodes,
           TreeViewName = "TreeView1",  // different tree view name to avoid elemment name conflict with the parent window
           ExpandAll = true // expand all to print all selected phase codes
       }); %>
</div>
<br />
<div class="tabArea">
    <div class="sectionTitle"><%: Utilities.GetResourceString(Constants.DOCUMENT).Pluralize()%></div>
    <% Html.RenderPartial("~/Views/Shared/List/ucDmdDocumentList.ascx", new DmdDocumentViewModel { Documents = Model.Documents, ShowButtons = false }); %>
</div>
<br />
<div class="tabArea">
    <div class="sectionTitle"><%: Utilities.GetResourceString(Constants.TERM_LIST)%></div>
    <% Html.RenderPartial("~/Views/Shared/List/ucTermListList.ascx", Model.TermLists); %>
</div>
<% } %>