﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.Domain.TermList>" %>

<table>
    <tr><td class="fieldLabel"><%=Utilities.GetResourceString(Constants.TERM_LIST) %> Name:</td>
    <td class="text"><%: Html.DisplayFor(termList => termList.Name)%></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top"><%:Utilities.GetResourceString(Constants.TERM).Pluralize() %>:</td>
        <td class="text">
        <%  if (Model != null && Model.Terms != null)
            {
                //CARA-966
                Html.Telerik().Grid(Model.Terms.Where(t => t.Active).OrderBy(t => t.Name))
                    .Name(Constants.TERM)
                    .DataKeys(keys => keys.Add(term => term.TermId).RouteKey("id"))
                    .Columns(columns =>
                    {
                        columns.Bound(term => term.Name).Width(200);
                    })
                    .Footer(false)
                    .Render();
            } %>
        </td>
    </tr>
</table>