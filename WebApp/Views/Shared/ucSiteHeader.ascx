﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Framework.Security.User>" %>

<% if (Model != null)
   { %>
<div id="navigation">
    <ul>
        <li><a href="<%= Url.Action(Constants.INDEX, Constants.HOME) %>" id="<%=(ViewData["ActiveTab"] == Constants.HOME ? "selected" : string.Empty)%>"><%:Constants.HOME %></a></li>
        <li><a href="<%= Url.Action(Constants.LIST, Constants.PROJECT, new { projectId = 0 }) %>" id="<%=(ViewData["ActiveTab"] == Constants.PROJECT ? "selected" : string.Empty)%>"><%:Utilities.GetResourceString(Constants.PROJECT).Pluralize()%></a></li>
        <% if (Model.HasPrivilege("RRPT")) { %>
            <li><a href="<%= Url.Action(Constants.INDEX, Constants.REPORT) %>" id="<%=(ViewData["ActiveTab"] == Constants.REPORT ? "selected" : string.Empty)%>"><%:Utilities.GetResourceString(Constants.REPORT).Pluralize()%></a></li>
        <% } %>
        <li><a href="<%= Url.Action(Constants.INPUT, Constants.PROFILE, new { id = Model.UserId }) %>" id="<%=(ViewData["ActiveTab"] == Constants.PROFILE ? "selected" : string.Empty)%>"><%:"My " + Constants.PROFILE%></a></li>
        <% if (Model.HasPrivilege("VAP")) { %>
            <li><a href="<%= Url.Action(Constants.INDEX, Constants.ADMIN) %>" id="<%=(ViewData["ActiveTab"] == Constants.ADMIN ? "selected" : string.Empty)%>"><%:Constants.ADMIN%></a></li>
        <% } %>
        <li><a href="<%= Url.Action(Constants.INDEX, Constants.SUPPORT) %>" id="<%=(ViewData["ActiveTab"] == Constants.SUPPORT ? "selected" : string.Empty)%>"><%:Constants.SUPPORT%></a></li>
    </ul>
</div>
   <% } %>