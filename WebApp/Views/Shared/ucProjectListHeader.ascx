﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<table class="t-widget t-grid-header" style="width:1273px; border-collapse:collapse;">
    <colgroup>
        <col id="cgPalsInfo" style="width:465px" />
        <col id="cgCaraInfo" style="width:175px" />
        <col id="cgLetterStatus" style="width:357px" />
        <col id="cgLetterType" style="width:357px" />
    </colgroup>
    <tr>
        <th class="t-header"style="text-align:center;border-bottom:none"><%=Constants.PROJECT %> Info</th>
        <th class="t-header"style="text-align:center;border-bottom:none"><%=Utilities.GetResourceString("AppNameAbbv") %></th>
        <th class="t-header"style="text-align:center;border-bottom:none"><%=Constants.CODING %> Status</th>
        <th class="t-header"style="text-align:center;border-bottom:none"><%=Constants.LETTER %> Type</th>
    </tr>
</table>