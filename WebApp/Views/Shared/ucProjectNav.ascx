﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProjectNavModel>" %>
<%@ Import Namespace="Aquilent.Cara.Domain" %>

<% Html.Telerik().PanelBar()
        .Name("ProjectNav")
        .HtmlAttributes(new { style = "width: 175px; float: left; margin-bottom: 30px;" })
        .ExpandMode(PanelBarExpandMode.Multiple)
        .HighlightPath(false)
        .Items(item =>
        {
            if (Model != null)
            {
                // add project home
                item.Add()
                    .Text(Constants.PROJECT)
                    .Items((subItem) =>
                    {
                        subItem.Add()
                                .Text(Utilities.GetPageTitle(Constants.PROJECT + Constants.DETAILS))
                                .Action(Constants.DETAILS, Constants.PROJECT, new { projectId = Model.Project.ProjectId });
                        subItem.Add()
                                .Text(string.Format("{0} {1}", Constants.ADD, Utilities.GetResourceString(Constants.PHASE)))
                                .Action(Constants.INPUT, Constants.PHASE, new { projectId = Model.Project.ProjectId, phaseId = 0, mode = PageMode.PhaseCreateWizard })
                                .Visible(Utilities.UserHasPrivilege("MPHS", Constants.UNIT, Model.UnitId, -1));
                        subItem.Add()
                                .Text(string.Format("{0} {1}", Constants.EDIT, Utilities.GetResourceString(Constants.PROJECT)))
                                .Action(Constants.INPUT, Constants.PROJECT, new { projectId = Model.Project.ProjectId, mode = PageMode.Edit })
                                .Visible(Utilities.UserHasPrivilege("MPRJ", Constants.UNIT, Model.UnitId, -1));
                    })
                    .Expanded(true);

                var phases = Model.Project.Phases.ToList();
                if (phases != null && phases.Count > 0)
                {
                    // determine the phase to be expanded
                    int selectedPhaseId = Utilities.GetEntityIdFromUrl(Request.Url.AbsolutePath, Constants.PHASE); // get phase id from url 

                    // if no phase is specified, set to current phase
                    if (selectedPhaseId < 0)
                    {
                        selectedPhaseId = Model.CurrentPhase.PhaseId;
                    }

                    // add items for each of the phases (either the phase is public or if the user is an active member or if user is a super user or coordinator)
                    var visiblePhases = phases.Where(x => !x.Private 
                        || (x.Private 
                            && (x.PhaseMemberRoles.Any(y => y.UserId == Model.CurrentUser.UserId && y.Active)
                                || Utilities.UserHasPrivilege("MPRJ", Constants.UNIT, Model.UnitId, -1))));
                    foreach (Phase phase in visiblePhases.OrderBy(x => x.PhaseTypeName))
                    {
                        // route values and url patterns
                        object routeValues = new { projectId = Model.Project.ProjectId, phaseId = phase.PhaseId };
                        object routeValuesEdit = new { projectId = Model.Project.ProjectId, phaseId = phase.PhaseId, mode = Constants.EDIT };
                        object routeValuesCreate = new { projectId = Model.Project.ProjectId, phaseId = phase.PhaseId, mode = Constants.CREATE };
                        string urlPattern = string.Format("/Project/{0}/Phase/{1}/", phase.ProjectId, phase.PhaseId) + "{0}";
                        string urlPatternEdit = urlPattern + "?mode=" + Constants.EDIT;

                        // add phase menu items
                        //CARA-921 Removing PhaseTypeName check when displaying description for comment period -JRhoadhouse 9/13/12 
                        item.Add()
                            .Text(phase.PhaseTypeDesc)
                            .Items((subItem) =>
                            {
                                subItem.Add()
                                        .Text(Utilities.GetResourceString(Constants.PHASE) + " " + Constants.HOME).Action(Constants.DETAILS, Constants.PHASE, routeValues);
                                
                                subItem.Add()
                                        .Text(Utilities.GetResourceString(Constants.PHASE) + " Setup")
                                        .Items((actionItem) =>
                                        {
                                            // -----------------------------------------------------------------------------------------------------------------
                                            // Note: Although Telerik offers "Action" method to set the url based on the action. We are not using it here
                                            //       since the "Action" method will perform privilege checking using the defined authorization attribute
                                            //       for the action. Since the privilege checking depends on the request url and it's not supplied in
                                            //       each phase it will incorrectly validate the privilege using the url. Thus, using url will just
                                            //       give the url link and "Visible" will be responsible for the authorization
                                            // -----------------------------------------------------------------------------------------------------------------
                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString(Constants.PHASE) + " Dates")
                                                    .Url(string.Format(urlPatternEdit, "Input"))
                                                    .Visible(Utilities.UserHasPrivilege("MTM", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);
                                            
                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString("TeamMember").Pluralize())
                                                    .Url(string.Format(urlPatternEdit, "MemberInput"))
                                                    .Visible(Utilities.UserHasPrivilege("MTM", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);

                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString("PhasePublicCommentFormInputTitle"))
                                                    .Url(string.Format(urlPatternEdit, "PublicCommentFormInput"))
                                                    .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);                                            
                                            
                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString(Constants.DOCUMENT).Pluralize())
                                                    .Url(string.Format(urlPatternEdit, "DocumentInput"))
                                                    .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);

                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString("CodingStructure"))
                                                    .Url(string.Format(urlPatternEdit, "CodeInput"))
                                                    .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);
                                
                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString("PhaseCodeTaskAssignmentTitle"))
                                                    .Url(string.Format(urlPatternEdit, "CodeTaskAssignment"))
                                                    .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);

                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString(Constants.TERM_LIST))
                                                    .Url(string.Format(urlPatternEdit, "TermListInput"))
                                                    .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);
                                
                                            actionItem.Add()
                                                    .Text(Utilities.GetResourceString("PhaseConcernResponseTemplateInputTitle"))
                                                    .Url(string.Format(urlPatternEdit, "ConcernResponseTemplateInput"))
                                                    .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);

                                        })
                                        .HtmlAttributes(new { style = "background-color:white;" })
                                        .Expanded(false)
                                        .Visible(Utilities.UserHasPrivilege("MPHS", Constants.PHASE, Model.UnitId, phase.PhaseId) && !phase.Locked);

                                subItem.Add()
                                        .Text(Utilities.GetResourceString(Constants.LETTER).Pluralize())
                                        .Items((actionItem) =>
                                        {
                                            actionItem.Add().Text(Utilities.GetPageTitle(Constants.LETTER + Constants.LIST))
                                                .Action(Constants.LIST, Constants.LETTER, routeValues);

                                            // Letters can be added to the phase only if it's active
                                            actionItem.Add().Text(Utilities.GetPageTitle(Constants.LETTER + Constants.CREATE))
                                                .Url(string.Format(urlPattern, string.Format("{0}/{1}", Constants.LETTER, Constants.CREATE)))
                                                .Visible(Utilities.UserHasPrivilege("MLTR", Constants.PHASE, Model.UnitId, phase.PhaseId) && phase.IsActive);

                                            actionItem.Add().Text(Utilities.GetPageTitle(Constants.LETTER + Constants.UPLOAD + Constants.LETTERS))
                                                .Url(string.Format(urlPattern, string.Format("{0}/{1}", Constants.LETTER, Constants.UPLOAD + Constants.LETTERS)))
                                                .Visible(Utilities.UserHasPrivilege("MLTR", Constants.PHASE, Model.UnitId, phase.PhaseId) && phase.IsActive);

                                            // Form Mgmt pages are visible to all users
                                            actionItem.Add().Text(Utilities.GetPageTitle(Constants.FORM + Constants.LIST))
                                                .Action(Constants.LIST, Constants.FORM, routeValues);

                                            actionItem.Add().Text(Utilities.GetPageTitle(Constants.FORM + Constants.CHANGE_LOG + Constants.LIST))
                                                .Action(Constants.CHANGE_LOG + Constants.LIST, Constants.FORM, routeValues);
                                        })
                                        .HtmlAttributes(new { style = "background-color:white;" })
                                        .Expanded(true);

                                subItem.Add()
                                        .Text(Utilities.GetPageTitle(Constants.COMMENT + Constants.LIST)).Action(Constants.LIST, Constants.COMMENT, routeValuesCreate);

                                subItem.Add()
                                        .Text(Utilities.GetPageTitle(Constants.CONCERN + Constants.LIST)).Action(Constants.LIST, Constants.CONCERN, routeValuesCreate);

                                //CARA-976
                                subItem.Add()
                                        .Text(Utilities.GetResourceString(Constants.READING_ROOM_MGMT_ABBR))
                                        .Url(string.Format(urlPattern, string.Format("{0}/{1}", Constants.READING_ROOM, Constants.LIST)))
                                        .Visible(phase.ReadingRoomActive && Utilities.UserHasPrivilege("VWRR", Constants.PHASE, Model.UnitId, phase.PhaseId));

                                subItem.Add()
                                        .Text(Utilities.GetResourceString(Constants.STANDARD_REPORTS)).Action(Constants.LIST, Constants.PHASE_REPORT, routeValues);

                            })
                            .Expanded(phase.PhaseId == selectedPhaseId);
                    }
                }
            }
        })
        .Render(); 
%>
