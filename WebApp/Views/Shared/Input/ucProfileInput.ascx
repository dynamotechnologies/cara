﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProfileViewModel>" %>

<table>
    <tr>
        <td colspan="2" class="text"><%: Html.CheckBoxFor(model => model.User.NotifySubmissionsReceived)%>Notify me when
            <%: Html.Telerik().DropDownListFor(model => model.User.SubmissionsRecv)
                                .BindTo(Model.Submissions)%> submissions have been received for My Project(s).
        </td>
    </tr>
    <tr>
        <td colspan="2" class="text"><%: Html.CheckBoxFor(model => model.User.NotifyCommentPeriodEnd)%>Notify me <%= Model.PhaseEndThreshold.ToString() %> days in advance to the end of a comment period for My Project(s).</td>
    </tr>
    <tr>
        <td colspan="2" class="text">Number of search results rows shown per page <%: Html.Telerik().DropDownListFor(model => model.User.SearchResultsPerPage).BindTo(Model.Rows) %></td>
    </tr>
    <tr>
        <td colspan="2" class="text">Letter font size <%: Html.Telerik().DropDownListFor(model => model.User.LetterFontSize).BindTo(Model.LetterFontSizes) %></td>
    </tr>
    <tr>
        <td class="text"><label for="User_TimeZone">Select Time Zone</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.User.TimeZone)
                             .BindTo(Model.TimeZones)
                             .HtmlAttributes(new { style = "width:400px;" })%></td>
    </tr>
</table>
<!-- Hidden fields for user model -->
<%: Html.HiddenFor(model => model.User.UserId)%>
<%: Html.HiddenFor(model => model.User.Username)%>
<%: Html.HiddenFor(model => model.User.FirstName)%>
<%: Html.HiddenFor(model => model.User.LastName)%>
<%: Html.HiddenFor(model => model.User.Email)%>
<%: Html.HiddenFor(model => model.User.NotifyAddedProject)%>
<%: Html.HiddenFor(model => model.User.NotifySubmissionsReceived)%>
<%: Html.HiddenFor(model => model.PhaseEndThreshold)%>
