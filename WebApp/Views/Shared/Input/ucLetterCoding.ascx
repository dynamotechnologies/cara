﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<div id="displayContainer" class="codingContainer toggleFullScreen">
    <!-- Only displayed in full screen mode -->
    <div id="subTitle" class="pageTitle toggleFullScreen" style="display:none;"><%=string.Format("{0} {1} #{2}", Constants.LETTER, Constants.CODING, Model.Letter.LetterSequence) %></div>

    <!-- Area of Manage Comment tab navigation -->
    <div id="codingArea" class="toggleFullScreen">
        <!-- Padding div to align the toolbar and the tab -->
        <div id="codingAreaPadding">&nbsp;</div>
        <div>
            <% Html.RenderPartial("~/Views/Shared/Input/ucLetterCodeInput.ascx", Model); %>
        </div>
    </div>

    <!-- Area of Coding Toolbar -->
    <div id="toolbar" class="toolbar codingAreaBuffer toggleFullScreen">
        <%: Html.DropDownList("ddlFontSize", Model.FontSizes) %>
        <%: Html.DropDownList("ddlTextMode", Model.TextModes)%>
        <a id="btnPrevComment" href="#" onclick="javascript:prevComment();"><img src="<%= ResolveUrl("~/Content/Images/icon_prev.png")%>" alt="Previous Comment" title="Previous Comment" /></a>
        <span id="currentComment"></span>
        <a id="btnNextComment" href="#" onclick="javascript:nextComment();"><img src="<%= ResolveUrl("~/Content/Images/icon_next.png")%>" alt="Next Comment" title="Next Comment" /></a>
        &nbsp;&nbsp;
        <% if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
           { %>
            <a id="btnLinkText" href="#" onclick="linkText(<%=Model.Letter.Phase.ProjectId%>, <%=Model.Letter.PhaseId%>, <%=Model.Letter.LetterId%>);"><img src="<%= ResolveUrl("~/Content/Images/icon_linktext.png")%>" alt="Link Text" title="Link Text" /></a>&nbsp;&nbsp;
        <% } %>
        <a id="btnCompareMaster" href="#" onclick="javascript:openWindow('WindowCompare', '/Project/<%=Model.Letter.Phase.ProjectId%>/Phase/<%=Model.Letter.PhaseId%>/Letter/CompareMaster/<%=Model.Letter.LetterId%>?targetLetterId=<%=(Model.Letter.FormSetId != null) ? Model.Letter.FormSet.MasterFormId : (Model.Letter.MasterDuplicateId != null) ? Model.Letter.MasterDuplicateId : -1%>&navigate=false')"><img src="<%= ResolveUrl("~/Content/Images/icon_compare.png")%>" alt="Compare to Master" title="Compare to Master" /></a >&nbsp;&nbsp;
        <a id="btnScreenSize" href="#" onclick="javascript:setWindowSize();"><img id="imgScreenSize" src="<%= ResolveUrl("~/Content/Images/icon_fullscreen.png")%>" alt="Full Screen" title="Full Screen" /></a>
    </div>

    <!-- Area of Letter Text/Comment -->
    <div id="commentArea" class="codingAreaBuffer">
    <%: Html.TextAreaFor(model => model.Letter.HtmlCodedText)%>
    </div>

    <!-- Area of action buttons and legend -->
    <div id="actionArea" class="codingAreaBuffer toggleFullScreen">
<% if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
   { 
        string updateStatusValue = "Complete";
        if (Constants.COMPLETE.Equals(Model.Letter.LetterStatusName) || Constants.NEW.Equals(Model.Letter.LetterStatusName))
        {
            updateStatusValue = "In Progress";
        }%>

        <table>
            <tr>
                <td class="fieldLabel">Coding&nbsp;Status:</td>
                <td align="left">
                    <span id="LetterStatusName"><%=Model.Letter.LetterStatusName %></span>
                    <% if (Model.Letter.LetterStatusId != 4)
                       { // Not Needed %>
                    &nbsp;&nbsp;
                    <input id ="UpdateStatus" value="<%=updateStatusValue%>" type="button" class="button" onclick="updateStatus(<%=Model.LetterId %>, <%=Model.PhaseId %>, '<%=Constants.IN_PROGRESS%>')" id="<%=Constants.IN_PROGRESS %>" />
                    <% } %>
                </td>
                <td width="30px">&nbsp;</td>
                <td class="fieldLabel"><%: Html.LabelFor(m => m.Letter.LastUpdated)%>:</td>
                <td><%= Model.Letter.LastUpdated.ConvertToUserTimeZone(Utilities.GetUserSession()).ToShortDateString()%></td>
            </tr>
            <tr>
                <td class="fieldLabel">Navigate&nbsp;To:</td>
                <td>
                    <%= Html.ActionLink(Utilities.GetPageTitle(Constants.LETTER + Constants.LIST), Constants.LIST, new {projectId = Model.ProjectId, phaseId = Model.PhaseId }, new { @class = "button", @onClick = "return warnUnappliedCodes();" })%>&nbsp;
                    <%= Html.ActionLink(string.Format("{0} {1}", Constants.NEXT, Constants.LETTER), Constants.NEXT + Constants.LETTER,
                        new { 
                            projectNum = Model.ProjectId, 
                            phaseNum = Model.PhaseId, 
                            letterNum = Model.LetterId, 
                            nextLetterNums = Model.NextLetterIds, 
                            numberOfLettersRemaining = Model.LettersRemaining}, 
                        new { @class = "button", @onClick = "return warnUnappliedCodes();" })%>
                </td>
                <td width="30px">&nbsp;</td>
                <td class="fieldLabel"><%:Html.LabelFor(m => m.Letter.LastUpdatedBy)%>:</td>
                <td><%: Html.DisplayFor(m => m.Letter.LastUpdatedBy)%></td>
            </tr>
        </table>

<% }
   else
   { %>
            <table width="200">
                <tr>
                    <td width="50%" align="center" class="fieldLabel">Navigate to:</td>
                </tr>
                <tr>
                    <td align="center">
                        <%= Html.ActionLink(Utilities.GetPageTitle(Constants.LETTER + Constants.LIST), Constants.LIST, new { projectId = Model.ProjectId, phaseId = Model.PhaseId }, new { @class = "button" })%>&nbsp;
                        <%= Html.ActionLink(string.Format("{0} {1}", Constants.NEXT, Constants.LETTER), Constants.NEXT + Constants.LETTER, new { projectId = Model.ProjectId, phaseId = Model.PhaseId, nextLetterIds = Model.NextLetterIds, lettersRemaining = Model.LettersRemaining }, new { @class = "button" })%>
                    </td>
                </tr>
            </table>
            <br />
            <div>
                <%: Html.LabelFor(m => m.Letter.LastUpdated)%>:&nbsp;<%= Model.Letter.LastUpdated.ConvertToUserTimeZone(Utilities.GetUserSession()).ToShortDateString()%><br />
                <%: Html.LabelFor(m => m.Letter.LastUpdatedBy)%>:&nbsp;<%: Html.DisplayFor(m => m.Letter.LastUpdatedBy)%>
            </div>
<% } %>
    </div>

    <div id="codingLegend" class="codingAreaBuffer toggleFullScreen">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td class="fieldLabel">Legend:&nbsp;</td>
                <td class="text"><span style="background-color:#FFFF99" title="Yellow - Comment">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - <%=Constants.COMMENT %></td>
                <td>&nbsp;</td>
                <td class="text"><span style="background-color:#82AAFF" title="Blue - Auto-markup">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - <%=Utilities.GetResourceString(Constants.TERM_LIST) %></td>
                <td>&nbsp;</td>
                <td class="text"><span style="background-color:#78DA76" title="Green - Current Comment">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - Current <%=Constants.COMMENT %></td>
                <!-- form plus highlight section start -->
                <%if (ViewData["CodingFormPlusLegend"] != null)
                  { %>
                  <td>&nbsp;</td>
                <td class="text"><span style="background-color:#D3D3D3" title="Light Gray - Form Plus">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - Form Plus</td>
                <script type="text/javascript">
                <!--
                   document.getElementById("codingLegend").style.width = (document.getElementById("codingLegend").offsetWidth + 80) + "px"; 
                // -->
                </script>
                <%} %>
                <!-- form plus highlight section end -->
            </tr>
        </table>
    </div>
</div>
<!-- All hidden variables used by the page -->
<!--CR#308 Not returning to same page of letter inbox -->
<div>
    <%: Html.HiddenFor(m => m.Letter.LetterTypeId) %>
    <%: Html.HiddenFor(m => m.ProjectId) %>
    <%: Html.HiddenFor(m => m.PhaseId) %>
    <%: Html.HiddenFor(m => m.LetterId) %>
    <%: Html.HiddenFor(m => m.LetterFontSize) %>
    <%: Html.HiddenFor(m => m.Letter.DmdId) %>
    <%: Html.HiddenFor(m => m.NextLetterIds) %>
    <%: Html.HiddenFor(m => m.LettersRemaining) %>
    <%: Html.HiddenFor(m => m.RequestUri) %>
</div>


