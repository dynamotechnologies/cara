﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ObjectionResponseInputViewModel>" %>

<div class="validation-summary-errors" id="objectionResponseErrors"></div>

<% Html.BeginForm("ObjectionResponse", "Letter", FormMethod.Post, new { id = "objectionResponseForm", name = "objectionResponseForm", enctype = "multipart/form-data" }); %>
<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionResponse.SignerFirstName)%>:</td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.ObjectionResponse.SignerFirstName) %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionResponse.SignerLastName)%>:</td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.ObjectionResponse.SignerLastName) %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionResponse.ObjectionDecisionMakerId)%>:</td>
        <td class="text">
            <%: Html.Telerik().DropDownListFor(model => model.ObjectionResponse.ObjectionDecisionMakerId)
                .BindTo(Model.LookupSignerTitle)
                .HtmlAttributes(new { style = "width:250px;" }) %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionResponse.ResponseDate)%>:</td>
        <td class="text">
            <%: Html.Telerik().DatePickerFor(model => model.ObjectionResponse.ResponseDate).ShowButton(true) %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top">Attachments:</td>
        <td class="text">
            <% Html.RenderPartial("~/Views/Shared/List/ucObjectionDocumentList.ascx", new ObjectionDocumentViewModel { Documents = Model.ObjectionResponse.Documents, ShowButtons = true }); %>
            <br />
            <% Html.RenderPartial("~/Views/Shared/Input/ucFileUpload.ascx", new Dictionary<string, object> { { "ShowDocumentName", false }, { "MaxUploadSize", Model.MaxUploadSize } }); %>
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            <table>
            <tr>
                <td style="width:500px">
                    Note: Please do not hit “Save” more than once. Final documents uploaded will be merged into one “Objection Response” in the order in which they are uploaded. The Documents and Response will be automatically posted to the project’s webpage
                </td>
                <td align="right">
                    <a class="button" id="objectionResponseSubmit">Save</a>
                    <a class="button" onclick="javascript:closeWindow('WindowObjectionResponse');">Cancel</a>
                </td>
            </tr>
            </table>
        </td>
    </tr>
 </table>

<%: Html.HiddenFor(model => model.PhaseId)%>
<%: Html.HiddenFor(model => model.ProjectId)%>

<!-- Hidden fields for letter objection  -->
<%: Html.HiddenFor(model => model.ObjectionResponse.LetterObjectionId)%>
<%: Html.HiddenFor(model => model.ObjectionResponse.FiscalYear)%>
<%: Html.HiddenFor(model => model.ObjectionResponse.Region) %>
<%: Html.HiddenFor(model => model.ObjectionResponse.Forest)%>
<%: Html.HiddenFor(model => model.ObjectionResponse.ObjectionNumber) %>
<%: Html.HiddenFor(model => model.ObjectionResponse.RegulationNumber) %>
<%: Html.HiddenFor(model => model.ObjectionResponse.ReviewStatusId) %>
<%: Html.HiddenFor(model => model.ObjectionResponse.OutcomeId) %>
<%: Html.HiddenFor(model => model.ObjectionResponse.MeetingInfo) %>
<%: Html.HiddenFor(model => model.ObjectionResponse.DmdId) %>
<% Html.EndForm(); %>

<script type="text/javascript">
    var form = new ObjectionInfoForm($("#objectionResponseForm"), "WindowObjectionResponse", $("#objectionResponseSubmit"), $("#objectionResponseErrors"), "Summary").Init();
    FileUploadInit();
</script>
