﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DataMartUserViewModel>" %>

<table>
    <tr><td class="fieldLabel" colspan="2">Search <%:Utilities.GetResourceString(Constants.USER).Pluralize() %> from <%:Utilities.GetResourceString("PALS") %></td></tr>
    <tr>
        <td class="fieldLabel"><label for="UserFirstName">First Name:</label></td>
        <td class="text"><%: Html.TextBoxFor(model => model.UserFirstName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="UserLastName">Last Name:</label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.UserLastName)%>&nbsp;
            <input type="submit" class="button" name="action" value="Search Users" />
        </td>
    </tr>
</table>
<br /><br />
<% if (Model.Users != null)
       {
           Html.RenderPartial("~/Views/Shared/List/ucDataMartUserList.ascx", Model.Users); %>
<% } %>