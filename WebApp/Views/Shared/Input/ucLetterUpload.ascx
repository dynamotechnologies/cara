﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterUploadViewModel>" %>
    <p>
      On this page you can upload batches of letters in the form of .pst files (containing Outlook emails) or FDMS .zip files (from regulations.gov).
      Click the Support tab for instructions on how to create these files. Note: .zip files NOT extracted from FDMS cannot be uploaded. Maximum file 
      size: 50 Mb per upload. Unlimited number of uploads.
    </p>
    
    <% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
       { %>
        <label for="fileUpload" class="fieldLabel">Select File to Upload:</label>&nbsp;
        <input type="file" name="File" id="File" /><br /><br />
        <a class="button" id="btnSubmit" href="#" onclick="submitForm()"><%=Constants.UPLOAD %></a>
    <% } %>

    <p>
      Please stay on this page until the upload is complete. Hourly on the hour, CARA processes the queue of uploaded files, extracts the letters, and
      adds them to the Letter Inbox. Files are “Pending” until processing is complete.
    </p>
    <p>Below is a list of all files that have been uploaded to this comment period.</p>
<% Html.Telerik().Grid<Aquilent.Cara.Domain.LetterUploadQueue>(Model.QueueFiles)
       .Columns(columns =>
           {
               columns.Bound(x => x.Filename).Title("File Name");
               columns.Bound(x => x.FileType).Title("File Type");
               
               columns.Bound(x => x.FileSize).Template(x=>
               {
                   %><%=(x.FileSize/1024).ToString()%><%
               }).Title("Size (in KB)");
               
               columns.Bound(x => x.UserId).Template(x =>
               {
                   %><%= Model.GetUserNameById(x.UserId)%><%
               })
               .Title("Uploading User");
               
               columns.Bound(x => x.Status).Title("Status").Template(x =>
               {
                    %><%= x.Status %>
                    <% if (x.Status == "Error" && Utilities.UserHasPrivilege("RQLU", Constants.PHASE))
                       { %>
                        <%: Html.ActionLink("Retry", "RequeueLetterUpload", new { projectId = Model.ProjectId, phaseId = x.PhaseId, letterUploadId = x.LetterUploadQueueId }, new { @class = "button" }) %>
                    <% }
                       else if (x.Status == "Pending" && Utilities.UserHasPrivilege("RQLU", Constants.PHASE))
                       { %>
                        <%: Html.ActionLink(Constants.DELETE, "DeleteLetterUpload", new { projectId = Model.ProjectId, phaseId = x.PhaseId, letterUploadId = x.LetterUploadQueueId }, new { @class = "button" }) %>
                    <% }
               });
               
               columns.Bound(x => x.UploadDate).Template(x=>
               {
                   %><%=x.UploadDate.ConvertToUserTimeZone(Utilities.GetUserSession()).ToString()%><%
               }).Title("Upload Date");
               
               columns.Bound(x => x.ProcessDate).Template(x=>
               {
                   %><%=x.ProcessDate.HasValue?
                     x.ProcessDate.Value.ConvertToUserTimeZone(Utilities.GetUserSession()).ToString()
                     :string.Empty%><%
               }).Title("Processed Date");
               
               columns.Bound(x=>x.LetterCount).Title("Number of Letters");
           })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        //.DataBinding(dataBinding => dataBinding.Ajax().Select("PreviewBinding", "Report"))
        .EnableCustomBinding(true)
        .Name("FileTable")
        //.Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
        //.Sortable()
        .Render(); %>