﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CaraSettingInputViewModel>" %>

<div id="text">
<%=Html.HiddenFor(model => model.CaraSetting.Name) %>
<% Html.Telerik().EditorFor(model => model.CaraSetting.Value)
       .Name("editor")
       .HtmlAttributes(new { style = "height:400px;width:700px" })
       .Render(); %>
</div>