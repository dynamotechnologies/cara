﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterInputViewModel>" %>

<table>
    <tr>
        <td class="fieldLabel"><%: Html.Label("Nombre primero")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.FirstName)%></td>
        <td class="fieldLabel"><%: Html.Label("Apellido")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.LastName)%></td>
    </tr>
    <% if (!Model.Public)
       { %>
    <tr>
        <td colspan="4" class="text"><%: Html.CheckBoxFor(model => model.Anonymous)%>&nbsp;<label for="Anonymous">Compruebe si desea presentar la  <%:Utilities.GetResourceString(Constants.LETTER).ToLower()%> anónima</label></td>
    </tr>
    <% }
       else
       { %>
    <tr>
        <td colspan="4" class="text">
          <%: Html.CheckBoxFor(model => model.OrganizationCheck) %>
          <span class="fieldLabel"><label for="OrganizationCheck">Represento a una organización oficialmente</label></span>
        </td>
    </tr>
    <% }
      
        bool showOrg = (Model.OrganizationCheck || !Model.Public);
    %>
    <tr id="organizationRow" <%= showOrg ? string.Empty : "class=\"invisible\"" %>>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.OrganizationName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.OrganizationName)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Title)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Title)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.Label("Dirección 1")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Address1)%></td>
        <td class="fieldLabel"><%: Html.Label("Dirección 2")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Address2)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.Label("Ciudad")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.City)%></td>
        <td class="fieldLabel"><%: Html.Label("Estado")%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.StateId)
                             .BindTo(Model.LookupStateSp)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.Label("Provincia / Región")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.ProvinceRegion)%></td>
        <td class="fieldLabel"><%: Html.Label("Postal / Código Postal")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.ZipPostal)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.Label("País")%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.CountryId)
                             .BindTo(Model.LookupCountrySp)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><%: Html.Label("Email")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Email)%></td>
    </tr>
    <tr valign="top">
        <td class="fieldLabel"><%: Html.Label("Número de teléfono")%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Phone)%></td>
        <td class="fieldLabel" width="150px"><%: Html.Label("¿Cómo le gustaría recibir información sobre este u otros proyectos similares en el futuro?")%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.ContactMethod)
                             .BindTo(Model.LookupContactMethodSp)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>

    <tr>
        <td class="fieldLabel" valign="top"></td>
        <td class="text" colspan="3" style="color: red"><br /><br /><b>No ingrese ninguna información de identificación personal (PII), como la dirección o el correo electrónico, en el editor de texto a continuación.</b> Su nombre y toda la información ingresada en el cuadro de texto a continuación pueden publicarse en este sitio web. <span style="color: Black">Introduzca la información de contacto en los campos de formulario de arriba.<br /><br />No incrustar imágenes en el editor de texto. Usa <b>Adjuntos</b> para subir imagenes.</span></td>
    </tr>

    <% if (Model.Public)
       { %>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.Label("El texto de la letra")%></td>
        <td class="text" colspan="3"><%: Html.TextAreaFor(model => model.Letter.Text, 10, 80, null)%></td>
    </tr>
    <%}
       else
       { %>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.Label("El texto de la letra")%></td>
        <td class="text" colspan="3"><%: Html.TextAreaFor(model => model.Letter.HtmlText, 10, 80, null)%></td>
    </tr>
    <%} %>
    <tr>
        <td class="fieldLabel" valign="top">Archivos adjuntos</td>
        <td class="text" colspan="3">
           <% Html.RenderPartial("~/Views/Shared/Input/ucFileUploadSp.ascx", new Dictionary<string, object> { { "ShowDocumentName", false }, { "MaxUploadSize", Model.MaxUploadSize } }); %>
        </td>
    </tr>
    <% if (!Model.Public)
       { %>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DateSubmitted)%></td>
        <td class="text"><%= Html.Telerik().DatePickerFor(model => model.Letter.DateSubmitted).ShowButton(true) %></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DeliveryTypeId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Letter.DeliveryTypeId)
                             .BindTo(Model.LookupDeliveryType)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="WithinCommentPeriod">Within <%:Utilities.GetResourceString(Constants.PHASE) %></label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.WithinCommentPeriod)
                             .BindTo(Model.LookupYesNo)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.CommonInterestClassId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Letter.CommonInterestClassId)
                             .BindTo(Model.LookupCommonInterestClass)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <% }
       else           
       { %>
       <%: Html.HiddenFor(model => model.Letter.DateSubmitted)%>
       <%: Html.HiddenFor(model => model.Letter.DeliveryTypeId)%>
       <%: Html.HiddenFor(model => model.WithinCommentPeriod)%>       
    <% } %>
</table>
<%: Html.HiddenFor(model => model.Public)%>
<%: Html.HiddenFor(model => model.Mode)%>
<%: Html.HiddenFor(model => model.Sender)%>

<!-- Hidden fields for letter  -->
<%: Html.HiddenFor(model => model.Letter.LetterId)%>
<%: Html.HiddenFor(model => model.Letter.CodedText)%>
<%: Html.HiddenFor(model => model.Letter.DateEntered)%>
<%: Html.HiddenFor(model => model.Letter.Deleted)%>
<%: Html.HiddenFor(model => model.Letter.EarlyActionStatusId)%>
<%: Html.HiddenFor(model => model.Letter.LetterSequence)%>
<%: Html.HiddenFor(model => model.Letter.LetterStatusId)%>
<%: Html.HiddenFor(model => model.Letter.LetterTypeId)%>
<%: Html.HiddenFor(model => model.Letter.PhaseId)%>
<%: Html.HiddenFor(model => model.Letter.Size)%>
<%: Html.HiddenFor(model => model.Letter.Protected)%>

<!-- Hidden fields for Author  -->
<%: Html.HiddenFor(model => model.Author.AuthorId)%>
<%: Html.HiddenFor(model => model.Author.Deleted)%>
<%: Html.HiddenFor(model => model.Author.LetterId)%>
<%: Html.HiddenFor(model => model.Author.MiddleName)%>
<%: Html.HiddenFor(model => model.Author.Prefix)%>
<%: Html.HiddenFor(model => model.Author.Sender)%>
<%: Html.HiddenFor(model => model.Author.Suffix)%>