﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseInputViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<% if (Model != null) { %>
<div class="text"><span class="required">*</span>&nbsp;=&nbsp;Required</div>
<table>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><%: Html.LabelFor(model => model.Phase.PhaseTypeId)%>:</td>
        <td class="text">
            <% if (Model.IsPalsProject)
               { %>
                    <%: Html.Telerik().DropDownListFor(model => model.Phase.PhaseTypeId)
                               .BindTo(Model.PhaseTypes)
                               .ClientEvents(events => events.OnChange("setDuration").OnLoad("setDuration"))
                               .HtmlAttributes(new { style = "width:200px;" }).Enable(Model.IsPalsProject)%>
            <% }
               else
               { %>
                    Other <%: Html.HiddenFor(model => model.Phase.PhaseTypeId, new { value = Model.Phase.PhaseTypeId }) %>
            <% } %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Phase.Name) %>:</td>
        <td class="text"><%: Html.TextBoxFor(model => model.Phase.Name, new { maxlength = 20 })%>
                    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="AlwaysOpenToolTip1" 
                    title="Description is required when the Comment Period is 'Other'." />     
        </td>

    </tr>
        <tr>
        <td class="fieldLabel"><label for="Phase_TimeZoneList">Time Zone:</label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(
                             model => model.SelectedTimeZone)
                             .BindTo(Model.TimeZoneList)
                             .HtmlAttributes(new { style = "width:400px;" })%>
                    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="TimeZoneToolTip" 
                    title="Comments will close at midnight in the selected time zone." />
        </td>
        </tr>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><%: Html.LabelFor(model => model.Phase.CommentStart) %>:</td>
        <td class="text"><%= Html.Telerik().DatePickerFor(model => model.Phase.CommentStart)
                             .ShowButton(true)
                             .ClientEvents(events => events.OnChange("setEndDate")) %>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><label for="Duration">Duration:</label></td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.Duration, new { maxlength = 4, style = "width:75px;", onchange = "javascript:setEndDate()" })%>&nbsp;Day(s)
            <% if (!Model.IsPalsProject)
               { %>
                    &nbsp;<%: Html.CheckBoxFor(model => model.Phase.AlwaysOpen, new { onchange = "javascript:alwaysOpen()" })
                      %>
                    <label for="Phase_AlwaysOpen">Make comment period "Always Open"</label>
                    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="AlwaysOpenToolTip" 
                    title="Checking this box will make the webform appear on the web indefinitely or until a user manually removes the webform from “Comment Period Home” or unchecks the box." />
            <% }
               else
               { %>
                 <%: Html.HiddenFor(model => model.Phase.AlwaysOpen) %>
            <% }%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="EndDate">End Date:</label></td>
        <td class="text"><span id="EndDate"></span></td>
    </tr>

    <tr class="phaseObjectionField">
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Phase.ObjectionResponseExtendedBy)%>:</td>
        <td><%: Html.TextBoxFor(model => model.Phase.ObjectionResponseExtendedBy, new { maxlength = 4, style = "width:75px;", onchange = "javascript:setEndDate()" })%>&nbsp;Day(s)
        <span id="DurationExtMessage" class="message" hidden="true">Response Due on a weekend, extending duration to next weekday.</span></td>
    </tr>

    <tr class="phaseObjectionField">
        <td class="fieldLabel"><label for="ObjectionResponseNewDueDate">New Response Due Date:</label></td>

        <td class="text"><span id="ObjectionResponseNewDueDate"></span>
        <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="DueDateToolTip" 
                    title="If the response would be due on a weekend the due date is moved to the next weekday." />
            </td>
    </tr>
    
    <tr class="phaseObjectionField">
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Phase.ObjectionResponseExtendedJustification)%>:</td>
        <td>
            <%: Html.TextAreaFor(model => model.Phase.ObjectionResponseExtendedJustification, new { rows = 6, cols = 50 })%>
            <br />
            <span id="countdown" />
        </td>
    </tr>

    <tr>
        <td class="fieldLabel"><%:Utilities.GetResourceString(Constants.READING_ROOM)%> Status:</td>
        <td class="text">
            <% if (Utilities.UserHasPrivilege("ACRR", Constants.PHASE) && !Model.Phase.Locked)
               { %>
                    <%= Html.RadioButtonFor(m => m.Phase.ReadingRoomActive, true)%>Active
                    <%= Html.RadioButtonFor(m => m.Phase.ReadingRoomActive, false)%>Inactive 
            <% } %>            
        </td>
    </tr>
    
    <tr>
        <td class="fieldLabel"><%:Utilities.GetResourceString(Constants.PUBLIC_COMMENT_INPUT)%> Status:</td>
        <td class="text">
            <% if (Utilities.UserHasPrivilege("ACPC", Constants.PHASE) && !Model.Phase.Locked)
               { %>
                    <%= Html.RadioButtonFor(m => m.Phase.PublicCommentActive, true)%>Active
                    <%= Html.RadioButtonFor(m => m.Phase.PublicCommentActive, false)%>Inactive
            <% } %>

            <!--<img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="When the status is Active, the Public Comment Form will only be visible between <%:Html.DisplayFor(model => model.Phase.CommentStart)%> - <%=Html.DisplayFor(model => model.Phase.CommentEnd)%>." title="When the status is Active, the Public Comment Form will only be visible between <%:Html.DisplayFor(model => model.Phase.CommentStart)%> - <%=Html.DisplayFor(model => model.Phase.CommentEnd)%>." />-->
        </td>
    </tr>
</table>
<!-- Hidden fields for phase model -->
<%: Html.HiddenFor(model => model.OriginalCommentEnd)%>
<%: Html.HiddenFor(model => model.DefaultDuration)%>
<%: Html.HiddenFor(model => model.DefaultObjectionDuration)%>
<%: Html.HiddenFor(model => model.Phase.PhaseId)%>
<%: Html.HiddenFor(model => model.Phase.ProjectId)%>
<%: Html.HiddenFor(model => model.Phase.Locked)%>
<%: Html.HiddenFor(model => model.Phase.Private)%>
<%: Html.HiddenFor(model => model.Phase.Deleted)%>
<%: Html.HiddenFor(model => model.Phase.LastUpdated)%>
<%: Html.HiddenFor(model => model.Phase.LastUpdatedBy)%>
<%: Html.HiddenFor(model => model.Phase.ReadingRoomActive)%>
<%: Html.HiddenFor(model => model.Phase.PublicCommentActive)%>
<%: Html.HiddenFor(model => model.IsPalsProject)%>
<%: Html.HiddenFor(model => model.Phase.PhaseTypeName) %>
<%: Html.HiddenFor(model => model.Phase.Duration) %>
<%: Html.HiddenFor(model => model.Phase.ObjectionResponseDue) %>
<%: Html.HiddenFor(model => model.Phase.ConcernTemplate) %>
<%: Html.HiddenFor(model => model.Phase.ResponseTemplate) %>
<%: Html.HiddenFor(model => model.Phase.PublicFormText) %>
<%: Html.HiddenFor(model => model.Phase.ContactName) %>
<%: Html.HiddenFor(model => model.Phase.CommentEmail) %>
<%: Html.HiddenFor(model => model.Phase.MailAddressStreet1) %>
<%: Html.HiddenFor(model => model.Phase.MailAddressStreet2) %>
<%: Html.HiddenFor(model => model.Phase.MailAddressCity) %>
<%: Html.HiddenFor(model => model.Phase.MailAddressState) %>
<%: Html.HiddenFor(model => model.Phase.MailAddressZip) %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/util.inputcountdown.js")
            .Add("~/Scripts/cara.phaseinput.js")
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js"));
%>

<script type="text/javascript">
    
</script>
