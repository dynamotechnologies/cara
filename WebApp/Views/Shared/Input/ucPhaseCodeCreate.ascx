﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseCodeInputViewModel>" %>

<table>
    <tr>
        <td class="fieldLabel"><span id="selectedCodeLabel">Parent <%:Utilities.GetResourceString(Constants.CODE) %>:</span></td>
        <td class="text">
            <%: Html.DisplayFor(model => model.SelectedCode.CodeNumberDisplay)%>&nbsp;<%: Html.DisplayFor(model => model.SelectedCode.Name)%>
            <%: Html.HiddenFor(model => model.SelectedCode.CodeNumberDisplay)%>
        </td>
    </tr>    
    <tr id="selectMode">
        <td class="fieldLabel">Create <%:Utilities.GetResourceString(Constants.CODE) %> As:</td>
        <td class="text"><%: Html.RadioButtonFor(model => model.Child, true, new { id = "rbChild", @onclick = "setPageMode()" })%><label for="rbChild">Child</label>&nbsp;
                        <%: Html.RadioButtonFor(model => model.Child, false, new { id = "rbSibling", @onclick = "setPageMode()" })%><label for="rbSibling">Sibling</label></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(x => x.PhaseCode.CodeCategoryId) %>:</td>
        <td class="text">
            <%: Html.Telerik().DropDownListFor(model => model.PhaseCode.CodeCategoryId)
                .BindTo(Model.LookupCodeCategory)
                .HtmlAttributes(new { style = "width:200px;", onChange ="getCodeNumber();return false;" })%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(x => x.PhaseCode.CodeNumber) %>:</td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.PhaseCode.CodeNumber, new { disabled = "disabled" })%>
            <span class="error" style="display: none;">You have reached the last code number at the target code level.</span>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(x => x.PhaseCode.CodeName) %>:</td>
        <td class="text"><%: Html.TextBoxFor(model => model.PhaseCode.CodeName, new { style = "width: 300px;", maxlength = "200" })%></td>
    </tr>
</table>

<% if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
   { %>
    <p>
        <input type="button" value="<%=Constants.SAVE %>" name="save" id="btnSave" class="button" onclick="saveCode()" />
        <input type="button" value="<%=Constants.CANCEL %>" name="cancel" class="button" onclick="customCloseWindow(false, '')" />
    </p>
<% } %>

<%: Html.HiddenFor(model => model.SelectedCodeId) %>
<%: Html.HiddenFor(model => model.PhaseTypeId) %>
<%: Html.HiddenFor(model => model.PhaseId) %>
<!-- hidden fields to store original value for the input fields -->
<%: Html.Hidden("CodeNumber", Model.PhaseCode.CodeNumber)%>
<%: Html.Hidden("CodeCategoryId", Model.PhaseCode.CodeCategoryId)%>


<script type="text/javascript">
    setPageMode();
</script>