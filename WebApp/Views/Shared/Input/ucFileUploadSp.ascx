﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<string, object>>" %>

<% for (int i = 1; i <= int.Parse(ConfigurationManager.AppSettings["fileUploadCount"]); i++)
   { %>
    <div id="fileUpload<%=i.ToString() %>" class="invisible">
        <table>
        <% if ((bool) Model["ShowDocumentName"])
        { %>
            <tr>
                <td><label for="DocumentName<%=i.ToString() %>" class="fieldLabel">Document Name <%=i.ToString() %></label></td>
                <td><input type="text" name="DocumentName<%=i.ToString() %>" id="DocumentName<%=i.ToString() %>" maxlength="500" /></td>
                <td>&nbsp;</td>
            </tr>
        <% }%>
            <tr>
                <td><label for="FileUpload<%=i.ToString() %>" class="fieldLabel">Archivo <%=i.ToString() %></label></td>
                <td><input type="file" name="FileUpload<%=i.ToString() %>" id="FileUpload<%=i.ToString() %>"  /></td>
                <td>
                    <% if ( i < int.Parse(ConfigurationManager.AppSettings["fileUploadCount"]))
                       { %>
                        <a class="button" id="show<%=i.ToString() %>" href="javascript:void(0);">&nbsp;+&nbsp;</a>
                    <% } %>
                    <% if (i > 1)
                       { %>
                        <a class="button" id="hide<%=i.ToString() %>" href="javascript:void(0);">&nbsp;-&nbsp;</a>
                    <% } %>
                </td>
            </tr>
        </table>
        <br />
    </div>
<% } %>
<span class="text"><i>(El tamaño total de los archivos cargados no puede superar &nbsp;<%=((int) Model["MaxUploadSize"] / 1024000) %>&nbsp;MBs.)</i></span>
<br />
<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.fileupload.js"));
%>