﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CodeTaskAssignmentViewModel>" %>

<% Html.Telerik().Grid<Aquilent.Cara.Domain.PhaseCode>(Model.CurrentPhaseCodes)
       .Name("CodeTaskGrid")
       .Columns(columns =>
           {
               columns.Bound(code => code.CodeNumberDisplay).Title("Code Number");
               columns.Bound(code => code.CodeName).Title("Name");
               columns.Bound(code => code.UserId).Title("Assigned To")
                   .Template(code =>
                   {%>
                        <%: Html.Telerik().DropDownList()
                        .BindTo(Model.TeamMembers)
                        .ClientEvents(events => events.OnChange("tuserOnChange"))
                        .Name("user_" + code.PhaseCodeId.ToString())
                        .SelectedIndex(Model.SelectedIndex(code.UserId))%>
                   <%});
           })
            .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY).Render();%>