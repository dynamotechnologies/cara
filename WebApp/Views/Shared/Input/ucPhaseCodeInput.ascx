﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseCodeViewModel>" %>

<a id="btnExpand1" class="button" href="#" onclick="toggleNodes('Expand')"><%: Utilities.GetResourceString("ExpandAll") %></a>
<a id="btnCollapse1" class="button" href="#" onclick="toggleNodes('Collapse')"><%: Utilities.GetResourceString("CollapseAll") %></a>
<br /><br />

<% List<int> checkedNodes = ViewData["checkedNodes"] as List<int>; %>
<% Html.Telerik().TreeView()
    .Name("TreeView")
    .ShowCheckBox(true)
    .ExpandAll(false)
    .ClientEvents(events => events.OnLoad("onLoad").OnChecked("onChecked").OnSelect("onSelect")
                    .OnCollapse("updateTreeViewState").OnExpand("updateTreeViewState"))
    .BindTo(Model.PhaseCodeTree, mappings =>
    {
        mappings.For<Aquilent.Cara.Domain.PhaseCodeSelect>(
            binding => binding.ItemDataBound((item, code) =>
            {
                string codeName = code.CodeNumberDisplay + (!string.IsNullOrEmpty(code.CodeName) ? " " + code.CodeName.Trim() : string.Empty);

                // expand the type level nodes
                // top level categories are always expanded
                if (string.IsNullOrEmpty(code.CodeName) && code.PhaseCodeId == 0)
                {
                    item.Checkable = false;
                }
                item.Expanded = ((string[])ViewData[Constants.EXPANDED_PHASE_CODE]).Contains(codeName);

                // check the node if it's in the view data or if it's already selected
                if (checkedNodes != null)
                {
                    //CARA1206
                    item.Checked = checkedNodes.Contains(code.CodeId.HasValue ? code.CodeId.Value : -1);
                }
                else
                {
                    item.Checked = code.PhaseId.HasValue;
                }

                // enable the checkbox only if it can be removed
                item.Enabled = code.CanRemove;
                item.Text = codeName;
                item.Value = code.CodeId.ToString();
            })
            .Children(code => code.ChildCodes));

        mappings.For<Aquilent.Cara.Domain.PhaseCodeSelect>(binding => binding.ItemDataBound((item, code) => { item.Text = code.CodeName; }));
    })
    .Render();

    Html.Telerik().Window()
        .Name("WindowCodeCreate")
        .Title(string.Format("Add Custom {0}", Utilities.GetResourceString(Constants.CODE)))
        .Draggable(true)
        .Resizable(resizing => resizing
                        .Enabled(true)
                        .MinHeight(250)
                        .MinWidth(250)
                        .MaxWidth(515)
                    )
        .Modal(true)
        .ClientEvents(events => events.OnLoad("WindowCodeCreate_OnLoad").OnOpen("setWindowFocus"))
        .Buttons(b => b.Maximize().Close())
        .Width(515)
        .Height(375)
        .Visible(false)
        .Render();

    Html.Telerik().Window()
        .Name("WindowCodeEdit")
        .Title(string.Format("Edit Custom {0}", Utilities.GetResourceString(Constants.CODE)))
        .Draggable(true)
        .Resizable(resizing => resizing
                        .Enabled(true)
                        .MinHeight(250)
                        .MinWidth(250)
                        .MaxWidth(500)
                    )
        .Modal(true)
        .ClientEvents(events => events.OnLoad("WindowCodeEdit_OnLoad").OnOpen("setWindowFocus"))
        .Buttons(b => b.Maximize().Close())
        .Width(500)
        .Height(250)
        .Visible(false)
        .Render(); %>

<br />
<a id="btnExpand2" class="button" href="#" onclick="toggleNodes('Expand')"><%: Utilities.GetResourceString("ExpandAll") %></a>
<a id="btnCollapse2" class="button" href="#" onclick="toggleNodes('Collapse')"><%: Utilities.GetResourceString("CollapseAll") %></a>
<input id="revertConfirm" type="hidden" value="false"/>
<br /><br />