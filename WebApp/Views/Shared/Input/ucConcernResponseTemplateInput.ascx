﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ConcernTemplateInputViewModel>" %>


<br />
<div class="fieldLabel">Concern Template</div>
<% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE))
   { %>
<%--<%: Html.EditorFor(model => model.ConcernText) %>--%>
<%: Html.TextAreaFor(model => model.ConcernText, new { style = "width:700px;height:600px;" })%>
<% } else { %>
    <div style="width: 700px;"><%=Model.ConcernText%></div>
<% } %>

<br />
<div class="fieldLabel">Response Template</div>
<% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE))
   { %>
    <%--<div style="width: 700px;"><%: Html.EditorFor(model => model.ResponseText) %></div>--%>
    <%: Html.TextAreaFor(model => model.ResponseText, new { style="width:700px;height:600px;"})%>
 <% }
   else
   { %>
    <div style="width: 700px;"><%=Model.ResponseText%></div>
<% } %>
<br />
<%
    Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/tinymce/tinymce.js")
                .Add("~/Scripts/util.tinymce.js")
                );         %>