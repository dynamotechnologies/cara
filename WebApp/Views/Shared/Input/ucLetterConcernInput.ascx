﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ConcernResponseInputViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<script type="text/javascript">
<!--
    var responseTinyMce = new CaraTinyMce(625, 175, "ResponseText");
	$(document).ready(function () {
		// ------------------------------------------------------------------------------------------------------
		// set up input element action events to validate the input text
	    // ------------------------------------------------------------------------------------------------------
	    responseTinyMce.Init();
	    $("textarea").bind("keyup paste", validateTextHandler);
	});

    // close the window and refresh the comment input on parent
    function refreshComment()
    {
        closeWindow("WindowConcern");

        setCommentInputArea('<%=Model.LetterCommentNumber %>');
    }

    // set the concern response status field
    function setStatus(statusId) {
        responseTinyMce.Update();
        $("#ConcernResponse_ConcernResponseStatusId").val(statusId);
    }
-->
</script>

<div id="inputArea">
<% using (Ajax.BeginForm("SaveConcern", Constants.LETTER, null, new AjaxOptions { UpdateTargetId = "inputArea", OnSuccess = "refreshComment" }))
   { %>
    <div class="fieldLabel"><label for="ConcernResponse_ResponseText">Response</label></div>
    
    <%: Html.TextAreaFor(model => model.ResponseText, new { style = "width:575px;height:150px;" })%>
<!--<% Html.Telerik().EditorFor(model => model.ConcernResponse.ResponseText)
       .Tools(tools => Utilities.EditorToolBar(tools))
       .HtmlAttributes(new { style = "height:150px; width:575px;" })
       .Render(); %>-->
    <% if (Utilities.UserHasPrivilege("CODE", Constants.PHASE))
       { %>
        <p>
            <input type="submit" value="<%=Constants.SAVE + " Response In Progress" %>" class="button"
                onclick="setStatus(<%=LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList.Where(x => x.Value == "Response In Progress").Select(y => y.Key).First() %>)" />
            <input type="submit" value="<%=Constants.SAVE + " Response Complete" %>" class="button"
                onclick="setStatus(<%=LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList.Where(x => x.Value == "Response Complete").Select(y => y.Key).First() %>)" />
            <input type="button" value="<%=Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('WindowConcern')" />
        </p>
        <% } %>
        <%: Html.HiddenFor(model => model.CommentId)%>
        <!-- Hidden fields for concern response model -->
        <%: Html.HiddenFor(model => model.ConcernResponse.ConcernResponseStatusId)%>
        <%: Html.HiddenFor(model => model.ConcernResponse.ConcernCreated)%>
        <%: Html.HiddenFor(model => model.ConcernResponse.ConcernCreatedBy)%>
        <%: Html.HiddenFor(model => model.ConcernResponse.ResponseCreated)%>
        <%: Html.HiddenFor(model => model.ConcernResponse.ResponseCreatedBy)%>
    <% } %>
</div>