﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterUnpublishReasonInputViewModel>" %>

<% if (Model != null)
   { %>
    <table>
        <tr>
            <td class="fieldLabel"><label for="UnpublishedReasonId">Reason:</label></td>
            <td class="text"><%: Html.Telerik().DropDownListFor(x => x.UnpublishedReasonId)
                                        .Name("ddlUnpublishedReason")
                                        .BindTo(Model.LookupUnpublishedReason)                                        
                                        .HtmlAttributes(new { style = "width:250px;" }) %></td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="UnpublishedReasonOther">If Other, Please Specify:</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.UnpublishedReasonOther, new { style = "width:250px;" })%></td>
        </tr>
    </table>
    <br />
    <input type="button" value="<%:Constants.SAVE %>" name="save" class="button" onclick="saveUnpublishReason()" />
    <input type="button" value="<%:Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('WindowUnpublish')" />
<% } %>