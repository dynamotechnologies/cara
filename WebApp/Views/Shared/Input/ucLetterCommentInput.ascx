﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Resource" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>

<script type="text/javascript">
<!--
	$(document).ready(function () {
		// ------------------------------------------------------------------------------------------------------
		// set up input element action events to validate the input text
		// ------------------------------------------------------------------------------------------------------
		$("#Comment_Annotation, #Comment_NoResponseReasonOther").bind("keyup paste", validateTextHandler);
	});
// -->
</script>

<% object htmlAttributes = new { style = "width:200px;", @disabled = "disabled" };
   object chkHtmlAttributes = new { @disabled = "disabled" };
    
   if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
    {
        htmlAttributes = new { style = "width:200px;" };
        chkHtmlAttributes = null;
    }
     %>
   <% if (Model.Comment != null)
   { %>
   <a id="btnPrevComment1" href="#" onclick="javascript:prevCommentOnly();"><img src="<%= ResolveUrl("~/Content/Images/icon_prev.png")%>" alt="Previous Comment" title="Previous Comment" /></a>&nbsp;
    <span class="fieldLabel"><%: Constants.COMMENT %> #</span><span id="currentCommentId" class="text"><%: Model.Comment.LetterCommentNumber%></span>&nbsp;
    <a id="btnNextComment1" href="#" onclick="javascript:nextCommentOnly();"><img src="<%= ResolveUrl("~/Content/Images/icon_next.png")%>" alt="Next Comment" title="Next Comment" /></a><br /><br />
    <% if (Model.Comment.ConcernResponseId.HasValue)
       { %>
        <span class="fieldLabel"><%=Utilities.GetResourceString(Constants.CONCERN)%> #</span><span id="currentConcernResponseId" class="text"><%: Html.ActionLink(Model.Comment.ConcernResponse.ConcernResponseNumber.ToString(), Constants.INPUT,
                                                                                                               Constants.CONCERN, new { projectId = Model.ProjectId, phaseId = Model.PhaseId, id = Model.Comment.ConcernResponseId,
                                                                                                                                        trackBackPage = Constants.CODING, trackBackLetterId = Model.Letter.LetterId, 
                                                                                                                                        trackBackCommentNum = String.Format("{0}-{1}",Model.Letter.LetterSequence, Model.Comment.CommentNumber)},
                                                                                                                null)%></span><br /><br />
    <% } %>
    <a id="btnCommentFocus" href="#" class="button" onclick="javascript:setCommentFocus();">Jump to Text</a>
    <% if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && !Model.Comment.ConcernResponseId.HasValue) 
       { %>
        <a class="button" id="btnAddResponse" onclick="javascript:openWindowConcern('<%=Model.PhaseId %>', '<%=Model.ProjectId %>', '<%=Model.LetterId %>', '<%=Model.Comment.CommentId %>', '<%=Model.Comment.LetterCommentNumber %>')">Respond Now</a >
    <% } %>
    <br /><br />
    <%  if (Model.Comment.CommentCodes != null)
        {
            bool isLast = (Model.Comment.CommentCodes.Count <= 1);
            %><%
            foreach (Aquilent.Cara.Domain.CommentCode code in Model.Comment.CommentCodes.OrderBy(x => x.Sequence))
            { %>
                
                    <% if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
                       { %>
                        
                            <a href="#" onclick="javascript:deleteCommentCode('<%=isLast %>', <%=code.CommentId %>, <%=code.PhaseCodeId %>);"><img src="<%= ResolveUrl("~/Content/Images/remove.png")%>" alt="Remove" title="Remove" /></a>
                        
                    <% } %>&nbsp;
                    <%= code.PhaseCode.CodeNumberDisplay %>&nbsp<%= code.PhaseCode.CodeName %><br />
                <%
            }
            %>
            <%: Html.Hidden("commentCodeCount", Model.Comment.CommentCodes.Count) %>
            <%: Html.Hidden("issueActionCodeCount", Model.Comment.CommentCodes.Where(x => x.PhaseCode.CodeNumber.StartsWith("1")).ToList().Count) %>
            <%
        } %>
    <br />      
    <%--  //*Added for comment code sorting--%>  

        <a class="button" id="btnCommentCodeReorder" style="display: none;" onclick="javascript:openWindowCommentCode('<%=Model.PhaseId %>','<%=Model.Comment.LetterCommentNumber %>')">Reorder</a >            
                                            
               
        <%--<% Html.RenderPartial("~/Views/Shared/Input/ucCommentCodeSort.ascx", Model); %>--%>
       <br /><br />                  
          
    
    <%: Html.CheckBoxFor(m => m.Comment.SampleStatement, chkHtmlAttributes)%><%: Html.LabelFor(m => m.Comment.SampleStatement)%><br />
    <%: Html.CheckBoxFor(m => m.Comment.NoResponseRequired, chkHtmlAttributes)%><%: Html.LabelFor(m => m.Comment.NoResponseRequired)%>
    <div id="noResponseReason" style="padding: 3px 0px 3px 12px">
        <label for="NoResponseReasonId">No Further Response Reason:</label><%: Html.Telerik().DropDownListFor(m => m.Comment.NoResponseReasonId)
                                 .BindTo(Model.LookupNoResponseReason)
                                 .HtmlAttributes(htmlAttributes)%>
        <br />
        <%: Html.LabelFor(m => m.Comment.NoResponseReasonOther) %>:&nbsp;<br /><%: Html.TextBoxFor(m => m.Comment.NoResponseReasonOther, htmlAttributes)%>
    </div>    
    <%: Html.LabelFor(m => m.Comment.Annotation)%><br />
    <%: Html.TextAreaFor(m => m.Comment.Annotation, 4, 20, htmlAttributes)%>
    <br />
    <br />
    <% if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
       { %>
        <a id="btnSaveComment" href="#" class="button" onclick="javascript:saveComment('<%=Model.Comment.CommentId %>');"><%=Constants.SAVE %></a>
        <a id="btnDeleteComment" href="#" class="button" onclick="javascript:deleteComment('<%=Model.Comment.CommentId %>');"><%=Constants.DELETE %></a>
        <a id="btnCancelComment" href="#" class="button" onclick="javascript:cancelComment('<%=Model.Comment.LetterCommentNumber %>');">Undo Changes</a>
    <% } %>
<% }
   else
   { %>
    <span class="text">No <%=Constants.COMMENT %> Selected</span>
<% } %>

    
 <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.concernlist.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/telerik.window.min.js"));
            %>


<script type="text/javascript">
    function openWindowCommentCode(PhaseId, LetterCommentNumber) {
        if (PhaseId == '0' || PhaseId === null || PhaseId === '' || PhaseId === undefined) {
            PhaseId = document.getElementById("PhaseId").value;
        }        
        openWindow('WindowCommentCode', '/Letter/SortCodeSequences?phaseId=' + PhaseId + '&letterCommentNumber=' + LetterCommentNumber);

        
    }

    function openWindowConcern(PhaseId, ProjectId, LetterId, CommentId, LetterCommentNumber) {
        if (PhaseId == '0' || PhaseId === null || PhaseId === '' || PhaseId === undefined) {
            PhaseId = document.getElementById("PhaseId").value;
        }
        openWindow('WindowConcern', '/Project/' +
                ProjectId + '/Phase/' +
                PhaseId + '/Letter/CreateConcern/' +
                LetterId + '/?commentId=' +
                CommentId + '&letterCommentNumber=' +
                LetterCommentNumber);
        
    }

</script>