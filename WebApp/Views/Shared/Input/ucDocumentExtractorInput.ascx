﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DocumentExtractorViewModel>" %>

<% if (Model != null)
   { %>
    <table width="850">
        <tr>
            <td colspan="2">
                From this page you can view the content as it was processed and how it will look once you have added it to the letter. Please review and correct any formatting errors before clicking on Add to Letter.<br /><br />
                Once you have added this document text to the letter, you will no longer be able to make any changes to the text added to the letter.
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><%=Constants.DOCUMENT%>&nbsp;<%: Html.LabelFor(model => model.Document.Name)%>:</td>
            <td class="text"><%: Html.DisplayFor(model => model.Document.Name)%></td>
        </tr>
        <tr>
            <td class="fieldLabel" valign="top"><label for="ExtractedText">Content:</label></td>
            <td class="text"><%: Html.TextAreaFor(model => model.ExtractedText, 20, 95, null)%></td>
        </tr>
    </table>
<% } %>