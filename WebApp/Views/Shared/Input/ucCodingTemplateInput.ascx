﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.TemplateCodeViewModel>" %>

<% List<TreeViewItem> checkedNodes = ViewData["checkedNodes"] as List<TreeViewItem>; %>

<table>
    <tr><td class="fieldLabel"><label for="TemplateName">Template Name:</label></td>
    <td class="text"><%: Html.TextBoxFor(model => model.TemplateName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top">List of <%:Utilities.GetResourceString(Constants.CODE).Pluralize() %>:</td>
        <td class="text">
        
        <% Html.Telerik().TreeView()
            .Name("TreeView")
            .ShowCheckBox(true)
            .ExpandAll(true)
            .ClientEvents(events => events.OnChecked("onChecked"))
            .BindTo(Model.TemplateCodeTree, mappings =>
            {
                mappings.For<Aquilent.Cara.Domain.TemplateCodeSelect>(
                    binding => binding.ItemDataBound((item, code) =>
                    {
                        // expand the type level nodes
                        if (string.IsNullOrEmpty(code.Name) && code.CodeId == 0)
                        {
                            item.Checkable = false;
                            item.Expanded = true;
                        }
                        
                        if (checkedNodes != null)
                        {
                            item.Checked = checkedNodes.Where(e => e.Value.Equals(code.CodeId.ToString())).FirstOrDefault() != null;
                        }
                        else
                        {
                            item.Checked = code.CodingTemplateId.HasValue;
                        }
                        item.Text = code.CodeNumberDisplay + " " + code.Name;
                        item.Value = code.CodeId.ToString();
                    })
                    .Children(code => code.ChildCodes));

                mappings.For<Aquilent.Cara.Domain.TemplateCodeSelect>(binding => binding.ItemDataBound((item, code) => { item.Text = code.Name; }));
            })
            .Render(); %>
        </td>
    </tr>
</table>