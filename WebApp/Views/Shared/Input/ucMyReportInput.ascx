﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.MyReportModel>" %>

<% if (Model != null)
   { %>
    <table>
        <tr>
            <td colspan="2" class="text">This will save the parameters of this report so that you can run it again 
            at your convenience.  If you'd like to save the results of this report, you can do so from the Report Viewer.</td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(model => model.Name)%></td>
            <td class="text"><%: Html.TextBoxFor(model => model.Name)%></td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(model => model.Private)%></td>
            <td class="text">
                <%: Html.RadioButtonFor(model => model.Private, false)%>Shared&nbsp;
                <%: Html.RadioButtonFor(model => model.Private, true)%>Private
            </td>
        </tr>
    </table>
    <%: Html.HiddenFor(x=>x.myReportId) %>
    <br />
    <% if (Utilities.UserHasPrivilege("RRPT", Constants.SYSTEM))
       { %>
        <input type="button" value="<%=Constants.SAVE %>" name="save" class="button" onclick="saveMyReport()" />
        <input type="button" value="<%=Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('WindowMyReport')" />
    <% } %>
<% } %>