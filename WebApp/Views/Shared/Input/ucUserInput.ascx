﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Framework.Security.User>" %>

<% if (Model != null) { %>
<table>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><%: Html.LabelFor(usr => usr.Username) %>:</td>
        <td class="text"><%: Html.DisplayFor(usr => usr.Username) %></td>
    </tr>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><%: Html.LabelFor(usr => usr.FirstName)%>:</td>
        <td class="text"><%: Html.TextBoxFor(usr => usr.FirstName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><%: Html.LabelFor(usr => usr.LastName)%>:</td>
        <td class="text"><%: Html.TextBoxFor(usr => usr.LastName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><span class="required">*</span><%: Html.LabelFor(usr => usr.Username) %>:</td>
        <td class="text"><%: Html.TextBoxFor(usr => usr.Email)%></td>
    </tr>
</table>
<% } %>