﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>


    <% Html.Telerik().TabStrip()
        .Name("CodingBar")
        .Items(items =>
        {
            if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
            {
                items.Add()
                    .Text("Codes")
                    .Content(() =>
                    {%>
                    <div id="codeInputArea" class="tabArea">
                        <%: Html.TextBoxFor(model => model.CodeSearchText, new { style = "width:135px;" })%>
                        <a id="btnSearchCodes" href="#" class="button" onclick="javascript:searchCodes();"><%:Constants.FIND %></a>
                        <a id="btnShowAllCodes" href="#" class="button" onclick="javascript:showAllCodes();"><%:Constants.CLEAR %></a>
                        <br />
                        <a href="#" onclick="javascript:openWindow('WindowCodeFilter', '/Project/<%=Model.Letter.Phase.ProjectId %>/Phase/<%=Model.Letter.PhaseId %>/Comment/SearchCode');">Advanced <%=Constants.SEARCH %></a>
                        <br /><br />
                        <a id="btnAddCommentTop" href="#" class="button" onclick="javascript:addCodes(<%=Model.Letter.PhaseId %>, <%=Model.Letter.LetterId %>);"><%:Constants.APPLY %>&nbsp;<%:Constants.CODE.Pluralize()%></a>
                        <a id="btnCancelCommentTop" href="#" class="button" onclick="javascript:resetComment();"><%:Constants.CANCEL %></a>
						<br /><br />
                        <div id="codeListArea">
                            <% Html.RenderPartial("~/Views/Shared/List/ucLetterPhaseCodeSelect.ascx", new LetterPhaseCodeViewModel
                               {
                                   PhaseId = Model.PhaseId,
                                   Expand = false
                               }); %>
                        </div>
                        <br />
                        <a id="btnAddComment" href="#" class="button" onclick="javascript:addCodes(<%=Model.Letter.PhaseId %>, <%=Model.Letter.LetterId %>);"><%:Constants.APPLY %>&nbsp;<%:Constants.CODE.Pluralize()%></a>
                        <a id="btnCancelComment" href="#" class="button" onclick="javascript:resetComment();"><%:Constants.CANCEL %></a>
                    </div>
                <%}).Selected(true);
            }

            items.Add()
                .Text(Constants.COMMENT)
                .Content(() =>
                {%>
                <div id="commentInputArea" class="tabArea"></div>
            <%});

            if (!Model.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE) && Model.Letter.LetterTypeId != 1 /* = Duplicate */)
            {
                items.Add()
                    .Text(Utilities.GetResourceString(Constants.TERM_LIST))
                    .Content(() =>
                    {%>
                    <div id="termListInputArea" class="tabArea">
                        <div id="termListArea">
                            <% Html.RenderPartial("~/Views/Shared/List/ucLetterTermConfirmationList.ascx", Model); %>
                        </div>
                      <br />
                      
                    <a class="button" id="btnManageTermList" href="#" onclick="openWindow('WindowTermList', '/Project/<%=Model.Letter.Phase.ProjectId %>/Phase/<%=Model.Letter.PhaseId %>/Letter/ManageTermList/<%=Model.Letter.LetterId %>')"><%:Constants.SELECT %>&nbsp;<%:Utilities.GetResourceString(Constants.TERM_LIST).Pluralize()%></a >
                    </div>
                    <%}).Visible(true);
            }
        }
    ).Render(); %>

<script src="<%= Url.Content("~/Scripts/MicrosoftAjax.js") %>" type="text/javascript"></script>
<script src="<%= Url.Content("~/Scripts/MicrosoftMvcAjax.js") %>" type="text/javascript"></script>

<div>
    <%: Html.HiddenFor(m => m.Letter.LetterTypeId) %>
 </div>

