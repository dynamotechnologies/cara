﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PublicCommentFormInputViewModel>" %>

<br />
<div class="fieldLabel">Reading Room Text</div>
<% if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
   { %>
<%: Html.TextAreaFor(model => model.ReadingRoomText)%>
<% } else { %>
    <div style="width: 700px;"><%=Model.ReadingRoomText%></div>
<% } %>

<%
        Html.Telerik()
                .ScriptRegistrar()
                .Scripts(script => script
                    .Add("~/Scripts/tinymce/tinymce.js")
                    .Add("~/Scripts/util.tinymce.js")
                    );
%>
