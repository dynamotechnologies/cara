﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PublicCommentFormInputViewModel>" %>

<br />
<div class="fieldLabel"><%= Constants.PUBLIC_COMMENT_FORM %> Text</div>
<% if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
   { %>
<%: Html.TextAreaFor(model => model.PublicFormText)%>
<% } else { %>
    <div style="width: 700px;"><%=Model.PublicFormText%></div>
<% } %>&nbsp;&nbsp;

<% if(Model.IsPalsProject)
   { %>
        <div class="fieldLabel"><%= "Public Comment Contact Information" %> </div> 
        <br /><br />
        <span class="fieldLabel">Update Contact Information?</span>&nbsp;&nbsp;
        <%: Html.CheckBoxFor(model => model.IsContactUpdated, new {onclick = "showHide(this)"}) %>
         <br /><br />
          
             <div id="updateContact" <%=(Model.IsContactUpdated) ? "style='display: block;'" : "style='display: none;'" %>>
                 <table>
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("Contact Name")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.ContactName, new { id = "txtContactName", Value = Model.ContactName })%></td>
                        <td class="fieldLabel"><%: Html.Label("Email Id")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.ContactEmail, new { id = "txtContactEmail", Value = Model.ContactEmail })%></td>
                    </tr>
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("Address 1")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.Address1, new { id = "txtAddress1", Value = Model.Address1 })%></td>
                        <td class="fieldLabel"><%: Html.Label("Address 2")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.Address2, new { id = "txtAddress2", Value = Model.Address2 })%></td>
                    </tr> 
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("City")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.City, new { id = "txtCity", Value = Model.City })%></td>
                        <td class="fieldLabel"><%: Html.Label("State")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.State, new { id = "txtState", Value = Model.State })%></td>
                    </tr> 
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("Zip")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.Zip, new { id = "txtZip", Value = Model.Zip})%></td>
                    </tr>

                </table>
                </div>
         
            <div id="readonlyContact" <%=(!Model.IsContactUpdated) ? "style='display: block;'" : "style='display: none;'" %>>
                <table>
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("Contact Name")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.ContactName, new { id = "txtContactName", Value = Model.ContactName, disabled = "disabled" })%></td>
                        <td class="fieldLabel"><%: Html.Label("Email Id")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.ContactEmail, new { id = "txtContactEmail", Value = Model.ContactEmail, disabled = "disabled" })%></td>
                    </tr>
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("Address 1")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.Address1, new { id = "txtAddress1", Value = Model.Address1, disabled = "disabled" })%></td>
                        <td class="fieldLabel"><%: Html.Label("Address 2")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.Address2, new { id = "txtAddress2", Value = Model.Address2, disabled = "disabled" })%></td>
                    </tr> 
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("City")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.City, new { id = "txtCity", Value = Model.City, disabled = "disabled" })%></td>
                        <td class="fieldLabel"><%: Html.Label("State")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.State, new { id = "txtState", Value = Model.State, disabled = "disabled" })%></td>
                    </tr> 
                    <tr>
                        <td class="fieldLabel"><%: Html.Label("Zip")%></td>
                        <td class="text"><%: Html.TextBoxFor(model => model.Zip, new { id = "txtZip", Value = Model.Zip, disabled = "disabled" })%></td>
                    </tr>

                </table>
            </div>
   

<% } %>

<%
        Html.Telerik()
                .ScriptRegistrar()
                .Scripts(script => script
                    .Add("~/Scripts/tinymce/tinymce.js")
                    .Add("~/Scripts/util.tinymce.js")
                    );
%>
