﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.MemberViewModel>" %>

<div id="memberListArea"><% Html.RenderPartial("~/Views/Shared/List/ucMemberList.ascx", Model); %></div>
<br /><hr /><br />
<% if (Utilities.UserHasPrivilege("MTM", Constants.PHASE))
    { %>
<div id="text">
    <span class="fieldLabel">I want to:</span>&nbsp;&nbsp;
    <%: Html.RadioButton("rbType", Constants.ADD, Model.Type.Equals(Constants.ADD), new { onclick = "showHide(this)" })%>Add Member from Directory&nbsp;&nbsp;
    <%: Html.RadioButton("rbType", Constants.REUSE, Model.Type.Equals(Constants.REUSE), new { onclick = "showHide(this)" })%>Reuse <%:Utilities.GetResourceString("TeamMember").Pluralize()%>&nbsp;&nbsp;
    <%: Html.RadioButton("rbType", Constants.NONE, new { onclick = "showHide(this)" })%>Do Not Wish to Add New Members Now
</div>
<% } %>
<br />
<div id="addArea" <%=Model.Type.Equals(Constants.ADD) ? "style='display: block;'" : "style='display: none;'" %>>
    <table>
        <tr>
            <td class="fieldLabel"><label for="txtFirstName">First Name:</label></td>
            <td class="text">
                <%: Html.TextBox("txtFirstName", Model.FirstName)%>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><label for="txtLastName">Last Name:</label></td>
            <td class="text"><%: Html.TextBox("txtLastName", Model.LastName)%>&nbsp;
                <input type="submit" id="btnSearchUsers" class="button" name="action" value="Search Users" />
            </td>
        </tr>
    </table>
    <br /><br />
    <% if (Model.UserViewModel != null && Model.UserViewModel.Users != null)
       {
           Html.RenderPartial("~/Views/Shared/List/ucMemberSelect.ascx", Model.UserViewModel); %>
           <br />
           <input type="submit" class="button" name="action" value="Add User" />
    <% } %>
</div>
<div id="importArea" <%=Model.Type.Equals(Constants.REUSE) ? "style='display: block;'" : "style='display: none;'" %>>
    <label for="txtProjectNameOrId" class="fieldLabel">Search <%:Utilities.GetResourceString(Constants.PROJECT).Pluralize()%> by ID or Name</label>&nbsp;&nbsp;
    <%: Html.TextBox("txtProjectNameOrId", Model.ProjectNameOrId, new { maxlength = "50", size = "100" })%>
    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by project ID or name." />
    <input type="submit" id="btnSearchProjects" class="button" name="action" value="Search Projects" />
    <br /><br />
    <% if (Model.Projects != null)
       {
           Html.RenderPartial("~/Views/Shared/List/ucProjectPhaseSelect.ascx", Model.Projects); %>
           <br />
           <input type="submit" id="btnReuseUsers" class="button" name="action" value="Reuse Users" />
           <hr />
    <% } %>
</div>
<br /><br />

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.memberinput.js")
            .Add("~/Scripts/util.confirmmessage.js"));
%>
