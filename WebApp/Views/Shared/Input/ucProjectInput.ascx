﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProjectInputViewModel>" %>

<div id="text">
    <span class="fieldLabel">Is your <%=Constants.PROJECT.ToLower() %> in <%=Utilities.GetResourceString("PALS") %>?</span>&nbsp;&nbsp;
    <% if (Model.ProjectId == 0)
       { %>
    <%: Html.RadioButtonFor(model => model.IsPalsProject, true, new { onclick = "showHide(this)" })%>Yes&nbsp;&nbsp;
    <%: Html.RadioButtonFor(model => model.IsPalsProject, false, new { onclick = "showHide(this)" })%>No
    
    <% }
       else
       { %>
    <%= (Model.IsPalsProject.Value ? "Yes" : "No") %>&nbsp;&nbsp;
    <%: Html.HiddenFor(model => model.IsPalsProject) %>
    <% } %>   

</div>
<br />
<div id="palsProject" <%=(Model.IsPalsProject.HasValue && Model.IsPalsProject.Value) ? "style='display: block;'" : "style='display: none;'" %>>
    <div class="text"><span class="required">*</span>&nbsp;=&nbsp;Required</div>
    <table>
    <% if (Model.ProjectId == 0)
       { %>
        <tr>
            <td class="fieldLabel"><span class="required">*</span><label for="ProjectNameOrId">Enter a <%=Utilities.GetResourceString("PALS")%> ID or <%=Constants.PROJECT%> Name:</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.ProjectNameOrId)%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by PALS project ID or name." />
            &nbsp;<input type="submit" id="btnSearchProjects" class="button" name="action" value="Search Projects" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <% if (Model.DataMartProjectViewModel != null && Model.DataMartProjectViewModel.Projects != null)
                   {
                       Html.RenderPartial("~/Views/Shared/List/ucDataMartProjectSelect.ascx", Model.DataMartProjectViewModel);
                   } %>
            </td>
        </tr>
    <% } 
       else 
       { %>
       <tr>
           <td class="fieldLabel">Project Name and Number:</td>
           <td><%: Html.DisplayFor(model => model.Project.ProjectNameNumber) %></td>
       </tr>
    <% } %>
    </table>
</div>
<div id="nonPalsProject" <%=(Model.IsPalsProject.HasValue && !Model.IsPalsProject.Value) ? "style='display: block;'" : "style='display: none;'" %>>

    <table>
    <tr>
    <td colspan="4" class="text"><%: Html.CheckBoxFor(model => model.IsOrmsProject)%>&nbsp;<label for="ORMS">Is your project ORMS? </label></td>
    </tr>
        <tr>
            <td class="fieldLabel">&nbsp;&nbsp;&nbsp;<span class="required">*</span><label for="ProjectName"><%=Constants.PROJECT %> Name:</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.NonPalsProjectName, new { maxlength = 100, size = 200 })%></td>
        </tr>
        <tr>
            <td class="fieldLabel" valign="top">&nbsp;&nbsp;&nbsp;<span class="required">*</span><label for="NonPalsDescription">Comment Web<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form Description:</label></td>
            <td class="text">
                <%: Html.TextAreaFor(model => model.NonPalsDescription, new { rows = 6, cols = 50 })%>
                <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="DescriptionToolTip" 
                    title="This information will appear at the top of your publically-visible comment period web form. To include a link in the description, please use this format:  [Google](http://www.google.com/).  The URL must always begin with 'http://' or 'https://'" />
                <br />
                <span id="countdown" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel" valign="top"><span class="required">*</span>Lead Management Unit:</td>
            <td valign="top">
                <%: Html.TextBoxFor(model => model.NonPalsUnitName, new { disabled = "disabled", size = "40" }) %>
                <% Html.RenderPartial("~/Views/Shared/Util/ucActiveUnitTreeSelector.ascx"
                       , new UnitTreeSelectorModel
                       {
                           Units = Model.Units,
                           Visible = false,
                           Closeable = true,
                           Modal = true,
                           UnitIdInputId = "NonPalsUnit",
                           UnitNameInputId = "NonPalsUnitName"
                       }); %>
                <%: Html.HiddenFor(model => model.NonPalsUnit) %>
                <a id="btnChooseUnit" class="button" onclick="$('#UnitSelectorWindow').data('tWindow').center().open()">Choose Unit</a>
            </td>
        </tr>
    </table>
</div>



