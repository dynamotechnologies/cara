﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CodingTemplateViewModel>" %>

<div id="text">
    <span class="fieldLabel">I want to:</span>&nbsp;&nbsp;
    <%: Html.RadioButton("rbType", Constants.ADD, Constants.ADD.Equals(Model.Type), new { onclick = "showHide(this)" })%>Select <%=Utilities.GetResourceString("CodingTemplate") %> From System&nbsp;&nbsp;
    <%: Html.RadioButton("rbType", Constants.REUSE, Constants.REUSE.Equals(Model.Type), new { onclick = "showHide(this)" })%>Reuse <%=Utilities.GetResourceString("CodingStructure") %>
</div>
<br />
<div id="addArea" <%=Constants.ADD.Equals(Model.Type) ? "style='display: block;'" : "style='display: none;'" %>>
    <% if (Model.CodingTemplates != null)
       {
           Html.RenderPartial("~/Views/Shared/List/ucCodingTemplateSelect.ascx", Model.CodingTemplates); %>
           <br />
    <% } %>
</div>
<div id="importArea" <%=Constants.REUSE.Equals(Model.Type) ? "style='display: block;'" : "style='display: none;'" %>>
    <label for="txtProject" class="fieldLabel">Search <%:Utilities.GetResourceString(Constants.PROJECT).Pluralize()%> by ID or Name</label>&nbsp;&nbsp;
    <input type="text" name="txtProject" id="txtProject" size="100" maxlength="50" value="<%=Model.ProjectNameOrId%>" />
    <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by project ID or name." />
    <input type="submit" class="button" name="action" value="Search Projects" />
    <br /><br />
    <% if (Model.Projects != null)
       {
           Html.RenderPartial("~/Views/Shared/List/ucProjectPhaseSelect.ascx", Model.Projects); %>
           <br />
    <% } %>
</div>