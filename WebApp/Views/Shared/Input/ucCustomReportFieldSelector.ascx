﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>

                        <table><tr><td>
                        <label for="FieldId" class="fieldLabel">Select Search Filter  </label>
                        </td>
                        <td valign="top">
                        <%= Html.Telerik().DropDownList()
                            .BindTo(Model.SelectListFields())
                            .ClientEvents(x => x.OnChange("FieldSelect"))
                            .Name("FieldId")
                            .HtmlAttributes(new { style = "width: 200px;" })%><td valign="top"></tr></table>

                    <div id="TextFieldDiv" style="display: none">
                    <table><tr><td>
                        <label for="StringFieldValue" id="StringFieldLabel" class="fieldLabel"></label></td></tr>
                        <tr><td>
                        <%:Html.TextBox("StringFieldValue", "", new { Class = "text", onkeypress = "TextFieldKeyPress()" })%>
                        <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="textFieldTool" title="" />
                        <input type="button" class="button" value="Include" onclick="AddStringValue(1)" />
                        <input type="button" class="button" value="Exclude" onclick="AddStringValue(0)" /></td></tr></table>
                    </div>
                    <div id="DateFieldDiv" style="display: none">
                        <table>
                            <tr>
                                <td><label for="FromDateFieldValue" class="fieldLabel">From:</label></td>
                                <td><% Html.Telerik().DatePicker().Name("FromDateFieldValue").Render(); %></td>
                            </tr>
                            <tr>
                                <td><label for="ToDateFieldValue" class="fieldLabel">To: </label></td>
                                <td><% Html.Telerik().DatePicker().Name("ToDateFieldValue").Render(); %></td>
                            </tr>
                            <tr><td><input type="button" class="button" value="Include" onclick="AddDateValue()" /></td></tr>
                        </table>
                    </div>
                    <div id="DropdownFieldDiv" style="display: none">
                    <table><tr>
                    <td valign="top">
                    <label for="DropdownFieldValue" id="DropdownFieldLabel" class="fieldLabel"></label>
                    </td></tr>
                    <tr>
                    <td valign="top">
                    <%= Html.Telerik().DropDownList()
                        .Name("DropdownFieldValue")
                        .HtmlAttributes(new { style = "width: 200px;" })%>
                    </td>
                    <td valign="top">
                    <input type="button" class="button" value="Include" onclick="AdddropdownValue(1)" />
                    </td>
                    <td valign="top">
                    <input type="button" class="button" value="Exclude" onclick="AdddropdownValue(0)" /> 
                    </td>
                    </tr></table>

                    </div>
                    <div id="TreeDropdownFieldDiv" style="display: none">
                    <table>
                        <tr><td>
                <% Html.RenderPartial("~/Views/Shared/Util/ucUnitTreeSelector.ascx"
                       , new UnitTreeSelectorModel
                       {
                           Units = Model.Units,
                           Visible = false,
                           Closeable = true,
                           Modal = true,
                           UnitIdInputId = "UnitId",
                           UnitNameInputId = "UnitName"
                       }); %>
                <input type="hidden" id="UnitId"/>
                <input type="hidden" id="UnitName" />
                <a id="btnChooseUnit" class="button" onclick="ShowUnitDialog()">Choose Unit</a>
                </td></tr></table>
                    </div>  
<input type="hidden" id="FieldId" />
 <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.customreportfieldselector.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/telerik.window.min.js"));%>

