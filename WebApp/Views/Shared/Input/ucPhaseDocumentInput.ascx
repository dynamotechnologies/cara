﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.DmdDocumentViewModel>" %>

<div class="fieldLabel"><%=Utilities.GetResourceString(Constants.PHASE) %> Documents</div>
<br />
<% Html.RenderPartial("~/Views/Shared/List/ucDmdDocumentList.ascx"); %>
<br />
<% if (Model.IsPalsProject)
   { %>
<div class="fieldLabel">Non-Associated Documents</div>
<br />
<% Html.RenderPartial("~/Views/Shared/List/ucDmdDocumentSelect.ascx"); %>
<% } 
   else
   { %>
<br />
<% Html.RenderPartial("~/Views/Shared/Input/ucFileUpload.ascx", new Dictionary<string, object> { { "ShowDocumentName", true }, { "MaxUploadSize", int.Parse(ConfigurationManager.AppSettings["maxFileUploadSize"]) } }); %>
<% } %>

<!-- Fields for model -->
<%: Html.HiddenFor(model => model.ShowButtons) %>