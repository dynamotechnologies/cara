﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ObjectionInfoInputViewModel>" %>

<div class="validation-summary-errors" id="objectionInfoErrors"></div>

<% Html.BeginForm("ObjectionInfo", "Letter", FormMethod.Post, new { id = "objectionInfoForm" }); %>
<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionInfo.ObjectionId)%>:</td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.ObjectionInfo.FiscalYear, new { style = "width:40px;", title = "Fiscal Year" })%>
            <%: Html.DisplayFor(model => model.ObjectionInfo.Region) %>
            <%: Html.TextBoxFor(model => model.ObjectionInfo.Forest, new { style = "width:40px;", title = "Forest" })%>
            <%: Html.TextBoxFor(model => model.ObjectionInfo.ObjectionNumber, new { style = "width:60px;", title = "Objection Number" }) %>
            <%: Html.DisplayFor(model => model.ObjectionInfo.RegulationNumber) %>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>"
                alt="Tooltip for Objection ID"
                title="The first box represents the Fiscal Year. The second text box represents the Forest number. The third text box represents the Objection Number, which must range from 1 to 9999 and is unique per Forest and will be converted to a 4-digit string for display ('23' will become '0023')." />
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionInfo.ReviewStatusId)%>:</td>
        <td class="text">
            <%: Html.Telerik().DropDownListFor(model => model.ObjectionInfo.ReviewStatusId)
                .BindTo(Model.LookupReviewState)
                .HtmlAttributes(new { style = "width:200px;" })%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionInfo.OutcomeId)%>:</td>
        <td class="text">
            <%: Html.Telerik().DropDownListFor(model => model.ObjectionInfo.OutcomeId)
                .BindTo(Model.LookupOutcome)
                .HtmlAttributes(new { style = "width:250px;" })%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ObjectionInfo.MeetingInfo)%>:</td>
        <td class="text">
            <%: Html.TextAreaFor(model => model.ObjectionInfo.MeetingInfo, new { rows = 6, cols = 50 })%>
            <br />
            <span id="countdown" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <a class="button" id="objectionInfoSubmit">Save</a>
            <a class="button" onclick="javascript:closeWindow('WindowObjectionInfo');">Cancel</a>
        </td>
    </tr>
 </table>

<%: Html.HiddenFor(model => model.PhaseId)%>
<%: Html.HiddenFor(model => model.ProjectId)%>

<!-- Hidden fields for letter objection  -->
<%: Html.HiddenFor(model => model.ObjectionInfo.LetterObjectionId)%>
<%: Html.HiddenFor(model => model.ObjectionInfo.Region)%>
<%: Html.HiddenFor(model => model.ObjectionInfo.RegulationNumber)%>
<%: Html.HiddenFor(model => model.ObjectionInfo.SignerFirstName)%>
<%: Html.HiddenFor(model => model.ObjectionInfo.SignerLastName)%>
<%: Html.HiddenFor(model => model.ObjectionInfo.ObjectionDecisionMakerId) %>
<%: Html.HiddenFor(model => model.ObjectionInfo.ResponseDate)%>
<%: Html.HiddenFor(model => model.ObjectionInfo.DmdId)%>
<% Html.EndForm(); %>

<script type="text/javascript">
    var countdown = new Countdown("ObjectionInfo_MeetingInfo", "countdown", 4000).Init();
    var form = new ObjectionInfoForm($("#objectionInfoForm"), "WindowObjectionInfo", $("#objectionInfoSubmit"), $("#objectionInfoErrors"), "Details").Init();
</script>
