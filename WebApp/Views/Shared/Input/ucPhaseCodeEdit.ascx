﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.PhaseCodeInputViewModel>" %>

<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(x => x.PhaseCode.CodeCategoryId) %>:</td>
        <td class="text">
            <%: Html.Telerik().DropDownListFor(model => model.PhaseCode.CodeCategoryId)
                .BindTo(Model.LookupCodeCategory)
                .HtmlAttributes(new { style = "width:200px;" })%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(x => x.PhaseCode.CodeNumber) %>:</td>
        <td class="text"><%: Html.TextBoxFor(model => model.PhaseCode.CodeNumberDisplay, new { disabled = "disabled" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(x => x.PhaseCode.CodeName) %>:</td>
        <td class="text"><%: Html.TextBoxFor(model => model.PhaseCode.CodeName, new { style = "width: 300px;", maxlength = "200" })%></td>
    </tr>
</table>

<% if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
   { %>
    <p>
        <input type="button" id="btnUpdate" value="<%=Constants.SAVE %>" class="button" onclick="updateCode()" />
        <input type="button" id="btnCloseEditWindow" value="<%=Constants.CANCEL %>" class="button" onclick="closeEditWindow(false)" />
    </p>
<% } %>

<%: Html.HiddenFor(model => model.PhaseCode.PhaseCodeId)%>

<script type="text/javascript" src="<%= ResolveUrl("~/Scripts/cara.phasecodeedit.js")%>"></script>

<script type="text/javascript">
    setEditPageMode();
</script>