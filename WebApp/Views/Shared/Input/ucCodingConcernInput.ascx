﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<Aquilent.Cara.Domain.PhaseCode>>" %>

<table>
    <tr>
        <td colspan="2">
            <input type="button" id="ucbtnSelectCodeTop" class="button" value="Select Code" onclick="assignCode();" />
<%--            <input type="button" class="button"value="Cancel" onclick="$('#ModalWindow').data('tWindow').close();" />--%>
        </td>
    </tr>
    <tr>
        <td class="fieldLabel" valign="top">Select a <%:Utilities.GetResourceString(Constants.CODE) %>:</td>
        <td class="text">
            <% Html.Telerik().TreeView()
                .Name("TreeView")
                .ExpandAll(false)
                .ClientEvents(events => events.OnSelect("setSelectedNode"))
                .BindTo(Model, mappings =>
                {
                    mappings.For<Aquilent.Cara.Domain.PhaseCode>(
                        binding => binding.ItemDataBound((item, code) =>
                        {
                            // expand the type level nodes
                            //if (string.IsNullOrEmpty(code.CodeName) && code.PhaseCodeId == 0)
                            //{
                            //    item.Expanded = true;
                            //}
                        
                            item.Text = code.CodeNumberDisplay + " " + code.CodeName;
                            item.Value = code.PhaseCodeId.ToString();
                        })
                        .Children(code => code.ChildCodes));

                    mappings.For<Aquilent.Cara.Domain.PhaseCode>(binding => binding.ItemDataBound((item, code) => { item.Text = code.CodeName; }));
                })
                .Render(); %>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="button" id="ucbtnSelectCodeBottom" class="button" value="Select Code" onclick="assignCode();" />
<%--            <input type="button" class="button"value="Cancel" onclick="$('#ModalWindow').data('tWindow').close();" />--%>
        </td>
    </tr>
</table>

