﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.MakeDuplicateModel>" %>

<div class="validation-summary-errors" id="objectionInfoErrors"></div>

<% Html.BeginForm("MakeDuplicate", "Letter", FormMethod.Post, new { id = "objectionInfoForm" }); %>
<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.MasterDuplicateSequence)%>:</td>
        <td class="text">
            <%: Html.TextBoxFor(model => model.MasterDuplicateSequence, new { style = "width:100px;" })%>
            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>"
                alt="Tooltip for Master Duplicate Letter #"
                title="Enter the Letter # for the Master Duplicate of this letter." />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <input class="button" type="submit" value="Save" />
            <a class="button" onclick="javascript:closeWindow('MakeDuplicate');">Cancel</a>
        </td>
    </tr>
 </table>

<%: Html.HiddenFor(model => model.PhaseId)%>
<%: Html.HiddenFor(model => model.ProjectId)%>
<%: Html.HiddenFor(model => model.LetterId)%>
<%: Html.HiddenFor(model => model.NextLetterIds)%>
<%: Html.HiddenFor(model => model.LettersRemaining)%>

<!-- Hidden fields for letter objection  -->
<% Html.EndForm(); %>