﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ConcernResponseInputViewModel>" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>

<br />
<div class="fieldLabel"><%: Html.LabelFor(m => m.ConcernResponse.ConcernText) %>&nbsp;<span class="text"><%: Model.ConcernInputNote %></span></div>
<% 
    bool assignedToOther = Model.ConcernResponse.UserId.HasValue ?
        Model.ConcernResponse.UserId.Value != Utilities.GetUserSession().UserId
        : false; 
    if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && !assignedToOther)
   { %>
<%--<%: Html.EditorFor(model => model.ConcernText) %>--%>
<%: Html.TextAreaFor(model => model.ConcernText, new { style = "width:700px;height:600px;" })%>
<% } else { %>
    <div style="width: 700px;"><%=Model.ConcernResponse.ConcernText%></div>
<% } %>

<br /><br />
    <%--//note required js loaded by host aspx--%>
    <% if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && !assignedToOther)
       { %>
        <input type="button" class="button" value="<%=Constants.SAVE %>" onclick="saveConcern('<%=Constants.SAVE %>')" />&nbsp;
        <% if (Model.TrackBackPage != "")
           { %>
            <input type="button" class="button" value="<%=Constants.SAVE_AND_RETURN %>" onclick="saveConcern('<%=Constants.SAVE_AND_RETURN %>')" />&nbsp;
        <% } %>
        <% if (Model.ConcernResponse.ConcernResponseId > 0)
           { %>
            <input type="button" class="button" value="<%=Constants.DELETE %>" onclick="saveConcern('<%=Constants.DELETE %>')" />&nbsp;
        <% } %>
    <% } %>
<br /><br />
<div class="fieldLabel">Response</div>
<% if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && !assignedToOther)
   { %>
    <div style="width: 700px;"><%: Html.EditorFor(model => model.ResponseText) %></div>
 <% }
   else
   { %>
    <div style="width: 700px;"><%=Model.ConcernResponse.ResponseText%></div>
<% } %>
<br />
<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ConcernResponse.LastUpdated)%>:</td>
        <td class="text"><%= Model.ConcernResponse.LastUpdated.ConvertToUserTimeZone(Utilities.GetUserSession()).ToShortDateString()%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.ConcernResponse.LastUpdatedBy)%>:</td>
        <td class="text"><%: Html.DisplayFor(model => model.ConcernResponse.LastUpdatedBy)%></td>
    </tr>
</table>
<br />

<%

    // This scripts are necessary for the codes to be displayed in the modal window
    Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/telerik.window.dialog.js")
                .Add("~/Scripts/tinymce/tinymce.js")
                .Add("~/Scripts/util.tinymce.js")                
                );        

 %>

<!-- Hidden fields for concern response model -->
<%: Html.HiddenFor(model => model.ConcernResponse.Sequence)%>
<%: Html.HiddenFor(model => model.ConcernResponse.ConcernCreated)%>
<%: Html.HiddenFor(model => model.ConcernResponse.ConcernCreatedBy)%>
<%: Html.HiddenFor(model => model.ConcernResponse.ResponseCreated)%>
<%: Html.HiddenFor(model => model.ConcernResponse.ResponseCreatedBy)%>
<%: Html.HiddenFor(model => model.Locked)%>
<%: Html.HiddenFor(model => model.ConcernResponse.PhaseCodeId)%>
<%: Html.HiddenFor(model => model.CodeNumberAndDescription)%>




