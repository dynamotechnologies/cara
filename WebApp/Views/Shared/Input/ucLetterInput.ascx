﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterInputViewModel>" %>

<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.FirstName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.FirstName)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.LastName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.LastName)%></td>
    </tr>
    <% if (!Model.Public)
       { %>
    <tr>
        <td colspan="4" class="text"><%: Html.CheckBoxFor(model => model.Anonymous)%>&nbsp;<label for="Anonymous">Check if you wish to submit the <%:Utilities.GetResourceString(Constants.LETTER).ToLower()%> anonymously</label></td>
    </tr>
    <% }
       else
       { %>
    <tr>
        <td colspan="4" class="text">
          <%: Html.CheckBoxFor(model => model.OrganizationCheck) %>
          <span class="fieldLabel"><label for="OrganizationCheck">I officially represent an organization</label></span>
        </td>
    </tr>
    <% }
      
        bool showOrg = (Model.OrganizationCheck || !Model.Public);
    %>
    <tr id="organizationRow" <%= showOrg ? string.Empty : "class=\"invisible\"" %>>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.OrganizationName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.OrganizationName)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Title)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Title)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Address1)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Address1)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Address2)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Address2)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.City)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.City)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.StateId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.StateId)
                             .BindTo(Model.LookupState)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.ProvinceRegion)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.ProvinceRegion)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.ZipPostal)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.ZipPostal)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.CountryId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.CountryId)
                             .BindTo(Model.LookupCountry)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Email)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Email)%></td>
    </tr>
    <tr valign="top">
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Phone)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Phone)%></td>
        <td class="fieldLabel" width="150px"><%: Html.LabelFor(model => model.Author.ContactMethod)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.ContactMethod)
                             .BindTo(Model.LookupContactMethod)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>

    <tr>
        <td class="fieldLabel" valign="top"></td>
        <td class="text" colspan="3" style="color: red"><br /><br /><b>Do not enter any personally identifiable information (PII) such address or email in the text editor below.</b> Your name and all information entered into the text box below may be published on this website. <span style="color: Black">Enter contact information in the form fields above.<br /><br />Do not embed images in the text editor. Use <b>Attachments</b> to upload images.</span></td>
    </tr>

    <% if (Model.Public)
       { %>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Letter.Text)%></td>
        <td class="text" colspan="3"><%: Html.TextAreaFor(model => model.Letter.Text, 10, 80, null)%></td>
    </tr>
    <%}
       else
       { %>
    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Letter.HtmlText)%></td>
        <td class="text" colspan="3"><%: Html.TextAreaFor(model => model.Letter.HtmlText, 10, 80, null)%></td>
    </tr>
    <%} %>
    <tr>
        <td class="fieldLabel" valign="top">Attachments</td>
        <td class="text" colspan="3">
           <% Html.RenderPartial("~/Views/Shared/Input/ucFileUpload.ascx", new Dictionary<string, object> { { "ShowDocumentName", false }, { "MaxUploadSize", Model.MaxUploadSize } }); %>
        </td>
    </tr>
    <% if (!Model.Public)
       { %>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DateSubmitted)%></td>
        <td class="text"><%= Html.Telerik().DatePickerFor(model => model.Letter.DateSubmitted).ShowButton(true) %></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DeliveryTypeId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Letter.DeliveryTypeId)
                             .BindTo(Model.LookupDeliveryType)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="WithinCommentPeriod">Within <%:Utilities.GetResourceString(Constants.PHASE) %></label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.WithinCommentPeriod)
                             .BindTo(Model.LookupYesNo)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.CommonInterestClassId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Letter.CommonInterestClassId)
                             .BindTo(Model.LookupCommonInterestClass)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <% }
       else           
       { %>
       <%: Html.HiddenFor(model => model.Letter.DateSubmitted)%>
       <%: Html.HiddenFor(model => model.Letter.DeliveryTypeId)%>
       <%: Html.HiddenFor(model => model.WithinCommentPeriod)%>       
    <% } %>
</table>
<%: Html.HiddenFor(model => model.Public)%>
<%: Html.HiddenFor(model => model.Mode)%>
<%: Html.HiddenFor(model => model.Sender)%>

<!-- Hidden fields for letter  -->
<%: Html.HiddenFor(model => model.Letter.LetterId)%>
<%: Html.HiddenFor(model => model.Letter.CodedText)%>
<%: Html.HiddenFor(model => model.Letter.DateEntered)%>
<%: Html.HiddenFor(model => model.Letter.Deleted)%>
<%: Html.HiddenFor(model => model.Letter.EarlyActionStatusId)%>
<%: Html.HiddenFor(model => model.Letter.LetterSequence)%>
<%: Html.HiddenFor(model => model.Letter.LetterStatusId)%>
<%: Html.HiddenFor(model => model.Letter.LetterTypeId)%>
<%: Html.HiddenFor(model => model.Letter.PhaseId)%>
<%: Html.HiddenFor(model => model.Letter.Size)%>
<%: Html.HiddenFor(model => model.Letter.Protected)%>

<!-- Hidden fields for Author  -->
<%: Html.HiddenFor(model => model.Author.AuthorId)%>
<%: Html.HiddenFor(model => model.Author.Deleted)%>
<%: Html.HiddenFor(model => model.Author.LetterId)%>
<%: Html.HiddenFor(model => model.Author.MiddleName)%>
<%: Html.HiddenFor(model => model.Author.Prefix)%>
<%: Html.HiddenFor(model => model.Author.Sender)%>
<%: Html.HiddenFor(model => model.Author.Suffix)%>