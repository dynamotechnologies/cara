﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.FormSetInputViewModel>" %>

<script type="text/javascript">
<!--
    // close the window and refresh the form set list on parent
    function doSaveFormSet(mode) {
        var formSetId = $("#FormSet_FormSetId").val();
        var masterFormId = $("#FormSet_MasterFormId").val();
        var name = $("#FormSet_Name").val();
        var phaseId = $("#FormSet_PhaseId").val();

        if (name != null && name != "") {
            // save the form set
            if (saveFormSet(mode, phaseId, formSetId, masterFormId, name)) {
                closeWindow("Window" + mode + "FormSet");
            }
        }
        else {
            alertWindow("Form Set Name is a required field.");
        }

        return false;
    }
-->
</script>
<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.FormSet.Name) %></td>
        <td class="text"><%: Html.TextBoxFor(model => model.FormSet.Name, new { style = "width: 300px;", maxlength = "100" })%></td>
    </tr>
</table>

<% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
   { %>
    <p>
        <input type="button" value="<%: (Model.Mode == PageMode.Create ? Constants.CREATE : Constants.UPDATE) %>" name="save" class="button" onclick="doSaveFormSet('<%=Model.Mode.ToString() %>')" />
        <input type="button" value="<%:Constants.CANCEL %>" name="cancel" class="button" onclick="closeWindow('Window' + '<%=Model.Mode.ToString() %>' + 'FormSet')" />
    </p>
<% } %>

<%: Html.HiddenFor(model => model.FormSet.FormSetId) %>
<%: Html.HiddenFor(model => model.FormSet.MasterFormId) %>
<%: Html.HiddenFor(model => model.FormSet.PhaseId)%>