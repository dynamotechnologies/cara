﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>
<div align="left">
  Edit the code order to change the display order
</div>
<br />
<%  if (Model.Comment != null)
    {
        %>
        <script type="text/javascript">
            var lettercommentnumber = '<%=Model.Comment.LetterCommentNumber %>';
        </script>
        <%
        
    
    //Html.Telerik().Grid<Aquilent.Cara.Domain.CommentCode>(Model.Comment.CommentCodes)
        Html.Telerik().Grid<Aquilent.Cara.Domain.CommentCode>(Model.Comment.CommentCodes.OrderBy(x => x.Sequence))
        .Name(Constants.LIST)
        .Name("SortTable")        
        .Columns(columns =>
        {
         
          // columns.Bound(commentCode => commentCode.Sequence)
           // .Title("Sequence ");
            
            columns.Bound(commentId => commentId.CommentId)
            .Hidden();  
                        
            columns.Bound(phaseCodeId => phaseCodeId.PhaseCodeId)
            .Hidden();
               
             columns.Bound(commentCode => commentCode.Sequence)
            .Title("Code Order")
            .Width(50)
            .Template(commentCode => {%>
                                        <%=Html.TextBox("txtSequence1", commentCode.Sequence, new { style = "width:25px;", maxlength = 5 })%>&nbsp;&nbsp;                            
                                        <%}).ClientTemplate("<input type='text' name='txtSequence1' value='<#= Sequence1 #>' style='width:25px;' maxlength='5' />&nbsp;&nbsp;");
                           
            columns.Bound(x => x.PhaseCode.CodeNumberDisplay)
            .ReadOnly() 
            .Title("Code Number");
                        
            columns.Bound(x => x.PhaseCode.CodeName)
            .ReadOnly() 
            .Title("Code Name");                       
           
                                
        }).ClientEvents(events => events.OnDataBound("onDataBound"))
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)        
        .EnableCustomBinding(true)      
        .HtmlAttributes(new { style = "width: 400px;"})
        .HtmlAttributes(new { align = "center" })
        .Footer(false)
        .Render();    
        
        
   
         


    } %>

 <br /><br />
<div align="center">
&nbsp;&nbsp;
<a id="btnCommentCodeUpdate" href="#" class="button" onclick="javascript:updateSortedSequence();"><%=Constants.UPDATE %></a>
&nbsp;&nbsp;&nbsp;&nbsp;
<a id="btnclosepopup" href="#" class="button" onclick="javascript:closeThisWindow();"><%=Constants.CLOSE %></a>
<%--<input type="button" value="<%=Constants.CLOSE %>" name="closepopup" class="button" onclick="javascript:closeThisWindow();" />--%>
</div>

<script type='text/javascript'>

function updateSortedSequence() {
    var table = $("#SortTable tbody");
    var sortedseq = new Array();
    var sortedseqNew = new Array();
    var commentId = "";    
    var inputError = '';

    table.find('tr').each(function (i) {

        var $tds = $(this).find('td'),
        phaseCodeId = $tds.eq(1).text();
        commentId = $tds.eq(0).text();
        sequence = $tds.eq(2).find("input").val();
        if (sequence === null || sequence === '' || isNaN(sequence)) {
            inputError = "Incorrect Sequence Value";
        }

        if (sequence < 0) {
            sequence = 0;
        }
        if (sortedseqNew[sequence] === null || sortedseqNew[sequence] === '' || sortedseqNew[sequence] === undefined) {
            sortedseqNew[sequence] = phaseCodeId;
        } else {
            shiftElementInArray(sortedseqNew, sequence, phaseCodeId);
        }
        //alert(sortedseqNew.toString());
    });
    if (inputError != '') {
        alert("Incorrect Sequence Value");
        return;
    }

    //alert(sortedseqNew.toString());

    for (i = 0; i < sortedseqNew.length; i++ ) {
        if (sortedseqNew[i] != null && sortedseqNew[i] != '' && sortedseqNew[i] != 'undefined') {
            //alert(sortedseqNew[i]);
            sortedseq.push(sortedseqNew[i]);
        }
    }
    postSortedSequence(commentId, sortedseq.toString())
    //alert(sortedseq.toString());
    setCommentInputArea(lettercommentnumber);
    closeWindow("WindowCommentCode");
}

function shiftElementInArray(sortedseqNew, sequence, phaseCodeId) {
    if (sortedseqNew[sequence] === null || sortedseqNew[sequence] === '' || sortedseqNew[sequence] === undefined) {
        sortedseqNew[sequence] = phaseCodeId;
    } else {
        storedElement = sortedseqNew[sequence];
        sortedseqNew[sequence] = phaseCodeId;
        shiftElementInArray(sortedseqNew, ++sequence, storedElement);
    }
}

function postSortedSequence(commentId, sortedseq) {
    if (commentId != null && sortedseq != null) {

            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/Letter/UpdateCommentCodesSequence?commentId=" + commentId + "&sortedCodeList=" + sortedseq,
                success: function (data) {
                    //self.close();
                    setCommentInputArea(lettercommentnumber);
                    closeWindow("WindowCommentCode");
                }
            });
    }
}
function closeThisWindow() {
    closeWindow("WindowCommentCode");
    setCommentInputArea(lettercommentnumber);
}

</script>




 
   