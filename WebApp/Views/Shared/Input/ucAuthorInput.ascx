﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.AuthorInputViewModel>" %>

<table>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.FirstName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.FirstName)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.LastName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.LastName)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.OrganizationName)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.OrganizationName)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Title)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Title)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Address1)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Address1)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Address2)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Address2)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.City)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.City)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.StateId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.StateId)
                             .BindTo(Model.LookupState)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.ProvinceRegion)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.ProvinceRegion)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.ZipPostal)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.ZipPostal)%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.CountryId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Author.CountryId)
                             .BindTo(Model.LookupCountry)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Email)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Email)%></td>
    </tr>
    <tr valign="top">
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Author.Phone)%></td>
        <td class="text"><%: Html.TextBoxFor(model => model.Author.Phone)%></td>
        <td class="fieldLabel" width="150px"><span class="required">*</span><%: Html.LabelFor(model => model.Author.ContactMethod)%></td>
        <td class="text"><%:Html.Telerik().DropDownListFor(model => model.Author.ContactMethod)
                             .BindTo(Model.LookupContactMethod)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
</table>

<%: Html.HiddenFor(model => model.Mode)%>
<%: Html.HiddenFor(model => model.Sender)%>
<%: Html.HiddenFor(model => model.ProjectId)%>

<!-- Hidden fields for author  -->
<%: Html.HiddenFor(model => model.Author.AuthorId)%>
<%: Html.HiddenFor(model => model.Author.OrganizationId)%>
<%: Html.HiddenFor(model => model.Author.Deleted)%>
<%: Html.HiddenFor(model => model.Author.LetterId)%>
<%: Html.HiddenFor(model => model.Author.MiddleName)%>
<%: Html.HiddenFor(model => model.Author.Prefix)%>
<%: Html.HiddenFor(model => model.Author.Sender)%>
<%: Html.HiddenFor(model => model.Author.Suffix)%>