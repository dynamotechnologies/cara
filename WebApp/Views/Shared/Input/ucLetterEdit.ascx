﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.LetterInputViewModel>" %>

<table>

    <tr>
        <td class="fieldLabel" valign="top"></td>
        <td class="text" colspan="3" style="color: red"><br /><br /><b>Do not enter any personally identifiable information (PII) such address or email in the text editor below.</b> Your name and all information entered into the text box below may be published on this website. <span style="color: Black">Enter contact information in the form fields above.<br /><br />Do not embed images in the text editor. Use <b>Attachments</b> to upload images.</span></td>
    </tr>

    <tr>
        <td class="fieldLabel" valign="top"><%: Html.LabelFor(model => model.Letter.HtmlText)%></td>
        <td class="text" colspan="3">
          <%: Html.TextAreaFor(model => model.Letter.HtmlText, 10, 80, null)%>
        </td>
    </tr>
    <%if (!Model.OriginalTextEditable)
    { %>
    <tr>
        <td class="fieldLabel" valign="top"><label for="NewText"><%=Constants.APPEND %>&nbsp;Text</label>&nbsp;<img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Append Text" title="Non-text objects (e.g., images) copied from letter attachments cannot be pasted into the Append Text field." /></td>
        <td class="text" colspan="3"><%: Html.TextAreaFor(model => model.NewText, 10, 80, null)%></td>
    </tr>
     <% } %>
    <%if (Model.CurrentTextNotOriginal)
    { %>
        <td>&nbsp;</td>
        <td>
          <a class="button" id="btnRevert" onclick="revertLetterWarning(); return false;" href="<%= Url.Action(Constants.REVERT, Constants.LETTER, new { id = Model.Letter.LetterId }) %>"><%=Constants.REVERT %></a >
        </td>
 <% } %>
      
    <tr>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DateSubmitted)%></td>
        <td class="text"><%= Html.Telerik().DatePickerFor(model => model.Letter.DateSubmitted).ShowButton(true)
                         .ClientEvents(events => events.OnChange("DateChange"))%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.DeliveryTypeId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Letter.DeliveryTypeId)
                             .BindTo(Model.LookupDeliveryType)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
    <tr>
        <td class="fieldLabel"><label for="WithinCommentPeriod">Within <%: Utilities.GetResourceString(Constants.PHASE) %></label></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.WithinCommentPeriod)
                             .BindTo(Model.LookupYesNo)
                             .HtmlAttributes(new { style = "width:200px;" })
                             .Enable(Model.WithinCommentPeriodEditable)%></td>
        <td class="fieldLabel"><%: Html.LabelFor(model => model.Letter.CommonInterestClassId)%></td>
        <td class="text"><%: Html.Telerik().DropDownListFor(model => model.Letter.CommonInterestClassId)
                             .BindTo(Model.LookupCommonInterestClass)
                             .HtmlAttributes(new { style = "width:200px;" })%></td>
    </tr>
</table>
<%: Html.HiddenFor(model => model.Mode)%>
<%: Html.HiddenFor(model => model.Changed)%>
<%: Html.HiddenFor(model => model.Phase.CommentStart) %>
<%: Html.HiddenFor(model => model.Phase.CommentEnd) %>
<%: Html.HiddenFor(model => model.OriginalTextEditable) %>

<!-- Hidden fields for letter  -->
<%: Html.HiddenFor(model => model.Letter.LetterId)%>
<%: Html.HiddenFor(model => model.Letter.CodedText)%>
<%: Html.HiddenFor(model => model.Letter.HtmlCodedText) %>
<%: Html.HiddenFor(model => model.Letter.DateEntered)%>
<%: Html.HiddenFor(model => model.Letter.Deleted)%>
<%: Html.HiddenFor(model => model.Letter.LetterSequence)%>
<%: Html.HiddenFor(model => model.Letter.LetterStatusId)%>
<%: Html.HiddenFor(model => model.Letter.EarlyActionStatusId)%>
<%: Html.HiddenFor(model => model.Letter.LetterTypeId)%>
<%: Html.HiddenFor(model => model.Letter.PhaseId)%>
<%: Html.HiddenFor(model => model.Letter.CommentSequence)%>
<%: Html.HiddenFor(model => model.Letter.FormSetId)%>
<%: Html.HiddenFor(model => model.Letter.PublishToReadingRoom)%>
<%: Html.HiddenFor(model => model.Letter.DmdId)%>
<%: Html.HiddenFor(model => model.Letter.UnpublishedReasonId)%>
<%: Html.HiddenFor(model => model.Letter.UnpublishedReasonOther)%>
<%: Html.HiddenFor(model => model.Letter.Protected)%>
<%: Html.HiddenFor(model => model.Letter.Size)%>
<%: Html.HiddenFor(model => model.Letter.OriginalText)%>
<%: Html.HiddenFor(model => model.Letter.HtmlOriginalText) %>
<%: Html.HiddenFor(model => model.Letter.LetterObjectionId)%>
<%: Html.HiddenFor(model => model.Letter.IsLetterHtml) %>
