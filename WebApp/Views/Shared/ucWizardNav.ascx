﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.ProjectWizard>" %>

<% Html.Telerik().PanelBar()
        .Name("WizardNav")
        .HtmlAttributes(new { style = "width: 250px; float: left; margin-bottom: 30px;" })
        .ExpandMode(PanelBarExpandMode.Single)
        .HighlightPath(true)
        .Items(item =>
        {
            if (Model != null && Model.WizardSteps.Count > 0)
            {
                item.Add().Expanded(true).Items(
                    items =>
                    {
                        foreach (Aquilent.Cara.WebApp.Models.Configuration.WizardStepElement step in Model.WizardSteps)
                        {
                            items.Add().Text(Utilities.GetWizardPageTitle(Model.Mode, step.Controller + step.Action, step.Name))
                                .ImageUrl("~/Content/Images/" + (step.Name.Equals(Model.CurrStep) ? "arrow_right.gif" : "spacer.jpg"))
                                .ImageHtmlAttributes(new { alt = (step.Name.Equals(Model.CurrStep) ? "Current Step" : string.Empty) })
                                .Selected(step.Name.Equals(Model.CurrStep))
                                .Enabled(step.Name.Equals(Model.CurrStep));
                        }
                    }
                );
            }
        })
        .ExpandAll(true)
        .Render();
%>
