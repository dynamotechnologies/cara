﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.Breadcrumb>" %>

<div>
<%  if (Model != null && Model.ActionLinks != null)
{
    foreach (Aquilent.Cara.WebApp.Models.ActionLink link in Model.ActionLinks) 
    { %>
        <%: Html.ActionLink(link.name, link.action, link.controller, link.routeValues, null) %>&nbsp;>&nbsp;
    <% }
} %><%=(Model == null ? string.Empty : Model.Text)%>
</div>