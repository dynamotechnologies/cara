﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>

<% Html.Telerik().PanelBar()
        .Name("WizardNav")
        .HtmlAttributes(new { style = "width: 325px; float: left; margin-bottom: 30px;" })
        .ExpandMode(PanelBarExpandMode.Single)
        .HighlightPath(true)
        .Items(item =>
        {
            if (Model != null && Model.WizardStepList().Count() > 0)
            {
                item.Add().Expanded(true).Items(
                    items =>
                    {
                        foreach (KeyValuePair<int,string> step in Model.WizardStepList())
                        {
                            items.Add().Text(step.Value)
                                .ImageUrl("~/Content/Images/" + (step.Key.Equals(Model.WizardStep) ? "arrow_right.gif" : "spacer.jpg"))
                                .ImageHtmlAttributes(new { alt = (step.Key.Equals(Model.WizardStep) ? "Current Step" : string.Empty) })
                                .Selected(step.Key.Equals(Model.WizardStep))
                                .Enabled(step.Key.Equals(Model.WizardStep));
                        }
                    }
                );
            }
        })
        .ExpandAll(true)
        .Render();
%>