﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Blank.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Logout
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Logout</h3>
	<p>Explain to the user what has happened and provide with the PALS link.</p>
	<p>
        <a href="#">Link to PALS Site</a>
    </p>
</asp:Content>
