﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HomeViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="NotificationContent" runat="server">
    <div id="welcome">
        <table>
            <tr>
                <td valign="top" style="border-right: solid 1px black;">
                    <% if (Utilities.GetUserSession().HasPrivilege("MPRJ"))
                        { %>                        
                        <%: Html.ActionLink(string.Format("{0} {1}", Constants.NEW, Constants.PROJECT),
                            Constants.INPUT, Constants.PROJECT, new { mode = PageMode.ProjectCreateWizard }, new { @class = "button" })%>
                        <%: Html.ActionLink("Objection",
                            "CreateObjection", Constants.PROJECT, new { mode = PageMode.CreateObjection }, new { @class = "button" })%>
                            <br /><br />
                        <div style="border-bottom: 1px solid black;"></div>
                        <br />
                    <% } %>
                    <label for="txtProjectNameOrId"><%:Constants.FIND %> <%:Utilities.GetResourceString("AppNameAbbv")%> <%:Utilities.GetResourceString(Constants.PROJECT).Pluralize()%> by Name or ID</label>
                    <% using (Html.BeginForm())                  
                       { %>
                        <%: Html.TextBox("txtProjectNameOrId") %>&nbsp;<br />
                        <input type="submit" class="button" name="action" value="<%=Constants.SEARCH%>" />&nbsp;
                        <%: Html.ActionLink("Advanced " + Constants.SEARCH, Constants.LIST, Constants.PROJECT)%>
                    <% } %>
                </td>
                <td valign="top">
                    <div class="sectionTitle">Notifications</div>
                    <% Html.RenderPartial("~/Views/Shared/Details/ucMessageSettingDetails.ascx", Model.WelcomeMessage); %>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="sectionTitle"></div>
    
  <% Html.Telerik().TabStrip()
        .Name("HomeTabs")
        .Items(tabstrip =>
        {
            tabstrip.Add()
                .Text("My Tasks")
                .Content(() =>
                {%>
                    <div class="tabArea">

                      <div class="sectionTitle">Comments &amp; Concerns</div>
                      <% Html.RenderPartial("~/Views/Shared/List/ucTaskList.ascx", Model.TaskListViewModel); %>
                      <br />
                      <div class="sectionTitle">Letters to Review</div>
                      <% Html.RenderPartial("~/Views/Shared/List/ucTaskList.ascx", Model.ReviewableLettersViewModel); %>
                    </div>
                <%})                
                .Selected(true);
                  
            tabstrip.Add()
                .Text(Utilities.GetResourceString("MyProjects"))
                .Content(() =>
                {%>
                    <div class="tabArea">
                    <% Html.RenderPartial("~/Views/Shared/List/ucMyProjectList.ascx", Model.ProjectListViewModel); %>
                    </div>
                <%});
        })
        .ClientEvents(events =>
        {
            events.OnLoad("AdjustMyProjectHeaders");
        })
        .Render(); %>
    <br />
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.projectlist.js")); %>
</asp:Content>