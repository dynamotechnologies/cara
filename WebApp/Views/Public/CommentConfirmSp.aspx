﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/NepaSp.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PublicCommentConfirmModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="server">
	US Forest Service NEPA Projects Home
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Model.IsPalsProject) { %><p><a href="<%=Model.ProjectUrl %>"><strong>Volver a la página principal del proyecto</strong></a></p><p></p><% } %>
	<h1><%=ViewData["model.ProjectNameNumber"]%> </h1>
    <h2>Gracias por sua comentario.</h2>
     <p></p><p></p>
    Su comentario ha sido recibido en nuestro sistema de <%=DateTime.Now.ToShortDateString() %><br />
     <% if (!Model.IsObjection)
        {
            %>Su carta de identificación es <strong><%: Html.DisplayFor(model => model.LetterReferenceNumber)%></strong>. Por favor, guardar o imprimir esta página para sus registros.<br /> <% 
        }
        else
        { %>
            Si necesita hacer un seguimiento de este asunto, por favor utilice la siguiente información de contacto:<br /><br />
                <p><strong><%=Model.ContactName%></strong><br />
                <%=Model.UnitName%><br />
                <% if (!string.IsNullOrWhiteSpace(Model.ContactAddress))
                   { %>
                        <%=Model.ContactAddress%><br />
                <% } %>
                <%=Model.ContactEmail%></p><%
        }%>
 
 <p></p><p></p>
   Saludos, <br />
   El equipo del  <%=ViewData["model.ProjectNameNumber"]%> 
 
</asp:Content>