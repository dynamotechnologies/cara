﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Nepa.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterReadingRoomListViewModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RobotsContent" runat="server">
  <meta name="ROBOTS" content="NONE" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">US Forest Service NEPA Project <%: Utilities.GetResourceString(Constants.PUBLIC_READING_ROOM) %></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>

    <% if (Model.IsPalsProject) { %><a href="<%=Model.PalsProjectUrl %>"><strong>Go back to main project page</strong></a><% } %>

        <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle)%></div>
        <div>
            <table>
				<tr>
					<td><h1><%: Html.DisplayFor(m => m.Project.Name) %> #<%: Html.DisplayFor(m => m.ProjectNumber) %></h1></td>
				</tr>
                <tr>
                    <td class="text markdown"><%: Html.DisplayFor(m => m.Project.Description) %></td>
                </tr>
                <% if (!string.IsNullOrEmpty(Model.Project.ReadingRoomText))
                   { %>
                <tr>
                    <td class="text"><%= Model.Project.ReadingRoomText %></td>
                </tr>
                  <% }
                %>
                <% if (Model.Project.ProjectNumber == "10001")
                   { %>
                <tr>
                    <td class="text">
                        <p>Thank you for joining the 4FRI reading room. Only comments that were received on the DEIS remain posted. No objections may be submitted via this reading room.</p>
                        <p>Written objections, including attachments, must be filed with the Regional Forester, Southwestern Region, 333 Broadway SE, Albuquerque, NM 87102, via FAX: (505) 842-3110, or via email: objections-southwestern-regional-office@fs.fed.us. The office business hours for those submitting hand-delivered objections are: 8:00 am to 4:30 pm Monday through Friday, excluding holidays. Electronic objections must be submitted in a format such as an email message in word (.doc), portable document format (.pdf), rich text format (.rtf), text (.txt), or hypertext markup language (.html). Please state “Four-Forest Restoration Initiative” in the subject line when providing electronic objections, or on the envelope when replying by mail.</p>
                        <p>If you have any questions or for document requests, please contact 4FRI Team Leader Annette Fredette at (928) 226-4684 or via email at <a href="4fri_comments@fs.fed.us">4fri_comments@fs.fed.us</a>. If using the email address, please note there is an underscore between the words "4fri" and "comments".</p>
                    </td>
                </tr>
                  <% }
                %>
            </table>
        </div>
        <br />
    <% if (Model.ReadingRoomAvailable)
       { %>
        <% using (Html.BeginForm())
           { %>
                <% Html.RenderPartial("~/Views/Shared/Filter/ucLetterReadingRoomFilter.ascx", Model); %>
                <br />
                <% Html.RenderPartial("~/Views/Shared/List/ucPublicReadingRoomSearchList.ascx", Model); %>
                <br />
                <a href="#" class="button" onclick="return downloadLetters();"><%: Constants.DOWNLOAD %></a>
                <br /><br />
                <div class="fieldLabel">
                    <i>Disclaimer: Letters that may contain proprietary or sensitive resource information, or that may be otherwise sensitive, 
are automatically withheld from displaying in the reading room pending human review. Letters received, 
but not accessible here, will still be considered and included as part of the record for this project.  
Most flagged letters are cleared for posting within a few days of being received. Posting may also be delayed for
 comments not submitted via the web form (e.g., email, mail or fax).</i>
                </div>                
            <!-- hidden fields for page variables -->
            <%: Html.HiddenFor(m => m.PhaseId)%>
            <%: Html.HiddenFor(m => m.BreadCrumbTitle)%>
            <%: Html.HiddenFor(m => m.PageTitle)%>
            <%: Html.HiddenFor(m => m.Mode)%>
            <!-- variable to keep track of the model values -->
            <%: Html.HiddenFor(m => m.ProjectNumber)%>
            <%: Html.HiddenFor(m => m.PublishToReadingRoom)%>
            <%: Html.HiddenFor(m => m.SearchResultsPerPage)%>
            <%: Html.HiddenFor(m => m.DmdIdList)%>
            <%: Html.Hidden("DownloadLettersUrl") %>
        <% } %>
    <% }
       else
       {
           %><b><%: string.Format("Sorry, the {0} {1} is not available for {2} #{3} or {2} #{3} doesn't exist.", Constants.PUBLIC, Utilities.GetResourceString(Constants.READING_ROOM),
                Utilities.GetResourceString(Constants.PROJECT), Model.ProjectNumber) %></b><%
       } %>

<% Html.Telerik().Window()
    .Name("WindowRecaptcha")
    .Title(string.Format("{0} {1}", Constants.DOWNLOAD, Utilities.GetResourceString(Constants.LETTER).Pluralize()))
    //.ClientEvents(events => events.OnOpen("showRecaptcha('recaptcha')"))
    .Draggable(true)
    .Buttons(b => b.Maximize())
    .Modal(true)
    .Width(400)
    .Height(220)
    .Visible(false)
    .Render(); %>

<% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/telerik.window.min.js")
                .Add("~/Scripts/telerik.window.dialog.js")
                .Add("~/Scripts/cara.readingroomlist.js")
                .Add("~/Scripts/Markdown.Converter.js")
                .Add("~/Scripts/cara.markdown.js"));
    %>
</asp:Content>