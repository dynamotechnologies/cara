﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TimeOut
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding: 0px 0px 0px 15px;">
        <div class="pageTitle">Your Session Has Expired</div>
        <div class="text" style="padding: 0px 0px 0px 15px;">
	        <p>Your session has been closed and you have been logged off due to inactivity.</p>
            <% if (ViewData["userName"] != "")
               { %>
                <%--<a id="btnBackToCara" class="button" href=<%: ConfigurationManager.AppSettings["caraLogBackInUrl"] %><%: ViewData["userName"] %>>Back to CARA</a>--%>
                <%--<input type="button" id="btnBackToCara" class="button" value="Back to CARA" onclick="location.href='<%: Url.Action("LogBackIn", "Authentication", new { userName = ViewData["userName"], tokenUrl = "https://cara.ecosystem-management.org/Authentication/Token?servicename=palsauth&servicepassword=}7GXbSy[xEEM&username=" }) %>'" />--%>
                <input type="button" id="btnBackToCara" class="button" value="Back to CARA" onclick="location.href='<%: Url.Action("LogBackIn", "Authentication", new { serviceName = "palsauth", servicePassword = ConfigurationManager.AppSettings["servicePassword"], userName = ViewData["userName"] }) %>    '" />
            <% } %>
            <a id="btnBackToPals" class="button" href=<%: ConfigurationManager.AppSettings["palsLoginUrl"]%>>Back to PALS</a>
        </div>
    </div>
</asp:Content>