﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Nepa.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PublicLetterViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">US Forest Service NEPA Project <%: Model.PageTitle %></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RobotsContent" runat="server"><meta name="ROBOTS" content="NONE" /></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <strong><%: Html.ActionLink(string.Format("{0} to {1}", Constants.BACK, Utilities.GetResourceString(Constants.PUBLIC_READING_ROOM)), Constants.READING_ROOM, new { project = Model.ProjectNumber })%></strong>
	<p>
        <% Html.RenderPartial("~/Views/Shared/Details/ucLetterDetailsPublic.ascx", Model); %>
        <br />
        <% Html.RenderPartial("~/Views/Shared/List/ucDmdFileMetaDataList.ascx", Model); %>
    </p>
</asp:Content>