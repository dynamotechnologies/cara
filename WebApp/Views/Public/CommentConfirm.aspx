<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Nepa.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PublicCommentConfirmModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="server">
	US Forest Service NEPA Projects Home
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Model.IsPalsProject) { %><p><a href="<%=Model.ProjectUrl %>"><strong>Go back to main project page</strong></a></p><p></p><% } %>
	<h1><%: Html.DisplayFor(model => model.ProjectName)%> #<%: Html.DisplayFor(model => model.ProjectNumber)%></h1>
    <h2>Thank you for Your Comment.</h2>
     <p></p><p></p>
     Your comment has been received by our system on <%=DateTime.Now.ToShortDateString() %><br />
     <% if (!Model.IsObjection)
        {
            %>Your letter ID is <strong><%: Html.DisplayFor(model => model.LetterReferenceNumber)%></strong>. Please save or print this page for your records.<br /> <% 
        }
        else
        { %>
            If you need to follow up on this matter, please use the following contact information:<br /><br />
                <p><strong><%=Model.ContactName%></strong><br />
                <%=Model.UnitName%><br />
                <% if (!string.IsNullOrWhiteSpace(Model.ContactAddress))
                   { %>
                        <%=Model.ContactAddress%><br />
                <% } %>
                <%=Model.ContactEmail%></p><%
        }%>
 
 <p></p><p></p>
   Regards,  <br />
   The <%: Html.DisplayFor(model => model.ProjectName)%> Team
 
</asp:Content>