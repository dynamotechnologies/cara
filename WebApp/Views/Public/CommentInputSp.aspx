﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/NepaSp.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PublicCommentInputViewModel>" %>
<%@ Import Namespace="Aquilent.Cara.WebApp.Models" %>


<asp:Content ID="Content3" ContentPlaceHolderID="RobotsContent" runat="server">
 <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?" async defer ></script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	US Forest Service NEPA Projects Home
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Model.IsPalsProject)
       { %>
            <p><a href="<%=Model.PalsProjectUrl %>"><strong>Volver a la página principal del proyecto</strong></a></p><p></p>
    <% } %>
    <% if (!string.IsNullOrEmpty(Model.PalsProjectId)) 
        {%>
    <div align="right"><a class="t-link" href="/Public/CommentInput?project=<%= Model.PalsProjectId %>">English</a></div>
   <%} %>
    <h1><%= ViewData["model.ProjectNameNumber"]%></h1>
    
    <h2>Al comentar sobre este proyecto</h2>

    <% if (!Model.IsPalsProject)
    { %>
        <p class="markdown"><%=ViewData["model.Project.Description"]%></p>
    <%} %>
   
<%----BEGIN CUSTOMIZED TEXT DISPLAY SECTION----%>
    <div id="customPublicText" style="width: 700px;"><p><%---=Model.PublicFormText---%><%=ViewData["model.PublicFormText"]%></p></div>
<%----END CUSTOMIZED TEXT DISPLAY SECTION----%>

<%----BEGIN NEWSPAPER, COMMENT PERIOD AND DOCUMENT SECTION----%>
    <!-- Display only if the project is in an informal comment period known by CARA -->
    <% if (Model.ExistInCara && Model.WithinCurrentPhase && Model.Phase.PublicCommentActive)
        {
        if (Model.IsPalsProject)
        {
            if (!string.IsNullOrEmpty(Model.NewspaperOfRecordUrl))
               { %>
                    Periódico de referencia: &nbsp;<a target="_blank" href="<%=Model.NewspaperOfRecordUrl%>"><%=Model.NewspaperOfRecord%></a>
                    <br /><br />
            <% }          
        }
           
        if (!Model.Phase.AlwaysOpen)
            { %>
                Sus comentarios pueden presentarse hasta el <%= ViewData["model.EndDate"]%>.<br />
         <% }

        if (Model.DmdDocumentViewModel.Documents != null && Model.DmdDocumentViewModel.Documents.Count > 0)
            { %>
                <h3>Documentos</h3>
                <% Html.RenderPartial("~/Views/Shared/List/ucDmdDocumentListSp.ascx", Model.DmdDocumentViewModel); %>
                <br />
        <%  }

        if (ViewData[Constants.ERROR] != null)
            { %>
                <div class="error"><%=ViewData[Constants.ERROR]%></div>
        <%  }
        } %>
<%----END NEWSPAPER, COMMENT PERIOD AND DOCUMENT SECTION----%>

<%----BEGIN WEB FORM SECTION----%>
        <!-- THE FOLLOWING TEXT IS OPTIONAL - display only if the project is in a comment period known by CARA -->
    <% if (Model.ExistInCara && Model.WithinCurrentPhase && Model.CurrentPhase.PublicCommentActive)
        { %>   
            <br />
            <%= Model.ElectronicFormDisclaimerSp %>

            <% using (Html.BeginForm(Constants.COMMENT + Constants.INPUT + "Sp", Constants.PUBLIC, FormMethod.Post, new { enctype = "multipart/form-data", id="commentInput", onsubmit="return validateCommentFormsp();",  onKeyPress = "return document.activeElement.className.toUpperCase() == \"BUTTON\" || document.activeElement.id.toUpperCase().search(\"FILEUPLOAD\") != -1 || document.activeElement.tagName.toUpperCase() == \"TEXTAREA\" || ((window.event)?event.keyCode : e.which)!=13;" }))
                { %>
                    <%: Html.ValidationSummary()%>
                    <div class="validation-summary-errors-client"></div>
                    <% Html.RenderPartial("~/Views/Shared/Input/ucLetterInputSp.ascx", Model); %>
                    <br />
                    <!--replace function below is a hot fix 
                        a more permenant solution will be added to the generaterecaptcha method-->
                    <%-- Html.GenerateRecaptcha().Replace("http:", "https:") --%>
                    <div class="g-recaptcha"  data-sitekey="<%= ConfigurationManager.AppSettings["reCaptchaPublicKey"] %>"></div>
                    <br />
                    <input type="Submit" class="button" name="action" value="Enviar"/>
                    <%: Html.HiddenFor(model => Model.PhaseId)%>
                    <%: Html.HiddenFor(model => model.Sender)%>
                    <%: Html.HiddenFor(model => model.Mode)%>
                    <%: Html.HiddenFor(model => model.PalsProjectId)%>
                    <%: Html.HiddenFor(model => model.ProjectName)%>
                    <%: Html.HiddenFor(model => model.ProjectId)%>
            <% } %>
    <% }
        else if (!string.IsNullOrWhiteSpace(Model.ContactName) || !string.IsNullOrWhiteSpace(Model.ContactEmail))
        { %>
            <!-- THE FOLLOWING TEXT IS OPTIONAL - display only if the project is NOT in a comment period known by CARA -->
	        <h3>Envío de comentarios</h3>
		    Si desea enviar un comentario, envíelo a:<br /><br />
            <p><strong><%=Model.ContactName%></strong><br />
            <%=Model.UnitName%><br />
            <% if (!string.IsNullOrWhiteSpace(Model.ContactAddress))
                { %>
                    <%=Model.ContactAddress%><br />
            <% } %>
            <%=Model.ContactEmail%></p>
    <% } %>
    

<%----END WEB FORM SECTION----%>

        <% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/Markdown.Converter.js")
                .Add("~/Scripts/cara.public.commentinput.js")
                .Add("~/Scripts/cara.markdown.js")
                .Add("~/Scripts/cara.commentformvalidation.js")); %>
    
</asp:Content>

