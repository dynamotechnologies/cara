﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>
<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Application&nbsp;<%=Constants.ERROR%>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="pageTitle">Application&nbsp;<%=Constants.ERROR%></div>
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="text" style="padding-left: 15px;">
        <span class="error">
            An error occurred while processing your request
            <% if (Model != null && Model.Exception != null)
               { %>
                - <%=Model.Exception.Message %>
            <% } %>
        </span><br /><br />
        For more information about <%=Utilities.GetResourceString("AppNameAbbv") %>, please visit the <%: Html.ActionLink("support page", Constants.INDEX, Constants.SUPPORT)%>.<br /><br />
        You may also report this error to the <%=Utilities.GetResourceString("AppNameAbbv") %> Admin at the following page:<br />
        <a href='https://ems-team.usda.gov/sites/fs-emnepa-sd/Lists/eMNEPASupport/Item/newifs.aspx'>eMNEPA Support</a>        
        <br /><br />
        Please include a brief description of repeatable actions which have generated this error.<br /><br />
        Thank you,<br />
        <%=Utilities.GetResourceString("AppNameAbbv") %> Support team<br /><br />
        <a id="btnBack" class="button" href="#">Go Back To Previous Page</a>

    <% if (Model != null && ConfigurationManager.AppSettings["displayErrorStackTrace"] == "True")
       { %>
       <br /><br /><hr />
       <p>
        <b>Inner Exception:</b>&nbsp;<%=Model.Exception.InnerException%><br />
        <b>Controller:</b>&nbsp;<%=Model.ControllerName %><br />
        <b>Action:</b>&nbsp;<%=Model.ActionName %><br />
       </p>
       <p>
        <b>Stack Trace:</b>&nbsp;<%= Model.Exception.StackTrace%>
       </p>
    <% } %>
    </div>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.error.js")); %>

</asp:Content>
