﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%=Constants.ADMIN%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Constants.ADMIN, Request.Url)); %></div>
    <div class="pageTitle"><%=Constants.ADMIN%></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Utilities.UserHasPrivilege("UWM", Constants.SYSTEM))
       { %>
        <span class="fieldLabel"><%=Utilities.GetResourceString("WelcomeMessage")%>:</span>&nbsp;
        <%: Html.ActionLink(Constants.EDIT, "CaraSettingInput", Constants.ADMIN)%>
        <br /><br /><br />
    <% } %>
    <% if (Utilities.UserHasPrivilege("MSU", Constants.SYSTEM) || Utilities.UserHasPrivilege("MUC", Constants.SYSTEM))
       { %>
        <span class="fieldLabel"><%=Utilities.GetResourceString("UserManagement")%>:</span>&nbsp;
        <%: Html.ActionLink(string.Format("{0} in {1}", Constants.SEARCH, Utilities.GetResourceString("AppNameAbbv")), Constants.LIST, Constants.USER)%>&nbsp;|&nbsp;
        <%: Html.ActionLink(string.Format("{0} from {1}", Constants.ADD, Utilities.GetResourceString("PALS")), Constants.LIST_DATAMART, Constants.USER)%>
        <br /><br /><br />
    <% } %>
    <% if (Utilities.UserHasPrivilege("MCDS", Constants.SYSTEM))
       { %>
        <span class="fieldLabel"><%=Utilities.GetResourceString("MasterCodingStructure")%>:</span>&nbsp;
        <%: Html.ActionLink(Constants.MANAGE, Constants.LIST, Constants.CODE)%>
    <% } %>
</asp:Content>
