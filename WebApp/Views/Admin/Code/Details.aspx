﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.TemplateCodeViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Details/ucCodingTemplateDetails.ascx", Model); %>
    <br />
    <% if (Utilities.UserHasPrivilege("MCDS", Constants.SYSTEM))
       { %>
    <%: Html.ActionLink(Constants.EDIT, Constants.INPUT, Constants.CODE, new { id = Model.CodingTemplateId }, new { @class = "button" })%>
    <% } %>
    <%: Html.ActionLink(Constants.BACK, Constants.LIST, Constants.CODE, null, new { @class = "button" })%>
</asp:Content>