﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.TemplateCodeViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm())
   { %>
        <%: Html.ValidationSummary() %>
        <% Html.RenderPartial("~/Views/Shared/Input/ucCodingTemplateInput.ascx", Model); %>
        <br />
        <% if (Utilities.UserHasPrivilege("MCDS", Constants.SYSTEM))
           { %>
        <input type="submit" class="button" name="action" value="<%=Constants.SAVE %>" />
        <%: Html.ActionLink(Constants.CANCEL, Constants.LIST, Constants.CODE, null, new { @class = "button" })%>
        <% } %>
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.CodingTemplateId) %>
    <%: Html.HiddenFor(m => m.Selection) %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.admin.codingtemplateinput.js"));
%>
</asp:Content>
