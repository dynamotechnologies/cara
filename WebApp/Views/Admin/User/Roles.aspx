﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.UserRolesViewModel>" %>
<%@ Import Namespace="Aquilent.Framework.Security" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%: Html.ValidationSummary() %>    
<%
    Html.Telerik().Grid<UserSystemRole>(Model.Roles.OrderBy(usr => usr.UnitId))
        .Name(Constants.LIST)
        .Columns(columns =>
        {
            columns.Bound(usr => usr.RoleId)
                .Title("Role")
                .Width(200)
                .Template(usr => { %><%= Model.GetRoleName(usr.RoleId) %><% });

            columns.Bound(usr => usr.UnitId)
                .Title("Unit")
                .Width(400)
                .Template(usr => { %><%= Model.GetUnitName(usr.UnitId) %><% });
                
            columns.Bound(usr => usr.UnitId)
                .Title("Remove")
                .Template(usr => 
                    {
                        if (Model.CanRemoveRole(usr.UnitId, usr.RoleId))
                        {
                            %>
                            <%: Html.ActionLink("Remove Role", "RemoveRole", "User", new { id = Model.UserToManage.UserId, roleId = usr.RoleId, unitId = usr.UnitId }, new { @class = "button" }) %>
                            <%
                        }
                    })
                .HtmlAttributes(new { style = "text-align:center;" });
        })
        .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
        .HtmlAttributes(new { style = "width:750px;" })
        .Render();
            
   using (Html.BeginForm("AddRole", "User", FormMethod.Post))
   { 
%>      
        <br />
        <div class="sectionTitle">Add a Role</div>
        <div class="filterArea">
            <table>
                <tr>
                    <td class="fieldLabel"><span class="required">*</span><label for="RoleId"><%:Constants.ROLE %></label>:</td>
                    <td class="text">
                        <%: Html.Telerik().DropDownListFor(model => model.RoleId)
                            .BindTo(Model.RolesToAssign)
                            .HtmlAttributes(new { style = "width:230px;" })%>
                    </td>
                </tr>
                <tr>
                    <td class="fieldLabel"><label for="UnitId"><%:Constants.UNIT %></label></td>
                    <td class="text">
                            <%: Html.TextBoxFor(model => model.UnitName, new { disabled = "disabled", size = "40" }) %>
                            <% Html.RenderPartial("~/Views/Shared/Util/ucUnitTreeSelector.ascx"
                                   , new UnitTreeSelectorModel
                                   {
                                       Units = Model.Units,
                                       Visible = false,
                                       Closeable = true,
                                       Modal = true,
                                       UnitIdInputId = "UnitId",
                                       UnitNameInputId = "UnitName"
                                   }); %>
                            <%: Html.HiddenFor(model => model.UnitId) %>
                            <a id="btnChooseUnit" class="button" onclick="$('#UnitSelectorWindow').data('tWindow').center().open()">Choose Unit</a>
                    </td>
                </tr>
                <tr>
                    <td class="fieldLabel" colspan="2"><input class="button" type="submit" value="Submit" /></td>
                </tr>
            </table>
        </div>



    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <input type="hidden" name="id" value="<%= Model.UserToManage.UserId %>" />
<% } %>
</asp:Content>
