﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.DataMartUserViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary() %>
    <% if (TempData[Constants.CONFIRM] != null)
       { %>
       <div class="message"><%: TempData[Constants.CONFIRM]%></div><br />
    <% } %>
        <% if (TempData[Constants.ERROR] != null)
       { %>
        <div class="error"><%=TempData[Constants.ERROR]%></div>
    <% } %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucDataMartUserInput.ascx", Model); %>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
<% } %>
</asp:Content>
