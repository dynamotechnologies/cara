﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.MemberViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% if (Model.IsWizardMode)
       {
           Html.RenderAction("WizardNav", Constants.NAV, new { actionName = ViewData["action"], mode = Model.Mode });
       }
       else
       {
           Html.RenderAction("ProjectNav", Constants.NAV);
       } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% bool showConfirm = !string.IsNullOrEmpty(TempData[Constants.PHASE + Constants.MEMBER] as string) && !Model.IsWizardMode;%>
        <div id="confirmAction" <%=showConfirm?"":"hidden" %> >
            <div class="message" style="width:500px;"><%: TempData[Constants.PHASE + Constants.MEMBER] as string%></div>
            <br />
        </div>
    <div class="pageDesc">
        <%: Html.DisplayFor(model => model.Description)%>
        <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Team Member Note" title="CARA Team Member designation is only for staff or contractors who will be reviewing, coding, analyzing or otherwise be participating in the comment analysis portion of the project." />
    </div>
    <br />
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary() %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucMemberInput.ascx", Model); %>
    <br />
    <% if (Model.IsWizardMode)
        { %>
        <input type="submit" class="button" name="action" value="<%=Constants.BACK %>" />
        <input type="submit" class="button" name="action" value="<%=Constants.NEXT %>" />
        <input type="submit" class="button" name="action" value="<%=Constants.FINISH %>" />
    <% } %>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Description) %>
    <%: Html.HiddenFor(m => m.Mode) %>
    <%: Html.HiddenFor(m => m.ShowButtons) %>
    <%: Html.HiddenFor(m => m.Locked) %>
<% } %>
</asp:Content>
