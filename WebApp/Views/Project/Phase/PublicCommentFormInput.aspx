﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PublicCommentFormInputViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% if (Model.IsWizardMode)
       {
           Html.RenderAction("WizardNav", Constants.NAV, new { actionName = ViewData["action"], mode = Model.Mode });
       }
       else
       {
           Html.RenderAction("ProjectNav", Constants.NAV);
       } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageDesc"><%: Html.DisplayFor(model => model.Description)%></div>
    <br />
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary() %>
    <br />
    <% if (Model.IsWizardMode)
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.BACK %>" onclick="submitForm('<%=Constants.BACK %>')"/>
        <input type="button" class="button" name="action" value="<%=Constants.NEXT %>" onclick="submitForm('<%=Constants.NEXT %>')" />
        <input type="button" class="button" name="action" value="<%=Constants.FINISH %>" onclick="submitForm('<%=Constants.FINISH %>')"/>
        <% }
        else if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.SAVE %>" onclick="submitForm('<%=Constants.SAVE %>')" />
    <% } %>

    <br /><br />
    <% Html.RenderPartial("~/Views/Shared/Input/ucPublicCommentFormInput.ascx", Model); %>
    <br />
    <% Html.RenderPartial("~/Views/Shared/Input/ucPublicReadingRoomInput.ascx", Model); %>
    <br />
    
    <% if (Model.IsWizardMode)
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.BACK %>" onclick="submitForm('<%=Constants.BACK %>')"/>
        <input type="button" class="button" name="action" value="<%=Constants.NEXT %>" onclick="submitForm('<%=Constants.NEXT %>')" />
        <input type="button" class="button" name="action" value="<%=Constants.FINISH %>" onclick="submitForm('<%=Constants.FINISH %>')"/>
        <% }
        else if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.SAVE %>" onclick="submitForm('<%=Constants.SAVE %>')" />
    <% } %>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Description) %>
    <%: Html.HiddenFor(m => m.Mode) %>
    <%: Html.HiddenFor(m => m.ProjectId)%>
    <%: Html.HiddenFor(m => m.PhaseId)%>
    <%: Html.HiddenFor(m => m.ActionString) %>

<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/jquery.cookie.js")
            .Add("~/Scripts/cara.publiccommentforminput.js")
            ); %>
</asp:Content>




