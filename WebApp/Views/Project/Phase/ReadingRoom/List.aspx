﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterReadingRoomListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% if (!string.IsNullOrEmpty(TempData[Constants.READING_ROOM_MGMT] as string))
    { %>
    <div id="confirmAction">
        <div class="message" style="width:500px;"><%: TempData[Constants.READING_ROOM_MGMT] as string%></div>
        <br />
    </div>
<% } %>
<% using (Html.BeginForm())
   { %>
		<%: Html.ValidationSummary() %>
        <% Html.RenderPartial("~/Views/Shared/Filter/ucLetterReadingRoomFilter.ascx", Model); %>
        <br />
        <%= Html.ActionLink(Utilities.GetResourceString(Constants.PUBLIC_READING_ROOM), Constants.READING_ROOM, Constants.PUBLIC, new { project = Model.ProjectNumber }, new { id = "linkPublicReadingRoom", target = "_blank" }).ToHtmlString()%>
        <br />
        <br />
        <% Html.RenderPartial("~/Views/Shared/List/ucLetterReadingRoomSearchList.ascx", Model); %>
        <br />
        <% if (Utilities.UserHasPrivilege("PURR", Constants.PHASE))
            { %>
            <input type="submit" class="button" name="action" value="<%:Constants.UPDATE %>" />
        <% } %>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.PhaseId) %>
    <%: Html.HiddenFor(m => m.IsPalsProject) %>
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Mode)%>
    <!-- variable to keep track of the changed letter data -->
    <%: Html.HiddenFor(model => model.UpdatedLetterList, new { id = "txtUpdatedLetterList" })%>
<% } %>

<% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/telerik.window.min.js")
                .Add("~/Scripts/telerik.window.dialog.js")
                .Add("~/Scripts/cara.readingroomlist.js"));
    %>
</asp:Content>