﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.DmdDocumentViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% if (Model.IsWizardMode)
       {
           Html.RenderAction("WizardNav", Constants.NAV, new { actionName = ViewData["action"], mode = Model.Mode });
       }
       else
       {
           Html.RenderAction("ProjectNav", Constants.NAV);
       } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (!string.IsNullOrEmpty(TempData[Constants.PHASE + Constants.DOCUMENT] as string) && !Model.IsWizardMode)
        { %>
        <div id="confirmAction">
            <div class="message" style="width:500px;"><%: TempData[Constants.PHASE + Constants.DOCUMENT] as string%></div>
            <br />
        </div>
    <% }
	   else if (!string.IsNullOrEmpty(TempData[Constants.ERROR] as string))
	   { %>
		   <br />
           <div class="validation-summary-errors"><%: TempData[Constants.ERROR] %></div>
		   <br />
	<% } %>
    <div class="pageDesc">
        <% if (Model.IsPalsProject)
           { %>
        <%: Html.DisplayFor(model => model.Description)%>
        <% }
           else
           { %>
           Uploading documents to a comment period makes them available through
           the public comment web form and to project team members for easy
           reference. Only documents that are in a final format and ready for
           public dissemination should be uploaded for inclusion on the web form.
           If you have any questions about which documents to post on your web
           form, please consult your leadership. 
        <% } %>
    </div>
        <% if (Model.IsPalsProject)
           { %>
    <br />
    <b><%=string.Format("{0} {1} Link:", Utilities.GetResourceString("PALS"), Constants.PROJECT) %></b>&nbsp;<a target="_blank" href="<%=Model.PalsProjectUrl %>"><%=Html.Encode(Model.PalsProjectUrl) %></a>
        
    <br /><% } %><br /><br />
<% using (Html.BeginForm(Constants.DOCUMENT + Constants.INPUT, Constants.PHASE, FormMethod.Post, new { enctype = "multipart/form-data" }))
   { %>
    <%: Html.ValidationSummary() %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucPhaseDocumentInput.ascx", Model); %>
    <br />
   <%  if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
       { %>
        <% if (Model.IsPalsProject && (Model.ProjectDocuments != null && Model.ProjectDocuments.Count > 0))
           { %>
               <input type="submit" class="button" name="action" value="<%=Constants.ASSOCIATE %>" />
        <% } %>

        <% if (!Model.IsPalsProject) { %><input type="submit" class="button" name="action" value="<%=Constants.UPLOAD %>" /><% } %>
    <% } %>

    <% if (Model.IsWizardMode)
       { %>
        <input type="submit" class="button" name="action" value="Back" />
        <input type="submit" class="button" name="action" value="Next" />
        <input type="submit" class="button" name="action" value="Finish" />
    <% } %>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Description) %>
    <%: Html.HiddenFor(m => m.Mode) %>
    <%: Html.HiddenFor(m => m.ProjectDocumentId) %>
    <%: Html.HiddenFor(m => m.ShowButtons) %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.phasedocumentinput.js"));
%>
</asp:Content>