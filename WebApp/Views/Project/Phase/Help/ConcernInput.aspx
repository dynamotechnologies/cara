﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle%></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ConcernResponseStrategies").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ConcernResponseVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ConcernResponseVideo"), Name = Constants.CONCERN_RESPONSE })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Concern/Response - In Progress")%><br />
    <%: Html.DefinitionActionLink("Ready for Response")%><br />
    <%: Html.DefinitionActionLink("Response in Progress")%><br />
    <%: Html.DefinitionActionLink("Concern/Response - Response Complete")%><br />
    <%: Html.DefinitionActionLink("Concern and Response Status")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Concern and response page?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can click on any of the buttons located at the bottom to go back to the concern and response listing page. Options include Save, Ready for response, and Response complete. Choose the option that best fits the current status of the Concern and Response.<br /><br />
        Method 2: You can go to the concern and response listing page from the expanded left navigation by clicking on Concerns / responses within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Coded Comments page?</div>
    <div id="link2" class="helpHidden">
        You can go to the coded comments page from the expanded left navigation by clicking on Coded comments within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Letter Inbox?</div>
    <div id="link3" class="helpHidden">
        You can go to the Comment Period letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Comment Period homepage?</div>
    <div id="link4" class="helpHidden">
        You can go to the Comment Periods homepage from the expanded left navigation by clicking on Comment Period Home for the Comment Period of interest.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Create a concern statement?</div>
    <div id="link5" class="helpHidden">
        You can type the desired text into the text field box labeled Concern statement. To save your work click on the button located at the bottom labeled Save, which will designate the Concern and Response with the status Concern in Progress. When you are satisfied with your completed concern statement click on the button located at the bottom entitled Ready for response to change the status of the concern and response to reflect that the concern statement has been completed.<br /><br />
        Note: Writing concern statements is optional.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Create a response?</div>
    <div id="link6" class="helpHidden">
        You can type the desired text into the text field box labeled Response. To save your work click on the button located at the bottom labeled Save, which will designate the Concern and Response with the status Response in Progress. When you are satisfied with your completed response click on the button located at the bottom entitled Response complete to change the status of the concern and response to reflect that the concern and response is complete.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Add or remove comments to this concern and response?</div>
    <div id="link7" class="helpHidden">
        You can remove associated comments by clicking on the remove icon listed under the column title Remove comment for the corresponding comment of interest. You can add more comments to the concern and response by clicking on the Add more comments button located directly below the comment listing table on the concern and response writing page.  Selecting the Add more comments button will take you to the Coded comments page where you may then add more comments to associate with this concern and response.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Manage sample statements?</div>
    <div id="link8" class="helpHidden">
        You can flag a comment as a sample statement by clicking to select the checkbox located under the column title Sample statement for the corresponding statement that you wish to designate as a sample statement. To remove the designation of a comment as a sample statement you can click to uncheck the check box located under the column title Sample statement for the corresponding comment. You can click the button labeled Save to save your progress.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Modify the status of this concern and response?</div>
    <div id="link9" class="helpHidden">
        Once you begin typing a Concern, the status will be designated as Concern in Progress. Once you have completed the Concern you can click on the button labeled Ready for Response to designate the status as Ready for Response. Once you type text in the Response text field, the status will be changed to Response in Progress. Once you are satisfied with the Response you can click on the button labeled Response complete to designate the status of the Concern and Response as Response Complete. 
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Delete a concern statement/response?</div>
    <div id="link10" class="helpHidden">
        From the Concern and Response listing page you can delete a concern and response statement by clicking on the delete icon listed under the column titled Delete for the corresponding concern and response listed within the concern and response listing page.
    </div>
    <br />
</asp:Content>