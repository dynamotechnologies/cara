﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "CodingStrategy").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Basic Coding Structure")%><br />
    <%: Html.DefinitionActionLink("Detailed Coding Structure")%><br />
    <%: Html.DefinitionActionLink("Appeals (Action Codes)")%>
    <%: Html.DefinitionActionLink("Appeals (Resources Codes)")%>
    <%: Html.DefinitionActionLink("Reuse")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Previous step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Back located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Next step of the Project setup wizard?</div>
    <div id="link2" class="helpHidden">
        You can click on the button labeled Next located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Approve Project creation page?</div>
    <div id="link3" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Project homepage?</div>
    <div id="link4" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can click on the button labeled Accept to the current Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">View the basic, or detailed coding structure?</div>
    <div id="link6" class="helpHidden">
        You can view a preview of the corresponding coding structure types under the column labeled Description. You can click the name of the corresponding coding structure types to view the codes that are included in either the basic, or detailed coding structure.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Select the coding structure to use?</div>
    <div id="link7" class="helpHidden">
        You can select a coding structure via its corresponding radio button labeled Select and click Next.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Decide which coding structure to select?</div>
    <div id="link8" class="helpHidden">
        CARA provides standard Action or Issue and Resource or Rationale code lists that should be selected as a starting point.  The Basic Coding structure, for use on small, less complex Projects, consists of 10 high-level Action or Issue codes and 11 high-level Resource or Rationale codes.  The Detailed Coding structure, for use on Projects such as Forest Plan Revisions, includes the Basic codes and over 300 sub-codes that provide greater detail in all topic areas. The selected coding structure can be modified as desired in the next step by adding or removing standard and custom codes.<br /><br />
        Recommendations:<br /><br />
        For small Projects select the Basic coding structure (+/- custom codes)<br /><br />
        For large Projects select the Detailed coding structure (+/- custom codes)<br /><br />
        For Projects in appeal/litigation select the Appeal/Litigation coding structure<br /><br />
        For additional information on coding basics, view the document titled Coding Strategy.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Reuse a coding structure?</div>
    <div id="link9" class="helpHidden">
        You can click on the button labeled Reuse coding structure located towards the bottom of the page to search for a Project and its corresponding Comment Period from where you wish to reuse the coding structure. You can reuse any coding structure which was used in a previously created Comment Period of any Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Finish creating this Comment Period?</div>
    <div id="link10" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can accept this Project and go to its Project homepage, or cancel the creation of this Project. You can edit this Comment Period at a later date by modifying its settings from the Project homepage, Comment Period homepage, as well as by visiting this Project creation wizard. Selecting Finish at this point will apply default settings for the remaining steps. You may view and modify these default settings in the approve Project creation page.
    </div>
    <br />
</asp:Content>