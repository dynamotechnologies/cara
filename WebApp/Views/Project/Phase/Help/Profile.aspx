﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Time Zone")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link2')">Manage the CARA Comment Period letter received notification?</div>
    <div id="link2" class="helpHidden">
        You can elect to be notified when a designated number of submissions have been received for a particular Comment Period. You can select the number of letters that should be received prior to you being notified from the drop down and selecting the check box to confirm that you wish to receive the notification. Press the Save button located at the bottom to save any changes. You will be notified at your Forest Service email address.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Manage the CARA Comment Period closing notification?</div>
    <div id="link3" class="helpHidden">
        You can elect to be notified when there are 5 days remaining in the closing of a Comment Period. You can select the check box to confirm that you wish to receive the notification. Press the Save button located at the bottom to save any changes. You will be notified at your Forest Service email address.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Establish the number of rows to display in search result tables?</div>
    <div id="link4" class="helpHidden">
        You can select the number of rows that CARA will display on a page by page basis from the drop down located next to the text displayed as Number of search result rows shown per page. This display row setting will affect most, but not all tables within CARA. Press the Save button located at the bottom to save any changes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Select my default letter font size for the letter coding page?</div>
    <div id="link6" class="helpHidden">
        You may select your default Letter font size for use on the letter coding page by selecting between 10, 12, and 14 font sizes from the dropdown menu labeled Letter font size.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Select my time zone?</div>
    <div id="link5" class="helpHidden">
        You can select your time zone from the provided drop down menu next to the text displayed as Select time zone. Press the Save button located at the bottom to save any changes.
    </div>
    <br />
</asp:Content>