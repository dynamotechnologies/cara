﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Form Management")%><br />
    <%: Html.DefinitionActionLink("Set")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Plus")%><br />
    <%: Html.DefinitionActionLink("Master Form Letter")%><br />
    <%: Html.DefinitionActionLink("Duplicates")%><br />
    <%: Html.DefinitionActionLink("Unique Letter")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Form Management page?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the form management page from the expanded left navigation by clicking on Form Management within the desired Comment Period.<br /><br />
        Method 2: You can click on the Form Management button located above and below the form change log.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Letter inbox?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the Comment Periods letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Comment Period homepage?</div>
    <div id="link3" class="helpHidden">
        Method 1: You can go to the Comment Periods homepage from the expanded left navigation by clicking on Comment Period Home for the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Project homepage?</div>
    <div id="link4" class="helpHidden">
        You can go to the Projects homepage from the expanded left navigation by clicking on the Project Home.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View the changed Form designations?</div>
    <div id="link5" class="helpHidden">
        The change log provides you with the listing of all re-designation or movement of letters. The entries are listed by date, with the most recent placed at the top.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Change a listing within the summary log?</div>
    <div id="link6" class="helpHidden">
        Method 1: You can go to the form management page from the expanded left navigation by clicking on Form Management within the desired Comment Period. From within the form management page you can take the appropriate actions to rectify any identified form letter designations.<br /><br />
        Method 2: You can click on the Form Management button located above and below the form change log. From within the form management page you can take the appropriate actions to rectify any identified form letter designations.
    </div>
    <br />
</asp:Content>