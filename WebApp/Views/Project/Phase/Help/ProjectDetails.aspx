﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("New Letters")%><br />
    <%: Html.DefinitionActionLink("Unique Letter")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Plus")%><br />
    <%: Html.DefinitionActionLink("Master Form Letter")%><br />
    <%: Html.DefinitionActionLink("Duplicates")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%><br />
    <%: Html.DefinitionActionLink("Coding in Progress")%><br />
    <%: Html.DefinitionActionLink("Active")%><br />
    <%: Html.DefinitionActionLink("Archived")%><br />
    <%: Html.DefinitionActionLink("Not Yet Active")%><br />
    <%: Html.DefinitionActionLink("Viewing Status")%><br />
    <%: Html.DefinitionActionLink("Comment Analysis Status")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>
    <div id="link2" class="helpHidden">
        You can go to the homepage of a Comment Period by selecting the name of the Comment Period in the Project homepage.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Comment Period letter inbox?</div>
    <div id="link3" class="helpHidden">
        Method 1: You can go to the letter inbox of a Comment Period by selecting the corresponding number listed under the column titled Total Letters in the Project homepage.<br /><br />
        Method 2: You can click and go to the letter inbox from the expanded left navigation for the corresponding Comment Period letter inbox of interest.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link4')">View more PALS Project information?</div>
    <div id="link4" class="helpHidden">
        You can click on the Show PALS Project information link located under the Comment Period listing table in the Project homepage. This table also includes a link to the read only view of the associated PALS Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">View Early Attention Required letters?</div>
    <div id="link5" class="helpHidden">
        You can view the Early Attention required letters of a Comment Period by selecting the corresponding number listed under column title Early Attention letters in the Project homepage.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">View associated documents?</div>
    <div id="link6" class="helpHidden">
        You can click and go to the associated documents from the expanded left navigation for the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Make changes to the CARA Project?</div>
    <div id="link7" class="helpHidden">
        Method 1: You can go to the homepage of a Comment Period by selecting the name of the Comment Period in the Project homepage. Once in the Comment Period homepage the CARA users with the authorized roles will have the option to modify the Comment analysis, Viewing status, Coding structure, as well as manage associated documents and team members.<br /><br />
        Method 2: The CARA users with the authorized roles can click and go to the CARA Project setup wizard from the expanded left navigation for the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Create a new Comment Period for this CARA Project?</div>
    <div id="link8" class="helpHidden">
        Method 1: The CARA users with the authorized roles can click and go to the Create CARA Project Comment Period from the expanded left navigation.<br /><br />
        Method 2: The CARA users with the authorized roles can click and go to the CARA Project setup wizard from the expanded left navigation. From within the Project set up, you can create another Comment Period.
    </div>
    <br />
</asp:Content>