﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("NEPA")%><br />
    <%: Html.DefinitionActionLink("PALS")%><br />
    <%: Html.DefinitionActionLink("Coordinator (Role)")%><br />
    <%: Html.DefinitionActionLink("Project Manager (Role)")%><br />
    <%: Html.DefinitionActionLink("CARA Comment Period Manager")%><br />
    <%: Html.DefinitionActionLink("Team Member (Role)")%><br />
    <%: Html.DefinitionActionLink("Project Setup Wizard")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Next step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Next located towards the bottom of the page. You can only click on next if this Project is a NEPA Project, has an associated PALS Project, and a Project manager has been selected for this Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">CARA homepage?</div>
    <div id="link2" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation. This will cancel the Project creation process.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Project homepage?</div>
    <div id="link3" class="helpHidden">
        You must complete the next CARA Project setup step before you can view the current Projects homepage.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Comment Period homepage?</div>
    <div id="link4" class="helpHidden">
        You must complete the next CARA Project setup step before you can view the current Projects Comment Period.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Associate a NEPA / non NEPA project?</div>
    <div id="link5" class="helpHidden">
        You can select to designate this Project as a NEPA Project from the radio button option labeled NEPA Project. Click yes to associate the NEPA Project with this CARA Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Search and link the CARA Project to a PALS Project?</div>
    <div id="link6" class="helpHidden">
        You can type a PALS Project ID or keyword in the text box labeled as Enter a PALS ID or Project name to search (supports partial searches) for the PALS Project to associate with this CARA Project. Once the Project search results are displayed you can select the PALS Project to associate via its corresponding radio button.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Search and establish a Project manager for this Project?</div>
    <div id="link7" class="helpHidden">
        You can type either the first name or the last name to search for a PALS user to associate as the Project manager with this CARA Project. Once the search results are displayed, you can select to associate the Project manager via their corresponding radio button and then click on Add Project Manager to add the Project Manager to this CARA Project.
    </div>
    <br />
</asp:Content>