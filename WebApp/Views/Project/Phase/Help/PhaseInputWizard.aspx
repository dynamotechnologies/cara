﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("Formal Comment Period")%><br />
    <%: Html.DefinitionActionLink("Object Comment (Filing) Period")%><br />
    <%: Html.DefinitionActionLink("Informal Comment Period")%><br />
    <%: Html.DefinitionActionLink("Duration")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Next step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Next located towards the bottom of the page. You can only click on next once you have selected the type of Comment Period to create, as well as provided the start date of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Approve Project creation page?</div>
    <div id="link2" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. You can only click on Finish once you have selected the type of Comment Period to create, as well as provided the start date of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can click on the button labeled Accept to go to the current Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">Select the type of Comment Period to create (formal, informal, objection)?</div>
    <div id="link6" class="helpHidden">
        You can select the type of Comment Period you want to create via the drop down options labeled as Type. For viewing the business rules associated with the Comment Period creation you can click to open the document Business rules for Project creation under the Support tab.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Provide a Comment Period description?</div>
    <div id="link7" class="helpHidden">
        You can type a short description of the Projects Comment Period in the text field labeled as Description.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Enter the CARA Project start date?</div>
    <div id="link8" class="helpHidden">
        You can click on the calendar icon to open an interactive calendar from which you can select the date on which you want the Comment Period to start. The CARA Project start date will be the date when the public facing comment web forms are made available for eligible Projects.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Modify the duration date?</div>
    <div id="link9" class="helpHidden">
        You can modify the duration date of the Comment Period by typing the number of days you want the Comment Period to last in the text field labeled as Duration. Certain business rules apply when modifying (subject to rules governing Appeals and Formal Comment Periods) the auto generated duration date. In order to view the business rules associated with creating a CARA Project you can click on the document labeled Business rules for Project creation located under the column Support on this help page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Finish creating this Comment Period?</div>
    <div id="link10" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can accept this Project and go to its Project homepage. You can edit this Comment Period at a later date by modifying its settings from the Project homepage, Comment Period homepage, as well as by visiting this Project creation wizard.<br /><br />
        Selecting Finish at this point will apply default settings for the remaining steps. You may view and modify these default settings in the approve Project creation page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">View the business rules associated with the different Comment Periods?</div>
    <div id="link11" class="helpHidden">
        You can click on the document labeled Business rules for Project creation located under the column Support on this help page.
    </div>
    <br />
</asp:Content>