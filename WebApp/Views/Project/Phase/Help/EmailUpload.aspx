﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "Export2TextInstructions").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "AgentInstallationInstructions").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter inbox?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the Comment Period letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the Comment Period homepage from the expanded left navigation by clicking on Comment Period Home for the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the expanded left navigation by clicking on Comment Period Home of the Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can go to the Projects homepage from the expanded left navigation by clicking on the Project Home.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link4')">Export emails from a Lotus Notes inbox?</div>
    <div id="link4" class="helpHidden">
        View the Export2Text_Instructions document for instructions on how to use the Export2Text agent to export Lotus Notes email.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">How do I install the Export2Text agent in Lotus Notes?</div>
    <div id="link5" class="helpHidden">
        See the  Agent_Installation_Instructions document for instructions on how to install the Export2Text agent.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Upload email letters?</div>
    <div id="link6" class="helpHidden">
        To upload emails for this Comment Period, you must first export the emails from the designated Lotus Notes email inbox. Once the emails have been exported, you can browse for the export file and upload it. This feature uploads the email letters and its attachments.<br /><br />
        NOTE: In order to export emails from a Lotus Notes inbox, you must have Lotus Notes' "Export2Text" agent installed on your workstation. For instructions on installing the Export2Text agent or additional information on exporting emails from Lotus Notes, please refer to the Support page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">View uploaded email letters?</div>
    <div id="link7" class="helpHidden">
        You can view the email letters that have been successfully uploaded in the letter inbox for the Comment Period.
    </div>
    <br />
</asp:Content>