﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("Active")%><br />
    <%: Html.DefinitionActionLink("Archived")%><br />
    <%: Html.DefinitionActionLink("Not Yet Active")%><br />
    <%: Html.DefinitionActionLink("Viewing Status")%><br />
    <%: Html.DefinitionActionLink("Reading Room Status")%><br />
    <%: Html.DefinitionActionLink("Comment Analysis Status")%><br />
    <%: Html.DefinitionActionLink("Team Only")%><br />
    <%: Html.DefinitionActionLink("Coding Structure")%><br />
    <%: Html.DefinitionActionLink("Auto-markup")%><br />
    <%: Html.DefinitionActionLink("Standard Reports")%><br />
    <%: Html.DefinitionActionLink("Public Reading Room")%><br />
    <%: Html.DefinitionActionLink("Project Setup Wizard")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">A different Comment Period homepage for the same Project?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to a different CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest. The option to select and go to another Comment Period will be available if another Comment Period has already been set up.<br /><br />
        Method 2: You can click on the Project name located directly below the global navigation. From the Project homepage you can go to the homepage of another Comment Period by selecting the name of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">A different Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the Home tab in the top navigation to go to your My Projects table. From this table you can then select the name of the Project of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Letter inbox?</div>
    <div id="link4" class="helpHidden">
        Method 1: You can go to the letter inbox for a Comment Period by clicking on its name from the expanded left navigation.<br /><br />
        Method 2: If you have the “Letters” tab selected, you can also go to the inbox by selecting a number there.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('prrlink')">The Public Reading Room?</div>
    <div id="prrlink" class="helpHidden">
		If a Public Reading Room has been established for the project comment period, a link to the corresponding Public Reading Room webpage will be present on this page.  Clicking this link will present the webpage that contains the published letters corresponding to the particular project comment period in a new browser window.
	</div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Modify the Comment Period settings?</div>
    <div id="link5" class="helpHidden">
        Method 1: CARA users with the authorized roles can select the desired Comment Period attribute tab to modify the Comment Period settings from the Comment Period homepage. Modification options include Comment analysis, Viewing status, Coding structure, as well as manage associated documents and team members.<br /><br />
        Method 2: CARA users with the authorized roles can click and go to the CARA Project setup wizard from the expanded left navigation to modify the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">View the coding structure?</div>
    <div id="link6" class="helpHidden">
        Method 1: You can select and view the Coding Structure tab within the Comment Period homepage. CARA users with the authorized roles will have the option to modify the coding structure.<br /><br />
        Method 2: You can view the Comment Period’s coding structure from the expanded left navigation by clicking on Coding structure. CARA users with the authorized roles will have the option to modify the coding structure.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">View team members?</div>
    <div id="link7" class="helpHidden">
        You can select and view the Team members by selecting the Team members tab within the Comment Period homepage. CARA users with the authorized roles will have the option to modify the team members.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">View coded comments?</div>
    <div id="link8" class="helpHidden">
        Method 1: You can go to the coded comments page from the expanded left navigation by clicking on Coded comments for the Comment Period of interest.<br /><br />
        Method 2: You can select and view the Comment Period’s letters by selecting the Letters tab from within the Comment Period homepage. From within this tab you may select the corresponding number listed under Complete to view the letters that have been coded.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">View letters to begin coding?</div>
    <div id="link9" class="helpHidden">
        Method 1: You can select and view the Comment Period’s letters by selecting the Letters tab from within the Comment Period homepage. From within this tab you may select the corresponding number listed under New or In progress to view the letters which you can code.<br /><br />
        Method 2: You can go to the coded comments page from the expanded left navigation by clicking on Coded comments for the Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">View / Create Concern & Response?</div>
    <div id="link10" class="helpHidden">
        You can select and view the Comment Period’s concerns and responses by selecting the Concern and Response tab from within the Comment Period homepage. From within this tab you may click to view concerns and responses which are In progress, Ready for Response, Response in progress, Response Complete, as well as create a new concern and response.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">View associated documents?</div>
    <div id="link11" class="helpHidden">
        Method 1: You can select and view the Comment Period’s associated documents by selecting the Documents tab from within the Comment Period homepage. From within this tab you may click to open any associated documents.<br /><br />
        Method 2: You can go to the Comment Period’s associated documents by clicking on Documents from the expanded left navigation for the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link12')">View auto-markup terms?</div>
    <div id="link12" class="helpHidden">
        Method 1: You can select the auto-markup tab within the Comment Period homepage to view the Comment Period’s selected List of Terms.<br /><br />
        Method 2: You can view the Comment Period’s List of Terms selected from the expanded left navigation by clicking on Auto-markup.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link13')">Upload letters via email or FDMS?</div>
    <div id="link13" class="helpHidden">
        Method 1: You can select and view the Comment Period’s letters by selecting the Letters tab from within the Comment Period homepage. From within this tab, the CARA users with the authorized roles can click the button for Upload emails or Upload FDMS to select the option to upload letters via email or FDMS.<br /><br />
        Method 2: CARA users with the authorized roles can click and upload letters via email or FDMS by selecting the Email upload or Upload via FDMS options under the expanded Letters section within the expanded Comment Period left navigation.
    </div>
    <br />		
    <div class="helpLink" onclick="toggleArea('link14')">Enter a new letter?</div>
    <div id="link14" class="helpHidden">
        Method 1: You can select and view the Comment Period’s letters by selecting the Letters tab from within the Comment Period homepage. From within this tab, the CARA users with the authorized roles can click to submit comments on behalf of the public (e.g., hand written letters, comments received via telephone, or in-person).<br /><br />
        Method 2: CARA users with the authorized roles can click to create a new letter by selecting the Enter new letter option under the expanded Letter inbox section within the expanded Comment Period left navigation. From within the Enter new letter option the CARA users with the authorized roles can click to add comments not received via the web form (e.g., hand written letters, comments received via telephone, or in-person).
    </div>
    <br />		
    <div class="helpLink" onclick="toggleArea('link15')">Generate reports?</div>
    <div id="link15" class="helpHidden">
        Method 1: You can select the Reports section from the expanded Comment Period left navigation. From within this section, you will be able to generate the standard reports available for the corresponding Comment Period.<br /><br />
        Method 2: You can select the Reports tab from within the Comment Period homepage. From within this tab, you will be able to generate the standard reports available for the corresponding Comment Period.<br /><br />
        Method 3: You can select the Reports tab from the global navigation. You can then search and select the Project, as well as the Comment Period, for which you wish to generate either the standard or custom reports.
    </div>
    <br />		
    <div class="helpLink" onclick="toggleArea('prrlinkactivate')">Activate the Public Reading Room?</div>
    <div id="prrlinkactivate" class="helpHidden">
		CARA users with the authorized roles will see a line for “Reading Room Status”.   To make the Public Reading Room available, select the “Activate” button.  To disable the Public Reading Room site, select the “Deactivate” button.
    </div>
    <br />		
</asp:Content>