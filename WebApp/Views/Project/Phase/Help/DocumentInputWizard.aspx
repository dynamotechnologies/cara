﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Associate")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Previous step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Back located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Next step of the Project setup wizard?</div>
    <div id="link2" class="helpHidden">
        You can click on the button labeled Next located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Approve Project creation page?</div>
    <div id="link3" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Project homepage?</div>
    <div id="link4" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can click on the button labeled Accept to go to the current Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">Manage document association with this Comment Period?</div>
    <div id="link6" class="helpHidden">
        You can associate none, one, or many documents to the Comment Period by selecting the check box under the column Select and click Associate. Once checked the corresponding document will be associated to this Comment Period. If you wish you disassociate the document(s), simply uncheck the check box.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Decide which documents to associate?</div>
    <div id="link7" class="helpHidden">
        The document(s) that should be associated to a Comment Period are those documents that the Forest Service intends to release to the public in an effort to solicit comments.  These documents typically consist of Scoping documents, Environmental Assessments, Environment Impact Statements, and any supporting materials.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Finish creating this Comment Period?</div>
    <div id="link8" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can accept this Project and go to its Project homepage, or cancel the creation of this Project. You can edit this Comment Period at a later date by modifying its settings from the Project homepage, Comment Period homepage, as well as by visiting this Project creation wizard. Selecting Finish at this point will apply default settings for the remaining steps. You may view and modify these default settings in the approve Project creation page.
    </div>
    <br />
</asp:Content>