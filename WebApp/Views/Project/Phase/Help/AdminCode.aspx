﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Basic Coding Structure")%><br />
    <%: Html.DefinitionActionLink("Detailed Coding Structure")%><br />
    <%: Html.DefinitionActionLink("Appeals (Action Codes)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Resources Codes)")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">CARA Admin page?</div>
    <div id="link2" class="helpHidden">
        You can go to the CARA Admin page by selecting the Admin tab in the top navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link3')">View the coding structure?</div>
    <div id="link3" class="helpHidden">
        You can click on the name of the coding structure located under the column Template name to view the coding structure.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Modify the coding structure?</div>
    <div id="link4" class="helpHidden">
        You can modify a coding structure by clicking on the button labeled Edit next to the corresponding coding structure name. Any changes to these coding structures will be applied globally across CARA for future Projects.
    </div>
    <br />
</asp:Content>