﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "CodingStrategy").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("CodingVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("CodingVideo"), Name = Constants.CODING })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Append")%><br />
    <%: Html.DefinitionActionLink("Author")%><br />
    <%: Html.DefinitionActionLink("Common Interest Class")%><br />
    <%: Html.DefinitionActionLink("Letter Attributes")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter to continue coding?</div>
    <div id="link1" class="helpHidden">
        You can click on either the Save or Cancel button located at the bottom of the page to go back to the letter you were just coding. Save will save all your changes, while Cancel will return you to the letter without saving any changes you have made.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Letter Inbox?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Coded Comments page?</div>
    <div id="link3" class="helpHidden">
        You can go to the coded comments page from the expanded left navigation by clicking on Coded comments within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Concern and response page?</div>
    <div id="link4" class="helpHidden">
        You can go to the Concern and Response page from the expanded left navigation by clicking on Concerns/Responses within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Comment Period homepage?</div>
    <div id="link5" class="helpHidden">
        Method 1: You can go to the Comment Period homepage from the expanded left navigation by clicking on Comment Period Home of the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">View attached documents?</div>
    <div id="link6" class="helpHidden">
        You can click on the name of the document located in the Attachments table under the column Document to view the corresponding document.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Manage authors?</div>
    <div id="link7" class="helpHidden">
        You can Edit the author information by selecting the button labeled Edit next to the corresponding author. You can credit more authors to this letter by selecting the button Add Author under the Authors table. Click on Save to save your changes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Append text to this letter?</div>
    <div id="link8" class="helpHidden">
        You can copy and paste text from attached documents into the text box labeled Append text. Once you click save, the text that you have appended will be made available on the letter coding page for that letter. Click on Save to save your changes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Change the submitted date?</div>
    <div id="link9" class="helpHidden">
        You can modify the date the letter was submitted by either typing in the correct date or by clicking on the calendar icon to open and select the date on which this letter was submitted. Click on Save to save your changes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Change the delivery type?</div>
    <div id="link10" class="helpHidden">
        You can modify the delivery type of the letter by selecting the delivery type from the drop down menu labeled Delivery type. Click on Save to save your changes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">Designate a letter as being within or out of a Comment Period?</div>
    <div id="link11" class="helpHidden">
        You can designate a letter as being within or out of a Comment Period by selecting yes or no from the drop down menu labeled Within Comment Period. Select yes for within Comment Period, and no for outside of Comment Period. Click on Save to save your changes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link12')">Designate the letters common interest class?</div>
    <div id="link12" class="helpHidden">
        You can designate the common interest class of the letter by selecting values from the drop down labeled Common Interest Class. Click on Save to save your changes.
    </div>
    <br />
</asp:Content>