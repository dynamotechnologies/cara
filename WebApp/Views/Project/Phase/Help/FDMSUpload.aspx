﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("FDMS Letters (Upload FDMS Letters)")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter inbox?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the Comment Period letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the Comment Periods homepage from the expanded left navigation by clicking on Comment Period Home for the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can go to the Projects homepage from the expanded left navigation by clicking on the Project Home.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link4')">Obtain the FDMS file?</div>
    <div id="link4" class="helpHidden">
        In order to request an FDMS file from the Requlations.gov site, send an email to the FDMS coordinator for FS requesting a bulk export of comments.  Include the specific ‘Document ID’ that you are requesting comments for when submitting the request.  A .zip file will be provided which can then be imported directly into CARA.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Upload FDMS letters?</div>
    <div id="link5" class="helpHidden">
        You can click the button labeled Upload FDMS letters to upload the letters into the letter inbox for the corresponding comment period.	
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">View uploaded FDMS letters?</div>
    <div id="link6" class="helpHidden">
        You can view the FDMS letters that have been successfully uploaded in the letter inbox for the comment period you provided the letters for.
    </div>
    <br />
</asp:Content>