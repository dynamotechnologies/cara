﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Coding Structure Report")%><br />
    <%: Html.DefinitionActionLink("Comments Report")%><br />
    <%: Html.DefinitionActionLink("Demographic Report")%><br />
    <%: Html.DefinitionActionLink("Early Attention Report")%><br />
    <%: Html.DefinitionActionLink("Mailing List Report")%><br />
    <%: Html.DefinitionActionLink("Organization Type Report")%><br />
    <%: Html.DefinitionActionLink("Response Status Report")%><br />
    <%: Html.DefinitionActionLink("Response to Comment Report")%><br />
    <%: Html.DefinitionActionLink("Team Member Report")%><br />
    <%: Html.DefinitionActionLink("Untimely Comments Report")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Comment Period homepage?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the Comment Period homepage from the expanded left navigation by clicking on Comment Period Home of the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Project homepage?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the Projects homepage from the expanded left navigation by clicking on the Project Home.<br /><br />
        Method 2: You can go to the Project homepage from the breadcrumb navigation located directly under the global navigation by clicking on the name of the Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Custom reporting?</div>
    <div id="link3" class="helpHidden">
        You can select the Reports tab from the global navigation. You can then search and select the Project, as well as the Comment Period, for which you wish to generate custom reports.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link4')">Run a report?</div>
    <div id="link4" class="helpHidden">
        You can click on the button that is adjacent and to the right of the standard report names. The button title may be listed as Run if the Project is active (may display different data next time it is run) or View Archived Report if the Project has been archived (will no longer be changing). Even when a Comment Period is archived some reports may still say Run; these reports have not been frozen and may display updated information.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Customize the report?</div>
    <div id="link5" class="helpHidden">
        You can select the Reports tab from the global navigation, and then click on the Custom Reports tab. You can then customize the report you wish to generate.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Determine if the report being viewed is an archived report?</div>
    <div id="link6" class="helpHidden">
        You can determine if the report is from an active or archived Project by reading the title of the button located next to the name of the standard reports. If the button title of the report is Run, then the report is active. If the button title of the report is View Archived Report, then the report has been archived.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Export the report?</div>
    <div id="link7" class="helpHidden">
        You can export the report via the export button located at the top of the report once you have run the report. Supported formats include .PDF, .XLS, .DOC, .TXT and .CSV.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Print the report?</div>
    <div id="link8" class="helpHidden">
        You can print the report via the Print button located at the top of the report once you have run the report.
    </div>
    <br />
</asp:Content>