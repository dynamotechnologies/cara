﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Auto-markup")%><br />
    <%: Html.DefinitionActionLink("Economic")%><br />
    <%: Html.DefinitionActionLink("Request for Comment Extension")%><br />
    <%: Html.DefinitionActionLink("Threats")%><br />
    <%: Html.DefinitionActionLink("Request for Information")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Previous step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Back located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Approve Project creation page?</div>
    <div id="link2" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can click on the button labeled Accept to go to the current Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View the list of terms in an auto-markup category?</div>
    <div id="link5" class="helpHidden">
        You can click on the button labeled View located under the column List of terms for the corresponding auto-markup category of choice. 
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Select an auto-markup list of terms to use?</div>
    <div id="link6" class="helpHidden">
        You can select the check box located under the column Select for the corresponding auto-markup category of choice.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Use the auto-markup functionality?</div>
    <div id="link7" class="helpHidden">
        The auto-markup functionality will be activated automatically once the Comment Period inbox begins populating with letters. The auto-markup categories selected will determine which words are scanned for matches within the text of the letters. The words identified per letter can be viewed and validated from the letter coding page in the auto-markup section on the right panel.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Finish creating this Comment Period?</div>
    <div id="link8" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can accept this Project and go to its Project homepage, or cancel the creation of this Project. You can edit this Comment Period at a later date by modifying its settings from the Project homepage, Comment Period homepage, as well as by visiting this Project creation wizard.
    </div>
    <br />
</asp:Content>