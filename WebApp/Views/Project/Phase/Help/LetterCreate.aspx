﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter inbox?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the Comment Period letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>
    <div id="link2" class="helpHidden">
        You can go to the Comment Period homepage from the expanded left navigation by clicking on the Comment Period Home.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can go to the Project homepage from the expanded left navigation by clicking on the name of the Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Upload email letters page?</div>
    <div id="link4" class="helpHidden">
        The CARA users with the authorized roles can click on the Upload email letters link inside the expanded letter inbox section within the expanded Comment Period in the left navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Upload FDMS import page?</div>
    <div id="link5" class="helpHidden">
        The CARA users with the authorized roles can click on the Upload FDMS letters link inside the expanded letter inbox section within the expanded Comment Period in the left navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">How do I create a new letter?</div>
    <div id="link6" class="helpHidden">
        You can enter the details of the letter within any of the form fields. Types of form fields may include calendar pop outs, drop downs, check boxes, radio buttons, or text fields. You must click on the Submit buttons in order to successfully save the letter.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Add or remove attachments?</div>
    <div id="link7" class="helpHidden">
        You can add or remove the letter associated attachments from the attachments table. You can click on the button labeled Choose to upload an attachment. The total size of all attachments combined cannot be greater than 10 Megabytes (MB). You can remove an attachment by clicking on the ‘ - ‘ icon. You can add more attachments by clicking on the ‘ + ‘ icon.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">What is required for the successful submission of this letter?</div>
    <div id="link10" class="helpHidden">
        In order to successfully submit the letter, the Last name of the author is required. Alternately, if the letter is being submitted anonymously, or the name of the author is not known, then the check box for anonymous submission must be checked.<br /><br />
        If the preferred contact is selected as Email, then an email address is required for submission.<br /><br />
        If the preferred contact is selected as Mail, then a valid address is required for submission.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Cancel the new letter creation?</div>
    <div id="link8" class="helpHidden">
        You can cancel the creation of the new letter via the button labeled Cancel at the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">View the letter that was submitted?</div>
    <div id="link9" class="helpHidden">
        You can view the letter that was submitted within the letter inbox of the Comment Period for which the letter was created.
    </div>
    <br />
</asp:Content>