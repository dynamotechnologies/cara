﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("Coding Structure")%><br />
    <%: Html.DefinitionActionLink("Auto-markup")%><br />
    <%: Html.DefinitionActionLink("Analysis Type")%><br />
    <%: Html.DefinitionActionLink("Activities")%><br />
    <%: Html.DefinitionActionLink("Lead Management Unit")%><br />
    <%: Html.DefinitionActionLink("Project Setup Wizard")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Project homepage?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Accept and go to this Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link3')">Modify any of the Project setup wizard steps?</div>
    <div id="link3" class="helpHidden">
        You can click the title of any of the steps in the wizard to go back and modify any settings. Once you have finished the modifications, you can quickly return to the Project creation acceptance page by clicking on the button labeled Finish setup located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">View a print ready version of all selected Project wizard Set up parameters?</div>
    <div id="link7" class="helpHidden">
        You can generate a Printable summary of the Project wizards Set up parameters by clicking on the button Printable Summary.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Approve the Project details and create the Comment Period?</div>
    <div id="link4" class="helpHidden">
        You can click on the button labeled Accept to approve the Project set up details and create this Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Approve and create another Comment Period for this Project?</div>
    <div id="link5" class="helpHidden">
        You can click on the button labeled Accept and go to the Projects homepage. From the left navigation you can click on Add Comment Period to add another Comment Period to this Project via the Project set up wizard.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Cancel this Comment Period creation?</div>
    <div id="link6" class="helpHidden">
        You can click on the button labeled Cancel to discard any changes or Project setup action.
    </div>
    <br />
</asp:Content>