﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Custom Code")%><br />
    <%: Html.DefinitionActionLink("Issue or Action Codes")%><br />
    <%: Html.DefinitionActionLink("Resource or Rationale Codes")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%><br />
    <%: Html.DefinitionActionLink("Location / Location Code")%><br />
    <%: Html.DefinitionActionLink("Document (Code)")%><br />
    <%: Html.DefinitionActionLink("Other (Code)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Action Codes)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Resources Codes)")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">A different Comment Period homepage for the same Project?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to a different CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest. The option to select and go to another Comment Period will be available if another Comment Period has already been set up.<br /><br />
        Method 2: You can click on the Project name located directly below the global navigation. From the Project homepage you can go to the homepage of another Comment Period by selecting the name of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">A different Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the Home tab in the top navigation to go to your My Projects table. From this table you can then select the name of the Project of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Letter inbox?</div>
    <div id="link4" class="helpHidden">
        You can go to the letter inbox for a Comment Period by clicking on its name from the expanded left navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View the coding structure?</div>
    <div id="link5" class="helpHidden">
        You may open or collapse additional levels of the coding structure by clicking to open or collapse the tree hierarchy designated by either a + or – icon on the left of the codes within the coding structure. You may also click on Expand All, or Collapse All to open or close the entire coding structure respectively.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">View a Preview version of the Selected codes?</div>
    <div id="link8" class="helpHidden">
        You may click on the button Preview to generate a preview of the codes you have selected to use for this Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Select a code to add or remove from the coding structure?</div>
    <div id="link6" class="helpHidden">
        If you wish to add a code to the coding structure for this Comment Period you can click to check the box next to the corresponding code of choice.<br /><br />
        If you wish to remove a code from the coding structure for this Comment Period you can click to uncheck the box next to the corresponding code of choice.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Create and add a custom code to the coding structure?</div>
    <div id="link7" class="helpHidden">
        <table>
            <tr><td colspan="2">You can create a Custom Code under or at the same level at code levels 2-4 for Issue or Action Codes, Resource or Rationale codes, and Early Attention codes. You may create child codes for the Location, Documentation, or the Other category by selecting the corresponding category and clicking on the Add Custom Code button.</td></tr>
            <tr><td colspan="2">To create a Custom Code under a code (Child level) click on the name of the Code and click on the button Add Custom Code. In the dialogue box that opens you may enter a description of the Code you are trying to create, followed by pressing Save. The type and number of the custom code will be auto generated.</td></tr>
            <tr><td colspan="2">You may also create a Code at the same level (Sibling level) of a code. You can click on the name of the code from which you want to create a same level code and click on the Add Custom Code button. In the Dialogue box that opens you may select the Type of code you wish to create once you have selected Sibling, provide a description and click save. The number of the custom code will be auto generated.</td></tr>
            <tr><td colspan="2">In the event that you wish to create a custom code using only the keyboard keys, you may do so using the following keys:</td></tr>
            <tr><td valign="top">Step 1:</td><td>Ctrl-Shift-A: Activate the keyboard access on code selection.</td></tr>
            <tr><td valign="top">Step 2:</td><td>Ctrl-Shift-L: Go to the next, Lower visible code in the tree view.</td></tr>
            <tr><td>&nbsp;</td><td>Ctrl-Shift-U: Go to the previous, higher Up visible code in the tree view.</td></tr>
            <tr><td valign="top">Step 3:</td><td>Ctrl-Shift-S: Selects the code where you wish to create a new custom code and begins the process of adding the new custom code.</td></tr>
            <tr><td>&nbsp;</td><td>Ctrl-Shift-E: Opens the Edit custom code window (if the Edit Custom Code” button is available). This is available once a custom code has been created.</td></tr>
            <tr><td valign="top">Step 4:</td><td>Provide the description of the code, and press Save or Cancel.</td></tr>
            <tr><td valign="top">Note:</td><td>ESC key: Will cancel the process of selecting a code where you want to insert a custom code.</td></tr>    
        </table>
    </div>
    <br />
</asp:Content>