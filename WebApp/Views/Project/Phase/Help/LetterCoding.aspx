﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "CodingStrategy").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("CodingVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("CodingVideo"), Name = Constants.CODING })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Append")%><br />
    <%: Html.DefinitionActionLink("Letter Attributes")%><br />
    <%: Html.DefinitionActionLink("Letter Coding Toolbar")%><br />
    <%: Html.DefinitionActionLink("Right panel (Letter coding page)")%><br />
    <%: Html.DefinitionActionLink("Annotations")%><br />
    <%: Html.DefinitionActionLink("Sample Statement")%><br />
    <%: Html.DefinitionActionLink("Auto-markup")%><br />
    <%: Html.DefinitionActionLink("Master Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Full Screen")%><br />
    <%: Html.DefinitionActionLink("Link Text")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter Inbox?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Coded Comments page?</div>
    <div id="link2" class="helpHidden">
        You can go to the coded comments page from the expanded left navigation by clicking on Coded comments within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Concern and response page?</div>
    <div id="link3" class="helpHidden">
        You can go to the concern and response listing page from the expanded left navigation by clicking on Concerns/Responses within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Comment Period homepage?</div>
    <div id="link4" class="helpHidden">
        Method 1: You can go to the Comment Period homepage from the expanded left navigation by clicking on Comment Period Home of the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View more letter attributes?</div>
    <div id="link5" class="helpHidden">
        You can click on the button labeled More located on the right of the letter attributes table to view more letter attributes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Edit letter attributes?</div>
    <div id="link6" class="helpHidden">
        You can click on the button labeled Edit located on the right of the letter attributes table to edit the letter attributes. From here you can view and download the attachments, view and add authors, append text to the letter, change the submission date, change the designation of whether this letter was received within the Comment Period, change the CIC class, as well as change the delivery type of the letter.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">View the complete author list?</div>
    <div id="link7" class="helpHidden">
        You can click on the link labeled Author list located on the right of the author name under the column Author in the letter attributes table. The Author list link will be shown only in instances in which there are multiple authors of the letter.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Code comments?</div>
    <div id="link8" class="helpHidden">
        You must first highlight the text that you wish to code. You can expand the coding component by clicking on the title Coding in the panel on the right of the letter text. You can then select the codes you want to apply to the highlighted text, and save by clicking on the button labeled Apply Codes located under the coding structure.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link26')">What Hotkeys are available for me to use?</div>
    <div id="link26" class="helpHidden">
        To begin the coding process using the Hotkeys you must first activate the textbox.<br /><br />
        Step 1: You may click on the letter text area. Alternatively, you may press 'CONTROL (Ctrl) + SHIFT (Shift) + L keys' together to activate the letter text area on the letter coding page.<br /><br />
        Once in the Letter coding textbox, you may move around to reach the desired comment location.<br /><br />
        Step 2: You may move around the Letter coding textbox by utilizing the ‘CONTROL (Ctrl) + -> (right arrow) or CONTROL (Ctrl) + <- (left arrow) keys’ to tab over the words in the letter text. A cursor position will be presented on the comment to indicate the position of the cursor.<br /><br />
        You may now move to the desired Starting location of the comment you wish to code.<br /><br />
        Step 3: Press the 'S’ key to select the Starting point of the comment. The comment will be highlighted from this point forward. The starting point will be marked by an orange-color tag [pending comment start].<br /><br />
        Note: You may now highlight up to the desired end location of the comment by utilizing the ‘CONTROL (Ctrl) + -> or CONTROL (Ctrl) + <- keys’ for movement within the letter coding textbox.<br /><br />
        You may now move to the desired Ending location of the comment you wish to code.<br /><br />
        Step 4: Press the 'E’ key to designate that location as the Ending point of the comment. The system will perform validation of the highlighted text and highlight it in the letter text.<br /><br />
        Note: You may press the 'Escape (Esc) key’ if you wish to stop highlighting the text and cancel this instance of comment creation.<br /><br />
        You may now code the highlighted comment.<br /><br />
        Step 5: Press the 'A’ key if you are ready to code the text. It will make the Coding section in the right panel Active so that you may apply codes to the highlighted comment text. Select the codes you wish to apply and then click on Apply codes to create the comment.<br /><br />
        Note: In the event you wish to use the link text functionality instead of coding the comment, you may press ‘L’ key. This will allow you to link the highlighted text to an existing comment.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Search for codes?</div>
    <div id="link9" class="helpHidden">
        Within the Coding panel on the right side of the page, you can type in the code number or keywords in the text field provided and click on the Find button to search for the codes you need. Partial search for the codes and keywords are also supported (e.g., Fore* for forest). You may also click on Advanced Search to search by keyword, code number, or by a range of code numbers.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Modify the coding structure?</div>
    <div id="link10" class="helpHidden">
        Authorized CARA users can modify the coding structure for the Comment Period from either the Comment Period homepage or the Project setup wizard. Modification of the coding structure consists of adding and removing standard or custom codes from the coding structure.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">Manage comments?</div>
    <div id="link11" class="helpHidden">
        You can manage the comments that have been created within the letter from the right panel section labeled Comment. Within this section you can remove codes associated with a comment, delete the comment, move from one comment to another, write a response to this comment, designate the comment as a sample statement, designate the comment as not requiring any further response as well as annotate the comment.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link12')">Manage auto-markup?</div>
    <div id="link12" class="helpHidden">
        You can manage the auto-markup terms that have been found within the letter from the right panel section labeled Auto-markup. Within this section you can view and validate whether the auto-markup terms found are applicable to the letter.<br /><br />
        You may select additional auto-markup list of terms by clickking on the button labeled Select Auto-markups. The selected list of terms will only be active while youe session is in progress, and will be deactivated once you log off.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link13')">Toggle between coded comments and auto-markup terms?</div>
    <div id="link13" class="helpHidden">
        You can toggle between coded comments and auto-markup terms found in the letter from the drop down menu in the letter coding tool bar. The default setting displays Show coding and Auto-markup, but you can modify it to display only the coded comments, only the auto-markup terms, or display neither the coded comments nor auto-markup terms in the letter. Once a selection has been made between coded comments and auto-markup terms, you can use the Next arrow button to move to the next applicable selection, as well as the Previous arrow button to move to the previous applicable selection.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link14')">Increase the font size?</div>
    <div id="link14" class="helpHidden">
        You can increase or decrease the font size of the letter from the drop down found in the letter coding tool bar. The default setting displays a font size of 10, but you can modify it to display 12, or 14 pt sizes.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link15')">Work in full screen mode?</div>
    <div id="link15" class="helpHidden">
        You can click on the button depicting Full screen in the letter coding tool bar to activate the full screen functionality. When in full screen mode, excess information is removed from view so the letter coding toolbar, letter body text, and the right panel are given more focus.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link16')">Link two pieces of text to code as one comment?</div>
    <div id="link16" class="helpHidden">
        This functionality is used to link two pieces of text as one comment in the event that they are scattered throughout the letter. You can click on the button labeled Link text in the letter coding tool bar to link two pieces of text together as one comment. First you must highlight and code a comment. Then you can highlight another selection of text and press the Link button to associate two comments as one. The codes applied to the original comment are also imposed on the text that has been linked together as one.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link17')">Compare a letter to its master form?</div>
    <div id="link17" class="helpHidden">
        You can click on the button labeled Compare to Master in the letter coding tool bar to activate the compare to master functionality. This functionality allows you to view a Form letter alongside its corresponding Master form letter.<br /><br />
        From within the Compare to Master page you can click on the drop down menu labeled Re-designate and select the desired option to re-designate this letter as a Form letter, Form plus letter, or Master Form letter within this form set. You may also send this letter to another form set if the Master Form for the other form set is selected from the left drop down labeled Compare with Master Form from Set.<br /><br />
        Re-designating this letter will initiate the Form detection process within the form set which will compare the newly designated letter with the other letters within the same form set.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link18')">View associated attachments?</div>
    <div id="link18" class="helpHidden">
        You can view the associated attachments by clicking the name of the document listed under the column Attachments located under the letter attributes table. You may also append the contents of the attachments to the letter if needed and if the attachment is in .PDF or .TXT formats.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link19')">Append contents of the attachments to the letter?</div>
    <div id="link19" class="helpHidden">
        Method 1: You may append the contents of a letter by clicking on Add Content To Letter from the attachments table. You will be provided with the option to preview the additions to the letter. If acceptable, click on the Add To Letter button to append the content of the attachment to the letter.<br /><br />
        You may only use this feature for .PDF and .TXT documents. Once the content has been appended, it cannot be removed.<br /><br />
        Method 2: You can click on Edit in the letter attributes table to open the edit screen. From the edit screen you can open an attachment by clicking on its name, highlight – copy – and paste the desired text into the section labeled Append. Click on save to append the text to the letter. Only content received with the original letter can be appended. The original text of the letter cannot be altered.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link20')">Annotate a comment?</div>
    <div id="link20" class="helpHidden">
        You can manage the comments that have been created within the letter from the right panel section labeled Manage comments. Within this section you can annotate the comment by typing in the text box labeled Annotations. *Warning – Annotations will be saved as part of the Project record when the Comment Period is archived.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link21')">Flag a comment as a sample statement?</div>
    <div id="link21" class="helpHidden">
        You can manage the comments that have been created within the letter from the right panel section labeled Comment. Within this section you can designate the comment as a sample statement by clicking to check the check box labeled Sample statement.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link22')">Flag a comment as no further response required?</div>
    <div id="link22" class="helpHidden">
        You can manage the comments that have been created within the letter from the right panel section labeled Comment. Within this section you can designate the comment as not requiring any further response. You must choose a reason from the drop down labeled “No Further Response Required” in order to complete the process for designating this comment as not requiring a response.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link23')">Save the letter?</div>
    <div id="link23" class="helpHidden">
        You can click on either of the buttons located under the column title Save and go to at the bottom of the page. The button labeled Letter Inbox under the save column will save the letter and take you back to the letter inbox. The button labeled Next Letter under the save column will save the letter and take you to the next letter from the letter inbox.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link24')">Mark the letter as complete?</div>
    <div id="link24" class="helpHidden">
        You can click on either of the buttons located under the column title Complete and go to at the bottom of the page. The button labeled Letter Inbox under the complete column will mark this letter as complete and take you back to the letter inbox. The button labeled Next Letter under the complete column will complete this letter and take you to the next letter from the letter inbox. Once a completed letter is modified by activities such as coding another comment, it reverts back to the In Progress state.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link25')">View the next letter from the letter inbox?</div>
    <div id="link25" class="helpHidden">
        Method 1: The button labeled Go to next letter, under the Save column, will save the letter and take you to the next letter from the letter inbox.<br /><br />
        Method 2: The button labeled Go to next letter, under the Complete column, will complete this letter and take you to the next letter from the letter inbox.
    </div>
</asp:Content>