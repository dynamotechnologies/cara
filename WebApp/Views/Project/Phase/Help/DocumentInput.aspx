﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">A different Comment Period homepage for the same Project?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to a different CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest. The option to select and go to another Comment Period will be available if another Comment Period has already been set up.<br /><br />
        Method 2: You can click on the Project name located directly below the global navigation. From the Project homepage you can go to the homepage of another Comment Period by selecting the name of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">A different Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the Home tab in the top navigation to go to your My Projects table. From this table you can then select the name of the Project of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Letter inbox?</div>
    <div id="link4" class="helpHidden">
        You can go to the letter inbox for a Comment Period by clicking on its name from the expanded left navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Manage document association with this Comment Pseriod?</div>
    <div id="link5" class="helpHidden">
        You can associate none, one or many documents to this Comment Period by selecting the check box under the column Select, and clicking on the button Save. If you wish you disassociate the document (s), simply click on the button labeled Remove.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Decide which documents to associate?</div>
    <div id="link6" class="helpHidden">
        The document(s) that should be associated to a Comment Period are those documents that the Forest Service intends to release to the public in an effort to solicit comments.  These documents typically consist of Scoping documents, Environmental Assessments, Environment Impact Statements, and any supporting materials.
    </div>
    <br />
</asp:Content>