﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Reading Room Management")%><br />
    <%: Html.DefinitionActionLink("Public Reading Room")%><br />
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Plus")%><br />
    <%: Html.DefinitionActionLink("Master Form Letter")%><br />
    <%: Html.DefinitionActionLink("Duplicates")%><br />
    <%: Html.DefinitionActionLink("Unique Letter")%><br />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('prrlink')">Public Reading Room website?</div>
    <div id="prrlink" class="helpHidden">
        Method 1: You can go to the Comment Period Home page from the expanded left navigation by clicking on Comment Period Home.  On the Comment Period page, select the Public Reading Room link.<br />
		Method 2: You can click on the associated comment period link at the top of the page to the left of “Reading Room Management”.  On the Comment Period page, select the Public Reading Room link.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('viewletterlink')">View letters?</div>
    <div id="viewletterlink" class="helpHidden">
		The initial view lists all the letters associated with the comment period.  If a letter has been published to the Public Reading Room webpage, the checkbox in the Published column will be checked. No checkmark indicates that the letter is unpublished. In order to view a letter, select the number in the Letter Number column.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('searchlink')">Search for specific letters?</div>
    <div id="searchlink" class="helpHidden">
		Within the first shaded area on this page, there are search fields that may be used in order to locate a specific letter or types of letters.  Utilize these fields to narrow your search.  Selecting the radio button for either “Only Show Published Letters” or “Only Show Unpublished Letters” will apply that filter to your results.  Click the “Filter” button to view results.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('publetterslink')">Publish letters?</div>
    <div id="publetterslink" class="helpHidden">
        Letters that have not been published to the Public Reading Room will be shown in the list with the checkbox in the Publish column unchecked.  Simply check (or use the Check All button) this checkbox for any letters you was to publish, then select the Update button in the lower left.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('unpubletterslink')">Unpublish letters?</div>
    <div id="unpubletterslink" class="helpHidden">
        Simply uncheck the checkbox in the Publish column and select an Unpublish Reason from the dropdown.  This reason is mandatory for any letters you wish to unpublish from the Public Reading Room.
    </div>
    <br />		
</asp:Content>