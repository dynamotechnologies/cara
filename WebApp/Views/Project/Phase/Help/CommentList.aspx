﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment ID")%><br />
    <%: Html.DefinitionActionLink("Sample Statement")%><br />
    <%: Html.DefinitionActionLink("Association")%><br />
    <%: Html.DefinitionActionLink("Issue or Action Codes")%><br />
    <%: Html.DefinitionActionLink("Resource or Rationale Code")%><br />
    <%: Html.DefinitionActionLink("Location / Location Code")%><br />
    <%: Html.DefinitionActionLink("Document (Code)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Action Codes)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Resources Codes)")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%><br />
    <%: Html.DefinitionActionLink("Other (Code)")%><br />
    <%: Html.DefinitionActionLink("Custom Code")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Comment Period letter inbox?</div>
    <div id="link1" class="helpHidden">
        You can go to the letter inbox for a Comment Period by clicking on its name from the expanded left navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the corresponding CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest.<br /><br />
        Method 2: You can go to the corresponding CARA Comment Period homepage from the breadcrumb navigation by clicking on the corresponding Comment Period name of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Comment details page?</div>
    <div id="link3" class="helpHidden">
        You can click on the number listed under the column title Comment ID to go to the comment details page.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View / Manage the comment within the context of the letter?</div>
    <div id="link5" class="helpHidden">
        You can click on the Comment ID to go to the comment within the context of the letter. The Comment ID consists of two parts: The Letter ID + the Comment ID. 
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Filter comments?</div>
    <div id="link6" class="helpHidden">
        You can filter the coded comments contained within the Comment Period’s coded comments table by utilizing the options directly above the table.  You may filter by Codes, Code types, Keywords, Designation as sample statements, Designation as no further response required, Concern or Response association, or remove all filters by checking Show all.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Sort letters?</div>
    <div id="link7" class="helpHidden">
        You can sort the letters contained within the Comment Period’s coded comments table by clicking to sort via the column titles.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Personalize table settings?</div>
    <div id="link8" class="helpHidden">
        You can select the desired number of rows displayed per table on the My Profile tab in the global navigation. This personal setting will be applied to many but not all of the data display tables within CARA.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Create a concern and response?</div>
    <div id="link9" class="helpHidden">
        You can create a concern and response by clicking a Create concern/response button located at the top and bottom of the coded comments table. You can link selected comments to a concern and/or response by selecting them prior to clicking on the Create concern/response button.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Delete a comment?</div>
    <div id="link10" class="helpHidden">
        You may delete a comment by clicking on its corresponding Delete button from the Action column.
    </div>
    <br />
</asp:Content>