﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">CARA Admin page?</div>
    <div id="link2" class="helpHidden">
        You can go to the CARA Admin page by selecting the Admin tab in the top navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link3')">Search for a user?</div>
    <div id="link3" class="helpHidden">
        You can search for a user by entering their first or last name and selecting the button labeled Search Users. This features searches for Users in PALS that you may wish to designate a role in CARA.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Update the role of a user?</div>
    <div id="link4" class="helpHidden">
        You can modify the role of a user by selecting the Super user or Coordinator check boxes located to the right of the corresponding user. Click on Add to CARA to save your changes.
    </div>
    <br />
</asp:Content>