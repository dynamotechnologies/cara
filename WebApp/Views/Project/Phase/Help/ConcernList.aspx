﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ConcernResponseStrategies").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ConcernResponseVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ConcernResponseVideo"), Name = Constants.CONCERN_RESPONSE })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Concern/Response - In Progress")%><br />
    <%: Html.DefinitionActionLink("Ready for Response")%><br />
    <%: Html.DefinitionActionLink("Response in Progress")%><br />
    <%: Html.DefinitionActionLink("Concern/Response - Response Complete")%><br />
    <%: Html.DefinitionActionLink("Concern and Response Status")%><br />
    <%: Html.DefinitionActionLink("Concern/Response ID")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Coded Comments page?</div>
    <div id="link1" class="helpHidden">
        You can go to the coded comments page from the expanded left navigation by clicking on Coded comments within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the Comment Periods homepage from the expanded left navigation by clicking on Comment Period Home for the Comment Period of interest.<br /><br />
        Method 2: You can click on the title of the Comment Period in the breadcrumb navigation located directly below the global navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Letter inbox?</div>
    <div id="link3" class="helpHidden">
        You can go to the Comment Period letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Reports page?</div>
    <div id="link4" class="helpHidden">
        Method 1: You can go to the Comment Periods report page from the expanded left navigation by clicking on Reports within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can click on the reports tab in the global navigation to access standard and custom reports.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Search for a concern(s) or response(s)?</div>
    <div id="link5" class="helpHidden">
        You can search for a particular concern and response by typing in related words of interest in the keywords field. You can also filter concerns and responses by status or by entering a “last updated” date range.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Create a concern or response?</div>
    <div id="link6" class="helpHidden">
        You can create a new concern and response by clicking on the Create concern response button located at the top or bottom of the concern and response listing page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Edit the concern and response?</div>
    <div id="link7" class="helpHidden">
        You can edit the concern and response by selecting its number listed under the column title Concern / response ID. Once in the concern and response writing page you can modify the concern and response, manage the associated comments, as well as change the status of the concern and response.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link12')">Edit the sequence in which the concern and responses appear on reports and this table?</div>
    <div id="link12" class="helpHidden">
        You can click on the button labeled Edit Sequence to begin the process of selecting the order in which these concern and responses will appear on this table as well as any associated reports. Click the arrows to move the concern and responses up or down. You may also input a number in the text box provided under the column Sequence to designate the placement of the concern and response. Click on the button labeled Finish to save and complete the sequence modification of concern and responses.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Manage the status of a concern and response?</div>
    <div id="link8" class="helpHidden">
        You can open the concern and response by selecting its number listed under the column title Concern / Response ID. From within the concern and response writing page you can modify the status of the response by utilizing the buttons located at the bottom. Status options include designating a comment as Ready for response, as well as Response complete. You can designate a Concern and Response as Response complete by clicking on the button labeled Response Complete.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Manage associated comments within the concern and response?</div>
    <div id="link9" class="helpHidden">
        You can open the concern and response by selecting its number listed under the column titled Concern / Response ID. You can remove associated comments by clicking on the remove icon listed under the column titled Remove comment for the corresponding comment of interest. You can add more comments to the concern and response by clicking on the Add more comments button located directly below the comment listing table on the concern and response writing page.  Selecting the Add more comments button will take you to the Coded Comments page where you may add more comments to associate with this concern and response.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Flag a comment as a sample statement?</div>
    <div id="link10" class="helpHidden">
        You can open the concern and response by selecting its number listed under the column titled Concern/Response ID. You can flag a comment as a sample statement by selecting the checkbox located under the column titled Sample statement for the corresponding statement that you wish to designate as a sample statement.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">Delete a concern statement/response?</div>
    <div id="link11" class="helpHidden">
        You can delete a concern statement and response by clicking on the delete within the corresponding Concern and Response writing page for that concern statement and response.
    </div>
    <br />
</asp:Content>