﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Letter ID")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Plus")%><br />
    <%: Html.DefinitionActionLink("Master Form Letter")%><br />
    <%: Html.DefinitionActionLink("Duplicates")%><br />
    <%: Html.DefinitionActionLink("Unique Letter")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Comment Period homepage?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the desired CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest.<br /><br />
        Method 2: You can go to the desired CARA Comment Period homepage from the breadcrumb navigation by clicking on the corresponding Comment Period name of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Project homepage?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the desired CARA Project homepage from the expanded left navigation by selecting the corresponding Project name of interest.<br /><br />
        Method 2: You can go to the desired CARA Project homepage from the breadcrumb navigation by clicking on the corresponding Project name of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Create concern and response page?</div>
    <div id="link3" class="helpHidden">
        You can go to the corresponding Comment Period’s concern and response page from the expanded left navigation by clicking on Concerns/Responses within the corresponding Comment Period of interest.
    </div>
    <br />		
    <div class="helpLink" onclick="toggleArea('link4')">Coded Comments page?</div>
    <div id="link4" class="helpHidden">
        You can go to the coded comments page from the expanded left navigation by clicking on Coded comments within the corresponding Comment Period of interest.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Open a letter for coding?</div>
    <div id="link5" class="helpHidden">
        You can click and view a letter within the Comment Period letter inbox by clicking on the number listed under the column Letter ID within the table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Filter letters?</div>
    <div id="link6" class="helpHidden">
        You can filter the letters contained within the Comment Period letter inbox table by utilizing the options directly above the table.  You may filter by Letter Type, Status, Date Submitted, Keyword or Early Attention. Users may also choose to exclude forms and duplicates from the results. After filtering, click “show all letters” to remove selected filters.
    </div>
    <br />    
    <div class="helpLink" onclick="toggleArea('link7')">Sort letters?</div>
    <div id="link7" class="helpHidden">
        You can sort the letters contained within the Comment Period letter inbox table by clicking the column titles.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Enter a new letter?</div>
    <div id="link8" class="helpHidden">
        Method 1: CARA users with the authorized roles can click on the Enter new letter link located directly below the letter inbox table to create a new letter.<br /><br />
        Method 2: CARA users with the authorized roles can click to create a new letter by selecting Enter new letter under the expanded Letters section within the expanded Comment Period in the left navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Upload letters via email or FDMS?</div>
    <div id="link9" class="helpHidden">
        Method 1: CARA users with the authorized roles can click on the Upload emails or Upload FDMS buttons located below the letter inbox table to upload the letters.<br /><br />
        Method 2: CARA users with the authorized roles can click on the Upload Email Letters or the Upload FDMS Letters links under the expanded Letters section within the expanded Comment Period in the left navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Personalize letter inbox table settings?</div>
    <div id="link10" class="helpHidden">
        You can select the desired number of rows displayed per table on the My Profile tab in the global navigation. This personal setting will be applied to many but not all of the data display tables within CARA.
    </div>
    <br />
</asp:Content>