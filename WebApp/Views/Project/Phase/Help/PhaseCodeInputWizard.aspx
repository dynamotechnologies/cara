﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Custom Code")%><br />
    <%: Html.DefinitionActionLink("Issue or Action Codes")%><br />
    <%: Html.DefinitionActionLink("Resource or Rationale Codes")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%><br />
    <%: Html.DefinitionActionLink("Location / Location Code")%><br />
    <%: Html.DefinitionActionLink("Document (Code)")%><br />
    <%: Html.DefinitionActionLink("Other (Code)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Action Codes)")%><br />
    <%: Html.DefinitionActionLink("Appeals (Resources Codes)")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Previous step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Back located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Next step of the Project setup wizard?</div>
    <div id="link2" class="helpHidden">
        You can click on the button labeled Next located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Approve Project creation page?</div>
    <div id="link3" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Project homepage?</div>
    <div id="link4" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can click on the button labeled Accept to go to the current Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">View the coding structure?</div>
    <div id="link6" class="helpHidden">
        You may open or collapse additional levels of the coding structure by clicking to open or collapse the tree hierarchy designated by either a + or – icon on the left of the codes within the coding structure. You may also click on Expand All, or Collapse All to open or close the entire coding structure respectively.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">View a Preview version of the Selected codes?</div>
    <div id="link10" class="helpHidden">
        You may click on the button Preview to generate a preview of the codes you have selected to use for this Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Select a code to add or remove from the coding structure?</div>
    <div id="link7" class="helpHidden">
        If you wish to add a code to your coding structure you can click to check the box next to the corresponding code of choice.<br /><br />
        If you wish to remove a code from your coding structure you can click to uncheck the box next to the corresponding code of choice.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Create and add a custom code to the coding structure? </div>
    <div id="link8" class="helpHidden">
        <table>
            <tr><td colspan="2">You can create a Custom Code under or at the same level at code levels 2-4 for Issue or Action Codes, Resource or Rationale codes, and Early Attention codes. You may create child codes for the Location, Documentation, or the Other category by selecting the corresponding category and clicking on the Add Custom Code button.</td></tr>
            <tr><td colspan="2">To create a Custom Code under a code (Child level) click on the name of the Code and click on the button Add Custom Code. In the dialogue box that opens you may enter a description of the Code you are trying to create, followed by pressing Save. The type and number of the custom code will be auto generated.</td></tr>
            <tr><td colspan="2">You may also create a Code at the same level (Sibling level) of a code. You can click on the name of the code from which you want to create a same level code and click on the Add Custom Code button. In the Dialogue box that opens you may select the Type of code you wish to create once you have selected Sibling, provide a description and click save. The number of the custom code will be auto generated.</td></tr>
            <tr><td colspan="2">In the event that you wish to create a custom code using only the keyboard keys, you may do so using the following keys:</td></tr>
            <tr><td valign="top">Step 1:</td><td>Ctrl-Shift-A: Activate the keyboard access on code selection.</td></tr>
            <tr><td valign="top">Step 2:</td><td>Ctrl-Shift-L: Go to the next, Lower visible code in the tree view.</td></tr>
            <tr><td>&nbsp;</td><td>Ctrl-Shift-U: Go to the previous, higher Up visible code in the tree view.</td></tr>
            <tr><td valign="top">Step 3:</td><td>Ctrl-Shift-S: Selects the code where you wish to create a new custom code and begins the process of adding the new custom code.</td></tr>
            <tr><td>&nbsp;</td><td>Ctrl-Shift-E: Opens the Edit custom code window (if the Edit Custom Code” button is available). This is available once a custom code has been created.</td></tr>
            <tr><td valign="top">Step 4:</td><td>Provide the description of the code, and press Save or Cancel.</td></tr>
            <tr><td valign="top">Note:</td><td>ESC key: Will cancel the process of selecting a code where you want to insert a custom code.</td></tr>
        </table>
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Finish creating this comment period?</div>
    <div id="link9" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can accept this Project and go to its Project homepage, or cancel the creation of this Project. You can edit this Comment Period at a later date by modifying its settings from the Project homepage, Comment Period homepage, as well as by visiting this Project creation wizard. Selecting Finish at this point will apply default settings for the remaining steps. You may view and modify these default settings in the approve Project creation page.
    </div>
    <br />
</asp:Content>