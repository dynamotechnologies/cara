﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.Title %></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("CustomReportVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("CustomReportVideo"), Name = "CustomReport" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Coding Structure Report")%><br />
    <%: Html.DefinitionActionLink("Comments Report")%><br />
    <%: Html.DefinitionActionLink("Demographic Report")%><br />
    <%: Html.DefinitionActionLink("Early Attention Report")%><br />
    <%: Html.DefinitionActionLink("Mailing List Report")%><br />
    <%: Html.DefinitionActionLink("Organization Type Report")%><br />
    <%: Html.DefinitionActionLink("Response Status Report")%><br />
    <%: Html.DefinitionActionLink("Response to Comment Report")%><br />
    <%: Html.DefinitionActionLink("Team Member Report")%><br />
    <%: Html.DefinitionActionLink("Untimely Comments Report")%><br />
    <%: Html.DefinitionActionLink("Custom Reports")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link2')">Select a standard report to run?</div>
    <div id="link2" class="helpHidden">
        You can select the Project and Comment Period for which to run a standard report in the tab entitled Standard reports on the Reports page. Once the Project and the Comment Period have been selected you can click on View Standard Reports to see the full list of standard reports that are available.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Customize a report to run?</div>
    <div id="link3" class="helpHidden">
        You can begin the customization of the reports by clicking on the Custom reports tab within the reports. Once in the custom report tab you will have the capability to customize the reports in multiple ways. For more information view the CARA custom reporting video guide, as well as the CARA users guide.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Modify the Parameters or Views?</div>
    <div id="link8" class="helpHidden">
        You may enter any criteria in the Submitter, Letter/Comment, and Project information sections to tell the custom report generator where to look for data in the CARA database when running a report.<br /><br />
        You may also check or uncheck the Include data from Submitter, Letter/Comment, or Project checkboxes to include the respective types of data in the report output.<br /><br />
        Press Clear to clear all criteria and start over.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Save custom reports?</div>
    <div id="link4" class="helpHidden">
        You can save the custom report via the Save to MyReports button located on the Custom Report Results page, which is displayed once you have run a report.<br /><br />
        You may save the report as Shared so that it is available for everyone to view and use, or Private so that only you can have access to it.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">View my Saved custom reports or Shared reports?</div>
    <div id="link5" class="helpHidden">
        You can view your saved custom reports in the MyReports table located on the reports page in my MyReports tab. You can run the reports from the MyReports table at any time to view the results again.<br /><br />
        You can view the reports that you have created, as well as the reports that other users have created by selecting the view to display MyReports and Shared Reports from the View Options drop down menu.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Export reports?</div>
    <div id="link6" class="helpHidden">
        You can export the report via the Export button located at the top of the report viewer once you have run the report, and selected to Show this report in Report Viewer. Supported formats include .PDF, .DOC, .XLS, .TXT, and .CSV.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Print reports?</div>
    <div id="link7" class="helpHidden">
        You can print the report via the Print button located at the top of the report viewer once you have run the report, and selected to Show this report in Report Viewer.
    </div>
    <br />
</asp:Content>