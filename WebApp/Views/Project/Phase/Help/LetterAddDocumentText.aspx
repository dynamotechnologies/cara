﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "CodingStrategy").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("CodingVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("CodingVideo"), Name = Constants.CODING })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Append")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter to continue coding?</div>
    <div id="link1" class="helpHidden">
        You can click on either the Add Content to Letter or Cancel button located at the bottom of the page to go back to the letter you were just coding. Add Content to Letter will append the attachment text to the letter, while Cancel will return you to the letter without saving any changes you have made.
    </div>
    <br />

    <div class="helpLink" onclick="toggleArea('link11')">Letter Inbox?</div>
    <div id="link11" class="helpHidden">
        Method 1: You can go to the letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />

    <div class="helpLink" onclick="toggleArea('link2')">Coded Comments page?</div>
    <div id="link2" class="helpHidden">
        You can go to the coded comments page from the expanded left navigation by clicking on Coded comments within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Concern and response page?</div>
    <div id="link3" class="helpHidden">
        You can go to the concern and response listing page from the expanded left navigation by clicking on Concerns/Responses within the corresponding Comment Period of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Comment Period homepage?</div>
    <div id="link4" class="helpHidden">
        Method 1: You can go to the Comment Period homepage from the expanded left navigation by clicking on Comment Period Home of the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View the attachment text?</div>
    <div id="link5" class="helpHidden">
        You can view the text of the attachment in the text field labeled Content. The document text is shown exactly as it will be appended to the letter. Make sure to make any changes to the document text prior to appending the text to the letter, as appending the text is an irreversible process.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Edit the appended attachment text?</div>
    <div id="link6" class="helpHidden">
        You can edit the text of the attachment within the text field labeled Content. Make sure to make any changes to the attachment text prior to appending the text to the letter, as appending the text is an irreversible process.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Append text to this letter?</div>
    <div id="link7" class="helpHidden">
        Once you have edited the text, and are satisfied with the attachment text that you wish to append, you may click the Add Content to Letter to Append the text to the letter.<br /><br />
        Make sure to make any changes to the attachment text prior to appending the text to the letter, as appending the text is an irreversible process.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">View or append text from the other attachments that are associated with this letter?</div>
    <div id="link8" class="helpHidden">
        You can click on either Add to Content or Cancel to go back to the letter coding page. From the letter coding page you may click on another attachment to view or append its content. You will only be able to use this functionality for .PDF and .TXT documents.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Cancel the append content to letter functionality?</div>
    <div id="link9" class="helpHidden">
        You may click on Cancel to cancel this attachment text append to letter action.
    </div>
</asp:Content>