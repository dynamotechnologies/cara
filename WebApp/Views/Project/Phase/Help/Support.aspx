﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Blank.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.Title %></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Details/ucSupport.ascx", new SupportViewModel { Mode = Constants.HELP }); %>
</asp:Content>