﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Form Management")%><br />
    <%: Html.DefinitionActionLink("Set")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Plus")%><br />
    <%: Html.DefinitionActionLink("Master Form Letter")%><br />
    <%: Html.DefinitionActionLink("Duplicates")%><br />
    <%: Html.DefinitionActionLink("Unique Letter")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Form Change Log?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can go to the form change log from the expanded left navigation by clicking on Form Change Log within the desired Comment Period.<br /><br />
        Method 2: You can click on the View Form Change Log button located above and below the Form Management table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Letter inbox?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to the Comment Periods letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding Comment Period of interest.<br /><br />
        Method 2: You can go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Comment Period homepage?</div>
    <div id="link3" class="helpHidden">
        Method 1: You can go to the Comment Periods homepage from the expanded left navigation by clicking on Comment Period Home for the Comment Period of interest.<br /><br />
        Method 2: You can go to the Comment Period homepage from the breadcrumb navigation located directly under the global navigation by clicking on the title of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Project homepage?</div>
    <div id="link4" class="helpHidden">
        You can go to the Projects homepage from the expanded left navigation by clicking on the Project Home.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View all the letters?</div>
    <div id="link5" class="helpHidden">
        The initial view displays only the Unique and Master Form letters. In order to view all the letters, you may click on the Show All Letters button within the filter table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Re-designate the letters?</div>
    <div id="link6" class="helpHidden">
        Method 1: You can click on the drop down menu under the column Re-designate and select the desired option to re-designate this letter as a Form letter, Form plus letter, or Master Form letter within this form set. Click on Go to re-designate the letter. You may also designate this letter as a Unique letter and send it to the Unique letter set. You may also send this letter to another form set, or use the letter to create a new form set.<br /><br />
        Re-designating this letter will initiate the Form detection process within the form set which will compare the newly designated letter with the other letters within the same form set.<br /><br />
        Method 2: For Form, Form plus, and Unique letters you may click on the Compare to Master icon to open the Master form comparison screen. From within the Compare to Master page you can click on the drop down menu labeled Re-designate and select the desired option to re-designate this letter as a Form letter, Form plus letter, or Master Form letter within this form set. Click on Go to re-designate the letter You may also send this letter to another form set if the Master Form for the other form set is selected from the left drop down labeled Compare with Master Form from Set.<br /><br />
        Re-designating this letter will initiate the Form detection process within the form set which will compare the newly designated letter with the other letters within the same form set.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">View the changed Form designations / Form Change Log?</div>
    <div id="link7" class="helpHidden">
        Method 1: You can go to the form change log from the expanded left navigation by clicking on Form Change Log within the desired Comment Period. From the form change log you can view the letters that have changed their form designation. The entries are listed by date, with the most recent placed at the top.<br /><br />
        Method 2: You can click on the View Form Change Log button located above and below the Form Management table. From the form change log you can view the letters that have changed their form designation. The entries are listed by date, with the most recent placed at the top.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Re-sort letters between Form sets?</div>
    <div id="link8" class="helpHidden">
        Step 1: CARA users with the authorized roles can click on the drop down labeled Re-sort Form Set to select the Set which they want to compare and re-sort.<br /><br />
        Step 2: Select the Master Form from the drop down on the right which the selected Set will be compared against.<br /><br />
        Step 3: Click on Re-sort to begin the compare and Re-sort process. If the letter set from step 1 is identified as a Form or Form plus of the Master Form from step 2, it will be moved to the Master Form set. If no similarity is detected no changes will occur.<br /><br />
        This is an irreversible step. That is, there is no Undo feature. You may have to re-sort again, or do manual designations of form letters in the event that the results are not desired.<br /><br />
        Step 4: Review the changes made from the view Form Change Log.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Rename Form sets?</div>
    <div id="link9" class="helpHidden">
        You may click on the button labeled Rename next to a Form set in the event that you wish to give the set a unique name for easier identification and management. You will not be able to rename the Unique set.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Create a new Form set?</div>
    <div id="link10" class="helpHidden">
        You can click on the drop down menu under the column Re-designate and use the letter to create a new form set. Click on Go to create the new form set.<br /><br />
        Once the new form set has been created, you can either manually designate form letters into the new form set, or the CARA users with the authorized roles can use the re-sort feature to compare and move letters into the new form set.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">Delete a Form set?</div>
    <div id="link11" class="helpHidden">
        A form set will be deleted if it contains no letters.<br /><br />
        Step 1: In order to delete a form set you have to first re-designate or move all the letters out of that form set, until the only letter remaining is the master form for that form set.<br /><br />
        Step 2: Once only the master form is remaining in the form set, a drop down will appear under the column Re-designate. You can then move or re-designate the master form to send it to another set thereby deleting the form set.
    </div>
    <br />
</asp:Content>