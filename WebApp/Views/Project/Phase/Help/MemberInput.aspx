﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Team Member (Role)")%><br />
    <%: Html.DefinitionActionLink("CARA Comment Period Manager")%><br />
    <%: Html.DefinitionActionLink("Reuse")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">A different Comment Period homepage for the same Project?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to a different CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest. The option to select and go to another Comment Period will be available if another Comment Period has already been set up.<br /><br />
        Method 2: You can click on the Project name located directly below the global navigation. From the Project homepage you can go to the homepage of another Comment Period by selecting the name of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">A different Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the Home tab in the top navigation to go to your My Projects table. From this table you can then select the name of the Project of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Letter inbox?</div>
    <div id="link4" class="helpHidden">
        You can go to the letter inbox for a Comment Period by clicking on its name from the expanded left navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Add team members?</div>
    <div id="link5" class="helpHidden">
        You can click on the radio button labeled Add member from directory located towards the bottom of the page to search for team members by either their first or last names. Once the results are displayed you can add team members or Comment Period managers for this Comment Periods by selecting their corresponding radio button and clicking on Add User.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Reuse team members?</div>
    <div id="link6" class="helpHidden">
        You can click on the radio button labeled Reuse team members located towards the bottom of the page to search for Projects (partial search supported) from where you wish to reuse team members or Comment Period managers for this Comment Periods. Once the results are displayed you can select the Comment Period from where you wish to reuse team members and click on Reuse Users.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Designate a CARA Comment Period manager?</div>
    <div id="link7" class="helpHidden">
        You can designate a team member as a Comment Period manager from the add team member functionality. You can select the radio button option under the column Comment Period manager to designate the user as a Comment Period manager.<br /><br />
        If you wish you modify their role at a later date, you can select the corresponding radio button for CPM and click on their corresponding Update button to modify they role.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Update or Modify the role of a Comment Period manager or Team member?</div>
    <div id="link10" class="helpHidden">
        You may modify the roles of a Team member or Comment Period manager by selecting the new designation from the Role column via their corresponding radio buttons, and then clicking on their corresponding Update buttons.<br /><br />
        You will not be able to modify or update the role of a Team member or Comment Period manager if they are Deactivated.<br />
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Remove a team member?</div>
    <div id="link8" class="helpHidden">
        You can remove a team member or Comment Period manager by selecting the button labeled Remove. The remove option will be available so as long as no work has been done on the Comment Period by the user you wish to remove. If work has been done, then the user can be deactivated.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Deactivate a team member?</div>
    <div id="link9" class="helpHidden">
        You can deactivate a team member or Comment Period manager by selecting the button labeled Deactivate. The deactivation option will be available once the user has done work on the Comment Period. If no work has been done, then the user can be removed.
    </div>
    <br />
</asp:Content>