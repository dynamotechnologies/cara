﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%><br />
    <%: Html.DefinitionActionLink("Unique Letter")%><br />
    <%: Html.DefinitionActionLink("Form Letter")%><br />
    <%: Html.DefinitionActionLink("Form Plus")%><br />
    <%: Html.DefinitionActionLink("Duplicates")%><br />
    <%: Html.DefinitionActionLink("Status")%><br />
    <%: Html.DefinitionActionLink("New Letters")%><br />
    <%: Html.DefinitionActionLink("Coding in Progress")%><br />
    <%: Html.DefinitionActionLink("Total Letters")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Projects homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the homepage of a Project by selecting the name of the Project in the My Projects table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Comment Period homepage?</div>    
    <div id="link2" class="helpHidden">
        You can go to the homepage of a Comment Period by selecting the name of the Comment Period in the My Projects table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Comment Period letter inbox?</div>
    <div id="link3" class="helpHidden">
        You can go to the letter inbox of a Comment Period by selecting the corresponding number listed under column title Total Letters in the My Projects table.
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link4')">Filter letters?</div>
    <div id="link4" class="helpHidden">
        Method 1: You can filter letters of a Comment Period by selecting the corresponding number listed under one of the following column titles: New letters, Unique letters, Form letters, Coding in Progress letters, Coding Complete letters, and Early Attention letters.<br /><br />
        Method 2: You can click on the link Advanced Search located in the upper right corner of the My Projects table. Once in the Projects listing page you will have the option to filter the letters by their Project status, Lead Management Unit, Project Analysis type, Project Activity, New letters, Unique letters, Form letters, Coding in Progress letters, Coding Complete letters, as well as Early Attention letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">Create new CARA project?</div>
    <div id="link5" class="helpHidden">
        If you have the authorized role designation, you can create a new CARA Project by selecting the link Create CARA Project. By selecting this link, you will be taken to step 1 of the CARA Project setup wizard. If you do not have an authorized role, this link will not appear.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Search CARA projects?</div>
    <div id="link6" class="helpHidden">
        Method 1: You can search for a CARA Project by its name or ID Number from the text box entitled Find CARA Projects by Name or ID.<br /><br />
        Method 2: You can click on the link Advanced Search located in the upper right corner of the My Projects table. Once in the Projects listing page you will have the option to search for CARA Projects by their Project name, Project ID, or Project description utilizing any keyword that may help identify the Project you are searching for.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Sort CARA Projects?</div>
    <div id="link7" class="helpHidden">
        You can sort the Projects listed in the My Projects table by clicking the column headings in the table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">View all Projects, or all of My Projects?</div>
    <div id="link8" class="helpHidden">
        Method 1: You can click on the link Advanced Search located in the upper right corner of the My Projects table. Once in the Projects listing page you will have the option to view all Projects in which you have a designated role by selecting the check box entitled View My Projects. To view all CARA Projects you can click on the button Show All Projects.<br /><br />
        Method 2: You can click on the Projects tab to go to the Project listing page. Once in the Projects listing page you will have the option to view all Projects in which you have a designated role by selecting the check box entitled View My Projects. To view all CARA Projects you can click on the button Show all Projects.
    </div>
    <br />
</asp:Content>