﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Letter inbox?</div>
    <div id="link1" class="helpHidden">
        Method 1: You can click and go to the comment periods letter inbox from the expanded left navigation by clicking on Letter inbox within the corresponding comment period of interest.<br /><br />
        Method 2: You can click and go to the letter inbox from the breadcrumb navigation located directly under the global navigation by clicking on Letter inbox.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Coded comments page?</div>
    <div id="link2" class="helpHidden">
        You can click and go to the coded comments from the expanded left navigation by clicking on Coded comments within the corresponding comment period of interest.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link3')">View the associated letter?</div>
    <div id="link3" class="helpHidden">
        You can click on the Letter ID displayed on the page to view the letter. Clicking on this link will take you directly to the comment within context of the corresponding letter.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">View the associated concern and response?</div>
    <div id="link4" class="helpHidden">
        You can click on the Concern and response ID displayed on the page to view the associated concern and response.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">View the coding structure?</div>
    <div id="link5" class="helpHidden">
        You can click and view the coding structure from the expanded left navigation by clicking on Coding structure within the corresponding comment period of interest.
    </div>
    <br />
</asp:Content>