﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Welcome Message")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link2')">Modify the welcome message?</div>
    <div id="link2" class="helpHidden">
        As designated by your role, you can click on the link entitled Edit next to the text description Welcome Message to type and publish a message to all CARA users. This message is displayed on the homepage. You may relay message to all CARA users informing them of events such as system outages, upcoming releases, new features, as well as the availability of new resources, materials or help content.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Search and manage CARA users?</div>
    <div id="link3" class="helpHidden">
        As designated by your role, you can click on the link entitled Search in CARA next to the text description User Management to search for users within CARA.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Add users to CARA from PALS?</div>
    <div id="link4" class="helpHidden">
        As designated by your role, you can click on the link entitled Add from PALS next to the text description User management to add users to CARA from PALS.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link5')">View the master coding structure?</div>
    <div id="link5" class="helpHidden">
        If authorized by your role, you may click on View next to the text description Master Coding Structure to view the complete master coding structure for use within Comment Periods for any CARA Project.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Manage the master coding structure?</div>
    <div id="link6" class="helpHidden">
        As authorized by your role, you can click on the link entitled Manage next to the text description Master Coding Structure to modify the complete master coding structure for use within the Comment Periods for any CARA Project. Any changes made to the master coding structure will only be applicable to future Projects and will not impact current or past Projects.
    </div>
    <br />
</asp:Content>