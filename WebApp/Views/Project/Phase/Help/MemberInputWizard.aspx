﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "ProjectCreationBusinessRules").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">
    <%: Html.ActionLink(Utilities.GetResourceString("ProjectWizardVideo"), Constants.VIDEO, new HelpViewModel { Title = Utilities.GetResourceString("ProjectWizardVideo"), Name = "ProjectWizard" })%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Team Member (Role)")%><br />
    <%: Html.DefinitionActionLink("CARA Comment Period Manager")%><br />
    <%: Html.DefinitionActionLink("Reuse")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">Previous step of the Project setup wizard?</div>
    <div id="link1" class="helpHidden">
        You can click on the button labeled Back located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Next step of the Project setup wizard?</div>
    <div id="link2" class="helpHidden">
        You can click on the button labeled Next located towards the bottom of the page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Approve Project creation page?</div>
    <div id="link3" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Project homepage?</div>
    <div id="link4" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can click on the button labeled Accept to go to the current Projects homepage.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link6')">Add team members?</div>
    <div id="link6" class="helpHidden">
        You can click on the radio button labeled Add member from directory located towards the bottom of the page to search for team members by either their first or last names. Once the results are displayed you can add team members or Comment Period managers for this Comment Periods by selecting their corresponding radio button and clicking on Add User.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Reuse team members?</div>
    <div id="link7" class="helpHidden">
        You can click on the radio button labeled Reuse team members located towards the bottom of the page to search for Projects (partial search supported) from where you wish to reuse team members or Comment Period managers for this Comment Periods. Once the results are displayed you can select the Comment Period from where you wish to reuse team members and click on Reuse Users.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">Designate a CARA Comment Period manager?</div>
    <div id="link8" class="helpHidden">
        You can designate a team member as a Comment Period manager from the add team member functionality. You can select the radio button option under the column Comment Period manager to designate the user as a Comment Period manager.<br /><br />
        If you wish you modify their role at a later date, you can select the corresponding radio button for CPM and click on their corresponding Update button to modify they role.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link12')">Update or Modify the role of a Comment Period manager or Team member?</div>
    <div id="link12" class="helpHidden">
        You may modify the roles of a Team member or Comment Period manager by selecting the new designation from the Role column via their corresponding radio buttons, and then clicking on their corresponding Update buttons.<br /><br />
        You will not be able to modify or update the role of a Team member or Comment Period manager if they are Deactivated.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link9')">Remove a team member?</div>
    <div id="link9" class="helpHidden">
        You can remove a team member or Comment Period manager by selecting the button labeled Remove. The remove option will be available so as long as no work has been done on the Comment Period by the user you wish to remove. If work has been started by the user, then the user can only be deactivated.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link10')">Deactivate a team member?</div>
    <div id="link10" class="helpHidden">
        You can deactivate a team member or Comment Period manager by selecting the button labeled Deactivate. The deactivation option will be available once the user has done work on the Comment Period. If no work has been started by the user, then the user can be removed.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link11')">Finish creating this Comment Period?</div>
    <div id="link11" class="helpHidden">
        You can click on the button labeled Finish located towards the bottom of the page to go to the approve Project creation page. From the approve Project creation page you can accept this Project and go to its Project homepage, or cancel the creation of this Project. You can edit this Comment Period at a later date by modifying its settings from the Project homepage, Comment Period homepage, as well as by visiting this Project creation wizard. Selecting Finish at this point will apply default settings for the remaining steps. You may view and modify these default settings in the approve Project creation page.
    </div>
    <br />
</asp:Content>