﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Comment Period")%><br />
    <%: Html.DefinitionActionLink("Early Attention")%><br />
    <%: Html.DefinitionActionLink("Status")%><br />
    <%: Html.DefinitionActionLink("Filter")%><br />
    <%: Html.DefinitionActionLink("Activities")%><br />
    <%: Html.DefinitionActionLink("Archived")%><br />
    <%: Html.DefinitionActionLink("Analysis Type")%><br />
    <%: Html.DefinitionActionLink("Lead Management Unit")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">Projects homepage?</div>
    <div id="link2" class="helpHidden">
        You can go to the homepage of a Project by selecting the name of the Project in the Project listing page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">Comment Period homepage?</div>
    <div id="link3" class="helpHidden">
        You can go to the homepage of a Comment Period by selecting the name of the Comment Period in the Project listing page.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Comment Period letter inbox?</div>
    <div id="link4" class="helpHidden">
        You can go to the letter inbox of a Comment Period by selecting the corresponding number listed under the column titled Total Letters in the Project listing page. You can also go to a filtered listing of letters by selecting the corresponding numbers listed under the columns titled New Letters, Early Attention Required, Coding in Progress, Coding Complete, Unique Letters, and Form Letters.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">Filter CARA Projects?</div>
    <div id="link5" class="helpHidden">
        Within the Projects listing page you can filter CARA Projects by their Project status, Lead management unit, Project Analysis type, Project activity, New letters, Unique letters, Form letters, Coding in progress letters, Coding complete letters, and Early Attention letters.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Search CARA projects?</div>
    <div id="link6" class="helpHidden">
        Within the Projects listing page you can search for CARA Projects by entering any keyword that may help identify the Project by Project name, Project ID, or Project description.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Sort CARA Projects?</div>
    <div id="link7" class="helpHidden">
        You can sort the Projects listed in the Project listing page by clicking to sort the column headings in the table.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link8')">View all Projects, or all My Projects?</div>
    <div id="link8" class="helpHidden">
        You can click the checkbox entitled View My Projects to display only those Projects in which you have a designated role. To view only your Projects, you may also return to the CARA Home page. To view all CARA Projects, you can click on the button entitled Show All Projects.
    </div>
    <br />
	<div class="helpLink" onclick="toggleArea('link9')">Personalize table settings?</div>
    <div id="link9" class="helpHidden">
        You can select the desired number of rows displayed per table on the My Profile tab in the global navigation. This personal setting will be applied to many but not all of the data display tables within CARA.
    </div>
    <br />
</asp:Content>