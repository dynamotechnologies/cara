﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Help.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.HelpViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"><%=Model.PageTitle %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server"><%=Model.PageTitle%></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DocumentContent" runat="server">
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserGuide").FirstOrDefault()); %>
    <% Html.RenderPartialDocumentLink(Model.Documents.Where(x => x.Key == "UserRolesMatrixTable").FirstOrDefault()); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="VideoContent" runat="server">

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="DefinitionContent" runat="server">
    <%: Html.DefinitionActionLink("Auto-markup")%><br />
    <%: Html.DefinitionActionLink("Economic")%><br />
    <%: Html.DefinitionActionLink("Request for Comment Extension")%><br />
    <%: Html.DefinitionActionLink("Threats")%><br />
    <%: Html.DefinitionActionLink("Request for Information")%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NavContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link1')">CARA homepage?</div>
    <div id="link1" class="helpHidden">
        You can go to the CARA homepage by selecting the Home tab in the top navigation.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link2')">A different Comment Period homepage for the same Project?</div>
    <div id="link2" class="helpHidden">
        Method 1: You can go to a different CARA Comment Period homepage from the expanded left navigation by selecting the corresponding Comment Period name of interest. The option to select and go to another Comment Period will be available if another Comment Period has already been set up.<br /><br />
        Method 2: You can click on the Project name located directly below the global navigation. From the Project homepage you can go to the homepage of another Comment Period by selecting the name of the Comment Period.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link3')">A different Project homepage?</div>
    <div id="link3" class="helpHidden">
        You can click on the Home tab in the top navigation to go to your My Projects table. From this table you can then select the name of the Project of interest.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link4')">Letter inbox?</div>
    <div id="link4" class="helpHidden">
        You can go to the letter inbox for a Comment Period by clicking on its name from the expanded left navigation.
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ActionContent" runat="server">
    <div class="helpLink" onclick="toggleArea('link5')">View the list of terms in an auto-markup category?</div>
    <div id="link5" class="helpHidden">
        You can click on the button labeled View located under the column List of terms for the corresponding auto-markup category of choice.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link6')">Select an auto-markup list of terms to use?</div>
    <div id="link6" class="helpHidden">
        You can select the check box located under the column Select for the corresponding auto-markup category of choice.
    </div>
    <br />
    <div class="helpLink" onclick="toggleArea('link7')">Use the auto-markup functionality?</div>
    <div id="link7" class="helpHidden">
        The auto-markup functionality will be activated automatically once the Comment Period inbox begins populating with letters. The auto-markup categories selected will determine which words are scanned for matches within the text of the letters. The words identified per letter can be viewed from the letter coding page in the auto-markup section on the right, and towards the bottom of the page.
    </div>
    <br />
</asp:Content>