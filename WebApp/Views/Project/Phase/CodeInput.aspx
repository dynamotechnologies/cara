﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PhaseCodeViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% if (Model.IsWizardMode)
       {
           Html.RenderAction("WizardNav", Constants.NAV, new { actionName = ViewData["action"], mode = Model.Mode });
       }
       else
       {
           Html.RenderAction("ProjectNav", Constants.NAV);
       } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (!string.IsNullOrEmpty(TempData[Constants.PHASE + Constants.CODE] as string) && !Model.IsWizardMode)
        { %>
        <div id="confirmAction">
            <div class="message" style="width:500px;"><%: TempData[Constants.PHASE + Constants.CODE] as string%></div>
            <br />
        </div>
    <% } %>
    <div class="pageDesc"><%: Html.DisplayFor(model => model.Description)%></div>
    <br />
    <%: Html.ValidationSummary() %>
    <br />
    <% if (Model.IsWizardMode)
        { %>
        <input type="button" class="button" name="action" value="Back" onclick="submitCodeInput('Back')"/>
        <input type="button" class="button" name="action" value="Next" onclick="submitCodeInput('Next')" />
        <input type="button" class="button" name="action" value="Finish" onclick="submitCodeInput('Finish')"/>
        <% }
        else if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
        { %>
        <input type="button" id="save1" class="button" name="action" value="Save" onclick="confirmSave()" />
    <% } %>
       <input type="button" value="<%=Constants.PREVIEW %>" name="preview" class="button" onclick="previewCodeTree()" />
       
    <% if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
        { %>
        <input type="button" class="button invisible" id="addCode1" onclick="return false;" value="Add Custom Code" />
        <input type="button" class="button invisible" id="editCode1" onclick="return false;" value="Edit Custom Code" />
    <% } %>
    <br /><br />
    <!-- progress indicator div: must be placed before the coding window -->
<%--    <div id="loading" style="display:none;">
        <div class="centered">Please wait...</div>
    </div>--%>
    <% //Html.RenderPartial("~/Views/Shared/Input/ucPhaseCodeTaskAssignment.ascx", Model); %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucPhaseCodeInput.ascx", Model); %>
    <% using (Html.BeginForm()){ %>
    <br />

    <% if (Model.IsWizardMode)
        { %>
        <input type="button" class="button" name="action" value="Back" onclick="submitCodeInput('Back')"/>
        <input type="button" class="button" name="action" value="Next" onclick="submitCodeInput('Next')" />
        <input type="button" class="button" name="action" value="Finish" onclick="submitCodeInput('Finish')"/>
     <% }
              
    else if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
        { %>
        <input type="button" id="save" class="button" name="action" value="Save" onclick="confirmSave()" />
     <% } %>

    <% if (!Model.IsWizardMode & Utilities.UserHasPrivilege("MPRJ", Constants.UNIT))
        { %>
        <input id="btnReset" type="button" class="button" name="action" value="Reset Coding Structure" onclick="return resetCode();" />
        <% } %>

       <input type="button" value="<%=Constants.PREVIEW %>" name="preview" class="button" onclick="previewCodeTree()" />
             
    <% if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
        { %>
        <input type="button" class="button invisible" id="addCode" onclick="return false;" value="Add Custom Code" />
        <input type="button" class="button invisible" id="editCode" onclick="return false;" value="Edit Custom Code" />
    <% } %>
    
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.TaskAssignmentContext) %>
    <%: Html.HiddenFor(m => m.TaskAssignmentUsers) %>
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Description) %>
    <%: Html.HiddenFor(m => m.Mode) %>
    <%: Html.HiddenFor(m => m.ProjectId)%>
    <%: Html.HiddenFor(m => m.PhaseId)%>
    <%: Html.HiddenFor(m => m.PhaseTypeId)%>
    <%: Html.HiddenFor(m => m.HasCodedComments)%>
    <%: Html.HiddenFor(m => m.ActionString) %>
    <%: Html.Hidden("NodeChangeCheck") %>
    <%: Html.Hidden("NodeChangeUncheck") %>
    <%: Html.Hidden("NoChecks", (Model.CheckedNodes==null)?false:!Model.CheckedNodes.Any()) %>
<% } %>

<% Html.Telerik().Window()
    .Name("WindowPreview")
    .Title(string.Format("{0} {1}", Constants.PREVIEW, Utilities.GetResourceString("CodingStructure")))
    .Draggable(true)
    .Resizable(resizing => resizing
                    .Enabled(true)
                    .MaxWidth(900)
                    .MaxHeight(700))
    .Modal(true)
    .ClientEvents(events => events.OnOpen("setWindowFocus"))
    .Buttons(b => b.Maximize().Close())
    .Width(700)
    .Height(500)
    .Visible(false)
    .Render(); %>

<!-- This scripts are necessary for the treeview to be displayed in the modal window -->
<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.list.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/jquery.cookie.js")
            .Add("~/Scripts/jquery.printElement.min.js")
            .Add("~/Scripts/cara.phasecodeinput.js")
            .Add("~/Scripts/cara.phasecodecreate.js")); %>
</asp:Content>