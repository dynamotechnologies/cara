﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.ConcernResponseInputViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% bool showConfirm = !string.IsNullOrEmpty(TempData[Constants.CONCERN_RESPONSE] as string);%>
        <div id="confirmAction" <%=showConfirm?"":"hidden" %> >
            <div class="message" style="width:500px;"><%: TempData[Constants.CONCERN_RESPONSE] as string%></div>
            <br />
        </div>
<%using (Html.BeginForm()) 
  {
      bool assignedToOther = Model.ConcernResponse.UserId.HasValue ?
          Model.ConcernResponse.UserId.Value != Utilities.GetUserSession().UserId
          : false;
      if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE)) 
       { %>
        <input type="button" class="button" value="<%=Constants.SAVE %>" onclick="saveConcern('<%=Constants.SAVE %>')" />&nbsp;
        <% if (Model.TrackBackPage != "")
           { %>
            <input type="button" class="button" value="<%=Constants.SAVE_AND_RETURN %>" onclick="saveConcern('<%=Constants.SAVE_AND_RETURN %>')" />&nbsp;
        <% } %>
        <% if (Model.ConcernResponse.ConcernResponseId > 0)
           { %>
            <input type="button" class="button" value="<%=Constants.DELETE %>" onclick="saveConcern('<%=Constants.DELETE %>')" />&nbsp;
        <% } %>
    <% } %>
    <br /><br />
    <table>
        <tr>
            <td><label for="ConcernResponse_UserId" class="fieldLabel">Assigned To:</label></td>
            <td>
                    <%if (!Utilities.UserHasPrivilege("WRSP", Constants.PHASE))//get user priviledge code
                    {
                        %><%=Model.TeamMembers[Model.SelectedIndex(Model.UserId)].Text%><%
                    }
                      else
                      {%><%:Html.Telerik()
                        .DropDownListFor(x => x.UserId)
                        .BindTo(Model.TeamMembers)
                        .SelectedIndex(Model.SelectedIndex(Model.UserId))
                        .ClientEvents(events => events.OnChange("function(){ if ($('#UserId').data('tDropDownList').value() != $('#oldUserId').val()) {handleFormChanged(false);}}"))
                        %>
                    <%} %>
           </td>
        </tr>
        <tr>
            <td><label for="ConcernResponse_StatusId" class="fieldLabel">Status:</label></td>
            <td>
                    <%if (!Utilities.UserHasPrivilege("WRSP", Constants.PHASE) || assignedToOther)//get user priviledge code
                    {
                        %><%=Model.ConcernResponse.ConcernResponseStatusName %><%
                    }
                    else
                    {%><%:Html.Telerik()
                        .DropDownListFor(x=>x.ConcernResponse.ConcernResponseStatusId)
                        .BindTo(Model.LookupConcernResponseStatus)
                        .SelectedIndex(Model.ConcernResponse.ConcernResponseStatusId)
                        .ClientEvents(events => events.OnChange("handleStatusChanged"))%>
                      <!--<img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" 
                        alt="If the Status is 'Ready for Review' or 'Response Complete' and you modify the Concern Statement and/or Response, the Status will update to either 'Concern In Progress' or Response In Progress'."
                        title="If the Status is 'Ready for Review' or 'Response Complete' and you modify the Concern Statement and/or Response, the Status will update either 'Concern In Progress' or Response In Progress'." />-->
                    <%} %>
            </td>
        </tr>
        <tr>
            <td class="fieldLabel"><%: Html.LabelFor(model => model.ConcernResponse.PhaseCodeId)%>:</td>
            <td class="text">
              <span id="spnCodeNumber"><%: Html.DisplayFor(model => model.CodeNumberAndDescription)%></span>
          
              <% if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && !assignedToOther) 
                 { %>
                     <input type="button" class="button" value="Select Code" onclick="openWindow('ModalWindow', '/Concern/SelectCode/<%= Model.PhaseId %>');" />
              <% } %>
            </td>
        </tr>
        <tr>
            <td><label for="ConcernResponse_Label" class="fieldLabel">Label:</label></td>
            <td>
                    <%if (!Utilities.UserHasPrivilege("WRSP", Constants.PHASE) || assignedToOther)//get user priviledge code
                    {
                        %><%=Model.ConcernResponse.Label %><%
                    }
                    else
                    {%>
                        <%:Html.TextBoxFor(x => x.ConcernResponse.Label, new { style="width:400px;", maxlength = "50", onchange = "handleFormChanged(false)" })%>
                    <%} %>
            </td>
        </tr>
    </table>
    <div id="commentListArea"><% Html.RenderPartial("~/Views/Shared/List/ucConcernCommentList.ascx", Model); %></div>
    <br />
    <% if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE) 
           && !assignedToOther)
       { %>
       <%: Html.ActionLink("Add More Comments", Constants.LIST, Constants.COMMENT, new { mode = Constants.EDIT, ConcernResponseId = Model.ConcernResponse.ConcernResponseId }, new { @class = "button", @id = "btnAddComments" })%>
        <input type="button" class="button" value="<%=Constants.SAVE %>" onclick="saveConcern('<%=Constants.SAVE %>')" />&nbsp;
        <% if (Model.TrackBackPage != "")
           { %>
            <input type="button" class="button" value="<%=Constants.SAVE_AND_RETURN %>" onclick="saveConcern('<%=Constants.SAVE_AND_RETURN %>')" />&nbsp;
        <% } %>
        <% if (Model.ConcernResponse.ConcernResponseId > 0)
           { %>
            <input type="button" class="button" value="<%=Constants.DELETE %>" onclick="saveConcern('<%=Constants.DELETE %>')" />&nbsp;
        <% } %>   
    <% } %>
    <br />
    <%: Html.ValidationSummary() %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucConcernInput.ascx", Model); %>
    <br />
    <% if (!Model.Locked && Utilities.UserHasPrivilege("WRSP", Constants.PHASE))
       { %>
        <input type="button" class="button" value="<%=Constants.SAVE %>" onclick="saveConcern('<%=Constants.SAVE %>')" />&nbsp;
        <% if (Model.TrackBackPage != "")
           { %>
            <input type="button" class="button" value="<%=Constants.SAVE_AND_RETURN %>" onclick="saveConcern('<%=Constants.SAVE_AND_RETURN %>')" />&nbsp;
        <% } %>
        <% if (Model.ConcernResponse.ConcernResponseId > 0)
           { %>
            <input type="button" class="button" value="<%=Constants.DELETE %>" onclick="saveConcern('<%=Constants.DELETE %>')" />&nbsp;
        <% } %>
    <% } %>


    <%if (!Utilities.UserHasPrivilege("WRSP", Constants.PHASE) || assignedToOther)//get user priviledge code
    {
        %>
        <%: Html.HiddenFor(m => m.ConcernResponse.ConcernResponseStatusId) %>
        <%: Html.HiddenFor(m => m.ConcernResponse.Label) %> 
        <%: Html.Hidden("concernText", Model.ConcernResponse.ConcernText) %>
        <%: Html.Hidden("responseText", Model.ConcernResponse.ResponseText) %>    
    <%}%>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.AnalysisTypeName) %>
    <%: Html.HiddenFor(m => m.PhaseId) %>
    <%: Html.HiddenFor(m => m.ProjectId) %>
    <%: Html.HiddenFor(m => m.ConcernResponse.ConcernResponseId) %>
    <%: Html.Hidden("oldUserId", Model.UserId) %>
    <%: Html.Hidden("action") %>
    <%: Html.Hidden("currentUserId", Utilities.GetUserSession().UserId) %>
    <%: Html.Hidden("assignedUserId", Model.ConcernResponse.UserId) %>
    <%: Html.Hidden("assignedToOther", Model.ConcernResponse.UserId.HasValue ?
          Model.ConcernResponse.UserId.Value != Utilities.GetUserSession().UserId
          : false) %>
<% } %>

<% 
    Html.Telerik().Window()
        .Name("ModalWindow")
        .Title("Choose A " + Utilities.GetResourceString(Constants.CODE))
        .Draggable(true)
        .Resizable(resizing => resizing
                        .Enabled(true)
                        .MinHeight(250)
                        .MinWidth(250)
                        .MaxHeight(800)
                        .MaxWidth(1000)
                    )
        .Modal(true)
        .Buttons(b => b.Maximize().Close())
        .Width(400)
        .Height(500)
        .Visible(false)
        .Render();
    
   
    Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/telerik.treeview.min.js")
            .Add("~/Scripts/jquery.signalR-1.2.2.min.js")
            .Add("~/Scripts/cara.concerninput.js")
            .Add("~/Scripts/cara.concerncommentlist.js")
            .Add("~/signalr/hubs")
            .Add("~/Scripts/util.confirmmessage.js"));
%>
</asp:Content>
