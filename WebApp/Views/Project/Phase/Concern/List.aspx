﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.ConcernResponseListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (!string.IsNullOrEmpty(TempData[Constants.CONCERN_RESPONSE + Constants.LIST] as string))
        { %>
        <div id="confirmAction">
            <div class="message" style="width:500px;"><%: TempData[Constants.CONCERN_RESPONSE + Constants.LIST] as string%></div>
            <br />
        </div>
    <% } %>
<% using (Html.BeginForm(string.Empty, "Concern", FormMethod.Post))
   { %>
    <%: Html.ValidationSummary() %>
    <% Html.RenderPartial("~/Views/Shared/Filter/ucConcernFilter.ascx", Model); %><br />
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.CanEdit)%>
    <%: Html.HiddenFor(m => m.Mode)%>
    <%: Html.Hidden("Updated", Request.Params["Updated"]) %>
<% } %>
    
    <% using (Html.BeginForm())
       { %>
        <% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && Model.CanEdit)
            { %>
            <% if (Constants.EDIT.Equals(Model.Mode))
               { %>
                <div class="text" style="width: 800px">
                  All of the Concerns/Responses are displayed in order by their current sequence number.
                  When you click the 'Finish' button, your previous filter and sorting options will be reapplied.
                </div>
                <br />
                <input type="button" class="button" name="action" value="<%=Constants.UPDATE %>" onclick="javascript:submitSequenceNumbers();" />
                <%: Html.ActionLink(Constants.FINISH, Constants.LIST, Constants.CONCERN, null, new { @class = "button" })%>
            <% }
               else
               { %>
                <%-- Html.ActionLink(string.Format("{0} {1}", Constants.CREATE, Utilities.GetResourceString(Constants.CONCERN)),
                        Constants.INPUT, Constants.CONCERN, new { id = 0 }, new { @class = "button" })--%>
                <% if (Model.ConcernResponses != null && Model.ConcernResponses.Count > 0)
                   { %>
                    <%: Html.ActionLink(string.Format("{0} {1}", Constants.EDIT, "Sequence"), Constants.LIST,
                            Constants.CONCERN, new { mode = Constants.EDIT }, new { @class = "button" })%>
                <% } %>
            <% } %>
                        <input type="submit" id="SaveTasks1" class="button" name="action" value="Save Task Assignments"/>
        <% } %>
        <br /><br />
        <% Html.RenderPartial("~/Views/Shared/List/ucConcernList.ascx", Model); %>
        <br />
        <% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && Model.CanEdit)
            { %>
                <% if (Constants.EDIT.Equals(Model.Mode))
                   { %>
                    <input type="button" class="button" name="action" value="<%=Constants.UPDATE %>" onclick="javascript:submitSequenceNumbers();" />
                    <%: Html.ActionLink(Constants.FINISH, Constants.LIST, Constants.CONCERN, null, new { @class = "button" })%>
                <% }
                   else
                   { %>
                    <%-- Html.ActionLink(string.Format("{0} {1}", Constants.CREATE, Utilities.GetResourceString(Constants.CONCERN)),
                            Constants.INPUT, Constants.CONCERN, new { id = 0 }, new { @class = "button" })--%>
                    <% if (Model.ConcernResponses != null && Model.ConcernResponses.Count > 0)
                       { %>
                        <%: Html.ActionLink(string.Format("{0} {1}", Constants.EDIT, "Sequence"), Constants.LIST,
                                Constants.CONCERN, new { mode = Constants.EDIT }, new { @class = "button" })%>
                    <% } %>
                    <input type="submit" id="SaveTasks2" class="button" name="action" value="Save Task Assignments"/>
                <% } %>
                <%: Html.HiddenFor(m => m.TaskAssignmentContext)%>
                <%: Html.HiddenFor(m => m.TaskAssignmentUsers)%>
                <%: Html.Hidden("Dirty", "False", new { @id = "Dirty" })%>
                <%: Html.HiddenFor(m=>m.ActionString) %>
                <%: Html.HiddenFor(m=>m.CanEdit) %>
                <%: Html.HiddenFor(m=>m.BreadCrumbTitle) %>
                <%: Html.HiddenFor(m=>m.PageTitle) %>
                <%: Html.HiddenFor(m=>m.CacheId) %>
            <%}
       }%>
<!-- Hidden fields for the model  -->
 <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.concernlist.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/cara.taskassignmentutil.js"));
%>
</asp:Content>
