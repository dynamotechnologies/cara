﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.SummaryViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.ActionLink(Constants.ACCEPT, Constants.DETAILS, Constants.PROJECT, new { projectId = Model.Project.ProjectId }, new { @class = "button" })%>
    <% string msg = string.Format(@"Discard the {0} from CARA and exit the wizard", PageMode.ProjectCreateWizard.Equals(Model.Mode) ? Constants.PROJECT : Utilities.GetResourceString(Constants.PHASE));
       if (PageMode.ProjectCreateWizard.Equals(Model.Mode) || PageMode.PhaseCreateWizard.Equals(Model.Mode))
        { %>
        <a class="button" href="#" onclick="cancelWizard('<%=msg%>', '<%= Url.Action(Constants.CANCEL + Constants.WIZARD, new { projectId = Model.Project.ProjectId, phaseId = Model.Phase.PhaseId, mode = Model.Mode }) %>')"><%=Constants.CANCEL%></a >
    <% } %>
    <a class="button" href="#" onclick="$('#WindowSummary').data('tWindow').center().open();return false;"><%=string.Format("{0} {1}", Constants.PRINTABLE, Constants.SUMMARY)%></a>
    <br /><br />
    <% Html.RenderPartial("~/Views/Shared/Details/ucProjectSummary.ascx", Model.Project); %>
    <br />
    <% Html.RenderPartial("~/Views/Shared/Details/ucPhaseSummary.ascx", Model.Phase); %>
    <br /><br />
    <%: Html.ActionLink(Constants.ACCEPT, Constants.ACCEPT, Constants.PHASE, new { projectId = Model.Project.ProjectId, phaseId = Model.Phase.PhaseId }, new { @class = "button" })%>
    <% if (PageMode.ProjectCreateWizard.Equals(Model.Mode) || PageMode.PhaseCreateWizard.Equals(Model.Mode))
        { %>
        <a class="button" href="#" onclick="cancelWizard('<%=msg%>', '<%= Url.Action(Constants.CANCEL + Constants.WIZARD, new { projectId = Model.Project.ProjectId, phaseId = Model.Phase.PhaseId, mode = Model.Mode }) %>')"><%=Constants.CANCEL%></a >
    <% } %>
    <a class="button" href="#" onclick="$('#WindowSummary').data('tWindow').center().open();return false;"><%=string.Format("{0} {1}", Constants.PRINTABLE, Constants.SUMMARY)%></a>

    <% Html.Telerik().Window()
        .Name("WindowSummary")
        .Title(string.Format("{0} {1}", Constants.PRINTABLE, Constants.SUMMARY))
        .Draggable(true)
        .Scrollable(true)
        .Modal(true)
        .Content(() =>
            {%>
            <% Html.RenderPartial("~/Views/Shared/Details/ucProjectSummaryPrint.ascx", Model); %>
            <%})
        .ClientEvents(events => events.OnOpen("setWindowFocus"))
        .Buttons(b => b.Maximize().Close())
        .Width(915)
        .Height(600)
        .Visible(false)
        .Render(); %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/jquery.printElement.min.js")
            .Add("~/Scripts/cara.phasesummary.js"));
%>
</asp:Content>
