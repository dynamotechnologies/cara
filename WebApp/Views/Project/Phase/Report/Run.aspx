﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PhaseReportRunViewModel>" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=6.0.12.215, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%">
<head runat="server">
    <title><%: Model.ReportName %></title>
	<script runat="server">
        
        public override void VerifyRenderingInServerForm(Control control)
        {
            // to avoid the server form (<form runat="server">) requirement
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

			// bind the report viewer
			ReportViewer1.Report = Model.Report;
			ReportViewer1.RefreshReport();

			if (Model.Report is Telerik.Reporting.ReportBook)
			{
				ReportViewer1.DocumentMapVisible = true;
			}
        }
    </script>    
</head>
<body style="height: 100%">
<div style="height: 100%">
  <telerik:ReportViewer runat="server" ID="ReportViewer1" Height="100%" 
	  ShowHistoryButtons="False" ShowParametersButton="False" Width="100%" 
	  ZoomMode="Percent" ZoomPercent="100" ShowZoomSelect="True" />
</div>
</body>
</html>
