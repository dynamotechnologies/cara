﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.PhaseReportViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Html.RenderPartial("~/Views/Shared/List/ucPhaseReportList.ascx", Model); --%>

    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>

<div class="t-widget t-grid" style="width: 700px;">
    <table>
        <tr>
            <td class="t-header">Name</td>
            <td class="t-header">Ordered By</td>
            <td class="t-header">Action</td>
        </tr>
        <tr>
            <td width="50%">Coding Structure</td>
            <td width="40%">Code Number</td>
            <td width="10%" style="text-align: center;">
                <% if (Model.Locked)
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.VIEW + Constants.REPORT, new { id = 1, artifactTypeId = 4 }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                <% }
                   else
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.RUN, new { id = 1 }) %>"><%=Constants.RUN%></a >
                <% } %>
            </td>
        </tr>
        <tr class="t-alt">
            <td>Early Attention Coded Comments</td>
            <td>Code Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.EARLYACTION_REPORT, new { id = 4 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr>
            <td>Comments</td>
            <td>Code Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENT, new { id = 2 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr class="t-alt">
            <td>Responses to Comments</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;In Progress Concerns and Responses</td>
            <td>C/R Sequence Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.RESPONSESTATUS_REPORT, new { id = 7 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Comments, Responses, No Concerns</td>
            <td>Commenter Last Name</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTSRESPONSESBYLETTER_REPORT, new { id = 12}) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
<%--        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Comments, Responses, No Concerns</td>
            <td>C/R Sequence Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTSRESPONSESBYLETTER_REPORT, new { id = 14}) %>"><%=Constants.RUN%></a >
            </td>
        </tr>--%>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Concerns, Responses, No Comments</td>
            <td>C/R Sequence Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.RESPONSETOCOMMENT_REPORT, new { id = 6, option = 1 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Concerns, Responses, Sample Comments</td>
            <td>C/R Sequence Number</td>
            <td style="text-align: center;">
                <% if (Model.Locked)
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.VIEW + Constants.REPORT, new { id = 6, artifactTypeId = 1 }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                <% }
                   else
                   { %>
                <a class="button" target="_self" href="<%= Url.Action(Constants.RESPONSETOCOMMENT_REPORT, new { id = 6, option = 2 }) %>"><%=Constants.RUN%></a >
                <% } %>
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Concerns, Responses, All Comments</td>
            <td>C/R Sequence Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.RESPONSETOCOMMENT_REPORT, new { id = 6, option = 3 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;C/R Sequence Number, Excluding Forms</td>
            <td>Commenter Last Name</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.CONCERNRESPONSEBYCOMMENTER_REPORT, new { id = 13, option = 2 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr class="t-alt">
            <td>Team Members</td>
            <td>Last Name</td>
            <td style="text-align: center;">
                <% if (Model.Locked)
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.VIEW + Constants.REPORT, new { id = 10, artifactTypeId = 3 }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                <% }
                   else
                   { %>
                <a class="button" target="_self" href="<%= Url.Action(Constants.TEAMMEMBER_REPORT, new { id = 10 }) %>"><%=Constants.RUN%></a >
                <% } %>
            </td>
        </tr>
        <tr>
            <td>Mailing List</td>
            <td>Last Name</td>
            <td style="text-align: center;">
                <% if (Model.Locked)
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.VIEW + Constants.REPORT, new { id = 8, artifactTypeId = 2 }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                <% }
                   else
                   { %>
                <a class="button" target="_self" href="<%= Url.Action(Constants.MAILINGLIST_REPORT, new { id = 8 }) %>"><%=Constants.RUN%></a >
                <% } %>
            </td>
        </tr>
        <tr class="t-alt">
            <td>Demographics</td>
            <td>N/A</td>
            <td style="text-align: center;">
                <% if (Model.Locked)
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.VIEW + Constants.REPORT, new { id = 3, artifactTypeId = 5 }) %>"><%=Constants.VIEW + " Archived " + Constants.REPORT%></a >
                <% }
                   else
                   { %>
                <a class="button" target="_blank" href="<%= Url.Action(Constants.RUN, new { id = 3 }) %>"><%=Constants.RUN%></a >
                <% } %>
            </td>
        </tr>
        <tr>
            <td>Untimely Submissions</td>
            <td>Letter Numbers</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.UNTIMELYCOMMENTS_REPORT, new { id = 9 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr class="t-alt">
            <td>Comments with C/R Information</td>
            <td>Code Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTWITHCRINFO_REPORT, new { id = 15 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
        <tr>
            <td>Comments with Annotation</td>
            <td>Letter Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.COMMENTWITHANNOTATION_REPORT, new { id = 16 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
         <tr class="t-alt">
            <td>Concern Response With AssignedTo</td>
            <td>CR Number</td>
            <td style="text-align: center;">
                <a class="button" target="_self" href="<%= Url.Action(Constants.CONCERNRESPONSEWITHASSIGNEDTO_REPORT, new { id = 17 }) %>"><%=Constants.RUN%></a >
            </td>
        </tr>
    </table>
</div>


<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.phasereportlist.js")); %>
</asp:Content>
