﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.GenericStandardReportViewModel<Aquilent.Cara.Domain.Report.ResponseToCommentReportData>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <table>
        <tr>
           <td align="right">
             <a class="button" onclick="exportReport('<%= Url.Action("ExportResponseToComment",  new { id = 6, option = Model.Option } ) %>');">Export</a >
           </td>
        </tr>
        <tr>
            <td>
                <% if (Model.Option == 1)
                   {
                       Html.RenderPartial("~/Views/Shared/List/StandardReports/ucResponseToComment.ascx", Model);
                   }
                   else
                   {
                       Html.RenderPartial("~/Views/Shared/List/StandardReports/ucResponseToCommentWithDetails.ascx", Model);
                   }
                %>
            </td>
        </tr>
     </table>

 <% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                 .Add("~/Scripts/telerik.window.js")
                 .Add("~/Scripts/telerik.window.dialog.js")
                 .Add("~/Scripts/cara.standardreports.js"));
    %>
</asp:Content>
