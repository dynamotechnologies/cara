﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.ConcernTemplateInputViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% if (Model.IsWizardMode)
       {
           Html.RenderAction("WizardNav", Constants.NAV, new { actionName = ViewData["action"], mode = Model.Mode });
       }
       else
       {
           Html.RenderAction("ProjectNav", Constants.NAV);
       } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (!string.IsNullOrEmpty(TempData[Constants.PHASE + Constants.CONCERN_RESPONSE + Constants.TEMPLATE] as string) && !Model.IsWizardMode)
        { %>
        <div id="confirmAction">
            <div class="message" style="width:500px;"><%: TempData[Constants.PHASE + Constants.CONCERN_RESPONSE + Constants.TEMPLATE] as string%></div>
            <br />
        </div>
    <% } %>
    <div class="pageDesc"><%: Html.DisplayFor(model => model.Description)%></div>
    <br />
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary() %>
    <br />
    <% if (Model.IsWizardMode)
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.BACK %>" onclick="submitForm('<%=Constants.BACK %>')"/>
        <input type="button" class="button" name="action" value="<%=Constants.NEXT %>" onclick="submitForm('<%=Constants.NEXT %>')" />
        <input type="button" class="button" name="action" value="<%=Constants.FINISH %>" onclick="submitForm('<%=Constants.FINISH %>')"/>
        <% }
        else if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.SAVE %>" onclick="submitForm('<%=Constants.SAVE %>')" />
    <% } %>
    <br /><br />
    <!-- progress indicator div: must be placed before the coding window -->
<%--    <div id="loading" style="display:none;">
        <div class="centered">Please wait...</div>
    </div>--%>
    <% Html.RenderPartial("~/Views/Shared/Input/ucConcernResponseTemplateInput.ascx", Model); %>
    <br />
    
    <% if (Model.IsWizardMode)
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.BACK %>" onclick="submitForm('<%=Constants.BACK %>')"/>
        <input type="button" class="button" name="action" value="<%=Constants.NEXT %>" onclick="submitForm('<%=Constants.NEXT %>')" />
        <input type="button" class="button" name="action" value="<%=Constants.FINISH %>" onclick="submitForm('<%=Constants.FINISH %>')"/>
        <% }
        else if (Utilities.UserHasPrivilege("MCDS", Constants.PHASE))
        { %>
        <input type="button" class="button" name="action" value="<%=Constants.SAVE %>" onclick="submitForm('<%=Constants.SAVE %>')" />
    <% } %>
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Description) %>
    <%: Html.HiddenFor(m => m.Mode) %>
    <%: Html.HiddenFor(m => m.ProjectId)%>
    <%: Html.HiddenFor(m => m.PhaseId)%>
    <%: Html.HiddenFor(m => m.ActionString) %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/jquery.cookie.js")
            .Add("~/Scripts/cara.concernresponsetemplateinput.js")
            ); %>
</asp:Content>