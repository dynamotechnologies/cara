﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterInputViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm(Constants.EDIT, Constants.LETTER, FormMethod.Post, new { enctype = "multipart/form-data" }))
   { %>
    <%: Html.ValidationSummary() %>
    <div class="fieldLabel">Attachments</div>
    <% Html.RenderPartial("~/Views/Shared/List/ucDocumentList.ascx", new DocumentViewModel
       {
           Documents = Model.Letter.Documents,
          // ShowButtons = false,//!Model.Letter.Phase.Locked
           ShowAddContentButton = false,//!Model.Letter.Phase.Locked,
           ShowDeleteButton = !Model.Phase.Locked,
           LetterId = Model.Letter.LetterId,
           ShowActions = true
       }); %>
    <br />
    <div class="fieldLabel">Attachments</div>
     <% Html.RenderPartial("~/Views/Shared/Input/ucFileUpload.ascx", new Dictionary<string, object> { { "ShowDocumentName", false }, { "MaxUploadSize", Model.MaxUploadSize } }); %>
    <br />    
    <div class="fieldLabel">Authors</div>
    <% Html.RenderPartial("~/Views/Shared/List/ucAuthorList.ascx", new AuthorListViewModel
       {
           Authors = Model.Letter.Authors,
           Mode = Model.Mode
       }); %>
    <br />
    <% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
       { %>
        <%: Html.ActionLink(string.Format("{0} {1}", Constants.ADD, Constants.AUTHOR), Constants.CREATE, Constants.AUTHOR,
            new { letterId = Model.Letter.LetterId, id = 0 }, new { @class = "button" })%>
    <% } %>
    <br /><br />
    <% Html.RenderPartial("~/Views/Shared/Input/ucLetterEdit.ascx", Model); %>
    <br /><br />
    <% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
       { %>
        <input type="button" class="button" name="action" value="Save" onclick="updateLetter()" />
    <% } %>
    <%: Html.ActionLink(Constants.CANCEL, Constants.CODING, new { id = Model.Letter.LetterId, nextLetterIds = Model.NextLetterIds, lettersRemaining = Model.LettersRemaining }, new { @class = "button" })%>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.ProjectId) %>
    <%: Html.HiddenFor(m => m.PhaseId) %>
    <%: Html.HiddenFor(m => m.RepostToDmd) %>
    <%: Html.HiddenFor(m => m.NextLetterIds)%>
    <%: Html.HiddenFor(m => m.LettersRemaining)%>

<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/tinymce/tinymce.js")
            .Add("~/Scripts/util.tinymce.js")
            .Add("~/Scripts/cara.letteredit.js"));
%>
</asp:Content>