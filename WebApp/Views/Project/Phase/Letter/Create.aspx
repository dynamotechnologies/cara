﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterInputViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm(Constants.CREATE, Constants.LETTER, FormMethod.Post, new { enctype = "multipart/form-data" }))
   { %>
    <div style="width:775px;"><%: Html.ValidationSummary() %></div>
    <% Html.RenderPartial("~/Views/Shared/Details/ucProjectPhaseUnitDetails.ascx", Model.Phase); %>
    <br />
    <% Html.RenderPartial("~/Views/Shared/Input/ucLetterInput.ascx", Model); %>
    <br /><br />
    <% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
       { %>
        <input type="button" class="button" value="Submit & Enter Another Letter" onclick="createLetter('Submit & Enter Another Letter')" />
        <input type="button" class="button" value="Submit & Go To Letter Inbox" onclick="createLetter('Submit & Go To Letter Inbox')" />
        <input type="button" class="button" value="Submit & Go To Letter Coding Page" onclick="createLetter('Submit & Go To Letter Coding Page')" />
    <% } %>
    <%: Html.ActionLink(Constants.CANCEL, Constants.LIST, null, new { @class = "button" })%>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.PhaseId) %>
    <%: Html.Hidden("action") %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/tinymce/tinymce.js")
            .Add("~/Scripts/util.tinymce.js")
            .Add("~/Scripts/cara.lettercreate.js"));
%>
</asp:Content>