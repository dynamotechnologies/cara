﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterCodingViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="NotificationContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<% bool showError = !string.IsNullOrEmpty(TempData[Constants.ERROR] as string);%>
<div <%=showError ? "" : "hidden" %> >
    <div class="error"><b><%: TempData[Constants.ERROR] as string%></b></div>
    <br />
</div>

<% bool showConfirm = !string.IsNullOrEmpty(TempData[Constants.CODING] as string);%>
<div id="confirmAction" <%=showConfirm?"":"hidden" %> >
    <div class="message" style="width:500px;"><%: TempData[Constants.CODING] as string%></div>
    <br />
</div>

<div class="letterCodingActionButtons">
    <% if (Model.Editable) { %>&nbsp;<%: Html.ActionLink(Constants.EDIT, Constants.EDIT, new { id = Model.LetterId, nextLetterNums = Model.NextLetterIds, numberOfLettersRemaining = Model.LettersRemaining }, new { @class = "button" })%><% } %>
	&nbsp;<a class="button" id="printLetter" href="javascript:void(0);">Print</a>&nbsp;<a class="button" id="printLetterComment" href="javascript:void(0);" >Print with Comment</a>
    <% if (Model.Deletable) { %>&nbsp;<%: Html.ActionLink(Constants.DELETE, Constants.DELETE, new { id = Model.LetterId }, new { @class = "button", id = "btnDeleteLetter", onclick = "return confirmWindow(\"Are you sure you want to permanently delete this letter\", \"deleteLetter()\")" })%><% } %>
</div>


    <% Html.Telerik().TabStrip()
        .Name("Section")
        .Items(tabstrip =>
        {
            tabstrip.Add()
                .Text("Coding")
                .Content(() =>
                {%>
                    <div id="attachments">
                        <span class="fieldLabel">&nbsp;&nbsp;Attachments</span>
                        <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Appending Text"
                         title="Appending Text Support Content: To make the text of an attachment available for coding: 1) open the attachment, 2) (Optional) Covert PDF (.pdf) files to Word (.doc or .docx) 3) Copy the text you wish to code, 4) press the “edit” button on the letter coding page 5) Paste the copied text in the “Append Text Box.” Images and formatting will not copy." />
                        <% Html.RenderPartial("~/Views/Shared/List/ucDocumentList.ascx", new DocumentViewModel
                           {
                               Documents = Model.Documents,
                               // ShowButtons = false,//!Model.Letter.Phase.Locked
                               ShowAddContentButton = !Model.Letter.Phase.Locked,
                               ShowDeleteButton = false,
                               LetterId = Model.LetterId,
                               ShowActions = false
                           }); %>
                    </div>

                    <% Html.RenderPartial("~/Views/Shared/Input/ucLetterCoding.ascx", Model); %>
              <%}).Selected(Model.TabName == "Coding");
            
            tabstrip.Add()
                .Text("Details")
                .Content(() =>
                {%>
                    <% Html.RenderPartial("~/Views/Shared/Details/ucLetterDetails.ascx", Model); %>
              <%}).Selected(Model.TabName == "Details");
                
            tabstrip.Add()
                .Text("Summary")
                .Content(() =>
                {
                    if (Model.ObjectionInfo != null)
                    {%>
                        <% Html.RenderPartial("~/Views/Shared/Details/ucLetterObjectionSummaryDetails.ascx", Model); %>
                    <%}
                    else
                    {%>
                        <br />
                    <%}
                })
                .Visible(Model.IsObjection && Model.Letter.LetterObjectionId.HasValue)
                .Selected(Model.TabName == "Summary");
                
        }).Render();
    %>
    <%  Html.Telerik().Window()
            .Name("WindowCompare")
            .Title("Compare to Master Letter")
            .Draggable(true)
            .Modal(true)
            .Resizable(resizing => resizing
                    .Enabled(false)
                )
            .Buttons(b => b.Close())
            .Width(925)
            .Height(525)
            .Visible(false)
            .Render(); %>
        
     <% Html.Telerik().Window()
            .Name("WindowConcern")
            .Title("Respond to Comment")
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(false)
                        )
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Maximize().Close())
            .Width(625)
            .Height(275)
            .Visible(false)
            .Render(); %>

     <% Html.Telerik().Window()
            .Name("WindowCommentCode")
            .Title("Reorder Comment Codes")
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(true)
                        )
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Maximize().Close())
            .Width(625)
            .Height(375)
            .Visible(false)
            .Render(); %>

     <% Html.Telerik().Window()
            .Name("WindowLinkText")
            .Title("Link Text to Comment")
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(true)
                            .MaxWidth(700)
                            .MaxHeight(700)
                        )
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Maximize().Close())
            .Width(600)
            .Height(400)
            .Visible(false)
            .Render(); %>

     <% Html.Telerik().Window()
            .Name("WindowTermList")
            .Title(string.Format("{0} {1}s", Constants.SELECT, Utilities.GetResourceString(Constants.TERM_LIST)))
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(true)
                            .MaxWidth(600)
                            .MaxHeight(300)
                        )
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Close())
            .Width(600)
            .Height(225)
            .Visible(false)
            .Render(); %>

    <% Html.Telerik().Window()
        .Name("WindowCodeFilter")
        .Title(string.Format("{0} {1}", Constants.SEARCH, Constants.CODE + "s"))
        .Draggable(true)
        .Modal(true)
        .ClientEvents(events => events.OnOpen("setWindowFocus"))
        .Buttons(b => b.Close())
        .Width(700)
        .Height(325)
        .Visible(false)
        .Render(); %>

    <% Html.Telerik().Window()
            .Name("WindowObjectionResponse")
            .Title(string.Format("{0} {1}", Constants.EDIT, "Objection Response"))
            .Draggable(true)
            .Resizable(resizing => resizing
                            .Enabled(false)
                        )
            .Modal(true)
            .Buttons(b => b.Maximize().Close())
            .Width(700)
            .Height(325)
            .Visible(false)
            .Render(); %>


    <% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/telerik.list.min.js")
                .Add("~/Scripts/telerik.combobox.min.js")
                .Add("~/Scripts/telerik.window.min.js")
                .Add("~/Scripts/telerik.window.dialog.js")
                .Add("~/Scripts/diffplex.diffviewer.js")
                .Add("~/Scripts/jquery.treeview.js")
                .Add("~/Scripts/jquery.textselect.js")
                .Add("~/Scripts/jquery.switchclass.js")
                .Add("~/Scripts/jquery.printElement.min.js")
                .Add("~/Scripts/tinymce/tinymce.js")
                .Add("~/Scripts/util.tinymce.js")
                .Add("~/Scripts/cara.form.util.js")
                .Add("~/Scripts/cara.lettertext.js")
                .Add("~/Scripts/cara.lettercoding.js")
                .Add("~/Scripts/cara.standardreports.js")
                .Add("~/Scripts/telerik.editor.min.js")
                .Add("~/Scripts/telerik.calendar.min.js")
                .Add("~/Scripts/telerik.datepicker.min.js")
                .Add("~/Scripts/cara.fileupload.js")
                .Add("~/Scripts/util.confirmmessage.js")
                );
    %>
    <div style="height:425px;"></div>
</asp:Content>