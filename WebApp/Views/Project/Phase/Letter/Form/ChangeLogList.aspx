﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.ChangeLogListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm())
   { %>
    <%: Html.ActionLink(Utilities.GetPageTitle(Constants.FORM + Constants.LIST), Constants.LIST, Constants.FORM, null, new { @class = "button" })%>
    <br /><br />
    <% Html.RenderPartial("~/Views/Shared/List/ucFormChangeLogList.ascx", Model); %>
    <br />
    <%: Html.ActionLink(Utilities.GetPageTitle(Constants.FORM + Constants.LIST), Constants.LIST, Constants.FORM, null, new { @class = "button" })%>
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.ProjectId)%>
    <%: Html.HiddenFor(m => m.PhaseId)%>
    <%: Html.HiddenFor(m => m.CanEdit)%>
<% } %>
</asp:Content>