﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.FormSetListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% bool showConfirm = !string.IsNullOrEmpty(TempData[Constants.FORM + Constants.LETTER + Constants.LIST] as string);%>
    <div id="confirmAction" <%=showConfirm?"":"hidden" %> >
        <div class="message" style="width:500px;"><%: TempData[Constants.FORM + Constants.LETTER + Constants.LIST] as string%></div>
        <br />
    </div>
<% using (Html.BeginForm())
   { %>
        <% Html.RenderPartial("~/Views/Shared/Filter/ucFormLetterSearchResultFilter.ascx", Model); %>
        <br />
        <%: Html.ActionLink(string.Format("{0} {1}", Constants.VIEW, Utilities.GetResourceString(Constants.CHANGE_LOG)),
                                Constants.CHANGE_LOG + Constants.LIST, Constants.FORM, null, new { @class = "button" })%>
        <br /><br />
        <div id="formSetListArea">
            <% Html.RenderPartial("~/Views/Shared/List/ucFormLetterList.ascx", Model); %>
        </div>
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.CanEdit)%>
    <%: Html.HiddenFor(m => m.ProjectId)%>
    <%: Html.HiddenFor(m => m.PhaseId)%>
    <%: Html.HiddenFor(m=>m.FormSetId) %>
    <%: Html.HiddenFor(m => m.RequestUri)%>
<% } %>
<br />
<% if (Model.CanEdit && Utilities.UserHasPrivilege("RSFS", Constants.PHASE))
   { %>
    <hr /><br />
    <% Html.RenderPartial("~/Views/Shared/List/ucFormSetCompareSelect.ascx", Model); %>
    <br />
<% } %>
<br />
<%: Html.ActionLink(string.Format("{0} {1}", Constants.VIEW, Utilities.GetResourceString(Constants.CHANGE_LOG)),
                        Constants.CHANGE_LOG + Constants.LIST, Constants.FORM, null, new { @class = "button" })%>

<%  Html.Telerik().Window()
        .Name("WindowCompare")
        .Title(string.Format("Compare to Master {0}", Utilities.GetResourceString(Constants.LETTER)))
        .Draggable(true)
        .Modal(true)
        .Resizable(resizing => resizing
                .Enabled(false)
            )
        .Buttons(b => b.Close())
        .Width(925)
        .Height(525)
        .Visible(false)
        .Render(); %>

<% Html.Telerik().Window()
            .Name("WindowSelectFormSet")
            .Title(string.Format("{0} {1} to {2}", Constants.SEND, Constants.LETTER, Utilities.GetResourceString(Constants.FORM_SET)))
            .Draggable(true)
            .Scrollable(true)
            .Resizable(resizing => resizing
                            .Enabled(true)
                            .MaxWidth(700)
                            .MaxHeight(700)
                        )
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Maximize().Close())
            .Width(600)
            .Height(400)
            .Visible(false)
            .Render(); %>

<% Html.Telerik().Window()
            .Name("WindowEditFormSet")
            .Title(string.Format("{0} {1}", Constants.RENAME, Utilities.GetResourceString(Constants.FORM_SET)))
            .Draggable(true)
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Maximize().Close())
            .Width(400)
            .Height(125)
            .Visible(false)
            .Render(); %>

<% Html.Telerik().Window()
            .Name("WindowCreateFormSet")
            .Title(string.Format("{0} New {1}", Constants.CREATE,Utilities.GetResourceString(Constants.FORM_SET)))
            .Draggable(true)
            .Modal(true)
            .ClientEvents(events => events.OnOpen("setWindowFocus"))
            .Buttons(b => b.Maximize().Close())
            .Width(400)
            .Height(125)
            .Visible(false)
            .Render(); %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/diffplex.diffviewer.js")
            .Add("~/Scripts/cara.form.util.js")
            .Add("~/Scripts/cara.formsetlist.js"));
%>
</asp:Content>