﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm())
   { %>
        <% Html.RenderPartial("~/Views/Shared/Filter/ucLetterFilter.ascx", Model); %>
    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.CanEdit)%>
<% } %>
        <br />
        <% Html.RenderPartial("~/Views/Shared/List/ucLetterSearchList.ascx", Model); %>
        <br />

<% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE) && Model.CanEdit)
    { %>
    <%: Html.ActionLink(Utilities.GetPageTitle(Constants.LETTER + Constants.CREATE), Constants.CREATE, Constants.LETTER, null, new { @class = "button" })%>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
             .Add("~/Scripts/cara.letterlist.js"));
%>
</asp:Content>