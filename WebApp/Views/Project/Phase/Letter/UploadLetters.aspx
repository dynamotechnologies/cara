﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterUploadViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
<% using (Html.BeginForm(Constants.UPLOAD + Constants.LETTERS, Constants.LETTER, FormMethod.Post, new { enctype = "multipart/form-data" }))
   { %>
    <%: Html.ValidationSummary()%>
    <% if (ViewData[Constants.CONFIRM] != null)
       { %>
       <span class="message"><%: ViewData[Constants.CONFIRM]%></span><br /><br />
    <% } %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucLetterUpload.ascx", Model); %>
    <br />
    <div id="progressbar"></div>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
<% } %>
<br />
<% if (Model.Letters != null)
   { %>
    <div class="fieldLabel"><%:Utilities.GetResourceString(Constants.LETTER).Pluralize()%> Uploaded:</div>
    <br />
    <% Html.RenderPartial("~/Views/Shared/List/ucLetterList.ascx", Model.Letters); %>
<% } %>
    <% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/cara.letterupload.js"));
    %>
</asp:Content>