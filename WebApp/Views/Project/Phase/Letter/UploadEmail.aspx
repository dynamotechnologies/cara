﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.LetterUploadViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="HelpContent" runat="server">
    <% Html.RenderPartial("ucHelpButton", new HelpViewModel { Name = Constants.EMAIL + Constants.UPLOAD, Mode = PageMode.None,
                                                              Title = Model.PageTitle
       }); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="text" style="width:700px;">
    <h2> Outlook email upload instructions </h2>
    <p>Emails to be uploaded to CARA should not be forwarded versions (or else CARA will not be able to extract correct contact information).</p>  
    <ol>
        <li>Move the emails you want to upload to an <u>empty</u> Outlook folder. (By moving emails from the inbox, you will know that any new or remaining emails in the inbox have not been uploaded.)</li>
        <li>In Outlook click on <b>File</b>, then on <b>Options</b>, then on <b>Advanced</b>, then on <b>Export</b></li>
        <li>Select <b>Export to a file;</b> click <b>Next</b></li>
        <li>Select <b>Outlook Data File (.pst)</b>; click <b>Next</b>. This will display your Outlook folders.</li>
        <li>Select the folder containing the emails to be uploaded. All emails in this folder will go into the .pst file.  </li>
        <li>Click <b>Browse</b> to select a convenient location to save the .pst file </li>
        <li>Make sure the radio button for “Replace duplicates with items exported” is selected and click <b>Finish</b></li>
        <li>If a Password dialog box pops up, just click <b>“Ok”</b> (do not enter a password).</li>
    </ol>
    </div>

    <br />
<% using (Html.BeginForm("UploadEmail", Constants.LETTER, FormMethod.Post, new { enctype = "multipart/form-data" }))
   { %>
    <%: Html.ValidationSummary()%>
    <% if (ViewData[Constants.CONFIRM] != null)
       { %>
       <span class="message"><%: ViewData[Constants.CONFIRM]%></span><br /><br />
    <% } %>
    <% Html.RenderPartial("~/Views/Shared/Input/ucLetterUpload.ascx", Model); %>
    <br /><br />
    <div class="text" style="width:700px;">
    <ul>
        <li>Maximum size for uploaded .pst files is approximately 50 MB, depending on internet speed.  Processing large .pst files can take some time, so be patient.  If your upload does not complete successfully, close Outlook and try again.  Failing that, try a smaller .pst file.</li>
        <li>Once the upload begins, <b>DO NOT NAVIGATE AWAY FROM THIS PAGE</b>. However, you may work on other tasks on your computer while CARA processes your letters. </li>
    </ul>
    </div>
    <br />
    <br />
    <% if (Utilities.UserHasPrivilege("MLTR", Constants.PHASE))
       { %>
        <a class="button" id="btnSubmit" href="#" onclick="submitForm()"><%=Constants.UPLOAD %></a>
    <% } %>
    <div id="progressbar"></div>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
<% } %>
<br />
<% if (Model.Letters != null)
   { %>
    <div class="fieldLabel"><%:Utilities.GetResourceString(Constants.LETTER).Pluralize() %> Uploaded:</div>
    <br />
    <% Html.RenderPartial("~/Views/Shared/List/ucLetterList.ascx", Model.Letters); %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.letterupload.js"));
%>
</asp:Content>
