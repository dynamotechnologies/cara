﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.TermListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% if (Model.IsWizardMode)
       {
           Html.RenderAction("WizardNav", Constants.NAV, new { actionName = ViewData["action"], mode = Model.Mode });
       }
       else
       {
           Html.RenderAction("ProjectNav", Constants.NAV);
       } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (!string.IsNullOrEmpty(TempData[Constants.PHASE + Constants.TERM_LIST] as string) && !Model.IsWizardMode)
        { %>
        <div id="confirmAction">
            <div class="message" style="width:500px;"><%: TempData[Constants.PHASE + Constants.TERM_LIST] as string%></div>
            <br />
        </div>
    <% } %>
    <div class="pageDesc"><%: Html.DisplayFor(model => model.Description)%></div>
    <br />
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary() %>
    <% Html.RenderPartial("~/Views/Shared/List/ucTermListSelect.ascx", Model.TermLists); %>
    <br />
    <% if (Model.IsWizardMode)
        { %>
        <input type="submit" class="button" name="action" value="Back" />
        <input id="btnFinish" type="submit" class="button" name="action" value="Finish" />
        <% }
        else if (Utilities.UserHasPrivilege("MPHS", Constants.PHASE))
        { %>
        <input type="submit" class="button" name="action" value="Save" />
    <% } %>

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.Description) %>
    <%: Html.HiddenFor(m => m.Mode) %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.termlistinput.js"));
%>
</asp:Content>
