﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.CommentListViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% if (!string.IsNullOrEmpty(TempData[Constants.PHASE + Constants.COMMENT] as string))
    { %>
    <div id="confirmAction">
        <div class="message" style="width:500px;"><%: TempData[Constants.PHASE + Constants.COMMENT] as string%></div>
        <br />
    </div>
<% } %>
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary() %>
    <% Html.RenderPartial("~/Views/Shared/Filter/ucCommentFilter.ascx", Model); %>
    <br />
        <% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && Model.CanEdit)
        {
            string submitValue = string.Format("{0} {1}", Constants.EDIT.Equals(Request.Params[Constants.MODE]) ? "Add To" :
                Constants.CREATE, Utilities.GetResourceString("Concern"));
        %>
        <input type="button" class="button" name="action" value="<%= submitValue %>" onclick="confirmSubmit('<%=submitValue %>')") />
        <% if (Constants.EDIT.Equals(Request.Params[Constants.MODE]))
            { %>
            <%: Html.ActionLink(Constants.CANCEL, Constants.INPUT, Constants.CONCERN, new { id = Request.Params[Constants.CONCERN_RESPONSE_ID] }, new { @class = "button" })%>
        <% } %>
        <input type="submit" id="SaveTasks1" class="button" name="action" value="Save Task Assignments"/>
    <% } %>
    <br /><br />
    <% Html.RenderPartial("~/Views/Shared/List/ucCommentSelect.ascx", Model); %>
    <br />
    <% if (Utilities.UserHasPrivilege("WRSP", Constants.PHASE) && Model.CanEdit)
       {
           string submitValue = string.Format("{0} {1}", Constants.EDIT.Equals(Request.Params[Constants.MODE]) ? "Add To" :
               Constants.CREATE, Utilities.GetResourceString("Concern"));%>
        <input type="button" class="button" name="action" value="<%= submitValue %>" onclick="confirmSubmit('<%=submitValue %>')") />
        <% if (Constants.EDIT.Equals(Request.Params[Constants.MODE]))
            { %>
            <%: Html.ActionLink(Constants.CANCEL, Constants.INPUT, Constants.CONCERN, new { id = Request.Params[Constants.CONCERN_RESPONSE_ID] }, new { @class = "button" })%>
        <% } %>
        <input type="submit" id="SaveTasks2" class="button" name="action" value="Save Task Assignments"/>
    <% } %>

    <!-- Hidden fields for the model -->
    <%: Html.Hidden("Dirty", "False", new { @id = "Dirty" })%>
    <%: Html.HiddenFor(m => m.ActionString) %>
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
    <%: Html.HiddenFor(m => m.CanEdit) %>
    <%: Html.HiddenFor(m => m.ProjectId) %>
    <%: Html.HiddenFor(m => m.PhaseId) %>
<% } %>

<% Html.Telerik()
            .ScriptRegistrar()
            .Scripts(script => script
                .Add("~/Scripts/telerik.window.dialog.js")
                .Add("~/Scripts/cara.commentlist.js")
                .Add("~/Scripts/cara.taskassignmentutil.js"));
    %>
</asp:Content>
