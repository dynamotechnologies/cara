﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.CommentDetailsViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
    <% Html.RenderAction("ProjectNav", Constants.NAV); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Details/ucCommentDetails.ascx", Model.Comment); %>
    <br />
    <% if (!Model.Comment.Letter.Phase.Locked && Utilities.UserHasPrivilege("CODE", Constants.PHASE))
       { %>
        <a class="button" href="#" onclick="confirmWindow('Comment will be deleted. Do you wish to continue', 'deleteComment(<%=Model.Comment.Letter.Phase.ProjectId%>,<%=Model.Comment.Letter.PhaseId%>,<%=Model.Comment.CommentId%>)')"><%=Constants.DELETE%></a >
    <% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.commentdetails.js"));
%>
</asp:Content>
