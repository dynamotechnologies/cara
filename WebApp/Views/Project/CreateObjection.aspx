﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.ObjectionInputViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="pageDesc">
    This page provides a shortcut method for creating a new Objection filing period on a PALS project.
    The following actions will occur once you select the appropriate project:
    <ul>
        <li>Add the project to CARA if it does not already exist in CARA</li>
        <li>Set the start date based on the Estimated or Actual Objection Period Legal notice date</li>
        <li>Set the end date based on the project's combination of notice and comment regulation</li>
        <li>Assign you as a team member</li>
        <li>Assign the Project Manager, Secondary Manager, Data Entry Person, and Point of Contact as team members</li>
        <li>Associate the Objection coding structure template for use on objection letters received. Using CARA to parse, code, and respond to objection issues is optional</li>
    </ul>
    When the objection period is created, you will be taken to the home page for the objection period, but note that once it is created, CARA will refer to it as a comment period. You will have an opportunity to modify any of the automated settings.    
</div>
<br />
<% using (Html.BeginForm())
   { %>
    <%: Html.ValidationSummary(string.Empty, new { id = "validationSummary" })%>
    <table>
        <tr>
            <td class="fieldLabel"><span class="required">*</span><label for="ProjectNameOrId">Enter a <%=Utilities.GetResourceString("PALS")%> ID or <%=Constants.PROJECT%> Name:</label></td>
            <td class="text"><%: Html.TextBoxFor(model => model.ProjectNameOrId)%>
                <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" alt="Keyword search" title="Search by PALS project ID or name." />
                &nbsp;<input type="submit" id="btnSearchProjects" class="button" name="action" value="Search Projects" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <% if (Model.DataMartProjectViewModel != null && Model.DataMartProjectViewModel.Projects != null)
                   {
                       Html.RenderPartial("~/Views/Shared/List/ucDataMartProjectSelect.ascx", Model.DataMartProjectViewModel);
                   } %>
            </td>
        </tr>
    </table>

    <br />
    <input type="submit" class="button" name="action" value="<%=Constants.CREATE %> Objection Period" />
    <input type="submit" class="button" name="action" value="<%=Constants.CANCEL %>" />

    <!-- hidden fields for page variables -->
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
<% } %>

<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/telerik.window.dialog.js"));
%>
</asp:Content>
