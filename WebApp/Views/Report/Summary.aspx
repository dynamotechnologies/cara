﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="HelpContent" runat="server">
    <% Html.RenderPartial("ucHelpButton", new HelpViewModel { Name = Constants.REPORT, Mode = PageMode.None,
                                                              Title = Model.PageTitle}); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%: Html.ValidationSummary() %> 
<%: Html.DisplayFor(m => m.Description) %>
<table>
<tr>
<td valign="top">
<table>
    <tr>
        <td valign="top" class="fieldLabel">Project Filter values</td>
        <td valign="top">
            <% List<SelectListItem> projectList;
               if (Model.ProjectOption == "CommentPeriod")
               { projectList = Model.SelectListFieldValues(CustomReportFieldType.CommentPeriod).ToList(); }
               else
               { projectList = Model.SelectListFieldValues(CustomReportFieldType.Project).ToList(); }
               bool first = true;
               foreach (SelectListItem si in projectList)
               {
                   if (!first && !si.Text.StartsWith("--"))
                   {%><br /><%}
                   else { first = false; }
                       
                   %><%=si.Text%><br /><%
               }
               if (projectList.Count() == 0)
               {
                   %>None<%
               }%>
        </td>
    </tr>
    <tr>
        <td valign="top" class="fieldLabel">Submitter Filter values</td>
        <td valign="top">
            <%first = true;
          foreach (SelectListItem si in Model.SelectListFieldValues(CustomReportFieldType.Submitter))
               {
                   if (!first && !si.Text.StartsWith("--"))
                   {%><br /><%}
                   else { first = false; }
                       
                   %><%=si.Text%><br /><%
               }
            if (Model.SelectListFieldValues(CustomReportFieldType.Submitter).Count() == 0)
            {
                %>None<%
            }%>
    </tr>
    <tr>
        <td valign="top" class="fieldLabel">Letter Filter values</td>
        <td valign="top">
            <%first = true;
          foreach (SelectListItem si in Model.SelectListFieldValues(CustomReportFieldType.Letter))
               {
                   if (!first && !si.Text.StartsWith("--"))
                   {%><br /><%}
                   else { first = false; }
                       
                   %><%=si.Text%><br /><%
               }
               if (Model.SelectListFieldValues(CustomReportFieldType.Letter).Count() == 0)
            {
                %>None<%
            }%>
    </tr>
    <tr>
        <td  valign="top" class="fieldLabel">Columns</td>
        <td valign="top" style="width:300px">
            <% foreach (SelectListItem si in Model.SelectListChosenColumns())
               {
                   %><%=si.Text%><br /><%
               }
                if (Model.SelectListChosenColumns().Count() == 0)
                {
                    %>None<%
                } 
               %>
        </td>
    </tr>
</table>
</td>
<td valign="top"><% Html.RenderPartial("~/Views/Shared/List/ucCustomReportParameterList.ascx", Model);%></td>
</tr>
</table>
<form action="/Report2/TrafficDirector" method="post">
    <br />
    <% string formatString = "<input type=\"button\" class=\"button\" name=\"action\" value=\"{0}\" onclick=\"ChangeStep({1})\" />"; %>
    <%=String.Format(formatString, "Back", Model.WizardStep -1 )%>
    <input type="submit" class="button" name="action" value="<%=Constants.FINISH %>" />
    <%:Html.ActionLink(Constants.CANCEL, Constants.INDEX, "Report2", new { @class = "button" })%>
    <br />
    <%: Html.HiddenFor(m=>m.ProjectOption) %>
    <%: Html.HiddenFor(m => m.ProjectId) %>
    <%: Html.HiddenFor(m=> m.ProjectSearchString) %>
    <%: Html.HiddenFor(m => m.BreadCrumbTitle)%>
    <%: Html.HiddenFor(m => m.Description)%>
    <%: Html.HiddenFor(m => m.PageTitle)%>
    <%: Html.HiddenFor(m => m.WizardStep) %>
    <%: Html.HiddenFor(m=>m.ReportId) %>

</form>
 <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.customreportwizard1.js"));%>
</asp:Content>