﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<form action="/Report/TrafficDirector" method="post" id="ConfigForm">
<%: Html.ValidationSummary() %> 
<table>
    <tr>
        <td valign="top" style="width: 400px">
            <table>
                <tr><td style="padding-top:0px">Generate Custom Reports by the Search Parameters below:</td></tr>
                <tr><td class="customReportSectionHeader">Filter by Project</td></tr>
                <tr><td><label for="ProjectFieldId" class="fieldLabel">Select Search Filter:</label></td></tr>
                <tr><td><%= Html.Telerik().DropDownListFor(x=>x.ProjectFieldId)
                            .BindTo(Model.SelectListFields("Project"))
                            .ClientEvents(x => x.OnChange("ProjectFieldSelect"))
                            .Name("ProjectFieldId")
                            .HtmlAttributes(new { style = "width: 200px;" })%></td></tr>
                <tr>
                    <td class="customReportValueSectionSmall">
                        <div id="ProjectTextFieldDiv" 
                            <%=(Model.GetFieldElement("Project").DataType == CustomReportFieldManager.FieldDataTypes.Text ||
                            Model.GetFieldElement("Project").DataType == CustomReportFieldManager.FieldDataTypes.Number)?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px">
                            <tr><td class="customReportValueTable">
                            <label for="ProjectStringFieldValue" id="ProjectStringFieldLabel" class="fieldLabel">
                                <%=Model.GetFieldElement("Project").Name %>:
                            </label>
                            </td></tr>
                            <tr><td class="customReportValueTable">
                            <%:Html.TextBox("ProjectStringFieldValue", "", new { Class = "text", onkeypress = "TextFieldKeyPress(Project)" })%>
                            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="ProjectTextFieldTool" 
                                title="<%=Model.GetFieldElement("Project").ToolTip %>" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Include" onclick="AddStringValue('Project', 'true')" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Exclude" onclick="AddStringValue('Project', 'false')" />
                            </td></tr>
                            </table>
                        </div>

                        <div id="ProjectDropdownFieldDiv" 
                            <%=Model.GetFieldElement("Project").DataType == CustomReportFieldManager.FieldDataTypes.Dropdown?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px">
                            <tr><td class="customReportValueTable">
                            <label for="ProjectDropdownFieldValue" id="ProjectDropdownFieldLabel" class="fieldLabel">
                                <%=Model.GetFieldElement("Project").Name %>:
                            </label>
                            </td></tr>
                            <tr><td class="customReportValueTable">
                            <%= Html.Telerik().DropDownList()
                                .BindTo(Model.SelectListFieldValues("Project"))
                                .Name("ProjectDropdownFieldValue")
                                .HtmlAttributes(new { style = "width: 200px;" }) %>
                            </td>
                            <td>
                            <input type="button" class="button" value="Include" onclick="AddDropdownValue('Project', 'true')" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Exclude" onclick="AddDropdownValue('Project', 'false')" /> 
                            </td></tr>
                            </table>
                        </div>

                        <div id="ProjectUnitDiv" 
                            <%=Model.GetFieldElement("Project").DataType == CustomReportFieldManager.FieldDataTypes.Unit?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px"><tr><td class="customReportValueTable">
                            <% Html.RenderPartial("~/Views/Shared/Util/ucUnitTreeSelector.ascx"
                               , new UnitTreeSelectorModel
                               {
                                   Units = Model.Units,
                                   Visible = false,
                                   Closeable = true,
                                   Modal = true,
                                   UnitIdInputId = "FieldValue",
                                   UnitNameInputId = "FieldValueName"
                               }); %>
                            <input type="hidden" id="UnitId"/>
                            <input type="hidden" id="UnitName" />
                            <a id="btnChooseUnit" class="button" onclick="ShowUnitDialog()">Choose Unit</a>
                            </td></tr></table>
                        </div>
                    </td>
                </tr>
                
                <tr><td class="customReportSectionHeader">Filter by Commenter</td></tr>
                <tr><td><label for="CommenterFieldId" class="fieldLabel">Select Search Filter:</label></td></tr>
                <tr><td><%= Html.Telerik().DropDownListFor(x => x.CommenterFieldId)
                            .BindTo(Model.SelectListFields("Commenter"))
                            .ClientEvents(x => x.OnChange("CommenterFieldSelect"))
                            .Name("CommenterFieldId")
                            .HtmlAttributes(new { style = "width: 200px;" })%></td></tr>
                <tr>
                    <td class="customReportValueSectionSmall">
                        <div id="CommenterTextFieldDiv" 
                            <%=(Model.GetFieldElement("Commenter").DataType == CustomReportFieldManager.FieldDataTypes.Text ||
                            Model.GetFieldElement("Commenter").DataType == CustomReportFieldManager.FieldDataTypes.Number)?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px"><tr><td class="customReportValueTable">
                            <label for="CommenterStringFieldValue" id="CommenterStringFieldLabel" class="fieldLabel">
                                <%=Model.GetFieldElement("Commenter").Name %>:
                            </label>
                            </td></tr>
                            <tr><td class="customReportValueTable">
                            <%:Html.TextBox("CommenterStringFieldValue", "", new { Class = "text", onkeypress = "TextFieldKeyPress(Commenter)" })%>
                            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="CommenterTextFieldTool" 
                                title="<%=Model.GetFieldElement("Commenter").ToolTip %>" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Include" onclick="AddStringValue('Commenter', 'true')" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Exclude" onclick="AddStringValue('Commenter', 'false')" />
                            </td></tr></table>
                        </div>

                        <div id="CommenterDropdownFieldDiv" 
                            <%=Model.GetFieldElement("Commenter").DataType == CustomReportFieldManager.FieldDataTypes.Dropdown?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px"><tr><td class="customReportValueTable">
                            <label for="CommenterDropdownFieldValue" id="CommenterDropdownFieldLabel" class="fieldLabel">
                                <%=Model.GetFieldElement("Commenter").Name %>:
                            </label>
                            </td></tr>
                            <tr><td class="customReportValueTable">
                            <%= Html.Telerik().DropDownList()
                                .BindTo(Model.SelectListFieldValues("Commenter"))
                                .Name("CommenterDropdownFieldValue")
                                .HtmlAttributes(new { style = "width: 200px;" })%>
                            </td>
                            <td>
                            <input type="button" class="button" value="Include" onclick="AddDropdownValue('Commenter', 'true')" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Exclude" onclick="AddDropdownValue('Commenter', 'false')" /> 
                            </td></tr></table>
                        </div>
                    </td>
                </tr>
                
                <tr><td class="customReportSectionHeader">Filter by Letter</td></tr>
                <tr><td><label for="LetterFieldId" class="fieldLabel">Select Search Filter:</label></td></tr>
                <tr><td><%= Html.Telerik().DropDownListFor(x=>x.LetterFieldId)
                            .BindTo(Model.SelectListFields("Letter"))
                            .ClientEvents(x => x.OnChange("LetterFieldSelect"))
                            .Name("LetterFieldId")
                            .HtmlAttributes(new { style = "width: 200px;" })%></td></tr>
                <tr>
                    <td class="customReportValueSectionSmall">
                        <div id="LetterTextFieldDiv" 
                            <%=(Model.GetFieldElement("Letter").DataType == CustomReportFieldManager.FieldDataTypes.Text ||
                            Model.GetFieldElement("Letter").DataType == CustomReportFieldManager.FieldDataTypes.Number)?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px"><tr><td class="customReportValueTable">
                            <label for="LetterStringFieldValue" id="LetterStringFieldLabel" class="fieldLabel">
                                <%=Model.GetFieldElement("Letter").Name %>:
                            </label>
                            </td></tr>
                            <tr><td class="customReportValueTable">
                            <%:Html.TextBox("LetterStringFieldValue", "", new { Class = "text", onkeypress = "TextFieldKeyPress(Letter)" })%>
                            <img src="<%= ResolveUrl("~/Content/Images/toolTip.gif")%>" id="LetterTextFieldTool" 
                                title="<%=Model.GetFieldElement("Letter").ToolTip %>" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Include" onclick="AddStringValue('Letter', 'true')" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Exclude" onclick="AddStringValue('Letter', 'false')" />
                            </td></tr></table>
                        </div>

                        <div id="LetterDropdownFieldDiv" 
                            <%=Model.GetFieldElement("Letter").DataType == CustomReportFieldManager.FieldDataTypes.Dropdown?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px"><tr><td class="customReportValueTable">
                            <label for="LetterDropdownFieldValue" id="LetterDropdownFieldLabel" class="fieldLabel">
                                <%=Model.GetFieldElement("Letter").Name %>:
                            </label>
                            </td></tr>
                            <tr><td class="customReportValueTable">
                            <%= Html.Telerik().DropDownList()
                                .Name("LetterDropdownFieldValue")
                                .BindTo(Model.SelectListFieldValues("Letter"))
                                .HtmlAttributes(new { style = "width: 200px;" })%>
                            </td>
                            <td>
                            <input type="button" class="button" value="Include" onclick="AddDropdownValue('Letter', 'true')" />
                            </td>
                            <td>
                            <input type="button" class="button" value="Exclude" onclick="AddDropdownValue('Letter', 'false')" />
                            </td></tr></table>
                        </div>

                        <div id="LetterDateFieldDiv" 
                            <%=Model.GetFieldElement("Letter").DataType == CustomReportFieldManager.FieldDataTypes.Date?
                            string.Empty: "style=\"display: none\"" %>>
                            <table style="padding:0px"><tr><td class="customReportValueTable">
                            <label for="LetterFromDateFieldValue" class="fieldLabel">From:</label>
                            </td>
                            <td><% Html.Telerik().DatePicker().Name("LetterFromDateFieldValue").Render(); %></td>
                            <td><label for="LetterToDateFieldValue" class="fieldLabel">To: </label></td>
                            <td><% Html.Telerik().DatePicker().Name("LetterToDateFieldValue").Render(); %></td>
                            <td><input type="button" class="button" value="Include" onclick="AddDateValue('Letter')" /></td>
                            </tr><tr><td colspan="4"><b>Note:</b> All dates are in midnight UTC.</td></tr></table>
                        </div>
                    </td>
                </tr>
                <tr><td>Change your report presentation by selecting the options below:</td></tr>
                <tr><td class="customReportSectionHeader">Show Columns</td></tr>
                <tr><td><label for="columnGroup" class="fieldLabel">Information Category</label></td></tr>
                <tr><td><%= Html.Telerik().DropDownListFor(x => x.ColumnGroup)
                                .BindTo(Model.SelectListColumnGroups())
                                .Name("columnGroup")
                                .ClientEvents(events => events.OnChange("GetColumns"))
                                .HtmlAttributes(new { style = "width: 150px;" })%></td></tr>        
                        
                <tr><td style="padding-bottom:2px"><label for="columns" class="fieldLabel">Column:</label></td></tr>
                <tr><td>
                        <table><tr><td class="customReportValueTable">
                        <%= Html.Telerik().DropDownList()
                                .Name("columns")
                                .BindTo(Model.SelectListColumns())
                                .HtmlAttributes(new { style = "width: 150px;" })%>
                                </td>
                                <td>
                                    <input type="button" class="button" value="Include" onclick="AddColumn()" />
                                </td>
                                </tr>
                        </table>
                                
                <tr><td class="customReportSectionHeader">Sort By</td></tr>
                <tr>
                    <td >        
                        <div id="SortColumn1Div" <%=Model.SelectListChosenColumns().Count() >= 1?string.Empty: "style=\"display: none\"" %>>
                        <label for="SortColumn1" class="fieldLabel">Sort Columns by:</label><br />
                        <%= Html.Telerik().DropDownListFor(x=>x.SortColumn1)
                                .Name("SortColumn1")
                                .BindTo(Model.SelectListChosenColumns(true))
                                .HtmlAttributes(new { style = "width: 150px;" })%>
                        <%= Html.Telerik().DropDownListFor(x=>x.Asc1)
                                .Name("Asc1")
                                .BindTo(Model.OrderOptions())%><br />
                        </div>
                        <div id="SortColumn2Div" <%=Model.SelectListChosenColumns().Count() >= 2?string.Empty: "style=\"display: none\"" %>>
                        <label for="SortColumn2" class="fieldLabel">Then by:</label><br />
                        <%= Html.Telerik().DropDownList()
                                .Name("SortColumn2")
                                .BindTo(Model.SelectListChosenColumns(true))
                                .HtmlAttributes(new { style = "width: 150px;" })%>
                        <%= Html.Telerik().DropDownList()
                                .Name("Asc2")
                                .BindTo(Model.OrderOptions())%><br />
                        </div>
                        <div id="SortColumn3Div" <%=Model.SelectListChosenColumns().Count() >= 3?string.Empty: "style=\"display: none\"" %>>
                        <label for="SortColumn3" class="fieldLabel">Then by:</label><br />
                        <%= Html.Telerik().DropDownList()
                                .Name("SortColumn3")
                                .BindTo(Model.SelectListChosenColumns(true))
                                .HtmlAttributes(new { style = "width: 150px;" })%>
                        <%= Html.Telerik().DropDownList()
                                .Name("Asc3")
                                .BindTo(Model.OrderOptions())%><br />
                        </div>

                        
                    </td>
                </tr>
                <tr>
                    <td>
                       <%:Html.ActionLink(Constants.CANCEL, Constants.INDEX, "Report", new { @class = "button" })%>
                       <input type="button" class="button"  value="Run Report" onclick="RunReport()"/>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top"><% Html.RenderPartial("~/Views/Shared/List/ucCustomReportParameterList.ascx", Model);%></td>
    </tr>
</table>
    <%: Html.HiddenFor(m => m.BreadCrumbTitle)%>
    <%: Html.HiddenFor(m => m.Description)%>
    <%: Html.HiddenFor(m => m.PageTitle)%>
    <%: Html.HiddenFor(m => m.ReportId) %>
    <%: Html.HiddenFor(m => m.Dirty) %>
    <%: Html.HiddenFor(m => m.FieldId) %>
    <%: Html.HiddenFor(m => m.FieldValue) %>
    <%: Html.HiddenFor(m => m.FieldValueName) %>
    <%: Html.HiddenFor(m => m.FieldValueInclude) %>
    <%: Html.HiddenFor(m => m.FieldDateFrom) %>
    <%: Html.HiddenFor(m => m.FieldDateTo) %>
    <%: Html.HiddenFor(m => m.Action) %>
    <%: Html.HiddenFor(m => m.ListId) %>
    </form>
 <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/cara.customreportconfiguration.js"));%>
</asp:Content>