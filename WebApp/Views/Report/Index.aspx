﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.ReportViewModel2>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
           <!-- navigation bar code goes here  -->
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%:Html.ActionLink(Utilities.GetResourceString("StandardReports"), Constants.STANDARD_REPORTS, "Report",new {action=Constants.STANDARD_REPORTS}, new {@class = "button"}) %>
<%:Html.ActionLink(Utilities.GetResourceString("CustomReports"), "TrafficDirector", "Report", new { action = "Start", WizardStep = 0, ReportId = 0 }, new { @class = "button" })%>
<%:Html.ActionLink(Utilities.GetResourceString("MyReports"), "MyReports", "report", new { @class = "button" })%>
</asp:Content>