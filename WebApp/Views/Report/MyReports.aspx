﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.MyReportModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%: Html.ValidationSummary() %> 
<span class="fieldLabel">View Options:</span>&nbsp;
<% Html.Telerik().DropDownList()
        .Name("MyReportListOption")
        .BindTo(Model.MyReportListOptions)
        .ClientEvents(events => events.OnChange("updateMyReportList"))
        .HtmlAttributes(new { style = "width:225px;" })
        .Render(); %>
<div id="myReportListArea" class="tabArea">
    <% Html.RenderPartial("~/Views/Shared/List/ucMyReportList.ascx", Model.MyReports); %>        
</div>
<% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/telerik.window.min.js")
            .Add("~/Scripts/cara.myreportlist.js")
            .Add("~/Scripts/cara.reportindex.js")); %>
</asp:Content>