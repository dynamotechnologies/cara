﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.StandardReportViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
           <!-- navigation bar code goes here  -->
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%: Html.ValidationSummary() %> 
<% using (Html.BeginForm())
   {%>
    <% Html.RenderPartial("~/Views/Shared/Filter/ucStandardReportFilter.ascx", Model); %>        
    <br />
    <% if (Utilities.UserHasPrivilege("RRPT", Constants.SYSTEM))
       { %>
        <input type="submit" class="button" name="action" value="Next" />
        <input type="submit" class="button" name="action" value="Cancel" />
    <% } %>
    <%: Html.HiddenFor(m => m.BreadCrumbTitle) %>
    <%: Html.HiddenFor(m => m.PageTitle) %>
<%} %>
</asp:Content>
