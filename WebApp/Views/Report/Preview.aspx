﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%: Html.ValidationSummary() %> 
Below is a preview of the data returned by the report. All dates and times are in UTC. You can save this data and the report that generated it.<br /><br />
<form action="/Report/TrafficDirector" method="post">
<input type="submit" value="<%=Constants.BACK %>" class="button"/>
    <a class="button" id="btnSaveAsNew" href="#" onclick="openWindow('WindowMyReport', '/Report/MyReportInput?reportId=0'); return false;">Save Parameters As New Report</a >
    <% if (Model.UserOwnsReport && Model.Dirty)
       { %>
        <a class="button" id="btnMyReport1" href="#" onclick="openWindow('WindowMyReport', '/Report/MyReportInput?reportId=<%=Model.ReportId %>'); return false;">Save Parameters of "<%=Model.ReportName%>"</a >
    <%} %>
<input type="button" class="button" onclick="Export()" value="Export" /><br /><br />
<%Html.Telerik().Grid<Aquilent.Cara.Domain.Report.CustomReportData>(Model.ReportData)
                .Name("Preview")
                .Columns(columns =>
                {
                    List<string> titles = Model.ColumnTitles();
                    if (titles.Count > 0)
                    { columns.Bound(x => x.Column1).Title(titles[0]); }
                    if (titles.Count > 1)
                    { columns.Bound(x => x.Column2).Title(titles[1]); }
                    if (titles.Count > 2)
                    { columns.Bound(x => x.Column3).Title(titles[2]); }
                    if (titles.Count > 3)
                    { columns.Bound(x => x.Column4).Title(titles[3]); }
                    if (titles.Count > 4)
                    { columns.Bound(x => x.Column5).Title(titles[4]); }
                    if (titles.Count > 5)
                    { columns.Bound(x => x.Column6).Title(titles[5]); }
                    if (titles.Count > 6)
                    { columns.Bound(x => x.Column7).Title(titles[6]); }
                    if (titles.Count > 7)
                    { columns.Bound(x => x.Column8).Title(titles[7]); }
                    if (titles.Count > 8)
                    { columns.Bound(x => x.Column9).Title(titles[8]); }
                    if (titles.Count > 9)
                    { columns.Bound(x => x.Column10).Title(titles[9]); } 
                    if (titles.Count > 10)
                    { columns.Bound(x => x.Column11).Title(titles[10]); }
                    if (titles.Count > 11)
                    { columns.Bound(x => x.Column12).Title(titles[11]); }
                    if (titles.Count > 12)
                    { columns.Bound(x => x.Column13).Title(titles[12]); }
                    if (titles.Count > 13)
                    { columns.Bound(x => x.Column14).Title(titles[13]); }
                    if (titles.Count > 14)
                    { columns.Bound(x => x.Column15).Title(titles[14]); }
                    if (titles.Count > 15)
                    { columns.Bound(x => x.Column16).Title(titles[15]); }
                    if (titles.Count > 16)
                    { columns.Bound(x => x.Column17).Title(titles[16]); }
                    if (titles.Count > 17)
                    { columns.Bound(x => x.Column18).Title(titles[17]); }
                    if (titles.Count > 18)
                    { columns.Bound(x => x.Column19).Title(titles[18]); }
                    if (titles.Count > 19)
                    { columns.Bound(x => x.Column20).Title(titles[19]); }
                })
               .NoRecordsTemplate(Constants.NO_RECORD_DISPLAY)
               .DataBinding(dataBinding => dataBinding.Ajax().Select("PreviewBinding", "Report"))
               .EnableCustomBinding(true)
               .Pageable(settings => settings.PageSize(Utilities.GetUserSession().SearchResultsPerPage).Total((int)ViewData["total"]).PageTo((int)ViewData["pageNum"]))
               //.Sortable()
               .Render();
%>
    
    <br /><input type="submit" value=<%=Constants.BACK %> class="button"/>
    <%: Html.HiddenFor(m => m.ReportId)%>
    <%: Html.HiddenFor(m => m.Dirty) %>
    <%: Html.HiddenFor(m => m.CollapsedListIds) %>
    <input type="hidden" name="WizardStep" id="WizardStep" value="<%=Model.LastStep %>" />
</form>

<% Html.Telerik().Window()
    .Name("WindowMyReport")
    .Title(string.Format("{0} to {1}", Constants.SAVE, Utilities.GetResourceString("MyReport")))
    .Draggable(true)
    .Resizable(resizing => resizing
                    .Enabled(true)
                    .MaxWidth(350)
                    .MaxHeight(250)
                )
    .Modal(true)
    .ClientEvents(events => events.OnOpen("setWindowFocus"))
    .Buttons(b => b.Close())
    .Width(325)
    .Height(175)
    .Visible(false)
    .Render(); %>

 <% Html.Telerik()
        .ScriptRegistrar()
        .Scripts(script => script
            .Add("~/Scripts/telerik.window.dialog.js")
            .Add("~/Scripts/cara.myreportinput.js")
            .Add("~/Scripts/cara.customreportpreview.js"));%>
</asp:Content>