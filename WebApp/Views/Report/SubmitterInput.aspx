﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.Models.CustomReportViewModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Html.DisplayFor(m => m.PageTitle) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="HelpContent" runat="server">
    <% Html.RenderPartial("ucHelpButton", new HelpViewModel { Name = Constants.REPORT, Mode = PageMode.None,
                                                              Title = Model.PageTitle}); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ProjectNav" runat="server">
           <!-- navigation bar code goes here  -->
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="breadcrumb"><% Html.RenderPartial("~/Views/Shared/ucBreadcrumb.ascx", new Breadcrumb(Model.BreadCrumbTitle, Request.Url)); %></div>
    <div class="pageTitle"><%: Html.DisplayFor(m => m.PageTitle) %></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%: Html.ValidationSummary() %> 
<%: Html.DisplayFor(m => m.Description) %>

<%using (Html.BeginForm())
  {%>
  <% Html.RenderPartial("~/Views/Shared/Input/ucCustomReportFieldSelector.ascx", Model);%>
    <br />
    <input type="submit" class="button" name="action" value="Back" />
    <input type="submit" class="button" name="action" value="Next" />
    <input type="submit" class="button" name="action" value="Cancel" />
    <br />
    <%: Html.HiddenFor(m => m.BreadCrumbTitle)%>
    <%: Html.HiddenFor(m => m.Description)%>
    <%: Html.HiddenFor(m => m.PageTitle)%>
<%} %>
</asp:Content>