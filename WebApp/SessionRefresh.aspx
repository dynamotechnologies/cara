<%@ Page language="c#" Codebehind="SessionRefresh.aspx.cs" AutoEventWireup="false" Inherits="Aquilent.Cara.WebApp.SessionRefresh" %>
<!DOCTYPE html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
  <head>
    <title>Session Refresh</title>
    <link rel="stylesheet" type="text/css" href="Content/cara.css" />

    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
  </head>
  <body>
  
    <div id="tabbar">
      <div>
        <ul>
          <li class="active"><div><span></span></div></li>
        </ul>
      </div>
    </div>

    <table border="0" cellpadding="0" cellspacing="0" width="100%" summary="This table is used to format the content area">
      <tr>
        <td width="100%">
          <div id="content">
            <table border="0" cellpadding="0" cellspacing="0" width="95%" summary="This table is used to format the content area">
              <tr>
                <td width="100%" class="contentArea">
    
                  <form id="form" method="post" runat="server">
                    Your session has been refreshed.  Please Click <b>Close Window</b> to continue.<br /><br />
                    <a class="button" href="javascript:window.close()">Close Window</a>
                  </form>
                  
                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>

    <div id="footer">
      <div>
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  
    <script type="text/javascript"><!--
      // This call will close the window before anyone will see it.
      // That's ok.
      opener.sessionStartTimer()
    // -->
    </script>
  </body>
</html>
