﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the project specific actions
    /// </summary>
    public class ProjectController : PageController
    {
        #region Private Members
        #endregion

        #region Core Actions
        public ViewResult List()
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.PROJECT,string.Empty) as ProjectSearchResultFilter;
            if (filter == null)
            {
                filter = new ProjectSearchResultFilter { Archived = false, PageNum = 1 };
                filter.UserId = Utilities.GetUserSession().UserId;
            }

            // translate the filter type from request param
            filter.SetFilterByType(GetParam(Constants.FILTER), GetParam("ProjectNameOrId"), Utilities.GetUserSession().UserId);

            // save the filter in the session
            Utilities.SetFilter(Constants.PROJECT, string.Empty, filter);

			int total;
			int pageNum;

            // Create the view model
            var viewModel = new ProjectListViewModel
            {
                BreadCrumbTitle = Utilities.GetResourceString(Constants.PROJECT).Pluralize(),
                PageTitle = Utilities.GetResourceString(Constants.PROJECT).Pluralize(),
				Projects = GetProjects(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage, Page = filter.PageNum.Value }, out total, out pageNum)
            };

            viewModel.LoadFilterToModel(filter);

            ViewData["total"] = total;
			ViewData["pageNum"] = pageNum;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult List(string action, ProjectListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // save the filter in the session
                    Utilities.SetFilter(Constants.PROJECT, string.Empty, model.GetFilterFromModel() as ProjectSearchResultFilter);
                }

                ViewData["total"] = 0;
				ViewData["pageNum"] = 1;

                // pass the view model, list binding method will get the data
                return RedirectToAction(Constants.LIST);
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
                Utilities.SetFilter(Constants.PROJECT, string.Empty, new ProjectSearchResultFilter { PageNum = 1 });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST);
            }

            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ProjectListBinding(GridCommand command)
        {
			int total;
			int pageNum;
            var data = GetProjects(command, out total, out pageNum).ToList();

			ViewData["pageNum"] = pageNum;

			return View(Constants.LIST, new GridModel { Data = data, Total = total });
        }

        private static ICollection<ProjectSearchResult> GetProjects(GridCommand command, out int total, out int pageNum)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.PROJECT, string.Empty) as ProjectSearchResultFilter;
            if (filter == null)
            {
                filter = new ProjectSearchResultFilter();
            }

            // set the paging
            filter.PageNum = command.Page;
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            total = 0;

            // get the data
            IList<ProjectSearchResult> data;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                data = projectManager.GetProjects(filter, out total);
            }

			pageNum = command.Page;
			filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.PROJECT, string.Empty, filter);

            // return the data as a list
            return data.ToList();
        }

        public ViewResult Details(int projectId)
        {
            // get the project details from service and pass the object as the view model
            Project project;
            IList<Domain.Location> locations;
            string activities;
            string purposes;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                // Turn off lazy loading
                projectManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                // Force load all of the properties needed in one query
                var projectData = projectManager
                    .All
                    .Where(p => p.ProjectId == projectId)
                    .Select(pi => new { Project = pi, Locations = pi.Locations, Activities = pi.ProjectActivities, Purposes = pi.ProjectPurposes })
                    .FirstOrDefault();

                project = projectData.Project;
                locations = project.Locations.ToList();
                activities = project.ProjectActivityNameList;
                purposes = project.ProjectPurposeNameList;
            }

            IList<PhaseSearchResult> phases;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phases = phaseManager.GetPhases(
                    new PhaseSearchResultFilter 
                    { 
                        UserId = Utilities.GetUserSession().UserId,
                        ProjectId = projectId
                    }).ToList();
            }


            return View(new ProjectDetailsViewModel
            {
                BreadCrumbTitle = project.ProjectNameNumber,
                PageTitle = string.Format("{0} for {1}", Utilities.GetPageTitle(ControllerAction), project.ProjectNameNumber),
                Project = project,
                Phases = phases,
                Locations = locations,
                Purposes = purposes,
                Activities = activities
            });
        }

        public ActionResult DetailsByPalsId(string palsProjectId)
        {
            int caraProjectId;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                caraProjectId = projectManager.GetByProjectNumber(palsProjectId).ProjectId;
            }

            return RedirectToAction(Constants.DETAILS, new { projectId = caraProjectId });
        }

        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.SYSTEM)]
        public ActionResult Delete(int projectId)
        {
            // get the project
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                try
                {
                    var project = projectManager.Get(projectId);
                    if (project != null)
                    {
                        // delete the project and the phases
                        projectManager.Delete(project);

                        projectManager.UnitOfWork.Save();
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    TempData[Constants.ERROR] = string.Format("An error occurred while deleting the {0}.", Constants.PROJECT);
                    return RedirectToAction(Constants.DETAILS, new { projectId = projectId });
                }
            }

            return RedirectToAction(Constants.INDEX, Constants.HOME);
        }
        #endregion

        #region External Project Input Actions
        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.SYSTEM)]
        public ViewResult Input(PageMode mode, int projectId = 0)
        {
            Project project = null;
            if (projectId != 0)
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    project = projectManager.Get(projectId);
                }
            }

            var model = new ProjectInputViewModel
            {
                IsPalsProject = project == null ? true : project.IsPalsProject,
                Mode = mode,
                Project = project
            };

            if (project != null)
            {
                model.ProjectId = projectId;
                model.PageTitle = string.Format("{0} {1}", Constants.EDIT, Constants.PROJECT);

                if (!project.IsPalsProject)
                {
                    model.NonPalsDescription = project.Description;
                    model.NonPalsProjectName = project.Name;
                    model.NonPalsUnit = project.UnitId;
                }

                // if user goes from step 2 back to step 1
                int phaseId;
                if (int.TryParse(Request.QueryString["phaseId"], out phaseId))
                {
                    model.PhaseId = phaseId;
                }
            }
            else
            {
                model.BreadCrumbTitle = Utilities.GetResourceString(mode.ToString());
                model.PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, ControllerAction);
                model.Description = ProjectWizardManager.Instance.GetStep(mode, ControllerAction).Description;
            }

            return View(model);
        }

        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.SYSTEM)]
        [HttpPost]
        public ActionResult Input(string action, ProjectInputViewModel model)
        {
            // instantiate models
            var dmpModel = new DataMartProjectViewModel();

            // get the form data
            // PALS project ID
            if (model.IsPalsProject.HasValue && model.IsPalsProject.Value && !string.IsNullOrEmpty(Request.Form.Get("rbProjectId")))
            {
                dmpModel.ProjectId = Request.Form.Get("rbProjectId");
            }

            // set model properties
            dmpModel.ProjectNameOrId = model.ProjectNameOrId;

            model.DataMartProjectViewModel = dmpModel;
            model.Action = action;

            if (Constants.NEXT.Equals(action) || Constants.SAVE.Equals(action))
            {
                // validate the model
                model.Validate(ModelState);

                if (ModelState.IsValid)
                {
                    // save the project
                    int projectId = model.Save(ModelState);

                    if (projectId > 0)
                    {
                        if (Constants.NEXT.Equals(action))
                        {
                            // if succeed, go to the next step with CARA project id
                            var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, ControllerAction);

                            return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = model.PhaseId, mode = model.Mode });
                        }
                        else if (Constants.SAVE.Equals(action))
                        {
                            return RedirectToAction("Details", new { projectId = projectId });
                        }
                    }
                }
            }
            else if (Constants.CANCEL.Equals(action))
            {
                if (model.Mode.Equals(PageMode.ProjectCreateWizard))
                {
                    return RedirectToAction(Constants.INDEX, Constants.HOME);
                }
                else if (model.Mode.Equals(PageMode.Edit))
                {
                    return RedirectToAction("Details", new { projectId = model.ProjectId });
                }
            }
            else if (string.Format("{0} {1}", Constants.SEARCH, Utilities.GetResourceString(Constants.PROJECT).Pluralize()).Equals(action))
            {
                // validate the model
                model.DataMartProjectViewModel.Validate(ModelState);

                if (ModelState.IsValid)
                {
                    var filter = model.DataMartProjectViewModel.GetFilterFromModel() as DataMartProjectFilter;

                    // set filter paging info
                    if (filter != null)
                    {
                        filter.StartRow = 0;
                        filter.NumRows = Constants.DEFAULT_PAGE_SIZE;
                    }

                    // save the filter so that the records can be reloaded when other form actions are performed
                    Utilities.SetFilter(Constants.DATAMART_PROJECT, string.Empty, filter);

                    // find the projects
                    try
                    {
                        var results = model.DataMartProjectViewModel.GetListRecords(ModelState, filter);

                        ViewData["totalProjects"] = 0;
                        ViewData["pageToProjects"] = 1;
                        if (results != null)
                        {
                            // set total count
                            ViewData["totalProjects"] = results.ResultMetaData.TotalCount;
                            ViewData["pageToProjects"] = 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        ModelState.AddModelError(string.Empty, string.Format("An error occurred while retrieving the projects from {0}", Utilities.GetResourceString("PALS")));
                    }
                }
            }

            // get the data mart project list if not loaded and search criteria is provided
            if (model.DataMartProjectViewModel.Projects == null && (!string.IsNullOrEmpty(model.DataMartProjectViewModel.ProjectId) ||
                !string.IsNullOrEmpty(model.ProjectNameOrId)))
            {
                // get the paged records
                var filter = Utilities.GetFilter(Constants.DATAMART_PROJECT, string.Empty) as DataMartProjectFilter;

                if (filter == null)
                {
                    filter = new DataMartProjectFilter
                    {
                        NumRows = Constants.DEFAULT_PAGE_SIZE,
                        StartRow = 0
                    };
                }

                ViewData["totalProjects"] = 0;
                ViewData["pageToProjects"] = 1;

                // get the list records on the current page
                try
                {
                    var results = model.DataMartProjectViewModel.GetListRecords(ModelState, filter);

                    if (results != null)
                    {
                        // set total count
                        ViewData["totalProjects"] = results.ResultMetaData.TotalCount;
                        ViewData["pageToProjects"] = GetPageTo(filter.StartRow, filter.NumRows);
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, string.Format("An error occurred while retrieving the projects from {0}", Utilities.GetResourceString("PALS")));
                }
            }

            return View(model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult DataMartProjectListBinding(GridCommand command)
        {
			int total;
            var data = GetDataMartProjects(command, out total).ToList();

            ViewData["pageToProjects"] = command.Page;

            return View(Constants.INPUT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<DataMartProject> GetDataMartProjects(GridCommand command, out int total)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.DATAMART_PROJECT, string.Empty) as DataMartProjectFilter;
            if (filter == null)
            {
                filter = new DataMartProjectFilter();
            }

            // set the paging            
            filter.StartRow = (command.Page - 1) * command.PageSize;
            filter.NumRows = command.PageSize;

            // save the filter so that the records can be reloaded when other form actions are performed
            Utilities.SetFilter(Constants.DATAMART_PROJECT, string.Empty, filter as DataMartProjectFilter);

            // get the data
            ICollection<DataMartProject> data;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {

                var dmManager = new DataMartManager();
                dmManager.ProjectManager = projectManager;

                // bind the results
                var result = dmManager.FindProjects(filter);
                data = result.Projects.ToList();

                if (filter.IsObjection)
                {
                    foreach (var project in data)
                    {
                        project.IsObjection = true;
                    }
                }

                // set the count
                total = result.ResultMetaData.TotalCount;

                dmManager.Dispose();
            }

            // return the data as a list
            return data;
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult DataMartUserListBinding(GridCommand command)
        {
			int total;
            var data = GetDataMartUsers(command, out total).ToList();

            ViewData["pageTo"] = command.Page;

			return View(Constants.INPUT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<User> GetDataMartUsers(GridCommand command, out int total)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.PROJECT_MANAGER, string.Empty) as DataMartUserFilter;
            if (filter == null)
            {
                filter = new DataMartUserFilter();
            }

            // set the paging            
            filter.StartRow = (command.Page - 1) * command.PageSize;
            filter.NumRows = command.PageSize;

            // save the filter so that the records can be reloaded when other form actions are performed
            Utilities.SetFilter(Constants.PROJECT_MANAGER, string.Empty, filter as DataMartUserFilter);

            // get the data
            IList<User> data;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.UserManager = userManager;

                // bind the results
                var result = dmManager.FindUsers(filter);
                data = result.Users.ToList();

                // set the count
                total = result.ResultMetaData.TotalCount;

                dmManager.Dispose();
            }

            // return the data as a list
            return data;
        }

        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.SYSTEM)]
        public ViewResult CreateObjection()
        {
            var model = new ObjectionInputViewModel
            {
                PageTitle = "Create Objection Filing Period",
                BreadCrumbTitle = "Create Objection Filing Period"
            };

            return View(model);
        }

        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.SYSTEM)]
        [HttpPost]
        public ActionResult CreateObjection(string action, ObjectionInputViewModel model)
        {
            ActionResult result = View(model);

            model.DataMartProjectViewModel = new DataMartProjectViewModel();
            model.DataMartProjectViewModel.ProjectNameOrId = model.ProjectNameOrId;
            model.DataMartProjectViewModel.IsObjection = true;

            string searchProjectsAction = string.Format("{0} {1}", Constants.SEARCH, Utilities.GetResourceString(Constants.PROJECT).Pluralize());
            bool createFailed = false;
            if (action == (Constants.CREATE + " Objection Period"))
            {
                // validate the model
                model.DataMartProjectViewModel.Validate(ModelState);
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrEmpty(Request.Form.Get("rbProjectId")))
                    {
                        model.DataMartProjectViewModel.ProjectId = Request.Form.Get("rbProjectId");
                        model.ProjectId = int.Parse(Request.Form.Get("rbProjectId"));
                    }

                    // validate the model
                    model.Validate(ModelState);

                    if (ModelState.IsValid)
                    {
                        // save the project/create the phase
                        Phase phase = model.Save(ModelState);

                        if (phase != null && phase.PhaseId > 0)
                        {
                            result = RedirectToAction("Details", "Phase", new { projectId = phase.ProjectId, phaseId = phase.PhaseId });
                        }
                    }
                    else
                    {
                        createFailed = true;
                    }
                }
            }
            
            if (searchProjectsAction == action || createFailed)
            {
                // validate the model
                model.DataMartProjectViewModel.Validate(ModelState);

                if (ModelState.IsValid || createFailed)
                {
                    var filter = model.DataMartProjectViewModel.GetFilterFromModel() as DataMartProjectFilter;

                    // set filter paging info
                    if (filter != null)
                    {
                        filter.StartRow = 0;
                        filter.NumRows = Constants.DEFAULT_PAGE_SIZE;
                        filter.IsObjection = true;
                    }

                    // save the filter so that the records can be reloaded when other form actions are performed
                    Utilities.SetFilter(Constants.DATAMART_PROJECT, string.Empty, filter);

                    // find the projects
                    try
                    {
                        var results = model.DataMartProjectViewModel.GetListRecords(ModelState, filter);

                        ViewData["totalProjects"] = 0;
                        ViewData["pageToProjects"] = 1;
                        if (results != null)
                        {
                            // set total count
                            ViewData["totalProjects"] = results.ResultMetaData.TotalCount;
                            ViewData["pageToProjects"] = 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        ModelState.AddModelError(string.Empty, string.Format("An error occurred while retrieving the projects from {0}", Utilities.GetResourceString("PALS")));
                    }
                }
            }

            else if (Constants.CANCEL.Equals(action))
            {
                if (model.Mode.Equals(PageMode.CreateObjection))
                {
                    return RedirectToAction(Constants.INDEX, Constants.HOME);
                }
            }

            return result;
        }
        #endregion

        #region Lead Unit Management JSON Actions
        // TODO: Move this region to a separate controller
        public JsonResult Regions()
        {
            var unitLookup = LookupManager.GetLookup<LookupUnit>();
            var regions = unitLookup.Where(u => u.UnitId.Length == 4).OrderBy(u => u.Name);
            
            return Json(new SelectList(regions, "UnitId", "Name"),
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult Forests(string projectId)
        {
            var unitLookup = LookupManager.GetLookup<LookupUnit>();
            IList<Unit> forests;

            if (!string.IsNullOrEmpty(projectId))
            {
                forests = unitLookup.Where(u => u.UnitId.Length == 6 && u.UnitId.StartsWith(projectId)).OrderBy(u => u.Name).ToList();
            }
            else
            {
                forests = new List<Unit> { new Unit { UnitId = "-1", Name = "All Forests in selected Region" } };
            }

            return Json(new SelectList(forests, "UnitId", "Name"),
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult Districts(string projectId)
        {
            var unitLookup = LookupManager.GetLookup<LookupUnit>();
            IList<Unit> districts;

            if (!string.IsNullOrEmpty(projectId))
            {
                districts = unitLookup.Where(u => u.UnitId.Length == 8 && u.UnitId.StartsWith(projectId)).OrderBy(u => u.Name).ToList();
            }
            else
            {
                districts = new List<Unit> { new Unit { UnitId = "-1", Name = "All Districts in selected Forest" } };
            }

            return Json(new SelectList(districts, "UnitId", "Name"),
                JsonRequestBehavior.AllowGet);
        }
        #endregion Lead Unit Management JSON Actions

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion
    }
}
