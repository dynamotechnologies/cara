﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.Reference;
using Aquilent.Cara.Reporting;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.WebApp.Models.Configuration;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using log4net;
using System.Runtime.Caching;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the phase specific actions
    /// </summary>
    public class PhaseController : PageController
    {
        #region Private Members
        private static int count = 0;
        private static ILog log = LogManager.GetLogger(typeof(PhaseController));
        #endregion

        #region Core Actions
        public ViewResult Details(int projectId, int phaseId)
        {
            // get phase
            Phase phase;
            PhaseSummary phaseSummary;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);
                phaseSummary = phaseManager.GetPhaseSummary(phaseId);

                // lazy load properties for the view
                var pmr = phase.PhaseMemberRoles;
                var docs = phase.Documents;
                var codes = phase.PhaseCodes;
                var termLists = phase.TermLists;
            }

            string projectNumber;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                projectNumber = projectManager.Get(projectId).ProjectNumber;
            }

            if (phase.PhaseTypeId == 3)
            {
                phase.ObjectionResponseExtendedBy = phase.ObjectionResponseExtendedBy == null ? 0 : phase.ObjectionResponseExtendedBy;
            }

            return View(GetViewPath(), new PhaseDetailsViewModel
            {
                BreadCrumbTitle = phase.PhaseTypeDesc,
                PageTitle = string.Format("{0} {1}", phase.PhaseTypeName, Utilities.GetResourceString(Constants.PHASE)),
                Phase = phase,
                PhaseSummary = phaseSummary,
				RequestContext = Request.RequestContext,
                ProjectNumber = projectNumber
            });
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ActionResult Delete(int projectId, int phaseId)
        {
            try
            {
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phase = phaseManager.Get(phaseId);

                    // delete the phase
                    if (phase != null)
                    {
                        phaseManager.Delete(phase);
                        phaseManager.UnitOfWork.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                TempData[Constants.ERROR] = string.Format("An error occurred while deleting the {0}.", Utilities.GetResourceString(Constants.PHASE));
                return RedirectToAction(Constants.DETAILS, new { projectId = projectId, phaseId = phaseId });
            }

            // go to project details page
            return RedirectToAction(Constants.DETAILS, Constants.PROJECT, new { projectId = projectId });
        }

        [HttpPost]
        public bool DownloadAllLetters(int projectId, int phaseId)
        {
            bool result = false;
            try
            {
                var dmdManager = new DmdManager();
                result = dmdManager.RequestAllPhaseLetters(projectId, phaseId, Utilities.GetUserSession().Username);
            }
            catch (DomainException de)
            {
                LogError(de);
            }

            return result;
        }

        [HttpPost]
        public bool DownloadFormLetters(int projectId, int phaseId, int formSetId)
        {
            bool result = false;
            try
            {
                var dmdManager = new DmdManager();
                result = dmdManager.RequestFormLetters(projectId, phaseId, Utilities.GetUserSession().Username, formSetId);
            }
            catch (DomainException de)
            {
                LogError(de);
            }

            return result;
        }
        #endregion

        #region Phase Input Actions
        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ViewResult Input(int projectId, int phaseId, PageMode mode)
        {
            int defaultObjectionDuration = 0;
            int defaultDuration = 0;
            Phase phase;
            Project project;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                // get the project and phases
                project = projectManager.Get(projectId);
                if (phaseId == 0)
                {
                    phase = new Phase
                    {
                        ProjectId = projectId,
                        CommentStart = DateTime.UtcNow,
                        ObjectionResponseExtendedBy = 0, 
                        PublicCommentActive = true,
                        ReadingRoomActive = true,
                        PhaseTypeId = LookupManager.GetLookup<LookupPhaseType>().First(x => x.Name == "Other").PhaseTypeId,
                        AlwaysOpen = false
                    };
                }
                else
                {
                    phase = project.Phases.First(x => x.PhaseId == phaseId);
                }

                if (project != null)
                {
                    // set the phase default durations
                    if (project.PhaseMinLength != null)
                    {
                        defaultDuration = project.PhaseMinLength.PhaseMinimumDays.HasValue ? project.PhaseMinLength.PhaseMinimumDays.Value : 0;    
                        defaultObjectionDuration = project.PhaseMinLength.ObjectionDecisionDueDays.HasValue ? project.PhaseMinLength.ObjectionDecisionDueDays.Value : 0;
                        phase.ObjectionResponseExtendedBy = phase.ObjectionResponseExtendedBy == null ? 0 : phase.ObjectionResponseExtendedBy;
                    }
                }
            }

            // create the view model
            var model = new PhaseInputViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                OriginalCommentEnd = phase.CommentEnd,
                BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, ControllerAction)),
                PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, ControllerAction, project.Name),
                Description = ProjectWizardManager.Instance.GetStep(mode, ControllerAction).Description,
                Mode = mode,
                Phase = phase,
                Duration = phaseId > 0 && phase != null ? phase.Duration : defaultDuration,
                DefaultDuration = defaultDuration,
                DefaultObjectionDuration = defaultObjectionDuration,
                IsPalsProject = project.IsPalsProject
            };

            return View(GetViewPath(), model);
        }

        [ValidateInput(false)]
        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult Input(int projectId, int phaseId, string action, PhaseInputViewModel model)
        {
            ActionResult result = View(GetViewPath(), model);
            if (Constants.CANCEL.Equals(action))
            {
                // if phase id is provided go to phase homepage, else go to project homepage
                if (phaseId > 0)
                {
                    return RedirectToAction(Constants.DETAILS);
                }
                else
                {
                    return RedirectToAction(Constants.DETAILS, Constants.PROJECT, new { projectId = projectId });
                }
            }
            else
            {
                // validate the input data
                model.Validate(ModelState);

                // validate the data
                if (ModelState.IsValid)
                {
                    // save the phase
                    phaseId = model.Save(ModelState);

                    if (phaseId > 0)
                    {
                        // if save data succeed, redirect to the next action
                        if (Constants.NEXT.Equals(action))
                        {
                            // go to the next step
                            var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, ControllerAction);

                            return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                        }
                        else if (Constants.BACK.Equals(action))
                        {
                            // go to the next step
                            var prevStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, ControllerAction);

                            return RedirectToAction(prevStep.Action, prevStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                        }
                        else if (Constants.FINISH.Equals(action))
                        {
                            // go to the setup summary page
                            return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                        }
                        //if the information the page is not using the wizard Display the confirmation message
                        else if (!model.IsWizardMode)
                        {
                            TempData[Constants.PHASE + model.Mode] = "Changes to Comment Period have been saved";
                        }
                    }

                    result = RedirectToAction("Details", new { projectId = projectId, phaseId = phaseId });
                }
            }

            return result;
        }
        #endregion

        #region Member Input Actions
        [HasPrivilege(PrivilegeCodes = "MTM", Scope = Constants.PHASE)]
        public ViewResult MemberInput(int projectId, int phaseId, PageMode mode)
        {
            Phase phase;
            string projectName;
            IList<PhaseMemberRole> phaseMemberRoles;
            bool isPalsProject;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);
                projectName = phase.Project.Name;
                isPalsProject = phase.Project.IsPalsProject;
                phaseMemberRoles = phase.PhaseMemberRoles.ToList();
            }

            // pass a list of existing members to the view (edit mode would only show the add individual user box)
            return View(GetViewPath(), new MemberViewModel
            {
                BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, projectName),
                Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,
                Mode = mode,
                Members = phaseMemberRoles,
                Locked = phase.Locked,
                Type = (mode.Equals(PageMode.Edit) ? Constants.ADD : string.Empty),
                ShowButtons = true,
                IsPalsProject = isPalsProject
            });
        }

        [HasPrivilege(PrivilegeCodes = "MTM", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult MemberInput(string action, int projectId, int phaseId, MemberViewModel model)
        {
            // instantiate models
            var usrModel = new DataMartUserViewModel();

            // get form data
            model.ProjectNameOrId = Request.Form.Get("txtProjectNameOrId");
            model.FirstName = Request.Form.Get("txtFirstName");
            model.LastName = Request.Form.Get("txtLastName");
            model.Type = Request.Form.Get("rbType");
            model.ProjectId = Request.Form.Get("rbProjectId");
            model.PhaseId = Request.Form.Get(string.Format("ddlPhase_{0}", model.ProjectId));
            model.RoleId = Request.Form.Get("rbRoleId");

            // parse the role id into username and role id
            model.ParseRoleId();

            // set model properties
            usrModel.UserFirstName = model.FirstName;
            usrModel.UserLastName = model.LastName;
            usrModel.Username = model.Username;

            model.UserViewModel = usrModel;

            // validate the input
            model.Validate(ModelState, action);

            if (ModelState.IsValid)
            {
                if (Constants.NEXT.Equals(action))
                {
                    // if succeed, go to the next step
                    var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);

                    return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                }
                else if (Constants.FINISH.Equals(action))
                {
                    return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                }
                else if (Constants.BACK.Equals(action))
                {
                    var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                    return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                }
                else if (Constants.CANCEL.Equals(action))
                {
                    return RedirectToAction(Constants.DETAILS, Constants.PHASE, new { projectId = projectId, phaseId = phaseId });
                }
                else if ("Search Users".Equals(action))
                {
                    var filter = model.UserViewModel.GetFilterFromModel() as DataMartUserFilter;

                    // set filter paging info
                    if (filter != null)
                    {
                        filter.StartRow = 0;
                        filter.NumRows = Constants.DEFAULT_PAGE_SIZE;
                    };

                    // save the filter so that the records can be reloaded when other form actions are performed
                    Utilities.SetFilter(Constants.MEMBER, phaseId.ToString(), filter);

                    // find the users
                    try
                    {
                        var results = model.UserViewModel.GetListRecords(ModelState, filter);

                        ViewData["total"] = 0;
                        ViewData["pageTo"] = 1;

                        if (results != null)
                        {
                            // set total count
                            ViewData["total"] = results.ResultMetaData.TotalCount;
                            ViewData["pageTo"] = 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        ModelState.AddModelError(string.Empty, string.Format("An error occurred while retrieving the users from {0}", Utilities.GetResourceString("PALS")));
                    }
                }
                else if ("Search Projects".Equals(action))
                {
                    // get the projects based on the filter
                    using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                    {
                        model.Projects = projectManager.FindProjects(model.ProjectNameOrId).ToList();
                        foreach (var project in model.Projects)
                        {
                            project.Phases.ToList();
                        }
                    }
                }
                else if ("Add User".Equals(action))
                {
                    // add user
                    model.AddUser(phaseId, ModelState);

                    if (ModelState.IsValid)
                    {
                        // clear the search after save
                        model.Username = model.LastName = model.FirstName =
                            usrModel.UserFirstName = usrModel.UserLastName = null;
                        //CARA-1379 add confirmation message
                        if (!model.IsWizardMode)
                        {
                            TempData[Constants.PHASE + Constants.MEMBER] = "Added user to Comment Period";
                        }
                    }
                }
                else if ("Reuse Users".Equals(action))
                {
                    // import users
                    model.ReuseUsers(phaseId, ModelState);

                    if (ModelState.IsValid)
                    {
                        // clear the search after import
                        model.ProjectId = model.ProjectNameOrId = null;
                    }
                }
            }

            // get members
            if (model.Members == null)
            {
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    model.Members = phaseManager.Get(phaseId).PhaseMemberRoles;
                }
            }

            // get the project list if name is provided
            if (model.Projects == null && (!string.IsNullOrEmpty(model.ProjectId) || (!string.IsNullOrEmpty(model.ProjectNameOrId) && model.ProjectNameOrId.Length >= 4)))
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    model.Projects = projectManager.FindProjects(model.ProjectNameOrId).ToList();
                    foreach (var project in model.Projects)
                    {
                        project.Phases.ToList();
                    }
                }
            }

            // get the user list if name is provided
            if (model.UserViewModel.Users == null && (!string.IsNullOrEmpty(model.LastName) || !string.IsNullOrEmpty(model.FirstName)))
            {
                // get the paged records
                var filter = Utilities.GetFilter(Constants.MEMBER, phaseId.ToString()) as DataMartUserFilter;

                if (filter == null)
                {
                    filter = new DataMartUserFilter
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        NumRows = Constants.DEFAULT_PAGE_SIZE,
                        StartRow = 0
                    };
                }

                ViewData["total"] = 0;
                ViewData["pageTo"] = 1;

                // get the list records on the current page
                try
                {
                    var results = model.UserViewModel.GetListRecords(ModelState, filter);

                    if (results != null)
                    {
                        // set total count
                        ViewData["total"] = results.ResultMetaData.TotalCount;
                        ViewData["pageTo"] = GetPageTo(filter.StartRow, filter.NumRows);
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, string.Format("An error occurred while retrieving the users from {0}", Utilities.GetResourceString("PALS")));
                }
            }
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    //CR-346 Remove POC for Pals Projec 
                    Phase phase;
                    phase = phaseManager.Get(phaseId);
                    model.IsPalsProject= phase.Project.IsPalsProject;

                }
         

            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult DataMartUserListBinding(GridCommand command)
        {
            string context = Request.RequestContext.RouteData.Values["phaseId"].ToString();
            var data = GetDataMartUsers(command, context).ToList();

            ViewData["pageTo"] = command.Page;

            
            return View(Constants.MEMBER + Constants.INPUT, new GridModel { Data = data, Total = count });
            

        }

        //CR#355 - Team Members Search - Add Member for Directory
        private static IQueryable<User> GetDataMartUsers(GridCommand command, string context)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.MEMBER, context) as DataMartUserFilter;
            if (filter == null)
            {
                filter = new DataMartUserFilter();
            }

            // set the paging            
            filter.StartRow = (command.Page - 1) * command.PageSize;
            filter.NumRows = command.PageSize;

            // save the filter so that the records can be reloaded when other form actions are performed
            Utilities.SetFilter(Constants.MEMBER, context, filter as DataMartUserFilter);

            // get the data
            IQueryable<User> data;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.UserManager = userManager;

                // bind the results
                var result = dmManager.FindUsers(filter);
                data = result.Users;

                // set the count
                count = result.ResultMetaData.TotalCount;

                dmManager.Dispose();
            }

            // return the data as a list
            return data;
        }



        [HasPrivilege(PrivilegeCodes = "MTM", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public PartialViewResult UpdateMember(int phaseId, int userId, string action, string roleName)
        {
            IList<PhaseMemberRole> phaseMemberRoles;
            PhaseMemberRole pmr;
            Phase phase;
            bool isPalsProject;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                // get the phase member
                pmr = phaseManager.AllPhaseMemberRoleByPhase(phaseId).FirstOrDefault(x => x.UserId == userId);

                if (pmr != null)
                {
                    // update the status or role based on the action type
                    if (Constants.ACTIVATE.Equals(action) || Constants.DEACTIVATE.Equals(action))
                    {
                        pmr.Active = action.Equals(Constants.ACTIVATE);
                    }
                    else if (Constants.ROLE.Equals(action) && !string.IsNullOrWhiteSpace(roleName))
                    {
                        pmr.RoleId = LookupManager.GetLookup<LookupRole>().First(x => x.Name == roleName).RoleId;
                    }

                    pmr.LastUpdated = DateTime.UtcNow;
                    pmr.LastUpdatedBy = Utilities.GetUserSession().Username;

                    // update the phase member role
                    phaseManager.UnitOfWork.Save();
                }

                phaseMemberRoles = phaseManager.Get(phaseId).PhaseMemberRoles.ToList();

                //CR-346 Remove POC for Pals Projec
                phase = phaseManager.Get(phaseId);
                isPalsProject = phase.Project.IsPalsProject;
                
            }
           
            return PartialView("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel
            {
                Members = phaseMemberRoles,
                ShowButtons = true,
                Locked = pmr.Phase.Locked,
                IsPalsProject = isPalsProject
            });
        }

        [HasPrivilege(PrivilegeCodes = "MTM", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ActionResult DeleteMember(int phaseId, int userId)
        {
            // delete the phase member role
            bool locked = true;
            IList<PhaseMemberRole> phaseMemberRoles;

            Phase phase;
            bool isPalsProject;

            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var pmr = phaseManager.AllPhaseMemberRoleByPhase(phaseId).Where(x => x.UserId == userId).FirstOrDefault();

                if (pmr != null)
                {
                    locked = pmr.Phase.Locked;
                    phaseManager.DeletePhaseMemberRole(pmr);
                    phaseManager.UnitOfWork.Save();
                }

                phaseMemberRoles = phaseManager.Get(phaseId).PhaseMemberRoles.ToList();

                //CR-346 Remove POC for Pals Projec
                phase = phaseManager.Get(phaseId);
                isPalsProject = phase.Project.IsPalsProject;
            }

            return PartialView("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel
            {
                Members = phaseMemberRoles,
                ShowButtons = true,
                Locked = locked,
                IsPalsProject = isPalsProject
            });
        }

        [AjaxErrorHandler]
        public ActionResult ChangeEarlyAttentionNewsletterSubscription(int phaseId, int userId, bool currentSubscribeSetting)
        {        
            IList<PhaseMemberRole> phaseMemberRoles;
            PhaseMemberRole subUser;
            Phase phase;
            bool isPalsProject;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                subUser = phaseManager.AllPhaseMemberRoleByPhase(phaseId).Where(x => x.UserId == userId).FirstOrDefault();

                if (subUser != null)
                {
                    subUser.IsEarlyAttentionNewsletterSubscriber = !currentSubscribeSetting;
                    phaseManager.UnitOfWork.Save();
                }

                phaseMemberRoles = phaseManager.Get(phaseId).PhaseMemberRoles.ToList();

                phase = phaseManager.Get(phaseId);
                isPalsProject = phase.Project.IsPalsProject;
            }


            return PartialView("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel
            {
                Members = phaseMemberRoles,
                ShowButtons = true,
                Locked = subUser.Phase.Locked,
                IsPalsProject = isPalsProject
             });
        }
 
        [HasPrivilege(PrivilegeCodes = "MTM", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ActionResult ChangePointOfContact(int phaseId, int userId)
        {
            // delete the phase member role
            bool locked = true;
            IList<PhaseMemberRole> phaseMemberRoles;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var currentPoc = phaseManager.AllPhaseMemberRoleByPhase(phaseId).Where(x => x.IsPointOfContact == true).FirstOrDefault();
                var newPoc = phaseManager.AllPhaseMemberRoleByPhase(phaseId).Where(x => x.UserId == userId).FirstOrDefault();

                if (newPoc != null)
                {
                    locked = newPoc.Phase.Locked;

                    if (currentPoc != null)
                    {
                        currentPoc.IsPointOfContact = false;
                    }

                    newPoc.IsPointOfContact = true;
                    phaseManager.UnitOfWork.Save();
                }

                phaseMemberRoles = phaseManager.Get(phaseId).PhaseMemberRoles.ToList();
            }

            return PartialView("~/Views/Shared/List/ucMemberList.ascx", new MemberViewModel
            {
                Members = phaseMemberRoles,
                ShowButtons = true,
                Locked = locked
            });
        }
        #endregion

        #region Public Web Form Text Input Actions
        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ViewResult PublicCommentFormInput(int projectId, int phaseId, PageMode mode)
        {
            //CR#239 - On multi-district projects contact edit
            PublicCommentFormInputViewModel model = null;            
            Project project;
            Phase phase = null;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.Get(projectId);
                phase = project.Phases.FirstOrDefault(x => x.PhaseId == phaseId);
                model = new PublicCommentFormInputViewModel
                {

                    Project = project,
                    ProjectId = projectId,
                    PhaseId = phaseId,
                    PalsProjectId = project.IsPalsProject ? project.ProjectNumber : null,                    
                    IsPalsProject = project.IsPalsProject,
                    BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                    PageTitle = Utilities.GetResourceString("PhasePublicCommentFormInputTitle"),
                    Description = ProjectWizardManager.Instance.GetStep(PageMode.Edit, Action).Description,
                    Mode = mode
                };
            }

            if (mode == PageMode.Edit)
            {
                if (model.IsPalsProject)
                {
                    if (string.IsNullOrWhiteSpace(phase.ContactName))
                    {
                        model.GetWebCommentPageInfoContact(true);
                    }
                    else
                    {
                        model.ContactName = phase.ContactName;
                        model.ContactEmail = phase.CommentEmail;
                        model.Address1 = phase.MailAddressStreet1;
                        model.Address2 = phase.MailAddressStreet2;
                        model.City = phase.MailAddressCity;
                        model.State = phase.MailAddressState;
                        model.Zip = phase.MailAddressZip;
                    }
                }
                return View(GetViewPath(), model); 
            }
            else
            {
                if (model.IsPalsProject)
                {
                    model.GetWebCommentPageInfoContact(true);
                }
                model.Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description;
                model.PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, project.Name);
                return View(GetViewPath(), model);
            }
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        [ValidateInput(false)]        
        [HttpPost]
        public ActionResult PublicCommentFormInput(string action, int projectId, int phaseId, PublicCommentFormInputViewModel model)
        {
         
            // validate the model
            model.Validate(ModelState);
            if (ModelState.IsValid)
            {
                try
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        Phase phase = phaseManager.Get(phaseId);
                        phase.PublicFormText = Request.Form.Get("PublicFormText");
                        phase.Project.ReadingRoomText = Request.Form.Get("ReadingRoomText");
                        //CR#239 - On multi-district projects contact edit
                        if (phase.Project.IsPalsProject && model.IsContactUpdated)
                        {
                            phase.ContactName = Request.Form.Get("ContactName");
                            phase.CommentEmail = Request.Form.Get("ContactEmail");
                            phase.MailAddressStreet1 = Request.Form.Get("Address1");
                            phase.MailAddressStreet2 = Request.Form.Get("Address2");
                            phase.MailAddressCity = Request.Form.Get("City");
                            phase.MailAddressState = Request.Form.Get("State");
                            phase.MailAddressZip = Request.Form.Get("Zip");                              
                        }
                        else if (!model.IsContactUpdated && string.IsNullOrWhiteSpace(phase.ContactName))
                        {
                            phase.ContactName = "";
                            phase.CommentEmail = "";
                            phase.MailAddressStreet1 = "";
                            phase.MailAddressStreet2 = "";
                            phase.MailAddressCity = "";
                            phase.MailAddressState = "";
                            phase.MailAddressZip = "";   
                        }
                        
                        phaseManager.UnitOfWork.Save();
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, "An error occurred while saving the Public Web Form Text");
                }

                if (!string.IsNullOrEmpty(model.ActionString))
                {
                    if (model.ActionString.Equals(Constants.NEXT))
                    {
                        // if succeed, go to the next step
                        var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);

                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.FINISH))
                    {
                        return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.BACK))
                    {
                        var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.SAVE))
                    {
                        return RedirectToAction(Constants.DETAILS, Constants.PHASE, new { phaseId = phaseId });
                    }
                }
            }
            return View(GetViewPath(), model);
        }
        #endregion

        #region Document Input Actions
        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ViewResult DocumentInput(int projectId, int phaseId, PageMode mode)
        {
            Project project;
            IList<DmdDocument> projectOnlyDocuments = null;
            bool isPalsProject;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.Get(projectId);
                isPalsProject = project.IsPalsProject;

                if (isPalsProject)
                {
                    projectOnlyDocuments = project.ProjectOnlyDocuments;
                }
            }

            IList<DmdDocument> phaseDocuments;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phaseDocuments = phaseManager.Get(phaseId).Documents;
            }

            // get a list of phase documents
            // get a list of non-associated documents at project level
            return View(GetViewPath(), new DmdDocumentViewModel
            {
                BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, project.Name),
                Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,
                Mode = mode,
                Documents = phaseDocuments,
                ProjectDocuments = projectOnlyDocuments,
                ProjectDocumentId = project.ProjectDocumentId,
                ShowButtons = true,
                IsPalsProject = isPalsProject
            });
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult DocumentInput(int projectId, int phaseId, string action, DmdDocumentViewModel model)
        {
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var dataMartManager = new DataMartManager();
                dataMartManager.ProjectManager = projectManager;

                var project = projectManager.Get(projectId);

                if (Constants.ASSOCIATE.Equals(action) || Constants.UPLOAD.Equals(action))
                {
                    // get checked document ids
                    var ids = Request.Form["chkDocumentId"];

                    #region Associate PALS documents to a phase
                    // associate the documents to the phase
                    if (!string.IsNullOrEmpty(ids) && phaseId > 0)
                    {
                        var docIds = ids.Split(',');

                        // update checked project documents with phase id
                        foreach (string docId in docIds)
                        {
                            // get the project document by dmd id
                            var doc = project.ProjectDocuments.Where(x => x.DmdId == docId).FirstOrDefault();

                            // if document is found and it's not associated with any phase, set the phase id
                            if (doc != null && !doc.PhaseId.HasValue)
                            {
                                doc.PhaseId = phaseId;

                                // link phase document in PALS
                                if (!dataMartManager.LinkPhaseDocuments(projectId, phaseId, doc.DmdId))
                                {
                                    ModelState.AddModelError(string.Empty, string.Format("Unable to link the document {0} in {1}", doc.DmdId, Utilities.GetResourceString("PALS")));
                                }
                            }
                        }

                        // save
                        projectManager.UnitOfWork.Save();
                    }
                    #endregion Associate PALS documents to a phase

                    #region Associate uploaded documents to Non-PALS project
                    if (!model.IsPalsProject && Request.Files.Count > 0)
                    {
                        model.AddDocuments(Request.Files, Request, ModelState, projectId, phaseId);
                    }
                    #endregion Associate uploaded documents to Non-PALS project

                    //CARA-1379 add confirmation message
                    if (!model.IsWizardMode)
                    {
                        TempData[Constants.PHASE + Constants.DOCUMENT] = "Associated Document(s) with Comment Period";
                    }
                }
                else if (Constants.NEXT.Equals(action))
                {
                    var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);

                    return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                }
                else if (Constants.FINISH.Equals(action))
                {
                    return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                }
                else if (Constants.CANCEL.Equals(action))
                {
                    return RedirectToAction(Constants.INDEX, Constants.HOME);
                }
                else if (Constants.BACK.Equals(action))
                {
                    var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                    return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                }

                dataMartManager.Dispose();
            }

            return RedirectToAction("DocumentInput", new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ActionResult DeleteDocument(int projectId, int phaseId, string id, PageMode mode = PageMode.None)
        {
            bool success = false;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var project = projectManager.Get(projectId);
                var doc = project.ProjectDocuments.Where(x => x.DmdId == id).FirstOrDefault();

                if (doc != null)
                {
                    if (project.IsPalsProject)
                    {
                        // set phase ID to null to unassociate it from phase
                        doc.PhaseId = null;

                        // unlink the document from data mart
                        var dmManager = new DataMartManager();
                        if (!dmManager.UnlinkPhaseDocuments(projectId, phaseId, id))
                        {
                            throw new DataMartException(string.Format("Unable to unlink the document {0} from {1}", doc.DmdId, Utilities.GetResourceString("PALS")));
                        }
                        dmManager.Dispose();
                        success = true;
                    }
                    else
                    {
                        Document document = null;
                        using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                        {
                            try
                            {
                                project.ProjectDocuments.Remove(doc);

                                // No need to delete the record from the DMD.  That will be taken care of by the DMD service.
                                document = documentManager.Find(x => x.DmdId == id).First();
                                documentManager.Delete(document);
                                documentManager.UnitOfWork.Save();
                                success = true;
                            }
                            catch (ApplicationException ae)
                            {
                                if (document != null)
                                {
                                    log.WarnFormat("An error occurred trying to delete the document with ID: {0}::{1}", document.DocumentId, ae.Message);
                                }
                                else
                                {
                                    log.WarnFormat("No document found for DMD ID: {0}::{1}", id, ae.Message);
                                }
                            }
                        }
                    }

                    // save the changes
                    projectManager.UnitOfWork.Save();

                    //CARA-1379 add confirmation message
                    if (success)
                    {
                        TempData[Constants.PHASE + Constants.DOCUMENT] = "Removed Document";
                    }
                }
            }

            return RedirectToAction("DocumentInput", new { projectId = projectId, phaseId = phaseId, mode = mode });
        }
        #endregion

        #region Coding Template Input Actions
        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.UNIT)]
        public ViewResult CodingTemplateInput(int projectId, int phaseId, PageMode mode)
        {
            Project project;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.Get(projectId);
            }

            // pass a list of coding templates to the view
            if (mode == PageMode.Edit)
            {
                return View(GetViewPath(), new CodingTemplateViewModel
                {
                    PageTitle = string.Format("{0} {1}", Constants.EDIT, Constants.CODE),
                    Description = ProjectWizardManager.Instance.GetStep(PageMode.PhaseCreateWizard, Action).Description, // provides coding template page description in PageMode.Edit
                    Type = string.Empty,
                    Mode = mode
                });            
            }
            else
            {
                return View(GetViewPath(), new CodingTemplateViewModel
                {
                    BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                    PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, project.Name),
                    Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,                
                    Type = string.Empty,
                    Mode = mode
                });            
            }
        }

        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.UNIT)]
        [HttpPost]
        public ActionResult CodingTemplateInput(int projectId, int phaseId, string action, CodingTemplateViewModel model)
        {
            // get form data
            model.ProjectNameOrId = Request.Form.Get("txtProject");
            model.Type = Request.Form.Get("rbType");
            model.ProjectId = Request.Form.Get("rbProjectId");
            model.PhaseId = Request.Form.Get(string.Format("ddlPhase_{0}", model.ProjectId));
            model.CodingTemplateId = Request.Form.Get("rbCodingTemplateId");

            if (Constants.NEXT.Equals(action) || Constants.SAVE.Equals(action))
            {
                // validate inputs based on type
                if (Constants.ADD.Equals(model.Type))
                {
                    if (string.IsNullOrEmpty(model.CodingTemplateId))
                    {
                        ModelState.AddModelError(string.Empty, "Please select a coding template.");
                    }
                    else
                    {
                        try
                        {
                            // populate codes from coding template to the phase
                            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                            {
                                phaseManager.InsertPhaseCodes(int.Parse(model.CodingTemplateId), new Nullable<int>(), null, phaseId);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogError(ex);
                            ModelState.AddModelError(string.Empty, "An error occurred while saving the code.");
                        }
                    }
                }
                else if (Constants.REUSE.Equals(model.Type))
                {
                    if (string.IsNullOrEmpty(model.PhaseId))
                    {
                        ModelState.AddModelError(string.Empty, "Please select a Project and a Comment Period.");
                    }
                    else
                    {
                        try
                        {
                            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                            {
                                // populate codes from selected phase to the phase
                                phaseManager.InsertPhaseCodes(new Nullable<int>(), int.Parse(model.PhaseId), null, phaseId);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogError(ex);
                            ModelState.AddModelError(string.Empty, "An error occurred while saving the codes.");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Please select a coding template or a coding structure to reuse.");
                }

                // if succeed, go to next step
                if (ModelState.IsValid)
                {
                    if (Constants.SAVE.Equals(action))
                    {
                        return RedirectToAction(Constants.DETAILS, Constants.PHASE, new {phaseId = phaseId});
                    }
                    else
                    {
                        var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);
                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                }
            }
            else if ("Search Projects".Equals(action))
            {
                if (string.IsNullOrWhiteSpace(model.ProjectNameOrId) || model.ProjectNameOrId.Length < 4)
                {
                    ModelState.AddModelError(string.Empty, "Project search criteria is required.");
                }

                if (ModelState.IsValid)
                {                    
                    try
                    {
                        using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                        {
                            //Return projects with at least one phase.  Exclude projects with only one phase where the phase is the phase being created.
                            var projects = projectManager.FindProjects(model.ProjectNameOrId).Where(p => (p.Phases.Count > 0) & !((p.Phases.Count == 1) & (p.Phases.FirstOrDefault().PhaseId == phaseId))).ToList();

                            //Return project phases for ucProjectPhaseSelect grid's drop down list.  Exclude a phase if it is the phase being created.
                            foreach (var project in projects)
                            {
                                foreach (var ph in project.Phases.ToList())
                                {
                                    if (ph.PhaseId == phaseId)
                                    {
                                        project.Phases.Remove(ph);
                                    }
                                }                             
                                project.Phases.ToList();

                                model.Projects = projects;
                            }
                            //prevents context disposal.  allows eager loading.
                            projectManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        ModelState.AddModelError(string.Empty, string.Format("An error occurred while searching {0} projects.", Utilities.GetResourceString("AppNameAbbv")));
                    }
                }
            }
            else if (Constants.FINISH.Equals(action))
            {
                return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
            }
            else if (Constants.BACK.Equals(action))
            {
                var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
            }

            return View(GetViewPath(), model);
        }
        #endregion

        #region Code Input Actions
        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public ViewResult CodeInput(int projectId, int phaseId, PageMode mode)
        {
            PhaseCodeViewModel model;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var phase = phaseManager.Get(phaseId);
                model = new PhaseCodeViewModel
                {
                    BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                    PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, phase.Project.Name),
                    ProjectId = projectId,
                    PhaseId = phaseId,
                    PhaseTypeId = phase.PhaseTypeId,
                    Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,
                    Mode = mode
                };
            }

            // set the expanded node cookie initially to expand the categories default
            if (Request.Cookies[Constants.EXPANDED_PHASE_CODE] == null)
            {
                // get categories name and create a string as cookie value
                CodeUtilities.SetExpandedNodes(Request, Constants.EXPANDED_PHASE_CODE,
                    string.Join(";", LookupManager.GetLookup<LookupCodeCategory>().Select(x => x.Name).ToArray()));
            }

            // set the view data
            ViewData[Constants.EXPANDED_PHASE_CODE] = CodeUtilities.GetExpandedNodes(Request, Constants.EXPANDED_PHASE_CODE);            

            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult CodeInput(int projectId, int phaseId, PhaseCodeViewModel model)
        {
            // set model properties
            using (PhaseManager phaseMan = ManagerFactory.CreateInstance<PhaseManager>())
            {
                model.CheckedNodes = phaseMan.Get(model.PhaseId).PhaseCodes.Select(x => x.CodeId).ToList();
            }

            string nodeChangeCheck = Request.Form.Get("NodeChangeCheck");
            string nodeChangeUncheck = Request.Form.Get("NodeChangeUncheck");
            bool noChecks = bool.Parse(Request.Form.Get("NoChecks"));
            bool dirty = false;
            if (noChecks)
                model.CheckedNodes.Clear();
            if (!string.IsNullOrEmpty(nodeChangeCheck))
            {
                nodeChangeCheck
                    .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => int.Parse(x)).ToList().ForEach(x =>
                    {
                        if (!model.CheckedNodes.Contains(x))
                        {
                            model.CheckedNodes.Add(x);
                            dirty = true;
                        }
                    });
            }
            if (!string.IsNullOrEmpty(nodeChangeUncheck))
            {
                nodeChangeUncheck
                    .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => int.Parse(x)).ToList().ForEach(x =>
                    {
                        if (model.CheckedNodes.Contains(x))
                        {
                            model.CheckedNodes.Remove(x);
                            dirty = true;
                        }
                    });
            }

            if (!string.IsNullOrWhiteSpace(model.TaskAssignmentContext))
            {
                dirty = true;
            }

            // validate the model
            model.Validate(ModelState);

            if (ModelState.IsValid)
            {
                try
                {
                    // try to get it from the form element
                    if (!string.IsNullOrEmpty(model.ActionString))
                    {
                        if (model.ActionString.Contains("Reset"))
                        {
                            ClearCodes(phaseId);
                            return RedirectToAction("CodingTemplateInput", Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = PageMode.Edit });
                        }
                        else
                        {
                            // save the codes
                            if (dirty)
                            {
                                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                                {
                                    phaseManager.InsertPhaseCodes(null, null, Utilities.ToIdListXml(model.CheckedCodeIdList), phaseId);
                                    model.ClearPhaseTreeCache();
                                }
                                //CARA-1379 add confirmation message
                                if (!model.IsWizardMode)
                                {
                                    TempData[Constants.PHASE + Constants.CODE] = "Updated codes associated with Comment Period";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, "An error occurred while saving the codes.");
                }

                if (!string.IsNullOrEmpty(model.ActionString))
                {
                    if (model.ActionString.Equals(Constants.NEXT))
                    {
                        // if succeed, go to the next step
                        var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);

                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.FINISH))
                    {
                        return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.BACK))
                    {
                        var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                }
            }

            // set view data for checked codes and expanded nodes
            ViewData["checkedNodes"] = model.CheckedNodes;
            ViewData[Constants.EXPANDED_PHASE_CODE] = CodeUtilities.GetExpandedNodes(Request, Constants.EXPANDED_PHASE_CODE);

            return View(GetViewPath(), model);
        }

        public PartialViewResult PreviewCodes(int projectId, int phaseId, bool startEmpty, string checkedCodeIds, string uncheckedCodeIds)
        {
            var phaseCodes = new List<PhaseCode>();
            // parse the code id list into a list of code ids
            List<int> addCodes = checkedCodeIds.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries).ToList()
                .ConvertAll(x => int.Parse(x));
            List<int> removeCodes = uncheckedCodeIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList()
                .ConvertAll(x => int.Parse(x));

            if (phaseId > 0)
            {
                // get the existing phase codes from the phase
                using (var phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                {
                    phaseCodes = phaseCodeManager.All.Where(x => x.PhaseId == phaseId).ToList();
                    phaseCodes.RemoveAll(x => removeCodes.Contains(x.CodeId));

                    // get the code ids from the list of existing phase codes
                    var phaseCodeCodeIds = phaseCodes.Select(x => x.CodeId).ToList();

                    // for the codes that not yet added to the phase, get from the code manager
                    using (var codeManager = ManagerFactory.CreateInstance<CodeManager>())
                    {
                        var codes = codeManager.All.Where(x => addCodes.Contains(x.CodeId) && !phaseCodeCodeIds.Contains(x.CodeId)).ToList();

                        if (codes != null && codes.Count > 0)
                        {
                            phaseCodes.AddRange(codes.Select(x => (CodeUtilities.ToPhaseCode(phaseId, x))));
                        }
                    }
                }
            }

            // render the popup with the codes
            return PartialView("~/Views/Shared/Details/ucPhaseCodesDetailsPrint.ascx", new PhaseCodesViewModel
            {
                PhaseCodes = phaseCodes,
                TreeViewName = Constants.PREVIEW,
                ExpandAll = true
            });
        }
        #endregion

        #region Code Assignment Actions
        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public ViewResult CodeTaskAssignment(int projectId, int phaseId, PageMode mode)
        {
            CodeTaskAssignmentViewModel model;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var phase = phaseManager.Get(phaseId);
                model = new CodeTaskAssignmentViewModel
                {
                    BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                    PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, phase.Project.Name),
                    ProjectId = projectId,
                    PhaseId = phaseId,
                    PhaseTypeId = phase.PhaseTypeId,
                    Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,
                    Mode = mode
                };
            }
            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult CodeTaskAssignment(int projectId, int phaseId, CodeTaskAssignmentViewModel model)
        {
            bool dirty = false;

            if (!string.IsNullOrWhiteSpace(model.TaskAssignmentContext))
            {
                dirty = true;
            }

            // validate the model
            model.Validate(ModelState);
            if (ModelState.IsValid)
            {
                try
                {
                    // save the Assignments
                    if (dirty)
                    {
                        List<KeyValuePair<int, int>> updateList = new List<KeyValuePair<int, int>>();
                        string[] arrContextIds = model.TaskAssignmentContext.Split('|');
                        string[] arrUserIds = model.TaskAssignmentUsers.Split('|');
                        for (int i = 0; i < arrContextIds.Count(); i++)
                        {
                            updateList.Add(new KeyValuePair<int, int>(int.Parse(arrContextIds[i]), int.Parse(arrUserIds[i])));
                        }
                        List<int> phaseCodeIdsToUpdate = updateList.Select(x=>x.Key).ToList();
                        List<KeyValuePair<int, int?>> oldUserIds = new List<KeyValuePair<int,int?>>();
                        using (PhaseCodeManager phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                        {
                            //update codes and children
                            updateList.ForEach(updateItem =>
                                {
                                    int? userId = null;
                                    if (updateItem.Value > 0)
                                    {
                                        userId = updateItem.Value;
                                    }
                                    oldUserIds.AddRange(
                                        UpdateChildren(updateItem.Key, userId, phaseCodeIdsToUpdate, phaseCodeManager));
                                });
                            phaseCodeManager.UnitOfWork.Save();
                        }
                        #region update comments if neccessary
                        if (model.UpdateComments)
                        {
                            using (CommentManager commentManager = ManagerFactory.CreateInstance<CommentManager>())
                            {
                                //create a list of comments that have codes with updated users ids 
                                //and those comments either have no user or have the same user as what the code used to have
                                List<int> relaventCodeIds = oldUserIds.Select(x => x.Key).ToList();
                                List<Comment> commentsToUpdate =
                                    commentManager
                                    .All
                                    .Where(comment => comment.ConcernResponseId == null && relaventCodeIds
                                        .Any(x=>comment.CommentCodes.Select(code=>code.PhaseCodeId).Contains(x)))
                                    .ToList();
                                commentsToUpdate.ForEach(comment =>
                                    {
                                        //if the userid is null then find the first 200 updated code
                                        IEnumerable<CommentCode> relevantCodes =
                                            comment.CommentCodes
                                            .Where(code =>
                                                oldUserIds.Select(x => x.Key).Contains(code.PhaseCodeId) &&
                                                code.PhaseCode.CodeNumber.StartsWith("2"))
                                            .OrderBy(code => code.PhaseCode.CodeNumber);
                                        //if the comment has a userid then find the first 200 updated code that had that user
                                        if (comment.UserId != null)
                                        {
                                            relevantCodes = relevantCodes.Where(code => oldUserIds
                                                .FirstOrDefault(x=>x.Key==code.PhaseCodeId).Value == comment.UserId);
                                        }
                                        //if no 200 codes found look for other codes
                                        if (!relevantCodes.Any())
                                        {
                                            relevantCodes =
                                                comment.CommentCodes
                                                .Where(code =>
                                                    oldUserIds.Select(x => x.Key).Contains(code.PhaseCodeId))
                                                .OrderBy(code => code.PhaseCode.CodeNumber);
                                            //if the comment has a userid make sure the code had that user id
                                            if (comment.UserId != null)
                                            {
                                                relevantCodes = relevantCodes.Where(code => oldUserIds
                                                    .FirstOrDefault(x => x.Key == code.PhaseCodeId).Value == comment.UserId);
                                            }
                                        }
                                        if (relevantCodes.Any() && 
                                            relevantCodes.FirstOrDefault().PhaseCode.UserId != comment.UserId)
                                        {
                                            comment.UserId = relevantCodes.FirstOrDefault().PhaseCode.UserId;
                                            comment.DateAssigned = DateTime.UtcNow;
                                        }

                                    });
                                commentManager.UnitOfWork.Save();
                            }
                        }
                        #endregion //update comments

                        //CARA-1379 
                        if (!model.IsWizardMode)
                        {
                            TempData[Constants.PHASE + Constants.CODE + Constants.TASK] = "Updated Code Task Assignments"; 
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, "An error occurred while saving the code assignments.");
                }

                if (!string.IsNullOrEmpty(model.ActionString))
                {
                    if (model.ActionString.Equals(Constants.NEXT))
                    {
                        // if succeed, go to the next step
                        var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);

                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.FINISH))
                    {
                        return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                    else if (model.ActionString.Equals(Constants.BACK))
                    {
                        var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                        return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                    }
                }
            }

            return View(GetViewPath(), model);
        }
        #endregion //Code Assignment Actions

        #region Term List Input Actions
        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ViewResult TermListInput(int projectId, int phaseId, PageMode mode)
        {
            IList<TermList> list;
            string projectName;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var phase = phaseManager.Get(phaseId);
                projectName = phase.Project.Name;

                // Get a list of term lists available in the system
                // (should revisit this)
                using (var termListManager = ManagerFactory.CreateInstance<TermListManager>())
                {
                    list = termListManager.All.ToList();
                }

                if (phase != null)
                {
                    // Get a list of term lists associated with the phase
                    var termLists = phase.TermLists;

                    // populate the selections only if there are term lists
                    if (termLists != null && termLists.Count > 0)
                    {
                        // Loop through and find the selected records
                        foreach (TermList tl in list)
                        {
                            tl.DefaultOn = termLists.Any(t => t.TermListId == tl.TermListId);
                        }
                    }
                }
            }

            return View(GetViewPath(), new TermListViewModel
            {
                BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, Action)),
                PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, projectName),
                Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,
                Mode = mode,
                TermLists = list
            });
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult TermListInput(int projectId, int phaseId, string action, TermListViewModel model)
        {
            // get the checked ids
            string ids = Request.Form["chkTermListId"];

            if (Constants.FINISH.Equals(action) || Constants.SAVE.Equals(action))
            {
                // validate
                if (string.IsNullOrEmpty(ids))
                {
                    ModelState.AddModelError(string.Empty, string.Format("Please select an {0}.", Utilities.GetResourceString(Constants.TERM_LIST)));
                }
                else
                {
                    try
                    {
                        // save the selected term lists
                        using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            phaseManager.InsertTermLists(phaseId, Utilities.ToIdListXml(ids));
                            

                            // Clear the cache used by the letter manager.
                            // This will have to be changed if we ever implement autoscaling
                            using (MemoryCache cache = MemoryCache.Default)
                            {
                                string key = string.Format("PhaseTermLists_{0}", phaseId);
                                if (cache.Contains(key))
                                {
                                    cache.Remove(key);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        ModelState.AddModelError(string.Empty, string.Format("An error occurred while saving the {0}s.", Utilities.GetResourceString(Constants.TERM_LIST)));
                    }
                }

                if (ModelState.IsValid)
                {
                    //CARA-1379 add confirmation message
                    if (!model.IsWizardMode)
                    {
                        TempData[Constants.PHASE + Constants.TERM_LIST] = "Updated Comment Period Auto-Markup Lists";
                    }
                    // redirect to the destination page
                    if (Constants.FINISH.Equals(action))
                    {
                        return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { phaseId = phaseId, mode = model.Mode });
                    }
                }
            }
            else if (Constants.BACK.Equals(action))
            {
                var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
            }

            // Get a list of term lists available in the system
            using (var termListManager = ManagerFactory.CreateInstance<TermListManager>())
            {
                model.TermLists = termListManager.All.ToList();
            }

            var intIds = Utilities.ToIntArray(ids);

            // Loop through and find the selected records
            foreach (TermList tl in model.TermLists)
            {
                tl.DefaultOn = intIds.Any(t => tl.TermListId == t);
            }

            return View(GetViewPath(), model);
        }
        #endregion

        #region Concern Response Template Input Actions

        public ViewResult ConcernResponseTemplateInput(int projectId, int phaseId, PageMode mode)
        {
            Phase phase = null;
            Project project = null;
            string concernTemplate = "";
            string responseTemplate = "";
            using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);
                project = phase.Project;
                concernTemplate = phase.ConcernTemplate;
                responseTemplate = phase.ResponseTemplate;
            }

            // create the view model
            var model = new ConcernTemplateInputViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                BreadCrumbTitle = GetBreadCrumbText(mode, Utilities.GetWizardPageTitle(mode, ControllerAction, ControllerAction)),
                PageTitle = Utilities.GetWizardPageTitle(mode, ControllerAction, Action, project.Name),
                Description = ProjectWizardManager.Instance.GetStep(mode, Action).Description,
                Mode = mode,
                ConcernText = concernTemplate,
                ResponseText = responseTemplate
            };

            return View(GetViewPath(), model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ConcernResponseTemplateInput(string action, int projectId, int phaseId, ConcernTemplateInputViewModel model)
        {

            // validate the model
            model.Validate(ModelState);
            if (ModelState.IsValid)
            {
                try
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        Phase phase = phaseManager.Get(phaseId);
                        phase.ConcernTemplate = model.ConcernText;
                        phase.ResponseTemplate = model.ResponseText;
                        phaseManager.UnitOfWork.Save();
                    }
                    
                    if (!string.IsNullOrEmpty(model.ActionString))
                    {
                        if (model.ActionString.Equals(Constants.NEXT))
                        {
                            // if succeed, go to the next step
                            var nextStep = ProjectWizardManager.Instance.GetNextWizardStep(model.Mode, Action);

                            return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                        }
                        else if (model.ActionString.Equals(Constants.FINISH))
                        {
                            return RedirectToAction(Constants.SUMMARY, Constants.PHASE, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                        }
                        else if (model.ActionString.Equals(Constants.BACK))
                        {
                            var nextStep = ProjectWizardManager.Instance.GetPrevWizardStep(model.Mode, Action);

                            return RedirectToAction(nextStep.Action, nextStep.Controller, new { projectId = projectId, phaseId = phaseId, mode = model.Mode });
                        }
                    }
                    //CARA-1379 add confirmation message
                    if (!model.IsWizardMode)
                    {
                        TempData[Constants.PHASE + Constants.CONCERN_RESPONSE + Constants.TEMPLATE] = "Updated Concern Response Template";
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, "An error occurred while saving the Concern Response Template");
                }

                
            }

            return View(GetViewPath(), model);
        }
        #endregion Concern Response Template Input Actions

        #region Wizard Actions
        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ViewResult Summary(int projectId, int phaseId, PageMode mode)
        {
            Project project;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.Get(projectId);

                // lazy load properties for the view
                var activities = project.ProjectActivityNameList;
            }

            Phase phase;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);

                // lazy load properties for the view
                var pmr = phase.PhaseMemberRoles;
                var docs = phase.Documents;
                var codes = phase.PhaseCodes;
                var termLists = phase.TermLists;
            }

            // Get project details
            return View(GetViewPath(), new SummaryViewModel
            {
                BreadCrumbTitle = Utilities.GetResourceString(Action),
                PageTitle = Utilities.GetResourceString(Action),
                Mode = mode,
                Project = project,
                Phase = phase
            });
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public RedirectToRouteResult Accept(int projectId, int phaseId)
        {
            NotificationManager.SendWebLinksNotification(phaseId, Utilities.GetUserSession());

            return RedirectToAction(Constants.DETAILS, Constants.PROJECT, new { projectId = projectId });
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ActionResult CancelWizard(int projectId, int phaseId, string mode)
        {
            // delete the project or phase from the db
            if (PageMode.ProjectCreateWizard.ToString().Equals(mode))
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    var project = projectManager.Get(projectId);

                    if (project != null)
                    {
                        projectManager.Delete(project);
                        projectManager.UnitOfWork.Save();
                    }
                }

                // redirect to the default page after the deletion
                return RedirectToAction(Constants.INDEX, Constants.HOME);
            }
            else if (PageMode.PhaseCreateWizard.ToString().Equals(mode))
            {
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phase = phaseManager.Get(phaseId);

                    // delete the phase
                    if (phase != null)
                    {
                        phaseManager.Delete(phase);
                        phaseManager.UnitOfWork.Save();
                    }
                }

                // redirect to the default page after the deletion
                return RedirectToAction(Constants.DETAILS, Constants.PROJECT, new { projectId = projectId });
            }

            return RedirectToAction(Constants.INDEX, Constants.HOME);
        }
        #endregion

        #region Phase Actions
        [HasPrivilege(PrivilegeCodes = "ARPH", Scope = Constants.PHASE)]
        public ActionResult UpdateStatus(int projectId, int phaseId, string type)
        {
            IList<Report> reports = null;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                // update the status
                if (Constants.ACTIVATE.Equals(type))
                {
                    try
                    {
                        phaseManager.Reactivate(phaseId);
                        phaseManager.UnitOfWork.Save();
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        TempData[Constants.ERROR] = string.Format("An error occurred while reactivating the {0}.", Utilities.GetResourceString(Constants.PHASE));
                    }
                }
                else if (Constants.ARCHIVE.Equals(type))
                {
                    try
                    {
                        bool isPalsProject = false;
                        using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                        {
                            isPalsProject = projectManager.Get(projectId).IsPalsProject;
                        }

                        if (isPalsProject)
                        {
                            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
                            {
                                reports = reportManager.Find(x => x.ArtifactTypeId != null).ToList();

                                // create a list of reports to send to data mart
                                foreach (var report in reports)
                                {
                                    if (!report.HasReportOptions)
                                    {
                                        report.Document = ReportFactory.GetReport(report.ReportId, phaseId);
                                    }
                                    else
                                    {
                                        var option = report.ReportOptions.First(x => x.IsArtifact.Value).Value;
                                        report.Document = ReportFactory.GetReport(report.ReportId, phaseId, option);
                                    }
                                }
                            }
                        }

                        // archive the phase
                        phaseManager.Archive(phaseId, reports, Utilities.GetUserSession(), isPalsProject);
                        phaseManager.UnitOfWork.Save();
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        TempData[Constants.ERROR] = string.Format("An error occurred while archiving the {0}.", Utilities.GetResourceString(Constants.PHASE));
                    }
                }
            }

            return RedirectToAction(Constants.DETAILS);
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        public ActionResult UpdateAccessStatus(int projectId, int phaseId, string type)
        {
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phaseManager.UpdateAccessStatus(phaseId, type);
                phaseManager.UnitOfWork.Save();
            }

            return RedirectToAction(Constants.DETAILS);
        }

        [HasPrivilege(PrivilegeCodes = "ACRR", Scope = Constants.PHASE)]
        public ActionResult UpdateReadingRoomStatus(int projectId, int phaseId, string type)
        {
            try
            {
                // get the phase
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phase = phaseManager.Get(phaseId);

                    // update the reading room active flag
                    if (phase != null)
                    {
                        bool isActive = type.Equals(Constants.ACTIVATE);
                        phase.LastUpdatedBy = Utilities.GetUserSession().Username;
                        phaseManager.UpdateReadingRoomStatus(phase, isActive);  // Unit of Work called inside this method
                        phaseManager.UnitOfWork.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                TempData[Constants.ERROR] = string.Format("An error occurred while trying to {1} the {0} status.", Utilities.GetResourceString(Constants.READING_ROOM), type.ToLower());
            }

            return RedirectToAction(Constants.DETAILS);
        }

        [HasPrivilege(PrivilegeCodes = "ACPC", Scope = Constants.PHASE)]
        public ActionResult UpdatePublicCommentStatus(int projectId, int phaseId, string type)
        {
            try
            {
                // get the phase
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phase = phaseManager.Get(phaseId);

                    // update the reading room active flag
                    if (phase != null)
                    {
                        bool isActive = type.Equals(Constants.ACTIVATE);
                        phase.PublicCommentActive = isActive;
                        phase.LastUpdatedBy = Utilities.GetUserSession().Username;
                        phaseManager.UnitOfWork.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                TempData[Constants.ERROR] = string.Format("An error occurred while trying to {1} the {0} status.", Utilities.GetResourceString(Constants.PUBLIC_COMMENT_INPUT), type.ToLower());
            }

            return RedirectToAction(Constants.DETAILS);
        }

        [HasPrivilege(PrivilegeCodes = "MPHS", Scope = Constants.PHASE)]
        [HttpPost]
        public JsonResult ExtendObjectionResponseDueDate(int projectId, int phaseId)
        {
            JsonResult result;
            int days = int.Parse(Request.Params["days"]);

            try
            {
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phase = phaseManager.Get(phaseId);
                    phase.ObjectionResponseExtendedBy = days;
                    phaseManager.UnitOfWork.Save();
                }

                result = Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                result = Json(new
                { 
                    success = false, 
                    error = "A problem occurred trying to update the Response Extended By field."
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(result);
        }
        #endregion

        #region Custom Phase Code Actions
        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public PartialViewResult CreateCode(int projectId, int phaseId, int id, string name)
        {
            var pc = new PhaseCode();
            PhaseCodeInputViewModel model;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                // if a code is selected, set the code category
                if (id > 0)
                {
                    using (var codeManager = ManagerFactory.CreateInstance<CodeManager>())
                    {
                        // custom code and phase code have different ways of getting the code category id
                        pc.CodeCategoryId = (id >= Constants.CUSTOM_CODE_ID_SEQ ? phaseManager.GetPhaseCodes(
                            new PhaseCodeSelectFilter
                            {
                                PhaseId = phaseId
                            }
                        ).Where(x => x.CodeId == id).FirstOrDefault().CodeCategoryId.Value : codeManager.Get(id).CodeCategoryId);
                    }
                }
                // else if category is selected, get the next available code
                else if (!string.IsNullOrEmpty(name))
                {
                    // get the code category by selected category name
                    var category = LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.Name == name).FirstOrDefault();

                    if (category != null)
                    {
                        // set the new phase code data
                        pc.CodeCategoryId = category.CodeCategoryId;
                        pc.CodeNumber = CodeUtilities.GetNextCodeNumber(phaseId, category.CodeCategoryId, category.Prefix + "00");
                    }
                }

                // create new model
                model = new PhaseCodeInputViewModel
                {
                    PhaseId = phaseId,
                    PhaseTypeId = phaseManager.Get(phaseId).PhaseTypeId,
                    SelectedCodeId = id,
                    PhaseCode = pc,
                    Child = true
                };
            }

            // generate next available code number if a code is selected
            if (id > 0)
            {
                model.PhaseCode.CodeNumber = model.NextChildCodeNumber;
            }

            // render the popup with input fields
            return PartialView("~/Views/Shared/Input/ucPhaseCodeCreate.ascx", model);
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public int CreatePhaseCode(int phaseId, int? selectedCodeId, int codeCategoryId, string codeNumber, string codeName)
        {
            int id = 0;
            if (phaseId > 0 && codeCategoryId > 0 && !string.IsNullOrEmpty(codeNumber) && !string.IsNullOrEmpty(codeName))
            {
                PhaseCode pc;
                using (var phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                {
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        // create a new phase code with the next available custom code identity
                        pc = new PhaseCode
                        {
                            CodeId = phaseManager.GetNextSequence("Custom Code ID"),
                            PhaseId = phaseId,
                            CodeCategoryId = codeCategoryId,
                            CodeNumber = CodeUtilities.ToCodeNumber(codeNumber),
                            CodeName = codeName,
                            CanRemove = true,
                            ParentCodeId = (selectedCodeId.HasValue && selectedCodeId.Value > 0 ? selectedCodeId : null)
                        };

                        // save the data
                        phaseCodeManager.Add(pc);
                        phaseCodeManager.UnitOfWork.Save();
                    }
                }

                // set the new code id
                id = pc.CodeId;
            }

            return id;
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public PartialViewResult EditCode(int projectId, int phaseId, int id)
        {
            PhaseCodeInputViewModel model;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                // create new model
                model = new PhaseCodeInputViewModel
                {
                    PhaseId = phaseId,
                    PhaseTypeId = phaseManager.Get(phaseId).PhaseTypeId,
                    PhaseCode = phaseManager.GetPhaseCodes(
                        new PhaseCodeSelectFilter
                        {
                            PhaseId = phaseId
                        }
                    ).Where(x => x.CodeId == id).FirstOrDefault().PhaseCode
                };
            }

            // render the popup with input fields
            return PartialView("~/Views/Shared/Input/ucPhaseCodeEdit.ascx", model);
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public bool UpdatePhaseCode(int phaseCodeId, string codeName)
        {
            bool result = false;

            if (phaseCodeId > 0 && !string.IsNullOrEmpty(codeName))
            {
                try
                {
                    using (var phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                    {
                        var pc = phaseCodeManager.Get(phaseCodeId);

                        if (pc != null)
                        {
                            // update code name
                            pc.CodeName = codeName;

                            // save the work
                            phaseCodeManager.UnitOfWork.Save();
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    result = false;
                }
            }

            return result;
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion

        #region Private Methods
        [NonAction]
        private string GetBreadCrumbText(PageMode mode, string text)
        {
            return (mode.Equals(PageMode.ProjectCreateWizard) ? Utilities.GetResourceString(mode.ToString()) : text);
        }

        private void ClearCodes(int phaseid)
        {
            //Delete all of the comments and codes created for the comment period
            using (CommentManager commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                commentManager.Find(x => x.Letter.PhaseId == phaseid).ToList()
                    .ForEach(x =>
                    {
                        x.Letter.CodedText = LetterUtilities.RemoveCommentFromText
                            (x.Letter.CodedText, string.Format("{0}-{1}", x.Letter.LetterSequence, x.CommentNumber));
                        commentManager.Delete(x);
                    });
                commentManager.UnitOfWork.Save();
            }
            //Deletes all of the codes from the PhaseCode table
            //Delete all custom codes for the comment period
            using (PhaseCodeManager phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
            {
                phaseCodeManager.Find(x => x.PhaseId == phaseid).ToList()
                    .ForEach(x => phaseCodeManager.Delete(x));
                phaseCodeManager.UnitOfWork.Save();
            }
        }

        List<KeyValuePair<int,int?>> UpdateChildren(int phaseCodeId, int? userId, List<int> phaseCodeIdsToUpdate, PhaseCodeManager phaseCodeManager)
        {
            List<KeyValuePair<int, int?>> retVal = new List<KeyValuePair<int, int?>>();
            PhaseCode parentCode = phaseCodeManager.Get(phaseCodeId);
            List<PhaseCode> childCodesToUpdate = phaseCodeManager.All
                .Where(x => x.ParentCodeId == parentCode.CodeId &&
                    x.PhaseId == parentCode.PhaseId &&
                    (x.UserId == null || x.UserId == parentCode.UserId) && 
                    !phaseCodeIdsToUpdate.Contains(x.PhaseCodeId))
                .ToList();
            retVal.Add(new KeyValuePair<int, int?>(phaseCodeId, parentCode.UserId));
            parentCode.UserId = userId;
            childCodesToUpdate.ForEach(childCode =>
                {
                    retVal.AddRange(UpdateChildren(childCode.PhaseCodeId, userId, phaseCodeIdsToUpdate, phaseCodeManager));
                });
            return retVal;
        }

        /// <summary>
        /// Saves the code-user task assosication
        /// </summary>
        /// <param name="context">list of codes delineated by '|'</param>
        /// <param name="userIds">list of users delineated by '|'</param>
        private bool SaveCodeTaskAssignments(string context, string userIds, int phaseId)
        {
            bool retVal = false;
            string[] strArrContext = context.Split('|');
            string[] strArrUserIds = userIds.Split('|');
            //make sure there are the same number of ids
            if (strArrContext.Count() != strArrUserIds.Count())
            {
                return retVal;
            }
            using (PhaseCodeManager phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
            {
                for (int i = 0; i < strArrContext.Count(); i++)
                {
                    int codeId = Int32.Parse(strArrContext[i]);
                    int userId = Int32.Parse(strArrUserIds[i]);
                    PhaseCode phaseCode = phaseCodeManager.All.FirstOrDefault(x => x.PhaseId == phaseId && x.CodeId == codeId);
                    if (phaseCode != null)
                    {
                        if (userId == 0)
                        {
                            phaseCode.UserId = null;
                        }
                        else
                        {
                            phaseCode.UserId = userId;
                        }
                    }
                }
                phaseCodeManager.UnitOfWork.Save();
            }
            return true;
        }
        #endregion
    }
}
