﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the error specific actions
    /// </summary>
    public class ErrorController : Controller
    {
        #region Action Methods
        public ViewResult Error()
        {
            return View();
        }

        public ViewResult NotAuthorized()
        {
            return View();
        }
        #endregion
    }
}
