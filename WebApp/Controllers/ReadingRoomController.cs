﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the reading room specific actions
    /// </summary>
    public class ReadingRoomController : PageController
    {
        #region Private Members
        #endregion

        #region Core Action Methods
        [HasPrivilege(PrivilegeCodes = "VWRR", Scope = Constants.PHASE)]
        public ViewResult List(int projectId, int phaseId)
        {
            // get the filter
            int pageNum = Request.Params.AllKeys.Contains("List-page") ? Convert.ToInt32(Request.Params["List-page"]) : 1;
            string sortBy = Request.Params["List-orderBy"];
            if (!string.IsNullOrEmpty(sortBy))
            {
                sortBy = sortBy.Replace('-', ' ');
            }

            LetterReadingRoomSearchResultFilter filter = Utilities.GetFilter(Constants.READING_ROOM_MGMT, phaseId.ToString()) as LetterReadingRoomSearchResultFilter;
			if (filter == null)
			{
                filter = new LetterReadingRoomSearchResultFilter();
			}

            // translate the filter type from request param
            filter.SetFilterByType(GetParam(Constants.FILTER));

            filter.PageNum = pageNum;
			filter.PhaseId = phaseId;
            filter.SortKey = sortBy;

            // set the filter
            Utilities.SetFilter(Constants.READING_ROOM_MGMT, phaseId.ToString(), filter);

            // Create the view model
			int total;
            var viewModel = new LetterReadingRoomListViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                Letters = ReadingRoomService.GetLettersReadingRoom(
                    Constants.READING_ROOM_MGMT, 
                    new GridCommand { 
                        PageSize = Utilities.GetUserSession().SearchResultsPerPage, 
                        Page = filter.PageNum.Value },
                    phaseId.ToString(),
                    out total, 
                    out pageNum),
                Mode = PageMode.None
            };

            viewModel.LoadFilterToModel(filter);
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                // Turn off lazy loading
                projectManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                // Force load all of the properties needed in one query
                var project = projectManager.Get(projectId);
                viewModel.ProjectNumber = project.ProjectNumber;
                viewModel.IsPalsProject = project.IsPalsProject;
            }

            ViewData["total"] = total;
			ViewData["pageNum"] = pageNum;

			// See if there are errors from trying to update letters
			if (TempData["LetterUpdateErrorMessages"] != null)
			{
				foreach (string message in (IList<string>) TempData["LetterUpdateErrorMessages"])
				{
					ModelState.AddModelError(string.Empty, message);
				}
			}

            return View(GetViewPath(), viewModel);
        }

        [HasPrivilege(PrivilegeCodes = "VWRR", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult List(int projectId, int phaseId, string action, LetterReadingRoomListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // save the filter in the session
                    Utilities.SetFilter(Constants.READING_ROOM_MGMT, phaseId.ToString(), model.GetFilterFromModel() as LetterReadingRoomSearchResultFilter);
                }

				ViewData["total"] = 0;
				ViewData["pageNum"] = 1;

                // pass the view model, list binding method will get the data
                return RedirectToAction(Constants.LIST);
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
				Utilities.SetFilter(Constants.READING_ROOM_MGMT, phaseId.ToString(), new LetterReadingRoomSearchResultFilter { PageNum = 1 });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST);
            }
            else if (Constants.UPDATE.Equals(action))
            {
                // update the letters
				IList<string> errorMessages = model.UpdateLetters(Request);

                if (errorMessages != null && errorMessages.Count > 0)
                {
                    TempData["LetterUpdateErrorMessages"] = errorMessages;
                }
                else
                {
                    TempData[Constants.READING_ROOM_MGMT] = "Reading Room(s) Updated";
                }

                // pass the view model, list binding method will get the data
                int pageNum = Request.Params.AllKeys.Contains("List-page") ? Convert.ToInt32(Request.Params["List-page"]) : 1;
                System.Web.Routing.RouteValueDictionary routeVals = new System.Web.Routing.RouteValueDictionary();
                routeVals.Add("List-page", pageNum);
                return RedirectToAction(Constants.LIST, routeVals);
            }

            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ReadingRoomListBinding(GridCommand command)
        {
            string context = Request.RequestContext.RouteData.Values["phaseId"].ToString();
			int total;
			int pageNum;
            var data = ReadingRoomService.GetLettersReadingRoom(Constants.READING_ROOM_MGMT, command, context, out total, out pageNum).ToList();

			ViewData["pageNum"] = pageNum;

            return View(GetViewPath(Constants.LIST), new GridModel { Data = data, Total = total });
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion
    }
}
