﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

using Telerik.Web.Mvc;
using Telerik.Web.Mvc.Infrastructure;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the form set specific actions
    /// </summary>
    public class FormController : PageController
    {
        #region Private Members
        #endregion

        #region Core Action Methods
        [GridAction(GridName = Constants.LIST)]
        public ViewResult List(int projectId, int phaseId, GridCommand command)
        {
            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            // Create the view model
            var viewModel = LoadFormSetList(projectId, phaseId, isActive,
                Utilities.GetPageTitle(ControllerAction), Utilities.GetPageTitle(ControllerAction), command);

            return View(GetViewPath(), viewModel);
        }

        [HttpPost]
        public ActionResult List(int projectId, int phaseId, string action, FormSetListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // save the filter in the session
                    Utilities.SetFilter(Constants.FORM_SET, phaseId.ToString(), model.GetFilterFromModel() as FormSetSearchResultFilter);
                }

                ViewData["total"] = 0;

                // pass the view model, list binding method will get the data
                return RedirectToAction(Constants.LIST);
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
				Utilities.SetFilter(Constants.FORM_SET, phaseId.ToString(), new FormSetSearchResultFilter { PageNum = 1 });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST);
            }

            return View(GetViewPath(), model);
        }

        [GridAction(GridName = Constants.LIST)]
        public ViewResult LetterList(int projectId, int phaseId, GridCommand command, string confirmMessage = "")
        {
            if (!string.IsNullOrWhiteSpace(confirmMessage))
            {
                TempData[Constants.FORM + Constants.LETTER + Constants.LIST] = confirmMessage;
            }
            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            // Create the view model
            var viewModel = LoadFormLetterList(projectId, phaseId, isActive, command);

            return View(GetViewPath(), viewModel);
        }

        [HttpPost]
        public ActionResult LetterList(int projectId, int phaseId, string action, FormSetListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // save the filter in the session
                    Utilities.SetFilter(Constants.FORM_SET + Constants.LETTERS, phaseId.ToString(), model.GetFilterFromModel() as FormSetSearchResultFilter);
                }

                ViewData["total"] = 0;

                // pass the view model, list binding method will get the data
                return RedirectToAction("LetterList");
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
                Utilities.SetFilter(Constants.FORM_SET + Constants.LETTERS, phaseId.ToString(), new FormSetSearchResultFilter { PageNum = 1, FormSetId = model.FormSetId });

                // redirect to the page again to clear the page settings
                return RedirectToAction("LetterList");
            }

            return View(GetViewPath(), model);
        }
        #endregion

        #region Form Set Action Methods
        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public PartialViewResult SelectFormSet(int projectId, int phaseId, int id)
        {
            // get the letter to get the form set id
            IList<FormSet> formSets;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                var letter = letterManager.Get(id);

                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var formSetsQ = formSetManager.All.Where(x => x.PhaseId == phaseId);
                    if (letter.FormSetId.HasValue)
                    {
                        formSetsQ = formSetsQ.Where(x => x.FormSetId != letter.FormSetId.Value);
                    }
                    formSets = formSetsQ.ToList();
                }
            }

            // render the popup with input fields
            return PartialView("~/Views/Shared/List/ucFormSetSelect.ascx", new FormSetSelectViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                LetterId = id,
                FormSets = formSets
            });
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public PartialViewResult CreateFormSet(int projectId, int phaseId, int id)
        {
            // render the popup with input fields
            return PartialView("~/Views/Shared/Input/ucFormSetInput.ascx", new FormSetInputViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                FormSet = new FormSet { PhaseId = phaseId, MasterFormId = id },
                Mode = PageMode.Create
            });
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public PartialViewResult EditFormSet(int projectId, int phaseId, int id)
        {
            FormSet formSet;
            using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
            {
                formSet = formSetManager.Get(id); 
            }

            // render the popup with input fields
            return PartialView("~/Views/Shared/Input/ucFormSetInput.ascx", new FormSetInputViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                FormSet = formSet,
                Mode = PageMode.Edit
            });
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public bool UpdateFormSet(int projectId, int phaseId, int id, string name)
        {
            // rename the form set
            return FormUtilities.RenameFormSet(id, name);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public int CreateFormSetWithMasterForm(int projectId, int phaseId, int id, string name)
        {
            // create a form set with the master letter
            return FormUtilities.CreateFormSetWithMasterForm(phaseId, id, name);
        }

        [HasPrivilege(PrivilegeCodes = "RSFS", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public int ResortFormSet(int projectId, int phaseId, int id, int compareToFormSetId)
        {
            // resort the form set
            return FormUtilities.ResortFormSet(id, compareToFormSetId);
        }
        #endregion

        #region Letter Action Methods
        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public bool UpdateLetterFormSet(int projectId, int phaseId, int id, int formSet)
        {
            // update letter form set
            return FormUtilities.UpdateLetterFormSet(id, formSet);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public bool UpdateLetterType(int projectId, int phaseId, int id, string type)
        {
            // update the letter type
            return LetterUtilities.UpdateLetterType(id, type);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public bool SetLetterToMaster(int projectId, int phaseId, int id)
        {
            // update the master form letter
            return FormUtilities.SetLetterToMaster(id);
        }
        #endregion

        #region Change Log Methods
        public ViewResult ChangeLogList(int projectId, int phaseId, GridCommand command)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.CHANGE_LOG, phaseId.ToString()) as FormManagementChangeLogFilter;
            if (filter == null)
            {
				filter = new FormManagementChangeLogFilter { PageNum = 1 };
            }

            // set phase ID of the filter
            filter.PhaseId = phaseId;
            Utilities.SetFilter(Constants.CHANGE_LOG, phaseId.ToString(), filter);

            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            // Create the view model
			int total;
            var viewModel = new ChangeLogListViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                ChangeLogs = GetChangeLogs(
                    new GridCommand { 
                        PageSize = Utilities.GetUserSession().SearchResultsPerPage, 
                        Page = filter.PageNum.Value },
                        phaseId.ToString(),
                        out total),
                CanEdit = isActive,
            };

            // load the filter values to the model
            viewModel.LoadFilterToModel(filter);

            // set the total count to the view data
			ViewData["total"] = total;
			ViewData["pageNum"] = filter.PageNum.Value;

            // return the model
            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ChangeLogListBinding(GridCommand command)
        {
            int total;
            string context = Request.RequestContext.RouteData.Values["phaseId"].ToString();
            IEnumerable data = GetChangeLogs(command, context, out total);

            return View(GetViewPath(Constants.CHANGE_LOG + Constants.LIST), new GridModel
            {
                Data = data,
                Total = total
            });
        }

        [NonAction]
        private static ICollection<FormManagementChangeLog> GetChangeLogs(GridCommand command, string context, out int total)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.CHANGE_LOG, context) as FormManagementChangeLogFilter;
            if (filter == null)
            {
				filter = new FormManagementChangeLogFilter { PageNum = 1 };
            }

            var phaseId = filter.PhaseId;

            // get the data
            IList<FormManagementChangeLog> data;
            using (var changeLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>())
            {
                data = changeLogManager.All.Where(x => x.PhaseId == phaseId).OrderByDescending(x => x.LogId).ToList();
            }

            // set the count
            total = data.Count;

            // apply paging
            if (command.Page > 0)
            {
                data = data.Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            }

            // convert log date to user's timezone
            foreach (FormManagementChangeLog log in data)
            {
                log.Date = log.Date.ConvertToUserTimeZone(Utilities.GetUserSession());
            }

			filter.PageNum = command.Page;
			Utilities.SetFilter(Constants.CHANGE_LOG, context, filter);

            // return the data as a list
            return data.ToList();
        }
        #endregion

        #region Methods for FormSet Listing
        [NonAction]
        private FormSetListViewModel LoadFormSetList(int projectId, int phaseId, bool canEdit, string breadCrumbText, string pageTitle, GridCommand command = null)
        {
            Phase phase;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);
            }

            if (phase.PhaseTypeId == 3)
            {
                phase.ObjectionResponseExtendedBy = phase.ObjectionResponseExtendedBy == null ? 0 : phase.ObjectionResponseExtendedBy;
            }

            // get the filter
            var filter = Utilities.GetFilter(Constants.FORM_SET, phaseId.ToString()) as FormSetSearchResultFilter;
            if (filter == null)
            {
				filter = new FormSetSearchResultFilter { PageNum = 1 };

                // default to show only the master form when the page is loaded for the first time
                filter.LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name == "Master Form or Unique").FirstOrDefault().LetterTypeId;
            }

			// translate the filter type from request param
			filter.SetFormSet(GetParam(Constants.FORM_SET));

            // set phase ID of the filter
            filter.PhaseId = phaseId;
            Utilities.SetFilter(Constants.FORM_SET, phaseId.ToString(), filter);

            // Create the view model
			int total;
			int totalFormSets;
			int pageNum;
            var viewModel = new FormSetListViewModel
            {
                Phase = phase,
                ProjectId = projectId,
                PhaseId = phaseId,
                BreadCrumbTitle = breadCrumbText,
                PageTitle = pageTitle,
				FormSets = GetFormSets(
                    IsEmptyCommand(command) ? 
                        new GridCommand { 
                            Page = filter.PageNum.Value, 
                            PageSize = Utilities.GetUserSession().SearchResultsPerPage }
                        : command,
                    phaseId.ToString(),
                    out total, 
                    out totalFormSets, 
                    out pageNum),
                CanEdit = canEdit,
                RequestUri = Request.Url.LocalPath
            };

            // load the filter values to the model
            viewModel.LoadFilterToModel(filter);

            // set the total counts to the view data
            ViewData["total"] = total;
			ViewData["totalFormSet"] = totalFormSets;
			ViewData["pageNum"] = pageNum;

            // return the model
            return viewModel;
        }

        [NonAction]
        private bool IsEmptyCommand(GridCommand command)
        {
            return (command == null || command.PageSize == 0);
        }

        [NonAction]
		private static IEnumerable GetFormSets(GridCommand command, string context, out int total, out int formSetCount, out int pageNum)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.FORM_SET, context) as FormSetSearchResultFilter;
            if (filter == null)
            {
				filter = new FormSetSearchResultFilter { PageNum = 1 };
            }

            // set the paging
            filter.PageNum = (command.Page > 0 ? command.Page : 1);
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            total = 0;
			formSetCount = 0;

            // get the data
			IList<FormSetSearchResult> data;
            using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
            {
                data = formSetManager.GetFormSetSearchResults(filter, out total);
            }

            pageNum = filter.PageNum.Value;
			//filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.FORM_SET, context, filter);

            // return the data as a list
            return data.ToList();
        }
        #endregion

        #region Methods for FormLetter Listing
        [NonAction]
        private FormSetListViewModel LoadFormLetterList(int projectId, int phaseId, bool canEdit, GridCommand command = null)
        {
            // get the filter
            string formSetId = GetParam(Constants.FORM_SET);
            var filter = Utilities.GetFilter(Constants.FORM_SET + Constants.LETTERS, phaseId.ToString()) as FormSetSearchResultFilter;
            if (filter == null)
            {
                filter = new FormSetSearchResultFilter();
                filter.SetFormSet(formSetId);
                filter.PageNum = 1;
            }
            else if (formSetId != null && filter.FormSetId.ToString() != formSetId)
            {
                filter.SetFormSet(formSetId);
            }

            // translate the filter type from request param
            string pageTitle = "Unique";
            if (filter.FormSetId > 0)
            {
                using (FormSetManager fsm = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var foundFormSet = fsm.Find(x => x.FormSetId == filter.FormSetId).FirstOrDefault();
                    if (foundFormSet != null)
                    {
                        pageTitle = foundFormSet.Name;
                    }
                }
            }
            else if (filter.FormSetId == -1)
            {
                pageTitle = "Unique";
            }
            else if (filter.FormSetId == -2)
            {
                pageTitle = "Duplicate";
            }

            // set phase ID of the filter
            filter.PhaseId = phaseId;
            Utilities.SetFilter(Constants.FORM_SET + Constants.LETTERS, phaseId.ToString(), filter);

            // Create the view model
            int total;
            int totalFormSets;
            int pageNum;
            var viewModel = new FormSetListViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                BreadCrumbTitle = pageTitle,
                PageTitle = pageTitle,
                FormSets = GetFormLetters(
                    IsEmptyCommand(command) ? 
                        new GridCommand { 
                            Page = filter.PageNum.Value, 
                            PageSize = Utilities.GetUserSession().SearchResultsPerPage }
                            : command, 
                        phaseId.ToString(),
                        out total, 
                        out totalFormSets, 
                        out pageNum),
                Count = total,

                CanEdit = canEdit,
                RequestUri = Request.Url.LocalPath
            };

            // load the filter values to the model
            viewModel.LoadFilterToModel(filter);

            // set the total counts to the view data
            ViewData["total"] = total;
            ViewData["totalFormSet"] = totalFormSets;
            ViewData["pageNum"] = pageNum;

            // return the model
            return viewModel;
        }

        [NonAction]
        private static IEnumerable GetFormLetters(GridCommand command, string context, out int total, out int formSetCount, out int pageNum)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.FORM_SET + Constants.LETTERS, context) as FormSetSearchResultFilter;
            if (filter == null)
            {
                filter = new FormSetSearchResultFilter { PageNum = 1 };
            }

            // set the paging
            filter.PageNum = (command.Page > 0 ? command.Page : 1);
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            total = 0;
            formSetCount = 0;

            // get the data
            IList<FormSetLetterSearchResult> data;
            using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
            {
                data = formSetManager.GetFormSetLetterSearchResults(filter, out total);
            }

            pageNum = filter.PageNum.Value;
            //filter.PageNum = command.Page;

            Utilities.SetFilter(Constants.FORM_SET + Constants.LETTERS, context, filter);

            // return the data as a list
            return data.ToList();
        }
        #endregion
    }
}
