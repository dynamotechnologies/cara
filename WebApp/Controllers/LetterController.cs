﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Configuration;

using DiffPlex.DiffBuilder;
using Telerik.Web.Mvc;
using log4net;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Domain.Report;
using System.Text;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the letter specific actions
    /// </summary>
    public class LetterController : PageController
    {
        #region Private Members
        private static readonly TextReplacementConfiguration txtReplacementConfig = ConfigUtilities.GetSection<TextReplacementConfiguration>("textReplacementConfiguration");
        
        #endregion

        #region Core Action Methods
        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
		[ValidateInput(false)]
        public ViewResult Create(int projectId, int phaseId)
        {
            var submittedDate = DateTime.UtcNow;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var phase = phaseManager.Get(phaseId);
                if (phase != null && !string.IsNullOrEmpty(phase.TimeZone))
                {
                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(phase.TimeZone);
                    submittedDate = TimeZoneInfo.ConvertTimeFromUtc(submittedDate, tzi);
                }
            }

            var model = new LetterInputViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                Mode = Constants.CREATE,
                PhaseId = phaseId,
                Letter = new Letter
                    {
                        DateSubmitted = submittedDate,
                        LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name.Equals("New")).First().LetterStatusId,
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name.Equals("Unique")).First().LetterTypeId,
						Protected = false
                    },
                Sender = true,
                Author = new Author { ContactMethod = Constants.EMAIL }
            };

            // set the within comment period flag based on the current phase
            model.WithinCommentPeriod = model.IsDateWithinPhase(submittedDate, model.Phase.TimeZone).ConvertToYesNo();

            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [HttpPost, ValidateInput(false)]
        public ActionResult Create(int projectId, int phaseId, string action, LetterInputViewModel model)
        {
            // add attachments
            model.AddDocuments(Request.Files, ModelState);

            // validate business rules
            model.Validate(ModelState);

            if (ModelState.IsValid)
            {
                // save the letter with attachments
                var letterId = model.Save(phaseId, ModelState);

                if (letterId > 0 && ModelState.IsValid)
                {
                    if ("Submit & Enter Another Letter".Equals(action))
                    {
                        // redirect to the listing page
                        return RedirectToAction(Constants.CREATE);
                    }
                    else if ("Submit & Go To Letter Inbox".Equals(action))
                    {
                        // redirect to the listing page
                        return RedirectToAction(Constants.LIST);
                    }
                    else if ("Submit & Go To Letter Coding Page".Equals(action))
                    {
                        // redirect to the listing page
                        return RedirectToAction(Constants.CODING, new { id = letterId, useDefaultFilter = true });
                    }
                }
            }

            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [ValidateInput(false)]
        public ViewResult Edit(int projectId, int phaseId, int id, string nextLetterNums, int? numberOfLettersRemaining)
        {
            // get the letter by id
            Letter letter;
            Author sender;
            string orgName;
            Phase phase;
            int commentCount;
            LetterUtilities.EncodeText(id);
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                var letterInfo = letterManager.All.Where(ltr => ltr.LetterId == id).Select(ltr => new
                    {
                        Letter = ltr,
                        Phase = ltr.Phase,
                        Comments = ltr.Comments,
                        LetterDocuments = ltr.LetterDocuments,
                        Authors = ltr.Authors,
                        Orgs = ltr.Authors.Select(x => x.OrganizationId != null ? x.Organization : null)
                    }).First();

                letter = letterInfo.Letter;
                phase = letter.Phase;
                commentCount = letter.Comments.Count;
                var documents = letter.Documents;
                sender = letter.Sender;
                orgName = sender.Organization != null ? sender.Organization.OrganizationId.ToString() : null;

                // Convert DateSubmitted to phase's time zone
                if (!string.IsNullOrEmpty(phase.TimeZone))
                {
                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(phase.TimeZone);
                    letter.DateSubmitted = TimeZoneInfo.ConvertTimeFromUtc(letter.DateSubmitted, tzi);
                }
            }

            bool originalTextEditable = false;
            if (commentCount == 0)
            {
                // If you have the privilege to delete a protected letter then you have the privilege to edit the
                // original text of a protected letter
                if (letter.Protected.HasValue && letter.Protected.Value && Utilities.UserHasPrivilege("DPL", Constants.SYSTEM))
                {
                    originalTextEditable = true;
                }
                else if (!letter.Protected.HasValue || !letter.Protected.Value)
                {
                    originalTextEditable = true;
                }
            }
          
            return View(GetViewPath(), new LetterInputViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                Mode = Constants.EDIT,
                Letter = letter,
                PhaseId = letter.PhaseId,
                Phase = phase,
                Author = sender,
                WithinCommentPeriod = letter.WithinCommentPeriod.ConvertToYesNo(),
                OrganizationName = orgName,
                OriginalTextEditable = originalTextEditable,
                DateSubmittedOriginal = letter.DateSubmitted,
                NextLetterIds = nextLetterNums,
                LettersRemaining = numberOfLettersRemaining
            });
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public ActionResult Revert(int projectId, int phaseId, int id)
        {
            bool doRepost = false;
            bool.TryParse(Request.QueryString["RepostToPRR"], out doRepost);

            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                if (doRepost)
                {
                    letterManager.UnitOfWork.Saved += letterManager.QueueLettersForDmdRepost;
                }

                letterManager.RevertLetter(id);
                letterManager.UnitOfWork.Save();
            }

            return RedirectToAction(Constants.CODING, new { id = id });
        }


        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(int projectId, int phaseId, int id, LetterInputViewModel model)
        {
            //CARA-714:- Allow users to add letter attachments - by LD on 10/27/11
            model.AddDocuments(Request.Files, ModelState);
            
            // validate the input
            model.Validate(ModelState);

            if (ModelState.IsValid)
            {
                // save the letter
                var letterId = model.Save(phaseId, ModelState);

                if (ModelState.IsValid)
                {
                    return RedirectToAction(Constants.CODING, new
                    {
                        id = id,
                        nextLetterIds = model.NextLetterIds,
                        lettersRemaining = model.LettersRemaining
                    });
                }
            }

            return View(GetViewPath(), model);
        }

        public ViewResult List(int projectId, int phaseId)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.LETTER, phaseId.ToString()) as LetterSearchResultFilter;
            if (filter == null)
            {
				filter = new LetterSearchResultFilter { PageNum = 1 };
            }

            // translate the filter type from request param
            filter.SetFilterByType(GetParam(Constants.FILTER));

            // set phase ID of the filter
            filter.PhaseId = phaseId;
            Utilities.SetFilter(Constants.LETTER, phaseId.ToString(), filter);

            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            // Create the view model
			int total;
			int pageNum;
            var viewModel = new LetterListViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                Letters = GetLetters(
                    new GridCommand { 
                        PageSize = Utilities.GetUserSession().SearchResultsPerPage, 
                        Page = filter.PageNum.Value },
                    phaseId.ToString(),
                    out total, 
                    out pageNum),
                CanEdit = isActive
            };

            viewModel.LoadFilterToModel(filter);

            ViewData["total"] = total;
			ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }

        [HttpPost]
        public ActionResult List(int projectId, int phaseId, string action, LetterListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // set the phaseId
                    model.PhaseId = phaseId;

                    // save the filter in the session
                    Utilities.SetFilter(Constants.LETTER, phaseId.ToString(), model.GetFilterFromModel() as LetterSearchResultFilter);
                }

                ViewData["total"] = 0;
				ViewData["pageNum"] = 1;

                // pass the view model, list binding method will get the data
                return RedirectToAction(Constants.LIST);
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
                Utilities.SetFilter(Constants.LETTER, phaseId.ToString(), new LetterSearchResultFilter { PageNum = 1 });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST);
            }

            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]        
        public ActionResult LetterListBinding(GridCommand command)
        {
			int total;
			int pageNum;
            string context = Request.RequestContext.RouteData.Values["phaseId"].ToString();
            var data = GetLetters(command, context, out total, out pageNum).ToList();

			ViewData["pageNum"] = pageNum;

            return View(GetViewPath(Constants.LIST), new GridModel { Data = data, Total = total });
        }

        [NonAction]
        private static ICollection<LetterSearchResult> GetLetters(GridCommand command, string context, out int total, out int pageNum)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.LETTER, context) as LetterSearchResultFilter;
            if (filter == null)
            {
                filter = new LetterSearchResultFilter();
            }

            // set the paging
            filter.PageNum = command.Page;
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (var sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            total = 0;

            // get the data
            IList<LetterSearchResult> data;

            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                data = letterManager.GetLetters(filter, out total);

                if (data.Count > 0)
                {
                    TimeZoneInfo tzi;
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        var phase = phaseManager.Get(data[0].PhaseId);
                        if (!string.IsNullOrEmpty(phase.TimeZone))
                        {
                            tzi = TimeZoneInfo.FindSystemTimeZoneById(phase.TimeZone);

                            // Convert the time to the phase's time
                            foreach (var letter in data)
                            {
                                letter.DateSubmitted = TimeZoneInfo.ConvertTimeFromUtc(letter.DateSubmitted, tzi);
                            }
                        }
                    }
                }
            }

			pageNum = command.Page;
			filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.LETTER, context, filter);

            // return the data as a list
            return data.ToList();
        }

        [HasPrivilege(PrivilegeCodes = "PURR", Scope = Constants.PHASE)]
        public ActionResult UpdatePublishStatus(int projectId, int phaseId, int id, string publish, int? unpublishedReasonId = null, string unpublishedReasonOther = null)
        {
			// 5 == Other.  Need this because null in javascript is passed as the string "null"
			if (unpublishedReasonId != 5 && unpublishedReasonOther == "null")
			{
				unpublishedReasonOther = null;
			}

            var publishToReadingRoom = Constants.PUBLISH.Equals(publish);
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                // get the letter
                var letter = letterManager.Get(id);

                if (letter != null)
                {
                    // update the publish status
                    letter.PublishToReadingRoom = publishToReadingRoom;

                    // update the unpublish reason fields based on status
                    letter.UnpublishedReasonId = publishToReadingRoom ? null : unpublishedReasonId;
                    letter.UnpublishedReasonOther = publishToReadingRoom ? null : unpublishedReasonOther;

                    if (letter.FormSet != null && letter.FormSet.MasterFormId == letter.LetterId)
                    {
                        letterManager.UpdatePublishingStatus(letter.FormSet.FormSetId, publishToReadingRoom, letter.UnpublishedReasonId, letter.UnpublishedReasonOther);
                    }

                    // save the work
                    letterManager.UnitOfWork.Save();
                }
            }

			TempData[Constants.CODING] = Utilities.GetResourceString(Constants.CONFIRM + publish);

            return RedirectToAction(Constants.CODING, new { id = id });
        }

        [HasPrivilege(PrivilegeCodes = "DLTR", Scope = Constants.PHASE)]
        public ActionResult Delete(int projectId, int phaseId, int id)
        {
            ActionResult result = null;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    using (var dmManager = new DataMartManager())
                    {
                        var project = projectManager.Get(projectId);
                        var letter = letterManager.Get(id);

                        if (letter.Comments.Where(x => x.ConcernResponseId != null).Count() == 0)
                        {
                            DeleteLetter(letter, project, letterManager, dmManager);
                            letterManager.UnitOfWork.Save();
                        }
                        else
                        {
                            TempData[Constants.ERROR] = "A letter that has comments associated with a Concern/Response cannot be deleted";
                            result = RedirectToAction(Constants.CODING,
                                new
                                { 
                                    projectId = projectId,
                                    phaseId = phaseId,
                                    id = id
                                });
                        }
                    }
                }
            }

            if (!TempData.ContainsKey(Constants.ERROR))
            {
                if (Request.Params.AllKeys.Contains("master"))
                {
                    TempData[Constants.CODING] = "Deleted duplicate letter";
                    result = RedirectToAction(Constants.CODING, new { tab = Constants.DETAILS, id = Request.Params["master"] });
                }
                else
                {
                    result = RedirectToAction(Constants.LIST);
                }
            }

            return result;
        }

        [HasPrivilege(PrivilegeCodes = "DLTR", Scope = Constants.PHASE)]
        public ActionResult DeleteDuplicates(int projectId, int phaseId, int id)
        {
            ActionResult result = null;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    using (var dmManager = new DataMartManager())
                    {
                        var project = projectManager.Get(projectId);
                        var letter = letterManager.Get(id);

                        var duplicates = letter.Duplicates.ToList();
                        for (int i = duplicates.Count - 1; i >= 0; i--)
                        {
                            DeleteLetter(duplicates[i], project, letterManager, dmManager);
                        }

                        letterManager.UnitOfWork.Save();
                    }
                }
            }

            TempData[Constants.CODING] = "Deleted all duplicates for this letter";
            return RedirectToAction(Constants.CODING, new { id = id, tab = Constants.DETAILS });
        }

        [NonAction]
        private void DeleteLetter(Letter letter, Project project, LetterManager letterManager, DataMartManager dmManager)
        {
            if (project.IsPalsProject && !string.IsNullOrEmpty(letter.Sender.ContactMethod))
            {
                dmManager.DeleteSubscriber(project.ProjectId, letter.Sender.AuthorId);
                dmManager.Dispose();
            }

            // Remove the letter from CARA
            letterManager.Delete(letter);
        }

        [HasPrivilege(PrivilegeCodes = "DLTR", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ActionResult DeleteAttachment(int projectId, int phaseId, int letterId, int documentId)
        {
            ActionResult result = Json(new { errorMessage = string.Empty });
            using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
            {
                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    var letter = letterManager.Get(letterId);

                    if (letter != null)
                    {
                        letterManager.DeleteAttachment(letter, documentId, documentManager);
                        letterManager.UnitOfWork.Save();
                        documentManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdDelete);
                        documentManager.UnitOfWork.Save();
                    }
                }
            }

			return result;
        }
        #endregion

        #region Upload Action Methods
        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public ViewResult UploadLetters(int projectId, int phaseId)
        {
            LetterUploadViewModel model = new LetterUploadViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction)
            };

            if (Request.QueryString["confirmed"] != null)
            {
                ViewData[Constants.CONFIRM] = "The file(s) have been successfully added to the queue.";
            }

            string requeued = Request.QueryString["requeued"];
            if (requeued != null)
            {
                if (requeued == "True")
                {
                    ViewData[Constants.CONFIRM] = "The file has been successfully re-added to the queue.";
                }
                else
                {
                    ViewData[Constants.CONFIRM] = "The file could not be re-added to the queue.";
                }
            }

            string deleted = Request.QueryString["deleted"];
            if (deleted != null)
            {
                if (deleted == "True")
                {
                    ViewData[Constants.CONFIRM] = "The file has been successfully deleted from the queue.";
                }
                else
                {
                    ViewData[Constants.CONFIRM] = "The file could not be deleted from the queue because the status is no longer Pending.";
                }
            }

            model.PopulateFiles();
            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [HttpPost]
        public ViewResult UploadLetters(int projectId, int phaseId, LetterUploadViewModel model)
        {
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                // validate file
                LetterUploadFileType? type = model.ValidatePostedFileBase(ModelState, hpf);

                // extract and upload the letters
                if (ModelState.IsValid && type.HasValue)
                {
                    model.PhaseId = phaseId;
                    //CARA-1226
                    string fileName = hpf.FileName
                        .Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries)
                        .Last();

                    using (LetterUploadQueueManager luqm = ManagerFactory.CreateInstance<LetterUploadQueueManager>())
                    {
                        LetterUploadQueue luq = new LetterUploadQueue
                        {
                            Filename = fileName,
                            LetterUploadFileType = type.Value,
                            FileSize = hpf.ContentLength,
                            PhaseId = phaseId,
                            LetterUploadStatus = Domain.LetterUploadStatus.Pending,
                            UserId = Utilities.GetUserSession().UserId
                        };

                        luqm.Add(luq, hpf.InputStream);
                        luqm.UnitOfWork.Save();
                    }

                    if (ModelState.IsValid)
                    {
                        // CARA-1278 Reload the page, so that refresh doesn't resubmit the form
                        Response.StatusCode = 303;
                        var redirectUrl = Request.Url.ToString() + "?confirmed=true";
                        if (Request.Headers.HasKeys() && Request.Headers.Get("X-Forwarded-Proto") == "https") // CARA-1320
                        {
                            redirectUrl = redirectUrl.Replace("http:", "https:");
                        }

                        Response.RedirectLocation = redirectUrl;
                    }
                }
            }

            model.PopulateFiles();

            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "RQLU", Scope = Constants.PHASE)]
        public ActionResult RequeueLetterUpload(int projectId, int phaseId, int letterUploadId)
        {
            // Check to see if the file still exists

            // Requeue it if it still exists
            bool isRequeued = false;
            using (LetterUploadQueueManager luqm = ManagerFactory.CreateInstance<LetterUploadQueueManager>())
            {
                var lu = luqm.Get(letterUploadId);
                if (lu != null)
                {
                    // Add the letter back to the queue
                    isRequeued = luqm.Requeue(lu);
                    if (isRequeued)
                    {
                        // Set the status to pending
                        lu.LetterUploadStatus = Domain.LetterUploadStatus.Pending;
                        luqm.UnitOfWork.Save();
                    }
                }
            }

            return RedirectToAction("UploadLetters", new { projectId = projectId, phaseId = phaseId, requeued = isRequeued });
        }

        [HasPrivilege(PrivilegeCodes = "RQLU", Scope = Constants.PHASE)]
        public ActionResult DeleteLetterUpload(int projectId, int phaseId, int letterUploadId)
        {
            // Check to see if the file still exists

            // Requeue it if it still exists
            bool isSuccessful = false;
            using (LetterUploadQueueManager luqm = ManagerFactory.CreateInstance<LetterUploadQueueManager>())
            {
                var lu = luqm.Get(letterUploadId);

                if (lu != null)
                {
                    if (lu.Status == "Pending")
                    {
                        // Delete the from the queue
                        luqm.Delete(lu);
                        luqm.UnitOfWork.Save();
                        isSuccessful = true;
                    }
                }
            }

            return RedirectToAction("UploadLetters", new { projectId = projectId, phaseId = phaseId, deleted = isSuccessful });
        }
        #endregion


        #region Letter Coding Methods

        public ViewResult Coding(int projectId, int phaseId, int id, string nextLetterIds, int? lettersRemaining, bool useDefaultFilter = false)
        {
            // get the letter and phase codes
            Phase phase;
            Letter letter;
            ICollection<TermList> termLists;
            ICollection<Document> documents;
            Author sender;
            FormSet formSet;
            ICollection<LetterTermConfirmation> letterTermConfirmations;
            ICollection<Letter> duplicates;
            IList<IGrouping<string, LetterTermConfirmation>> termGroups;
            LetterUtilities.EncodeText(id);
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                // Turn off lazy loading
                letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                // Force load all of the properties needed in one query
                var letterData = letterManager
                    .All
                    .Where(ltr => ltr.LetterId == id)
                    .Select(ltr => new
                        {
                            Letter = ltr,
                            Authors = ltr.Authors,
                            LetterObjection = ltr.LetterObjection,
                            OriginalDuplicate = ltr.MasterDuplicate,
                            Duplicates = ltr.Duplicates,
                            Phase = ltr.Phase,
                            TermLists = ltr.Phase.TermLists,
                            FormSet = ltr.FormSet,
                            Documents = ltr.LetterDocuments,
                            LetterTermConfirmations = ltr.LetterTermConfirmations,
                            LetterObjectionDocuments = ltr.LetterObjection.LetterObjectionDocuments
                        })
                    .FirstOrDefault();

                letter = letterData.Letter;
                sender = letter.Sender;
                phase = letter.Phase;
                termLists = phase.TermLists;
                formSet = letter.FormSet;
                documents = letter.Documents;
                letterTermConfirmations = letter.LetterTermConfirmations;
                duplicates = letter.Duplicates.ToList();

                if (letterData.Phase.PhaseTypeId == 3 && letter.LetterObjectionId != null) // Objection
                {
                    var loDocuments = letter.LetterObjection.Documents;
                }

                // Turn on lazy loading
                letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = true;

                // Load properties so they are available to the View
                termGroups = letterTermConfirmations.GroupBy(x => x.Term.TermList.Name).ToList();
                var org = sender.Organization;

                //CARA-885 querying for all letters in comment period after current letter 
                //for better performance when requesting the next letter
                if (nextLetterIds == null || nextLetterIds == "")
                {
                    #region execute query
                    //prepare to execute the query
                    LetterSearchResultFilter filter = Utilities.GetFilter(Constants.LETTER, phaseId.ToString()) as LetterSearchResultFilter;
                    IList<LetterSearchResult> data = null;
                    int total;

                    //if the page was navigated to from somewhere besides the comment period letter list the filter will be null
                    if (filter == null || useDefaultFilter)
                    {
                        filter = new LetterSearchResultFilter();
                        filter.ViewAll = false;
                        filter.PhaseId = phaseId;
                        
                        //find the correct page
                        filter.PageNum = 1;
                        data = letterManager.GetLetters(filter, out total);
                        lettersRemaining = data.Count();
                        Utilities.SetFilter(Constants.LETTER, phaseId.ToString(), filter);
                        filter.StartingLetterId = id;
                    }

                    //nextLetterIds is empty check to see if there are still letters in the query page
                    else if (lettersRemaining != 0)
                    {
                        //either this is the first "next letter" requested or the last query page had too many results on it
                        data = letterManager.GetLetters(filter, out total);
                        data = data.SkipWhile(x => x.LetterId != id).Skip(1).ToList();
                        if (data.Count() == 0)
                        {
                            // there are no letters after the current one on the current query page
                            // set lettersRemaining = 0 so that the next page is queried
                            lettersRemaining = 0;
                        }
                    }
                    if (lettersRemaining == 0)
                    {
                        filter.PageNum++;
                        data = letterManager.GetLetters(filter, out total);
                        if (data.Count() == 0)
                        {
                            filter.PageNum = 1;
                        }
                    }

                    #endregion
                    //if there are more results in the query convert them to a string
                    if (data.Count() > 0)
                    {
                        nextLetterIds = data[0].LetterId.ToString();
                        int count = 1;
                        foreach (LetterSearchResult tempLetter in data)
                        {
                            if (tempLetter.LetterId.ToString() == nextLetterIds)
                            {
                                continue;
                            }
                            nextLetterIds += "," + tempLetter.LetterId.ToString();
                            count++;
                            if (count >= 100)
                            {
                                break;
                            }
                        }
                        lettersRemaining = data.Count() - count;
                    }
                    else
                    {
                        lettersRemaining = 0;
                    }
                    //now that the next letterids have been retrieved reset the startingLetterId so it does not interfere with the letter list page
                    filter.StartingLetterId = null;
                }
            }
            
            // code the letter with add'l auto-markups stored in the session
            letter.HtmlCodedText = LetterUtilities.GetSessionLetter(letter);

            var letterCodingViewModel = new LetterCodingViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                ProjectId = projectId,
                PhaseId = phaseId,
                LetterId = id,
                Letter = letter,
                TermLists = termLists,
                Locked = phase.Locked,
                LetterFontSize = Utilities.GetUserSession().LetterFontSize,
                RequestUri = Request.Url.PathAndQuery,
                Sender = sender,
                FormSet = formSet,
                Documents = documents,
                TermConfirmations = letterTermConfirmations,
                Duplicates = duplicates,
                TermGroups = termGroups,
                NextLetterIds = nextLetterIds,
                LettersRemaining = lettersRemaining
            };

            if (phase.PhaseTypeId == 3) /* Objection */
            {
                using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
                {
                    int total;
                    var objectionSummaryReport = new GenericStandardReportViewModel<CommentsAndResponsesByLetterReportData>();
                    objectionSummaryReport.ReportData = reportManager.GetCommentsAndResponsesByLetterReport(phaseId, id, 0, 0, out total).ToList();
                    objectionSummaryReport.HasPaging = false;
                    letterCodingViewModel.ObjectionSummaryReport = objectionSummaryReport;

                    // Required for the view
                    ViewData["total"] = total;
                    ViewData["pagenum"] = 1;
                }
            }

            string activeTab = Request.QueryString["tab"];
            if (string.IsNullOrEmpty(activeTab)  || activeTab.ToLower() != "details")
            {
                letterCodingViewModel.TabName = "Coding";
            }
            else
            {
                letterCodingViewModel.TabName = activeTab;
            }
            //letterCodingViewModel.TermLists.FirstOrDefault()..FirstOrDefault().
            //form plus highlight section start
            if (letterCodingViewModel.Letter.LetterTypeId == 5)
            {

                try
                {
                    if (letterCodingViewModel.Letter.HtmlCodedText.IndexOf("data-formtype=\"plus\"") == -1)
                    {
                        HighLightFormPlus(letterCodingViewModel.Letter);
                        letterCodingViewModel.Letter.HtmlCodedText = LetterUtilities.CodeTextBySessionTermList(letterCodingViewModel.Letter.HtmlCodedText);
                    }
                    ViewData["CodingFormPlusLegend"] = "Y";
                }
                catch
                {
                };
            }
            //form plus highlight section end
            return View(GetViewPath(), letterCodingViewModel);
        }

        public void HighLightFormPlus(Letter pLetter )
        {
            // initialize the differ and builder objects
            var diff = new DiffPlex.Differ();
            var builder = new SideBySideDiffBuilder(diff);
            int targetLetterId =(int) ((pLetter.FormSetId != null) ? pLetter.FormSet.MasterFormId : ((pLetter.MasterDuplicateId != null) ? pLetter.MasterDuplicateId : -1));
            // get the letters
            Letter letter;
            Letter masterLetter;
            Letter nextLetter = null;
            Letter prevLetter = null;
            int letterFormSetCount;
            FormSet masterLetterFormSet = null;
            LetterType uniqueLetterType = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name == "Unique").First();
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                letter = letterManager.Get(pLetter.LetterId);
                int ltrSequence = letter.LetterSequence.HasValue ? letter.LetterSequence.Value : -1;

                // Enter this block if:
                //    The targetletterId is the master form letter of the letter specified by id
                //    The letter is unique and is not a part of a formset
                /*if (navigate
                     && (letter.FormSet != null
                        || (letter.FormSet == null && letter.LetterTypeId == uniqueLetterType.LetterTypeId)))
                {
                    // get the filter
                    var filter = Utilities.GetFilter(Constants.FORM_SET + Constants.LETTERS, phaseId.ToString()) as FormSetSearchResultFilter;

                    if (filter == null)
                    {
                        filter = new FormSetSearchResultFilter();
                    }

                    var navigationFilter = new FormSetSearchResultFilter
                    {
                        PhaseId = filter.PhaseId,
                        LetterStatusId = filter.LetterStatusId,
                        LetterTypeId = filter.LetterTypeId,
                        FormSetId = letter.FormSetId
                    };

                    nextLetter = letterManager.GetNextFormLetter(navigationFilter, ltrSequence);
                    prevLetter = letterManager.GetPrevFormLetter(navigationFilter, ltrSequence);
                }
                */
                letterFormSetCount = 0;
                if (letter.FormSet != null)
                {
                    letterFormSetCount = letterManager.All.Where(x => x.FormSetId == letter.FormSetId).Count();
                }

                masterLetter = new Letter();
                if (targetLetterId > 0)
                {
                    masterLetter = letterManager.Get(targetLetterId);
                    if (masterLetter != null)
                    {
                        masterLetterFormSet = masterLetter.FormSet;
                    }
                }
            }
            /*
            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }
            */
            // get the diff viewer model
            var transformedMasterLetterText = TransformForComparison(masterLetter.Text);
            var transformedLetterText = TransformForComparison(letter.Text);

            var model = builder.BuildDiffModel(transformedMasterLetterText, transformedLetterText);
            string htmlcoded = pLetter.HtmlCodedText; //htmlcoded = htmlcoded.Replace("\r", " ").Replace("\n", " ");
            System.Text.RegularExpressions.Regex regexsb = new System.Text.RegularExpressions.Regex(@"\[.*?\]|<.*?>");
            System.Text.RegularExpressions.Regex regexhtml = new System.Text.RegularExpressions.Regex(@"<.*?>");
            System.Text.RegularExpressions.MatchCollection mchsbhtml = regexsb.Matches(htmlcoded);
            System.Text.RegularExpressions.MatchCollection mchhtml = regexhtml.Matches(htmlcoded);
            System.Text.RegularExpressions.Match[] arrm = mchsbhtml.Cast<System.Text.RegularExpressions.Match>().ToArray(); 
            StringBuilder htmlcodedresult = new StringBuilder();
            //string strsearch = "";
            int htmlcodelen = htmlcoded.Length - 1;
            int indexstart = 0;
            int indexend = 0;
            int searchresult = 0;
            bool highlight = true;
            System.Action<DiffPlex.DiffBuilder.Model.DiffPiece> parseLine = null;
            parseLine = new System.Action<DiffPlex.DiffBuilder.Model.DiffPiece>(line1 =>
            {
                string[] arrstrsearch =  line1.Text.Split((" ").ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                //indexstart = 0;
                int notfoundarea = 1;
                indexstart = indexend;
                for (int i = 0; i < arrstrsearch.Length; i++)
                {
                    searchresult = htmlcoded.IndexOf(arrstrsearch[i], indexend);
                    if (searchresult > -1)
                    {
                        //if (indexstart == 0 && searchresult >= indexend) { indexstart = searchresult; }
                        if (searchresult - indexend < 40 + arrstrsearch[i].Length * notfoundarea * 3)  // echancment again found some special symbol for restore, 40 longest automarkup
                        {
                            indexend = searchresult + arrstrsearch[i].Length;
                        }
                        else
                        { 
                           // indexend += arrstrsearch[i].Length;
                            notfoundarea = notfoundarea * 2;
                            //string aaaa = htmlcoded.Substring(indexstart); 
                        }

                    }
                    else
                    { indexend += arrstrsearch[i].Length; }
                    if (indexend > htmlcodelen)
                    { indexend = htmlcodelen; }
                    if (indexstart > htmlcodelen)
                    { indexstart = htmlcodelen; }
                    //else
                    //{indexend +=htmlcoded.IndexOf(strsearch, indexend)}

                }
            });
            System.Action<DiffPlex.DiffBuilder.Model.DiffPiece> parseLinerecurcive = null;
            parseLinerecurcive = new System.Action<DiffPlex.DiffBuilder.Model.DiffPiece>(line2 =>
            {
                //if (indexstart == 0) { indexstart = (searchresult!=-1)?searchresult:indexend; }
                if (line2.Type == DiffPlex.DiffBuilder.Model.ChangeType.Unchanged)
                {
                    parseLine.Invoke(line2);

                    if (arrm.Where(m => m.Index >= indexstart && m.Index <= indexend).Count() > 0)
                    {
                        foreach (var mch in arrm.Where(m => m.Index >= indexstart && m.Index <= indexend).OrderBy(m => m.Index))
                        {

                            if (mch.Index == indexstart)
                            {
                                htmlcodedresult.Append(htmlcoded.Substring(mch.Index, mch.Length));
                                indexstart += mch.Length;
                            }
                            else if (mch.Index > indexstart)
                            {
                                htmlcodedresult.Append(htmlcoded.Substring(indexstart, mch.Index - indexstart)).Append(htmlcoded.Substring(mch.Index, mch.Length));
                                indexstart = mch.Index + mch.Length;
                            }
                            if (mch.Value.Substring(0, 1) == "[")
                            {
                                highlight = highlight ? false : true;

                            }
                        }
                        if (indexstart < indexend)
                        {
                            htmlcodedresult.Append(htmlcoded.Substring(indexstart, indexend - indexstart));
                        }
                        else
                        { indexend = indexstart; }
                    }
                    else
                    {
                        htmlcodedresult.Append(htmlcoded.Substring(indexstart, indexend - indexstart));
                    }

                }
                else if (line2.Type == DiffPlex.DiffBuilder.Model.ChangeType.Inserted)
                {
                    parseLine.Invoke(line2);
                    //string aaaa = htmlcoded.Substring(indexstart, indexend - indexstart); 
                    if (arrm.Where(m => m.Index >= indexstart && m.Index <= indexend).Count() > 0)
                    {
                        foreach (var mch in arrm.Where(m => m.Index >= indexstart && m.Index <= indexend).OrderBy(m => m.Index))
                        //foreach (var mch in arrm.Where(m => m.Index >= indexstart && m.Index < indexend).OrderBy(m => m.Index))
                        {

                            if (mch.Index == indexstart)
                            {
                                htmlcodedresult.Append(htmlcoded.Substring(mch.Index, mch.Length));
                                indexstart += mch.Length;
                                if (mch.Value.Substring(0, 1) == "[")
                                {
                                    highlight = highlight ? true : false;

                                }
                            }
                            else if (mch.Index > indexstart)
                            {
                                if (highlight)
                                {
                                    htmlcodedresult.Append("<span style=\"background-color:#D3D3D3\" data-formtype=\"plus\">" + htmlcoded.Substring(indexstart, mch.Index - indexstart) + " </span>").Append(htmlcoded.Substring(mch.Index, mch.Length));
                                    
                                 }
                                else
                                {
                                    htmlcodedresult.Append(htmlcoded.Substring(indexstart, mch.Index - indexstart)).Append(htmlcoded.Substring(mch.Index, mch.Length));

                                    if (mch.Value.Substring(0, 1) == "[")
                                    {
                                        highlight = highlight ? false : true;

                                    }
                                }
                                indexstart = mch.Index + mch.Length;
                            }
                        }
                        if (indexstart < indexend)
                        {
                            if (highlight)
                            {
                               htmlcodedresult.Append("<span style=\"background-color:#D3D3D3\" data-formtype=\"plus\">" + htmlcoded.Substring(indexstart, indexend - indexstart) + " </span>");
                               
                            }

                        }
                        else
                        { indexend = indexstart; }
                    }
                    else
                    {
                        htmlcodedresult.Append("<span style=\"background-color:#D3D3D3\" data-formtype=\"plus\">" + htmlcoded.Substring(indexstart, indexend - indexstart) + " </span>");
                        
                    }
                }
                else if (line2.Type == DiffPlex.DiffBuilder.Model.ChangeType.Modified)
                {
                    //parseLine.Invoke(line2);
                    //htmlcodedresult.Append("<span style=\"background-color:#D3D3D3\">" + htmlcoded.Substring(indexstart, indexend - indexstart) + " </span>");
                    foreach (DiffPlex.DiffBuilder.Model.DiffPiece subline in line2.SubPieces)
                    {
                        parseLinerecurcive.Invoke(subline);
                    }
                }

            });
            foreach (var line in model.NewText.Lines)
            {   //line.SubPieces
                
                if (line.Text != null && line.Text.Length > 0)
                {
                    
                    parseLinerecurcive.Invoke(line);
                }

            }
            if (indexend < htmlcodelen)
            {
                htmlcodedresult.Append(htmlcoded.Substring(indexend, htmlcodelen - indexend) );
            }
           // if (htmlcodedresult.ToString().Substring(0,3) == "<p>" && htmlcodedresult.ToString().Substring(3,1) != " ")
           // {
           //     htmlcodedresult.Insert(3, " ");
           // }
           // lcv.Letter.HtmlCodedText = htmlcodedresult.ToString();
            pLetter.HtmlCodedText = htmlcodedresult.ToString();
            
        }
               
        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public PartialViewResult AddCommentCodes(int phaseId, int letterId, string letterCommentNumber, string checkedCodeList)
        {
            // add the codes to the comment
            var updatedComment = LetterUtilities.AddCommentCodes(phaseId, letterId, letterCommentNumber, checkedCodeList);

            // get the letter and phase codes
            Phase phase;
            Letter letter;
            int projectId;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                letter = letterManager.Get(letterId);
                phase = letter.Phase;
                projectId = phase.ProjectId;
            }

            return PartialView("~/Views/Shared/Input/ucLetterCommentInput.ascx", new LetterCodingViewModel
            {
                Comment = updatedComment,
                ProjectId = projectId,
                PhaseId = phase.PhaseId,
                Letter = letter,
                Locked = phase.Locked,
            });
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ContentResult AddComment(int phaseId, int letterId, string selectedText, string checkedCodeList, string codedLetterText, string pendingTag)
        {
            codedLetterText = codedLetterText.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&");
            selectedText = selectedText.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&");
            return Content(LetterUtilities.AddComment(phaseId, letterId, selectedText, checkedCodeList, codedLetterText, pendingTag));
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public bool SaveComment(int commentId, bool sampleStatement, int? noResponseReasonId, string noResponseReasonOther,
            string annotation)
        {
            // save the comment to the letter
            return LetterUtilities.SaveComment(commentId, sampleStatement, noResponseReasonId, noResponseReasonOther, annotation);
        }

        public PartialViewResult CommentInput(int phaseId, string letterCommentNumber)
        {
            Comment comment = null;
            Letter letter = null;
            var projectId = 0;
            var locked = false;

            // get the comment
            if (phaseId > 0 && !string.IsNullOrWhiteSpace(letterCommentNumber))
            {
                using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                {
                    comment = commentManager.Get(phaseId, letterCommentNumber);
                    if (comment != null)
                    {
                        letter = comment.Letter;
                        projectId = letter.Phase.ProjectId;
                        locked = letter.Phase.Locked;
                    }

                    // lazy load for view
                    var concern = comment.ConcernResponse;
                    var phasecodes = comment.CommentCodes;
                    foreach (var x in phasecodes)
                    {
                        var p = x.PhaseCode;
                    }
                }
            }

            // return the comment input user control with the comment
            return PartialView("~/Views/Shared/Input/ucLetterCommentInput.ascx", new LetterCodingViewModel
            {
                Comment = comment,
                ProjectId = projectId,
                PhaseId = phaseId,
                Letter = letter,
                Locked = locked
            });
        }

        [AjaxErrorHandler]
        public PartialViewResult SearchCode(int phaseId, string searchCodeText)
        {
            return PartialView("~/Views/Shared/List/ucLetterPhaseCodeSelect.ascx", new LetterPhaseCodeViewModel
            {
                PhaseId = phaseId,
                SearchCodeText = searchCodeText,
                Expand = (!string.IsNullOrEmpty(searchCodeText))
            });
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public PartialViewResult DeleteCommentCode(int commentId, int phaseCodeId)
        {
            // delete the code from the comment
            Comment comment = null;
            Letter letter = null;
            var projectId = 0;
            var locked = false;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                comment = commentManager.Get(commentId);
                comment = LetterUtilities.DeleteCommentCode(comment, phaseCodeId, commentManager);
                if (comment != null)
                {
                    letter = comment.Letter;
                    projectId = letter.Phase.ProjectId;
                    locked = letter.Phase.Locked;
                }

                // lazy load for view
                var concern = comment.ConcernResponse;
                var phasecodes = comment.CommentCodes;
                foreach (var x in phasecodes)
                {
                    var p = x.PhaseCode;
                }
            }

            return PartialView("~/Views/Shared/Input/ucLetterCommentInput.ascx", new LetterCodingViewModel
            {
                Comment = comment,
                Locked = locked,
                ProjectId = projectId,
                Letter = letter
            });
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ContentResult DeleteComment(int commentId)
        {
            return Content(LetterUtilities.DeleteComment(commentId));
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        public PartialViewResult LinkText(int projectId, int phaseId, int id)
        {
            IList<Comment> comments;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                comments = commentManager.All.Where(x => x.LetterId == id).ToList();
                foreach (var comment in comments)
                {
                    var text = comment.CommentText;
                    var letter = comment.Letter;
                }
            }

            // render the popup with input fields
            return PartialView("~/Views/Shared/List/ucLetterCommentSelect.ascx", new LetterCodingViewModel
            {
                LetterId = id,
                Comments = comments
            });
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        public PartialViewResult SearchComment(int letterId, string text)
        {
            // search comments
            var comments = LetterUtilities.SearchComment(letterId, text);

            // render the popup with input fields
            return PartialView("~/Views/Shared/List/ucLetterCommentSelect.ascx", new LetterCodingViewModel
            {
                LetterId = letterId,
                Comments = comments
            });
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ContentResult AddCommentText(int phaseId, int letterId, int commentId, string selectedText, string codedText)
        {
            codedText = codedText.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&");
            return Content(LetterUtilities.AddCommentText(phaseId, letterId, commentId, selectedText, codedText));
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        public PartialViewResult CreateConcern(int projectId, int phaseId, int id, int commentId, string letterCommentNumber)
        {
            // render the popup with input fields
            Phase phase = null;
            string ConcernText = "";
            string ResponseText = "";
            using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);
                if (!String.IsNullOrWhiteSpace(phase.ConcernTemplate))
                {
                    ConcernText = phase.ConcernTemplate;
                }
                if (!String.IsNullOrWhiteSpace(phase.ResponseTemplate))
                {
                    ResponseText = phase.ResponseTemplate;
                }
            }
            return PartialView("~/Views/Shared/Input/ucLetterConcernInput.ascx", new ConcernResponseInputViewModel
            {
                ConcernResponse = new ConcernResponse
                {
                    ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList.Where(x => x.Value == "Concern In Progress").Select(y => y.Key).First(),
                    ConcernCreated = DateTime.UtcNow,
                    ConcernCreatedBy = Utilities.GetUserSession().Username,
                    ResponseCreated = DateTime.UtcNow,
                    ResponseCreatedBy = Utilities.GetUserSession().Username,
                    ConcernText = ConcernText,
                    ResponseText = ResponseText
                },
                CommentId = commentId,
                LetterCommentNumber = letterCommentNumber
            });
        }

        [ValidateInput(false)]
        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult SaveConcern(int projectId, int phaseId, ConcernResponseInputViewModel model)
        {
            model.ConcernResponse.ResponseText = RichTextValue(HttpUtility.HtmlDecode(model.ConcernResponse.ResponseText));
            // save the new concern response
            if (ModelState.IsValid)
            {
                model.Create();
            }

            return PartialView("~/Views/Shared/Input/ucLetterConcernInput.ascx", model);
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        //CARA-885 changed parameters from taking a sequence id to taking a list of letterids
        //the letter ids are from the next letters in the list 
        public JsonResult UpdateStatus()
        {
            string error = string.Empty;
            try
            {
                int id = Int32.Parse(Request.Params["id"].ToString());
                string currentStatus = Request.Params["currentStatus"].ToString();
                string newStatus = Constants.COMPLETE;

                if (currentStatus == Constants.NEW || currentStatus == Constants.COMPLETE)
                {
                    newStatus = Constants.IN_PROGRESS;
                }
                else
                {
                    newStatus = Constants.COMPLETE;
                }
                
                // update the coding status   
                bool success = LetterUtilities.UpdateStatus(id, newStatus, true);
                if (!success)
                {
                    error = "Could not update coding status.";
                }

                return Json(
                    new { error = error, success = success, newStatus = newStatus },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);

                return Json(
                    new { error = "An error occurred while updating status.", success = false }, 
                    JsonRequestBehavior.AllowGet);
            }
        }

        //Cara-885 changed parameters from taking in a sequence id to taking in a list of the next letter ids
        public ActionResult NextLetter(int projectNum, int phaseNum, int? letterNum, string nextLetterNums, int? numberOfLettersRemaining )
        {
            if (letterNum == null)
            {
                ILog log = LogManager.GetLogger(this.GetType());

                if (log != null)
                {
                    log.Error(string.Format("NextLetter called with Null ID. Project Number:{0} PhaseNumber:{1}", projectNum, phaseNum));
                }
                throw new Exception
                    ("Next Letter called with Null ID.  Please return to previous page and try again.  If the problem persists contact support.");
            }
            try
            {
                if (nextLetterNums == null)
                {
                    nextLetterNums = "";
                }
                if (numberOfLettersRemaining == null)
                {
                    numberOfLettersRemaining = 1;
                }
                string[] letterIdsArr = nextLetterNums.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int nextLetter;
                if (letterIdsArr.Count() > 0)
                {
                    #region Use nextLetterIds values instead of query
                    //if so get the first one and build a new next letterid string
                    nextLetter = Convert.ToInt32(letterIdsArr[0]);
                    nextLetterNums = "";
                    if (letterIdsArr.Count() > 1)
                    {
                        nextLetterNums = letterIdsArr[1];
                        foreach (string tempLetterId in letterIdsArr)
                        {
                            if (tempLetterId != letterIdsArr[0] && tempLetterId != letterIdsArr[1])
                            {
                                nextLetterNums += "," + tempLetterId;
                            }
                        }
                    }
                    //then return the coding page
                    return RedirectToAction(Constants.CODING, new
                    {
                        projectId = projectNum,
                        phaseId = phaseNum,
                        id = nextLetter,
                        nextLetterIds = nextLetterNums,
                        lettersRemaining = numberOfLettersRemaining
                    });
                    #endregion
                }

                #region execute query
                //prepare to execute the query
                var filter = Utilities.GetFilter(Constants.LETTER, phaseNum.ToString()) as LetterSearchResultFilter;
                IList<LetterSearchResult> data = null;
                int total;
                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    //nextLetterIds is empty check to see if there are still letters in the query page
                    if (numberOfLettersRemaining != 0)
                    {
                        //either this is the first "next letter" requested or the last query page had too many results on it
                        data = letterManager.GetLetters(filter, out total);
                        data = data.SkipWhile(x => x.LetterId != letterNum).Skip(1).ToList();
                        if (data.Count() == 0)
                        {
                            // there are no letters after the current one on the current query page
                            // set lettersRemaining = 0 so that the next page is queried
                            numberOfLettersRemaining = 0;
                        }
                    }
                    if (numberOfLettersRemaining == 0)
                    {
                        filter.PageNum++;
                        data = letterManager.GetLetters(filter, out total);
                    }
                }
                #endregion

                nextLetterNums = "";
                if (data.Count() <= 0)
                {
                    //go to letter inbox
                    return RedirectToAction(Constants.LIST);
                }
                else
                {
                    #region Get id and create NextLetterIds
                    //if there are more results in the query convert them to a string
                    nextLetter = data[0].LetterId;
                    if (data.Count() > 1)
                    {
                        nextLetterNums = data[1].LetterId.ToString();
                        int count = 1;
                        foreach (LetterSearchResult tempLetter in data)
                        {
                            if (tempLetter != data[0] && tempLetter != data[1])
                            {
                                nextLetterNums += "," + tempLetter.LetterId.ToString();
                                count++;
                                if (count >= 100)
                                {
                                    break;
                                }
                            }
                        }
                        numberOfLettersRemaining = data.Count() - count - 1;
                    }
                    else
                    {
                        numberOfLettersRemaining = 0;
                    }
                    #endregion
                    return RedirectToAction(Constants.CODING, new
                    {
                        projectId = projectNum,
                        phaseId = phaseNum,
                        id = nextLetter,
                        nextLetterIds = nextLetterNums,
                        lettersRemaining = numberOfLettersRemaining
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured while retrieving the next letter", ex);
            }
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ContentResult UpdateLetterTerm(int letterId, string termListId, string termId, string statusId, string annotation, int? cnt)
        {
            // update the letter term confirmation
            return Content(LetterUtilities.UpdateLetterTermConfirmation(letterId, termListId, termId, statusId, annotation, cnt));
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        public PartialViewResult ManageTermList(int projectId, int phaseId, int id)
        {
            // get the term list ids of the phase
            IList<TermList> termLists;
            using (var termListManager = ManagerFactory.CreateInstance<TermListManager>())
            {
                IList<int> phaseTermListIds;
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    phaseTermListIds = phaseManager.Get(phaseId).TermLists.Select(x => x.TermListId).ToList();
                }

                // get the term lists not selected in the phase
                termLists = termListManager.All.Where(x => !phaseTermListIds.Contains(x.TermListId)).ToList();
            }

            var model = new LetterCodingViewModel
            {
                TermLists = termLists,
                LetterId = id
            };

            return PartialView("~/Views/Shared/List/ucLetterTermListSelect.ascx", model);
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public ContentResult SaveTermList(int letterId, string termListIdList)
        {
            var output = string.Empty;
            int[] termListIds = null;

            // parse checked ids into an array of integers
            if (!string.IsNullOrEmpty(termListIdList))
            {
                termListIds = Utilities.ToIntArray(termListIdList);
            }
            
            // save the term lists to the session
            Utilities.SetTermList(termListIds);            

            // get the letter coded text with the selected term lists
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
               var  letter = letterManager.Get(letterId);

                if (letter != null)
                {
                    //form plus highlight section start
                    if (letter.LetterTypeId == 5)
                    {

                        try
                        {
                            if (letter.HtmlCodedText.IndexOf("data-formtype=\"plus\"") == -1)
                            {
                                HighLightFormPlus(letter);
                               // letter.HtmlCodedText = LetterUtilities.CodeTextBySessionTermList(letter.HtmlCodedText);
                            }
                            ViewData["CodingFormPlusLegend"] = "Y";
                        }
                        catch
                        {
                        };
                    }
                    //form plus highlight section end
                    output = string.Format("<result><codedText> <![CDATA[{0}]]></codedText></result>",
                        LetterUtilities.CodeTextBySessionTermList(letter.HtmlCodedText).ToHtmlString());
                   // output = LetterUtilities.CodeTextBySessionTermList(letter.HtmlCodedText).ToHtmlString();
                }
            }
            
            
            

            return Content(output);
        }

        [HasPrivilege(PrivilegeCodes = "PURR", Scope = Constants.PHASE)]
        public PartialViewResult UnpublishReasonInput(int projectId, int phaseId, int id)
        {
            return PartialView("~/Views/Shared/Input/ucLetterUnpublishReasonInput.ascx", new LetterUnpublishReasonInputViewModel
            {
                LetterId = id
            });
        }

        //*Added for comment code sort 
        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        public PartialViewResult SortCodeSequences(int phaseId, string letterCommentNumber)
        {
            Comment comment = null;
            Letter letter = null;
            var projectId = 0;
            var locked = false;

            // get the comment
            if (phaseId > 0 && !string.IsNullOrWhiteSpace(letterCommentNumber))
            {
                using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                {
                    comment = commentManager.Get(phaseId, letterCommentNumber);
                    if (comment != null)
                    {
                        letter = comment.Letter;
                        projectId = letter.Phase.ProjectId;
                        locked = letter.Phase.Locked;
                    }

                    var phasecodes = comment.CommentCodes;
               
                }
            }

          return PartialView("~/Views/Shared/Input/ucCommentCodeSort.ascx", new LetterCodingViewModel

            {
                Comment = comment,
                ProjectId = projectId,
                PhaseId = phaseId,
                Letter = letter,
                Locked = locked
            });
        }
        #endregion

        //*Added for comment code sort 
        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        [AjaxErrorHandler]
        public JsonResult UpdateCommentCodesSequence(int commentId, string sortedCodeList)
        {
            // add the codes to the comment
            bool updatedComment = LetterUtilities.UpdateCommentCodesSequence(commentId, sortedCodeList);
            String error = "";
            String success = "";
            if (updatedComment)
            {
                success = "success";
            }
            return Json(
                    new { error = error, success = success },
                    JsonRequestBehavior.AllowGet);
        }
            

        #region Letter Text Extraction Methods
        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
		[ValidateInput(false)]
        public ViewResult AddDocumentText(int projectId, int phaseId, int id, int documentId)
        {
            // Create the view model
            var viewModel = new DocumentExtractorViewModel
            {
                DocumentId = documentId,
                LetterId = id,
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction)                
            };

            viewModel.GetDocumentText();

            return View(GetViewPath(), viewModel);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [HttpPost, ValidateInput(false)]
        public ActionResult AddDocumentText(int projectId, int phaseId, int id, int documentId, string action, DocumentExtractorViewModel model)
        {
            if ("Add to Letter".Equals(action))
            {
                // append text to the letter
                model.AddTextToLetter(ModelState);

                // back to the coding page when complete
                if (ModelState.IsValid)
                {
                    return RedirectToAction(Constants.CODING, new { id = id });
                }
            }

            return View(GetViewPath(), model);
        }
        #endregion

        #region Author Action Methods
        public PartialViewResult AuthorList(int projectId, int phaseId, int id)
        {
            IList<Author> authors;
            using (var authorManager = ManagerFactory.CreateInstance<AuthorManager>())
            {
                authors = authorManager.All.Where(x => x.LetterId == id).ToList();

                // Load org property for view
                foreach (var author in authors)
                {
                    var org = author.Organization;
                }
            }

            return PartialView("~/Views/Shared/List/ucAuthorList.ascx", new AuthorListViewModel
            {
                Authors = authors,
                Mode = Constants.LIST
            });
        }
        #endregion

        #region LetterObjection Methods
        public PartialViewResult ObjectionInfo(int projectId, int phaseId, int id)
        {
            LetterObjection objectionInfo;
            using (var objectionManager = ManagerFactory.CreateInstance<LetterObjectionManager>())
            {
                objectionInfo = objectionManager.Get(id);
            }

            return PartialView("~/Views/Shared/Input/ucObjectionInfoInput.ascx", new ObjectionInfoInputViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                ObjectionInfo = objectionInfo
            });
        }

        [HttpPost]
        [AjaxErrorHandler]
        public JsonResult ObjectionInfo(int projectId, int phaseId, ObjectionInfoInputViewModel model)
        {
            JsonResult result = null;

            model.Validate(ModelState);

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage);
                result = Json(new { errorMessages = errors }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.Save();
                Json(null, JsonRequestBehavior.AllowGet);
            }

            return result;
        }
        #endregion

        #region LetterObjectionResponse Methods
        public PartialViewResult ObjectionResponse(int projectId, int phaseId, int id)
        {
            LetterObjection objectionInfo;
            using (var objectionManager = ManagerFactory.CreateInstance<LetterObjectionManager>())
            {
                objectionInfo = objectionManager.Get(id);
                var documents = objectionInfo.Documents; // get now to use later
            }

            return PartialView("~/Views/Shared/Input/ucObjectionResponseInput.ascx", new ObjectionResponseInputViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                ObjectionResponse = objectionInfo
            });
        }

        [HttpPost]
        [AjaxErrorHandler]
        public JsonResult ObjectionResponse(int projectId, int phaseId, ObjectionResponseInputViewModel model)
        {
            JsonResult result = null;

            // add attachments
            model.AddDocuments(Request.Files, ModelState);
            
            model.Validate(ModelState);

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage);
                result = Json(new { errorMessages = errors }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Determine which documents are being deleted
                var documentsIds = Request.Form.AllKeys
                    .Where(x => x.StartsWith("DocumentId_") && Request.Form[x].Contains("true"))
                    .Select(x => int.Parse(x.Split('_')[1]))
                    .ToList();

                model.Save(documentsIds);
                Json(null, JsonRequestBehavior.AllowGet);
            }

            return result;
        }
        #endregion


        #region Compare Master Methods
        public PartialViewResult CompareMaster(int projectId, int phaseId, int id, int targetLetterId, bool navigate = false)
        {
            // initialize the differ and builder objects
            var diff = new DiffPlex.Differ();
            var builder = new SideBySideDiffBuilder(diff);

            // get the letters
            Letter letter;
            Letter masterLetter;
            Letter nextLetter = null;
            Letter prevLetter = null;
            int letterFormSetCount;
            FormSet masterLetterFormSet = null;
            LetterType uniqueLetterType = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name == "Unique").First();
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                letter = letterManager.Get(id);
                int ltrSequence = letter.LetterSequence.HasValue ? letter.LetterSequence.Value : -1;

                // Enter this block if:
                //    The targetletterId is the master form letter of the letter specified by id
                //    The letter is unique and is not a part of a formset
                if (navigate
                     && (letter.FormSet != null 
                        || (letter.FormSet == null && letter.LetterTypeId == uniqueLetterType.LetterTypeId)))
                {
                    // get the filter
                    var filter = Utilities.GetFilter(Constants.FORM_SET + Constants.LETTERS, phaseId.ToString()) as FormSetSearchResultFilter;

                    if (filter == null)
                    {
                        filter = new FormSetSearchResultFilter();
                    }

                    var navigationFilter = new FormSetSearchResultFilter
                    {
                        PhaseId = filter.PhaseId,
                        LetterStatusId = filter.LetterStatusId,
                        LetterTypeId = filter.LetterTypeId,
                        FormSetId = letter.FormSetId
                    };

                    nextLetter = letterManager.GetNextFormLetter(navigationFilter, ltrSequence);
                    prevLetter = letterManager.GetPrevFormLetter(navigationFilter, ltrSequence);
                }

                letterFormSetCount = 0;
                if (letter.FormSet != null)
                {
                    letterFormSetCount = letterManager.All.Where(x => x.FormSetId == letter.FormSetId).Count();
                }

                masterLetter = new Letter();
                if (targetLetterId > 0)
                {
                    masterLetter = letterManager.Get(targetLetterId);
                    if (masterLetter != null)
                    {
                        masterLetterFormSet = masterLetter.FormSet;
                    }
                }
            }

            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            // get the diff viewer model
            var transformedMasterLetterText = TransformForComparison(masterLetter.Text);
            var transformedLetterText = TransformForComparison(letter.Text);

            var model = builder.BuildDiffModel(transformedMasterLetterText, transformedLetterText);
            
            // build the view model
            var viewModel = new CompareMasterViewModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                SideBySideDiffModel = model,
                MasterLetter = masterLetter,
                Letter = letter,
                NextLetter = nextLetter,
                PrevLetter = prevLetter,
                CanEdit = isActive,
                MasterLetterFormSet = masterLetterFormSet,
                LetterFormSetCount = letterFormSetCount,
                CanNavigate = navigate
            };

            // render the viewer control
            return PartialView("~/Views/Shared/Util/ucDiffViewer.ascx", viewModel);
        }
  
        private string TransformForComparison(string letterText)
        {
            // get the diff viewer model
            var transformedLetterText = string.Empty;
            if (!string.IsNullOrEmpty(letterText))
            {
                transformedLetterText = letterText;

                // Remove all line breaks
                transformedLetterText.ReplaceLineBreaks(" ");

                // Split the letter on periods
                var fragments = transformedLetterText.Split('.');

                var fragmentBuilder = new StringBuilder();
                foreach (var fragment in fragments)
                {
                    fragmentBuilder.Append(fragment);
                    fragmentBuilder.AppendLine(".");
                }

                transformedLetterText = fragmentBuilder.ToString();
            }

            return transformedLetterText;
        }

        #endregion

        #region Make Duplicate
        public PartialViewResult MakeDuplicate(int projectId, int phaseId, int id, string nextLetterNums, int? numberOfLettersRemaining)
        {
            MakeDuplicateModel model = new MakeDuplicateModel
            {
                ProjectId = projectId,
                PhaseId = phaseId,
                LetterId = id,
                NextLetterIds = nextLetterNums,
                LettersRemaining = numberOfLettersRemaining
            };

            return PartialView(@"~\views\shared\input\ucMakeDuplicate.ascx", model);
        }

        [HttpPost]
        public ActionResult MakeDuplicate(int projectId, int phaseId, MakeDuplicateModel model)
        {

            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                var masterDuplicate = letterManager.All.FirstOrDefault(x => x.PhaseId == model.PhaseId && x.LetterSequence == model.MasterDuplicateSequence);
                if (masterDuplicate != null)
                {
                    if (letterManager.MakeDuplicate(model.PhaseId, model.LetterId, masterDuplicate))
                    {
                        letterManager.UnitOfWork.Save();
                    }
                    else
                    {
                        TempData[Constants.ERROR] = string.Format("An error occurred attempting to make this letter a duplicate. Please contact the helpdesk.", model.MasterDuplicateSequence);
                    }
                }
                else
                {
                    TempData[Constants.ERROR] = string.Format("Cannot make duplicate because Master Duplicate Letter #{0} cannot be found", model.MasterDuplicateSequence);
                }               

                  
                    
            }
            return RedirectToAction("Coding", new
            {
                projectId = model.ProjectId,
                phaseId = model.PhaseId,
                id = model.LetterId,
                nextLetterIds = model.NextLetterIds,
                lettersRemaining = model.LettersRemaining,
                tab = "Details"
            });
            //return View(GetViewPath(), model);
        }
           
        
        #endregion Make Duplicate

        #region NonActions
        [NonAction]
        // Returns null if the contents of richText only contains markup, but no content
        private string RichTextValue(string richText)
        {
            string actualValue = null;

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml("<html><head></head><body>" + richText + "</body></html>");
            if (doc != null)
            {

                string output = "";
                foreach (var node in doc.DocumentNode.ChildNodes)
                {
                    output += node.InnerText;
                }

                if (!string.IsNullOrWhiteSpace(output))
                {
                    actualValue = txtReplacementConfig.ReplaceInvalidCharacters(richText);
                }

            }

            return actualValue;
        }
        #endregion NonActions

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion
    }
}
