﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the my profile specific actions
    /// </summary>
    public class ProfileController : PageController
    {
        #region Core Action Methods
        public ViewResult Input(int id)
        {
            User user;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                user = userManager.Get(id);
            }

            // build the model
            var model = new ProfileViewModel
            {
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                User = user
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Input(int id, ProfileViewModel model)
        {
            // save the data
            model.Save();
            
            return View(model);
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROFILE;
        }
        #endregion
    }
}
