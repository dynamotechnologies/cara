﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;

namespace Aquilent.Cara.WebApp.Controllers
{
    public class SupportController : PageController
    {
        #region Core Action Methods
        public ViewResult Index()
        {
            return View(new SupportViewModel
            {
                BreadCrumbTitle = Utilities.GetResourceString(Constants.SUPPORT),
                PageTitle = Utilities.GetResourceString(Constants.SUPPORT),
                Mode = Constants.SUPPORT
            });
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.SUPPORT;
        }
        #endregion
    }
}
