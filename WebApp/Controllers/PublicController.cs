﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Net.Http;
using System.Threading;

using ICSharpCode.SharpZipLib.Zip;
using Telerik.Web.Mvc;
using System.Configuration;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.Dmd;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Telerik.Reporting.Processing;
using Telerik.Reporting;
using System.Data.Common;
using System.Data.SqlClient;
using Aquilent.Cara.Domain.DataMart;
using System.ServiceModel;

using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;


namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the public form specific actions
    /// </summary>
    public class PublicController : AbstractController
    {
        #region Private Members
        private const int SEARCH_RESULTS_PER_PAGE = 25;
        private const string TRANSLATOR_SERVICE_ERROR = "El servicio de traducción al español no está disponible. Por favor, inténtelo de nuevo más tarde.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>Spanish translation service is not available. Please try again later.";
		private DmdServiceConfiguration _dmdConfig = ConfigUtilities.GetSection<DmdServiceConfiguration>("dmdServiceConfiguration");
        #endregion

        public ActionResult TimeOut(string userName)
        {
            ViewData["userName"] = userName;
            return View();
        }

        public ContentResult TestConnections()
        {
            return Content("");

            Boolean connection1 = true;
            Boolean connection2 = true;
            Boolean connection3 = true;

            /*MS SQL Server Connection*/

            try
            {
                String dbProvider = ConfigurationManager.ConnectionStrings["CaraDomainEntities"].ProviderName;
                String dbConnectionString = ConfigurationManager.ConnectionStrings["CaraDomainEntities"].ConnectionString;

                DbConnection sqlConnection;

                DbProviderFactory dbProFac = DbProviderFactories.GetFactory(dbProvider);
                sqlConnection = dbProFac.CreateConnection();
                sqlConnection.ConnectionString = dbConnectionString;
                sqlConnection.Open();
            }

            catch (SqlException)
            {
                connection1 = false;
            }

            /*DataMart API*/

            try
            {
                var dataMartManager = new DataMartManager();
                var dataMart = (IClientChannel)dataMartManager.GetDataMart();

                if (dataMart.State != CommunicationState.Opened)
                {
                    connection2 = false;
                }

            }

            catch (Exception)
            {
                connection2 = false;
            }

            /*Service Broker*/

            try
            {
                DmdManager dmdManager = new DmdManager();
                var testing = dmdManager.EchoMsg("testing");

                if (!String.Equals(testing, "testing"))
                {
                    connection3 = false;
                }
            }

            catch (Exception)
            {
                connection3 = false;
            }

            return Content("");
        }

        public ContentResult CheckCommentInput(string key, int projectId, int phaseId)
        {
            try
            {
                if (key.Equals("LXJJgKzTBgEUm3B"))
                {
                    var submittedDate = DateTime.UtcNow;
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        var phase = phaseManager.Get(phaseId);
                        if (phase != null && !string.IsNullOrEmpty(phase.TimeZone))
                        {
                            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(phase.TimeZone);
                            submittedDate = TimeZoneInfo.ConvertTimeFromUtc(submittedDate, tzi);
                        }
                    }

                    var newLetter = new LetterInputViewModel
                    {
                        Mode = Constants.CREATE,
                        PhaseId = phaseId,
                        Letter = new Letter
                            {
                                DateSubmitted = submittedDate,
                                HtmlText = "This is an automated test.",
                                LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name.Equals("New")).First().LetterStatusId,
                                LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name.Equals("Unique")).First().LetterTypeId,
						        Protected = false
                            },
                        Sender = true,
                        Author = new Author { FirstName = "CARA", LastName = "Automated Test", ContactMethod = "Do not contact me in the future" }
                    };

                    newLetter.Save(phaseId, ModelState); 

                    return Content("status=1");
                }

                else
                {
                    return Content("status=0"); 
                }
            }

            catch
            {
                return Content("status=0");
            }
        }

        #region Comment Input Form Actions
        public ViewResult CommentInput()
        {
            var submittedDate = DateTime.UtcNow;
			var model = new PublicCommentInputViewModel();

            try
            {
                               
                model.PalsProjectId = GetParam(Constants.PROJECT);

                if (model.Project != null)
                {
                    model.ProjectId = model.Project.ProjectId;
                }

                model.GetWebCommentPageInfo();
                model.WithinCommentPeriod = false.ConvertToYesNo(); // Default to No

                if (model.ExistInCara)
                {
                    // build the model
                    model.Public = true;
                    model.Mode = Constants.CREATE;
                    model.Letter = new Letter
                    {
                        DateSubmitted = submittedDate,
                        LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name.Equals("New")).First().LetterStatusId,
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name.Equals("Unique")).First().LetterTypeId,
                        DeliveryTypeId = LookupManager.GetLookup<LookupDeliveryType>().Where(x => x.Name.Equals("CARA Web-portal")).First().DeliveryTypeId,
                        Protected = true
                    };
                    model.Sender = true;
                    model.Author = new Author { ContactMethod = Constants.EMAIL };
                    model.PhaseId = model.CurrentPhase.PhaseId;
                    model.OrganizationCheck = false;

                    // set the within comment period flag based on the current phase
                    if (model.Project != null && model.Project.CurrentPhase != null)
                    {
                        model.WithinCommentPeriod = model.IsDateWithinPhase(submittedDate, model.Phase.TimeZone).ConvertToYesNo();
                    }
                }

                //code is no longer useful? JJR 2/21/2017
                ViewData["/Public/CommentInput?project="] = "/Public/CommentInputSp?project=" + model.PalsProjectId.ToString();
            }
            catch (Exception ex)
            {

                ViewData[Constants.ERROR] = string.Format("Project information of ID '{0}' cannot be retrieved", model.PalsProjectId);
                // empty the project id so that the page displays informational page
                model.PalsProjectId = null;
            }
            
            return View(model);
        }

        public ViewResult CommentInputSp()
        {
            var submittedDate = DateTime.UtcNow;
            var model = new PublicCommentInputViewModel();


            try
            {
                
                model.PalsProjectId = GetParam(Constants.PROJECT);

                if (model.Project != null)
                {
                    model.ProjectId = model.Project.ProjectId;
                }

                model.GetWebCommentPageInfo();
                model.WithinCommentPeriod = false.ConvertToYesNo(); // Default to No

                if (model.ExistInCara)
                {
                    // build the model
                    model.Public = true;
                    model.Mode = Constants.CREATE;
                    model.Letter = new Letter
                    {
                        DateSubmitted = submittedDate,
                        LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name.Equals("New")).First().LetterStatusId,
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name.Equals("Unique")).First().LetterTypeId,
                        DeliveryTypeId = LookupManager.GetLookup<LookupDeliveryType>().Where(x => x.Name.Equals("CARA Web-portal")).First().DeliveryTypeId,
                        Protected = true
                    };
                    model.Sender = true;
                    model.Author = new Author { ContactMethod = Constants.EMAIL };
                    model.PhaseId = model.CurrentPhase.PhaseId;
                    model.OrganizationCheck = false;

                    // set the within comment period flag based on the current phase
                    if (model.Project != null && model.Project.CurrentPhase != null)
                    {
                        model.WithinCommentPeriod = model.IsDateWithinPhase(submittedDate, model.Phase.TimeZone).ConvertToYesNo();
                    }
                }
                try
                {
                    if (!TraslateSpanishCommentInput(model))
                    {
                        // int aa = 0;
                        // int bb = 1 / aa;
                        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Aquilent.Cara.WebApp.Controllers.PublicController));
                        logger.Info("Translation Service Error. Wrong translation for test message");
                        TempData["SpanishTranslationError"] = TRANSLATOR_SERVICE_ERROR;
                        Response.Redirect("/Public/CommentInput?project=" + model.PalsProjectId);


                    }
                }
                catch (Exception ex)
                {
                    TempData["SpanishTranslationError"] = TRANSLATOR_SERVICE_ERROR;
                    log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Aquilent.Cara.WebApp.Controllers.PublicController));
                    logger.Info("Translation Service Error", ex);
                    Response.Redirect("/Public/CommentInput?project=" + model.PalsProjectId);

                }
            }
            catch (Exception ex)
            {

                ViewData[Constants.ERROR] = string.Format("Project information of ID '{0}' cannot be retrieved", model.PalsProjectId);
                // empty the project id so that the page displays informational page
                model.PalsProjectId = null;
            }
            
            
            /*
            CaraReferenceEntities aa = new CaraReferenceEntities();
            var dt = ((ObjectContext)aa).ExecuteStoreQuery<Country>("Select * from CountrySp");
            List<SelectListItem> lc = dt.Select(pp => new SelectListItem() { Value = pp.CountryId.ToString(), Text = pp.Name }).OrderBy(pp => pp.Text).ToList();
            model.LookupCountrySp =  lc;
            */
           

            return View(model);
        }
        

        [RecaptchaValidator]
		[HttpPost, ValidateInput(false)]
		public ActionResult CommentInput(PublicCommentInputViewModel model, bool captchaValid)
        {
            // if form validation succeed
            if (ModelState.IsValid)
            {
               // add attachments
                model.AddDocuments(Request.Files, ModelState);

                // validate the input
                model.Validate(ModelState);

                // validate the recaptcha
                if (!captchaValid)
                {
                    ModelState.AddModelError(string.Empty, "You did not enter the reCAPTCHA correctly. Please try again.");
                }

                if (ModelState.IsValid)
                {
                    // save the letter
                    var letterId = model.Save(model.CurrentPhase.PhaseId, ModelState);

                    if (letterId > 0 && ModelState.IsValid)
                    {
                        Letter letter;
                        using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            letter = letterManager.Get(letterId);
                        }

                        // redirect to confirmation page
                        return RedirectToAction("CommentConfirm", new { project = model.PalsProjectId, publicId = string.Format("{0}-{1}-{2}", model.PalsProjectId, model.CurrentPhase.PhaseId, letter.LetterSequence) });
                    }
                }
            }

            return View(model);
        }
        //CR#239 - On multi-district projects contact edit
        [RecaptchaValidator]
        [HttpPost, ValidateInput(false)]
        public ActionResult CommentInputSp(PublicCommentInputViewModel model, bool captchaValid)
        {
            // if form validation succeed
            if (ModelState.IsValid)
            {
                // add attachments
                model.AddDocuments(Request.Files, ModelState);

                // validate the input
                model.Validate(ModelState);

                // validate the recaptcha
                if (!captchaValid)
                {
                    ModelState.AddModelError(string.Empty, "You did not enter the reCAPTCHA correctly. Please try again.");
                }

                if (ModelState.IsValid)
                {
                    // save the letter
                    var letterId = model.Save(model.CurrentPhase.PhaseId, ModelState);

                    if (letterId > 0 && ModelState.IsValid)
                    {
                        Letter letter;
                        using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            letter = letterManager.Get(letterId);
                        }

                        // redirect to confirmation page
                        return RedirectToAction("CommentConfirmSp", new { project = model.PalsProjectId, publicId = string.Format("{0}-{1}-{2}", model.PalsProjectId, model.CurrentPhase.PhaseId, letter.LetterSequence) });
                    }
                }
                else
                {
                    
                    try
                    {
                        if (!TraslateSpanishCommentInput(model))
                        {
                            //int aa = 0;
                            //int bb = 1 / aa;
                            log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Aquilent.Cara.WebApp.Controllers.PublicController));
                            logger.Info("Translation Service Error. Wrong translation for test message");
                            TempData["SpanishTranslationError"] = TRANSLATOR_SERVICE_ERROR;
                            Response.Redirect("/Public/CommentInput?project=" + model.PalsProjectId);


                        }
                    }
                    catch (Exception ex)
                    {
                        TempData["SpanishTranslationError"] = TRANSLATOR_SERVICE_ERROR;
                        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Aquilent.Cara.WebApp.Controllers.PublicController));
                        logger.Info("Translation Service Error", ex);
                        Response.Redirect("/Public/CommentInput?project=" + model.PalsProjectId);

                    }
                    
                }
            }

            return View(model);
        }

        public ViewResult CommentConfirm()
        {
            Project project = null;
            Phase currentPhase;
            WebCommentPageInfo commentPageInfo;
            string computedAddress = null;

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.GetByProjectNumber(GetParam(Constants.PROJECT));
                currentPhase = project.CurrentPhase.Phase;

                if (project.IsPalsProject)
                {
                    commentPageInfo = project.WebCommentPageInfos.FirstOrDefault();


                    commentPageInfo.ContactName = String.IsNullOrWhiteSpace(currentPhase.ContactName) ? commentPageInfo.ContactName : currentPhase.ContactName;
                    commentPageInfo.CommentEmail = String.IsNullOrWhiteSpace(currentPhase.CommentEmail) ? commentPageInfo.CommentEmail : currentPhase.CommentEmail;
                    computedAddress = String.IsNullOrWhiteSpace(GetFullAddressFromCurrentPhase()) ? commentPageInfo.FullAddress : GetFullAddressFromCurrentPhase();
                                       
                    
                }
                else
                {
                    commentPageInfo = new WebCommentPageInfo();
                    var pointOfContact = currentPhase.PhaseMemberRoles.FirstOrDefault(x => x.IsPointOfContact);
                    if (pointOfContact != null)
                    {
                        commentPageInfo.ContactName = pointOfContact.User.FullName;
                        commentPageInfo.CommentEmail = pointOfContact.User.Email;
                    }
                }
            }
            
                PublicCommentConfirmModel model = new PublicCommentConfirmModel
                {
                    ProjectNumber = GetParam(Constants.PROJECT),
                    ProjectName = "ERROR - Cannot determine",
                    LetterReferenceNumber = "ERROR - Cannot determine",
                    IsPalsProject = false
                };

                if (project != null)
                {
                    model = new PublicCommentConfirmModel
                    {
                        ProjectNumber = project.ProjectNumber,
                        ProjectName = project.Name,
                        LetterReferenceNumber = GetParam("publicId"),
                        IsPalsProject = project.IsPalsProject,
                        IsObjection = currentPhase.PhaseTypeId == 3,
                        ContactName = commentPageInfo.ContactName,
                        ContactAddress = computedAddress,
                        ContactEmail = commentPageInfo.CommentEmail,
                        UnitName = project.UnitName
                    };
                }
          
                return View(model);
             
        }

        //CR#239 - On multi-district projects contact edit
        private string GetFullAddressFromCurrentPhase()
        {
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                Project project = null;
                Phase currentPhase;

                project = projectManager.GetByProjectNumber(GetParam(Constants.PROJECT));
                currentPhase = project.CurrentPhase.Phase;
                string retVal = null;
                if (!project.IsPalsProject)
                {
                    return retVal;
                }
                retVal = string.Format("{0} {1} {2} {3} {4}",
                    currentPhase.MailAddressStreet1,
                    currentPhase.MailAddressStreet2 + (!string.IsNullOrWhiteSpace(currentPhase.MailAddressStreet1 + currentPhase.MailAddressStreet2) ? "," : string.Empty),
                    currentPhase.MailAddressCity + (!string.IsNullOrWhiteSpace(currentPhase.MailAddressCity) ? "," : string.Empty),
                    currentPhase.MailAddressState + (!string.IsNullOrWhiteSpace(currentPhase.MailAddressState) ? "," : string.Empty),
                    currentPhase.MailAddressZip).Trim();

                return retVal;
            }
        }

        public ViewResult CommentConfirmSp()
        {
            Project project = null;
            Phase currentPhase;
            WebCommentPageInfo commentPageInfo;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.GetByProjectNumber(GetParam(Constants.PROJECT));
                currentPhase = project.CurrentPhase.Phase;

                if (project.IsPalsProject)
                {
                    commentPageInfo = project.WebCommentPageInfos.FirstOrDefault();
                }
                else
                {
                    commentPageInfo = new WebCommentPageInfo();
                    var pointOfContact = currentPhase.PhaseMemberRoles.FirstOrDefault(x => x.IsPointOfContact);
                    if (pointOfContact != null)
                    {
                        commentPageInfo.ContactName = pointOfContact.User.FullName;
                        commentPageInfo.CommentEmail = pointOfContact.User.Email;
                    }
                }
            }

            PublicCommentConfirmModel model = new PublicCommentConfirmModel
            {
                ProjectNumber = GetParam(Constants.PROJECT),
                ProjectName = "ERROR - No se puede determinar",
                LetterReferenceNumber = "ERROR - Cannot determine",
                IsPalsProject = false
            };

            if (project != null)
            {
                model = new PublicCommentConfirmModel
                {
                    ProjectNumber = project.ProjectNumber,
                    ProjectName = project.Name,
                    LetterReferenceNumber = GetParam("publicId"),
                    IsPalsProject = project.IsPalsProject,
                    IsObjection = currentPhase.PhaseTypeId == 3,
                    ContactName = commentPageInfo.ContactName,
                    ContactAddress = commentPageInfo.FullAddress,
                    ContactEmail = commentPageInfo.CommentEmail,
                    UnitName = project.UnitName
                };
            }
            if (Session["CommentConfirmSp.model.ProjectNameNumber"] != null)
            {
                ViewData["model.ProjectNameNumber"] = Session["CommentConfirmSp.model.ProjectNameNumber"];
                Session.Remove("CommentConfirmSp.model.ProjectNameNumber");
            }
            else
            {
                ViewData["model.ProjectNameNumber"] = model.ProjectName + " #" + model.ProjectNumber;
            }
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">PhaseId</param>
        /// <param name="dmdId"></param>
        /// <returns></returns>
        [HttpHeader("X-Robots-Tag", "none")]
        public FileContentResult DownloadCommentPeriodDocument(int id, string dmdId)
        {
            bool showMessage = false;
            string errorMessage = "The file that you are trying to download (reference #: {0}) could not be found or is not yet available.  Please navigate to the previous page and try again.  If the problem persists contact technical support.";

            // Verify the DMD ID is part of this phase (i.e., prevent URL tampering)
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                try
                {
                    var phase = phaseManager.Get(id);
                    if (phase != null)
                    {
                        phase.Documents.Single(x => x.DmdDocumentId == dmdId);
                    }
                }
                catch (InvalidOperationException ioe)
                {
                    showMessage = true;
                }
            }

            FileContentResult returnResult = null;

            if (!showMessage)
            {
                // get file data from dmd
                var dmdManager = new DmdManager();
                var fileMetaData = dmdManager.GetFileMetadata(dmdId);

                string docType = "PDF";
                if (fileMetaData != null)
                {
                    try
                    {
                        var fileData = dmdManager.GetFileData(dmdId, docType);

                        if (fileData != null)
                        {
                            // send the file data back
                            var fileName = fileMetaData.Filename;
                            if (!fileName.ToUpper().EndsWith(docType))
                            {
                                fileName += ".pdf";
                            }

                            returnResult = File(fileData, "application/pdf", fileName);
                        }
                        else
                        {
                            showMessage = true;
                        }
                    }
                    catch (DomainException de)
                    {
                        errorMessage = "The file that you are trying to download (reference #: {0}) is not yet available to view.  If the problem persists longer than 1 hour, please contact technical support.";
                        showMessage = true;
                    }
                }
                else
                {
                    showMessage = true;
                }
            }

            if (showMessage)
            {
                throw new Exception(String.Format(errorMessage, dmdId));
            }

            return returnResult;
        }

        [NonAction]
        private bool TraslateSpanishCommentInput(PublicCommentInputViewModel model)
        {
            string from = "en";
            string to = "es";

            //look in to storing translated text on model for consistency

            ViewData["model.EndDate"] = model.EndDate.Day.ToString() + "/" + model.EndDate.Month.ToString() + "/" + model.EndDate.Year.ToString(); //model.Author.
            
            //possibly not useful code?
            ViewData["/Public/CommentInput?project="] = "/Public/CommentInput?project=" + model.PalsProjectId.ToString();

            int isArr = 0;
            if (model.DmdDocumentViewModel.Documents != null && model.DmdDocumentViewModel.Documents.Count() > 0)
            { 
                isArr = 25 + model.DmdDocumentViewModel.Documents.Count(); 
            }
            else
            { 
                isArr = 25; 
            }

            string[] sArr = new string[isArr];
            int iCurrentIndex = 0;
            // why is this used for checking values instead of null?
            sArr[iCurrentIndex] = "Test message";
            sArr[++iCurrentIndex] = model.PublicFormText ?? string.Empty;
            sArr[++iCurrentIndex] =  model.ProjectNameNumber;

            if (model.Project != null)
            {
                sArr[++iCurrentIndex] = model.Project.Description;
            }

            int basicInfoCount = iCurrentIndex + 1;

            if (ViewData[Constants.ERROR] != null)
            {
                sArr[++iCurrentIndex] =  ViewData[Constants.ERROR].ToString();
            }
            
            if (isArr > 25)
            {
                foreach (Domain.DataMart.DmdDocument dd in model.DmdDocumentViewModel.Documents)
                {
                    sArr[++iCurrentIndex] =  dd.Name;
                }
            }

            var mls = ModelState.Where(p => p.Value.Errors.Count > 0).ToDictionary(pp => pp.Key, pp => pp.Value.Errors);
            if (mls.Count > 0)
            {
                foreach (string sKey in mls.Keys)
                {
                    ModelErrorCollection mec = null;
                    mec = ModelState[sKey].Errors;
                    if (mec.Count > 0)
                    {
                        foreach (ModelError me in mec)
                        {
                            sArr.SetValue(me.ErrorMessage, ++iCurrentIndex);
                            if (++iCurrentIndex == isArr) { break; }
                        }
                    }
                    if (iCurrentIndex == isArr) { break; }
                }
            }

            string[] sArrToTr = new string[iCurrentIndex + 1]; 
            Array.Copy(sArr, sArrToTr, iCurrentIndex + 1);

            // New method created to use MS Translator Text API V3
            string[] tar = TranslateTextFromTo(sArrToTr, from, to);

            int tarIndex = 0;
            // I don't know why it checks validity by using a test value (as opposed to null) but I'm keeping it in case it applies in some way I don't understand
            if (tar[tarIndex] != "Mensaje de prueba")
            {
                return false;
            }
            ViewData["model.PublicFormText"] = System.Web.HttpUtility.HtmlDecode(tar[++tarIndex]).Replace("< br/>", "<br/>");
            ViewData["model.ProjectNameNumber"] = System.Web.HttpUtility.HtmlDecode(tar[++tarIndex]).Replace("< br/>", "<br/>");
            if (model.Project != null)
            {
                ViewData["model.Project.Description"] = System.Web.HttpUtility.HtmlDecode(tar[++tarIndex]).Replace("< br/>", "<br/>");
            }
            
            Session["CommentConfirmSp.model.ProjectNameNumber"] = ViewData["model.ProjectNameNumber"];
            iCurrentIndex = basicInfoCount;
            if (ViewData[Constants.ERROR] != null)
            {
                ViewData[Constants.ERROR] = tar[iCurrentIndex];
                iCurrentIndex++;
            }
            if (isArr > 25)
            {
                foreach (Domain.DataMart.DmdDocument dd in model.DmdDocumentViewModel.Documents)
                {
                    ViewData["Document" + dd.DmdDocumentId] = tar[iCurrentIndex];
                    iCurrentIndex++;
                }

            }
            if (mls.Count > 0)
            {
                foreach (string sKey in mls.Keys)
                {
                    ModelErrorCollection mec = ModelState[sKey].Errors;
                    int iErrCount = mec.Count;
                    mec.Clear();
                    for (int i = 0; i < iErrCount;i++ )
                    {
                        ModelState.AddModelError(sKey, tar[iCurrentIndex]);
                        iCurrentIndex++;
                        if (iCurrentIndex == isArr) { break; }
                    }
                    
                    if (iCurrentIndex == isArr) { break; }
                }
            }
            
            return true;
        }


        //[NonAction]
        //private bool TraslateSpanishCommentInput(PublicCommentInputViewModel model)
        //{
        //    //look in to storing translated text on model for consistency

        //    Aquilent.Cara.Reference.TranslatorService.LanguageServiceClient client = new Aquilent.Cara.Reference.TranslatorService.LanguageServiceClient();
        //    //AdmAuthentication admAuth = new AdmAuthentication(ConfigurationManager.AppSettings["TranslatorServiceClientId"], ConfigurationManager.AppSettings["TranslatorServiceClientSecret"]);
        //    //AdmAccessToken admToken = admAuth.GetAccessToken();
        //    string headerValue = GetHeaderValue();// admToken.access_token;
        //    ViewData["model.EndDate"] = model.EndDate.Day.ToString() + "/" + model.EndDate.Month.ToString() + "/" + model.EndDate.Year.ToString(); //model.Author.

        //    //possibly not useful code?
        //    ViewData["/Public/CommentInput?project="] = "/Public/CommentInput?project=" + model.PalsProjectId.ToString();

        //    int isArr = 0;
        //    if (model.DmdDocumentViewModel.Documents != null && model.DmdDocumentViewModel.Documents.Count() > 0)
        //    {
        //        isArr = 25 + model.DmdDocumentViewModel.Documents.Count();
        //    }
        //    else
        //    {
        //        isArr = 25;
        //    }

        //    string[] sArr = new string[isArr];
        //    int iCurrentIndex = 0;
        //    sArr[iCurrentIndex] = "Test message";
        //    sArr[++iCurrentIndex] = model.PublicFormText ?? string.Empty;
        //    sArr[++iCurrentIndex] = model.ProjectNameNumber;

        //    if (model.Project != null)
        //    {
        //        sArr[++iCurrentIndex] = model.Project.Description;
        //    }

        //    int basicInfoCount = iCurrentIndex + 1;

        //    if (ViewData[Constants.ERROR] != null)
        //    {
        //        sArr[++iCurrentIndex] = ViewData[Constants.ERROR].ToString();
        //    }

        //    if (isArr > 25)
        //    {
        //        foreach (Domain.DataMart.DmdDocument dd in model.DmdDocumentViewModel.Documents)
        //        {
        //            sArr[++iCurrentIndex] = dd.Name;
        //        }
        //    }

        //    var mls = ModelState.Where(p => p.Value.Errors.Count > 0).ToDictionary(pp => pp.Key, pp => pp.Value.Errors);
        //    if (mls.Count > 0)
        //    {
        //        foreach (string sKey in mls.Keys)
        //        {
        //            ModelErrorCollection mec = null;
        //            mec = ModelState[sKey].Errors;
        //            if (mec.Count > 0)
        //            {
        //                //string[] sArr = new string[mec.Count];

        //                foreach (ModelError me in mec)
        //                {
        //                    sArr.SetValue(me.ErrorMessage, ++iCurrentIndex);
        //                    if (++iCurrentIndex == isArr) { break; }
        //                }
        //            }
        //            if (iCurrentIndex == isArr) { break; }
        //        }
        //    }

        //    string[] sArrToTr = new string[iCurrentIndex + 1];
        //    Array.Copy(sArr, sArrToTr, iCurrentIndex + 1);
        //    //Cara.Reference.TranslatorService.TranslateOptions op = new Cara.Reference.TranslatorService.TranslateOptions();
        //    //op.ContentType = "text/plain";
        //    //Aquilent.Cara.Reference.TranslatorService.TranslateArrayResponse[] tar = client.Translate(headerValue, sArrToTr, "en", "es", null);
        //    Aquilent.Cara.Reference.TranslatorService.TranslateArrayResponse[] tar = client.TranslateArray(headerValue, sArrToTr, "en", "es", null);
        //    int tarIndex = 0;
        //    if (tar[tarIndex].TranslatedText != "Mensaje de prueba")
        //    {
        //        return false;
        //    }
        //    ViewData["model.PublicFormText"] = System.Web.HttpUtility.HtmlDecode(tar[++tarIndex].TranslatedText).Replace("< br / >", "<br/>");
        //    ViewData["model.ProjectNameNumber"] = System.Web.HttpUtility.HtmlDecode(tar[++tarIndex].TranslatedText).Replace("< br / >", "<br/>");
        //    if (model.Project != null)
        //    {
        //        ViewData["model.Project.Description"] = System.Web.HttpUtility.HtmlDecode(tar[++tarIndex].TranslatedText).Replace("< br / >", "<br/>");
        //    }

        //    Session["CommentConfirmSp.model.ProjectNameNumber"] = ViewData["model.ProjectNameNumber"];
        //    iCurrentIndex = basicInfoCount;
        //    if (ViewData[Constants.ERROR] != null)
        //    {
        //        ViewData[Constants.ERROR] = tar[iCurrentIndex].TranslatedText;
        //        iCurrentIndex++;
        //    }
        //    if (isArr > 25)
        //    {
        //        foreach (Domain.DataMart.DmdDocument dd in model.DmdDocumentViewModel.Documents)
        //        {
        //            ViewData["Document" + dd.DmdDocumentId] = tar[iCurrentIndex].TranslatedText;
        //            iCurrentIndex++;
        //        }

        //    }
        //    if (mls.Count > 0)
        //    {
        //        foreach (string sKey in mls.Keys)
        //        {
        //            ModelErrorCollection mec = ModelState[sKey].Errors;
        //            int iErrCount = mec.Count;
        //            mec.Clear();
        //            for (int i = 0; i < iErrCount; i++)
        //            {
        //                ModelState.AddModelError(sKey, tar[iCurrentIndex].TranslatedText);
        //                iCurrentIndex++;
        //                if (iCurrentIndex == isArr) { break; }
        //            }

        //            if (iCurrentIndex == isArr) { break; }
        //        }
        //    }

        //    client.Close();
        //    return true;
        //}
        #endregion

        #region Reading Room Actions
        public ContentResult GetReadingRoomData(string project)
        {
            // create the model
            var model = new LetterReadingRoomListViewModel { ProjectNumber = project };

            // return the xml result
            return Content(ReadingRoomService.GetReadingRoomActiveResultXml(model.ReadingRoomAvailable), "text/xml");
        }

        [HttpHeader("X-Robots-Tag", "none")]
        [GridAction(GridName = Constants.LIST)]
        public ViewResult ReadingRoom(GridCommand command)
        {
            // Create the view model
            var viewModel = LoadReadingRoomListModel(command);

            return View(Constants.READING_ROOM, viewModel);
        }

        [HttpPost]
        [HttpHeader("X-Robots-Tag", "none")]
        public ActionResult ReadingRoom(string action, LetterReadingRoomListViewModel model)
        {
            // get search results per page from querystring if applicable
            var searchResultsPerPage = SEARCH_RESULTS_PER_PAGE;

            if (!string.IsNullOrEmpty(GetParam("SearchResultsPerPage")))
            {
                searchResultsPerPage = Convert.ToInt32(GetParam("SearchResultsPerPage").Split(',')[0]);
            }

            // set up the parameters
            var parms = new { Project = model.ProjectNumber, SearchResultsPerPage = searchResultsPerPage };

            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // save the filter in the session
					Utilities.SetFilter(GetSessionKey(model.Project.ProjectId), string.Empty, model.GetFilterFromModel() as LetterReadingRoomSearchResultFilter);
                }

                ViewData["total"] = 0;

                // pass the view model, list binding method will get the data
                return RedirectToAction(Constants.READING_ROOM, parms);
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
				Utilities.SetFilter(GetSessionKey(model.Project.ProjectId), string.Empty, null);

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.READING_ROOM, parms);
            }

            return View(model);
        }

        [HttpHeader("X-Robots-Tag", "none")]
        public ViewResult Letter(int id, string project)
        {
            var model = new PublicLetterViewModel
            {
                LetterId = id,
                ProjectNumber = project,
                PageTitle = string.Format("{0} - {1} {2}", Utilities.GetResourceString(Constants.PUBLIC_READING_ROOM), Constants.VIEW, Utilities.GetResourceString(Constants.LETTER))
            };

            return View(model);
        }

        [HttpHeader("X-Robots-Tag", "none")]
        [ActionName("DownloadCommentFileComment")]
        public ActionResult DownloadLetterComment(int LetterId, string pdfDisplay = "false")
        {
            ActionResult returnResult = RedirectToAction(Constants.READING_ROOM, Constants.PUBLIC, new { project = GetParam(Constants.PROJECT) });
            using (LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                bool showMessage = false;
                // Turn off lazy loading
                //letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;
                Letter letterData = letterManager.All.Where(ld => ld.LetterId == LetterId).FirstOrDefault();//.Get(150845);
                if (letterData != null)
                {
                    try
                    {
                        Author authorData = letterData.Sender;

                        string htmlAuth = @"<span style=""font-size: 12px"">" + "Data Submited (UTC 11): " + System.Web.HttpUtility.HtmlEncode(letterData.DateSubmitted.ToString()) + "<br/>"
                                                + "First name: " + System.Web.HttpUtility.HtmlEncode(authorData.FirstName) + "<br/>"
                                                + "Last name: " + System.Web.HttpUtility.HtmlEncode(authorData.LastName) + "<br/>"
                                                + "Organization: " + System.Web.HttpUtility.HtmlEncode(authorData.Organization) + "<br/>"
                                                + "Title: " + System.Web.HttpUtility.HtmlEncode(authorData.Title) + "<br/>"
                                                + "Official Representative/Member Indicator :" + System.Web.HttpUtility.HtmlEncode(authorData.OfficialRepresentativeTypeName) + "<br/>"
                                                + "Address1: " + System.Web.HttpUtility.HtmlEncode(authorData.Address1) + "<br/>"
                                                + "Address2: " + System.Web.HttpUtility.HtmlEncode(authorData.Address2) + "<br/>"
                                                + "City: " + System.Web.HttpUtility.HtmlEncode(authorData.City) + "<br/>"
                                                + "State: " + System.Web.HttpUtility.HtmlEncode(authorData.StateName) + "<br/>"
                                                + "Province/Region: " + System.Web.HttpUtility.HtmlEncode(authorData.ProvinceRegion) + "<br/>"
                                                + "Zip/Posstal Code: " + System.Web.HttpUtility.HtmlEncode(authorData.ZipPostal) + "<br/>"
                                                + "Country: " + System.Web.HttpUtility.HtmlEncode(authorData.CountryName) + "<br/>"
                                                + "Email: " + System.Web.HttpUtility.HtmlEncode(authorData.Email) + "<br/>"
                                                + "Phone: " + System.Web.HttpUtility.HtmlEncode(authorData.Phone) + "<br/>"
                                                + "Comments: "
                                                ;
                        Cara.Reporting.Reports.StandardReports.LeterWithCommentReport crp = new Cara.Reporting.Reports.StandardReports.LeterWithCommentReport();
                        string modifiedLetterText = letterData.HtmlCodedText;
                        foreach (Comment comment in letterData.Comments)
                        {
                            string oldString = string.Format("{0}{1}]",LetterUtilities.COMMENT_START_TAG, comment.LetterCommentNumber);
                            /*string codeString = string.Empty;
                            foreach (CommentCode commentCode in comment.CommentCodes)
                            {
                                codeString = string.Format("{0}{1}{2}: {3}",
                                    codeString, 
                                    string.IsNullOrWhiteSpace(codeString)? string.Empty : ", ",
                                    commentCode.PhaseCode.CodeNumberDisplay,
                                    commentCode.PhaseCode.CodeName);
                            }*/
                            string newString = string.Format("{0}{1}({2})]",
                                LetterUtilities.COMMENT_START_TAG,
                                comment.LetterCommentNumber,
                                comment.CommentCodeText.Replace("&","&amp;").Replace("|", ", "));
                            modifiedLetterText = modifiedLetterText.Replace(oldString, newString);
                        }
                        //escape curly braces for telerik reporting
                        modifiedLetterText = modifiedLetterText.Replace("{", "{{").Replace("}", "}}");
                        crp.htmlTextBox1.Value = htmlAuth + modifiedLetterText + "</span>";

                        ReportProcessor reportProcessor = new ReportProcessor();
                        RenderingResult result = reportProcessor.RenderReport("PDF", crp, null);
                        returnResult = File(result.DocumentBytes, "application/pdf");
                    }
                    catch (Exception ex)
                    {
                        showMessage = true;
                    }
                }
                else
                {
                    showMessage = true;
                }

                if (showMessage)
                {
                    throw new Exception(String.Format
                        ("The file that you are trying to download (reference #: {0}) could not be found.  Please navigate to the previous page and try again.  If the problem persists contact technical support.", LetterId));
                }

                return returnResult;
            }

        }

        [HttpHeader("X-Robots-Tag", "none")]
        [ActionName("DownloadCommentFile")]
        public ActionResult DownloadDmd(string dmdId, string pdfDisplay = "false")
        {
            
            
            ActionResult returnResult = RedirectToAction(Constants.READING_ROOM, Constants.PUBLIC, new { project = GetParam(Constants.PROJECT) });

            // get file data from dmd
            var letterFilename = _dmdConfig.LetterDefaults.Filename;
            var dmdManager = new DmdManager();
           var fileMetaData = dmdManager.GetFileMetadata(dmdId);
            bool showMessage = false;
            string docType = null;
            if (fileMetaData != null)
            {
                if (fileMetaData.Filename == letterFilename)
                {
                    docType = "PDF";
                    fileMetaData.Filename = "LetterText.pdf";
                }

                byte[] fileData = null;
                try
                {
                    fileData = dmdManager.GetFileData(dmdId, docType);
                }
                catch (Exception ex)
                {
                    // This exception usually only occurs if the PDF version is not yet available.
                    showMessage = true;
                }

                if (fileData != null)
                {
                    string contentType;
                    using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        contentType = documentManager.ContentType(Path.GetExtension(fileMetaData.Filename).Trim('.'));
                    }

                    if (docType != null)
                    {
                        contentType = "application/pdf";
                    }
                    
                     returnResult = File(fileData, contentType);
                     
                   
                }
                else
                {
                    showMessage = true;
                }
            }
            else
            {
                showMessage = true;
            }

            if (showMessage)
            {               
                throw new Exception(String.Format
                    ("The file that you are trying to download (reference #: {0}) could not be found.  Please navigate to the previous page and try again.  If the problem persists contact technical support.", dmdId));
            }

            return returnResult;
        }

        [HttpHeader("X-Robots-Tag", "none")]
        [ActionName("DownloadCommentFiles")]
        public ActionResult DownloadDmds(int letterId, string project)
        {
			return DownloadLetters(letterId.ToString());
        }

        public PartialViewResult RecaptchaInput()
        {
            return PartialView("~/Views/Shared/Util/ucRecaptcha.ascx", new RecaptchaViewModel
                {
                    Description = "For security reasons, please complete the reCAPTCHA and click Submit.",
                    RemoteIp = Request.UserHostAddress
                });
        }
        
        public ContentResult RecaptchaVerify(string responseValue)
        {
            return Content(Utilities.IsRecaptchaValid(Request.RequestContext.HttpContext, responseValue).ToString());
        }

        [HttpHeader("X-Robots-Tag", "none")]
        public ActionResult DownloadLetters(string letterIdList)
        {
			string projectNumber = GetParam(Constants.PROJECT);
			ActionResult returnAction = RedirectToAction(Constants.READING_ROOM, Constants.PUBLIC, new { project = projectNumber });

			MemoryStream stream = null; // in memory stream for the zip file
			ZipOutputStream zipOutputStream = null; // zip file stream
			ZipEntry zipEntry;

            try
            {
                if (!string.IsNullOrEmpty(letterIdList))
                {
                    // split the string ids into int ids
                    var letterIds = letterIdList.Split(',');

                    if (letterIds != null)
                    {
                        using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            // convert letter ids to int type
                            var intLetterIds = letterIds.Select(x => Convert.ToInt32(x)).ToList();

                            stream = new MemoryStream();
                            zipOutputStream = new ZipOutputStream(stream);

                            var dmdManager = new DmdManager();

                            // get all checked letter objects
                            var letters = letterManager.All.Where(x => intLetterIds.Contains(x.LetterId)).ToList();

                            // loop thru each letter
                            foreach (var letter in letters)
                            {
                                // letter text dmd id
                                string letterDmdId = null;

                                // get letter document dmd ids
                                var dmdIds = LetterService.GetDmdIds(letter, out letterDmdId);

                                // loop thru each dmd file
                                foreach (var dmdId in dmdIds)
                                {
                                    var fileMetaData = dmdManager.GetFileMetadata(dmdId);
                                    string fileType = null;
                                    // if it's a letter text, pass in the PDF param to always get back the PDF
                                    if (dmdId.Equals(letterDmdId))
                                    {
                                        fileType = "PDF";
                                        fileMetaData.Filename = "LetterText.pdf";
                                    }

                                    var fileData = dmdManager.GetFileData(dmdId, fileType);

                                    if (fileMetaData != null && fileData != null)
                                    {
                                        // prepend letter id as the prefix for better goruping
                                        zipEntry = new ZipEntry(
                                            string.Format(
                                                "{0}_{1}_{2}_{3}",
                                                projectNumber,
                                                letter.Phase.PhaseTypeDesc,
                                                letter.LetterSequence.ToString(),
                                                fileMetaData.Filename
                                            )
                                        );

                                        // zip entry for dmd file
                                        zipOutputStream.PutNextEntry(zipEntry);
                                        zipOutputStream.Write(fileData, 0, fileData.Length);
                                        zipOutputStream.CloseEntry();
                                    }
                                }
                            }

                            zipOutputStream.Finish();
                            stream.Position = 0;
                            returnAction = File(stream.ToArray(), "application/x-zip-compressed",
                                string.Format(
                                    "Project{0}Letters_{1}.zip",
                                    projectNumber,
                                    DateTime.UtcNow.ToString("yyyyMMddHHmmss")
                                )
                            );
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                if (zipOutputStream != null)
                {
                    zipOutputStream.Close();
                }

                if (stream != null)
                {
                    stream.Close();
                }
            }

			return returnAction;
        }

        [NonAction]
        private LetterReadingRoomListViewModel LoadReadingRoomListModel(GridCommand command = null)
        {
            var searchResultsPerPage = SEARCH_RESULTS_PER_PAGE;
            var model = new LetterReadingRoomListViewModel
            {
                Mode = PageMode.Public,
                SearchResultsPerPage = searchResultsPerPage
            };

            // get the PALS project ID from the request param
            model.ProjectNumber = GetParam(Constants.PROJECT);

            if (!string.IsNullOrEmpty(model.ProjectNumber))
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    var project = projectManager.GetByProjectNumber(model.ProjectNumber);
                    if (project != null) model.IsPalsProject = project.IsPalsProject;
                }

                if (model.Project != null)
                {
                    // parse the search results per page from url querystring
                    if (int.TryParse(GetParam("SearchResultsPerPage"), out searchResultsPerPage))
                    {
                        model.SearchResultsPerPage = searchResultsPerPage;
                    }
                    else
                    {
                        model.SearchResultsPerPage = SEARCH_RESULTS_PER_PAGE;
                    }

                    // get the filter
                    var filter = Utilities.GetFilter(GetSessionKey(model.Project.ProjectId), string.Empty) as LetterReadingRoomSearchResultFilter;
                    if (filter == null)
                    {
                        filter = new LetterReadingRoomSearchResultFilter
                        {
                            ProjectId = model.Project.ProjectId,
                            LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name == "Unique or Forms").FirstOrDefault().LetterTypeId,
                            ShowForms = true,
                            SortKey = "DateSubmitted DESC",
                            NumRows = model.SearchResultsPerPage,
                            PublishToReadingRoom = true
                        };
                    }

                    // set the filter
                    filter.NumRows = model.SearchResultsPerPage;
                    Utilities.SetFilter(GetSessionKey(model.Project.ProjectId), string.Empty, filter);

                    // set the data
                    int total;
                    int pageNum;
                    model.Letters = ReadingRoomService.GetPublicReadingRoomLetters(
                        GetSessionKey(model.Project.ProjectId), 
                        IsEmptyCommand(command) ?
                            new GridCommand { 
                                Page = 1, 
                                PageSize = model.SearchResultsPerPage } 
                            : command, 
                            string.Empty,
                            out total, 
                            out pageNum);

                    // load the filter values to the model
                    model.LoadFilterToModel(filter);

                    // set the total counts to the view data
                    ViewData["total"] = total;
                    ViewData["pageNum"] = pageNum;
                }
            }
            // return the model
            return model;
        }

        [NonAction]
        private bool IsEmptyCommand(GridCommand command)
        {
            return (command == null || command.PageSize == 0);
        }

		[NonAction]
		private string GetSessionKey(int projectId)
		{
			return string.Format("{0}_{1}", Constants.READING_ROOM, projectId);
		}
        #endregion

        #region Translation Tools


        private string GetHeaderValue()
        {
            string retVal = string.Empty;
            string secret = ConfigurationManager.AppSettings["TranslatorServiceClientSecret"];
            string url = ConfigurationManager.AppSettings["TranslatorTokenUrl"];
            //string subscriptionKey = ConfigurationManager.AppSettings["TranslatorSubscriptionKey"];
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage())
                {

                    request.Method = HttpMethod.Post;

                    request.RequestUri = new Uri(url);

                    request.Content = new StringContent(string.Empty);

                    request.Headers.TryAddWithoutValidation("Ocp-Apim-Subscription-Key", secret);

                    var response = client.SendAsync(request);

                    //response.RunSynchronously();
                    response.Result.EnsureSuccessStatusCode();

                    var token = response.Result.Content.ReadAsStringAsync();
                    //token.RunSynchronously();

                    retVal = "Bearer " + token.Result;

                }
            }

            return retVal;
        }
        #endregion
        private string[] TranslateTextFromTo(string[] textToConvert, string from, string to)
        {
            int textIndices = textToConvert.Length;
            string[] translated = new string[textIndices];
            string secret = ConfigurationManager.AppSettings["TranslatorServiceClientSecret"];
            string host = ConfigurationManager.AppSettings["TranslatorBaseURL"];
            string route = ConfigurationManager.AppSettings["TranslatorTranslateBaseRoute"] + "&from=" + from + "&to=" + to;

            // putting text from string array into translator-json-friendly sytem object array
            System.Object[] body = new System.Object[textIndices];
            int bodyCount = 0;
            foreach (var item in textToConvert)
            {
                body[bodyCount] = new { Text = item };
                bodyCount++;
            }

            try
            {
                var requestBody = JsonConvert.SerializeObject(body);

                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage())
                {
                    request.Method = HttpMethod.Post;
                    // host is Translator Text API base url, post request for language translation sent to host + route
                    request.RequestUri = new Uri(host + route);
                    request.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");
                    request.Headers.Add("Ocp-Apim-Subscription-Key", secret);

                    var response = client.SendAsync(request).Result;
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;

                    // parsing json to put translated text back into array for return
                    JArray jArrayResponse = JArray.Parse(jsonResponse);
                    int transCount = 0;
                    foreach (var text in jArrayResponse)
                    {
                        translated[transCount] = (string)text["translations"][0]["text"];
                        transCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                translated[0] = "";
            }
            return translated;
        }
    }
}
