﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain;
using System.Web.Script.Serialization;

namespace Aquilent.Cara.WebApp.Controllers
{
    public class OrmsController : Controller
    {
        //
        // GET: /Orms/


        public JsonResult ProposedActiveDirectives()
        {
            IList<OrmsProjectModel> model;
            int? projectTypeId = 3; // 3 = ORMS

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var currentFuture = projectManager.FindProjects(projectTypeId, null, null);
                model = currentFuture.Select(x => new OrmsProjectModel
                {
                    Phase = x.CurrentPhase.Phase,
                    ProjectNumber = x.ProjectNumber,
                    ProjectName = x.Name,
                    Description = x.Description,
                    CommentStartDate = x.CurrentPhase.Phase.CommentStart,
                    CommentEndDate = x.CurrentPhase.Phase.CommentEnd,
                    IsArchived = x.CurrentPhase.Phase.Locked,
                }).ToList();

                foreach (var item in model)
                {
                    var documents = item.Phase.Documents;

                    item.CurrentlyActive = item.Phase.IsActive;

                    if (documents != null)
                    {
                        item.DocumentUrls = documents.Select(x => x.WwwLink);
                        item.Documents = documents.Select(x => new OrmsProjectModel.OrmsDocument { Name = x.Name, Url = x.WwwLink, Size = x.Size }).ToList();
                    }
                }
            }

            var proposedActiveList = model.Where(x => x.CurrentlyActive != null && x.CurrentlyActive == true);

            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "https://www.fs.fed.us/");

            return Json(proposedActiveList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProposedDirectives()
        {
            IList<OrmsProjectModel> model;
            int? projectTypeId = 3; // 3 = ORMS

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var currentFuture = projectManager.FindProjects(projectTypeId,null,null);
                model = currentFuture.Select(x => new OrmsProjectModel
                {
                    Phase = x.CurrentPhase.Phase,
                    ProjectNumber = x.ProjectNumber,
                    ProjectName = x.Name,
                    Description = x.Description,
                    CommentStartDate = x.CurrentPhase.Phase.CommentStart,
                    CommentEndDate = x.CurrentPhase.Phase.CommentEnd,
                    IsArchived = x.CurrentPhase.Phase.Locked
                }).ToList();

                foreach (var item in model)
                {
                    var documents = item.Phase.Documents;

                    if (documents != null)
                    {
                        item.DocumentUrls = documents.Select(x => x.WwwLink);
                        item.Documents = documents.Select(x => new OrmsProjectModel.OrmsDocument { Name = x.Name, Url = x.WwwLink, Size = x.Size }).ToList();
                    }
                }
            }

            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "https://www.fs.fed.us/");

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RecentlyIssuedActiveDirectives()
        {
            IList<OrmsProjectModel> model;
            int? projectTypeId = 3; // 3 = ORMS
            DateTime? earliest = new DateTime?(DateTime.UtcNow.AddYears(-1));

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var currentFuture = projectManager.FindProjects(projectTypeId, earliest, null);
                model = currentFuture.Select(x => new OrmsProjectModel
                {
                    Phase = x.CurrentPhase.Phase,
                    ProjectNumber = x.ProjectNumber,
                    ProjectName = x.Name,
                    Description = x.Description,
                    CommentStartDate = x.CurrentPhase.Phase.CommentStart,
                    CommentEndDate = x.CurrentPhase.Phase.CommentEnd,
                    IsArchived = x.CurrentPhase.Phase.Locked
                }).ToList();

                foreach (var item in model)
                {
                    var documents = item.Phase.Documents;

                    item.CurrentlyActive = item.Phase.IsActive;

                    if (documents != null)
                    {
                        item.DocumentUrls = documents.Select(x => x.WwwLink);
                        item.Documents = documents.Select(x => new OrmsProjectModel.OrmsDocument { Name = x.Name, Url = x.WwwLink, Size = x.Size }).ToList();
                    }
                }
            }

            var recentlyIssuedActiveDirectivesList = model.Where(x => x.CurrentlyActive != null && x.CurrentlyActive == true);

            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "https://www.fs.fed.us/");

            return Json(recentlyIssuedActiveDirectivesList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RecentlyIssuedDirectives()
        {
            IList<OrmsProjectModel> model;
            int? projectTypeId = 3; // 3 = ORMS
            DateTime? earliest = new DateTime?(DateTime.UtcNow.AddYears(-1));

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var currentFuture = projectManager.FindProjects(projectTypeId, earliest,null);
                model = currentFuture.Select(x => new OrmsProjectModel
                {
                    Phase = x.CurrentPhase.Phase,
                    ProjectNumber = x.ProjectNumber,
                    ProjectName = x.Name,
                    Description = x.Description,
                    CommentStartDate = x.CurrentPhase.Phase.CommentStart,
                    CommentEndDate = x.CurrentPhase.Phase.CommentEnd,
                    IsArchived = x.CurrentPhase.Phase.Locked
                }).ToList();

                foreach (var item in model)
                {
                    var documents = item.Phase.Documents;

                    if (documents != null)
                    {
                        item.DocumentUrls = documents.Select(x => x.WwwLink);
                        item.Documents = documents.Select(x => new OrmsProjectModel.OrmsDocument { Name = x.Name, Url = x.WwwLink, Size = x.Size }).ToList();
                    }
                }
            }

            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "https://www.fs.fed.us/");

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }

    public class OrmsProjectModel
    {
        public class OrmsDocument
        {
            public string Name { get; set; }
            public string Url { get; set; }
            public string Size { get; set; }
        }

        private const string dateFormat = "yyyy-MM-dd";

        public string ProjectNumber { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string CommentStart { get { return CommentStartDate.ToString(dateFormat); } }
        public string CommentEnd { get { return CommentEndDate.ToString(dateFormat); } }
        public bool CurrentlyActive { get; set; }
        public bool ReadingRoomActive { get; set; }
        public bool CommentInputActive { get; set; } 
        public bool IsArchived { get; set; }

        public IEnumerable<string> DocumentUrls { get; set; }

        public List<OrmsDocument> Documents { get; set; }

        [ScriptIgnore]
        public DateTime CommentStartDate { get; set; }

        [ScriptIgnore]
        public DateTime CommentEndDate { get; set; }

        [ScriptIgnore]
        public Phase Phase { get; set; }
    }
}
