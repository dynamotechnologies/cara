﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc;

using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the user specific actions
    /// </summary>
    public class UserController : PageController
    {
        #region Private Members
        private static int count = 0;
        #endregion

        #region Core Action Methods
        [HasPrivilege(PrivilegeCodes = "VAP", Scope = Constants.SYSTEM)]
        public ViewResult List()
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.USER,string.Empty) as UserSearchResultFilter;
            if (filter == null)
            {
                filter = new UserSearchResultFilter { ShowOnlyNonProjectRoles = true };
            }

            // set user ID of the filter
            filter.UserId = Utilities.GetUserSession().UserId;
            Utilities.SetFilter(Constants.USER, string.Empty, filter);

            // Create the view model
            var viewModel = new UserListViewModel
            {
                BreadCrumbTitle = string.Format("{0} {1}", Utilities.GetResourceString("AppNameAbbv"), Utilities.GetResourceString(Constants.USER).Pluralize()),
                PageTitle = string.Format("{0} {1}", Utilities.GetResourceString("AppNameAbbv"), Utilities.GetResourceString(Constants.USER).Pluralize()),
                Users = GetUsers(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage })
            };

            viewModel.LoadFilterToModel(filter);

            ViewData["total"] = count;

            return View(GetViewPath(), viewModel);
        }

        [HasPrivilege(PrivilegeCodes = "VAP", Scope = Constants.SYSTEM)]
        [HttpPost]
        public ActionResult List(string action, UserListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // set user ID
                    model.UserId = Utilities.GetUserSession().UserId;

                    // save the filter in the session
                    Utilities.SetFilter(Constants.USER, string.Empty, model.GetFilterFromModel() as UserSearchResultFilter);
                }

                ViewData["total"] = 0;

                // return to list page, list binding method will get the data
                return RedirectToAction(Constants.LIST);
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
                Utilities.SetFilter(Constants.USER, string.Empty, new UserSearchResultFilter { UserId = Utilities.GetUserSession().UserId });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST);
            }

            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult UserListBinding(GridCommand command)
        {
            var data = GetUsers(command).ToList();

            return View(GetViewPath(Constants.LIST), new GridModel { Data = data, Total = count });
        }

        private static ICollection<UserSearchResult> GetUsers(GridCommand command)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.USER, string.Empty) as UserSearchResultFilter;
            if (filter == null)
            {
                filter = new UserSearchResultFilter();
            }

            // set the paging
            filter.PageNum = command.Page;
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            int total = 0;

            // get the data
            IList<UserSearchResult> data;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                data = userManager.GetUsers(filter, out total).ToList();
            }

            // set the count
            count = total;

            // return the data as a list
            return data;
        }
        
        [HasPrivilege(PrivilegeCodes = "MUC", Scope = Constants.SYSTEM)]
        public ActionResult Create(string id, string roles)
        {
            ActionResult action = RedirectToAction(Constants.LIST_DATAMART);
            if (!string.IsNullOrEmpty(id))
            {
                try
                {
                    // get the user from data mart
                    using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                    {
                        var dmManager = new DataMartManager();
                        dmManager.UserManager = userManager;

                        // get the user (it will also create the user if it's not already in cara)
                        var user = dmManager.GetUser(id);
                        dmManager.Dispose();

                        action = RedirectToAction(Constants.ROLE.Pluralize(), new { id = user.UserId });
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    TempData[Constants.ERROR] = string.Format("An error occurred while creating the {0} in {1}.", Constants.USER, Utilities.GetResourceString("AppNameAbbv"));
                }
            }

            return action;
        }

        public ActionResult Roles(int id)
        {
            UserRolesViewModel model;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                model = GetUserRolesViewModel(id, userManager);
            }

            return View(GetViewPath(), model);
        }
  
        public RedirectToRouteResult RemoveRole(int id, int roleId, string unitId)
        {
            bool canRemove = false;
            if (roleId == 1)
            {
                canRemove = Utilities.GetUserSession().HasSystemPrivilege("MSU");
            }
            else
            {
                canRemove = Utilities.GetUserSession().HasSystemPrivilege("MUC") || Utilities.GetUserSession().HasUnitPrivilege("MUC", unitId);
            }

            if (canRemove)
            {
                using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                {
                    var model = GetUserRolesViewModel(id, userManager);

                    var roleToRemove = model.UserToManage.UserSystemRoles.FirstOrDefault(r => r.UserId == id && r.RoleId == roleId && r.UnitId == unitId);

                    if (roleToRemove != null)
                    {
                        model.UserToManage.UserSystemRoles.Remove(roleToRemove);

                        userManager.UnitOfWork.Save();
                    }
                }
            }

            return RedirectToAction("Roles", new { id = id });
        }

        [HttpPost]
        public ActionResult AddRole(int id, int? roleId, string unitId)
        {
            var userManager = ManagerFactory.CreateInstance<UserManager>();
            var model = GetUserRolesViewModel(id, userManager);

            #region Validation
            if (!roleId.HasValue)
            {
                ModelState.AddModelError("roleId", "Role is a required field");
            }
            else if (roleId == 1 && !string.IsNullOrEmpty(unitId))
            {
                ModelState.AddModelError("unitId", "Unit must be blank when the role is set to System Admin");
            }
            else if (roleId == 2 && string.IsNullOrEmpty(unitId))
            {
                ModelState.AddModelError("unitId", "Unit is required when the role is set to Planning Contributor");
            }
            #endregion Validation

            if (ModelState.IsValid)
            {
                bool canAdd = false;
                if (roleId == 1)
                {
                    canAdd = Utilities.GetUserSession().HasSystemPrivilege("MSU");
                    unitId = "0000";
                }
                else
                {
                    canAdd = Utilities.GetUserSession().HasSystemPrivilege("MUC") || Utilities.GetUserSession().HasUnitPrivilege("MUC", unitId);
                }

                if (canAdd)
                {
                    var roleToAdd = new UserSystemRole { UserId = id, RoleId = roleId.Value, UnitId = unitId };

                    if (roleToAdd != null)
                    {
                        model.UserToManage.UserSystemRoles.Add(roleToAdd);

                        userManager.UnitOfWork.Save();
                        userManager.Dispose();
                    }

                    return RedirectToAction("Roles", new { id = id });
                }
            }
            #region Handle Validation Errors
            else
            {
                model.RoleId = roleId.HasValue ? roleId.Value : 0;
                model.UnitId = unitId;
                model.UnitName = unitId != string.Empty ? LookupManager.GetLookup<LookupUnit>()[unitId].Name : string.Empty;
            }

            userManager.Dispose();
            return View(GetViewPath("Roles"), model);
            #endregion Handle Validation Errors
        }
        #endregion

        #region DataMart Actions
        [HasPrivilege(PrivilegeCodes = "VAP", Scope = Constants.SYSTEM)]
        public ViewResult ListDataMart()
        {
            return View(GetViewPath(), new DataMartUserViewModel
            {
                BreadCrumbTitle = string.Format("{0} {1} from {2}", Constants.ADD, Constants.USER, Utilities.GetResourceString("PALS")),
                PageTitle = string.Format("{0} {1} from {2}", Constants.ADD, Constants.USER, Utilities.GetResourceString("PALS"))
            });
        }

        [HasPrivilege(PrivilegeCodes = "VAP", Scope = Constants.SYSTEM)]
        [HttpPost]
        public ActionResult ListDataMart(string action, DataMartUserViewModel model)
        {
            // validation
            model.Validate(ModelState);

            if (ModelState.IsValid)
            {
                // get the filter
                DataMartUserFilter filter = model.GetFilterFromModel() as DataMartUserFilter;

                // update paging info
                filter.StartRow = 0;
                filter.NumRows = Utilities.GetUserSession().SearchResultsPerPage;

                // save the filter in the session
                Utilities.SetFilter(Constants.DATAMART_USER, string.Empty, filter);

                try
                {
                    using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                    {
                        var dmManager = new DataMartManager();
                        dmManager.UserManager = userManager;

                        // search users
                        var result = dmManager.FindUsers(filter);

                        ViewData["total"] = 0;
                        if (result != null)
                        {
                            model.Users = result.Users.ToList();

                            // set total count
                            ViewData["total"] = result.ResultMetaData.TotalCount;
                        }

                        dmManager.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    ModelState.AddModelError(string.Empty, string.Format("An error occurred while retrieving the data from {0}.", Utilities.GetResourceString("PALS")));
                }
            }

            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult DataMartUserListBinding(GridCommand command)
        {
            var data = GetDataMartUsers(command).ToList();

            return View(Constants.LIST_DATAMART, new GridModel { Data = data, Total = count });
        }

        private static ICollection<User> GetDataMartUsers(GridCommand command)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.DATAMART_USER, string.Empty) as DataMartUserFilter;
            if (filter == null)
            {
                filter = new DataMartUserFilter();
            }

            // set the paging
            filter.StartRow = (command.Page - 1) * command.PageSize;
            filter.NumRows = command.PageSize;

            // get the data
            IList<User> data;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.UserManager = userManager;

                var result = dmManager.FindUsers(filter);
                data = result.Users.ToList();

                // set the count
                count = result.ResultMetaData.TotalCount;

                dmManager.Dispose();
            }

            // return the data as a list
            return data;
        }
        #endregion

        #region Private Methods
        [NonAction]
        private UserRolesViewModel GetUserRolesViewModel(int id, UserManager userManager)
        {
            UserRolesViewModel model;
            model = userManager.All
                               .Where(x => x.UserId == id)
                               .Select(x => new UserRolesViewModel
                                      {
                                          UserToManage = x,
                                          Roles = x.UserSystemRoles.Where(r => r.RoleId == 1 || r.RoleId == 2) // System Admins and Planning Contributors
                                      })
                               .First();

            model.BreadCrumbTitle = "Manage Roles";
            model.PageTitle = string.Format("Manage Roles for {0} ({1})", model.UserToManage.FullNameLastFirst, model.UserToManage.Username);
            model.CurrentUser = Utilities.GetUserSession();

            return model;
        }

        /// <summary>
        /// Replace the role names with role ids in the input string
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        [NonAction]
        private string ReplaceRolesWithIds(string roles)
        {
            var roleIds = roles;

            if (!string.IsNullOrEmpty(roles))
            {
                // split the role name string into an array
                var list = roles.Split(',');

                foreach (string roleName in list)
                {
                    // replace role name with role id from lookup
                    roleIds = roleIds.Replace(roleName, LookupManager.GetLookup<LookupRoleByName>()[roleName].RoleId.ToString());
                }
            }

            return roleIds;
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.ADMIN;
        }
        #endregion
    }
}
