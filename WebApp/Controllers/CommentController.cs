﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the comment specific actions
    /// </summary>
    public class CommentController : PageController
    {
        #region Private Members
        #endregion

        #region Core Actions
        public ViewResult List(int projectId, int phaseId)
        {
            int pageNum = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["List-page"]))
            {
                if (!int.TryParse(Request.QueryString["List-page"], out pageNum))
                {
                    pageNum = 1;
                }
            }

            // get the filter
            var filter = Utilities.GetFilter(Constants.COMMENT, phaseId.ToString()) as CommentSearchResultFilter;
            if (filter == null)
            {
                filter = new CommentSearchResultFilter();
            }

            // Set the page number in the filter
            filter.PageNum = pageNum;

            // translate the filter type from request param
            filter.SetFilterByType(GetParam(Constants.FILTER));

            // set phase ID of the filter
            filter.PhaseId = phaseId;
            Utilities.SetFilter(Constants.COMMENT, phaseId.ToString(), filter);

            // Create the view model
			int total;
            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            var viewModel =new CommentListViewModel();



            if (String.IsNullOrWhiteSpace(filter.LetterSequence) || Regex.IsMatch(filter.LetterSequence, "^\\s*\\d\\d*\\s*(-\\s*\\d\\d*\\s*)?$"))
            {
                //initialize the grid command with sorting if appropriate
                GridCommand gridcommand = new GridCommand 
                {
                    PageSize = Utilities.GetUserSession().SearchResultsPerPage,
                    Page = filter.PageNum.Value 
                };
                if (Request.Params["List-orderBy"] != null)
                {
                    string sortString = Request.Params["List-orderBy"].ToString();
                    if (sortString.Split('-').Count() > 1)
                    {
                        SortDescriptor sortDescriptor = new SortDescriptor()
                            {
                                Member = sortString.Split('-')[0],
                                SortDirection =
                                    sortString.Split('-')[1].ToLower() == "asc" ?
                                        ListSortDirection.Ascending :
                                        ListSortDirection.Descending
                            };
                        gridcommand.SortDescriptors.Add(sortDescriptor);
                        filter.SortKey = sortDescriptor.Member +
                            (sortDescriptor.SortDirection == ListSortDirection.Ascending ? " ASC" : " DESC");
                        Utilities.SetFilter(Constants.COMMENT, phaseId.ToString(), filter);
                    }
                }
                viewModel = new CommentListViewModel
                {
                    BreadCrumbTitle = Utilities.GetPageTitle(Constants.COMMENT + Constants.LIST),
                    PageTitle = Utilities.GetPageTitle(Constants.COMMENT + Constants.LIST),
                    ProjectId = projectId,
                    PhaseId = phaseId,
                    CommentDetails = GetCommentDetails(gridcommand, phaseId.ToString(), out total, out pageNum),
                    CanEdit = isActive
                };

                #region Add persistent paging values (if any) to the model
                if (!String.IsNullOrEmpty(Request.QueryString["CommentIdList"]))
                {
                    viewModel.CommentIdList = Request.QueryString["CommentIdList"];
                }
                if (!String.IsNullOrEmpty(Request.QueryString["TaskAssignmentContext"]))
                {
                    viewModel.TaskAssignmentContext = Request.QueryString["TaskAssignmentContext"];
                }
                if (!String.IsNullOrEmpty(Request.QueryString["TaskAssignmentUsers"]))
                {
                    viewModel.TaskAssignmentUsers = Request.QueryString["TaskAssignmentUsers"];
                }
                #endregion //Add persistent values (if any) to the model


                viewModel.LoadFilterToModel(filter);

                ViewData["total"] = total;
                ViewData["pageNum"] = pageNum;

                return View(GetViewPath(), viewModel);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Please enter a valid Comment ID");
                return View(GetViewPath(), viewModel);
            }
        }

        [HttpPost]
        public ActionResult List(int projectId, int phaseId, string action, CommentListViewModel model)
        {
            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (string.Format("{0} {1}", Constants.CREATE, Utilities.GetResourceString("Concern")).Equals(model.ActionString) ||
                string.Format("{0} {1}", "Add To", Utilities.GetResourceString("Concern")).Equals(model.ActionString))
            {
                // get a list of checked comment ids
                int[] ids = Utilities.ToIntArray(model.CommentIdList);

                // validate
                if (ids != null && ids.Count() > 0)
                {
                    // get the concern response id from request param
                    var concernResponseId = Utilities.ToEntityId(GetParam(Constants.CONCERN_RESPONSE_ID));

                    // create a concern response if id is not found
                    if (concernResponseId <= 0)
                    {
                        Phase phase = null;
                        string ConcernText = "";
                        string ResponseText = "";
                        using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            phase = phaseManager.Get(phaseId);
                            if (!String.IsNullOrWhiteSpace(phase.ConcernTemplate))
                            {
                                ConcernText = phase.ConcernTemplate;
                            }
                            if (!String.IsNullOrWhiteSpace(phase.ResponseTemplate))
                            {
                                ResponseText = phase.ResponseTemplate;
                            }
                        }
                        var concern = new ConcernResponse
                        {
                            ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList.Where(x => x.Value == "Concern In Progress").Select(y => y.Key).First(),
                            ConcernCreated = DateTime.UtcNow,
                            ConcernCreatedBy = Utilities.GetUserSession().Username,
                            LastUpdated = DateTime.UtcNow,
                            LastUpdatedBy = Utilities.GetUserSession().Username,
                            UserId = Utilities.GetUserSession().UserId,
                            DateAssigned = DateTime.UtcNow,
                            ConcernText = ConcernText,
                            ResponseText = ResponseText
                        };

                        // create a new concern with selected comments
                        using (var concernManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
                        {
                            concernManager.Add(concern);
                            concernManager.UnitOfWork.Save();
                        }

                        concernResponseId = concern.ConcernResponseId;
                    }

                    // associate the comments
                    if (concernResponseId > 0)
                    {
                        using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                        {
                            var comments = commentManager.All.Where(x => ids.Contains(x.CommentId)).ToList();

                            foreach (var comment in comments)
                            {
                                // set concern response id
                                comment.ConcernResponseId = concernResponseId;

                                //update lastupdated column
                                comment.ConcernResponse.LastUpdated = DateTime.UtcNow;
                                comment.ConcernResponse.LastUpdatedBy = Utilities.GetUserSession().Username;

                                if (comment.UserId != null)
                                {
                                    comment.UserId = null;
                                    comment.DateAssigned = DateTime.UtcNow;                                    
                                }
                            }

                            commentManager.UnitOfWork.Save();
                        }

                        // redirect to the concern response input page
                        //CARA-988 add trackback functionality
                        return RedirectToAction(Constants.INPUT, Constants.CONCERN, new { projectId = projectId, phaseId = phaseId, id = concernResponseId, trackbackpage=Constants.COMMENT+Constants.LIST });
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Please select a comment.");
                }
            }
            else if (model.IsSubmit(action))
            {
                if (model != null)
                {
                    // save the filter in the session
                    Utilities.SetFilter(Constants.COMMENT, phaseId.ToString(), model.GetFilterFromModel() as CommentSearchResultFilter);
                }

                ViewData["total"] = 0;
                ViewData["pageNum"] = 1;

                // pass the view model, list binding method will get the data
                return RedirectToAction(Constants.LIST, new { mode = GetParam(Constants.MODE), ConcernResponseId = GetParam(Constants.CONCERN_RESPONSE_ID) });
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
                Utilities.SetFilter(Constants.COMMENT, phaseId.ToString(), new CommentSearchResultFilter { PageNum = 1 });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST, new { mode = GetParam(Constants.MODE), ConcernResponseId = GetParam(Constants.CONCERN_RESPONSE_ID) });
            }
            else if (action.Equals("Save Task Assignments"))
            {
                string contextIds = Request.Params["TaskAssignmentContext"].ToString();
                string userIds = Request.Params["TaskAssignmentUsers"].ToString();
                bool success = SaveTaskAssignments(contextIds, userIds);
                model.TaskAssignmentContext = "";
                model.TaskAssignmentUsers = "";
                TempData[Constants.PHASE + Constants.COMMENT] = "Task Assignments Saved";
            }

            // get the filter settings
            int total = 0;
            int pageNum = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["List-page"]))
            {
                if (!int.TryParse(Request.QueryString["List-page"], out pageNum))
                {
                    pageNum = 1;
                }
            }

            if (model != null)
            {
                // set the phaseId
                model.PhaseId = phaseId;
                var filter = model.GetFilterFromModel() as CommentSearchResultFilter;
                filter.PageNum = pageNum;

                // save the filter in the session
                Utilities.SetFilter(Constants.COMMENT, phaseId.ToString(), filter);

                // get a list of comments
                model.CommentDetails = GetCommentDetails(
                    new GridCommand { 
                        PageSize = Utilities.GetUserSession().SearchResultsPerPage },
                    phaseId.ToString(),
                    out total, 
                    out pageNum);
            }

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            // pass the empty view model, list binding method will get the data
            return View(GetViewPath(), model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult CommentListBinding(GridCommand command)
        {
			int total;
            int pageNum;
            string context = Request.RequestContext.RouteData.Values["phaseId"].ToString();
			var data = GetCommentDetails(command, context, out total, out pageNum).ToList();

			ViewData["pageNum"] = pageNum;
            
            return View(GetViewPath(Constants.LIST), new GridModel { Data = data, Total = total });
        }
        
        public ViewResult Details(int projectId, int phaseId, int id)
        {
            Comment comment;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                comment = commentManager.Get(id);
            }

            // get the comment details
            return View(GetViewPath(), new CommentDetailsViewModel
            {
                BreadCrumbTitle = string.Format("{0} {1}", Constants.COMMENT, Constants.DETAILS),
                PageTitle = string.Format("{0} {1}", Constants.COMMENT, Constants.DETAILS),
                Comment = comment
            });
        }

        [HasPrivilege(PrivilegeCodes = "CODE", Scope = Constants.PHASE)]
        public ActionResult Delete(int projectId, int phaseId, int id)
        {
            // delete the comment
            var result = LetterUtilities.DeleteComment(id);
            
            // go back to listing page
            return RedirectToAction(Constants.LIST);
        }

        private static ICollection<CommentDetails> GetCommentDetails(GridCommand command, string context, out int total, out int pageNum)
        {
            // get the filter
            var filter = Utilities.GetFilter(Constants.COMMENT, context) as CommentSearchResultFilter;
            if (filter == null)
            {
                filter = new CommentSearchResultFilter();
            }

            // set the paging
            //filter.PageNum = command.Page;
            filter.NumRows = command.PageSize;
          
            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
               
            }

            total = 0;

            // get the data
            IList<CommentSearchResult> commentIds;
            IList<CommentDetails> data;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                commentIds = commentManager.GetComments(filter, out total).ToList();
                
                XDocument xdoc = new XDocument(new XElement("commentsList"));
                
                IEnumerable<XElement> elements = (from comment in commentIds
                                                      select new XElement("comment",
                                                          new XAttribute("rowNumber", comment.RowNumber),
                                                          new XAttribute("commentId", comment.CommentId)));

                foreach (XElement element in elements)
                {
                    xdoc.Root.Add(element);
                }
          
                data = commentManager.CommentDetails(xdoc).ToList();
            }

            //code to write contents to new compact list of type data
            int tempCommentId = 0;
            var compactList = new List<CommentDetails>();
            CommentDetails gcd = new CommentDetails();

            foreach (var entry in data)
            {
                if (tempCommentId != entry.CommentId)
                {
                    //copy entry into the new list
                    gcd = new CommentDetails();
                    tempCommentId = entry.CommentId;
                    compactList.Add(gcd);
                }

                //Format CommentCodeNumber
                string commentCodeNumber = Code.FormatCodeNumber(entry.CommentCodeNumber);

                if (String.IsNullOrEmpty(gcd.CommentCodeText))
                {
                    gcd.CommentCodeText = commentCodeNumber + " " + entry.CommentCodeText;
                }
                else
                {
                    gcd.CommentCodeText = gcd.CommentCodeText + "<br>" + commentCodeNumber + " " + entry.CommentCodeText;
                }

                gcd.CommentCodeList = entry.CommentCodeList;
                gcd.CommentCodeNumber = entry.CommentCodeNumber;
                gcd.CommentId = entry.CommentId;
                gcd.CommentNumber = entry.CommentNumber;
                gcd.CommentText = entry.CommentText;
                gcd.ConcernResponseId = entry.ConcernResponseId;
                gcd.ConcernResponseNumber = entry.ConcernResponseNumber;
                gcd.LetterId = entry.LetterId;
                gcd.LetterSequence = entry.LetterSequence;
                gcd.PhaseId = entry.PhaseId;
                gcd.SampleStatement = entry.SampleStatement;
                gcd.ConcernResponseCodeName = entry.ConcernResponseCodeName;
                gcd.ConcernResponseCodeNumber = entry.ConcernResponseCodeNumber;
                gcd.UserId = entry.UserId;
            }
          
            //code ends here   

			pageNum = command.Page;
			filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.COMMENT, context, filter);

            // return the data as a list
            return compactList;
        }
     
        [HasPrivilege(PrivilegeCodes = "WRSP", Scope = Constants.PHASE)]
        public bool UpdateSampleStatement(int projectId, int phaseId, int id)
        {
            bool sampleStatement = Boolean.Parse(Request.Params["sampleStatement"]);
            bool result = false;

            if (id > 0)
            {
                using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                {
                    // get the comment by ud
                    var comment = commentManager.Get(id);

                    if (comment != null)
                    {
                        // set fields to be updated
                        comment.SampleStatement = sampleStatement;
                        comment.LastUpdated = DateTime.UtcNow;
                        comment.LastUpdatedBy = Utilities.GetUserSession().Username;

                        //update lastupdated column
                        comment.ConcernResponse.LastUpdated = DateTime.UtcNow;
                        comment.ConcernResponse.LastUpdatedBy = Utilities.GetUserSession().Username;

                        // save the comment
                        commentManager.UnitOfWork.Save();

                        result = true;
                    }
                }
            }

            return result;
        }

        private bool SaveTaskAssignments(string commentIds, string userIds)
        {
            if (string.IsNullOrWhiteSpace(commentIds) || string.IsNullOrWhiteSpace(userIds))
            {
                return true;
            }
            string[] strArrContext = commentIds.Split('|');
            string[] strArrUserIds = userIds.Split('|');
            //make sure there are the same number of ids
            if (strArrContext.Count() != strArrUserIds.Count())
            {
                return false;
            }
            int[] intContextIds = strArrContext.Select(x=>int.Parse(x)).ToArray();
            int[] intUserIds = strArrUserIds.Select(x => int.Parse(x)).ToArray();
            using (CommentManager commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                List<Comment> comments = commentManager.All.Where(x => intContextIds.Contains(x.CommentId)).ToList();
                for (int index = 0; index < intContextIds.Count(); index++)
                {
                    Comment comment = comments.FirstOrDefault(x => x.CommentId == intContextIds[index]);
                    if (comment != null)
                    {
                        if ((intUserIds[index] > 0 && comment.UserId == null) ||
                            (comment.UserId.HasValue && comment.UserId.Value != intUserIds[index]))
                        {
                            if (intUserIds[index] == 0)
                            {
                                comment.UserId = null;
                            }
                            else
                            {
                                comment.UserId = intUserIds[index];
                            }
                            comment.DateAssigned = DateTime.UtcNow;
                        }

                    }
                }
                commentManager.UnitOfWork.Save();
            }
            return true;
        }
        #endregion

        #region Search Code Actions
        public PartialViewResult SearchCode(int projectId, int phaseId)
        {
            return PartialView("~/Views/Shared/Filter/ucCodeFilter.ascx");
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion
    }
}
