﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;

using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the home specific actions
    /// </summary>
    public class HomeController : PageController
    {
        #region Private Members
        #endregion

        #region Action Methods
        public ViewResult Index()
        {
			int total;
			int pageNum;
			var gridCommand = new GridCommand();
			var filter = Utilities.GetFilter(Constants.MY_PROJECTS,string.Empty) as ProjectSearchResultFilter;
			if (filter != null)
			{
				gridCommand.Page = filter.PageNum.Value;
			}
            //get tasks
            bool couldFetchTasks = true;
            List<TaskItem> tasks = new List<TaskItem>();
            List<TaskItem> reviewableLetters = new List<TaskItem>();
            using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                try
                {
                    tasks = phaseManager.GetTaskAssignments(Utilities.GetUserSession().UserId);
                }
                catch (Exception ex)
                {
                    couldFetchTasks = false;
                }
                reviewableLetters = phaseManager.GetReviewableLetters(Utilities.GetUserSession());
            }
            // Create home view model
            var viewModel = new HomeViewModel
            {
                PageTitle = Utilities.GetResourceString("MyProjects"),
                ProjectListViewModel = new ProjectListViewModel
                {
                    Projects = GetMyProjects(gridCommand, out total, out pageNum),
                    MyProjects = true,
                    Archived = false
                },
                TaskListViewModel = new TaskListViewModel
                {
                    TaskItems = tasks,
                    Mode = TaskListViewModel.TaskMode.CommentsConcerns,
                    CouldFetchTasks = couldFetchTasks
                },
                ReviewableLettersViewModel = new TaskListViewModel
                {
                    TaskItems = reviewableLetters,
                    Mode = TaskListViewModel.TaskMode.ReviewableLetters,
                    CouldFetchTasks = true // Always true
                },
            };

            viewModel.ProjectListViewModel.LoadFilterToModel(filter);

			ViewData["total"] = total;
			ViewData["pageNum"] = pageNum;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(HomeViewModel model, string action)
        {
            if (Constants.SEARCH.Equals(action) || Constants.INDEX.Equals(action))
            {
                return RedirectToAction(Constants.LIST, Constants.PROJECT, new { filter = "Keyword", ProjectNameOrId = GetParam("txtProjectNameOrId") });
            }

            return View(model);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ProjectListBinding(GridCommand command)
        {
			int total;
			int pageNum;
            var data = GetMyProjects(command, out total, out pageNum).ToList();

			ViewData["pageNum"] = pageNum;

            return View(Constants.INDEX, new GridModel { Data = data, Total = total });
        }

        private static ICollection<ProjectSearchResult> GetMyProjects(GridCommand command, out int total, out int pageNum)
        {
			var filter = Utilities.GetFilter(Constants.MY_PROJECTS, string.Empty) as ProjectSearchResultFilter;
			if (filter == null)
			{
				filter = new ProjectSearchResultFilter
				{
					NumRows = Constants.DEFAULT_PAGE_SIZE,
					MyProjects = true,
					Archived = false,
	                UserId = Utilities.GetUserSession().UserId
		        };
			}

			pageNum = command.Page;
			filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.MY_PROJECTS, string.Empty, filter);

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            total = 0;

            // get the data
            IList<ProjectSearchResult> data;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                data = projectManager.GetProjects(filter, out total);
            }

            // return the data as a list
            return data.ToList();
        }

        public void Logout()
        {
            // invalidate the current user
            var secToken = System.Web.HttpContext.Current.Session["SecurityToken"] as SecurityToken;

            if (secToken != null)
            {
                using (var secTokenManager = ManagerFactory.CreateInstance<SecurityTokenManager>())
                {
                    secTokenManager.UnitOfWork.Context.AttachTo("SecurityTokens", secToken);
                    secTokenManager.Invalidate(secToken);
                }
            }

			System.Web.HttpContext.Current.Session.Clear();
            System.Web.Security.FormsAuthentication.SignOut();

            Response.Redirect(ConfigurationManager.AppSettings["palsLoginUrl"] , true);
        }

        public ViewResult LoginError()
        {
            return View();
        }

        public ViewResult Error()
        {
            throw new ApplicationException();
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.HOME;
        }
        #endregion
    }
}
