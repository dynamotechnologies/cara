﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the document specific actions
    /// </summary>
    public class DocumentController : AbstractController
    {
        #region Action Methods
        public ActionResult Download(int id)
        {
            byte[] buffer;
            byte[] streamInBytes;

            using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
            {
                // get the document
                var d = docManager.Get(id);

                if (d != null && d.File != null)
                {
                    buffer = new byte[d.File.Size];

                    // create the file as a file stream
                    using (var stream = new MemoryStream(buffer, true))
                    {
                        // download the file to the stream
                        docManager.DownloadFile(d, stream);
                        stream.Position = 0;

                        // send the file data back
                        streamInBytes = stream.ToArray();
                    }

                    return File(streamInBytes, d.File.ContentType, d.File.OriginalName);
                }
            }

            return RedirectToAction(Constants.ERROR, Constants.HOME);
        }

        public ActionResult DownloadAll(string ids)
        {
            using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    // set the file name
                    var downloadFileTo = string.Format("attachments_{0}.zip", DateTime.UtcNow.ToString("yyyyMMddHHmmss"));
                    // split the string ids into int ids
                    var sIds = ids.Split(',');

                    if (sIds != null)
                    {
                        var docIds = sIds.Select(x => Convert.ToInt32(x)).ToArray();

                        // instantiate the output stream
                        byte[] streamInBytes;
                        using (var stream = new MemoryStream())
                        {
                            // download the zipped file with documents
                            docManager.ZipDocuments(docIds, stream);
                            stream.Position = 0;

                            // send the file data back
                            streamInBytes = stream.ToArray();
                        }

                        return File(streamInBytes, "application/x-zip-compressed", downloadFileTo);
                    }
                }
            }

            return RedirectToAction(Constants.ERROR, Constants.HOME);
        }
        #endregion

        #region Dmd Methods
        public ActionResult DownloadDmd(string id)
        {
            var dmdManager = new DmdManager();

            // get the file and metadata
            var metaData = dmdManager.GetFileMetadata(id);
            var fileData = dmdManager.GetFileData(id);

            if (metaData != null && fileData != null)
            {
                // send the file data back
                return File(fileData, metaData.AppDocType, metaData.Filename);
            }

            return RedirectToAction(Constants.ERROR, Constants.HOME);
        }
        #endregion
    }
}
