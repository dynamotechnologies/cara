﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the author specific actions
    /// </summary>
    public class AuthorController : PageController
    {
        #region Core Action Methods
        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public ViewResult Create(int projectId, int phaseId, int letterId, int id)
        {
            // get the letter by id
            Letter letter;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                letter = letterManager.Get(letterId);
            }
            
            var author = new Author { LetterId = letterId };

            return View(GetViewPath(), new AuthorInputViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                Author = author,
                Sender = false,
                OrganizationName = null,
                Mode = Constants.CREATE,
                ProjectId = projectId
            });
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult Create(int projectId, int phaseId, int letterId, int id, AuthorInputViewModel model)
        {
            // validate the input
            model.Validate(ModelState);

            if (ModelState.IsValid)
            {
                // save the author
                var authorId = model.SaveAuthor(ModelState);

                if (authorId > 0)
                {
                    return RedirectToAction(Constants.EDIT, Constants.LETTER, new { id = letterId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "An error occurred while creating a new author.");
                }
            }

            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public ViewResult Edit(int projectId, int phaseId, int letterId, int id)
        {
            // get the letter by id
            //Letter letter;
            //using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            //{
            //    letter = letterManager.Get(letterId);
            //}

            Author author;
            AuthorInputViewModel authorInputViewModel;
            using (var authorManager = ManagerFactory.CreateInstance<AuthorManager>())
            {
                author = authorManager.Get(id);

                //Fixed: JIRA #CARA-1259
                if (string.IsNullOrEmpty(author.ContactMethod)) author.ContactMethod = "Do not contact me in the future";

                authorInputViewModel = new AuthorInputViewModel
                {
                    BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                    PageTitle = Utilities.GetPageTitle(ControllerAction),
                    Author = author,
                    Sender = author.Sender,
                    OrganizationName = (author.Organization != null ? author.Organization.Name : null),
                    Mode = Constants.EDIT,
                    ProjectId = projectId
                };
            }

            return View(GetViewPath(), authorInputViewModel);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        [HttpPost]
        public ActionResult Edit(int projectId, int phaseId, int letterId, int id, AuthorInputViewModel model)
        {
            // validate the input
            model.Validate(ModelState);

            if (ModelState.IsValid)
            {
                // save the author
                var authorId = model.SaveAuthor(ModelState);

                if (authorId > 0)
                {
                    return RedirectToAction(Constants.EDIT, Constants.LETTER, new { id = letterId });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "An error occurred while updating the author.");
                }
            }

            return View(GetViewPath(), model);
        }

        [HasPrivilege(PrivilegeCodes = "MLTR", Scope = Constants.PHASE)]
        public ActionResult Delete(int letterId, int authorId)
        {
            // get the author
            using (var authorManager = ManagerFactory.CreateInstance<AuthorManager>())
            {
                try
                {
                    var author = authorManager.Get(authorId);
                    if (author != null)
                    {
                        // delete the author
                        authorManager.Delete(author);

                        authorManager.UnitOfWork.Save();
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    TempData[Constants.ERROR] = string.Format("An error occurred while deleting the {0}.", Constants.AUTHOR);
                    return RedirectToAction(Constants.DETAILS, new { authorId = authorId });
                }
            }

            return RedirectToAction(Constants.EDIT, Constants.LETTER, new { id = letterId });
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion
    }
}
