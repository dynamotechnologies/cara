﻿using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;

using System.Configuration;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the external authentication specific actions
    /// </summary>
    public class AuthenticationController : AbstractController
    {
        #region External Actions
        public ContentResult Token()
        {
            // take the username and password as parameters and send it to PALS for validation
            return Content(
				AuthenticationService.ValidateLoginRequest(GetParam("serviceName"), GetParam("servicePassword"), GetParam("userName")), 
				"text/xml"
				);
        }

        public ActionResult Login()
        {
            return RedirectToAction((AuthenticationService.ValidateToken(GetParam("token")) ? Constants.INDEX : "LoginError"), Constants.HOME);
        }
        #endregion

        public ActionResult LogBackIn(string serviceName, string servicePassword, string userName)
        {
            //var validationRequestToken = AuthenticationService.ValidateLoginRequest(GetParam("serviceName"), GetParam("servicePassword"), GetParam("userName"));
            var validationRequestToken = AuthenticationService.ValidateLoginRequest(serviceName, servicePassword, userName);
            string token = validationRequestToken.Substring(validationRequestToken.IndexOf('>') + 1);
            token = token.Substring(0, token.LastIndexOf('<'));
            //string urlRequest = ConfigurationManager.AppSettings["caraLoginUrl"] + token;
            //return Redirect(urlRequest);
            return RedirectToAction((AuthenticationService.ValidateToken(token) ? Constants.INDEX : "LoginError"), Constants.HOME);
        }
    }
}
