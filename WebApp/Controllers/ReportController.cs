﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.Domain.Report.Personal;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.WebApp.Models.Configuration;
using Aquilent.EntityAccess;
using Telerik.Web.Mvc;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using System.Configuration;
using Amazon.SQS;
using Amazon;
using Amazon.SQS.Model;

namespace Aquilent.Cara.WebApp.Controllers
{
    public class ReportController : PageController
    {
        #region page actions
        /// <summary>
        /// Controller method for the Index Page
        /// </summary>
        /// <returns></returns>
        public ViewResult Index()
        {
            //clear existing filter from cache so that if custom report is selected existing parameters will not persist
            new CustomReportMetaData().Clear();
            return View(new ReportViewModel2{
                BreadCrumbTitle = Utilities.GetResourceString(Constants.REPORT).Pluralize(),
                PageTitle = Utilities.GetResourceString(Constants.REPORT).Pluralize(),
            });
        }

        /// <summary>
        /// Controller Method for Standard Reports
        /// </summary>
        /// <param name="model">Model for Standard Reports page</param>
        /// <returns></returns>
        public ActionResult StandardReports(StandardReportViewModel model = null)
        {
            string action = Constants.STANDARD_REPORTS;
            if (model != null)
            {
                action = model.action;
            }
            if (Constants.STANDARD_REPORTS.Equals(action))
            {
                return View(new StandardReportViewModel
                {
                    BreadCrumbTitle = Utilities.GetResourceString(Constants.STANDARD_REPORTS),
                    PageTitle = Constants.STANDARD_REPORTS
                });
            }
            else if ("Search Projects".Equals(action))
            {
                // get the projects based on the project filter
                if (string.IsNullOrEmpty(model.ProjectNameOrNumber))
                {
                    ModelState.AddModelError(string.Empty, "Project search criteria is required.");
                }
                else if (model.ProjectNameOrNumber.Length < 4)
                {
                    ModelState.AddModelError(string.Empty, "Please enter more than four characters or numbers in the Project search criteria.");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        // get the projects based on the filter
                        using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                        {
                            model.Projects = projectManager.FindProjects(model.ProjectNameOrNumber).ToList();
                            foreach (var proj in model.Projects)
                            {
                                var phases = proj.Phases;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        ModelState.AddModelError(string.Empty, string.Format("An error occurred while searching {0} projects.", Utilities.GetResourceString("AppNameAbbv")));
                    }
                }
            }
            else if (Constants.NEXT.Equals(action))
            {

                model.ProjectId = Utilities.ToEntityId(Request.Form.Get("rbProjectId"));
                model.PhaseId = Utilities.ToEntityId(Request.Form.Get(string.Format("ddlPhase_{0}", model.ProjectId.ToString())));
                model.Validate(ModelState);
                if (ModelState.IsValid)
                {
                    return RedirectToAction(Constants.LIST, Constants.PHASE_REPORT, new { projectId = model.ProjectId, phaseId = model.PhaseId });
                }
            }
            else if (Constants.CANCEL.Equals(action))
            {
                return RedirectToAction(Constants.INDEX);
            }
            return View(model);
        }

        /// <summary>
        /// Controller method for the Custom Report Configuration Page
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult CustomReportConfiguration(CustomReportControlModel model)
        {
            //create a view model from the control model (see CustomReportViewModel for details)
            CustomReportViewModel viewModel = new CustomReportViewModel(model);
            return View("~/views/Report/CustomReportConfiguration.aspx", viewModel);
        }

        /// <summary>
        /// Controller method for the Wizard Field Entry page (steps 1 - 3 of custom report wizard)
        /// </summary>
        /// <param name="model">Model for wizard field entry page</param>
        /// <returns></returns>
        public ActionResult WizardFieldEntry(CustomReportControlModel model)
        {
            //create a view model from the control model (see CustomReportViewModel for details)
            CustomReportViewModel viewModel = new CustomReportViewModel(model);
            return View("~/views/Report/WizardFieldEntry.aspx", viewModel);
        }

        /// <summary>
        /// Control method for Column Selection Page (step 4 of custom report wizard)
        /// </summary>
        /// <param name="model">Model for Column Selection Page</param>
        /// <returns></returns>
        public ActionResult ColumnSelection(CustomReportControlModel model)
        {
            //create a view model from the control model (see CustomReportViewModel for details)            
            CustomReportViewModel viewModel = new CustomReportViewModel(model);
            return View("~/views/Report/ColumnSelection.aspx", viewModel);
        }

        /// <summary>
        /// Control method for Sort Order page (step 5 of custom report wizard)
        /// </summary>
        /// <param name="model">Model for Sort Order page</param>
        /// <returns></returns>
        public ActionResult SortOrder(CustomReportControlModel model)
        {
            //create a view model from the control model (see CustomReportViewModel for details)     
            CustomReportViewModel viewModel = new CustomReportViewModel(model);

            //set the default sort order so that default values will populate the pages dropdowns
            viewModel.MetaData.SetDefaultSortOrder();

            return View("~/views/Report/SortOrder.aspx", viewModel);
        }

        /// <summary>
        /// Control method for Preview page (final page of Custom Report wizard)
        /// </summary>
        /// <param name="model">Model for preview page</param>
        /// <returns></returns>
        public ActionResult Preview(CustomReportControlModel model)
        {
            int total;
            int pageNum;

            //create a view model from the control model (see CustomReportViewModel for details)
            CustomReportViewModel viewModel = new CustomReportViewModel(model);

            //get and execute the filter
            CustomReportFilter2 filter = (CustomReportFilter2)Utilities.GetFilter(Constants.CUSTOM_REPORT, string.Empty);
            if (filter.PageNum == null)
            { filter.PageNum = 1; }
            viewModel.ReportData = GetCustomReportData(new GridCommand
            {
                PageSize = Utilities.GetUserSession().SearchResultsPerPage,
                Page = filter.PageNum.Value
            }, out total, out pageNum);

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View("~/views/Report/Preview.aspx", viewModel);
        }

        #region My Report Methods
        /// <summary>
        /// Control Method for Saved Reports Page
        /// </summary>
        /// <param name="model">Model for Saved Reports page</param>
        /// <returns></returns>
        public ActionResult MyReports(MyReportModel model = null)
        {
            model = new MyReportModel
            {
                BreadCrumbTitle = Utilities.GetResourceString("MyReports"),
                PageTitle = Utilities.GetResourceString("MyReports")
            };

            return View(model);
        }

        /// <summary>
        /// Control Method to execute report from Saved Reports page
        /// </summary>
        /// <param name="model">Model for Saved Reports page</param>
        /// <returns></returns>
        public ActionResult RunMyReport(MyReportModel model)
        {
            //Load the report parameters in to the filter
            SetMyReportFilter(model.myReportId);

            return RedirectToAction("Preview", "Report", new CustomReportControlModel
            {
                BreadCrumbTitle = "Custom Reports",
                PageTitle = "Preview",
                ReportId = model.myReportId,
                Dirty = false
            });
        }

        /// <summary>
        /// Control Method for Modify Search button on Saved reports page
        /// </summary>
        /// <param name="model">Model for Saved Reports Page</param>
        /// <returns></returns>
        public ActionResult UpdateMyReport(MyReportModel model)
        {
            //Load the report parameters in to the filter
            SetMyReportFilter(model.myReportId);

            return RedirectToAction("TrafficDirector", new CustomReportControlModel
            {
                BreadCrumbTitle = "Custom Report",
                WizardStep = 0,
                ReportId = model.myReportId,
                Dirty = true
            });
        }

        /// <summary>
        /// Control method for the dialog that saves the custom report parameters on the preview page
        /// </summary>
        /// <param name="reportId">The id of the report to be modified or 0 if saving as a new report</param>
        /// <returns></returns>
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.SYSTEM)]
        public PartialViewResult MyReportInput(int reportId)
        {
            return PartialView("~/Views/Shared/Input/ucMyReportInput.ascx", new MyReportModel
            {
                Private = true,
                myReportId = reportId,
                //Set the name of the report to an empty string if saving as new
                Name = reportId>0?
                    ManagerFactory.CreateInstance<MyReportManager>().Find(x=>x.MyReportId==reportId).FirstOrDefault().Name
                    :""
            });
        }

        /// <summary>
        /// Control action to delete a report from the MyReportList page
        /// </summary>
        /// <param name="myReportId">The Id of the report to be deleted</param>
        /// <param name="viewOption">The filter option for the MyReportList</param>
        /// <returns></returns>
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.SYSTEM)]
        [AjaxErrorHandler]
        public ActionResult DeleteMyReport(int myReportId, string viewOption)
        {
            using (var reportManager = ManagerFactory.CreateInstance<MyReportManager>())
            {
                // get the report to delete
                var myReport = reportManager.Get(myReportId);
                var userId = Utilities.GetUserSession().UserId;

                // delete my report
                if (myReport != null)
                {
                    reportManager.Delete(myReport);
                    reportManager.UnitOfWork.Save();
                }
            }

            // refresh the my report list
            return RedirectToAction("GetMyReportList", new { viewOption = viewOption });
        }

        /// <summary>
        /// Control method to get the list of reports for the MyReportList page
        /// </summary>
        /// <param name="viewOption">The view option for the list</param>
        /// <returns></returns>
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.SYSTEM)]
        [AjaxErrorHandler]
        public PartialViewResult GetMyReportList(string viewOption)
        {
            IList<MyReport> data;
            using (var reportManager = ManagerFactory.CreateInstance<MyReportManager>())
            {
                var userId = Utilities.GetUserSession().UserId;

                var dataQ = reportManager.All.Where(x => x.UserId == userId || (x.Private.HasValue && !x.Private.Value));

                if ("MyReport".Equals(viewOption))
                {
                    dataQ = dataQ.Where(x => x.UserId == userId);
                }
                else if ("PublicReport".Equals(viewOption))
                {
                    dataQ = dataQ.Where(x => x.Private.HasValue && !x.Private.Value);
                }

                data = dataQ.ToList();
            }

            // refresh the my report list
            return PartialView("~/Views/Shared/List/ucMyReportList.ascx", data);
        }

        #endregion

        #region grid actions
        [GridAction(EnableCustomBinding = true)]
        public ActionResult PreviewBinding(GridCommand command, CustomReportControlModel model)
        {
            int total;
            int pageNum;
            CustomReportViewModel viewModel = new CustomReportViewModel(model);
            viewModel.ReportData = GetCustomReportData(command, out total, out pageNum);


            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View(new GridModel {Data = viewModel.ReportData, Total = total });
        }

        private static List<CustomReportData> GetCustomReportData(GridCommand command, out int total, out int pageNum)
        {
            total = 0;
            int PageNum = 1;
            int NumRows = 0;

            PageNum = (command.Page > 0 ? command.Page : 1);
            NumRows = command.PageSize;

            IList<CustomReportData> data;
            CustomReportMetaData metaData = new CustomReportMetaData();
            data = metaData.GetReportResults(PageNum, NumRows, out total);


            pageNum = command.Page;

            return data.ToList();
        }
        #endregion

        /// <summary>
        /// Calls the appropriate controller method for the Custom Report Wizard
        /// </summary>
        /// <param name="model">The Model for the Custom Report Wizard</param>
        /// <returns></returns>
        public ActionResult TrafficDirector(CustomReportControlModel model)
        {
            CustomReportMetaData metaData = null;
            model.BreadCrumbTitle = Utilities.GetResourceString(Constants.CUSTOM_REPORT.Pluralize());
            model.PageTitle = Utilities.GetResourceString(Constants.CUSTOM_REPORT.Pluralize());

            //If the cancel button was clicked redirect to the Report index page
            if (Constants.CANCEL.Equals(model.Action))
            { return RedirectToAction(Constants.INDEX, "Report"); }

            //If the run report button (on the sort order page) was clicked then go to the preview page
            if (model.Action == "Run Report")
            {
                metaData = new CustomReportMetaData();
                if (!metaData.parameters.DataItems.Any())
                {
                    ModelState.AddModelError(string.Empty, "At least one search parameter is required");
                }
                if (!metaData.parameters.ColumnDataItems.Any())
                {
                    ModelState.AddModelError(string.Empty, "At least one Column is required");
                }
                if (!ModelState.IsValid)
                {
                    return CustomReportConfiguration(model);
                }
                List<KeyValuePair<int, bool>> sortOrder = new List<KeyValuePair<int,bool>>();
                if (model.SortColumn1 > 0)
                { sortOrder.Add(new KeyValuePair<int, bool>(model.SortColumn1, model.Asc1.GetValueOrDefault(true))); }
                if (model.SortColumn2 > 0 && model.SortColumn2 != model.SortColumn1)
                { sortOrder.Add(new KeyValuePair<int, bool>(model.SortColumn2, model.Asc2.GetValueOrDefault(true))); }
                if (model.SortColumn3 > 0 && model.SortColumn3 != model.SortColumn1 && model.SortColumn3 != model.SortColumn2)
                { sortOrder.Add(new KeyValuePair<int, bool>(model.SortColumn3, model.Asc3.GetValueOrDefault(true))); }
                if (!sortOrder.Any())
                { sortOrder.Add(new KeyValuePair<int, bool>(metaData.parameters.ColumnDataItems.FirstOrDefault().ListId, true)); }
                metaData.SetSortOrder(sortOrder);
                return RedirectToAction("Preview", "Report", new
                {
                    WizardStep = model.WizardStep,
                    BreadCrumbTitle = "Preview",
                    PageTitle = "Preview",
                    ReportId = model.ReportId,
                    Dirty = model.Dirty
                });
            }

            if (model.Action == "Add")
            {
                metaData = new CustomReportMetaData();
                CustomReportFieldElement field = CustomReportFieldManager.GetById(model.FieldId);
                bool success = false;
                switch (field.DataType)
                {
                    case CustomReportFieldManager.FieldDataTypes.Text:
                    case CustomReportFieldManager.FieldDataTypes.Number:
                        success = metaData.Add(field, !model.FieldValueInclude, model.FieldValue);
                        break;
                    case CustomReportFieldManager.FieldDataTypes.Dropdown:
                    case CustomReportFieldManager.FieldDataTypes.Unit:
                        success = metaData.Add(field, !model.FieldValueInclude, model.FieldValue, model.FieldValueName);
                        break;
                    case CustomReportFieldManager.FieldDataTypes.Date:
                        DateTime from = model.FieldDateFrom.GetValueOrDefault(DateTime.MinValue);
                        DateTime to = model.FieldDateTo.GetValueOrDefault(DateTime.MaxValue);
                        success = metaData.Add(field, !model.FieldValueInclude, to, from);
                        break;
                }
                if (!success)
                {
                    ModelState.AddModelError(string.Empty, String.Format("The invalid value for field {1}", field.Name));
                }
            }
            else if (model.Action == "Remove")
            {
                metaData = new CustomReportMetaData();
                metaData.Remove(model.ListId);
            }
            else if (model.Action == "AddColumn")
            {
                metaData = new CustomReportMetaData();
                metaData.AddColumn(CustomReportFieldManager.GetColumnById(Int32.Parse(model.FieldValue)));
            }
            else if (model.Action == "MoveUp")
            {
                metaData = new CustomReportMetaData();
                metaData.MoveColumns(new List<int>(new[] { model.ListId }), true);
            }
            else if (model.Action == "MoveDown")
            {
                metaData = new CustomReportMetaData();
                metaData.MoveColumns(new List<int>(new[] { model.ListId }), false);
            }
            else if (model.Action == "ClearAllValues")
            {
                metaData = new CustomReportMetaData();
                metaData.Remove(metaData.parameters.DataItems.Select(x => x.ListId).ToList());
            }
            else if (model.Action == "ClearAllColumns")
            {
                metaData = new CustomReportMetaData();
                metaData.Remove(metaData.parameters.ColumnDataItems.Select(x => x.ListId).ToList());
            }

            return CustomReportConfiguration(model);
        }

        /// <summary>
        /// Gets the field type (Project, Commenter, or Letter) based on the Report Wizard step
        /// </summary>
        /// <param name="currentStep">The current step of the wizard</param>
        /// <returns>"Project" "Commenter" "Letter" or "All" if undefined</returns>
        private string GetFieldType(WizardStepElement currentStep)
        {
            switch (currentStep.Name)
            {
                case "ReportProjectInput":
                    return CustomReportFieldType.Project;
                case "ReportSubmitterInput":
                    return CustomReportFieldType.Commenter;
                case "ReportLetterInput":
                    return CustomReportFieldType.Letter;
                case "ReportCommentInput":
                    return CustomReportFieldType.Comment;
                default:
                    return CustomReportFieldType.All;
            }
        }
        #endregion

        #region AJAX methods and related functions

        /// <summary>
        /// Saves the parameters of the Custom Report as a "Saved Report"
        /// </summary>
        /// <param name="name">The name of the report</param>
        /// <param name="isPrivate">Should the report be marked as private</param>
        /// <param name="reportId">The Id of the report</param>
        /// <returns>Success or failure</returns>
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.SYSTEM)]
        public bool SaveMyReport(string name, bool isPrivate, int reportId)
        {
            var result = false;

            // get the current filter setting
            var filter = Utilities.GetFilter(Constants.CUSTOM_REPORT, string.Empty) as CustomReportFilter2;

            if (filter != null)
            {
                // create the object and set the parameters
                if (reportId == 0)
                {
                    var myReport = new MyReport
                    {
                        Name = name,
                        Private = isPrivate,
                        DateCreated = DateTime.UtcNow,
                        UserId = Utilities.GetUserSession().UserId,
                        Parameters = filter.ParameterXML
                    };

                    try
                    {
                        // save the my report settings
                        using (var reportManager = ManagerFactory.CreateInstance<MyReportManager>())
                        {
                            reportManager.Add(myReport);
                            reportManager.UnitOfWork.Save();
                        }

                        // get the result
                        result = myReport.MyReportId > 0;
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        result = false;
                    }
                }
                else
                {
                    try
                    {
                        using (var reportManager = ManagerFactory.CreateInstance<MyReportManager>())
                        {
                            MyReport rpt = reportManager.Find(x => x.MyReportId == reportId).FirstOrDefault();
                            rpt.Parameters = filter.ParameterXML;
                            rpt.Name = name;
                            rpt.Private = isPrivate;
                            rpt.UserId = Utilities.GetUserSession().UserId;
                            rpt.DateCreated = DateTime.UtcNow;
                            reportManager.UnitOfWork.Save();

                        }

                        // get the result
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                        result = false;
                    }
                }
            }

            return result;
        }
        
        /// <summary>
        /// This method is called when a user selects a field on one of the Wizard Field entry pages
        /// </summary>
        /// <param name="fieldId">The Id of the selected field</param>
        /// <returns>
        /// {dataType = the data type of the field,
        /// values = selectlist of possible values for dropdown fields,
        /// toolTip = tool tip for string or text fields}</returns>
        public JsonResult GetFieldMetaData(int fieldId)
        {
            CustomReportFieldElement fieldElement = CustomReportFieldManager.GetById(fieldId);

            List<KeyValuePair<string, string>> valueList = new List<KeyValuePair<string, string>>();
            if (fieldElement.DataType == "dropdown")
            {
                valueList = GetDropdownValues(fieldElement);
            }

            string tooltip = "";
            if (fieldElement.DataType == "string" || fieldElement.DataType == "number")
            {
                tooltip = GetToolTip(fieldElement);
            }

            object retVal = new { dataType = fieldElement.DataType, values = Utilities.ToSelectList(valueList, null, false), toolTip =  tooltip};
            return Json(retVal, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Adds a field value to the custom report
        /// </summary>
        /// <returns>{tree = the parameter tree, error = error message, collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult AddFieldValue()
        {
            try
            {
                //get the field id and include/exclude value from the request
                int fieldId = Int32.Parse(Request.Params["fieldId"].ToString());
                bool not = bool.Parse(Request.Params["not"].ToString());

                //create the custom report metadata (from the cached filter) and get the field
                CustomReportMetaData metaData = new CustomReportMetaData();
                CustomReportFieldElement field = CustomReportFieldManager.GetById(fieldId);

                #region set value
                string value = null;
                bool success = false;
                switch (field.DataType)
                {
                    case "treedropdown":
                    case "dropdown":
                        string name = Request.Params["name"].ToString();
                        value = Request.Params["value"].ToString();
                        success = metaData.Add(field, not, value, name);
                        break;
                    case "date":
                        DateTime from = DateTime.MinValue;
                        DateTime to = DateTime.MaxValue;
                        if (Request.Params.AllKeys.Contains("from") && !string.IsNullOrEmpty(Request.Params["from"].ToString()))
                        { 
                            value = Request.Params["from"].ToString() + " - ";
                            from = DateTime.Parse(Request.Params["from"].ToString());
                        }
                        if (Request.Params.AllKeys.Contains("to") && !string.IsNullOrEmpty(Request.Params["to"].ToString()))
                        { 
                            value += Request.Params["to"].ToString();
                            to = DateTime.Parse(Request.Params["to"].ToString());
                        }
                        success = metaData.Add(field, not, to, from);
                        break;
                    default:
                        value = Request.Params["value"].ToString();
                        success = metaData.Add(field, not, value);
                        break;
                }
                #endregion

                //test to see if the value was successfully added
                if (success)
                {
                    return Json(new
                    {
                        error = "",
                        parameters = metaData.GetParameterDisplay()
                    });
                }
                else
                { 
                    return Json(
                        new { error = String.Format("Value {0} is not in the proper format for field {1}", value, field.Name)},
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while adding field value: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Removes a parameter (field, value, or column) from the custom report
        /// </summary>
        /// <returns>{tree = paramter tree, list = select list of columns, error = error message, collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult RemoveItem()
        {
            try
            {

                // Only one list ID passed
                string listId = Request.Params["listId"].ToString();
                CustomReportMetaData metaData = new CustomReportMetaData();

                object retVal = null;
                //Can not remove the last column of the report
                if (metaData.parameters.ColumnDataItems.Count == 1 && metaData.parameters.ColumnDataItems[0].ListId.ToString() == listId)
                {
                    retVal = new { error = "One column must be defined in the Custom Report Definition." };
                }
                else
                {
                    metaData.Remove(Int32.Parse(listId));
                    retVal = new { 
                        error = string.Empty,
                        parameters = metaData.GetParameterDisplay(),
                        list = metaData.ColumnDisplay()
                    };
                }
                return Json(retVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while removing field value: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the columns for a paticular columkn group
        /// </summary>
        /// <param name="id">the id of the column group</param>
        /// <returns>a select list of columns in that group</returns>
        public JsonResult GetColumns(int id)
        {
            List<CustomReportColumnElement> columns = CustomReportFieldManager.GetColumnList(id).OrderBy(x => x.Name).ToList();
            List<KeyValuePair<int, string>> retVal = new List<KeyValuePair<int, string>>();
            
            //add a blank line for the default value
            retVal.Add(new KeyValuePair<int, string>(0, ""));

            columns.ForEach(x => retVal.Add(new KeyValuePair<int, string>(x.Id, x.Name)));

            return Json(Utilities.ToSelectList(retVal, null, false),JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds a Column to the custom report.
        /// This method is called by the Column Selection page
        /// </summary>
        /// <param name="columnId">The Id of the column to add to the report</param>
        /// <returns>{error = Error message,
        ///     tree = custom report parameter tree,
        ///     list = select list of columns,
        ///     collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult AddColumn()
        {
            try
            {
                int columnId = Int32.Parse(Request.Params["columnId"].ToString());

                CustomReportMetaData metaData = new CustomReportMetaData();

                //custom report can only show a maximum of 20 columns
                string errormsg = "";
                if (metaData.parameters.ColumnDataItems.Count >= 20)
                {
                    errormsg = "Custom Report already has maximum number of columns for preview display.  Additional Columns will only be displayed in csv output.";
                }

                CustomReportColumnElement column = CustomReportFieldManager.GetColumnById(columnId);
                metaData.AddColumn(column);
                object retVal = new {
                    error=errormsg,
                    parameters = metaData.GetParameterDisplay(),
                    list = metaData.ColumnDisplay()
                };
                return Json(retVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while adding column: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Moves one or more columns up or down in the list of columns
        /// </summary>
        /// <returns>{error = error message, tree = parameter tree, list = list of columns, collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult MoveColumns()
        {
            try
            {
                //get the parameters from the request
                int listId = Int32.Parse(Request.Params["listIds"].ToString());
                bool direction = bool.Parse(Request.Params["direction"].ToString());

                CustomReportMetaData metaData = new CustomReportMetaData();
                metaData.MoveColumns(new [] {listId}.ToList(), direction);
                object retVal = new {
                    error = "",
                    parameters = metaData.GetParameterDisplay(),
                    list = metaData.ColumnDisplay()
                };
                return Json(retVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while moving columns: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }



        /// <summary>
        /// Removes all of the columns from the custom report
        /// </summary>
        /// <returns>{error = error message, tree= parameter tree, list = list of columns (empty), collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult ClearAllColumns()
        {
            try
            {
                CustomReportMetaData metaData = new CustomReportMetaData();
                List<int> columnIds = metaData.parameters.ColumnDataItems.Select(x => x.ListId).ToList();
                metaData.Remove(columnIds);
                object retVal = new
                {
                    error = "",
                    parameters = metaData.GetParameterDisplay(),
                    list = metaData.ColumnDisplay()
                };
                return Json(retVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while attempting to clear columns: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        /// <summary>
        /// Removes all of the fields from the custom report
        /// </summary>
        /// <returns>{error = error message, tree= parameter tree, list = list of columns (empty), collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult ClearAllFields()
        {
            try
            {
                CustomReportMetaData metaData = new CustomReportMetaData();
                List<int> fieldIds = metaData.parameters.DataItems.Select(x => x.ListId).ToList();
                metaData.Remove(fieldIds);
                object retVal = new
                {
                    error = "",
                    parameters = metaData.GetParameterDisplay(),
                    list = metaData.ColumnDisplay()
                };
                return Json(retVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while attempting to clear fields: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Changes the sort order of the custom report with the cgiven parameters
        /// </summary>
        /// <returns>{error = error message, tree = parameter tree, collapsedListIds = list of collapsed nodes}</returns>
        public JsonResult UpdateSortOrder()
        {
            try
            {
                //get the parameters from the request
                List<int> collapsedListIds = String.IsNullOrWhiteSpace(Request.Params["collapsedListIds"].ToString()) ?
                    new List<int>() :
                    Request.Params["collapsedListIds"].ToString()
                        .Split("|".ToCharArray())
                        .Select(x => Int32.Parse(x))
                        .ToList();
                CustomReportMetaData metaData = new CustomReportMetaData(collapsedListIds);
                int column1 = Int32.Parse(Request.Params["column1"].ToString());
                int column2 = Int32.Parse(Request.Params["column2"].ToString());
                int column3 = Int32.Parse(Request.Params["column3"].ToString());
                bool asc1 = bool.Parse(Request.Params["asc1"].ToString());
                bool asc2 = bool.Parse(Request.Params["asc2"].ToString());
                bool asc3 = bool.Parse(Request.Params["asc3"].ToString());

                List<KeyValuePair<int, bool>> sortOrder = new List<KeyValuePair<int, bool>>();
                sortOrder.Add(new KeyValuePair<int, bool>(column1, asc1));

                if (column2 > 0 && column2 != column1)
                { sortOrder.Add(new KeyValuePair<int, bool>(column2, asc2)); }

                if (column3 > 0 && column3 != column2 && column3 != column1)
                { sortOrder.Add(new KeyValuePair<int, bool>(column3, asc3)); }

                metaData.SetSortOrder(sortOrder);
                return Json(new {
                    error = "",
                    tree = metaData.ParameterTreeDisplay(),
                    collapsedListIds = String.Join("|", metaData.GetCollapsedListIds())
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = "An error occurred while updating sort order: " + ex.Message }, JsonRequestBehavior.AllowGet); 
            }
        }

        /// <summary>
        /// Adds the custom report to the export queue
        /// </summary>
        /// <returns>An error message.  Empty srting if no error</returns>
        public string Export()
        {
            try
            {
                CustomReportMetaData metaData = new CustomReportMetaData();
                int userId = Utilities.GetUserSession().UserId;
                string xml = metaData.parameters.GetXml(true);
                InsertIntoReportQueue(userId, xml);
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Adds the parameters and userid to the custom report queue
        /// </summary>
        /// <param name="userId">The userid of the user to execute the report</param>
        /// <param name="xml">The parameters of the report</param>
        private void InsertIntoReportQueue(int userId, string xml)
        {
            using (CustomReportQueueManager crqm = ManagerFactory.CreateInstance<CustomReportQueueManager>())
            {
                var crqe = new CustomReportQueue { UserId = userId, ReportDefinition = xml };
                crqm.Add(crqe);
                crqm.UnitOfWork.Save();
            }
        }
  
        /// <summary>
        /// This method generates the custom tool tip for string field UIs
        /// </summary>
        /// <param name="field">The field tooltip is for</param>
        /// <returns>The Tooltip</returns>
        private string GetToolTip(CustomReportFieldElement field)
        {
            return field.ToolTip;
        }

        /// <summary>
        /// Fetches the dropdown values for a specified field
        /// </summary>
        /// <param name="field">The field the values are for</param>
        /// <returns>A list of Keyvalue pairs that contain the id and name of each value</returns>
        private List<KeyValuePair<string, string>> GetDropdownValues(CustomReportFieldElement field)
        {
            List<KeyValuePair<string, string>> retVal = new List<KeyValuePair<string, string>>();
            
            //add an empty entry for default value
            retVal.Add(new KeyValuePair<string,string>("0",""));

            switch (field.Id)
            {
                case 1://analysis type
                    LookupAnalysisType lat = LookupManager.GetLookup<LookupAnalysisType>();
                    lat.OrderBy(x=>x.Name).ToList().ForEach(at => 
                        retVal.Add(new KeyValuePair<string, string>(at.AnalysisTypeId.ToString(), at.Name)));
                    break;

                case 2: //project activities
                    LookupActivity lact = LookupManager.GetLookup<LookupActivity>();
                    lact.OrderBy(x=>x.Name).ToList().ForEach(act=>
                        retVal.Add(new KeyValuePair<string, string>(act.ActivityId.ToString(), act.Name)));
                    break;

                case 3: //project purpose
                    LookupPurpose lpur = LookupManager.GetLookup<LookupPurpose>();
                    lpur.OrderBy(x => x.Name).ToList().ForEach(pur =>
                        retVal.Add(new KeyValuePair<string, string>(pur.PurposeId.ToString(), pur.Name)));
                    break;

                case 7: //state
                    LookupState lsta = LookupManager.GetLookup<LookupState>();
                    lsta.OrderBy(x => x.Name).ToList().ForEach(sta =>
                        retVal.Add(new KeyValuePair<string, string>(sta.StateId, sta.Name)));
                    break;

                case 9: //country
                    LookupCountry lcountry = LookupManager.GetLookup<LookupCountry>();
                    lcountry.OrderBy(x => x.Name).ToList().ForEach(country =>
                        retVal.Add(new KeyValuePair<string, string>(country.CountryId.ToString(), country.Name)));
                    break;

                case 10: //organization
                    using (OrganizationManager orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                    {
                        orgManager.GetSelectList().ToList().ForEach(org=>
                            retVal.Add(new KeyValuePair<string, string>(org.Key.ToString(), org.Value)));
                    }
                    retVal = retVal.OrderBy(x => x.Value).ToList();
                    break;

                case 11: //organization type
                    LookupOrganizationType lorgType = LookupManager.GetLookup<LookupOrganizationType>();
                    lorgType.OrderBy(x => x.Name).ToList().ForEach(orgType =>
                        retVal.Add(new KeyValuePair<string, string>(orgType.OrganizationTypeId.ToString(), orgType.Name)));
                    break;

                case 12: //official representative type
                    LookupOfficialRepresentativeType lorepType = LookupManager.GetLookup<LookupOfficialRepresentativeType>();
                    lorepType.OrderBy(x => x.Name).ToList().ForEach(orepType =>
                        retVal.Add(new KeyValuePair<string, string>(orepType.OfficialRepresentativeTypeId.ToString(), orepType.Name)));
                    break;

                case 13: //delivery type
                    LookupDeliveryType ldeliveryType = LookupManager.GetLookup<LookupDeliveryType>();
                    ldeliveryType.OrderBy(x => x.Name).ToList().ForEach(deliveryType =>
                        retVal.Add(new KeyValuePair<string, string>(deliveryType.DeliveryTypeId.ToString(), deliveryType.Name)));
                    break;

                case 17: //comment code
                    CodeManager codeManager = ManagerFactory.CreateInstance<CodeManager>();
                    codeManager.All.ToList().ForEach(code =>
                        retVal.Add(new KeyValuePair<string, string>(code.CodeId.ToString(), code.Name)));
                    retVal = retVal.OrderBy(x => x.Value).ToList();
                    break;

                case 22: //project type
                    LookupProjectType lprojectType = LookupManager.GetLookup<LookupProjectType>();
                    lprojectType.OrderBy(x => x.Name).ToList().ForEach(projectType =>
                        retVal.Add(new KeyValuePair<string, string>(projectType.ProjectTypeId.ToString(), projectType.Name)));
                    break;

                case 23: //Letter type
                    LookupLetterType lletterType = LookupManager.GetLookup<LookupLetterType>();
                    lletterType.OrderBy(x => x.Name).ToList().ForEach(letterType =>
                        retVal.Add(new KeyValuePair<string, string>(letterType.LetterTypeId.ToString(), letterType.Name)));
                    break;

                case 24: //Comment Period Type
                    LookupPhaseType lphaseType = LookupManager.GetLookup<LookupPhaseType>();
                    lphaseType.OrderBy(x => x.Name).ToList().ForEach(x => 
                        retVal.Add(new KeyValuePair<string,string>(x.PhaseTypeId.ToString(), x.Name)));
                    break;

                case 28: //Preferred Contact Method
                    retVal.Add(new KeyValuePair<string, string>("NULL", "Do Not Contact"));
                    retVal.Add(new KeyValuePair<string, string>("Email", "Email"));
                    retVal.Add(new KeyValuePair<string, string>("Mail", "Mail"));
                    break;

                case 35: //Reviewing Officer Title
                    List<ObjectionDecisionMaker> odms = GetSortedODM(null);
                    odms.ForEach(x =>retVal.Add(new KeyValuePair<string,string>(x.ObjectionDecisionMakerId.ToString(), x.Name)));
                    break;

                case 37: //Review Status
                    LookupObjectionReviewStatus lors = LookupManager.GetLookup<LookupObjectionReviewStatus>();
                    lors.OrderBy(x => x.Name).ToList().ForEach(x =>
                        retVal.Add(new KeyValuePair<string, string>(x.ObjectionReviewStatusId.ToString(), x.Name)));
                    break;

                case 38: //Overall Objection Outcome
                    LookupObjectionOutcome loo = LookupManager.GetLookup<LookupObjectionOutcome>();
                    loo.OrderBy(x => x.Name).ToList().ForEach(x =>
                        retVal.Add(new KeyValuePair<string, string>(x.ObjectionOutcomeId.ToString(), x.Name)));
                    break;

            }
            return retVal;
        }

        private List<ObjectionDecisionMaker> GetSortedODM(int? parentId)
        {
            LookupObjectionDecisionMaker lobjectionDecisionMaker = LookupManager.GetLookup<LookupObjectionDecisionMaker>();
            List<ObjectionDecisionMaker> root = lobjectionDecisionMaker.Where(x => x.ParentId == parentId).OrderBy(x => x.SortOrder).ToList();
            List<ObjectionDecisionMaker> retVal = new List<ObjectionDecisionMaker>();
            root.ForEach(x =>
                {
                    retVal.Add(x);
                    retVal.AddRange(GetSortedODM(x.ObjectionDecisionMakerId));
                });
            return retVal;
        }
        #endregion

        /// <summary>
        /// Loads the specified custom report parameters in to the cache
        /// </summary>
        /// <param name="myReportId">The specified Report Id</param>
        private void SetMyReportFilter(int myReportId)
        {
            using (var reportManager = ManagerFactory.CreateInstance<MyReportManager>())
            {
                // get the report
                var myReport = reportManager.Get(myReportId);

                // get my report parameter
                if (myReport != null)
                {
                    CustomReportFilter2 filter = new Aquilent.Cara.Domain.CustomReportFilter2 { ParameterXML = myReport.Parameters };

                    // get the filter in the session to be loaded by the target page
                    if (filter != null)
                    {
                        Utilities.SetFilter(Constants.CUSTOM_REPORT, string.Empty, filter);
                    }
                }
            }
        }

        

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.REPORT;
        }
        #endregion

    }
}
