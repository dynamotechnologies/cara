﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Controllers
{
    public class ClearLookupCacheController : Controller
    {
        //
        // GET: /ClearLookupCache/

        public string Index()
        {
			LookupManager.Clear();

            return "Lookup Cache Cleared";
        }

    }
}
