﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the navigation specific actions
    /// </summary>
    public class NavController : AbstractController
    {
        #region Project Nav Action Methods
        /// <summary>
        /// Returns the project/phase navigation control
        /// </summary>
        /// <returns></returns>
        public PartialViewResult ProjectNav()
        {
            Project project = null;
            ProjectNavModel model = new ProjectNavModel();

            // parse the project Id from the url
            int projectId = Utilities.GetEntityIdFromUrl(Request.Url.AbsolutePath, Constants.PROJECT);

            // get the project details
            if (projectId > 0)
            {
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    // Turn off lazy loading
                    projectManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                    // Force load all of the properties needed in one query
                    var projectData = projectManager
                        .All
                        .Where(p => p.ProjectId == projectId)
                        .Select(p => new { Project = p, CurrentPhase = p.CurrentPhase, Phase = p.CurrentPhase.Phase, Phases = p.Phases, Members = p.Phases.Select(x => x.PhaseMemberRoles) })
                        .FirstOrDefault();

                    Phase currentPhase = null;
                    if (projectData.CurrentPhase != null)
                    {
                        currentPhase = projectData.Phase;
                    }

                    model = new ProjectNavModel
                    {
                        Project = projectData.Project,
                        CurrentPhase = currentPhase,
                        CurrentUser = Utilities.GetUserSession()
                    };
                }
            }

            // render the project nav view
            return PartialView("~/Views/Shared/ucProjectNav.ascx", model);
        }

        /// <summary>
        /// Returns the wizard navigation control
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public PartialViewResult WizardNav(string actionName, PageMode mode)
        {
            // render the project wizard nav view
            return PartialView("~/Views/Shared/ucWizardNav.ascx", new ProjectWizard(mode, actionName));
        }
        #endregion
    }
}
