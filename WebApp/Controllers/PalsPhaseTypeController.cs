﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain;
using System.Web.Script.Serialization;

namespace Aquilent.Cara.WebApp.Controllers
{
    public class PalsPhaseTypeController : Controller
    {
        //
        // GET: /PalsPhaseType/

        public JsonResult PalsProjectsByUnitId(string unitIdList)
        {
            IList<PalsPhaseTypeProjectModel> model;            

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var projectByUnitID = projectManager.FindProjects(null, null, unitIdList);
                model = projectByUnitID.Select(x => new PalsPhaseTypeProjectModel
                {
                    ProjectNumber = x.ProjectNumber,
                    ProjectName = x.Name,
                    Description = x.Description,
                    CommentStartDate = x.CurrentPhase.Phase.CommentStart,
                    CommentEndDate = x.CurrentPhase.Phase.CommentEnd,
                    LetterCount = x.CurrentPhase.Phase.Letters.Count
                }).ToList();
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        
        public class PalsPhaseTypeProjectModel
        {
            private const string dateFormat = "yyyy-MM-dd";

            public string ProjectNumber { get; set; }
            public string ProjectName { get; set; }
            public string Description { get; set; }
            public string CommentStart { get { return CommentStartDate.ToString(dateFormat); } }
            public string CommentEnd { get { return CommentEndDate.ToString(dateFormat); } }
            public int LetterCount { get; set; }
            [ScriptIgnore]
            public DateTime CommentStartDate { get; set; }

            [ScriptIgnore]
            public DateTime CommentEndDate { get; set; }
        }

    }
}
