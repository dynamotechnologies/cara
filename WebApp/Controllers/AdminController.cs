﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the admin specific actions
    /// </summary>
    public class AdminController : PageController
    {
        #region Core Action Methods
        [HasPrivilege(PrivilegeCodes = "VAP", Scope = Constants.SYSTEM)]
        public ViewResult Index()
        {
            return View();
        }
        #endregion

        #region Cara Setting Action Methods
        [HasPrivilege(PrivilegeCodes = "UWM", Scope = Constants.SYSTEM)]
        public ViewResult CaraSettingInput()
        {
            CaraSetting caraSetting;
            using (var settingManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                caraSetting = settingManager.Get("cara.sys.welcomemessage");
            }

            var model = new CaraSettingInputViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                CaraSetting = caraSetting
            };

            return View(model);
        }

        [HasPrivilege(PrivilegeCodes = "UWM", Scope = Constants.SYSTEM)]
        [HttpPost]//, ValidateInput(false)]
        public ViewResult CaraSettingInput(CaraSettingInputViewModel model)
        {
            if (model.CaraSetting != null)
            {
                // get the decoded form value
                model.CaraSetting.Value = HttpUtility.HtmlDecode(Request.Form.Get("editor"));

                model.ValidateText(ModelState);

                if (ModelState.IsValid)
                {
                    // save the cara setting
                    using (var caraSettingManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                    {
                        caraSettingManager.UnitOfWork.Context.AttachTo("CaraSettings", model.CaraSetting);
                        caraSettingManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(model.CaraSetting, System.Data.EntityState.Modified);
                        caraSettingManager.UnitOfWork.Save();
                    }
                }
            }

            return View(model);
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.ADMIN;
        }
        #endregion
    }
}
