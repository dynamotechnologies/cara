﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Aquilent.Cara.Configuration;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the concern specific actions
    /// </summary>
    public class ConcernController : PageController
    {
        #region Private Members

        private static readonly TextReplacementConfiguration txtReplacementConfig = ConfigUtilities.GetSection<TextReplacementConfiguration>("textReplacementConfiguration");
        
        #endregion

        #region Core Action Methods
        public ViewResult Input(int projectId, int phaseId, int id, string trackBackPage = "", int trackBackLetterId = 0, string trackBackCommentNum = "")
        {
            var title = Utilities.GetResourceString("Concern");

            Phase phase;
            string analysisTypeName;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = phaseManager.Get(phaseId);
                analysisTypeName = phase.Project.AnalysisTypeName;
            }

            ConcernResponse concern = new ConcernResponse
            {                
                ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList.Where(x => x.Value == "Concern In Progress").Select(y => y.Key).First(),
                ConcernCreated = DateTime.UtcNow,
                ConcernCreatedBy = Utilities.GetUserSession().Username,
                LastUpdated = DateTime.UtcNow,
                LastUpdatedBy = Utilities.GetUserSession().Username
            };

            if (!String.IsNullOrWhiteSpace(phase.ConcernTemplate))
            {
                concern.ConcernText = phase.ConcernTemplate;
            }

            if (!String.IsNullOrWhiteSpace(phase.ResponseTemplate))
            {
                concern.ResponseText = phase.ResponseTemplate;
            }

            IList<Comment> comments = null;
            string codeNumberAndDescription = string.Empty;
            if (id > 0)
            {
                using (var concernManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
                {
                    concern = concernManager.Get(id);

                    if (concern != null && concern.ConcernResponseNumber.HasValue)
                    {
                        if (concern.PhaseCodeId.HasValue)
                        {
                            codeNumberAndDescription = string.Format("{0} {1}", concern.PhaseCode.CodeNumberDisplay, concern.PhaseCode.CodeName);
                        }

                        title += " #" + concern.ConcernResponseNumber.ToString();

                        using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                        {
                            comments = commentManager.GetCommentsForConcernResponse(id).ToList();
                        }
                    }
                }
            }

            var model = new ConcernResponseInputViewModel
            {
                BreadCrumbTitle = title,
                PageTitle = title,
                ConcernResponse = concern,
                Locked = phase.Locked,
                AnalysisTypeName = analysisTypeName,
                Comments = comments,
                PhaseId = phase.PhaseId,
                ProjectId = projectId,
                CodeNumberAndDescription = codeNumberAndDescription,
                TrackBackPage = trackBackPage,
                TrackBackLetterId = trackBackLetterId,
                TrackBackCommentNum = trackBackCommentNum,
                UserId = concern.UserId.HasValue?concern.UserId.Value:0
            };

            return View(GetViewPath(), model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Input(int projectId, int phaseId, int id, string action, ConcernResponseInputViewModel model)
        {
            ConcernResponse concern = null;
            int oldUserId = 0;

            using (var concernManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                if (id > 0)
                {
                    concern = concernManager.Get(id);

                    // set the status based on action and data
                    if (concern != null && action.Equals("AssignToSelf"))
                    {
                        concern.ConcernResponseId = id;
                        concern.LastUpdated = DateTime.UtcNow;
                        concern.LastUpdatedBy = Utilities.GetUserSession().Username;
                        oldUserId = concern.UserId.HasValue ? concern.UserId.Value : 0;
                        concern.UserId = Utilities.GetUserSession().UserId;
                        concern.DateAssigned = DateTime.UtcNow;
                    }
                    else if (concern != null)
                    {
                        concern.ConcernResponseStatusId = model.ConcernResponse.ConcernResponseStatusId;
                        concern.Label = model.ConcernResponse.Label;

                        //update the concern task assignment
                        if (action.Equals("ForceSave") && concern.UserId.HasValue)
                        {
                            model.UserId = concern.UserId.Value;
                            using (UserManager userManager = ManagerFactory.CreateInstance<UserManager>())
                            {
                                User assignedUser = userManager.Get(model.UserId);
                                TempData[Constants.CONCERN_RESPONSE] = string.Format(
                                    "User {0} has taken assignment of this Concern/Response.\nYour current changes have been saved but no further changes will be allowed.",
                                    assignedUser.FullName);
                            }
                        }
                        else if (model.UserId == 0 && concern.UserId != null)
                        {
                            concern.UserId = null;
                            concern.DateAssigned = DateTime.UtcNow;
                        }
                        else if (model.UserId > 0 && (concern.UserId == null || concern.UserId.Value != model.UserId))
                        {
                            concern.UserId = model.UserId;
                            concern.DateAssigned = DateTime.UtcNow;
                        }

                        //concern.ConcernText = RichTextValue(HttpUtility.HtmlDecode(Request.Form.Get("concernText")));
                        concern.ConcernText = RichTextValue(Request.Form.Get("concernText"));
                        if (concern.ConcernText == null)
                        {
                            concern.ConcernText = String.Empty;
                        }
                        //concern.ResponseText = RichTextValue(HttpUtility.HtmlDecode(Request.Form.Get("responseText")));
                        concern.ResponseText = RichTextValue(Request.Form.Get("responseText"));
                        if (concern.ResponseText == null)
                        {
                            concern.ResponseText = String.Empty;
                        }
                        concern.PhaseCodeId = model.ConcernResponse.PhaseCodeId;
                        model.ConcernResponse = concern;

                        var commentManager = ManagerFactory.CreateInstance<CommentManager>();
                        commentManager.UnitOfWork = concernManager.UnitOfWork;
                        model.Comments = commentManager.GetCommentsForConcernResponse(id).ToList();
                    }
                }

                // validation
                model.Validate(ModelState, action);

                if (ModelState.IsValid)
                {
                    if (Constants.DELETE.Equals(action))
                    {
                        concernManager.Delete(concern);
                        
                        concernManager.UnitOfWork.Save();

                        return RedirectToAction(Constants.LIST);
                    }

                    //update lastupdated column
                    concern.LastUpdated = DateTime.UtcNow;
                    concern.LastUpdatedBy = Utilities.GetUserSession().Username;

                    // save the data
                    concernManager.UnitOfWork.Save();
                }
            }
            //After the concern has been saved broadcast message to any user that are currently editing the concern
            //if action is assign to self

            if (action.Equals("AssignToSelf"))
            {
                SignalServer.Instance.ReassignConcern(concern.ConcernResponseId, oldUserId);
                //get the new concern information after the other users application has saved
                using (ConcernResponseManager concernManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
                {
                    concern = concernManager.Get(id);
                    if (concern != null)
                    {
                        model.UserId = concern.UserId.HasValue?concern.UserId.Value:0;
                        model.ConcernResponse = concern;
                        var commentManager = ManagerFactory.CreateInstance<CommentManager>();
                        commentManager.UnitOfWork = concernManager.UnitOfWork;
                        model.Comments = commentManager.GetCommentsForConcernResponse(id).ToList();
                    }
                }
            }
            //CARA 988 implement Save and Return functionality
            if (Constants.SAVE_AND_RETURN.Equals(action) && model.TrackBackPage != "")
            {
                switch (model.TrackBackPage)
                {
                    case Constants.CONCERN_RESPONSE+Constants.LIST:
                        return RedirectToAction(Constants.LIST, Constants.CONCERN, new { projectId = projectId, phaseId = phaseId });
                    case Constants.CODING:
                        return RedirectToAction(Constants.CODING, Constants.LETTER, new { id = model.TrackBackLetterId, commentId = model.TrackBackCommentNum });
                    case Constants.COMMENT + Constants.LIST:
                        return RedirectToAction(Constants.LIST, Constants.COMMENT, new { projectId = projectId, phaseId = phaseId, mode = Constants.CREATE });
                    default:
                        break;
                }
            }
            //CARA-1379 implement confirmation message
            if (Constants.SAVE.Equals(action))
            {
                TempData[Constants.CONCERN_RESPONSE] = "Saved Concern Response";
            }
            return View(GetViewPath(), model);
        }

        public ViewResult List(int projectId, int phaseId)
        {
            int pageNum = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["List-page"]))
            {
                if (!int.TryParse(Request.QueryString["List-page"], out pageNum))
                {
                    pageNum = 1;
                }
            }

            // get the filter
            var filter = Utilities.GetFilter(Constants.CONCERN, phaseId.ToString()) as ConcernResponseSearchResultFilter;
            if (filter == null)
            {
                filter = new ConcernResponseSearchResultFilter();
            }

            // Set the page number in the filter
            filter.PageNum = pageNum;

            // translate the filter type from request param
            filter.SetFilterByType(GetParam(Constants.FILTER));

            // set phase ID of the filter
            filter.PhaseId = phaseId;
            Utilities.SetFilter(Constants.CONCERN, phaseId.ToString(), filter);

            string mode = GetParam(Constants.MODE);
            if (Constants.EDIT.Equals(mode))
            {
                filter = new ConcernResponseSearchResultFilter
                {
                    PhaseId = phaseId,
                    SortKey = "Sequence ASC",
                    PageNum = 1
                };
            }

            bool isActive;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                isActive = phaseManager.Get(phaseId).IsActive;
            }

            //set the sort key
            GridCommand gridCommand = new GridCommand
                {
                    PageSize = Constants.EDIT.Equals(mode) ? Constants.MAX_ROW_COUNT : Utilities.GetUserSession().SearchResultsPerPage,
					Page = filter.PageNum.Value
                };
            if (Request.Params["List-orderBy"] != null)
            {
                string sortString = Request.Params["List-orderBy"].ToString();
                if (sortString.Split('-').Count() > 1)
                {
                    SortDescriptor sortDescriptor = new SortDescriptor()
                    {
                        Member = sortString.Split('-')[0],
                        SortDirection =
                            sortString.Split('-')[1].ToLower() == "asc" ?
                                ListSortDirection.Ascending :
                                ListSortDirection.Descending
                    };
                    //filter.SortKey = sortDescriptor.Member +
                    //    (sortDescriptor.SortDirection == ListSortDirection.Ascending ? " ASC" : " DESC");
                    //Utilities.SetFilter(Constants.CONCERN, filter);
                    gridCommand.SortDescriptors.Add(sortDescriptor);
                }
            }
            // Create the view model
			int total;
            var viewModel = new ConcernResponseListViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                ConcernResponses = GetConcernResponses(gridCommand, filter, mode, out total, out pageNum),
                CanEdit = isActive,
                Mode = mode,
                PhaseId = phaseId,
                ProjectId = projectId
            };

            #region Add persistent paging values (if any) to the model
            if (!String.IsNullOrEmpty(Request.QueryString["TaskAssignmentContext"]))
            {
                viewModel.TaskAssignmentContext = Request.QueryString["TaskAssignmentContext"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["TaskAssignmentUsers"]))
            {
                viewModel.TaskAssignmentUsers = Request.QueryString["TaskAssignmentUsers"];
            }
            #endregion //Add persistent values (if any) to the model

            viewModel.LoadFilterToModel(Utilities.GetFilter(Constants.CONCERN, phaseId.ToString()) as ConcernResponseSearchResultFilter);

			ViewData["total"] = total;
			ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }

        [HttpPost]
        public ActionResult List(int projectId, int phaseId, string action, ConcernResponseListViewModel model)
        {
            ModelState.Clear();// disable model element caching
            string mode = GetParam(Constants.MODE);

            // get the filter settings
            int total = 0;
            int pageNum = 1;
            if (!string.IsNullOrEmpty(Request.QueryString["List-page"]))
            {
                if (!int.TryParse(Request.QueryString["List-page"], out pageNum))
                {
                    pageNum = 1;
                }
            }

            // set the filter settings if it's a filter action, else clear all filters to show all records
            if (model.IsSubmit(action))
            {
                // get the filter settings
                if (model != null)
                {
                    // save the filter in the session
                    Utilities.SetFilter(Constants.CONCERN, phaseId.ToString(), model.GetFilterFromModel() as ConcernResponseSearchResultFilter);
                }

				ViewData["total"] = 0;
                ViewData["pageNum"] = 1;

                // list action will get the data
                return RedirectToAction(Constants.LIST, new { mode = GetParam(Constants.MODE) });
            }
            else if (model.IsShowAll(action))
            {
                // clear the filter
				Utilities.SetFilter(Constants.CONCERN, phaseId.ToString(), new ConcernResponseSearchResultFilter { PageNum = 1 });

                // redirect to the page again to clear the page settings
                return RedirectToAction(Constants.LIST, new { mode = model.Mode });
            }
            else if (action.Equals("Save Task Assignments"))
            {
                //SaveTaskAssignments will check to see if any users are editing those CRs
                //and return a list of Concern Response numbers of the ones being edited
                List<int> openCRIds = SaveTaskAssignments(model.TaskAssignmentContext, model.TaskAssignmentUsers);
                model.TaskAssignmentContext = string.Empty;
                model.TaskAssignmentUsers = string.Empty;
                if (openCRIds == null)
                {
                    TempData[Constants.CONCERN_RESPONSE + Constants.LIST] = "An error occurred saving task assignments. Please reload the page and try again.";
                    model.CacheId++;
                }
                else if (openCRIds.Any())
                {
                    string openCRError = OpenCrIdError(openCRIds);
                    TempData[Constants.CONCERN_RESPONSE + Constants.LIST] = openCRError;
                    model.CacheId++;
                }
                else
                {
                    TempData[Constants.CONCERN_RESPONSE + Constants.LIST] = "Concern Response Task Assignments Saved";
                }
            }
            else if (Constants.UPDATE.Equals(action))
            {
                using (var concernManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
                {
                    var editSequencefilter = new ConcernResponseSearchResultFilter
                    {
                        PhaseId = phaseId,
                        SortKey = "Sequence ASC"
                    };

                    // get all filtered concern response phase
                    var list = GetConcernResponses(new GridCommand
                    {
                        PageSize = Constants.EDIT.Equals(GetParam(Constants.MODE)) ? Constants.MAX_ROW_COUNT : Utilities.GetUserSession().SearchResultsPerPage
                    }, editSequencefilter, mode, out total, out pageNum);

                    // list to store the updated concern response
                    var listToUpdate = new List<ConcernResponse>();

                    if (list != null)
                    {
                        // get the sequence inputs from the control and update it if it's changed
                        foreach (ConcernResponseSearchResult concern in list)
                        {
                            var id = "txtSequence" + concern.ConcernResponseId.ToString();
                            var sequence = 0;

                            // validate the sequence input
                            if (Request.Form[id] == null || !int.TryParse(Request.Form[id].ToString(), out sequence))
                            {
                                ModelState.AddModelError(string.Empty, string.Format("Sequence number for {0} ID #{1} is invalid.",
                                    Utilities.GetResourceString(Constants.CONCERN), concern.ConcernResponseId.ToString()));

                                break;
                            }
                            else if (concern.Sequence != sequence)
                            {
                                listToUpdate.Add(new ConcernResponse { ConcernResponseId = concern.ConcernResponseId, Sequence = sequence });
                            }
                        }

                        // if validation succeed
                        if (ModelState.IsValid)
                        {
                            // loop through the changed concern response
                            foreach (ConcernResponse concern in listToUpdate)
                            {
                                // update the concern response sequence
                                concernManager.UpdateSequence(concern.ConcernResponseId, concern.Sequence);
                            }
                            string contextIds = Request.Params["TaskAssignmentContext"].ToString();
                            string userIds = Request.Params["TaskAssignmentUsers"].ToString();
                            List<int> openCRIds = SaveTaskAssignments(contextIds, userIds);
                            if (openCRIds == null)
                            {
                                TempData[Constants.CONCERN_RESPONSE + Constants.LIST] = "An error occurred saving task assignments. Please reload the page and try again.";
                            }
                            else if (openCRIds.Any())
                            {
                                string openCRError = OpenCrIdError(openCRIds);
                                TempData[Constants.CONCERN_RESPONSE + Constants.LIST] = openCRError;
                            }
                            else
                            {
                                TempData[Constants.CONCERN_RESPONSE + Constants.LIST] = "Concern / Response Sequence and Task Assignments Saved";
                            }
                        }
                    }

                    return RedirectToAction(Constants.LIST, new { Mode = Constants.EDIT });
                }
            }

            // get all concern response in the phase after the update
            var filter = Utilities.GetFilter(Constants.CONCERN, phaseId.ToString()) as ConcernResponseSearchResultFilter;
            model.ConcernResponses = GetConcernResponses(new GridCommand
            {
                PageSize = Constants.EDIT.Equals(GetParam(Constants.MODE)) ? Constants.MAX_ROW_COUNT : Utilities.GetUserSession().SearchResultsPerPage
            }, filter, mode, out total, out pageNum);

			ViewData["total"] = total;
			ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), model);
        }

        private List<int> SaveTaskAssignments(string contextIds, string userIds)
        {
            List<int> retVal = new List<int>();
            if (string.IsNullOrWhiteSpace(contextIds) || string.IsNullOrWhiteSpace(userIds))
            {
                return retVal;
            }
            string[] strArrContext = contextIds.Split('|');
            string[] strArrUserIds = userIds.Split('|');
            //make sure there are the same number of ids
            if (strArrContext.Count() != strArrUserIds.Count() || strArrContext.Count() == 0)
            {
                return null;
            }
            int[] intContextIds = strArrContext.Select(x => int.Parse(x)).ToArray();
            int[] intUserIds = strArrUserIds.Select(x => int.Parse(x)).ToArray();
            using (ConcernResponseManager concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                List<ConcernResponse> concerns = concernResponseManager.All
                    .Where(x => intContextIds.Contains(x.ConcernResponseId))
                    .ToList();

                //create a list of all the concernids with their assigned users to check for active editors
                List<KeyValuePair<int, int>> concernUserIds = new List<KeyValuePair<int, int>>();
                for (int i = 0; i < intContextIds.Count(); i++)
                {
                    ConcernResponse concern =  concerns.FirstOrDefault(x => x.ConcernResponseId == intContextIds[i]);

                    /*do not include the concernid if 
                     * the concern does not exist, 
                     * is not assigned, 
                     * is assigned to the current user
                     * or is assigned to the user it is trying to be assigned to*/
                     
                    if (concern == null || 
                        !(concern.UserId.HasValue) || 
                        concern.UserId.Value == Utilities.GetUserSession().UserId ||
                        concern.UserId.Value == intUserIds[i])
                    {
                        continue;
                    }
                    concernUserIds.Add(new KeyValuePair<int, int>(concern.ConcernResponseId, concern.UserId.Value));
                }
                List<int> activeEditCRIds = SignalServer.Instance.CheckHubs(Constants.CONCERN_RESPONSE, concernUserIds);

                for (int i = 0; i < intContextIds.Count(); i++)
                {
                    ConcernResponse concern = concerns.FirstOrDefault(x => x.ConcernResponseId == intContextIds[i]);
                    if (concern == null)
                    {
                        continue;
                    }
                    if (activeEditCRIds.Contains(intContextIds[i]))
                    {
                        retVal.Add(concern.ConcernResponseNumber.HasValue?concern.ConcernResponseNumber.Value:0);
                        continue;
                    }
                    if (intUserIds[i] == 0 && concern.UserId != null)
                    {
                        concern.UserId = null;
                        concern.DateAssigned = DateTime.UtcNow;
                    }
                    else if (intUserIds[i] > 0 && (concern.UserId == null || concern.UserId.Value != intUserIds[i]))
                    {
                        concern.UserId = intUserIds[i];
                        concern.DateAssigned = DateTime.UtcNow;
                    }
                }
                concernResponseManager.UnitOfWork.Save();
            }
            return retVal;
        }

        private string OpenCrIdError(List<int> openCrIds)
        {
            string retVal = "The following Concern / Responses are currently being edited and cannot be reassigned: ";
            string crIdList = string.Empty;
            foreach (int crId in openCrIds)
            {
                if (!String.IsNullOrWhiteSpace(crIdList))
                {
                    crIdList = crIdList + ", ";
                }
                crIdList = crIdList + crId.ToString();
            }
            retVal = retVal + crIdList;
            return retVal;
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ConcernListBinding(GridCommand command)
        {
			int total;
            int pageNum;
            string context = Request.RequestContext.RouteData.Values["phaseId"].ToString();
            var filter = Utilities.GetFilter(Constants.CONCERN, context) as ConcernResponseSearchResultFilter;
			var data = GetConcernResponses(command, filter, null, out total, out pageNum).ToList();

			ViewData["pageNum"] = pageNum;

            return View(GetViewPath(Constants.LIST), new GridModel { Data = data, Total = total });
        }

        private static ICollection<ConcernResponseSearchResult> GetConcernResponses(GridCommand command, ConcernResponseSearchResultFilter filter, string mode, out int total, out int pageNum)
        {
            // get the filter
            //var filter = Utilities.GetFilter(Constants.CONCERN) as ConcernResponseSearchResultFilter;
            if (filter == null)
            {
                filter = new ConcernResponseSearchResultFilter();
            }

            // set the paging
            //filter.PageNum = command.Page;
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }
            
            total = 0;

            // get the data
            IList<ConcernResponseSearchResult> data;
            using (var concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                data = concernResponseManager.GetConcernResponses(filter, out total);
            }

			pageNum = command.Page;
			filter.PageNum = command.Page;

            if (!string.IsNullOrEmpty(mode) && !mode.Equals(Constants.EDIT))
            {
                Utilities.SetFilter(Constants.CONCERN, filter.PhaseId.ToString(), filter);
            }

            // return the data as a list
            return data.ToList();
        }

        public PartialViewResult SelectCode(int id)
        {
            // Get the codes for this phase
            using (var phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
            {
                var codes = phaseCodeManager.All.Where(pc => pc.PhaseId == id).OrderBy(pc => pc.CodeNumber).ToList();
                

                return PartialView("~/Views/Shared/Input/ucCodingConcernInput.ascx", CodeUtilities.ToPhaseCodeTreeByType(codes));
            }
        }
        #endregion

        #region Sequence Methods
        [HasPrivilege(PrivilegeCodes = "WRSP", Scope = Constants.PHASE)]
        public ActionResult First(int projectId, int phaseId, int id)
        {
            // update the concern response sequence
            using (var concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                concernResponseManager.UpdateSequence(id, null, Constants.FIRST);
            }

            return RedirectToAction(Constants.LIST, new { Mode = Constants.EDIT, Updated = id });
        }

        [HasPrivilege(PrivilegeCodes = "WRSP", Scope = Constants.PHASE)]
        public ActionResult Prev(int projectId, int phaseId, int id)
        {
            // update the concern response sequence
            using (var concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                concernResponseManager.UpdateSequence(id, null, Constants.PREV);
            }

            return RedirectToAction(Constants.LIST, new { Mode = Constants.EDIT, Updated = id });
        }

        [HasPrivilege(PrivilegeCodes = "WRSP", Scope = Constants.PHASE)]
        public ActionResult Next(int projectId, int phaseId, int id)
        {
            // update the concern response sequence
            using (var concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                concernResponseManager.UpdateSequence(id, null, Constants.NEXT);
            }

            return RedirectToAction(Constants.LIST, new { Mode = Constants.EDIT, Updated = id });
        }

        [HasPrivilege(PrivilegeCodes = "WRSP", Scope = Constants.PHASE)]
        public ActionResult Last(int projectId, int phaseId, int id)
        {
            // update the concern response sequence
            using (var concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                concernResponseManager.UpdateSequence(id, null, Constants.LAST);
            }

            return RedirectToAction(Constants.LIST, new { Mode = Constants.EDIT, Updated = id });
        }
        #endregion

        #region Comment Action Methods
        [HasPrivilege(PrivilegeCodes = "WRSP", Scope = Constants.PHASE)]
        public ActionResult DeleteComment(int id, int commentId)
        {
            IList<Comment> comments;
            bool locked;
            int phaseId;
            int userId;

            // unassociate the comment from concern response
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                var comment = commentManager.Get(commentId);
                userId = comment.ConcernResponse.UserId.HasValue ? comment.ConcernResponse.UserId.Value : 0;

                if (comment != null)
                {

                    //update lastupdated column
                    comment.ConcernResponse.LastUpdated = DateTime.UtcNow;
                    comment.ConcernResponse.LastUpdatedBy = Utilities.GetUserSession().Username;

                    // set phase ID to null to unassociate it from phase
                    comment.ConcernResponseId = null;
                    commentManager.UnitOfWork.Save();
                }

                comments = commentManager.GetCommentsForConcernResponse(id).ToList();
                locked = comments[0].Letter.Phase.Locked;
                phaseId = comments[0].Letter.Phase.PhaseId;
            }

            var model = new ConcernResponseInputViewModel
            {
                Comments = comments,
                Locked = locked,
                PhaseId = phaseId,
                UserId = userId
            };

            return PartialView("~/Views/Shared/List/ucConcernCommentList.ascx", model);
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion

        #region NonActions
        [NonAction]
        // Returns null if the contents of richText only contains markup, but no content
        private string RichTextValue(string richText)
        {
            string actualValue = null;

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml("<html><head></head><body>" + richText + "</body></html>");
            if (doc != null)
            {

                string output = "";
                foreach (var node in doc.DocumentNode.ChildNodes)
                {
                    output += node.InnerText;
                }

                if (!string.IsNullOrWhiteSpace(output))
                {
                    actualValue = txtReplacementConfig.ReplaceInvalidCharacters(richText);
                }

            }

            return actualValue;
        }
        #endregion NonActions
    }
}
