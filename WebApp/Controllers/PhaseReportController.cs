﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc.UI;
using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reporting;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.Domain.Report;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using System.IO;
using Telerik.Reporting.Processing;
using Telerik.Reporting.Xml;
using System.Threading;
using Telerik.Web.Mvc.Infrastructure;
using System.Collections;
using System.Text;
using System.Web.UI;
using NPOI.XWPF.UserModel;
using Aquilent.Cara.Reporting.Export.Web.Mvc;
using Aquilent.Cara.Reporting.Export;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for the phase report specific actions
    /// </summary>
    public class PhaseReportController : PageController
    {
        #region Core Actions
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult List(int projectId, int phaseId)
        {            
            bool locked;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                locked = phaseManager.Get(phaseId).Locked;
            }

            IList<Aquilent.Cara.Domain.Report.Report> reports;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reports = reportManager.All.ToList();
            }

            return View(GetViewPath(), new PhaseReportViewModel
            {
                BreadCrumbTitle = Utilities.GetResourceString(Constants.STANDARD_REPORTS),
                PageTitle = Utilities.GetResourceString(Constants.STANDARD_REPORTS),
                Locked = locked,
                Reports = reports,
                BrowserName = Request.Browser.Browser
            });
        }
        
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult Run(int projectId, int phaseId, int id)
        {
			var option = Request.Params["option"];

			var reportEntity = LookupManager.GetLookup<LookupReport>()[id];
			
			var report = ReportFactory.GetReport(id, phaseId, option);

			return View(GetViewPath(),
				new PhaseReportRunViewModel
				{
					PhaseId = phaseId,
					Report = report,
					ReportName = reportEntity.Name
				});
        }

        #region TeamMember Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult TeamMember(int phaseId, int id)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

             var viewModel = new GenericStandardReportViewModel<TeamMemberReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetTeamMemberReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)
              
              };

             ViewData["total"] = total;
             ViewData["pageNum"] = pageNum;
             

             return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult TeamMemberBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetTeamMemberReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.TEAMMEMBER_REPORT, new GridModel { Data = data, Total = total });
         }

        private static ICollection<TeamMemberReportData> GetTeamMemberReportData(GridCommand command, int phaseId, out int total, out int pageNum)
         {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;
            
            PageNum = command.Page;
            NumRows =  command.PageSize;
           
            IList<TeamMemberReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetTeamMemberReport(phaseId, PageNum, NumRows, out total).ToList();
                                
            }

            pageNum = command.Page;
           
            return data;
        }
       
        #endregion TeamMember Report

        #region Mailing List Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult MailingList(int phaseId, int id)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<MailingListReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetMailingListReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)

            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult MailingListBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetMailingListReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.MAILINGLIST_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<MailingListReportData> GetMailingListReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = command.Page;
            NumRows = command.PageSize;

            IList<MailingListReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetMailingListReport(phaseId, PageNum, NumRows, out total).ToList();

            }

            pageNum = command.Page;

            return data;
        }

        #endregion MailingList Report

        #region Untimely Comments Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult UntimelyComments(int phaseId, int id)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<UntimelyCommentsReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetUntimelyCommentsReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)

            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult UntimelyCommentsBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetUntimelyCommentsReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.UNTIMELYCOMMENTS_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<UntimelyCommentsReportData> GetUntimelyCommentsReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = command.Page;
            NumRows = command.PageSize;

            IList<UntimelyCommentsReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetUntimelyCommentsReport(phaseId, PageNum, NumRows, out total).ToList();

                if (data != null && data.Count > 0)
                {
                    TimeZoneInfo tzi;
                    foreach (var item in data)
                    {
                        if (!string.IsNullOrEmpty(item.TimeZone))
                        {
                            tzi = TimeZoneInfo.FindSystemTimeZoneById(item.TimeZone);
                            item.DateSubmitted = TimeZoneInfo.ConvertTimeFromUtc(item.DateSubmitted, tzi);
                        }
                    }
                }
            }

            pageNum = command.Page;

            return data;
        }

        #endregion Untimely Comments Report

        #region CommenterOrgType Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult CommenterOrgType(int phaseId, int id)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<CommenterOrganizationTypeData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetCommenterOrgTypeReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)

            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult CommenterOrgTypeBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetCommenterOrgTypeReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.COMMENTERORGTYPE_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<CommenterOrganizationTypeData> GetCommenterOrgTypeReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = command.Page;
            NumRows = command.PageSize;

            IList<CommenterOrganizationTypeData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetCommenterOrganizationTypeReport(phaseId, PageNum, NumRows, out total).ToList();

            }

            pageNum = command.Page;

            return data;
        }

        #endregion CommenterOrganizationType Report

        #region EarlyAction Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        [GridAction(GridName = "Grid")]
        public ViewResult EarlyAction(int phaseId, int id, GridCommand command)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new StandardReportWithGroupingViewModel
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetEarlyActionReportData(IsEmptyCommand(command) ? new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage } : command, phaseId, out total, out pageNum)

            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }

        private static IEnumerable GetEarlyActionReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = (command.Page > 0 ? command.Page : 1);
            NumRows = command.PageSize;

            IList<EarlyActionReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetEarlyActionReport(phaseId, PageNum, NumRows, out total);

            }

            // always apply the form set name
            if (!command.GroupDescriptors.Any())
            {
                command.GroupDescriptors.Add(new GroupDescriptor { Member = "TermListName" });
            }

            pageNum = command.Page;

            //apply grouping and return the result
            if (command.GroupDescriptors.Any())
            {
                return ApplyGrouping(data.AsQueryable(), command.GroupDescriptors);
            }

            return data.ToList();
        }

        [NonAction]
        private static IEnumerable<AggregateFunctionsGroup> ApplyGrouping(IQueryable<EarlyActionReportData> data, IList<GroupDescriptor> groupDescriptors)
        {
            Func<IEnumerable<EarlyActionReportData>, IEnumerable<AggregateFunctionsGroup>> selector = null;

            // apply the grouping
            foreach (var group in groupDescriptors.Reverse())
            {
                if (selector == null)
                {
                    if (group.Member == "TermListName")
                    {
                        selector = g => BuildInnerGroup(g.Select(p => p), p => p.TermListNameDisplay, i => i.ToList());
                       
                    }
                  }
                else
                {
                    if (group.Member == "TermListName")
                    {
                        var tempSelector = selector;
                        selector = g => g.GroupBy(p => p.TermListNameDisplay)
                                         .Select(c => new AggregateFunctionsGroup
                                         {
                                             Key = c.Key,
                                             HasSubgroups = true,
                                             Items = tempSelector.Invoke(c).ToList()
                                         });
                    }
                   
                }
            }

            return selector.Invoke(data).ToList();
        }

        [NonAction]
        private static IEnumerable<AggregateFunctionsGroup> BuildInnerGroup<U>(IEnumerable<EarlyActionReportData> group, Func<EarlyActionReportData, U> groupSelector,
            Func<IEnumerable<EarlyActionReportData>, IEnumerable> innerSelector)
        {
            return group.GroupBy(groupSelector)
                    .Select(i => new AggregateFunctionsGroup
                    {
                        Key = i.Key,
                        Items = innerSelector(i)
                    });
        }

        #endregion EarlyAction Report

        #region Comment Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        [GridAction(GridName = "Grid")]
        public ViewResult Comment(int phaseId, int id, GridCommand command)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new StandardReportWithGroupingViewModel
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetCommentReportData(IsEmptyCommand(command) ? new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage } : command, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }

        private static IEnumerable GetCommentReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = (command.Page > 0 ? command.Page : 1);
            NumRows = command.PageSize;

            IList<CommentReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetCommentReport(phaseId, PageNum, NumRows, out total);            

            }

            // always apply the form set name
            if (!command.GroupDescriptors.Any())
            {
                command.GroupDescriptors.Add(new GroupDescriptor { Member = "CodeCategory" });
            }

            pageNum = command.Page;

            //apply grouping and return the result
            if (command.GroupDescriptors.Any())
            {
                return ApplyGrouping(data.AsQueryable(), command.GroupDescriptors);
            }

            return data.ToList();
        }

        [NonAction]
        private static IEnumerable<AggregateFunctionsGroup> ApplyGrouping(IQueryable<CommentReportData> data, IList<GroupDescriptor> groupDescriptors)
        {
            Func<IEnumerable<CommentReportData>, IEnumerable<AggregateFunctionsGroup>> selector = null;

            // apply the grouping
            foreach (var group in groupDescriptors.Reverse())
            {
                if (selector == null)
                {
                    if (group.Member == "CodeCategory")
                    {
                        selector = g => BuildInnerGroup(g.Select(p => p), p => p.CodeCategory, i => i.ToList());

                    }
                }
                else
                {
                    if (group.Member == "CodeCategory")
                    {
                        var tempSelector = selector;
                        selector = g => g.GroupBy(p => p.CodeCategory)
                                         .Select(c => new AggregateFunctionsGroup
                                         {
                                             Key = c.Key,
                                             HasSubgroups = true,
                                             Items = tempSelector.Invoke(c).ToList()
                                         });
                    }

                }
            }

            return selector.Invoke(data).ToList();
        }

        [NonAction]
        private static IEnumerable<AggregateFunctionsGroup> BuildInnerGroup<U>(IEnumerable<CommentReportData> group, Func<CommentReportData, U> groupSelector,
            Func<IEnumerable<CommentReportData>, IEnumerable> innerSelector)
        {
            return group.GroupBy(groupSelector)
                    .Select(i => new AggregateFunctionsGroup
                    {
                        Key = i.Key,
                        Items = innerSelector(i)
                    });
        }

        #endregion Comment Report

        #region Comment with C\R Info report
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        [GridAction(GridName = "Grid")]
        public ViewResult CommentWithCRInfo(int phaseId, int id, GridCommand command)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<CommentWithCRInfoReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetCommentWithCRInfoReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult CommentWithCRInfoBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetCommentWithCRInfoReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.COMMENTWITHCRINFO_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<CommentWithCRInfoReportData> GetCommentWithCRInfoReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = command.Page;
            NumRows = command.PageSize;

            IList<CommentWithCRInfoReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetCommentWithCRInfoReportData(phaseId, PageNum, NumRows, out total).ToList();

            }

            pageNum = command.Page;

            return data;
        }

        #endregion

        #region Concern Response With AssignedTo
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        [GridAction(GridName = "Grid")]
        public ViewResult ConcernResponseWithAssignedTo(int phaseId, int id, GridCommand command)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<ConcernResponseWithAssignedToReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetConcernResponseWithAssignedToReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult ConcernResponseWithAssignedToBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetConcernResponseWithAssignedToReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.COMMENTWITHCRINFO_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<ConcernResponseWithAssignedToReportData> GetConcernResponseWithAssignedToReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = command.Page;
            NumRows = command.PageSize;

            IList<ConcernResponseWithAssignedToReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetConcernResponseWithAssignedToReportData(phaseId, PageNum, NumRows, out total).ToList();

            }

            pageNum = command.Page;

            return data;
        }

        #endregion

        #region Comment With Annotation Report
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        [GridAction(GridName = "Grid")]
        public ViewResult CommentWithAnnotation(int phaseId, int id, GridCommand command)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<CommentWithAnnotationReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetCommentWithAnnotationReportData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;


            return View(GetViewPath(), viewModel);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult CommentWithAnnotationBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetCommentWithAnnotationReportData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.COMMENTWITHANNOTATION_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<CommentWithAnnotationReportData> GetCommentWithAnnotationReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = command.Page;
            NumRows = command.PageSize;

            IList<CommentWithAnnotationReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetCommentWithAnnotationReportData(phaseId, PageNum, NumRows, out total).ToList();

            }

            pageNum = command.Page;

            return data;
        }

        #endregion

        #region Concern Response Status Report

        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        [GridAction(GridName = "Grid")]
        public ViewResult ResponseStatus(int phaseId, int id, GridCommand command)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new StandardReportWithGroupingViewModel
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetResponseStatusReportData(IsEmptyCommand(command) ? new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage } : command, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }


        private static IEnumerable GetResponseStatusReportData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            var PageNum = 1;
            var NumRows = 0;

            PageNum = (command.Page > 0 ? command.Page : 1);
            NumRows = command.PageSize;

            IList<ResponseStatusReportData> data;
            var commentId = string.Empty;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetResponseStatusReport(phaseId, PageNum, NumRows, out total).ToList();
            }

            //code to write contents to new compact list of type data
            int tempCommentId = 0;
            string tempConcernResponseId = string.Empty;
            var compactList = new List<ResponseStatusReportData>();
            ResponseStatusReportData rsrdata = new ResponseStatusReportData();

            foreach (var entry in data)
            {
                //if (tempConcernResponseId != Convert.ToString(entry.ConcernResponseId))
                if (tempCommentId != entry.CommentId)
                {
                    //copy entry into the new list
                    rsrdata = new ResponseStatusReportData();
                    //tempConcernResponseId = Convert.ToString(entry.ConcernResponseId);
                    tempCommentId = entry.CommentId;
                    compactList.Add(rsrdata);
                }

                //Format CommentCodeNumber
                string commentCodeNumber = string.Empty;
                if (!String.IsNullOrEmpty(entry.CodeNumber))
                {
                    StringBuilder cCodeNumber = new StringBuilder(entry.CodeNumber);

                    cCodeNumber.Replace(".0000.00", string.Empty);
                    cCodeNumber.Replace("00.00", string.Empty);
                    cCodeNumber.Replace(".00", string.Empty);

                    commentCodeNumber = cCodeNumber.ToString();
                }

                if (String.IsNullOrEmpty(rsrdata.CodeName))
                {
                    rsrdata.CodeName = commentCodeNumber + " " + entry.CodeName;
                }
                else
                {
                    rsrdata.CodeName = rsrdata.CodeName + "<br>" + commentCodeNumber + " " + entry.CodeName;
                }
                rsrdata.Associated = entry.Associated;
                rsrdata.CodeNumber = entry.CodeNumber;
                rsrdata.CommentId = entry.CommentId;
                rsrdata.CommentNumber = entry.CommentNumber;
                // CR-296 Handling new line characters in comment text
                rsrdata.CommentText = ReportManager.CleanCommentText(entry.CommentText);
                rsrdata.ConcernResponseId = entry.ConcernResponseId;
                rsrdata.ConcernResponseNumber = entry.ConcernResponseNumber;
                rsrdata.StatusName = entry.StatusName;
                rsrdata.LetterSequence = entry.LetterSequence;
            }

            // always apply the form set name
            if (!command.GroupDescriptors.Any())
            {
                command.GroupDescriptors.Add(new GroupDescriptor { Member = "GroupBy1" });
            }

            pageNum = command.Page;

            //apply grouping and return the result
            if (command.GroupDescriptors.Any())
            {
                return ApplyGrouping(compactList.AsQueryable(), command.GroupDescriptors);
            }

            return compactList;
        }

        [NonAction]
        private static IEnumerable<AggregateFunctionsGroup> ApplyGrouping(IQueryable<ResponseStatusReportData> data, IList<GroupDescriptor> groupDescriptors)
        {
            Func<IEnumerable<ResponseStatusReportData>, IEnumerable<AggregateFunctionsGroup>> selector = null;

            // apply the grouping
            foreach (var group in groupDescriptors.Reverse())
            {
                if (selector == null)
                {
                    if (group.Member == "GroupBy1")
                    {
                        selector = g => BuildInnerGroup(g.Select(p => p), p => p.GroupBy1, i => i.ToList());

                    }
                }
                else
                {
                    if (group.Member == "GroupBy1")
                    {
                        var tempSelector = selector;
                        selector = g => g.GroupBy(p => p.GroupBy1)
                                         .Select(c => new AggregateFunctionsGroup
                                         {
                                             Key = c.Key,
                                             HasSubgroups = true,
                                             Items = tempSelector.Invoke(c).ToList()
                                         });
                    }

                }
            }

            return selector.Invoke(data).ToList();
        }

        [NonAction]
        private static IEnumerable<AggregateFunctionsGroup> BuildInnerGroup<U>(IEnumerable<ResponseStatusReportData> group, Func<ResponseStatusReportData, U> groupSelector,
            Func<IEnumerable<ResponseStatusReportData>, IEnumerable> innerSelector)
        {
            return group.GroupBy(groupSelector)
                    .Select(i => new AggregateFunctionsGroup
                    {
                        Key = i.Key,
                        Items = innerSelector(i)
                    });
        }

       #endregion Concern Response Status Report

        #region Comments and Responses by Letter Report
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult CommentsAndResponsesByLetter(int phaseId, int id)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<CommentsAndResponsesByLetterReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetCommentsAndResponsesByLetterData(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult CommentsAndResponsesByLetterBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetCommentsAndResponsesByLetterData(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.COMMENTSRESPONSESBYLETTER_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<CommentsAndResponsesByLetterReportData> GetCommentsAndResponsesByLetterData(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            int numRows = 0;

            pageNum = command.Page;
            numRows = command.PageSize;

            IList<CommentsAndResponsesByLetterReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetCommentsAndResponsesByLetterReport(phaseId, null, pageNum, numRows, out total).ToList();
            }

            pageNum = command.Page;

            return data;
        }
        #endregion Comments and Responses by Letter Report

        #region Concern Response By Commenter
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult ConcernResponseByCommenter(int phaseId, int id, int option)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];
            var reportData = GetConcernResponseByCommenter(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, option, out total, out pageNum);
            var viewModel = new GenericStandardReportViewModel<ConcernResponseByCommenterReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                Option = option,
                ReportData = reportData
            };

            total = reportData.Count > 0 ? reportData.ElementAt(0).TotalRows : 0;

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ConcernResponseByCommenterBinding(GridCommand command, int phaseId, int option)
        {
            int total;
            int pageNum;

            var data = GetConcernResponseByCommenter(command, phaseId, option, out total, out pageNum).ToList();

            total = data.Count > 0 ? data.ElementAt(0).TotalRows : 0;

            return View(Constants.CONCERNRESPONSEBYCOMMENTER_REPORT, new GridModel { Data = data, Total = total });
        }

        private static ICollection<ConcernResponseByCommenterReportData> GetConcernResponseByCommenter(GridCommand command, int phaseId, int option, out int total, out int pageNum)
        {
            total = 0;
            int numRows = 0;

            pageNum = command.Page;
            numRows = command.PageSize;

            IList<ConcernResponseByCommenterReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetConcernResponseByCommenterReport(phaseId, option, pageNum, numRows, out total).ToList();
            }

            pageNum = command.Page;

            return data;
        }
        #endregion Comments and Responses by Letter Report

        #region Comment Response No Concern
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult CommentResponseNoConcern(int phaseId, int id)
        {
            int total;
            int pageNum;

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<CommentResponseNoConcernReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                ReportData = GetCommentResponseNoConcernReport(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage }, phaseId, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult CommentResponseNoConcernBinding(GridCommand command, int phaseId)
        {
            int total;
            int pageNum;

            var data = GetCommentResponseNoConcernReport(command, phaseId, out total, out pageNum).ToList();

            return View(Constants.COMMENTRESPONSENOCONCERN_REPORT, new GridModel { Data = data, Total = total });
        }

        public WordResult ExportCommentResponseNoConcern(int phaseId, int id)
        {
            int total;
            int pageNum;
            WordResult wordResult = null;

            try
            {
                var data = GetCommentResponseNoConcernReport(new GridCommand { Page = -1 }, phaseId, out total, out pageNum);

                CommentResponseNoConcernReportExport export = new CommentResponseNoConcernReportExport(data);
                XWPFDocument doc = export.Export();

                var reportTimeStamp = DateTime.UtcNow.ConvertToUserTimeZone(Utilities.GetUserSession()).ToString("yyyyMMddhhmmss");
                wordResult = new WordResult(doc, string.Format("{0}_{1}_CommentResponseNoConcernReport.docx", reportTimeStamp, phaseId));
            }
            catch (Exception ex)
            {
                throw new ApplicationException("A problem occurred exporting the 'Comment Response (No Concern)' report", ex);
            }

            return wordResult;
        }

        private static ICollection<CommentResponseNoConcernReportData> GetCommentResponseNoConcernReport(GridCommand command, int phaseId, out int total, out int pageNum)
        {
            total = 0;
            int numRows = 0;

            pageNum = command.Page;
            numRows = command.PageSize;

            IList<CommentResponseNoConcernReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetCommentResponseNoConcernReport(phaseId, pageNum, numRows, out total).ToList();
                            
            }

            pageNum = command.Page;

            return data;
        }
        #endregion Comment Response No Concern

        #region Response To Comment
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ViewResult ResponseToComment(int phaseId, int id, int option)
        {
            int total;
            int pageNum;

            if (!int.TryParse(Request.QueryString["ResponseToComment-page"], out pageNum))
            {
                pageNum = 1;
            }

            var reportEntity = LookupManager.GetLookup<LookupReport>()[id];

            var viewModel = new GenericStandardReportViewModel<ResponseToCommentReportData>
            {
                BreadCrumbTitle = reportEntity.Name,
                PageTitle = reportEntity.Name,
                PhaseId = phaseId,
                Option = option,
                ReportData = GetResponseToCommentReport(new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage, Page = pageNum }, phaseId, option, out total, out pageNum)
            };

            ViewData["total"] = total;
            ViewData["pageNum"] = pageNum;

            return View(GetViewPath(), viewModel);
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult ResponseToCommentBinding(GridCommand command, int phaseId, int option)
        {
            int total;
            int pageNum;

            var data = GetResponseToCommentReport(command, phaseId, option, out total, out pageNum).ToList();

            return View(Constants.RESPONSETOCOMMENT_REPORT, new GridModel { Data = data, Total = total });
        }

        public WordResult ExportResponseToComment(int phaseId, int id, int option)
        {
            int total = int.MaxValue;
            int pageSize = 50;
            int pageNum;
            WordResult wordResult = null;
            List<ResponseToCommentReportData> reportData = new List<ResponseToCommentReportData>();

            try
            {
                for (int i = 1; i <= Math.Ceiling((double)total / pageSize); i++)
                {
                    var data = GetResponseToCommentReport(new GridCommand { Page = i, PageSize = pageSize }, phaseId, option, out total, out pageNum);
                    reportData.AddRange(data);
                }

                ResponseToCommentReportExport export = new ResponseToCommentReportExport(reportData);
                XWPFDocument doc = export.Export();
                var reportTimeStamp = DateTime.UtcNow.ConvertToUserTimeZone(Utilities.GetUserSession()).ToString("yyyyMMddhhmmss");
                wordResult = new WordResult(doc, string.Format("{0}_{1}_{2}_ResponseToCommentReport.docx", reportTimeStamp, phaseId, option));
            }
            catch (Exception ex)
            {
                throw new ApplicationException("A problem occurred exporting the 'Response To Comment' report", ex);
            }

            return wordResult;
        }

        private static ICollection<ResponseToCommentReportData> GetResponseToCommentReport(GridCommand command, int phaseId, int option, out int total, out int pageNum)
        {
            total = 0;
            int numRows = 0;

            pageNum = command.Page;
            numRows = command.PageSize;

            IList<ResponseToCommentReportData> data;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetResponseToCommentReport(phaseId, option, pageNum, numRows, out total).ToList();
            }

            pageNum = command.Page;

            return data;
        }
        #endregion Response To Comment

        /// <summary>
        /// Exports the report in the requested format as a downloadable file
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="id"></param>
        /// <param name="exportType"></param>
        /// <returns></returns>
        public ActionResult ExportReports(int phaseId, int id, string exportType, string option = "1")
        {
            var reportEntities = LookupManager.GetLookup<LookupReport>();
            var reportEntity = reportEntities[id];
            reportEntity.Document = ReportFactory.GetReport(id, phaseId, option);

            RenderingResult result = ReportUtilities.ExportReport(reportEntity.Document, exportType);

            return File(result.DocumentBytes, result.MimeType, result.DocumentName + "." + result.Extension);
        }

       ////code ends

        [NonAction]
        private bool IsEmptyCommand(GridCommand command)
        {
            return (command == null || command.PageSize == 0);
        }

        
        [HasPrivilege(PrivilegeCodes = "RRPT", Scope = Constants.PHASE)]
        public ActionResult ViewReport(int projectId, int phaseId, int id, int artifactTypeId)
        {
            try
            {
                string dmdId;
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    dmdId = phaseManager.Get(phaseId).PhaseArtifacts.Where(x => x.ArtifactTypeId == artifactTypeId).FirstOrDefault().DmdId;
                }

                return RedirectToAction(Constants.DOWNLOAD_DMD, Constants.DOCUMENT, new { id = dmdId });
            }
            catch (Exception ex)
            {
                throw new DomainException(string.Format("The archived {0} report cannot be found in the system.", Utilities.GetResourceString(Constants.PHASE)), ex);
            }
        }

        private string GetViewToString(ControllerContext context, ViewEngineResult result, object model)
        {
            string viewResult = "";
            var viewData = ViewData;
            viewData.Model = model;
            TempDataDictionary tempData = new TempDataDictionary();
            StringBuilder sb = new StringBuilder();

            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter output = new HtmlTextWriter(sw))
                {
                    ViewContext viewContext = new ViewContext(context,
                        result.View, viewData, tempData, output);
                    result.View.Render(viewContext, output);
                }
                viewResult = sb.ToString();
            }
            return viewResult;
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.PROJECT;
        }
        #endregion
    }
}
