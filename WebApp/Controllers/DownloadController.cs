﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

using ICSharpCode.SharpZipLib.Zip;
using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Configuration.Dmd;
using Aquilent.Cara.Configuration;
using Aquilent.Framework.Documents.Web.Mvc;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for downloading unprotetected files
    /// </summary>
    public class DownloadController : AbstractController
    {
        #region Private Members
        private const int SEARCH_RESULTS_PER_PAGE = 25;
		private DmdServiceConfiguration _dmdConfig = ConfigUtilities.GetSection<DmdServiceConfiguration>("dmdServiceConfiguration");
        #endregion

        #region Download custom report csv file
        public S3FileResult CustomReport(string id)
        {
            string customReportFolder;
            using (var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>())
            {
                var setting = resourceManager.Get("resource.customreport.folder");

                if (setting == null)
                {
                    throw new ApplicationException("Missing resource:  resource.customreport.folder");
                }

                customReportFolder = setting.Value;
            }

            return S3FileResult.File(customReportFolder, id, "text/csv");
        }
        #endregion
    }
}
