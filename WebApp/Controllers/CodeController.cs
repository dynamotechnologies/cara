﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Telerik.Web.Mvc.UI;
using System.Text;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for code specific actions
    /// </summary>
    public class CodeController : PageController
    {
        #region Core Action Methods
        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.SYSTEM)]
        public ViewResult List()
        {
            return View(GetViewPath(), new CodingTemplateViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                PageTitle = Utilities.GetPageTitle(ControllerAction)
            });
        }

        [HasPrivilege(PrivilegeCodes = "MPRJ", Scope = Constants.SYSTEM)]
        public PartialViewResult View(int id)
        {
            string templateName = null;

            // separate call to get the template so that template name can be retrieved through the output parameter
            IList<TemplateCodeSelect> list;
            using (var codingTemplateManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
            {
                list = codingTemplateManager.GetTemplateCodes(id, out templateName)
                    .Where(x => x.CodingTemplateId == id).ToList();
            }

            return PartialView("~/Views/Shared/Details/ucCodingTemplateDetails.ascx", new TemplateCodeViewModel
            {
                CodingTemplateId = id,
                TemplateName = templateName,
                TemplateCodes = list,
                Selection = false
            });
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.SYSTEM)]
        public ViewResult Details(int id)
        {
            string templateName = null;

            IList<TemplateCodeSelect> list;
            using (var codingTemplateManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
            {
                list = codingTemplateManager.GetTemplateCodes(id, out templateName)
                    .Where(x => x.CodingTemplateId == id).ToList();
            }

            return View(GetViewPath(), new TemplateCodeViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                CodingTemplateId = id,
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                TemplateName = templateName,
                TemplateCodes = list,
                Selection = false
            });
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.SYSTEM)]
        public ViewResult Input(int id)
        {
            string templateName = null;

            IList<TemplateCodeSelect> list;
            using (var codingTemplateManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
            {
                list = codingTemplateManager.GetTemplateCodes(id, out templateName).ToList();
            }

            return View(GetViewPath(), new TemplateCodeViewModel
            {
                BreadCrumbTitle = Utilities.GetPageTitle(ControllerAction),
                CodingTemplateId = id,
                PageTitle = Utilities.GetPageTitle(ControllerAction),
                TemplateName = templateName,
                TemplateCodes = list,
                Selection = true
            });
        }

        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.SYSTEM)]
        [HttpPost]
        public ActionResult Input(int id, string action, TemplateCodeViewModel model, List<TreeViewItem> TreeView_checkedNodes)
        {
            if (TreeView_checkedNodes == null || TreeView_checkedNodes.Count == 0)
            {
                ModelState.AddModelError(string.Empty, "Please select a code.");
            }

            if (string.IsNullOrEmpty(model.TemplateName))
            {
                ModelState.AddModelError(string.Empty, "Template Name is required.");
            }

            if (ModelState.IsValid)
            {
                if (Constants.SAVE.Equals(action))
                {
                    var sb = new StringBuilder();

                    // create a comma separated list of code ids
                    foreach (TreeViewItem tvi in TreeView_checkedNodes)
                    {
                        sb.Append((string.IsNullOrEmpty(sb.ToString()) ? string.Empty : ",") + tvi.Value);
                    }

                    // save the coding template
                    using (var codingTemplateManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
                    {
                        codingTemplateManager.UpdateCodingTemplate(id, model.TemplateName, Utilities.ToIdListXml(sb.ToString()));
                    }
                }
            }

            string templateName = null;

            // set view data for checked codes
            ViewData["checkedNodes"] = TreeView_checkedNodes;

            // set the model properties
            using (var codingTemplateManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
            {
                model.TemplateCodes = codingTemplateManager.GetTemplateCodes(id, out templateName);
            }

            return View(GetViewPath(), model);
        }
        #endregion

        #region Number Methods
        [HasPrivilege(PrivilegeCodes = "MCDS", Scope = Constants.PHASE)]
        public string GetNextCodeNumber(int phaseId, int codeCategoryId, string selectedCodeNumber)
        {
            return CodeUtilities.GetNextCodeNumber(phaseId, codeCategoryId, selectedCodeNumber);
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.ADMIN;
        }
        #endregion
    }
}
