﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using log4net;
using Aquilent.Cara.WebApp.Models;

namespace Aquilent.Cara.WebApp.Controllers.Abstract
{
    /// <summary>
    /// Abstract controller which serves as the common base of any controllers in the application
    /// </summary>
    public class AbstractController : Controller
    {
        #region Class Members
        /// <summary>
        /// Returns the action name from route data context
        /// </summary>
        protected string Action
        {
            get
            {
                return ControllerContext.RouteData.Values["action"].ToString();
            }
        }

        /// <summary>
        /// Returns the controller name from route data context
        /// </summary>
        protected string Controller
        {
            get
            {
                return ControllerContext.RouteData.Values["controller"].ToString();
            }
        }

        /// <summary>
        /// Returns the controller action name from route data context
        /// </summary>
        protected string ControllerAction
        {
            get
            {
                return (this.Controller + this.Action);
            }
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Return parameter value from the request based on the input key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected string GetParam(string key)
        {
            return (string.IsNullOrEmpty(key) ? string.Empty : Request.Params[key]);
        }

        /// <summary>
        /// Ovverides the OnExcepetion error handling method to log the exception to log4net
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {            
            base.OnException(filterContext);

            // handle the exception   
            if (filterContext.Exception != null)
            {
				string controllerName = (string) filterContext.RouteData.Values["controller"];
				string actionName = (string) filterContext.RouteData.Values["action"];
				
				// set exception handled flag
                filterContext.ExceptionHandled = true;

                // log the exception output
				LogError(filterContext.Exception);

                // redirect to the error page
                HandleErrorInfo model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);

                this.View("~/Views/Error/Error.aspx", model).ExecuteResult(this.ControllerContext);
            }
        }

        protected void SetViewData(string key, object value)
        {
            if (!string.IsNullOrEmpty(key) && value != null)
            {
                ViewData[key] = value;
            }
        }

        /// <summary>
        /// Log the exception occurred in the controller
        /// </summary>
        /// <param name="ex"></param>
        protected void LogError(Exception ex)
        {
            if (ex != null)
            {
                ILog log = LogManager.GetLogger(this.GetType());

                if (log != null && !(ex is System.UnauthorizedAccessException))
                {
                    log.Error(string.Format("Error occurred in controller {0} with action {1}", this.Controller, this.Action), ex);
                }
            }
        }

        /// <summary>
        /// Handle the exception occurred in the controller
        /// </summary>
        /// <param name="ex"></param>
        protected void HandleError(Exception ex)
        {
            if (ex != null)
            {
                // log the exception
                LogError(ex);

                // re-throw the exception
                throw ex;
            }
        }
        #endregion

        #region Override Methods
        /// <summary>
        /// Overrides the OnActionExecuted method to perform add'l actions
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            // set action name
            SetViewData("action", Action);

            // set controller action name
            SetViewData("controllerAction", ControllerAction);
        }
        #endregion
    }
}
