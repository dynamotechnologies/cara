﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using log4net;
using Aquilent.Cara.WebApp.Models;


using System.Web.Routing;

namespace Aquilent.Cara.WebApp.Controllers.Abstract
{
    /// <summary>
    /// Abstract controller which serves as the common base of any page controllers in the application
    /// </summary>
    public class PageController : AbstractController
    {
        #region Protected Methods
        /// <summary>
        /// Returns the current active tab name
        /// </summary>
        /// <returns></returns>
        protected virtual string GetActiveTab()
        {
            return Constants.HOME;
        }

        /// <summary>
        /// Return the view path associated with the action based on the controller
        /// </summary>
        /// <returns></returns>
        protected string GetViewPath()
        {
            return GetViewPath(this.Action);
        }

        /// <summary>
        /// Return the view path based on the controller and the view name
        /// </summary>
        /// <param name="viewName"></param>
        /// <returns></returns>
        protected string GetViewPath(string viewName)
        {
            string path = "~/Views/{0}{1}.aspx";

            if (this is ConcernController)
            {
                path = string.Format(path, "Project/Phase/Concern/", viewName);
            }
            else if (this is CommentController)
            {
                path = string.Format(path, "Project/Phase/Comment/", viewName);
            }
            else if (this is LetterController)
            {
                path = string.Format(path, "Project/Phase/Letter/", viewName);
            }
            else if (this is FormController)
            {
                path = string.Format(path, "Project/Phase/Letter/Form/", viewName);
            }
            else if (this is AuthorController)
            {
                path = string.Format(path, "Project/Phase/Letter/Author/", viewName);
            }
            else if (this is PhaseController)
            {
                path = string.Format(path, "Project/Phase/", viewName);
            }
            else if (this is ProjectController)
            {
                path = string.Format(path, "Project/", viewName);
            }
            else if (this is UserController)
            {
                path = string.Format(path, "Admin/User/", viewName);
            }
            else if (this is CodeController)
            {
                path = string.Format(path, "Admin/Code/", viewName);
            }
            else if (this is TermListController)
            {
                path = string.Format(path, "Admin/TermList/", viewName);
            }
			else if (this is PhaseReportController)
			{
				path = string.Format(path, "Project/Phase/Report/", viewName);
			}
            else if (this is ReadingRoomController)
            {
                path = string.Format(path, "Project/Phase/ReadingRoom/", viewName);
            }
			else if (this is ReportController)
			{
				path = string.Format(path, "Report/", viewName);
			}
			else
            {
                path = string.Format(path, string.Empty, viewName);
            }

            return path;
        }

        protected virtual void SetActiveTab()
        {
            // set active tab view data
            ViewData["ActiveTab"] = GetActiveTab();
        }
        
        /// <summary>
        /// Returns the page number based on the start row and number of rows
        /// </summary>
        /// <param name="startRow"></param>
        /// <param name="numRows"></param>
        /// <returns></returns>
        protected int GetPageTo(int startRow, int numRows = 10)
        {
            return ((startRow / (numRows > 0 ? numRows : 10)) + 1);
        }        
        #endregion

        #region Override Methods
        /// <summary>
        /// Overrides the OnActionExecuted method to perform add'l actions
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            // set active tab
            SetActiveTab();
        }

        /// <summary>
        /// Overrides the OnAuthorization method to perform add'l actions
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);


            var userName = filterContext.HttpContext.User.Identity.Name;

			if (Utilities.GetUserSession() == null)
			{
                Response.Redirect("~/Public/TimeOut?userName=" + userName);
                //throw new UnauthorizedAccessException("Due to security concerns, your session has been closed and you have been logged off due to inactivity");
			}

            // perform basic privilege check on reader role
            if (!Utilities.GetUserSession().HasPrivilege("VPRJ"))
            {
                throw new UnauthorizedAccessException("You are not authorized to view this page");
            }
        }
        #endregion
    }
}
