﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Controllers.Abstract;
using Aquilent.Cara.WebApp.Models;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Controllers
{
    /// <summary>
    /// Controller for term list specific actions
    /// </summary>
    public class TermListController : PageController
    {
        #region Core Action Methods
        public PartialViewResult View(int id)
        {
            TermList termList;
            using (var termListManager = ManagerFactory.CreateInstance<TermListManager>())
            {
                termList = termListManager.Get(id);
                var terms = termList.Terms;
            }

            // get the term list by id
            return PartialView("~/Views/Shared/Details/ucTermListDetails.ascx", termList);
        }
        #endregion

        #region Override Methods
        [NonAction]
        protected override string GetActiveTab()
        {
            return Constants.ADMIN;
        }
        #endregion
    }
}
