﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using System.Configuration;
using Aquilent.Framework.Documents;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Domain.Aws.Sqs;
using Amazon.SQS.Model;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents an author input view Model
    /// </summary>
    public class ObjectionResponseInputViewModel : AbstractViewModel
    {
        #region Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public LetterObjection ObjectionResponse { get; set; }
        private bool hasAttachmentListChanged = false;

        public int MaxUploadSize
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["maxFileUploadSize_Internal"]);
            }
        }


        #region Lookups
        /// <summary>
        /// Returns a select list of countries
        /// </summary>
        public IEnumerable<SelectListItem> LookupSignerTitle
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupObjectionDecisionMaker>().SelectionList, string.Empty, false);
            }
        }
        #endregion
        #endregion

        #region Public Methods
        /// <summary>
        /// Validate the author input with defined buinsess rules
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public virtual void Validate(ModelStateDictionary modelState)
        {
            if (ObjectionResponse != null)
            {
            }
        }

        /// <summary>
        /// Validates document size in the file collection against the configured setting
        /// </summary>
        /// <param name="files"></param>
        /// <param name="modelState"></param>
        public void ValidateDocuments(HttpFileCollectionBase files, ModelStateDictionary modelState)
        {
            if (files != null)
            {
                var totalSize = 0;

                // loop through the file collection and sum up the file sizes
                foreach (string file in files)
                {
                    var hpf = files[file] as HttpPostedFileBase;

                    // add file size (bytes)
                    if (hpf != null && hpf.ContentLength > 0)
                    {
                        totalSize += hpf.ContentLength;
                    }
                }


                if (totalSize > MaxUploadSize)
                {
                    modelState.AddModelError(string.Empty, "Total file upload size cannot excceed " +
                        (MaxUploadSize / 1024000) + " MBs.");
                }
            }
        }

        /// <summary>
        /// Add attachments to the letteras letter documents
        /// </summary>
        /// <param name="files"></param>
        public void AddDocuments(HttpFileCollectionBase files, ModelStateDictionary modelState)
        {
            try
            {
                using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                {
                    var documents = new List<Document>();

                    // validate file size
                    ValidateDocuments(files, modelState);

                    if (modelState.IsValid)
                    {
                        if (ObjectionResponse != null && files != null)
                        {
                            foreach (string file in files)
                            {
                                var hpf = files[file] as HttpPostedFileBase;

                                using (hpf.InputStream)
                                {
                                    // save the letter attachment
                                    if (hpf != null && hpf.ContentLength > 0)
                                    {
                                        var fileAttachment = new Aquilent.Framework.Documents.File(hpf);
                                        fileAttachment.Uploaded = DateTime.UtcNow;
                                        fileAttachment.UploadedBy = Utilities.GetUserSession().Username;

                                        var document = new Document { File = fileAttachment, Name = fileAttachment.OriginalName };

                                        // add to the letter document list
                                        documents.Add(document);

                                        // add to the doc manager
                                        docManager.Add(document);
                                    }
                                }
                            }

                            // save the documents
                            if (documents.Count > 0)
                            {
                                hasAttachmentListChanged = true;
                                docManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueObjectionDocumentsForDmdSend);
                                docManager.UnitOfWork.Save();
                            }
                        }
                    }

                    // add the letter documents with the added documents
                    ObjectionResponse.LetterObjectionDocuments = documents.Select(doc => new LetterObjectionDocument { DocumentId = doc.DocumentId }).ToList();
                }
            }
            catch (InvalidFileTypeException ifte)
            {
                string message = string.Format("This file type is not supported and cannot be uploaded. Please select one of the following supported file types: "
                    + "bmp, doc, docx, gif, htm, html, jpeg, jpg, pdf, png, ppt, pptx, rtf, sgml, tiff, txt, wpd, xls, xlsx, and xml");
                LogError(this.GetType(), message, ifte);

                modelState.AddModelError(string.Empty, message);
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred while adding the {0}.", Utilities.GetResourceString(Constants.DOCUMENT).Pluralize());
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);
            }
        }


        public virtual void Save(IList<int> deletedDocumentIds)
        {
            using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
            {
                using (var objectionInfoManager = ManagerFactory.CreateInstance<LetterObjectionManager>())
                {
                    // Get existing doc IDs
                    var docIds = objectionInfoManager.All
                        .Where(x => x.LetterObjectionId == ObjectionResponse.LetterObjectionId)
                        .SelectMany(x => x.LetterObjectionDocuments).ToList().Select(x => x.DocumentId);

                    objectionInfoManager.UnitOfWork.Context.AttachTo("LetterObjections", ObjectionResponse);
                    objectionInfoManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(ObjectionResponse, System.Data.EntityState.Modified);

                    for (int i = 0; i < ObjectionResponse.LetterObjectionDocuments.Count; i++)
                    {
                        LetterObjectionDocument document = ObjectionResponse.LetterObjectionDocuments.ElementAt(i);
                        objectionInfoManager.UnitOfWork.Context.AttachTo("LetterObjectionDocuments", document);
                        objectionInfoManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(document, docIds.Any(x => x == document.DocumentId) ?
                            System.Data.EntityState.Modified : System.Data.EntityState.Added);

                        if (deletedDocumentIds.Any(x => x == document.DocumentId))
                        {
                            hasAttachmentListChanged = true;
                            objectionInfoManager.DeleteAttachment(ObjectionResponse, document.DocumentId, documentManager);
                            i--;
                        }
                    }

                    documentManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdDelete);

                    if (hasAttachmentListChanged)
                    {
                        // Add to queue
                        string queueName;
                        var sqs = SqsHelper.GetQueue("cara.queue.letterobjection", out queueName);

                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                            {
                                QueueUrl = queueUrl,
                                MessageBody = ObjectionResponse.LetterObjectionId.ToString()
                            };
                        sqs.SendMessage(sendMessageRequest);

                    }
                    objectionInfoManager.UnitOfWork.Save();
                    documentManager.UnitOfWork.Save();
                }
            }
        }
        #endregion
    }
}