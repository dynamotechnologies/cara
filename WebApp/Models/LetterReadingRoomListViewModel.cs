﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter reading room listing view Model
    /// </summary>
    public class LetterReadingRoomListViewModel : AbstractListViewModel
    {
        #region Private Properties
        private Project project = null;
        #endregion

        #region Properties
        public IEnumerable Letters { get; set; }
        public string ProjectNumber { get; set; }
        public int? ProjectId { get; set; }
        public int? PhaseId { get; set; }
        public int? LetterTypeId { get; set; }
        public string OrganizationId { get; set; }
        public string Keyword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? PublishToReadingRoom { get; set; }
        public bool? ShowForms { get; set; }
        public string UpdatedLetterList { get; set; }
        public int SearchResultsPerPage { get; set; }
        public string DmdIdList { get; set; }
        public PageMode Mode { get; set; }
        public string ProjectUrl { get; set; }

        /// <summary>
        /// Returns a CARA project
        /// </summary>
        public Project Project
        {
            get
            {
                if (project == null && !string.IsNullOrEmpty(ProjectNumber))
                {
                    using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                    {
                        project = projectManager.GetByProjectNumber(ProjectNumber);
                        if (project != null)
                        {
                            var phases = project.Phases; // lazy load for later
                        }
                    }
                }

                return project;
            }
        }

        public bool IsPalsProject { get; set; }
     
        //CARA-721 :- Made changes to the PalsProjectUrl return value to use WebCommentPageInfo.ProjectUrl if not null.
        public string PalsProjectUrl
        {
            get
            {
                var uRl = string.Empty;
                if (IsPalsProject)
                {
                    var projectUrl = GetProjectUrl(Convert.ToInt32(ProjectNumber));

                    if (!string.IsNullOrEmpty(projectUrl))
                    {
                        uRl = projectUrl.ToString();
                    }
                    else
                    {
                        uRl = ConfigurationManager.AppSettings["fsPortalProjectHomeUrl"].Replace("[[palsId]]", ProjectNumber);
                    }
                }

                return uRl;
            }
        }

        /// <summary>
        /// Returns true if the PALS project exists in CARA and the reading room is available in at least one of the phases
        /// </summary>
        public bool ReadingRoomAvailable
        {
            get
            {
                return (!string.IsNullOrEmpty(ProjectNumber) && Project != null &&
                    Project.Phases.Where(x => x.ReadingRoomActive == true).FirstOrDefault() != null);
            }
        }

        /// <summary>
        /// Returns a select list of letter types
        /// </summary>
        public IEnumerable<SelectListItem> LookupLetterType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupLetterType>().SelectionList, string.Empty);
            }
        }
        
        /// <summary>
        /// Returns a select list of organizations
        /// </summary>
        public IEnumerable<SelectListItem> LookupOrganization
        {
            get
            {
                IList<KeyValuePair<int, string>> list;
                using (var orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                {
                    list = orgManager.GetSelectList();
                }

                return Utilities.ToSelectList(list, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of unpublished reasons
        /// </summary>
        public IEnumerable<SelectListItem> LookupUnpublishedReason
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupUnpublishedReason>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a list of row per listing page
        /// </summary>
        public SelectList Rows
        {
            get
            {
                return new SelectList(new int[] { 10, 25, 50 });
            }
        }

        /// <summary>
        /// Returns true if it's in public mode
        /// </summary>
        public bool IsPublicMode
        {
            get
            {
                return (PageMode.Public == Mode);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Update letters on letter reading room management page
        /// </summary>
        /// <param name="request">HTTP Request</param>
        /// <returns></returns>
        public IList<string> UpdateLetters(HttpRequestBase request)
        {
			IList<string> messages = new List<string>();

            try
            {
                // get the changed letters
                var letters = GetUpdatedLetters(request.Form.Get("UpdatedLetterList"));

                if (letters != null)
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        // get the letter ids
                        var letterIds = letters.Select(x => x.LetterId).ToArray();
                        // get the letters to be updated from database
                        var lettersToUpdate = letterManager.All.Where(x => x.PhaseId == PhaseId && letterIds.Contains(x.LetterId)).ToList();

                        // process each letter to update with updated values
                        if (lettersToUpdate != null)
                        {
                            foreach (var letter in letters)
                            {
                                // get the letter with updated values
                                var letterToUpdate = lettersToUpdate.Where(x => x.LetterId == letter.LetterId).FirstOrDefault();

                                if (letterToUpdate != null)
                                {
                                    bool published = letter.PublishToReadingRoom;
                                    bool unpublishedReasonMissing = !letter.UnpublishedReasonId.HasValue;
                                    bool unpublishedReasonTypeOther = false;
                                    if (!unpublishedReasonMissing)
                                    {
                                        unpublishedReasonTypeOther = LookupManager.GetLookup<LookupUnpublishedReason>()[letter.UnpublishedReasonId.Value].Name.StartsWith("Other");
                                    }

                                    var unpublishedReasonOtherMissing = string.IsNullOrEmpty(letter.UnpublishedReasonOther);

                                    // It's ok to update the letter if:
                                    //    1. The letter is being published, or
                                    //    2. The letter is being unpublished and the unpublished reason is not missing, or
                                    //    3. The letter is being unpublished and the unpublished reason is not missing and the unpublished reason is not other, or
                                    //    4. The letter is being unpublished and the unpublished reason is not missing and the unpublished reason is other and the reason is not missing
                                    if (!published && unpublishedReasonMissing)
                                    {
                                        messages.Add(string.Format("Letter #{0}: Unpublished Reason cannot be blank when unpublishing a letter", letterToUpdate.LetterSequence));
                                    }
                                    else if (!published && !unpublishedReasonMissing && unpublishedReasonTypeOther && unpublishedReasonOtherMissing)
                                    {
                                        messages.Add(string.Format("Letter #{0}: If Unpublished Reason is Other, the specified reason cannot be blank", letterToUpdate.LetterSequence));
                                    }
                                    else
                                    {
                                        // update the data based on updated values
                                        letterToUpdate.PublishToReadingRoom = letter.PublishToReadingRoom;
                                        letterToUpdate.UnpublishedReasonId = letter.UnpublishedReasonId;
                                        letterToUpdate.UnpublishedReasonOther = letter.UnpublishedReasonOther;
                                    }

                                    if (letterToUpdate.FormSet != null && letterToUpdate.FormSet.MasterFormId == letterToUpdate.LetterId)
                                    {
                                        letterManager.UpdatePublishingStatus(letterToUpdate.FormSet.FormSetId, published, letterToUpdate.UnpublishedReasonId, letterToUpdate.UnpublishedReasonOther);
                                    }
                                }
                            }
                        }

                        // save the work
                        if (messages.Count == 0)
                        {
                            letterManager.UnitOfWork.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var message = string.Format("An error occurred updating the letters from {0}", Utilities.GetResourceString(Constants.READING_ROOM_MGMT));
                LogError(this.GetType(), message, ex);                
            }

			return messages;
        }
        
        /// <summary>
        /// Returns letters from the updated letter list string
        /// </summary>
        /// <param name="updatedLetterList"></param>
        /// <param name="delimiter"></param>
        /// <param name="kvDelimiter"></param>
        /// <returns></returns>
        private ICollection<LetterReadingRoomSearchResult> GetUpdatedLetters(string updatedLetterList, char delimiter = ',', char kvDelimiter = ';')
        {
            ICollection<LetterReadingRoomSearchResult> letters = null;

            if (!string.IsNullOrEmpty(updatedLetterList))
            {
                // parse the letters from the input
                var elements = updatedLetterList.Split(delimiter);

                // select the letters
                letters = elements.Select(x => new LetterReadingRoomSearchResult
                {
                    LetterId = Convert.ToInt32(x.Split(kvDelimiter)[0]),
                    PublishToReadingRoom = Convert.ToBoolean(x.Split(kvDelimiter)[1]),
                    UnpublishedReasonId = !string.IsNullOrEmpty(x.Split(kvDelimiter)[2]) ? Convert.ToInt32(x.Split(kvDelimiter)[2]) : new Nullable<int>(),
                    UnpublishedReasonOther = !string.IsNullOrEmpty(x.Split(kvDelimiter)[3]) ? x.Split(kvDelimiter)[3] : null
                }).ToArray();
            }

            return letters;
        }

        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as LetterReadingRoomSearchResultFilter;

                ProjectId = filter.ProjectId;
                PhaseId = filter.PhaseId;
                LetterTypeId = filter.LetterTypeId;
                FirstName = filter.FirstName;
                LastName = filter.LastName;
                Keyword = filter.Keyword;
                OrganizationId = filter.OrganizationId;
                PublishToReadingRoom = filter.PublishToReadingRoom;
                ShowForms = filter.ShowForms;
                SearchResultsPerPage = filter.NumRows.HasValue ? filter.NumRows.Value : Utilities.GetUserSession().SearchResultsPerPage;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new LetterReadingRoomSearchResultFilter();

            filter.ProjectId = ProjectId;
            filter.PhaseId = PhaseId;
            filter.LetterTypeId = LetterTypeId;
            filter.FirstName = FirstName;
            filter.LastName = LastName;
            filter.Keyword = Keyword;
            filter.OrganizationId = OrganizationId;
            filter.PublishToReadingRoom = PublishToReadingRoom;
            filter.ShowForms = ShowForms;
            filter.NumRows = SearchResultsPerPage > 0 ? SearchResultsPerPage : Utilities.GetUserSession().SearchResultsPerPage;
			filter.PageNum = 1;

            return filter;
        }
        #endregion

        #region Public Methods

        //CARA-721 :- Method to return WebCommentPageInfo.ProjectUrl if not null.
        public string GetProjectUrl(int projectId)
        {
            var url = string.Empty;

            using (var m = ManagerFactory.CreateInstance<WebCommentPageInfoManager>())
            {
                var wcpi = m.All.FirstOrDefault(x => x.ProjectId == project.ProjectId && x.PalsId == projectId);

                if (wcpi != null)
                {
                    var projectUrl = wcpi.ProjectUrl;
                    if (!string.IsNullOrEmpty(projectUrl))
                    {
                        url = projectUrl;
                    }
                }
            }

            return url;
        }
        #endregion
    }
}