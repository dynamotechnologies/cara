﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;



namespace Aquilent.Cara.WebApp.Models
{
    public class PublicCommentFormInputViewModel : AbstractWizardModel
    {
        #region Properties
        private Project project = null;
        public string ActionString { get; set; }
        public string PalsProjectId { get; set; }
        public bool IsPalsProject { get; set; }   
        public int PhaseId { get; set; }        
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        //CR#239 - On multi-district projects contact edit 
        public string ProjectName { get; set; }
        public string ContactName { get; set; }
        public string ContactAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactEmail { get; set; }
        public bool IsContactUpdated { get; set; }

        private string displayedPublicFormText = string.Empty;
        private string displayedReadingRoomText = string.Empty;

        private Phase phase = null;
        private Phase Phase
        {
            get
            {
                if (phase == null)
                {
                    //If project and phase exists retrieve time zone
                    var isExistingProject = ProjectId > 0;
                    var isExistingPhase = PhaseId > 0;
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        if (isExistingProject & isExistingPhase)
                        {
                            phase = phaseManager.Get(PhaseId);
                        }
                    }
                }

                return phase;
            }
        }

        [DataType(DataType.MultilineText)]
        public string PublicFormText
        {
            get
            {
                if (Phase != null)
                {
                    displayedPublicFormText = Phase.PublicFormText;
                }

                //If phase does not exist or if existing phase's public form text is null
                //retrieve default public form text from sysconfig based on PALS status
                if (String.IsNullOrEmpty(displayedPublicFormText))
                {
                    try
                    {
                        using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                        {
                            displayedPublicFormText = IsPalsProject ? caraSettingsManager.Get("cara.sys.publictextpals").Value : caraSettingsManager.Get("cara.sys.publictextnonpals").Value;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PHASE, Utilities.GetResourceString("AppNameAbbv"));
                        LogError(this.GetType(), message, ex);
                    }
                }
                return displayedPublicFormText;
            }
        }

        [DataType(DataType.MultilineText)]
        public string ReadingRoomText
        {
            get
            {
                if (Project != null)
                {
                    displayedReadingRoomText = Project.ReadingRoomText;
                }

                //If phase does not exist or if existing phase's public form text is null
                //retrieve default public form text from sysconfig based on PALS status
                if (String.IsNullOrEmpty(displayedReadingRoomText))
                {
                    try
                    {
                        using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                        {
                            displayedReadingRoomText = IsPalsProject ? caraSettingsManager.Get("cara.sys.readingroomtextpals").Value : caraSettingsManager.Get("cara.sys.readingroomtextnonpals").Value;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PHASE, Utilities.GetResourceString("AppNameAbbv"));
                        LogError(this.GetType(), message, ex);
                    }
                }
                return displayedReadingRoomText;
            }
        }
        #endregion

        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        public void Validate(ModelStateDictionary modelState)
        {
        }

        //CR#239 - On multi-district projects contact edit
        public void GetWebCommentPageInfoContact(bool alwaysUseDatamart = false)
        {          
            
            if (!string.IsNullOrEmpty(PalsProjectId))
            {
                // get the web info from cara
                using (var webInfoManager = ManagerFactory.CreateInstance<WebCommentPageInfoManager>())
                {
                    using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                    {
                        var dataMartManager = new DataMartManager();
                        dataMartManager.WebInfoManager = webInfoManager;
                        dataMartManager.ProjectManager = projectManager;


                        // get the web comment page info from data mart
                        if (IsPalsProject)
                        {
                            WebCommentPageInfo webInfo = null;
                            webInfo = dataMartManager.GetProjectCommentPage(PalsProjectId.ToString());
                            
                            if (webInfo != null)
                            {
                                // set the contact data
                                if (webInfo.ProjectId != null)
                                {
                                    project = projectManager.Get(webInfo.ProjectId.Value);
                                    var currentPhase = project.CurrentPhase.Phase;                               

                                    ProjectName = webInfo.Name;
                                    if (string.IsNullOrWhiteSpace(currentPhase.ContactName) || alwaysUseDatamart)
                                    {
                                        ContactName = webInfo.ContactName;
                                        ContactEmail = webInfo.CommentEmail;
                                        Address1 = webInfo.MailAddressStreet1;
                                        Address2 = webInfo.MailAddressStreet2;
                                        City = webInfo.MailAddressCity;
                                        State = webInfo.MailAddressState;
                                        Zip = webInfo.MailAddressZip;
                                    }                                   
                                    else
                                    {
                                        ContactName = currentPhase.ContactName;
                                        ContactEmail = currentPhase.CommentEmail;
                                        Address1 = currentPhase.MailAddressStreet1;
                                        Address2 = currentPhase.MailAddressStreet2;
                                        City = currentPhase.MailAddressCity;
                                        State = currentPhase.MailAddressState;
                                        Zip = currentPhase.MailAddressZip;
                                    }

                                 }
                               
                            }
                            dataMartManager.Dispose();
                        }

                    }
                }
            }            
        }
    }
}