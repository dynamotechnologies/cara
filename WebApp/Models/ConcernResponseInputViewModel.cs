﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using System.ComponentModel.DataAnnotations;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a concern/response input view Model
    /// </summary>
    public class ConcernResponseInputViewModel : AbstractViewModel
    {
        #region Properties
        public ConcernResponse ConcernResponse { get; set; }
        public bool Locked { get; set; }
        public int CommentId { get; set; }
        public string LetterCommentNumber { get; set; }
        public string AnalysisTypeName { get; set; }
        public IList<Comment> Comments { get; set; }
        public int PhaseId { get; set; }
        public string CodeNumberAndDescription { get; set; }
        public int ProjectId { get; set; }
        public string TrackBackPage { get; set; }
        public int TrackBackLetterId { get; set; }
        public string TrackBackCommentNum { get; set; }
        public int UserId { get; set; }
        public List<SelectListItem> TeamMembers
        {
            get
            {
                if (teamMembers != null)
                {
                    return Utilities.ToSelectList(teamMembers, null, false).ToList();
                }
                else if (PhaseId == null || PhaseId <= 0)
                {
                    return null;
                }
                else
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        teamMembers = new List<KeyValuePair<int, string>>();
                        teamMembers.Add(new KeyValuePair<int, string>(0, "None"));
                        teamMembers.AddRange(
                            phaseManager
                            .Get(PhaseId)
                            .PhaseMemberRoles
                            .OrderBy(x => x.User.FullNameLastFirst)
                            //TODO: take out id in text
                            .Select(x => new KeyValuePair<int, string>(x.UserId, x.User.FullNameLastFirst))
                            .ToList());
                    }
                    return Utilities.ToSelectList(teamMembers, null, false).ToList(); ;
                }
            }
        }

        private List<KeyValuePair<int, string>> teamMembers = null;


        public string ConcernInputNote
        {
            get
            {
                return (AnalysisTypeName != "EIS" ? string.Format("(Writing {0} Statements is optional)", Constants.CONCERN) : string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of concern response statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupConcernResponseStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList, null, false);
            }
        }

        [DataType(DataType.MultilineText)]
        public string ConcernText
        {
            get
            {
                return ConcernResponse.ConcernText;
            }

            set
            {
                ConcernResponse.ConcernText = value;
            }
        }

        [DataType(DataType.MultilineText)]
        public string ResponseText
        {
            get
            {
                return ConcernResponse.ResponseText;
            }

            set
            {
                ConcernResponse.ResponseText = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="action"></param>
        public void Validate(ModelStateDictionary modelState, string action)
        {
            if (ConcernResponse == null)
            {
                modelState.AddModelError(string.Empty, "Concern/Response is missing from the model.");
            }
            else
            {
                // delete action does not require further validations
                if (!Constants.DELETE.Equals(action))
                {
                    if (ConcernResponse.ConcernResponseId == 0)
                    {
                        modelState.AddModelError(string.Empty, "At least one comment is required to create the " + Utilities.GetResourceString("Concern") + ".");
                    }
                    else if ("Response Complete".Equals(action) && string.IsNullOrWhiteSpace(Utilities.RemoveHtmlTags(ConcernResponse.ResponseText)))
                    {
                        modelState.AddModelError(string.Empty, "Response is required to complete the response.");
                    }
                }
            }
        }


        /// <summary>
        /// Create a new concern/response record from the model properties
        /// </summary>
        /// <returns></returns>
        public int Create()
        {
            int concernResponseId = 0;

            using (var concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                if (ConcernResponse != null && CommentId > 0)
                {
                    // add concern response
                    ConcernResponse.LastUpdated = DateTime.UtcNow;
                    ConcernResponse.LastUpdatedBy = Utilities.GetUserSession().Username;
                    ConcernResponse.UserId = Utilities.GetUserSession().UserId;
                    ConcernResponse.DateAssigned = DateTime.UtcNow;

                    concernResponseManager.Add(ConcernResponse);
                    concernResponseManager.UnitOfWork.Save();

                    // set the record id
                    concernResponseId = ConcernResponse.ConcernResponseId;

                    // update comment's concern response id (which will set the concern response sequence)
                    if (concernResponseId > 0)
                    {
                        using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                        {
                            Comment comment = commentManager.Get(CommentId);

                            if (comment != null)
                            {
                                comment.ConcernResponseId = ConcernResponse.ConcernResponseId;
                                if (comment.UserId != null)
                                {
                                    comment.UserId = null;
                                    comment.DateAssigned = DateTime.UtcNow;
                                }
                                commentManager.UnitOfWork.Save();
                            }
                        }
                    }
                }
            }

            return concernResponseId;
        }

        public int SelectedIndex(int? userId)
        {
            //initialize teammembers
            if (teamMembers == null)
            {
                List<SelectListItem> lsli = TeamMembers;
            }
            int intUserId = userId.HasValue ? userId.Value : 0;
            int retVal = 0;
            if (teamMembers.Any(x => x.Key == intUserId))
            {
                KeyValuePair<int, string> item = teamMembers.FirstOrDefault(x => x.Key == intUserId);
                retVal = teamMembers.IndexOf(item);
            }
            if (retVal < 0)
            {
                retVal = 0;
            }
            return retVal;
        }
        #endregion
    }
}