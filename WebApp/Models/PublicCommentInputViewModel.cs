﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a public comment input view Model
    /// </summary>
    public class PublicCommentInputViewModel : LetterInputViewModel
    {
        #region Private Properties
        private Project project = null;
        private Phase currentPhase = null;
        #endregion

        #region Properties
        public string PalsProjectId { get; set; }

        //CARA-721 :- Made changes to the PalsProjectUrl return value to use WebCommentPageInfo.ProjectUrl if not null.
        public string PalsProjectUrl
        {
            get
            {
                var uRl = string.Empty;

                if (!string.IsNullOrEmpty(ProjectUrl))
                {
                    uRl = ProjectUrl.ToString();
                }
                else
                {
                    uRl = ConfigurationManager.AppSettings["fsPortalProjectHomeUrl"].Replace("[[palsId]]", PalsProjectId);
                }
                return uRl;
            }
        }

        public string ProjectNameNumber
        {
            get
            {
                return string.Format("{0} #{1}", ProjectName, PalsProjectId);
            }
        }

        public Project Project
        {
            get
            {
                if (project == null && !string.IsNullOrEmpty(PalsProjectId))
                {
                    using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                    {
                        project = projectManager.GetByProjectNumber(PalsProjectId);
                        if (project != null)
                        {
                            currentPhase = project.CurrentPhase.Phase;
                        }
                    }
                }

                return project;
            }
        }

        public bool ExistInCara
        {
            get
            {
                return (!string.IsNullOrEmpty(PalsProjectId) && Project != null);
            }
        }

        public Phase CurrentPhase
        {
            get
            {
                if (currentPhase == null && Project != null)
                {
                    currentPhase = Project.CurrentPhase.Phase;
                }

                return currentPhase;
            }
        }

        /// <summary>
        /// Returns true if the current comment period has started now
        /// </summary>
        private bool? withinCurrentPhase = new bool?();
        public bool WithinCurrentPhase
        {
            get
            {
                if (!withinCurrentPhase.HasValue)
                {
                    withinCurrentPhase = CurrentPhase.IsWithinCommentPeriod(DateTime.UtcNow, CurrentPhase.TimeZone);
                }

                return withinCurrentPhase.Value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return (CurrentPhase != null ? CurrentPhase.CommentEnd : DateTime.MinValue);
            }
        }

        /// <summary>
        /// Returns true if the current comment period is informal
        /// </summary>
        public bool IsInformal
        {
            get
            {
                return (CurrentPhase != null ? CurrentPhase.IsInformal : false);
            }
        }

        /// <summary>
        /// Returns true if the project comment rule is 215
        /// </summary>
        public bool IsCommentRule215
        {
            get
            {
                return IsCommentRule("215");
            }
        }

        public bool IsPalsProject
        {
            get
            {
                int result;
                return int.TryParse(PalsProjectId, out result);
            }
        }

        /// <summary>
        /// Returns true if the project comment rule is 218
        /// </summary>
        public bool IsCommentRule218
        {
            get
            {
                return IsCommentRule("218");
            }
        }

        public string ProjectName { get; set; }
        public string ContactName { get; set; }
        public string ContactAddress { get; set; }
        public string ContactEmail { get; set; }
        string electronicFormDisclaimer = string.Empty;
        public string ElectronicFormDisclaimer
        {
            get
            {
                try
                {
                    using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                    {
                        electronicFormDisclaimer = caraSettingsManager.Get("cara.sys.publicformdisclaimer").Value;
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PUBLIC_COMMENT_INPUT, Utilities.GetResourceString("AppNameAbbv"));
                    LogError(this.GetType(), message, ex);
                }
                return electronicFormDisclaimer;
            }
        }
        public string NewspaperOfRecord { get; set; }
        public string ProjectUrl { get; set; }
        public string NewspaperOfRecordUrl { get; set; }
        string noNewspaper = string.Empty;
        public string NoNewspaper
        {
            get
            {
                try
                {
                    using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                    {
                        noNewspaper = caraSettingsManager.Get("cara.sys.newspaper").Value;
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PUBLIC_COMMENT_INPUT, Utilities.GetResourceString("AppNameAbbv"));
                    LogError(this.GetType(), message, ex);
                }
                return noNewspaper;
            }
        }
        public string UnitName { get; set; }
        public DmdDocumentViewModel DmdDocumentViewModel
        {
            get
            {
                var model = new DmdDocumentViewModel();

                if (CurrentPhase != null && Project != null)
                {
                    IList<DmdDocument> documents;
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        documents = phaseManager.Get(CurrentPhase.PhaseId).Documents;
                    }

                    model = new DmdDocumentViewModel
                    {
                        Documents = documents,
                        ShowButtons = false
                    };
                }

                return model;
            }
        }

        string displayedPublicFormText = string.Empty;
        public string PublicFormText
        {
            get
            {
                if (Project != null && CurrentPhase != null)
                {
                    displayedPublicFormText = CurrentPhase.PublicFormText;
                }

                //If phase does not exist or if existing phase's public form text is null
                //retrieve default public form text from sysconfig based on PALS status
                if (String.IsNullOrEmpty(displayedPublicFormText))
                {
                    try
                    {
                        using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                        {
                            displayedPublicFormText = IsPalsProject ? caraSettingsManager.Get("cara.sys.publictextpals").Value : caraSettingsManager.Get("cara.sys.publictextnonpals").Value;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PHASE, Utilities.GetResourceString("AppNameAbbv"));
                        LogError(this.GetType(), message, ex);
                    }
                }
                return displayedPublicFormText;
            }
        }

        public string ElectronicFormDisclaimerSp
        {
            get
            {
                try
                {
                    using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                    {
                        electronicFormDisclaimer = caraSettingsManager.Get("cara.sys.publicformdisclaimersp").Value;
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PUBLIC_COMMENT_INPUT, Utilities.GetResourceString("AppNameAbbv"));
                    LogError(this.GetType(), message, ex);
                }
                return electronicFormDisclaimer;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get and create the web comment page info and then assign it to the project if it exists in CARA
        /// </summary>
        /// //CARA-721 :- Made changes to the below method to return value for WebCommentPageInfo.ProjectUrl.
        public void GetWebCommentPageInfo()
        {
            int palsProjectId = 0;

            if (!string.IsNullOrEmpty(PalsProjectId))
            {
                // get the web info from cara
                using (var webInfoManager = ManagerFactory.CreateInstance<WebCommentPageInfoManager>())
                {
                    using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                    {
                        var dataMartManager = new DataMartManager();
                        dataMartManager.WebInfoManager = webInfoManager;
                        dataMartManager.ProjectManager = projectManager;

                        // get the web comment page info from data mart
                        if (IsPalsProject)
                        {
                            WebCommentPageInfo webInfo = null;
                            webInfo = dataMartManager.GetProjectCommentPage(PalsProjectId.ToString());

                            if (webInfo != null)
                            {
                                // set the contact data
                                if (webInfo.ProjectId != null)
                                {
                                    project = projectManager.Get(webInfo.ProjectId.Value);
                                    var currentPhase = project.CurrentPhase.Phase;
                                }

                                ProjectName = webInfo.Name;
                                ContactName = CurrentPhase == null || String.IsNullOrWhiteSpace(CurrentPhase.ContactName) ?
                                    webInfo.ContactName : CurrentPhase.ContactName;
                                ContactAddress = CurrentPhase == null || String.IsNullOrWhiteSpace(GetFullContactAddressFromPhase()) ?
                                    webInfo.FullAddress : GetFullContactAddressFromPhase();
                                NewspaperOfRecord = (string.IsNullOrEmpty(webInfo.NewspaperOfRecord) ? "Newspaper of Record" : webInfo.NewspaperOfRecord);
                                NewspaperOfRecordUrl = webInfo.NewspaperOfRecordUrl;
                                ContactEmail = CurrentPhase == null || String.IsNullOrWhiteSpace(CurrentPhase.CommentEmail) ?
                                    webInfo.CommentEmail : CurrentPhase.CommentEmail;
                                ProjectUrl = webInfo.ProjectUrl;
                                UnitName = webInfo.UnitName;
                            }
                            dataMartManager.Dispose();
                        }
                        // Non-PALS
                        else
                        {
                            ProjectName = Project.Name;

                            User firstPoc = null;

                            using (var npProjectManager = ManagerFactory.CreateInstance<ProjectManager>())
                            {
                                project = npProjectManager.Get(ProjectId);
                                var npCurrentPhase = project.CurrentPhase.Phase;
                                var pointOfContact = npCurrentPhase.PhaseMemberRoles.FirstOrDefault(x => x.IsPointOfContact);
                                firstPoc = pointOfContact != null ? pointOfContact.User : null;
                            }

                            if (firstPoc != null)
                            {
                                ContactName = firstPoc.FullName;
                                ContactEmail = firstPoc.Email;
                            }

                            if (Project.UnitId != null)
                            {
                                UnitName = Project.UnitName;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Returns true if the comment rule key matches the project comment rule
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool IsCommentRule(string name)
        {
            bool isCommentRule = false;
            if (CurrentPhase != null && Project.CommentRuleId.HasValue)
            {
                isCommentRule = LookupManager.GetLookup<LookupCommentRule>()[Project.CommentRuleId.Value].Name.Equals(name);
            }

            return isCommentRule;
        }

        private string GetFullContactAddressFromPhase()
        {
            string retVal = null;
            if (!project.IsPalsProject)
            {
                return retVal;
            }
            retVal = string.Format("{0} {1} {2} {3} {4}",
                CurrentPhase.MailAddressStreet1,
                CurrentPhase.MailAddressStreet2 + (!string.IsNullOrWhiteSpace(CurrentPhase.MailAddressStreet1 + CurrentPhase.MailAddressStreet2) ? "," : string.Empty),
                CurrentPhase.MailAddressCity + (!string.IsNullOrWhiteSpace(CurrentPhase.MailAddressCity) ? "," : string.Empty),
                CurrentPhase.MailAddressState + (!string.IsNullOrWhiteSpace(CurrentPhase.MailAddressState) ? "," : string.Empty),
                CurrentPhase.MailAddressZip).Trim();
            return retVal;
        }
        #endregion
    }
}