﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc.UI;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using System.Text;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase coding structure customization view Model
    /// </summary>
    public class CodeTaskAssignmentViewModel : AbstractWizardModel
    {
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int PhaseTypeId { get; set; }
        public string ActionString { get; set; }
        public string TaskAssignmentContext { get; set; }
        public string TaskAssignmentUsers { get; set; }
        public bool UpdateComments { get; set; }

        /// <summary>
        /// Returns true if a coded comment exists in the phase
        /// </summary>
        public bool HasCodedComments
        {
            get
            {
                bool result = false;

                // determine whether a coded comment exists in the phase
                if (PhaseId > 0)
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        //CARA-924 refactored query for better performance JRhoadhouse 9/14/12
                        //Query was: result = letterManager.All.Where(y => y.PhaseId == PhaseId).Any(y => y.Comments.Count > 0);
                        result = letterManager.All.Any(y => y.PhaseId == PhaseId && y.Comments.Count > 0);

                    }
                }

                return result;
            }
        }

        public List<SelectListItem> TeamMembers
        {
            get
            {
                if (teamMembers != null)
                {
                    return Utilities.ToSelectList(teamMembers, null, false).ToList();
                }
                else if (PhaseId == null || PhaseId <= 0)
                {
                    return null;
                }
                else
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        teamMembers = new List<KeyValuePair<int, string>>();
                        teamMembers.Add(new KeyValuePair<int, string>(0, "None"));
                        teamMembers.AddRange(
                            phaseManager
                            .Get(PhaseId)
                            .PhaseMemberRoles
                            //.Where(x => x.Active)
                            .OrderBy(x => x.User.FullNameLastFirst)
                            .Select(x => new KeyValuePair<int, string>(x.UserId, x.User.FullNameLastFirst))
                            .ToList());
                    }
                    return Utilities.ToSelectList(teamMembers, null, false).ToList();
                }
            }
        }

        public int SelectedIndex(int? userId)
        {
            //initialize teammembers
            if (teamMembers == null)
            {
                List<SelectListItem> lsli = TeamMembers;
            }
            int intUserId = userId.HasValue ? userId.Value : 0;
            int retVal = 0;
            if (teamMembers.Any(x => x.Key == intUserId))
            {
                KeyValuePair<int, string> item = teamMembers.FirstOrDefault(x => x.Key == intUserId);
                retVal = teamMembers.IndexOf(item);
            }
            if (retVal < 0)
            {
                retVal = 0;
            }
            return retVal;
        }

        public string TeamMemberSelectionString
        {
            get
            {
                string retVal = "";
                foreach (SelectListItem item in TeamMembers)
                {
                    retVal = String.Format("{0}<option value=\"{1}\">{2}</option>", retVal, item.Value, item.Text);
                }
                return retVal;
            }
        }

        private List<KeyValuePair<int, string>> teamMembers = null;

        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        public void Validate(ModelStateDictionary modelState)
        {
        }

        public List<PhaseCode> CurrentPhaseCodes
        {
            get
            {
                List<PhaseCode> retVal = new List<PhaseCode>();
                using (PhaseCodeManager phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                {
                    retVal = phaseCodeManager.All.Where(x => x.PhaseId == PhaseId).OrderBy(x => x.CodeNumber).ToList();
                }
                return retVal;
            }
        }
    }
}