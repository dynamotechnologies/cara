﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a Phase details view Model
    /// </summary>
    public class ProjectDetailsViewModel : AbstractViewModel
    {
        #region Properties
        public ICollection<PhaseSearchResult> Phases { get; set; }
        public Project Project { get; set; }
        public IList<Location> Locations { get; set; }
        public string Activities { get; set; }
        public string Purposes { get; set; }
        public string PalsProjectUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["palsProjectHomeUrl"].Replace("[[palsId]]", Project.ProjectDocumentId.ToString());
            }
        }
        public bool CanDelete
        {
            get
            {
                bool result = false;

                if (Utilities.UserHasPrivilege("MPRJ", Constants.SYSTEM))
                {
                    if (Project != null)
                    {
                        bool hasArchivedPhase = false;
                        bool hasLetter = false;

                        using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            // check whether any phase is archived
                            var phases = phaseManager.All.Where(x => x.ProjectId == Project.ProjectId).ToList();

                            hasArchivedPhase = phases.Any(x => x.Locked);

                            // stop if there is an archived phase
                            if (!hasArchivedPhase)
                            {
                                using (var letters = ManagerFactory.CreateInstance<LetterManager>())
                                // check whether any phase contains a letter
                                foreach (Phase phase in phases)
                                {
                                    int phaseId = phase.PhaseId;
                                    if (letters.All.Any(x => x.PhaseId == phaseId))
                                    {
                                        hasLetter = true;
                                        break;
                                    }
                                }
                            }
                        }

                        // the project can be deleted only if both conditions are false
                        result = (!hasArchivedPhase && !hasLetter);
                    }
                }

                return result;
            }
        }
        #endregion
    }
}