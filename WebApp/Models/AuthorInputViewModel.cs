﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using System.ComponentModel;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents an author input view Model
    /// </summary>
    public class AuthorInputViewModel : AbstractViewModel
    {
        #region Properties
        public Author Author { get; set; }
        public string Mode { get; set; }

        [DisplayName("Organization")]
        public string OrganizationName { get; set; }
        public int? OrgTypeId { get; set; }
        public bool Sender { get; set; }
        public int ProjectId { get; set; }

        #region Lookups
        /// <summary>
        /// Returns a select list of organizations
        /// </summary>
        public IEnumerable<SelectListItem> LookupOrganization
        {
            get
            {
                IList<KeyValuePair<int, string>> orgs;
                using (var orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                {
                    orgs = orgManager.GetSelectList();
                }

                return Utilities.ToSelectList(orgs);
            }
        }

        /// <summary>
        /// Returns a select list of contact methods
        /// </summary>
        public IEnumerable<SelectListItem> LookupContactMethod
        {
            get
            {
                return Utilities.ToSelectList(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("", ""),
                        new KeyValuePair<string, string>(null, "Do not contact me in the future"),
                        new KeyValuePair<string, string>("Email", "Contact me via e-mail"),
                        new KeyValuePair<string, string>("Mail", "Contact me via postal mail")
                    });
            }
        }

        /// <summary>
        /// Returns a select list of contact methods in spanish
        /// </summary>
        public IEnumerable<SelectListItem> LookupContactMethodSp
        {
            get
            {
                return Utilities.ToSelectList(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("", ""),
                        new KeyValuePair<string, string>("Do not contact me in the future", "No me entra en contacto en el futuro"),
                        new KeyValuePair<string, string>("Email", "contacto conmigo por e-mail"),
                        new KeyValuePair<string, string>("Mail", "Ponte en contacto conmigo a través de correo postal")
                    });
            }
        }


        /// <summary>
        /// Returns a select list of official representative types
        /// </summary>
        public IEnumerable<SelectListItem> LookupOfficialRepresentativeType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupOfficialRepresentativeType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of states
        /// </summary>
        public IEnumerable<SelectListItem> LookupState
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupState>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of states in spanish
        /// </summary>
        public IEnumerable<SelectListItem> LookupStateSp
        {
            get
            {
                CaraReferenceEntities aa = new CaraReferenceEntities();
                var dt = ((ObjectContext)aa).ExecuteStoreQuery<State>("Select * from StateSp");
                List<SelectListItem> lc = dt.Select(pp => new SelectListItem() { Value = pp.StateId.ToString(), Text = pp.Name }).OrderBy(pp => pp.Text).ToList();

                return lc;
            }
        }

        /// <summary>
        /// Returns a select list of countries
        /// </summary>
        public IEnumerable<SelectListItem> LookupCountry
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupCountry>().SelectionList, null, false);
            }
        }

        /// <summary>
        /// Returns a select list of countries in spanish
        /// </summary>

        public IEnumerable<SelectListItem> LookupCountrySp
        {
            get
            {
                CaraReferenceEntities aa = new CaraReferenceEntities();

                var dt = ((ObjectContext)aa).ExecuteStoreQuery<Country>("Select * from CountrySp");
                List<SelectListItem> lc = dt.Select(pp => new SelectListItem() { Value = pp.CountryId.ToString(), Text = pp.Name }).OrderBy(pp => pp.Text).ToList();
                return lc;
            }

        }
        

        /// <summary>
        /// Returns a select list of organization types
        /// </summary>
        public IEnumerable<SelectListItem> LookupOrganizationType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupOrganizationType>().Where(x => x.ParentOrganizationTypeId.HasValue)
                    .ToLookup(o => o.OrganizationTypeId.ToString(), o => o.Parent.Name + " " + o.Name), string.Empty);
            }
        }
        #endregion
        #endregion

        #region Public Methods
        /// <summary>
        /// Save an author from the internal form
        /// </summary>
        /// <returns></returns>
        public int SaveAuthor(ModelStateDictionary modelState)
        {
            // save the organization
            var organization = SaveOrganization();

            // set the required letter data for creation
            if (Constants.CREATE.Equals(Mode))
            {
                Author.Deleted = false;
                Author.Sender = Sender;
            }

            // update the data
            Author.LastUpdatedBy = (Utilities.GetUserSession() != null ? Utilities.GetUserSession().Username : Author.FullName);
            Author.LastUpdated = DateTime.UtcNow;
            if (organization != null)
            {
                Author.OrganizationId = (organization.OrganizationId > 0 ? organization.OrganizationId : new Nullable<int>());
         
            }
            else
            {
                Author.OrganizationId = new Nullable<int>();
                Author.Organization = null;
            }

            // fix the contact method value
            Author.ContactMethod = Author.ContactMethod.Equals("Do not contact me in the future") ? null : Author.ContactMethod;

            //Trim Author name fields.  CARA-1276
            if (Author.FirstName != null)
            {
                Author.FirstName = Author.FirstName.Trim();
            }

            if (Author.LastName != null)
            {
                Author.LastName = Author.LastName.Trim();
            }

            if (Author.MiddleName != null)
            {
                Author.MiddleName = Author.MiddleName.Trim();
            }

            // save the author
            using (var authorManager = ManagerFactory.CreateInstance<AuthorManager>())
            {
                if (Constants.CREATE.Equals(Mode))
                {
                    authorManager.Add(Author);
                }
                else
                {
                    authorManager.UnitOfWork.Context.AttachTo("Authors", Author);
                    authorManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(Author, System.Data.EntityState.Modified);
                }
                // save the work
                authorManager.UnitOfWork.Save();
            }

            // manage the subscriber through data mart
            ManageSubscriber(Mode, Author);

            // return the author id
            return Author.AuthorId;
        }

        /// <summary>
        /// Save an organization from the model name and return the organization id
        /// </summary>
        /// <param name="isPublic">External or Internal web form</param>
        /// <returns></returns>
        public Organization SaveOrganization(bool isPublic = false)
        {
            Organization org = null;

            // check whether an organization is selected
            if (!string.IsNullOrWhiteSpace(OrganizationName))
            {
                using (var orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                {
                    // create organization if the name doesn't exist
                    org = orgManager.Get(OrganizationName.Trim());

                    if (org == null)
                    {
                        int? orgTypeId = new int?();
                        if (!isPublic)
                        {
                            //CARA-731:-Below line commented as a part of the fix.
                            //orgTypeId = Author.Organization.OrganizationTypeId;
                            if (OrgTypeId.HasValue)
                            {
                                orgTypeId = OrgTypeId;
                            }
                            else
                            {
                                orgTypeId = null;
                            }
                        }
                        org = new Organization { Name = OrganizationName.Trim(), OrganizationTypeId = orgTypeId };

                        orgManager.Add(org);
                    }
                    // update the organization type (for internal forms only)
                    else if (!isPublic)
                    {
                        //CARA-731:-Below line commented as a part of the fix.
                        //org.OrganizationTypeId = Author.Organization.OrganizationTypeId;
                        if (OrgTypeId.HasValue)
                        {
                            org.OrganizationTypeId = OrgTypeId;
                        }
                        else
                        {
                            org.OrganizationTypeId = null;
                        }
                    }
                    //Save the context.
                    orgManager.UnitOfWork.Save();
                }
            }

            return org;
        }

        /// <summary>
        /// Validate the author input with defined buinsess rules
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public virtual void Validate(ModelStateDictionary modelState)
        {
            if (Author != null)
            {
                // email is required if contact method email is selected
                if ("Email".Equals(Author.ContactMethod) && string.IsNullOrWhiteSpace(Author.Email))
                {
                    modelState.AddModelError(string.Empty, "Email List Error Message:  If your preferred contact method is \"Email\" you must provide a valid Email address.");
                }

                //full address is required if both contact method mail is selected and U.S. is selected
                if (("Mail".Equals(Author.ContactMethod) && "United States".Equals(Author.CountryName)) && (string.IsNullOrWhiteSpace(Author.FirstName) || string.IsNullOrWhiteSpace(Author.LastName) || string.IsNullOrWhiteSpace(Author.Address1) ||
                        string.IsNullOrWhiteSpace(Author.City) || string.IsNullOrWhiteSpace(Author.StateId) || string.IsNullOrWhiteSpace(Author.ZipPostal)))
                {
                    modelState.AddModelError(string.Empty, "Mailing List Error Message:  Subscribing to the mailing list requires a full name and address.  Please enter a name and address to be added to the mailing list.");
                }

                //full address except for state is required if contact method mail is selected and U.S. is not selected
                if (("Mail".Equals(Author.ContactMethod) && !"United States".Equals(Author.CountryName)) && (string.IsNullOrWhiteSpace(Author.FirstName) || string.IsNullOrWhiteSpace(Author.LastName) || string.IsNullOrWhiteSpace(Author.Address1) ||
                        string.IsNullOrWhiteSpace(Author.City) || string.IsNullOrWhiteSpace(Author.ZipPostal)))
                {
                    modelState.AddModelError(string.Empty, "Mailing List Error Message:  Subscribing to the mailing list requires a Full Name and Address 1, City and Zip/Postal Code.  Please enter a name and address to be added to the mailing list.");
                }

                // Organization Name is required if Organization Type is selected
                if ((OrgTypeId.HasValue) && (string.IsNullOrWhiteSpace(OrganizationName)))
                {
                    modelState.AddModelError(string.Empty, "Organization Error Message:  \"Organization Name\" is required if an \"Organization Type\" is selected.");
                }

                /*if (string.IsNullOrWhiteSpace(Author.LastName))
                {
                    modelState.AddModelError(string.Empty, "Last Name field is required.");
                }*/
            }
        }

        /// <summary>
        /// Manage subscriber through datamart
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="author"></param>
        public void ManageSubscriber(string mode, Author author)
        {
            // add subscriber if the author wish to be contacted
            if (ProjectId > 0 && author != null)
            {
                Project project;
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    project = projectManager.Get(ProjectId);
                }

                if (project.IsPalsProject)
                {

                    var dataMartManager = new DataMartManager();

                    if (!string.IsNullOrEmpty(author.ContactMethod))
                    {
                        if (Constants.CREATE.Equals(mode))
                        {
                            dataMartManager.AddSubscriber(ProjectId, author);
                        }
                        else if (Constants.EDIT.Equals(mode))
                        {
                            // We don't know if the author needs to be added or updated
                            // First try to add, if that fails, update.
                            if (!dataMartManager.AddSubscriber(ProjectId, author))
                            {
                                dataMartManager.UpdateSubscriber(ProjectId, author);
                            }
                        }
                    }

                    if (Constants.EDIT.Equals(mode) && string.IsNullOrEmpty(author.ContactMethod))
                    {
                        // We don't know if the user was already removed...but we'll try anyway, just in case
                        dataMartManager.DeleteSubscriber(ProjectId, author.AuthorId);
                    }

                    dataMartManager.Dispose();
                }
            }
        }
        #endregion
    }
}