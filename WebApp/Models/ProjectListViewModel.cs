﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a project listing view Model
    /// </summary>
    public class ProjectListViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<ProjectSearchResult> Projects { get; set; }
        public string Keyword { get; set; }
        public string UnitId { get; set; }
        public int? AnalysisTypeId { get; set; }
        public int? ProjectStatusId { get; set; }
        public int? ProjectActivityId { get; set; }
        public string StateId { get; set; }
        public bool MyProjects { get; set; }
        public bool Archived { get; set; }
        public int UserId { get; set; }
        public int? PhaseTypeId { get; set; }

        public IEnumerable<Unit> Units
        {
            get
            {
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();
                return lookupUnit.Where(unit => unit.IsRegion);
            }
        }

        public string UnitName
        {
            get
            {
                string unitName = string.Empty;
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();

                if (!string.IsNullOrEmpty(UnitId))
                {
                    unitName = lookupUnit[UnitId].Name;
                }

                return unitName;
            }
        }

        /// <summary>
        /// Returns a select list of analysis types
        /// </summary>
        public IEnumerable<SelectListItem> LookupAnalysisType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupAnalysisType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of project statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupProjectStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupProjectStatus>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of activities
        /// </summary>
        public IEnumerable<SelectListItem> LookupActivity
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupActivity>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of states
        /// </summary>
        public IEnumerable<SelectListItem> LookupState
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupState>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of comment period types
        /// </summary>
        public IEnumerable<SelectListItem> LookupPhaseType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupPhaseType>().Where(x => x.Active == true)
                   .ToLookup(o => o.PhaseTypeId.ToString(), o => o.Name), string.Empty);
            }
        }

        public string FilterSortKey { get; set; }

        public Action<Telerik.Web.Mvc.UI.Fluent.GridSortDescriptorFactory<ProjectSearchResult>> SortDescriptor
        {
            get
            {
                Action<Telerik.Web.Mvc.UI.Fluent.GridSortDescriptorFactory<ProjectSearchResult>> sortDescriptor;

                string sortKey = "CommentStart";
                string direction = "DESC";
                if (FilterSortKey != null)
                {
                    var splitSortKey = FilterSortKey.Split(' ');

                    if (splitSortKey.Length == 2)
                    {
                        sortKey = splitSortKey[0];
                        direction = splitSortKey[1];
                    }
                }

                if (direction == "ASC")
                {
                    sortDescriptor = sortOrder => sortOrder.Add(sortKey).Ascending();
                }
                else
                {
                    sortDescriptor = sortOrder => sortOrder.Add(sortKey).Descending();
                }

                return sortDescriptor;
            }

        }
        #endregion

        #region Methods
        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as ProjectSearchResultFilter;

                AnalysisTypeId = filter.AnalysisTypeId;
                Keyword = filter.Keyword;
                MyProjects = filter.MyProjects.HasValue ? filter.MyProjects.Value : false;
                Archived = filter.Archived.HasValue ? filter.Archived.Value : false;
                UnitId = filter.UnitId;
                ProjectActivityId = filter.ProjectActivityId;
                ProjectStatusId = filter.ProjectStatusId;
                StateId = filter.StateId;
                FilterSortKey = filter.SortKey;
                PhaseTypeId = filter.PhaseTypeId;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new ProjectSearchResultFilter();

            filter.MyProjects = MyProjects;
            filter.Keyword = Keyword;
            filter.AnalysisTypeId = AnalysisTypeId;
            filter.UnitId = UnitId;
            filter.ProjectActivityId = ProjectActivityId;
            filter.ProjectStatusId = ProjectStatusId;
            filter.StateId = StateId;
            filter.Archived = Archived;
			filter.PageNum = 1;
            filter.UserId = Utilities.GetUserSession().UserId;
            filter.PhaseTypeId = PhaseTypeId;

            return filter;
        }
        #endregion
    }
}