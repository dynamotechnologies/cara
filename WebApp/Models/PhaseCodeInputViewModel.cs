﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase code input view Model
    /// </summary>
    public class PhaseCodeInputViewModel
    {
        #region Private Properties
        private Code selectedCode = null;
        #endregion

        #region Properties
        public int PhaseId { get; set; }
        public int PhaseTypeId { get; set; }
        public int SelectedCodeId { get; set; }
        public PhaseCode PhaseCode { get; set; }
        public bool Child { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns a selected code object
        /// </summary>
        public Code SelectedCode
        {
            get
            {
                if (selectedCode == null && SelectedCodeId > 0)
                {
                    // get the code from phase code if it's a custom code, else get it from the master coding structure
                    Code phaseCode;
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        phaseCode = phaseManager.GetPhaseCodes(
                            new PhaseCodeSelectFilter
                            {
                                PhaseId = PhaseId
                            }).Where(x => x.CodeId == SelectedCodeId).FirstOrDefault().Code;
                    }

                    Code code;
                    using (var codeManager = ManagerFactory.CreateInstance<CodeManager>())
                    {
                        code = codeManager.Get(SelectedCodeId);
                    }

                    selectedCode = SelectedCodeId >= Constants.CUSTOM_CODE_ID_SEQ ? phaseCode : code;
                }

                return selectedCode;
            }
        }

        /// <summary>
        /// Returns a list of code categories associated with the phase type
        /// </summary>
        public IEnumerable<SelectListItem> LookupCodeCategory
        {
            get
            {
                return (PhaseTypeId > 0 ? Utilities.ToSelectList(LookupManager.GetLookup<LookupPhaseType>()[PhaseTypeId].CodeCategories
                    .Select(x => new KeyValuePair<int, string>(x.CodeCategoryId, x.Name)).ToList(), string.Empty) : null);
            }
        }

        /// <summary>
        /// Returns the next available child code number of the selected code or code category
        /// </summary>
        public string NextChildCodeNumber
        {
            get
            {
                string number = string.Empty;

                // get the next available code number using either the code or category, whichever provided
                // in the model
                if (SelectedCodeId > 0 && SelectedCode != null && !string.IsNullOrEmpty(SelectedCode.CodeNumberDisplay))
                {
                    number = CodeUtilities.GetNextChildCodeNumber(PhaseId, SelectedCode);
                }

                return number;
            }
        }
        #endregion
    }
}