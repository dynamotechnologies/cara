﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a concern/response listing view Model
    /// </summary>
    public class ConcernResponseListViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<ConcernResponseSearchResult> ConcernResponses { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public DateTime? DateUpdatedFrom { get; set; }
        public DateTime? DateUpdatedTo { get; set; }
        public int? ConcernResponseStatusId { get; set; }
        public int? ConcernResponseNumber { get; set; }
        public string Keyword { get; set; }
        public bool CanEdit { get; set; }
        public string Mode { get; set; }
        public int? CodeCategoryId { get; set; }
        public string CodeSearchText { get; set; }
        public int? FilterUserId { get; set; }
        public string ActionString { get; set; }
        public string TaskAssignmentContext { get; set; }
        public string TaskAssignmentUsers { get; set; }
        private int? cacheId;
        public int CacheId
        {
            get
            {
                if (!cacheId.HasValue)
                {
                    cacheId = 0;
                }
                return cacheId.Value;
            }
            set
            {
                cacheId = value;
            }
        }

        public string UserDropDownName
        {
            get
            {
                return String.Format("user{0}_", CacheId);
            }
        }

        public List<SelectListItem> TeamMembers
        {
            get
            {
                if (teamMembers != null)
                {
                    return Utilities.ToSelectList(teamMembers, null, false).ToList();
                }
                else if (PhaseId == null || PhaseId <= 0)
                {
                    return null;
                }
                else
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        teamMembers = new List<KeyValuePair<int, string>>();
                        teamMembers.Add(new KeyValuePair<int, string>(0, "None"));
                        teamMembers.AddRange(
                            phaseManager
                            .Get(PhaseId)
                            .PhaseMemberRoles
                            //.Where(x => x.Active)
                            .OrderBy(x => x.User.FullNameLastFirst)
                            .Select(x => new KeyValuePair<int, string>(x.UserId, x.User.FullNameLastFirst))
                            .ToList());
                    }
                    return Utilities.ToSelectList(teamMembers, null, false).ToList(); ;
                }
            }
        }

        private List<KeyValuePair<int, string>> teamMembers = null;

        /// <summary>
        /// Returns a select list of concern response statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupConcernResponseStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList, string.Empty, false);
            }
        }

        /// <summary>
        /// Returns a select list of code categories
        /// </summary>
        public IEnumerable<SelectListItem> LookupCodeCategory
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupCodeCategory>().SelectionList, string.Empty);
            }
        }

        public int SelectedIndex(int contextId, int? userId)
        {
            //first check to see if the contextid is in the list to be saved
            if (!String.IsNullOrWhiteSpace(TaskAssignmentContext) && !String.IsNullOrWhiteSpace(TaskAssignmentUsers))
            {
                string[] arrContextIds = TaskAssignmentContext.Split('|');
                string[] arrUserIds = TaskAssignmentUsers.Split('|');
                for (int i = 0; i < arrContextIds.Count(); i++)
                {
                    if (int.Parse(arrContextIds[i]) == contextId)
                    {
                        userId = int.Parse(arrUserIds[i]);
                    }
                }
            }
            //initialize teammembers
            if (teamMembers == null)
            {
                List<SelectListItem> lsli = TeamMembers;
            }
            int intUserId = userId.HasValue ? userId.Value : 0;
            int retVal = 0;
            if (teamMembers.Any(x => x.Key == intUserId))
            {
                KeyValuePair<int, string> item = teamMembers.FirstOrDefault(x => x.Key == intUserId);
                retVal = teamMembers.IndexOf(item);
            }
            if (retVal < 0)
            {
                retVal = 0;
            }
            return retVal;
        }

        public string TeamMemberSelectionString
        {
            get
            {
                string retVal = "";
                foreach (SelectListItem item in TeamMembers)
                {
                    retVal = String.Format("{0}<option value=\"{1}\">{2}</option>", retVal, item.Value, item.Text);
                }
                return retVal;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as ConcernResponseSearchResultFilter;

                CodeSearchText = (filter.CodeSearch == null ? null : filter.CodeSearch.CodeSearchText);
                PhaseId = filter.PhaseId;
                DateUpdatedFrom = filter.DateUpdatedFrom;
                DateUpdatedTo = filter.DateUpdatedTo;
                ConcernResponseStatusId = filter.ConcernResponseStatusId;
                ConcernResponseNumber = filter.ConcernResponseNumber;
                Keyword = filter.Keyword;
                CodeCategoryId = filter.CodeCategoryId;
                FilterUserId = filter.UserId;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new ConcernResponseSearchResultFilter();
            if (!string.IsNullOrEmpty(CodeSearchText))
            {
                filter.CodeSearch = new CodeSearch(CodeSearchText);
            }

            filter.PhaseId = PhaseId;
            filter.DateUpdatedFrom = DateUpdatedFrom;
            filter.DateUpdatedTo = DateUpdatedTo;
            filter.ConcernResponseStatusId = ConcernResponseStatusId;
            filter.ConcernResponseNumber = ConcernResponseNumber;
            filter.CodeCategoryId = CodeCategoryId;
            filter.Keyword = Keyword;
            filter.UserId = FilterUserId;
			filter.PageNum = 1;

            return filter;
        }
        #endregion
    }
}