﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a term list view Model
    /// </summary>
    public class TermListViewModel : AbstractWizardModel
    {
        #region Properties
        public ICollection<TermList> TermLists { get; set; }
        #endregion
    }
}