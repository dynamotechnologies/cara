﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;

using Telerik.Web.Mvc.UI;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;


namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter view Model
    /// </summary>
    public class LetterViewModel : AbstractViewModel
    {
        #region Private Properties
        private Letter letter = null;
        
        #endregion

        #region Properties
        public int LetterId { get; set; }
        
        /// <summary>
        /// Returns a Letter object by Letter id
        /// </summary>
        public virtual Letter Letter
        {
            get
            {
                if (LetterId > 0 && letter == null)
                {
                    // get the letter
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        letter = letterManager.Get(LetterId);
                    }
                }

                return letter;
            }
        }
     
        #endregion
    }
}