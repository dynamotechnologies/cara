﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc.UI;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using System.Text;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase coding structure customization view Model
    /// </summary>
    public class PhaseCodeViewModel : AbstractWizardModel
    {
        #region Private Properties
        private ICollection<PhaseCodeSelect> phaseCodes;
        #endregion

        #region Public Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int PhaseTypeId { get; set; }
        public string ActionString { get; set; }
        public ICollection<PhaseCodeSelect> PhaseCodes
        {
            get
            {
                if (PhaseId > 0 && phaseCodes == null)
                {
                    // get the phase codes from db
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        phaseCodes = phaseManager.GetPhaseCodes(
                            new PhaseCodeSelectFilter
                            {
                                PhaseId = PhaseId
                            }).ToList();
                    }
                }

                return phaseCodes;
            }
        }

        public void ClearPhaseTreeCache()
        {
            phaseCodes = null;
        }

        public List<int> CheckedNodes { get; set; }
        public string CheckedCodeIdList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (CheckedNodes != null)
                {
                    // create a comma separated list of code ids
                    foreach (int tvi in CheckedNodes)
                    {
                        sb.Append((sb.Length > 0 ? "," : string.Empty) + tvi.ToString());
                    }
                }

                return sb.ToString();
            }
        }

        public string TaskAssignmentContext { get; set; }
        public string TaskAssignmentUsers { get; set; }
        public List<SelectListItem> TeamMembers
        {
            get
            {
                if (teamMembers != null)
                {
                    return Utilities.ToSelectList(teamMembers, null, false).ToList();
                }
                else if (PhaseId == null || PhaseId <= 0)
                {
                    return null;
                }
                else
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        teamMembers = new List<KeyValuePair<int, string>>();
                        teamMembers.Add(new KeyValuePair<int, string>(0, "None"));
                        teamMembers.AddRange(
                            phaseManager
                            .Get(PhaseId)
                            .PhaseMemberRoles
                            .OrderBy(x => x.User.FullNameLastFirst)
                            .Select(x => new KeyValuePair<int, string>(x.UserId, x.User.FullNameLastFirst))
                            .ToList());
                    }
                    return Utilities.ToSelectList(teamMembers, null, false).ToList(); ;
                }
            }
        }

        public int SelectedIndex(int? userId)
        {
            //initialize teammembers
            if (teamMembers == null)
            {
                List<SelectListItem> lsli = TeamMembers;
            }
            int intUserId = userId.HasValue ? userId.Value : 0;
            int retVal = 0;
            if (teamMembers.Any(x => x.Key == intUserId))
            {
                KeyValuePair<int, string> item = teamMembers.FirstOrDefault(x => x.Key == intUserId);
                retVal = teamMembers.IndexOf(item);
            }
            if (retVal < 0)
            {
                retVal = 0;
            }
            return retVal;
        }

        public string TeamMemberSelectionString
        {
            get
            {
                string retVal = "";
                foreach (SelectListItem item in TeamMembers)
                {
                    retVal = String.Format("{0}<option value=\"{1}\">{2}</option>", retVal, item.Value, item.Text);
                }
                return retVal;
            }
        }

        private List<KeyValuePair<int, string>> teamMembers = null;
        #endregion


        #region Methods

        /// <summary>
        /// CARA-717: Returns true if codeCatetoryId = 7 (Early Attention code) - By LD on 11/2/11
        /// </summary>
        public bool CheckCodeCategoryId(int phaseCodeId)
        {
            bool result = true;

            if (phaseCodeId > 0)
            {
                using (var phasecodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                {
                    int codeCat = phasecodeManager.GetCodeCategoryId(phaseCodeId);
                    if (codeCat != LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.Name.Equals("Early Attention")).First().CodeCategoryId)
                    {
                        result = false;
                    }
                }
             }
            return result;
        }

        /// <summary>
        /// Returns true if a coded comment exists in the phase
        /// </summary>
        public bool HasCodedComments
        {
            get
            {
                bool result = false;

                // determine whether a coded comment exists in the phase
                if (PhaseId > 0)
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        //CARA-924 refactored query for better performance JRhoadhouse 9/14/12
                        //Query was: result = letterManager.All.Where(y => y.PhaseId == PhaseId).Any(y => y.Comments.Count > 0);
                        result = letterManager.All.Where(y => y.PhaseId == PhaseId && y.Comments.Count>0).Count() > 0 ;

                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Returns the PhaseCodes in a tree view structure with phase type code categories populated
        /// </summary>
        public ICollection<PhaseCodeSelect> PhaseCodeTree
        {
            get
            {
                return CodeUtilities.ToPhaseCodeTreeByType(PhaseCodes, PhaseTypeId);
            }
        }

        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            // must have at least one code selected
            if (CheckedNodes == null || CheckedNodes.Count == 0)
            {
                modelState.AddModelError(string.Empty, "Please select a code.");

            }

            // ensure no selected codes with the same code number (e.g. one from master coding structure and one from custom code)
            if (modelState.IsValid && PhaseCodes != null)
            {
                // get the checked codes
                List<string> codes = PhaseCodes.Where(x => CheckedNodes.Contains(x.CodeId.HasValue?x.CodeId.Value:-1))
                    .Select(x => x.CodeNumber).ToList();

                // compare the total count and distinct count
                if (codes.Distinct().Count() != codes.Count)
                {
                    StringBuilder codeNumberList = new StringBuilder();

                    // find the offending code numbers
                    var codeNumbers = codes.GroupBy(x => x).Where(y => y.Count() > 1).ToList();

                    foreach (IGrouping<string, string> codeNumber in codeNumbers)
                    {
                        codeNumberList.Append((codeNumberList.Length > 0 ? ", " : string.Empty) + CodeUtilities.ToDisplayCodeNumber(codeNumber.Key));
                    }

                    modelState.AddModelError(string.Empty, string.Format("More than one code with the same number ({0}) was selected. Please deselect one of them.", codeNumberList.ToString()));
                }
            }
        }

        public string GetUserName(int? userId)
        {
            string name = "";
            if (userId.HasValue)
            {
                using (UserManager userManager = ManagerFactory.CreateInstance<UserManager>())
                {
                    name = userManager.Get(userId.Value).FullNameLastFirst;
                }
            }
            return name;
        }
        #endregion
    }
}