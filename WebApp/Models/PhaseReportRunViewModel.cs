﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain.Report;
using Telerik.Reporting;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase report view Model
    /// </summary>
    public class PhaseReportRunViewModel
    {
        #region Properties
		public int PhaseId { get; set; }
		public IReportDocument Report { get; set; }
		public string ReportName { get; set; }
        #endregion
    }
}