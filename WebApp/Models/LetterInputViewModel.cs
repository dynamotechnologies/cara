﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter input view Model
    /// </summary>
    public class LetterInputViewModel : AuthorInputViewModel
    {
        #region Constants
        // max text length of external form
        public const int MAX_TEXT_LENGTH = 10000;
        public const string NO_CONTACT = "Do not contact me in the future"; // CARA-1310
        #endregion

        #region Properties
        public Letter Letter { get; set; }
        public string NextLetterIds { get; set; }
        public int? LettersRemaining { get; set; }
        public int PhaseId { get; set; }

        Phase _phase;
        public Phase Phase
        {
            get
            {
                if (_phase == null)
                {
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        _phase = phaseManager.Get(PhaseId);

                        // lazy load phase properties for the view
                        var project = _phase.Project;
                        var units = _phase.Project.UnitNameList;
                    }
                }

                return _phase;
            }

            set
            {
                _phase = value;
            }
        }        
        public string WithinCommentPeriod { get; set; }
        public string NewText { get; set; }
        public bool Public { get; set; }
        public bool Anonymous { get; set; }
        public bool OriginalTextEditable { get; set; }
        public bool Changed { get; set; }
        public bool RepostToDmd { get; set; }
        public bool WithinCommentPeriodEditable
        {
            get
            {
                return Letter.DateSubmitted < Phase.CommentStart || Letter.DateSubmitted > Phase.CommentEnd;
            }
        }

        public bool CurrentTextNotOriginal
        {
            get
            {
                bool retVal = Letter.Text != Letter.OriginalText;
                //revert functionality is only pertenante to reading room which does not display text formatting
                //if the reading room ever does display formatting the following code will need to be enabled
                /*if (Letter.IsLetterHtml)
                {
                    retVal = Letter.HtmlText != Letter.HtmlOriginalText;
                }*/
                return retVal;
            }
        }

        public int MaxUploadSize
        {
            get
            {
                int maxFileUploadSize = 0;
                if (Public)
                {
                    maxFileUploadSize = int.Parse(ConfigurationManager.AppSettings["maxFileUploadSize"]);
                }
                else
                {
                    maxFileUploadSize = int.Parse(ConfigurationManager.AppSettings["maxFileUploadSize_Internal"]);
                }

                return maxFileUploadSize;
            }
        }

        public DateTime DateSubmittedOriginal { get; set; }

        public bool OrganizationCheck { get; set; }

        #region Lookups
        /// <summary>
        /// Returns a select list of delivery types
        /// </summary>
        public IEnumerable<SelectListItem> LookupDeliveryType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupDeliveryType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of yes/no
        /// </summary>
        public IEnumerable<SelectListItem> LookupYesNo
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupYesNo>().SelectionList);
            }
        }

                /// <summary>
        /// Returns a select list of common interest classes
        /// </summary>
        public IEnumerable<SelectListItem> LookupCommonInterestClass
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupCommonInterestClass>().SelectionList, string.Empty);
            }
        }
        #endregion
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns true if the input date falls within the input phase commend start and end
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool IsDateWithinPhase(DateTime date, string phaseTimeZone)
        {
            var result = false;

            if (date > DateTime.MinValue)
            {
                result = Phase.IsWithinCommentPeriod(date, phaseTimeZone);
            }

            return result;
        }

        /// <summary>
        /// Validate the letter input with defined buinsess rules
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public override void Validate(ModelStateDictionary modelState)
        {
            //Public form does not use html text
            if (Public && (Letter.LetterDocuments == null || Letter.LetterDocuments.Count == 0) && string.IsNullOrWhiteSpace(Letter.Text))
            {
                modelState.AddModelError(string.Empty, "You cannot submit the form that has no comment text and no attachments.");
            }

            if (!string.IsNullOrEmpty(OrganizationName) && OrganizationName.Length > 100)
            {
                modelState.AddModelError(string.Empty, "Organization Name is limited to 100 characters");
            }


            string dnc = LookupContactMethod.FirstOrDefault(x => x.Value == null).Text;
            if (Author != null)
            {
                // internal form validation
                if (!Public)
                {
                    if (Anonymous)
                    { // CARA-1310
                        Author.ContactMethod = NO_CONTACT;
                        if (string.IsNullOrWhiteSpace(Author.LastName)
                            && string.IsNullOrWhiteSpace(Author.FirstName))
                        {
                            Author.LastName = Constants.ANON;
                            Author.FirstName = Constants.ANON;
                        }
                        else
                        {
                            modelState.AddModelError(string.Empty, "Both First name and Last Name fields must be blank if you wish to submit the letter anonymously.");
                        }
                    }
                    else if (string.IsNullOrWhiteSpace(Author.LastName))
                    {
                        Author.LastName = Constants.ANON;
                    }
                }
                //CARA-1469
                if (Public && Author.ContactMethod == dnc && string.IsNullOrWhiteSpace(Author.LastName))
                {
                    if (string.IsNullOrWhiteSpace(Author.FirstName))
                    {
                        Author.FirstName = Constants.ANON;
                    }
                    Author.LastName = Constants.ANON;
                }

                //Trim Author name fields.  CARA-1276
                if (Author.FirstName != null)
                {
                    Author.FirstName = Author.FirstName.Trim();
                }

                if (Author.LastName != null)
                {
                    Author.LastName = Author.LastName.Trim();
                }
                
                if (Author.MiddleName != null)
                {
                    Author.MiddleName = Author.MiddleName.Trim();
                }

                if (Author.Title != null)
                {
                    Author.Title = Author.Title.Trim();
                }

                // validate the author
                base.Validate(modelState);
            }
        }

        /// <summary>
        /// Save a letter
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public int Save(int phaseId, ModelStateDictionary modelState)
        {
            try
            {
                PhaseId = phaseId;
                if (PhaseId > 0)
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        using (var altLetterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            var termIds = new List<int>();
                            var docIds = new List<int>();
                            
                            // set the required letter data for creation
                            if (Constants.CREATE.Equals(Mode))
                            {
                                // save the organization
                                var organization = SaveOrganization(Public);

                                Letter.PhaseId = PhaseId;
                                Letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>()
                                    .Where(x => x.Name.Equals("Not Required")).First().StatusId;
                                Letter.DateEntered = DateTime.UtcNow;
                                Letter.Deleted = false;
                                letterManager.FixLetterText(Letter);
                                if (!Public)
                                {
                                    Letter.Text = LetterManager.ConvertHtmlToPlainText(Letter.HtmlText);
                                    Letter.HtmlOriginalText = Letter.HtmlText;
                                    Letter.HtmlCodedText = letterManager.TagAutoMarkupTerms(Letter, Letter.HtmlText);
                                    Letter.CodedText = LetterManager.ConvertHtmlToPlainText(Letter.HtmlCodedText);

                                    //CR#383 To fix date submitted issue.
                                    //Public input form already has the date in UTC (since it does not need to be displayed to the user
                                    //so this section is only executed if the letter comes from the internal create letter page
                                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                                    {
                                        var phaseToDataSubmitted = phaseManager.Get(PhaseId);

                                        if (!string.IsNullOrEmpty(phaseToDataSubmitted.TimeZone))
                                        {
                                            var tzi = TimeZoneInfo.FindSystemTimeZoneById(phaseToDataSubmitted.TimeZone);
                                            Letter.DateSubmitted = TimeZoneInfo.ConvertTimeToUtc(Letter.DateSubmitted, tzi);
                                            DateSubmittedOriginal = TimeZoneInfo.ConvertTimeToUtc(DateSubmittedOriginal, tzi);
                                        }
                                    }
                                }
                                else
                                {
                                    Letter.CodedText = letterManager.TagAutoMarkupTerms(Letter, Letter.Text);
                                }
                                Letter.OriginalText = Letter.Text;
                                Letter.IsLetterHtml = !Public;

                                #region Create Author
                                // author (Sender) can be added when creating a new letter
                                Author.Deleted = false;
                                Author.Sender = Sender;

                                if (organization != null)
                                {
                                    Author.OrganizationId = (organization.OrganizationId > 0 ? organization.OrganizationId : new Nullable<int>());
                                }
                                else
                                {
                                    // set the organization to null so that EF won't attempt insert an empty organization
                                    Author.OrganizationId = new Nullable<int>();
                                    Author.Organization = null;
                                }

                                // fix the contact method value
                                Author.ContactMethod = Author.ContactMethod.Equals(NO_CONTACT) ? null : Author.ContactMethod;
                                Author.LastUpdatedBy = (Utilities.GetUserSession() != null ? Utilities.GetUserSession().Username : Author.FullName);
                                Author.LastUpdated = DateTime.UtcNow;

                                // set last and first name to anon if anonymous is checked
                                Author.FixAuthorName(Author, Anonymous);

                                // add the author
                                Letter.Authors.Add(Author);
                                #endregion Create Author

                                #region Create Objection Info
                                Phase phase;
                                string unitId;
                                string regulationNumber;
                                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                                {
                                    phase = phaseManager.All.Single(x => x.PhaseId == phaseId);
                                    unitId = Phase.Project.UnitId;
                                    if (Phase.Project.CommentRuleId.HasValue)
                                    {
                                        int regulationId = Phase.Project.CommentRuleId.Value;
                                        regulationNumber = LookupManager.GetLookup<LookupCommentRule>()[regulationId].Name.Contains("218") ? "218" : "219";
                                    }
                                    else
                                    {
                                        regulationNumber = "N/A";
                                    }
                                }

                                if (phase.PhaseTypeId == 3) // Objection
                                {
                                    var now = DateTime.UtcNow;
                                    var thisYear = now.Year.ToString().Substring(2, 2);
                                    var nextYear = (now.Year + 1).ToString().Substring(2, 2);

                                    Letter.LetterObjection = new LetterObjection
                                    {
                                        FiscalYear = now.Month <= 9 ? thisYear : nextYear,
                                        Region = unitId.Substring(2, 2),
                                        Forest = unitId.Substring(4, 2),
                                        ReviewStatusId = 1, // New
                                        OutcomeId = null,
                                        MeetingInfo = null,
                                        RegulationNumber = regulationNumber,
                                        SignerFirstName = null,
                                        SignerLastName = null,
                                        ObjectionDecisionMakerId = null,
                                        ResponseDate = null
                                    };
                                }
                                #endregion Create Objection Info

                            }
                            //else if (Constants.EDIT.Equals(Mode) && !string.IsNullOrEmpty(NewText))
                            else if (Constants.EDIT.Equals(Mode))
                            {
                                letterManager.UnitOfWork.Context.AttachTo("Letters", Letter);
                                
                                if (RepostToDmd)
                                {

                                    letterManager.UnitOfWork.Saved += letterManager.QueueLettersForDmdRepost;                                    

                                }

                                var originalLetter = altLetterManager.Get(Letter.LetterId);
                                docIds = originalLetter.LetterDocuments.Select(x => x.DocumentId).ToList();
                               
                                var terms = originalLetter.LetterTermConfirmations;                                

                                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                                {
                                    var phase = phaseManager.Get(PhaseId);

                                    if (!string.IsNullOrEmpty(phase.TimeZone))
                                    {
                                        var tzi = TimeZoneInfo.FindSystemTimeZoneById(phase.TimeZone);
                                        Letter.DateSubmitted = TimeZoneInfo.ConvertTimeToUtc(Letter.DateSubmitted, tzi);
                                        DateSubmittedOriginal =  TimeZoneInfo.ConvertTimeToUtc(DateSubmittedOriginal, tzi);
                                    }
                                }

                                if (Letter.DateSubmitted.Date.CompareTo(DateSubmittedOriginal.Date) == 0)
                                {
                                    Letter.DateSubmitted = DateSubmittedOriginal;
                                }

                                if (!string.IsNullOrEmpty(NewText) || Changed)
                                {
                                    // select the current term ids from the confirmation (it needs a separate manager because the context cannot track two of the same letter)
                                    termIds = terms.Select(x => x.TermId).ToList();

                                    if (!string.IsNullOrEmpty(NewText))
                                    {
                                        // append new text
                                        Letter.HtmlText += "<br/><br/>" + NewText;
                                        Letter.Text = LetterManager.ConvertHtmlToPlainText(Letter.HtmlText) + "\r\n\r\n" + LetterManager.ConvertHtmlToPlainText(NewText);

                                        // append the new coded text
                                        Letter.HtmlCodedText += "<br/><br/>" + letterManager.TagAutoMarkupTerms(Letter, NewText);
                                        Letter.CodedText += "\r\n\r\n" + LetterManager.ConvertHtmlToPlainText(letterManager.TagAutoMarkupTerms(Letter, NewText));
                                        letterManager.FixLetterText(Letter);
                                    }
                                    else //if no new text then the existing text is being edited.
                                    {
                                        //CARA-996 early action status not updated when editing letter text.
                                        Letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>()
                                            .Where(x => x.Name.Equals("Not Required")).First().StatusId;
                                        Letter.HtmlCodedText = letterManager.TagAutoMarkupTerms(Letter, Letter.HtmlText);
                                        Letter.CodedText = LetterManager.ConvertHtmlToPlainText(Letter.HtmlCodedText);
                                        Letter.Text = LetterManager.ConvertHtmlToPlainText(Letter.HtmlText);
                                        letterManager.FixLetterText(Letter);
                                    }

                                    // detach the Phase to remove the entity checker when saving the data
                                    Letter.Phase = null;

                                    // set the letter status to be "in-progress" if it's marked as complete or new
                                    if (!Letter.LetterStatusName.Equals(Utilities.GetResourceString("InProgress")) && !Letter.LetterStatusName.Equals(Utilities.GetResourceString("NotNeeded")))
                                    {
                                        Letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name == Utilities.GetResourceString("InProgress")).FirstOrDefault().LetterStatusId;
                                        LetterUtilities.UpdateStatus(Letter.LetterId, Constants.IN_PROGRESS);
                                    }

                                    //CARA-1001
                                    List<Term> naTermList = terms.Where(x => x.StatusId == 1).Select(x => x.Term).ToList();
                                    foreach (Term naTerm in naTermList)
                                    {
                                        Letter.CodedText = LetterUtilities.RemoveAutoMarkupFromText(Letter.CodedText, naTerm);
                                    }
                                }
                                else
                                {
                                    Letter.Text = LetterManager.ConvertHtmlToPlainText(Letter.HtmlText);
                                }
                            }

                            // update the letter data
                            if (WithinCommentPeriodEditable)
                            {
                                Letter.WithinCommentPeriod = "Yes".Equals(WithinCommentPeriod);
                            }
                            else
                            {
                                Letter.WithinCommentPeriod = Phase.IsWithinCommentPeriod(Letter.DateSubmitted, Phase.TimeZone);
                            }

                            Letter.LastUpdatedBy = (Utilities.GetUserSession() != null ? Utilities.GetUserSession().Username : Author.FullName);
                            Letter.LastUpdated = DateTime.UtcNow;

                            // save the letter
                            if (Constants.CREATE.Equals(Mode))
                            {
                                letterManager.Add(Letter);
                            }
                            else
                            {
                                //CR#379 CARA would require a code change to resend the attachments to DMD 
                                var originalLetter = altLetterManager.Get(Letter.LetterId);
                                originalLetter.LetterDocuments
                                    .Where(oldDocument => !Letter.LetterDocuments
                                        .Any(newDocument => newDocument.DocumentId == oldDocument.DocumentId))
                                    .ToList()
                                    .ForEach(oldDocument =>
                                    {
                                        Letter.LetterDocuments.Add(oldDocument);
                                        altLetterManager.UnitOfWork.Context.Detach(oldDocument);

                                    });



                                foreach (LetterDocument ltrDocument in Letter.LetterDocuments)
                                {
                                    letterManager.UnitOfWork.Context.AttachTo("LetterDocuments", ltrDocument);
                                    // if found a new attachment in the list add to the LetterDocuments
                                    ltrDocument.Protected = false;
                                    letterManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(ltrDocument, docIds.Where(x => x == ltrDocument.DocumentId).Count() > 0 ?
                                        System.Data.EntityState.Modified : System.Data.EntityState.Added);
                                }

                                // reconcile any added or deleted letter term confirmations. - EN:- CARA-849
                                LetterTermConfirmation ltc;
                                if (string.IsNullOrEmpty(NewText))
                                {
                                    // Find existing and deleted terms
                                    foreach (var termId in termIds)
                                    {
                                        ltc = Letter.LetterTermConfirmations.FirstOrDefault(x => x.TermId == termId);

                                        // A letter term from the original letter is not found
                                        if (ltc == null)
                                        {
                                            ltc = new LetterTermConfirmation
                                            {
                                                LetterId = Letter.LetterId,
                                                TermId = termId
                                            };

                                            letterManager.UnitOfWork.Context.AttachTo("LetterTermConfirmations", ltc);
                                            letterManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(ltc, System.Data.EntityState.Deleted);
                                        }
                                        else
                                        {
                                            letterManager.UnitOfWork.Context.AttachTo("LetterTermConfirmations", ltc);
                                            letterManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(ltc, System.Data.EntityState.Unchanged);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var termId in termIds)
                                    {
                                        ltc = Letter.LetterTermConfirmations.FirstOrDefault(x => x.TermId == termId);

                                        // A letter term from the original letter is also in the appended text.
                                        // it needs to be removed from the new version of the letter, so the
                                        // same auto-markup term isn't added twice.
                                        if (ltc != null)
                                        {
                                            Letter.LetterTermConfirmations.Remove(ltc);
                                        }
                                    }
                                }
                                
                                letterManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(Letter, System.Data.EntityState.Modified);
                            }

                            letterManager.UnitOfWork.Save();

                            // The LetterSequence is populated by an insert trigger and not returned when
                            // the letter is added.  We call Refresh on the Context to get
                            // the LetterSequence column.
                            letterManager.UnitOfWork.Context.Refresh(RefreshMode.StoreWins, Letter);

                            // This is dependent on the selections on the My Profile page being mutltiples of 10
                            if (Constants.CREATE.Equals(Mode))
                            {
                                if (Letter.LetterSequence.HasValue)
                                {
                                    // Send the new letter notifications in a separate thread
                                    Thread thread = new Thread(d =>
                                        NotificationManager.SendNewLettersArrivedNotification(
                                            Phase,
                                            new List<int?> { Letter.LetterSequence })
                                        );
                                    thread.Start();
                                }
                            }
                            else
                            {
                                // Update Early Attention Status if the letter text changed
                                if (!string.IsNullOrEmpty(NewText) || Changed)
                                {
                                    // Unfortunately, we need to create a completely separate letter manager instance
                                    using (var freshLetterManager = ManagerFactory.CreateInstance<LetterManager>())
                                    {
                                        var letter = freshLetterManager.Get(Letter.LetterId);
                                        var lkuLetterType = LookupManager.GetLookup<LookupLetterType>();
                                        var letterType = lkuLetterType[letter.LetterTypeId];
                                        letter.EarlyActionStatusId = letterManager.TransitionEarlyActionStatusByLetterType(letterType, letterType, letter);
                                        freshLetterManager.UnitOfWork.Save();
                                    }
                                }
                            }

                            try
                            {
                                // manage subscriber
                                ManageSubscriber(Mode, Letter.Authors.FirstOrDefault());
                            }
                            catch (Exception ex)
                            {
                                string message = string.Format("An error occurred while sending the {0} information to {1}.", Constants.AUTHOR, Utilities.GetResourceString("PALS"));
                                LogError(this.GetType(), message, ex);

                                modelState.AddModelError(string.Empty, message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred while saving the {0}.", Constants.LETTER);
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);
            }

            // return the letter id
            return Letter.LetterId;
        }

        /// <summary>
        /// Add attachments to the letteras letter documents
        /// </summary>
        /// <param name="files"></param>
        public void AddDocuments(HttpFileCollectionBase files, ModelStateDictionary modelState)
        {
            try
            {
                using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                {
                    var documents = new List<Document>();

                    // validate file size
                    ValidateDocuments(files, modelState);

                    if (modelState.IsValid)
                    {
                        if (Letter != null && files != null)
                        {
                            foreach (string file in files)
                            {
                                var hpf = files[file] as HttpPostedFileBase;

                                using (hpf.InputStream)
                                {
                                    // save the letter attachment
                                    if (hpf != null && hpf.ContentLength > 0)
                                    {
                                        var fileAttachment = new Aquilent.Framework.Documents.File(hpf);
                                        fileAttachment.Uploaded = DateTime.UtcNow;
                                        fileAttachment.UploadedBy = (Utilities.GetUserSession() != null ? Utilities.GetUserSession().Username : Author.FullName);

                                        var document = new Document { File = fileAttachment, Name = fileAttachment.OriginalName };

                                        // add to the letter document list
                                        documents.Add(document);

                                        // add to the doc manager
                                        docManager.Add(document);
                                    }
                                }
                            }

                            // save the documents
                            if (documents.Count > 0)
                            {
                                docManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdSend);
                                docManager.UnitOfWork.Save();
                            }
                        }
                    }

                    // add the letter documents with the added documents
                    //CARA-714:- Add/Delete letter attachments - For Internal Webforms set the Protected Flag to 'FALSE' when Mode='Create/Edit'
                    Letter.LetterDocuments = documents.Select(doc => new LetterDocument { DocumentId = doc.DocumentId, Protected = Letter.Protected }).ToList();
                }
            }
            catch (InvalidFileTypeException ifte)
            {
                string message = string.Format("This file type is not supported and cannot be uploaded. Please select one of the following supported file types: "
                    + "bmp, doc, docx, gif, htm, html, jpeg, jpg, pdf, png, ppt, pptx, rtf, sgml, tiff, txt, wpd, xls, xlsx, and xml");
                LogError(this.GetType(), message, ifte);

                modelState.AddModelError(string.Empty, message);
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred while adding the {0}.", Utilities.GetResourceString(Constants.DOCUMENT).Pluralize());
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);
            }
        }

        /// <summary>
        /// Validates document size in the file collection against the configured setting
        /// </summary>
        /// <param name="files"></param>
        /// <param name="modelState"></param>
        public void ValidateDocuments(HttpFileCollectionBase files, ModelStateDictionary modelState)
        {
            if (files != null)
            {
                var totalSize = 0;

                // loop through the file collection and sum up the file sizes
                foreach (string file in files)
                {
                    var hpf = files[file] as HttpPostedFileBase;

                    // add file size (bytes)
                    if (hpf != null && hpf.ContentLength > 0)
                    {
                        totalSize += hpf.ContentLength;
                    }
                }


                if (totalSize > MaxUploadSize)
                {
                    modelState.AddModelError(string.Empty, "Total file upload size cannot excceed " +
                        (MaxUploadSize / 1024000) + " MBs.");
                }
            }
        }
        #endregion
    }
}