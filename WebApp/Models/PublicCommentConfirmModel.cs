﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a public comment input view Model
    /// </summary>
    public class PublicCommentConfirmModel
    {
        #region Properties
        public string ProjectNumber { get; set;}
        public string ProjectName { get; set; }
        public string LetterReferenceNumber { get; set; }
        public bool IsPalsProject { get; set; }
        public bool IsObjection { get; set; }
        public string ContactName { get; set; }
        public string ContactAddress { get; set; }
        public string ContactEmail { get; set; }
        public string UnitName { get; set; }

        public string ProjectUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["fsPortalProjectHomeUrl"].Replace("[[palsId]]", ProjectNumber);
            }
        }
        #endregion
    }
}