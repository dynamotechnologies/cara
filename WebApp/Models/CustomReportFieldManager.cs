﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Aquilent.Cara.Domain;

using log4net;

using Aquilent.Cara.Configuration;
using Aquilent.Cara.WebApp.Models.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    public class CustomReportFieldManager
    {
        public static List<CustomReportFieldElement> GetList(string type)
        {
            CustomReportFieldConfiguration section = ConfigUtilities.GetSection<CustomReportFieldConfiguration>("CustomReportFieldConfiguration");
            return section.CustomReportfields.Cast<CustomReportFieldElement>().Where
                (x=> x.FieldType == type || x.FieldType == CustomReportFieldType.All).ToList();
        }
        public static CustomReportFieldElement GetById(int id)
        {
            CustomReportFieldConfiguration section = ConfigUtilities.GetSection<CustomReportFieldConfiguration>("CustomReportFieldConfiguration");
            return section.CustomReportfields.Cast<CustomReportFieldElement>().Where(x => x.Id == id).FirstOrDefault();
        }
        public static CustomReportFieldElement GetByName(string name)
        {
            CustomReportFieldConfiguration section = ConfigUtilities.GetSection<CustomReportFieldConfiguration>("CustomReportFieldConfiguration");
            return section.CustomReportfields.Cast<CustomReportFieldElement>().Where(x => x.Name == name).FirstOrDefault();
        }
        public static List<CustomReportColumnElement> GetColumnList(int groupId = -1)
        {
            CustomReportFieldConfiguration section = ConfigUtilities.GetSection<CustomReportFieldConfiguration>("CustomReportFieldConfiguration");
            return section.CustomReportColumns.Cast<CustomReportColumnElement>().Where(x => x.GroupId == groupId).OrderBy(x=>x.Name).ToList();
        }
        public static CustomReportColumnElement GetColumnById(int id)
        {
            CustomReportFieldConfiguration section = ConfigUtilities.GetSection<CustomReportFieldConfiguration>("CustomReportFieldConfiguration");
            return section.CustomReportColumns.Cast<CustomReportColumnElement>().Where(x => x.Id == id).FirstOrDefault();
        }
        public static string DbProvider
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CaraCustomReportConnection"].ProviderName;
            }
        }
        public static string DbConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CaraCustomReportConnection"].ConnectionString;
            }
        }

        public static class FieldDataTypes
        {
            public const string Dropdown = "dropdown";
            public const string Text = "string";
            public const string Date = "date";
            public const string Number = "number";
            public const string Unit = "treedropdown"; 
        }
    }
}