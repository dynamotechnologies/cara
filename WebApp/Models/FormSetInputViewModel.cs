﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a form set input view Model
    /// </summary>
    public class FormSetInputViewModel : AbstractInputViewModel
    {
        #region Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public FormSet FormSet { get; set; }
        #endregion
    }
}