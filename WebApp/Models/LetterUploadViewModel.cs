﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

using ICSharpCode.SharpZipLib.Zip;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.Cara.Reference;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Independentsoft.Pst;
using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter upload view Model
    /// </summary>
    public class LetterUploadViewModel : AbstractViewModel
    {
        #region Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public HttpPostedFileBase File { get; set; }
        public ICollection<Letter> Letters { get; set; }
        public List<LetterUploadQueue> QueueFiles { get; set; }

        private List<KeyValuePair<int, string>> userNames;
        #endregion

        #region Methods
        /// <summary>
        /// Perform validations against the input posted file base
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="hpf"></param>
        public LetterUploadFileType? ValidatePostedFileBase(ModelStateDictionary modelState, HttpPostedFileBase hpf)
        {
            if (hpf == null || string.IsNullOrEmpty(hpf.FileName))
            {
                modelState.AddModelError(string.Empty, "A file must be selected.");
                return null;
            }
            if (hpf.ContentLength == 0)
            {
                modelState.AddModelError(string.Empty, "The uploaded file was empty.");
                return null;
            }
            // CARA-1318
            if (IsDuplicateUpload(hpf)) {
                modelState.AddModelError(string.Empty, "A file of the same name and size has already been uploaded.");
                return null;
            }
            // End CARA-1318
            if (hpf.FileName.ToLower().EndsWith(".pst"))
            {
                return LetterUploadFileType.PST;
            }
            if (hpf.FileName.ToLower().EndsWith(".zip"))
            {
                return LetterUploadFileType.FDMS;
            }
            modelState.AddModelError(string.Empty, "The uploaded file must be a zip file or a pst file.");
            return null;
        }

        // CARA-1318 : Detect potential duplicates
        public bool IsDuplicateUpload(HttpPostedFileBase hpf)
        {
            var result = false;

            int size = hpf.ContentLength;
            string name = hpf.FileName;

            using (LetterUploadQueueManager luqm = ManagerFactory.CreateInstance<LetterUploadQueueManager>())
            {
                QueueFiles = luqm.GetByPhase(PhaseId).ToList();
                // Iterate over QueueFiles to match filename and size
                // if match set result = true and break
                foreach (LetterUploadQueue e in QueueFiles)
                {
                    if (e.Filename.Equals(name) && e.FileSize == size) {
                        result = true;
                        break;
                    } 
                }
            }
            return result;
        }

        public void PopulateFiles()
        {
            using (LetterUploadQueueManager luqm = ManagerFactory.CreateInstance<LetterUploadQueueManager>())
            {
                QueueFiles = luqm.GetByPhase(PhaseId).ToList();
                List<int> userIds = QueueFiles.Select(x => x.UserId).ToList();
                userNames = new List<KeyValuePair<int, string>>();
                using (UserManager um = ManagerFactory.CreateInstance<UserManager>())
                {
                    um.All.Where(user => userIds.Contains(user.UserId)).ToList().ForEach(user =>
                        userNames.Add(new KeyValuePair<int, string>(user.UserId, string.Format("{0}, {1}", user.LastName, user.FirstName))));
                }
            }
        }


        public string GetUserNameById(int userId)
        {
            return userNames.FirstOrDefault(x => x.Key == userId).Value;
        }

        #endregion
    }
}