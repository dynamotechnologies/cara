﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a comment listing view Model
    /// </summary>
    public class CommentListViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<CommentSearchResult> Comments { get; set; }
        public ICollection<CommentDetails> CommentDetails { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public string LetterSequence { get; set; }
        public string CommentNumber { get; set; }
        public string CodeSearchText { get; set; }
        public int? CodeCategoryId { get; set; }
        public string Keyword { get; set; }
        public string Association { get; set; }
        public bool SampleStatement { get; set; }
        public bool NoResponseRequired { get; set; }
        public int? NoResponseReasonId { get; set; }
        public bool Annotation { get; set; }
        public bool CanEdit { get; set; }
        public string CommentIdList { get; set; }
        public int? FilterUserId { get; set; }
        public string TaskAssignmentContext { get; set; }
        public string TaskAssignmentUsers { get; set; }
        public string ActionString { get; set; }
        public List<SelectListItem> TeamMembers
        {
            get
            {
                if (teamMembers != null)
                {
                    return Utilities.ToSelectList(teamMembers, null, false).ToList();
                }
                else if (PhaseId == null || PhaseId <= 0)
                {
                    return null;
                }
                else
                {
                    using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        teamMembers = new List<KeyValuePair<int, string>>();
                        teamMembers.Add(new KeyValuePair<int, string>(0, "None"));
                        teamMembers.AddRange(
                            phaseManager
                            .Get(PhaseId)
                            .PhaseMemberRoles
                            //.Where(x=>x.Active)
                            .OrderBy(x => x.User.FullNameLastFirst)
                            //TODO: take out id in text
                            .Select(x => new KeyValuePair<int, string>(x.UserId, x.User.FullNameLastFirst))
                            .ToList());
                    }
                    return Utilities.ToSelectList(teamMembers, null, false).ToList(); ;
                }
            }
        }

        public int SelectedIndex(int contextId, int? userId)
        {
            //first check to see if the contextid is in the list to be saved
            if (!String.IsNullOrWhiteSpace(TaskAssignmentContext) && !String.IsNullOrWhiteSpace(TaskAssignmentUsers))
            {
                string[] arrContextIds = TaskAssignmentContext.Split('|');
                string[] arrUserIds = TaskAssignmentUsers.Split('|');
                for (int i = 0; i < arrContextIds.Count(); i++)
                {
                    if (int.Parse(arrContextIds[i]) == contextId)
                    {
                        userId = int.Parse(arrUserIds[i]);
                    }
                }
            }
            //initialize teammembers
            if (teamMembers == null)
            {
                List<SelectListItem> lsli = TeamMembers;
            }
            int intUserId = userId.HasValue ? userId.Value : 0;
            int retVal = 0;
            if (teamMembers.Any(x => x.Key == intUserId))
            {
                KeyValuePair<int, string> item = teamMembers.FirstOrDefault(x => x.Key == intUserId);
                retVal = teamMembers.IndexOf(item);
            }
            if (retVal < 0)
            {
                retVal = 0;
            }
            return retVal;
        }

        public string TeamMemberSelectionString
        {
            get
            {
                string retVal = "";
                foreach (SelectListItem item in TeamMembers)
                {
                    retVal = String.Format("{0}<option value=\"{1}\">{2}</option>", retVal, item.Value, item.Text);
                }
                return retVal;
            }
        }

        private List<KeyValuePair<int,string>> teamMembers = null;
        

        /// <summary>
        /// Returns a select list of yes/no
        /// </summary>
        public SelectList LookupYesNo
        {
            get
            {
                return new SelectList(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("", ""),
                        new KeyValuePair<string, string>("Yes", "Show Comments assigned to a CR"),
                        new KeyValuePair<string, string>("No", "Show Comments not assigned to a CR"),
                    }, "Key", "Value", ""
                );
            }
        }

        /// <summary>
        /// Returns a select list of code categories
        /// </summary>
        public IEnumerable<SelectListItem> LookupCodeCategory
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupCodeCategory>().SelectionList, string.Empty);
            }
        }


        /// <summary>
        /// Returns a select list of no response reasons
        /// </summary>
        public IEnumerable<SelectListItem> LookupNoResponseReason
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupNoResponseReason>().SelectionList, string.Empty);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as CommentSearchResultFilter;

                PhaseId = filter.PhaseId;
                CodeSearchText = (filter.CodeSearch == null ? null : filter.CodeSearch.CodeSearchText);
                CodeCategoryId = filter.CodeCategoryId;
                Keyword = filter.Keyword;
                Association = filter.Association.HasValue ? filter.Association.Value.ConvertToYesNo() : null;
                SampleStatement = filter.SampleStatement.HasValue ? filter.SampleStatement.Value : false;
                NoResponseRequired = filter.NoResponseRequired.HasValue ? filter.NoResponseRequired.Value : false;
                NoResponseReasonId = filter.NoResponseReasonId;
                Annotation = filter.Annotation.HasValue ? filter.Annotation.Value : false;
                LetterSequence = filter.LetterSequence;
                CommentNumber = filter.CommentNumber;
                FilterUserId = filter.UserId;

                if (!string.IsNullOrEmpty(filter.CommentNumber))
                {
                    LetterSequence = filter.LetterSequence + "-" + filter.CommentNumber;
                }
                else
                {
                    LetterSequence = filter.LetterSequence;
                }
                CommentNumber = filter.CommentNumber;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new CommentSearchResultFilter();

            filter.PhaseId = PhaseId;
            if (!string.IsNullOrEmpty(CodeSearchText))
            {
                filter.CodeSearch = new CodeSearch(CodeSearchText);
            }
            filter.CodeCategoryId = CodeCategoryId;
            filter.Keyword = Keyword;
            filter.Association = ("Yes".Equals(Association) ? true : ("No".Equals(Association) ? false : new Nullable<bool>()));
            filter.SampleStatement = SampleStatement ? true : new Nullable<bool>();
            filter.NoResponseRequired = NoResponseRequired ? true : new Nullable<bool>();
            filter.NoResponseReasonId = NoResponseReasonId;
            filter.Annotation = Annotation ? true : new Nullable<bool>();
            filter.UserId = FilterUserId == 0 ? null : FilterUserId;
            filter.PageNum = 1;

            if (!string.IsNullOrEmpty(LetterSequence))
            {
                var letterCommentNumber = LetterSequence.Split('-');

                filter.LetterSequence = letterCommentNumber[0].Trim();
                if (letterCommentNumber.Length > 1)
                {
                    filter.CommentNumber = letterCommentNumber[1].Trim();
                }
            }
            else
            {
                filter.LetterSequence = LetterSequence;
                filter.CommentNumber = CommentNumber;
            }

            return filter;
        }

        public List<SelectListItem> TeamMembersSelected(int? userId)
        {
            List<SelectListItem> retVal = TeamMembers;
            string value = userId.HasValue ? userId.Value.ToString() : "0";
            foreach (SelectListItem sli in retVal)
            {
                sli.Selected = (sli.Value == value);
            }
            return retVal;
        }


        #endregion
    }
}