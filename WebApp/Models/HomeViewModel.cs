﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a Home View Model
    /// </summary>
    public class HomeViewModel : AbstractViewModel
    {
        public ProjectListViewModel ProjectListViewModel { get; set; }
        public TaskListViewModel TaskListViewModel { get; set; }
        public TaskListViewModel ReviewableLettersViewModel { get; set; }
        public CaraSetting WelcomeMessage
        {
            get
            {
                CaraSetting welcomeMsg;
                using (var caraSettingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                {
                    welcomeMsg = caraSettingsManager.Get("cara.sys.welcomemessage");
                }

                return welcomeMsg;
            }
        }
    }
}