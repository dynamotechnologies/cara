﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.FormDetection;


using System.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a form set listing view Model
    /// </summary>
    public class FormSetListViewModel : AbstractListViewModel
    {

        private static DateTime DownloadAllLettersDate;
        static FormSetListViewModel()
        {
            string downloadAllLettersDateString = ConfigurationManager.AppSettings["DownloadAllLettersDate"];
            bool success = false;
            try
            {
                success = DateTime.TryParse(downloadAllLettersDateString, out DownloadAllLettersDate);
            }
            finally
            {
                if (!success)
                {
                    DownloadAllLettersDate = new DateTime(2014, 2, 8);
                }
            }
        }

        #region Properties
        public IEnumerable FormSets { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int? FormSetId { get; set; }
        public int? LetterTypeId { get; set; }
        public int? LetterStatusId { get; set; }
        public bool CanEdit { get; set; }
        public string RequestUri { get; set; }
        public int Count { get; set; }

        public Phase Phase { get; set; }


        public bool CanDownloadLetters
        {
            get
            {
                return Phase.CommentStart >= DownloadAllLettersDate;
            }
        }

        /// <summary>
        /// Returns a select list of letter types
        /// </summary>
        public IEnumerable<SelectListItem> LookupLetterType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupLetterType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of letter statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupLetterStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupLetterStatus>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of form sets
        /// </summary>
        public IEnumerable<SelectListItem> LookupFormSet
        {
            get
            {
                // get the form sets for the phase
                IList<KeyValuePair<int, string>> selectList;
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var formSets = formSetManager.All.Where(x => x.PhaseId == PhaseId);
                    var tempList = formSets.Select(fs => new { Key = fs.FormSetId, Value = fs.Name });

                    selectList = new List<KeyValuePair<int, string>>(tempList.Count());
                    foreach (var kvp in tempList)
                    {
                        selectList.Add(new KeyValuePair<int, string>(kvp.Key, kvp.Value));
                    }
                }

                return Utilities.ToSelectList(selectList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of form sets
        /// </summary>
        public IEnumerable<SelectListItem> LookupFormSetMasterId
        {
            get
            {
                // get the form sets for the phase
                IList<KeyValuePair<int, string>> selectList;
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var formSets = formSetManager.All.Where(x => x.PhaseId == PhaseId);
                    var tempList = formSets.Select(fs => new { Key = fs.MasterFormId, Value = fs.Name });

                    selectList = new List<KeyValuePair<int, string>>(tempList.Count());
                    foreach (var kvp in tempList)
                    {
                        selectList.Add(new KeyValuePair<int, string>(kvp.Key, kvp.Value));
                    }
                }

                return Utilities.ToSelectList(selectList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of form sets and the unique form set
        /// </summary>
        public IEnumerable<SelectListItem> LookupFormSetAndUnique
        {
            get
            {
                // get the form sets
                var lookup = LookupFormSet.ToList();
                // add unique form set
                var unique = new SelectListItem { Value = "-1", Text = "Unique" };
                lookup.Insert(1, unique);

                return lookup.AsEnumerable();
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns a select list of letter actions
        /// </summary>
        public IEnumerable<SelectListItem> LookupLetterAction(FormSetLetterSearchResult formSet, int letterCount)
        {
            var list = new List<KeyValuePair<string, string>>();

            // set variables
            var formSetId = formSet.FormSetId;
            var isMasterForm = formSet.MasterFormId == formSetId;

            
            var letterTypeName = formSet.LetterType;

            // add this option if it's a form set and 1. not a master form or 2. is a master form and the last letter in the form set
            if (!isMasterForm || letterCount <= 1)
            {
                switch (letterTypeName)
                {
                    case "Unique":
                        list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                        list.Add(new KeyValuePair<string, string>("Form", "Form"));
                        list.Add(new KeyValuePair<string, string>("Form Plus", "Form Plus"));
                        break;
                    case "Form":
                        list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                        list.Add(new KeyValuePair<string, string>("Form Plus", "Form Plus"));
                        break;
                    case "Form Plus":
                        list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                        list.Add(new KeyValuePair<string, string>("Form", "Form"));
                        break;
                    case "Master Form":
                        list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                        break;
                    case "Duplicate":
                        list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                        break;
                }                
            }

            // add this option if it's not a master
            if (!isMasterForm && !letterTypeName.Equals("Master Form") && !letterTypeName.Equals("Duplicate"))
            {
                list.Add(new KeyValuePair<string, string>("Master", "Make " + Utilities.GetResourceString("MasterForm")));
            }

            list.Add(new KeyValuePair<string, string>(Constants.SEND, string.Format("{0} to Another {1}", Constants.SEND, Utilities.GetResourceString(Constants.FORM_SET))));

            // add this option if the form set has more than 1 letter
            if (letterCount > 1)
            {
                list.Add(new KeyValuePair<string, string>(Constants.CREATE, string.Format("{0} {1} {2}", Constants.CREATE, Constants.NEW, Utilities.GetResourceString(Constants.FORM_SET))));
            }

            return Utilities.ToSelectList(list, string.Empty, false);
        }

        /// <summary>
        /// Returns the Confidence Grade title for the confidence
        /// </summary>
        /// <param name="Confidence"></param>
        /// <returns></returns>
        public string LookupConfidenceGrade(double? confidence)
        {
            string retVal = "N/A";
            if (!confidence.HasValue)
            {
                return retVal;
            }
            ConfidenceGrade confidenceGrade = ConfigUtilities
                .GetSection<FormDetectionConfiguration>("formDetectionConfiguration")
                .ConfidenceGrades.FirstOrDefault(x => x.MinConfidence <= confidence.Value && x.MaxConfidence > confidence.Value);
            if (confidenceGrade != null)
            {
                retVal = confidenceGrade.GradeName;
            }
            return retVal;
        }

        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as FormSetSearchResultFilter;

                PhaseId = filter.PhaseId;
                FormSetId = filter.FormSetId;
                LetterTypeId = filter.LetterTypeId;                
                LetterStatusId = filter.LetterStatusId;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new FormSetSearchResultFilter();

            filter.PhaseId = PhaseId;
            filter.FormSetId = FormSetId;
            filter.LetterTypeId = LetterTypeId;
            filter.LetterStatusId = LetterStatusId;
			filter.PageNum = 1;

            return filter;
        }
        #endregion
    }
}