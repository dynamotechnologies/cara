﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.EntityClient;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Report;
using System.Data.Common;
using System.Data;
using Telerik.Web.Mvc.UI;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Report.DataModel;

using Aquilent.Cara.WebApp.Models.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    public class CustomReportMetaData
    {
        public CustomReportParameters parameters;

        public CustomReportMetaData()
        {
            BaseConstructor(new List<int>());
        }

        public CustomReportMetaData(List<int> collapsedListIds)
        {
            BaseConstructor(collapsedListIds);
        }

        private void BaseConstructor(List<int> collapsedListIds)
        {
            CustomReportFilter2 filter = (CustomReportFilter2)Utilities.GetFilter(Constants.CUSTOM_REPORT,string.Empty);
            if (filter == null)
            {
                parameters = new CustomReportParameters() { CollapsedListIds = collapsedListIds };
                SetDefaultColumns();
                filter = new CustomReportFilter2 { ParameterXML = parameters.GetXml(), ReportUserId = Utilities.GetUserSession().UserId, PageNum = 1 };
                Utilities.SetFilter(Constants.CUSTOM_REPORT, string.Empty, filter);
            }
            else
            { parameters = new CustomReportParameters(filter.ParameterXML) { CollapsedListIds = collapsedListIds }; }
        }


        public void Clear()
        {
            parameters.Clear();
            SetDefaultColumns();
            CustomReportFilter2 filter = new CustomReportFilter2 { ParameterXML = parameters.GetXml(), ReportUserId = Utilities.GetUserSession().UserId, PageNum = 1 };
            Utilities.SetFilter(Constants.CUSTOM_REPORT, string.Empty, filter);
        }


        public bool Add(CustomReportFieldElement field, bool not, string value, string name = null)   
        {
            bool retVal = parameters.Add(field.Id, field.DataType, field.Name, field.FieldType, not, value, name);
            SaveParameters();
            return retVal;
        }
        public bool Add(CustomReportFieldElement field, bool not, DateTime to, DateTime from)
        {
            bool retVal = parameters.Add(field.Id, field.DataType, field.Name, field.FieldType, not, to, from);
            SaveParameters();
            return retVal;
        }
        public void AddColumn(CustomReportColumnElement column)
        {
            parameters.AddColumn(column.Name, column.Id);
            SaveParameters();
        }
        public void Remove(int listId)
        {
            parameters.Remove(listId);
            SaveParameters();
        }
        public void Remove(List<int> listIds)
        {
            parameters.Remove(listIds);
            SaveParameters();
        }
        public void MoveColumns(List<int> listIds, bool direction)
        {
            parameters.MoveColumns(listIds, direction);
            SaveParameters();
        }
        public void SetDefaultSortOrder()
        {
            List<KeyValuePair<int, bool>> defaultSortColumns = new List<KeyValuePair<int, bool>>();
            List<CustomReportColumnDataItem> unsortedColumns =
                parameters.ColumnDataItems.Where(x => x.SortOrder < 0).OrderBy(x => x.ListId).ToList();
            List<CustomReportColumnDataItem> sortedColumns =
                parameters.ColumnDataItems.Where(x => x.SortOrder > 0).OrderBy(x => x.SortOrder).ToList();
            foreach (CustomReportColumnDataItem column in sortedColumns)
            { defaultSortColumns.Add(new KeyValuePair<int, bool>(column.ListId, column.Ascending)); }
            foreach (CustomReportColumnDataItem column in unsortedColumns)
            {
                if (defaultSortColumns.Count >= 3)
                { break; }
                defaultSortColumns.Add(new KeyValuePair<int, bool>(column.ListId, column.Ascending));
            }
            parameters.SetSortOrder(defaultSortColumns);
            SaveParameters();
        }
        public void SetSortOrder(List<KeyValuePair<int, bool>> sortOrder)
        {
            parameters.SetSortOrder(sortOrder);
            SaveParameters();
        }

        public object GetParameterDisplay()
        {
            return null;
            /*
            List<object> columns = (List<object>)parameters.ColumnDataItems.Select(x => new
            {
                listId = x.ListId,
                name = x.ColumnName
            }).ToList();
            return (object)new
            {
                projectFields = FieldDisplay(CustomReportFieldType.Project),
                commenterFields = FieldDisplay(CustomReportFieldType.Commenter),
                letterFields = FieldDisplay(CustomReportFieldType.Letter),
                columns = columns
            };*/
        }

        /// <summary>
        /// This method is deprecated
        /// </summary>
        /// <returns></returns>
        public List<int> GetCollapsedListIds()
        { return parameters.CollapsedListIds; }

        /// <summary>
        /// This method is deprecated
        /// </summary>
        /// <returns></returns>
        public List<TreeViewItem> ParameterTreeDisplay()
        {
            List<TreeViewItem> retVal = new List<TreeViewItem>();
            TreeViewItem searchParameters = new TreeViewItem();
            searchParameters.Text = "Search Parameters";
            searchParameters.Value = "-1";
            searchParameters.Checkable = false;
            searchParameters.Expanded = !parameters.CollapsedListIds.Contains(-1);
            searchParameters.Items.Add(GetFieldTypeTreeItem(CustomReportFieldType.Project));
            searchParameters.Items.Add(GetFieldTypeTreeItem(CustomReportFieldType.Commenter));
            searchParameters.Items.Add(GetFieldTypeTreeItem(CustomReportFieldType.Letter));
            retVal.Add(searchParameters);
            TreeViewItem reportPresentation = new TreeViewItem();
            reportPresentation.Text = "Report Presentation";
            reportPresentation.Value = "-5";
            reportPresentation.Checkable = false;
            reportPresentation.Expanded = !parameters.CollapsedListIds.Contains(-5);
            reportPresentation.Items.Add(GetColumnTreeItem());
            reportPresentation.Items.Add(GetSortTreeItem());
            retVal.Add(reportPresentation);
            return retVal;
        }

        public List<KeyValuePair<int, string>> ColumnDisplay()
        {
            List<KeyValuePair<int, string>> retVal = new List<KeyValuePair<int, string>>();
            foreach (CustomReportColumnDataItem column in parameters.ColumnDataItems.OrderBy(x=>x.ListId))
            { retVal.Add(new KeyValuePair<int, string>(column.ListId, column.ColumnName)); }
            return retVal;
        }
        public List<CustomReportData> GetReportResults(int pageNum, int numRows, out int total)
        {
            string provider = CustomReportFieldManager.DbProvider;
            string constring = CustomReportFieldManager.DbConnectionString;
            int userId = Utilities.GetUserSession().UserId;
            List<CustomReportData> retVal;
            using (CustomReportManager reportManager = new Domain.Report.CustomReportManager())
            {
                reportManager.Open(provider, constring);
                retVal = reportManager.RunReport(parameters.GetXml(true), out total, userId, pageNum, numRows);
                reportManager.Close();
            }
            return retVal;
        }

        private void SaveParameters()
        {
            CustomReportFilter2 filter = (CustomReportFilter2)Utilities.GetFilter(Constants.CUSTOM_REPORT, string.Empty);
            filter.ParameterXML = parameters.GetXml();
            Utilities.SetFilter(Constants.CUSTOM_REPORT, string.Empty, filter);
        }

        /*private List<object> FieldDisplay(string customReportFieldType)
        {
            return (List<object>)parameters.DataItems
                   .Where(field => field.FieldType == customReportFieldType)
                   .Select(field => new
                   {
                       listId = field.ListId,
                       name = field.FieldName,
                       includeValues = field.Values
                           .Where(value => !value.Not)
                           .Select(value => new
                           {
                               listId = value.ListId,
                               name = value.Name
                           })
                           .ToList(),
                       excludeValues = field.Values
                           .Where(value => value.Not)
                           .Select(value => new
                           {
                               listId = value.ListId,
                               name = value.Name
                           })
                           .ToList()
                   }).ToList();
        }*/

        private TreeViewItem GetFieldTypeTreeItem(string fieldType)
        {
            int listId = CustomReportFieldType.GetListIdforFieldType(fieldType);
            TreeViewItem retVal = new TreeViewItem();
            retVal.Text = fieldType;
            retVal.Value = listId.ToString();
            retVal.Checkable = false;
            retVal.Expanded = !parameters.CollapsedListIds.Contains(listId);
            List<CustomReportDataItem> relevantDataItems = parameters.DataItems
                    .Where(x => x.FieldType == fieldType || (x.FieldType == CustomReportFieldType.CommentPeriod && fieldType == CustomReportFieldType.Project))
                    .OrderBy(x => x.FieldName)
                    .ToList();
            if (relevantDataItems.Count() == 0)
            { retVal.Text += " (All)"; }
            foreach (CustomReportDataItem dataItem in relevantDataItems)
            {
                TreeViewItem treeItemField = new TreeViewItem();
                treeItemField.Text = dataItem.FieldName;
                treeItemField.Value = dataItem.ListId.ToString();
                treeItemField.Checkable = false;
                treeItemField.Expanded = !parameters.CollapsedListIds.Contains(dataItem.ListId);
                foreach (CustomReportValueItem valueItem in dataItem.Values)
                {
                    TreeViewItem treeItemValue = new TreeViewItem();
                    #region construct value text
                    string text = "";
                    if (valueItem.Not)
                    { text = "excluding "; }
                    switch (dataItem.FieldDataType)
                    {
                        case "date":
                            if (valueItem.DateFromValue == DateTime.MinValue)
                            { text = String.Format("{0}Before {1}", text, valueItem.DateToValue.ToShortDateString()); }
                            else if (valueItem.DateToValue == DateTime.MaxValue)
                            { text = String.Format("{0}After {1}", text, valueItem.DateFromValue.ToShortDateString()); }
                            else
                            {
                                text = String.Format("{0}Between {1} and {2}",
                                    text,
                                    valueItem.DateFromValue.ToShortDateString(),
                                    valueItem.DateToValue.ToShortDateString());
                            }
                            break;
                        case "dropdown":
                        case "treedropdown":
                            text += valueItem.Name;
                            break;
                        case "string":
                        case "number":
                            text += valueItem.Value;
                            break;
                        default:
                            text += valueItem.Value;
                            break;
                    }
                    #endregion
                    treeItemValue.Text = text;
                    treeItemValue.Value = valueItem.ListId.ToString();
                    treeItemValue.Checkable = false;
                    treeItemField.Items.Add(treeItemValue);
                }
                retVal.Items.Add(treeItemField);
            }
            return retVal;
        }

        private TreeViewItem GetColumnTreeItem()
        {
            TreeViewItem retVal = new TreeViewItem();
            retVal.Checkable = false;
            retVal.Expanded = !parameters.CollapsedListIds.Contains(-6);
            retVal.Value = "-6";
            if (parameters.ColumnDataItems.Count == 0)
            {
                retVal.Text = "Columns (no entries)";
                return retVal;
            }
            retVal.Text = "Columns";
            foreach (CustomReportColumnDataItem colDataItem in parameters.ColumnDataItems)
            {
                TreeViewItem treeItemCol = new TreeViewItem();
                treeItemCol.Text = colDataItem.ColumnName;
                treeItemCol.Value = colDataItem.ListId.ToString();
                treeItemCol.Checkable = false;
                retVal.Items.Add(treeItemCol);
            }
            return retVal;
        }

        private TreeViewItem GetSortTreeItem()
        {
            TreeViewItem retVal = new TreeViewItem();
            retVal.Checkable = false;
            retVal.Expanded = !parameters.CollapsedListIds.Contains(-7);
            retVal.Value = "-7";
            List<CustomReportColumnDataItem> sortItems = 
                parameters.ColumnDataItems.Where(x => x.SortOrder > -1).OrderBy(x => x.SortOrder).ToList();
            retVal.Text = "Sort Sequence";
            if (sortItems.Count == 0)
            {
                retVal.Text += " (no entries)";
                return retVal;
            }
            foreach (CustomReportColumnDataItem colDataItem in sortItems)
            {
                TreeViewItem treeSortItem = new TreeViewItem();
                treeSortItem.Text = String.Format("{0} ({1})", colDataItem.ColumnName, colDataItem.Ascending ? "ASC" : "DESC");
                treeSortItem.Value = "-1";
                treeSortItem.Checkable = false;
                retVal.Items.Add(treeSortItem);
            }
            return retVal;
        }

        private void SetDefaultColumns()
        {
            new List<int>(new[] { 9, 17, 37, 36, 30, 55, 56, 63 })
                .ForEach(x => parameters.AddColumn(CustomReportFieldManager.GetColumnById(x).Name, x));
        }



    }
}