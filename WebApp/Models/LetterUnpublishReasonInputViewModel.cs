﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter unpublish reason input view Model
    /// </summary>
    public class LetterUnpublishReasonInputViewModel : AbstractInputViewModel
    {
        #region Private Properties
        private Letter letter = null;
        #endregion

        #region Properties
        public int LetterId { get; set; }
        public int? UnpublishedReasonId { get; set; }
        public string UnpublishedReasonOther { get; set; }
        public Letter Letter
        {
            get
            {
                if (letter == null && LetterId > 0)
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        letter = letterManager.Get(LetterId);
                    }
                }

                return letter;
            }
        }

        /// <summary>
        /// Returns a select list of unpublished reasons
        /// </summary>
        public IEnumerable<SelectListItem> LookupUnpublishedReason
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupUnpublishedReason>().SelectionList, string.Empty);
            }
        }
        #endregion
    }
}