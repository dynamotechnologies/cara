﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a user view Model
    /// </summary>
    public class UserViewModel
    {
        #region Properties
        public ICollection<User> Users { get; set; }
        public string UserId { get; set; }
        #endregion
    }
}