﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents an author list view Model
    /// </summary>
    public class AuthorListViewModel
    {
        #region Properties
        public ICollection<Author> Authors { get; set; }
        public string Mode { get; set; }
        #endregion
    }
}