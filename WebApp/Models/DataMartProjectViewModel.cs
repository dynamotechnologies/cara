﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a data mart project view Model
    /// </summary>
    public class DataMartProjectViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<DataMartProject> Projects { get; set; }
        public string ProjectNameOrId { get; set; }
        public string ProjectId { get; set; }
        public string DataMartProjectId { get; set; }

        private bool _isObjection = false;
        public bool IsObjection
        {
            get { return _isObjection; }
            set { _isObjection = value; }
        }
        
        /// <summary>
        /// PALS project status ids (1: Developing Proposal, 2: In Progress, 3: On Hold)
        /// </summary>
        public List<int> StatusIds
        {
            get
            {
                return new List<int> { 1, 2, 3, 4, 5 };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            if (string.IsNullOrEmpty(ProjectNameOrId))
            {
                //modelState.AddModelError(string.Empty, "Project search criteria is required.");
                modelState.AddModelError(string.Empty, "PALS ID or Project Name is required.");
            }
        }

        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as DataMartProjectFilter;

                ProjectNameOrId = filter.ProjectId;
                ProjectNameOrId = filter.PartialName;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new DataMartProjectFilter();

            // parse project name or id value
            try
            {
                Convert.ToInt32(ProjectNameOrId);
                filter.ProjectId = ProjectNameOrId;
            }
            catch
            {
                filter.PartialName = ProjectNameOrId;
            }

            // add status ids
            foreach (int id in StatusIds)
            {
                filter.StatusIds.Add(id);
            }

            // check whether the user has privilege to see all units
            var currentUser = Utilities.GetUserSession();
            var ignoreUnit = currentUser.HasPrivilege("PNLU");
            if (!ignoreUnit)
            {
                filter.UnitCodes = currentUser.UnitPrivileges
                    .Where(x => x.Key == "MPRJ")
                    .SelectMany(x => x.Value)
                    .Distinct()
                    .ToList();
            }

            return filter;
        }


        /// <summary>
        /// Returns a list of records from the list view model
        /// </summary>
        /// <param name="filter">DataMart Project Filter (Use model properties if not provided)</param>
        /// <returns></returns>
        public DataMartProjectSearchResults GetListRecords(ModelStateDictionary modelState, DataMartProjectFilter filter = null)
        {
            var results = new DataMartProjectSearchResults();

            // set up the default filter from the model if not provided in the parameter
            if (filter == null)
            {
                filter = GetFilterFromModel() as DataMartProjectFilter;

                // set filter paging info
                if (filter != null)
                {
                    filter.StartRow = 0;
                    filter.NumRows = 10;
                }
            }

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.ProjectManager = projectManager;

                // search for projects based on filtering
                try
                {
                    results = dmManager.FindProjects(filter);

                    if (results != null)
                    {
                        Projects = results.Projects.ToList();

                        if (filter.IsObjection)
                        {
                            foreach (var project in Projects)
                            {
                                project.IsObjection = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred finding the projects from {0}", Utilities.GetResourceString("PALS"));
                    LogError(this.GetType(), message, ex);

                    modelState.AddModelError(string.Empty, message);
                }

                dmManager.Dispose();
            }

            return results;
        }
        #endregion
    }
}