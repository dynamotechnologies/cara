﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc.UI;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Documents;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter coding view Model
    /// </summary>
    public class LetterCodingViewModel : AbstractViewModel
    {
        #region Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int LetterId { get; set; }
        public Letter Letter { get; set; }
        public ICollection<TermList> TermLists { get; set; }
        public ICollection<LetterTermConfirmation> LetterTermConfirmations { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public string SelectedText { get; set; }
        public string CodeSearchText { get; set; }
        public Comment Comment { get; set; }
        public bool Locked { get; set; }
        public int LetterFontSize { get; set; }
        public string RequestUri { get; set; }
        public int? StatusId { get; set; }
        public string Annotations { get; set; }
        public Author Sender { get; set; }
        public FormSet FormSet { get; set; }
        public ICollection<Document> Documents { get; set; }
        public ICollection<LetterTermConfirmation> TermConfirmations { get; set; }
        public ICollection<Letter> Duplicates { get; set; }
        public IList<IGrouping<string, LetterTermConfirmation>> TermGroups { get; set; }
        public string NextLetterIds { get; set; }
        public int? LettersRemaining { get; set; }
        public string TabName { get; set; }
        public GenericStandardReportViewModel<CommentsAndResponsesByLetterReportData> ObjectionSummaryReport { get; set; }

        public LetterObjection ObjectionInfo
        {
            get
            {
                return Letter.LetterObjection;
            }
        }

        TimeZoneInfo phaseTimeZone;
        public TimeZoneInfo PhaseTimeZone
        {
            get 
            {
                if (PhaseId != 0)
                {
                    try
                    {
                        using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            string timeZone = string.Empty;
                            string configTimeZone = string.Empty;
                            Phase caraPhase = null;
                            caraPhase = phaseManager.Get(PhaseId);
                            timeZone = caraPhase.TimeZone;

                            //retrieve default time zone from config
                            if (String.IsNullOrEmpty(timeZone))
                            {
                                try
                                {
                                    configTimeZone = ConfigurationManager.AppSettings["CommentPeriodTimeZoneEnd"];
                                }
                                catch (ConfigurationErrorsException cee)
                                {
                                    configTimeZone = "UTC-11"; //key for the default time zone in case config can't be read
                                }
                                timeZone = configTimeZone;
                            }

                            phaseTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format("An error occurred while retrieving the phase time zone.");
                        LogError(this.GetType(), message, ex);
                    }
                }
                return phaseTimeZone;            
            }
        }

        DateTime dateSubmittedUtc;
        public DateTime DateSubmittedUtc
        {
            get 
            {
                try
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        Letter caraLetter = null;
                        caraLetter = letterManager.Get(LetterId);
                        dateSubmittedUtc = caraLetter.DateSubmitted;
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred while retrieving the letter.");
                    LogError(this.GetType(), message, ex);
                }
                return dateSubmittedUtc;
            }
        }

        string displayDateAndTimeZone = string.Empty;
        public string ConvertedDateSubmitted
        {
            get 
            {
                DateTime convertedTime = TimeZoneInfo.ConvertTimeFromUtc(DateSubmittedUtc, PhaseTimeZone);
                //displayDateAndTimeZone = convertedTime.ToShortDateString() + " (" + ProjectTimeZone + ")"; //displays day/month/year without time
                //displayDateAndTimeZone = convertedTime.ToString() + " (" + ProjectTimeZone + ")"; //displays UTC offset and standard name
                displayDateAndTimeZone = convertedTime.ToString() + " (" + PhaseTimeZone.StandardName + ")";
                return displayDateAndTimeZone;
            }
        }
        
        /// <summary>
        /// Returns a select list of no response reasons
        /// </summary>
        public IEnumerable<SelectListItem> LookupNoResponseReason
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupNoResponseReason>().SelectionList, string.Empty);
            }
        }

       
        ///<summary>
        ///Returns a select list of letter confirmation status
        ///</summary>
        public IEnumerable<SelectListItem> LookupLetterTermConfirmationStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupLetterTermConfirmationStatus>().SelectionList, string.Empty, false);
            }
        }

        
        /// <summary>
        /// Returns a list of available text modes
        /// </summary>
        public SelectList TextModes
        {
            get
            {
                return new SelectList(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("Markup", "Show " + Utilities.GetResourceString(Constants.TERM_LIST)),
                        new KeyValuePair<string, string>(Constants.COMMENT, "Show " + Constants.COMMENT),
                        new KeyValuePair<string, string>("CommentMarkup", "Show " + Constants.COMMENT + " & " + Utilities.GetResourceString(Constants.TERM_LIST)),
                        new KeyValuePair<string, string>(Constants.NONE, Constants.NONE)
                    }, "Key", "Value", "CommentMarkup"
                );
            }
        }
        
        /// <summary>
        /// Returns a list of available font sizes
        /// </summary>
        public SelectList FontSizes
        {
            get
            {
                return new SelectList(new List<KeyValuePair<int, string>>
                    {
                        new KeyValuePair<int, string>(10, "10 pt"),
                        new KeyValuePair<int, string>(12, "12 pt"),
                        new KeyValuePair<int, string>(14, "14 pt")
                    }, "Key", "Value", 10
                );
            }
        }

		/// <summary>
		/// Determines whether the letter can be deleted
		/// </summary>
		public bool Deletable
		{
			get
			{
				bool isLocked = Letter.Phase.Locked;
				bool isProtected = Letter.Protected.HasValue && Letter.Protected.Value;
				bool canDeleteLetters = Utilities.UserHasPrivilege("DLTR", Constants.PHASE);
				bool canDeleteProtectedLetters = Utilities.UserHasPrivilege("DPL",Constants.SYSTEM);
                bool hasDuplicates = Duplicates.Count > 0;

				return !isLocked && !hasDuplicates && ((!isProtected && canDeleteLetters) || (isProtected && canDeleteProtectedLetters));
			}
		}

		/// <summary>
		/// Determines whether the letter can be edited
		/// </summary>
		public bool Editable
		{
			get
			{
                return !Letter.Phase.Locked && Utilities.UserHasPrivilege("MLTR", Constants.PHASE);
			}
		}

		/// <summary>
		/// Determines whether the letter can be published/unpublished
		/// </summary>
        public bool Publishable
        {
            get
            {
                return Utilities.UserHasPrivilege("PURR", Constants.PHASE) && !Letter.LetterTypeName.Equals(Constants.DUPLICATE);
            }
        }

        public bool IsObjection
        {
            get
            {
                return Letter.Phase.PhaseTypeId == 3; /* Objection */
            }
        }
        #endregion
    }
}