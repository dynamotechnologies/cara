﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a project input view Model
    /// </summary>
    public class ObjectionInputViewModel : AbstractWizardModel
    {
        #region Properties
        public DataMartProjectViewModel DataMartProjectViewModel { get; set; }
        public string ProjectNameOrId { get; set; }
        public string Action { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public string SelectedTimeZone { get; set; }

        private bool _isObjection = false;
        public bool IsObjection
        {
            get { return _isObjection; }
            set { _isObjection = value; }
        }

        public IEnumerable<Unit> Units
        {
            get
            {
                User currentUser = Utilities.GetUserSession();
                IEnumerable<Unit> lookup = null;
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();
                if (currentUser.HasSystemPrivilege("MSU"))
                {
                    lookup = lookupUnit.Where(unit => unit.IsRegion);
                }
                else
                {
                    // Restrict unit selection by membership ordered by UnitId
                    // Units stored in the user are padded by 0s to make each unitId 8 characters long
                    var units = currentUser.Units.OrderBy(x => x).ToList();

                    // Remove any units in the list that already have a parent
                    string tempUnit = "TEMPUNIT";
                    string trimmedUnit;
                    var relevantUnits = new List<string>();
                    foreach (var unitId in units)
                    {
                        if (unitId.EndsWith("0000"))
                        {
                            trimmedUnit = unitId.Substring(0, unitId.Length - 4);
                        }
                        else if (unitId.EndsWith("00"))
                        {
                            trimmedUnit = unitId.Substring(0, unitId.Length - 2);
                        }
                        else
                        {
                            trimmedUnit = unitId;
                        }

                        if (!trimmedUnit.StartsWith(tempUnit))
                        {
                            relevantUnits.Add(trimmedUnit);
                        }

                        tempUnit = trimmedUnit;
                    }

                    lookup = lookupUnit.Where(unit => relevantUnits.Contains(unit.UnitId));
                }

                return lookup;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the project input model
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="action"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            if (string.IsNullOrEmpty(DataMartProjectViewModel.ProjectId))
            {
                modelState.AddModelError(string.Empty, "You must select a project.");
            }
        }

        /// <summary>
        /// Save a project and new phase into CARA system
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns>Cara.Domain.Phase object</returns>
        public Phase Save(ModelStateDictionary modelState)
        {
            Phase phase = null;
            var dmManager = new DataMartManager();

            try
            {
                // save new project and new phase
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    dmManager.ProjectManager = projectManager;

                    Project caraProject = null;
                    Project dmProject;
                    caraProject = projectManager.GetByProjectNumber(DataMartProjectViewModel.ProjectId);
                    dmProject = dmManager.GetProject(int.Parse(DataMartProjectViewModel.ProjectId));

                    #region Create the project if it doesn't already exist in CARA
                    if (caraProject == null)
                    {
                        caraProject = dmProject;
                        caraProject.Deleted = false;
                        caraProject.LastUpdated = DateTime.UtcNow;
                        if (caraProject.CreateDate.Year == 1)
                        {
                            caraProject.CreateDate = caraProject.LastUpdated;
                        }

                        caraProject.LastUpdatedBy = Utilities.GetUserSession().Username;
                        projectManager.Add(caraProject);
                        projectManager.UnitOfWork.Save();

                        // link the project to PALS project
                        if (!dmManager.LinkProject(caraProject.ProjectId, int.Parse(caraProject.ProjectNumber)))
                        {
                            modelState.AddModelError(string.Empty, string.Format("An error occurred while linking {0} {1} to {2}.", Constants.PROJECT,
                                caraProject.ProjectNumber, Utilities.GetResourceString("PALS")));
                            throw new ApplicationException();
                        }
                    }
                    #endregion Create the project if it doesn't already exist in CARA

                    #region Create the new Phase
                    var milestones = dmManager.GetProjectMilestones(ProjectId);
                    var milestoneStartDate = milestones.FirstOrDefault(x => x.MilestoneType == Milestone.OBJECTION_START_DATE);
                    DateTime commentStartDate;
                    if (milestoneStartDate == null)
                    {
                        var currentDate = DateTime.UtcNow;
                        // Use a default date
                        milestoneStartDate = new Milestone { DateString = currentDate.ToString("MM/dd/yyyy") };
                        commentStartDate = currentDate;
                    }
                    else
                    {
                        //CARA-1477 add one day to comment start date
                        commentStartDate = DateTime.Parse(milestoneStartDate.DateString).AddDays(1);
                    }

                    // Compute the end date
                    var lkuPhaseMinLength = LookupManager.GetLookup<LookupPhaseMinimumLength>();
                    var pml = lkuPhaseMinLength.GetValue(caraProject.AnalysisTypeId, caraProject.CommentRuleId.Value);

                    if (!pml.ObjectionPeriodMinimumDays.HasValue)
                    {
                        Logger.InfoFormat("Project {0} does not have a defined Objection Period length.  Defaulting to 30", ProjectId);
                        pml.ObjectionPeriodMinimumDays = 30;
                    }

                    if (!pml.ObjectionDecisionDueDays.HasValue)
                    {
                        Logger.InfoFormat("Project {0} does not have a defined Objection Decision Due Date.", ProjectId);
                        switch (caraProject.CommentRuleId)
                        {
                            case 13: /* 219 (2012) */
                                pml.ObjectionDecisionDueDays = 90;
                                break;
                            case 14: /* 218 (2013) HFRA */
                                pml.ObjectionDecisionDueDays = 30;
                                break;
                            case 15: /* 218 (2013) Non-HFRA */
                                pml.ObjectionDecisionDueDays = 45;
                                break;
                            default:
                                pml.ObjectionDecisionDueDays = 30;
                                break;
                        }
                    }

                    var commentEndDate = commentStartDate.AddDays(pml.ObjectionPeriodMinimumDays.Value);
                    var objectionResponseDue = commentEndDate.AddDays(pml.ObjectionDecisionDueDays.Value);

                    //CARA-1476
                    if (objectionResponseDue.DayOfWeek == DayOfWeek.Saturday)
                    {
                        objectionResponseDue = objectionResponseDue.AddDays(2);
                    }
                    else if (objectionResponseDue.DayOfWeek == DayOfWeek.Sunday)
                    {
                        objectionResponseDue = objectionResponseDue.AddDays(1);
                    }

                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        phase = new Phase
                        {
                            ProjectId = caraProject.ProjectId,
                            PhaseTypeId = 3, /* Objection */
                            AlwaysOpen = false,
                            Deleted = false,
                            CommentStart = commentStartDate,
                            CommentEnd = commentEndDate,
                            ObjectionResponseDue = objectionResponseDue,
                            ObjectionResponseExtendedBy = 0,
                            PublicCommentActive = false,
                            ReadingRoomActive = false,
                            Locked = false,
                            Private = false,
                            LastUpdated = DateTime.UtcNow,
                            LastUpdatedBy = Utilities.GetUserSession().Username
                        };

                        if (!PhaseUtilities.IsValidatePeriod(caraProject.ProjectId, phase.PhaseId, phase.CommentStart, phase.CommentEnd))
                        {
                            modelState.AddModelError(
                                string.Empty,
                                string.Format("This Objection period for Project {0} overlaps an existing comment period (Start Date: {1}, End Date: {2})",
                                    ProjectId,
                                    phase.CommentStart.ToString("d"),
                                    phase.CommentEnd.ToString("d")));

                            throw new ApplicationException();
                        }

                        // TODO:  Add point of contact as a team member.  As of now, that person is 
                        //        mentioned by name in the Datamart and we need the person's shortname.
                        #region Add team members
                        // -------------------------------------------------
                        // Add users defined in the Datamart
                        // Primary Project Manager, Secondary Project Manager, DataEntryPerson, PointOfContact
                        // -------------------------------------------------
                        var filter = DataMartProjectViewModel.GetFilterFromModel() as DataMartProjectFilter;

                        // set filter paging info
                        if (filter != null)
                        {
                            filter.StartRow = 0;
                            filter.NumRows = Constants.DEFAULT_PAGE_SIZE;
                            filter.ProjectId = DataMartProjectViewModel.ProjectId;
                        }

                        var results = dmManager.FindProjects(filter);
                        var peopleToAdd = new List<string>();

                        // Add the current user
                        peopleToAdd.Add(Utilities.GetUserSession().Username);

                        string primaryProjectManager = results.Projects[0].NepaInfo.PrimaryProjectManager;
                        if (!string.IsNullOrEmpty(primaryProjectManager) && !peopleToAdd.Contains(primaryProjectManager))
                        {
                            peopleToAdd.Add(primaryProjectManager);
                        }

                        string secondaryProjectManager = results.Projects[0].NepaInfo.SecondaryProjectManager;
                        if (!string.IsNullOrEmpty(secondaryProjectManager) && !peopleToAdd.Contains(secondaryProjectManager))
                        {
                            peopleToAdd.Add(secondaryProjectManager);
                        }

                        string dataEntryPerson = results.Projects[0].NepaInfo.DataEntryPerson;
                        if (!string.IsNullOrEmpty(dataEntryPerson) && !peopleToAdd.Contains(dataEntryPerson))
                        {
                            peopleToAdd.Add(dataEntryPerson);
                        }

                        foreach (var shortname in peopleToAdd)
                        {
                            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                            {
                                dmManager.UserManager = userManager;

                                try
                                {
                                    var userId = dmManager.GetUser(shortname).UserId;

                                    phase.PhaseMemberRoles.Add(new PhaseMemberRole
                                    {
                                        UserId = userId,
                                        CanRemove = true,
                                        Active = true,
                                        LastUpdated = DateTime.UtcNow,
                                        LastUpdatedBy = Utilities.GetUserSession().Username,
                                        RoleId = 4,  // Team Member
                                        IsPointOfContact = false
                                    });
                                }
                                catch (Exception ex)
                                {
                                    string message = string.Format("An error occurred adding a team member '{0}' to an objection period: {1}", shortname, ProjectId);
                                    LogError(this.GetType(), message, ex);
                                }
                            }
                        }
                        #endregion Add team members

                        if (modelState.IsValid)
                        {
                            phaseManager.Add(phase);
                            phaseManager.UnitOfWork.Save();

                            // Add the Objection coding structure
                            phaseManager.InsertPhaseCodes(5 /* Objection Template */, null, null, phase.PhaseId);

                            // link the new phase to PALS project //
                            if (!dmManager.LinkPhase(ProjectId, phase))
                            {
                                modelState.AddModelError(string.Empty, string.Format("An error occurred while linking {0} {1} to {2}", Utilities.GetResourceString(Constants.PHASE),
                                    phase.PhaseTypeDesc, Utilities.GetResourceString("PALS")));
                            }
                        }
                    }
                    #endregion Create the new Phase
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PROJECT, Utilities.GetResourceString("AppNameAbbv"));
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);

                phase = null;
            }
            finally
            {
                dmManager.Dispose();
            }

            return phase;
        }
        #endregion
    }
}