﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a member view Model
    /// </summary>
    public class MemberViewModel : AbstractWizardModel
    {
        #region Properties
        public ICollection<PhaseMemberRole> Members { get; set; }
        public DataMartUserViewModel UserViewModel { get; set; }
        public ICollection<Project> Projects { get; set; }
        public bool ShowButtons { get; set; }
        public bool Locked { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProjectNameOrId { get; set; }
        public string Type { get; set; }
        public bool IsPalsProject { get; set; }

        /// <summary>
        /// Selected project ID
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// Selected phase ID
        /// </summary>
        public string PhaseId { get; set; }

        /// <summary>
        /// Selected user name
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Selected role ID
        /// </summary>
        public string RoleId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the member input model based on the input action
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="action"></param>
        public void Validate(ModelStateDictionary modelState, string action)
        {
            if ("Search Users".Equals(action))
            {
                if (string.IsNullOrWhiteSpace(LastName) && string.IsNullOrWhiteSpace(FirstName))
                {
                    modelState.AddModelError(string.Empty, "User search criteria is required.");
                }
            }
            else if ("Search Projects".Equals(action))
            {
                if (string.IsNullOrEmpty(ProjectNameOrId) || ProjectNameOrId.Length < 4)
                {
                    modelState.AddModelError(string.Empty, "Project search criteria is required.");
                }
            }
            else if ("Add User".Equals(action))
            {
                // validate
                if (string.IsNullOrEmpty(RoleId) || string.IsNullOrEmpty(Username))
                {
                    modelState.AddModelError(string.Empty, "Please select a user.");
                }
            }
            else if ("Reuse Users".Equals(action))
            {
                // validate
                if (string.IsNullOrEmpty(ProjectId) || string.IsNullOrEmpty(PhaseId))
                {
                    modelState.AddModelError(string.Empty, string.Format("Please select a {0} and a {1} to reuse users.", Constants.PROJECT, Utilities.GetResourceString(Constants.PHASE)));
                }
            }

        }

        /// <summary>
        /// Add a single user to the phase
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="modelState"></param>
        public void AddUser(int phaseId, ModelStateDictionary modelState)
        {
            int userId = 0;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.UserManager = userManager;

                // get the user id by username in cara
                if (!string.IsNullOrEmpty(UserViewModel.Username))
                {
                    // get user by name
                    var user = userManager.Get(UserViewModel.Username);

                    // if the user does not exist in cara, call get user to get the user to create the user in cara
                    if (user == null)
                    {
                        try
                        {
                            dmManager.UserManager = userManager;
                            userId = dmManager.GetUser(UserViewModel.Username).UserId;
                        }
                        catch (Exception ex)
                        {
                            string message = string.Format("An error occurred retrieving the users from {0}", Utilities.GetResourceString("PALS"));
                            LogError(this.GetType(), message, ex);

                            modelState.AddModelError(string.Empty, message);
                        }
                    }
                    else
                    {
                        userId = user.UserId;
                    }
                }

                dmManager.Dispose();
            }

            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                if (userId > 0 && phaseManager.AllPhaseMemberRoleByPhase(phaseId).Any(x => x.UserId == userId))
                {
                    modelState.AddModelError(string.Empty, "The user is already a team member.");
                }

                if (modelState.IsValid)
                {
                    try
                    {
                        // create the phase member role object
                        var pmr = new PhaseMemberRole
                        {
                            UserId = userId,
                            PhaseId = phaseId,
                            Phase = phaseManager.Get(phaseId),
                            CanRemove = true,
                            Active = true,
                            LastUpdated = DateTime.UtcNow,
                            LastUpdatedBy = Utilities.GetUserSession().Username,
                            RoleId = LookupManager.GetLookup<LookupRoleByName>()[RoleId].RoleId,
                            IsPointOfContact = false
                        };

                        // save the new phase member role
                        phaseManager.AddPhaseMemberRole(pmr);
                        phaseManager.UnitOfWork.Save();

                        // notify the user
                        NotificationManager.SendAddedToPhaseNotification(pmr);
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format("An error occurred adding the {0}.", Constants.MEMBER);
                        LogError(this.GetType(), message, ex);

                        modelState.AddModelError(string.Empty, message);
                    }
                }
            }
        }

        /// <summary>
        /// Resuse the team members from an existing phase
        /// </summary>
        /// <param name="targetPhaseId">Target Phase ID</param>
        /// <param name="modelState"></param>
        public void ReuseUsers(int targetPhaseId, ModelStateDictionary modelState)
        {
            try
            {
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {

                    // get a list of current members
                    var members = phaseManager.Get(targetPhaseId).PhaseMemberRoles.Select(x => new KeyValuePair<int, int>(x.UserId, x.RoleId));

                    // only the active members will be reused
                    var membersToAdd = phaseManager.Get(int.Parse(PhaseId)).PhaseMemberRoles
                        .Where(x => x.Active).Select(x => new KeyValuePair<int, int>(x.UserId, x.RoleId));

                    // validate and filter out the duplicates
                    if (members != null)
                    {
                        // get only the members currently not in the phase
                        IEnumerable<KeyValuePair<int, int>> membersNew = membersToAdd.Except(members);

                        // process the new phase member roles
                        if (membersNew != null && membersNew.Count() > 0)
                        {
                            foreach (KeyValuePair<int, int> pmr in membersNew)
                            {
                                // create the phase member role object
                                var pmrNew = new PhaseMemberRole
                                {
                                    UserId = pmr.Key,
                                    PhaseId = targetPhaseId,
                                    CanRemove = true,
                                    Active = true,
                                    LastUpdated = DateTime.UtcNow,
                                    LastUpdatedBy = Utilities.GetUserSession().Username,
                                    RoleId = pmr.Value,
                                    IsPointOfContact = false
                                };

                                // save the new phase member role
                                phaseManager.AddPhaseMemberRole(pmrNew);

                                // notify the user
                                NotificationManager.SendAddedToPhaseNotification(pmrNew);
                            }

                            phaseManager.UnitOfWork.Save();
                        }
                        else
                        {
                            modelState.AddModelError(string.Empty, string.Format("No new team members were added from the selected {0}.", Utilities.GetResourceString(Constants.PHASE)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred reusing the users from the selected {0}.", Utilities.GetResourceString(Constants.PHASE));
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);
            }
        }

        /// <summary>
        /// Parse the role id after it is set
        /// </summary>
        public void ParseRoleId()
        {
            // parse the selected role id (format: username~role id)
            if (!string.IsNullOrWhiteSpace(RoleId))
            {
                var elements = RoleId.Split('~');

                if (elements != null && elements.Length > 1)
                {
                    Username = elements[0];
                    RoleId = elements[1];
                }
            }
        }
        #endregion
    }
}