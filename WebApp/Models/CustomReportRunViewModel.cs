﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain.Report;
using Telerik.Reporting;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a custom report view Model
    /// </summary>
    public class CustomReportRunViewModel
    {
        #region Properties
		public IReportDocument Report { get; set; }
		public string ReportName { get; set; }
        #endregion
    }
}