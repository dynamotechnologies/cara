﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Aquilent.Cara.Domain;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Service class for the Letter operations
    /// </summary>
    public class LetterService
    {
        #region Letter Attachment Methods
        /// <summary>
        /// Returns a collection of DMD ids from the letter, including letter text and attachments
        /// </summary>
        /// <param name="letter">The Letter object</param>
        /// <param name="letterDmdId">The DMD Id of the letter text</param>
        /// <returns></returns>
        public static ICollection<string> GetDmdIds(Letter letter, out string letterDmdId)
        {
            ICollection<string> dmdIds = null;
            letterDmdId = null;

            if (letter != null)
            {
                // get letter attachment dmd ids
                dmdIds = letter.LetterDocuments.Select(x => x.Document.DmdId).Where(x => !(string.IsNullOrEmpty(x))).ToList();

                // get letter text dmd id
                letterDmdId = letter.DmdId;
                if (!string.IsNullOrEmpty(letterDmdId))
                {
                    dmdIds.Add(letter.DmdId);
                }
            }

            return dmdIds;
        }

        /// <summary>
        /// Returns a collection of DMD ids from the letter, including letter text and attachments
        /// </summary>
        /// <param name="letter">The Letter object</param>
        /// <returns></returns>
        public static ICollection<string> GetDmdIds(Letter letter)
        {
            string output = null;
            var dmdIds = GetDmdIds(letter, out output);

            return dmdIds;
        }
        #endregion
    }
}