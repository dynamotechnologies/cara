﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Properties;
using Aquilent.EntityAccess;
using Aquilent.Framework;
using Aquilent.Framework.Security;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Telerik.Web.Mvc.UI.Fluent;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using Newtonsoft.Json;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Utility helper class for the application
    /// </summary>
    public class Utilities
    {
        #region Format Methods
        /// <summary>
        /// Returns the string text without any html tags
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveHtmlTags(string text)
        {
            return (!string.IsNullOrEmpty(text) ? System.Text.RegularExpressions.Regex.Replace(text, "<[^>]*>", string.Empty) : string.Empty);
        }

        /// <summary>
        /// Returns the text after converting all multiple whitespaces and tabs into a single whitespace. Also removes
        /// the space immediately before and after a linebreak.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveTabsAndWhitespaces(string text)
        {
            var result = string.Empty;

            if (!string.IsNullOrEmpty(text))
            {
                result = text.Replace("  ", " ").Replace("\t", " ");

                while (result.IndexOf("  ") > -1)
                {
                    result = result.Replace("  ", " ");
                }
            }

			result = result.Replace(" \r\n", "\r\n").Replace("\r\n ", "\r\n");
			result = result.Replace(" \n", "\n").Replace("\n ", "\n");

			return result;
        }
        #endregion

        #region Resource Methods
        /// <summary>
        /// Get the resource value of the key from resource manager
        /// </summary>
        /// <param name="key"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string GetResourceString(string key, string prefix = "")
        {
            return Resources.ResourceManager.GetString(prefix + key);
        }

        /// <summary>
        /// Get the page title of the page name from resource manager
        /// </summary>
        /// <param name="name">Page name</param>
        /// <param name="mode">Page mode (optional)</param>
        /// <returns></returns>
        public static string GetPageTitle(string name, PageMode mode = PageMode.None)
        {
            // get the resource by the key [page_name]Title
            var title = GetResourceString(Constants.TITLE, name);

            // if the resource cannot be found, search [page_name][mode]Title
            if (string.IsNullOrEmpty(title) && mode != PageMode.None)
            {
                title = GetResourceString(Constants.TITLE, name + mode.ToString());
            }

            return title;
        }

        /// <summary>
        /// Get the wizard page title of the page name from resource manager
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="controllerAction"></param>
        /// <param name="step"></param>
        /// <param name="projectName">Optional Project Name</param>
        /// <returns></returns>
        public static string GetWizardPageTitle(PageMode mode, string controllerAction, string step, string projectName = "")
        {
            return GetPageTitle(controllerAction, mode) + (mode.ToString().Contains(Constants.WIZARD) ? " " +
                ProjectWizardManager.Instance.GetStepNumber(mode, step) +
                (!string.IsNullOrEmpty(projectName) ? ": " + projectName : string.Empty) : string.Empty);
        }
        #endregion

        #region Filter Methods
        /// <summary>
        /// Sets the filter to the filter dictionary in the session
        /// </summary>
        /// <param name="key"></param>
        /// <param name="filter"></param>
        public static void SetFilter(string key, string context, IFilter filter)
        {
            if (context == null)
            {
                context = string.Empty;
            }
            if (HttpContext.Current.Session[Constants.FILTER] == null)
            {
                HttpContext.Current.Session[Constants.FILTER] = new Dictionary<string, IFilter>();
            }

            if (!string.IsNullOrEmpty(key))
            {
                (HttpContext.Current.Session[Constants.FILTER] as IDictionary<string, IFilter>)[key+context] = filter;
            }
        }

        /// <summary>
        /// Gets the filter from the filter dictionary in the session
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IFilter GetFilter(string key, string context)
        {
            if (context == null)
            {
                context = string.Empty;
            }
            IFilter filter = null;

            if (!string.IsNullOrEmpty(key) && HttpContext.Current.Session[Constants.FILTER] != null)
            {
                filter = ((HttpContext.Current.Session[Constants.FILTER] as IDictionary<string, IFilter>).ContainsKey(key+context) ?
                    (HttpContext.Current.Session[Constants.FILTER] as IDictionary<string, IFilter>)[key+context] as IFilter : null);
            }

            return filter;
        }
        #endregion

        #region Term List Methods
        /// <summary>
        /// Sets the term list id array to the session key
        /// </summary>
        /// <param name="termListIds"></param>
        public static void SetTermList(int[] termListIds)
        {
            HttpContext.Current.Session[Constants.TERM_LIST] = termListIds;
        }

        /// <summary>
        /// Gets the term list ids from the the session key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int[] GetTermList()
        {
            int[] ids = null;

            if (HttpContext.Current.Session[Constants.TERM_LIST] != null)
            {
                ids = (HttpContext.Current.Session[Constants.TERM_LIST] as int[]);
            }

            return ids;
        }

        /// <summary>
        /// Returns true if the id is in the session term lists
        /// </summary>
        /// <param name="id">Term List ID</param>
        /// <returns></returns>
        public static bool TermListContains(int id)
        {
            bool result = false;

            if (HttpContext.Current.Session[Constants.TERM_LIST] != null)
            {
                result = (HttpContext.Current.Session[Constants.TERM_LIST] as int[]).Any(x => x == id);
            }

            return result;
        }
        #endregion

        #region User Methods
        /// <summary>
        /// Returns the user object stored in the security token object in the session
        /// </summary>
        /// <returns></returns>
        public static User GetUserSession()
        {
            return (HttpContext.Current.Session["SecurityToken"] != null ?
                (HttpContext.Current.Session["SecurityToken"] as SecurityToken).User : null);
        }

        public static bool UserHasPrivilege(string privilegeCode, string scope)
        {
            return UserHasPrivilege(privilegeCode, scope, null, -1);
        }

        private static IDictionary<int, string> unitIdsByPhaseId = new Dictionary<int, string>();
        private static IDictionary<int, string> unitIdsByProjectId = new Dictionary<int, string>();

        /// <summary>
        /// Returns true if the user has privilege within the scope
        /// </summary>
        /// <param name="privilegeCode">Privilege Code</param>
        /// <param name="scope">Scope to validate (System, Unit, or Phase)</param>
        /// <param name="phaseId">null or Unit ID or Phase ID</param>
        /// <returns></returns>
        public static bool UserHasPrivilege(string privilegeCode, string scope, string unitId, int phaseId)
        {
            var result = false;

            // get the user from the session
            if (GetUserSession() != null && !string.IsNullOrEmpty(privilegeCode) && !string.IsNullOrEmpty(scope))
            {
                // Retreive the phaseId and/or the unitId, if unknown
                if (scope == Constants.PHASE || scope == Constants.UNIT)
                {
                    if (phaseId <= 0)
                    {
                        phaseId = Utilities.GetEntityIdFromUrl(HttpContext.Current.Request.Url.AbsolutePath, Constants.PHASE);
                        
                        // Handle the case where the PhaseId is not in the URL and is
                        // submitted as a query or form param
                        if (phaseId <= 0)
                        {
                            int.TryParse(HttpContext.Current.Request.Params["phaseId"], out phaseId);
                        }
                    }

                    if (phaseId > 0 && string.IsNullOrEmpty(unitId))
                    {
                        // Check dictionary
                        if (unitIdsByPhaseId.ContainsKey(phaseId))
                        {
                            unitId = unitIdsByPhaseId[phaseId];
                        }
                        else
                        {
                            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                            {
                                unitId = phaseManager.All.Where(ph => ph.PhaseId == phaseId).Select(ph => ph.Project.UnitId).FirstOrDefault();

                                if (unitId != null)
                                {
                                    unitIdsByPhaseId.Add(phaseId, unitId);
                                }
                            }
                        }
                    }
                    else if (phaseId <= 0 && string.IsNullOrEmpty(unitId))
                    {
                        int projectId = Utilities.GetEntityIdFromUrl(HttpContext.Current.Request.Url.AbsolutePath, Constants.PROJECT);
                        if (projectId > 0)
                        {
                            if (unitIdsByProjectId.ContainsKey(projectId))
                            {
                                unitId = unitIdsByProjectId[projectId];
                            }
                            else
                            {
                                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                                {
                                    unitId = projectManager.All.Where(pr => pr.ProjectId == projectId).Select(pr => pr.UnitId).FirstOrDefault();

                                    if (unitId != null)
                                    {
                                        unitIdsByProjectId.Add(projectId, unitId);
                                    }
                                }
                            }
                        }
                    }
                }

                switch (scope)
                {
                    case Constants.SYSTEM:
                        result = GetUserSession().HasPrivilege(privilegeCode.Trim());
                        break;
                    case Constants.UNIT:
                        if (!string.IsNullOrEmpty(unitId))
                        {
                            result = GetUserSession().HasUnitPrivilege(privilegeCode, unitId);
                        }

                        if (!result)
                        {
                            result = GetUserSession().HasSystemPrivilege(privilegeCode);
                        }

                        break;

                    case Constants.PHASE:
                        if (phaseId <= 0)
                        {
                            // get phase id by parsing the current url path
                            phaseId = Utilities.GetEntityIdFromUrl(HttpContext.Current.Request.Url.AbsolutePath, Constants.PHASE);

                            // if phase id is not found in the url path, try to search in request parameters
                            if (phaseId <= 0)
                            {
                                if (HttpContext.Current.Request.Params["phaseId"] != null)
                                {
                                    int.TryParse(HttpContext.Current.Request.Params["phaseId"], out phaseId);
                                }
                            }
                        }

                        // use phase id and the privilege code to perform the authorization against the user
                        if (phaseId > 0 || !string.IsNullOrEmpty(unitId))
                        {
                            result = GetUserSession().HasPhasePrivilege(privilegeCode.Trim(), phaseId);

                            if (!result && !string.IsNullOrEmpty(unitId))
                            {
                                result = GetUserSession().HasUnitPrivilege(privilegeCode, unitId);
                            }
                        }

                        if (!result)
                        {
                            result = GetUserSession().HasSystemPrivilege(privilegeCode);
                        }

                        break;
                    default:
                        break;
                }
            }

            return result;
        }
        #endregion

        #region Parsing Methods
        /// <summary>
        /// get the entity id from the input url based on the {entity}/{id} or {entity}/{action}/{id} route patterns
        /// </summary>
        /// <param name="url"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public static int GetEntityIdFromUrl(string url, string entityType)
        {
            var id = -1;

            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(entityType))
            {
                var index = -1;

                // if /{entity}/ is found
                if (url.LastIndexOf("/" + entityType + "/") > -1)
                {
                    // try to get the url chunk after the entity
                    var idText = url.Substring(url.LastIndexOf("/" + entityType + "/") + entityType.Length + 2);

                    // try {entity}/{id}/{action} pattern first
                    index = idText.IndexOf('/');

                    // if trailing url is found
                    if (index > -1)
                    {
                        // get the text before '/'
                        var idText1 = idText.Substring(0, index);

                        // parse the text to get the id
                        if (!int.TryParse(idText1, out id))
                        {
                            // try {entity}/{action}/{id} pattern next
                            idText1 = idText.Substring(idText.LastIndexOf('/') + 1);
                            if (!int.TryParse(idText1, out id))
                            {
                                id = -1;
                            }
                        }
                    }
                    // try {entity}/{id}
                    else
                    {
                        if (!int.TryParse(idText, out id))
                        {
                            // TODO: handle missing phase id
                        }
                    }
                }
            }

            return id;
        }

        /// <summary>
        /// Returns an integer value of the input text as the entity id
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static int ToEntityId(string text)
        {
            var id = 0;

            if (!int.TryParse(text, out id) || id < 0)
            {
                id = 0;
            }

            return id;
        }

        /// <summary>
        /// Returns a list of ids in integer format
        /// </summary>
        /// <param name="text">String of a delimiter separated list of ids</param>
        /// <param name="delimiter">Delimiter of the text</param>
        /// <returns></returns>
        public static int[] ToIntArray(string text, char delimiter = ',')
        {
            var ints = new int[0];

            if (!string.IsNullOrEmpty(text))
            {
                var strings = text.Split(delimiter);

                ints = strings.Select(x => int.Parse(x)).ToArray();
            }

            return ints;
        }

        /// <summary>
        /// Returns an xml of id list from input string
        /// e.g. <idList><id>1</id><id>2</id><id>3</id></idList>
        /// </summary>
        /// <param name="list"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string ToIdListXml(string list, char delimiter = ',')
        {
            var xml = new StringBuilder();

            xml.Append("<idList>");

            if (!string.IsNullOrEmpty(list))
            {
                var ids = list.Split(delimiter);

                foreach (var id in ids)
                {
                    xml.Append(string.Format("<id>{0}</id>", id));
                }
            }

            xml.Append("</idList>");

            return xml.ToString();
        }
        #endregion

        #region Select List Methods
        /// <summary>
        /// Convert a list of key value pair into a list of selection list items
        /// </summary>
        /// <param name="list">Data Source</param>
        /// <param name="optionLabel">Option Label</param>
        /// <param name="sortByName">Flag To Sort Data By Name</param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList(IList<KeyValuePair<int, string>> list, string optionLabel = null, bool sortByName = true)
        {
            var selectList = InitSelectList(optionLabel);

            if (list != null)
            {
                selectList = selectList.Concat(list.Select(kvp => new SelectListItem()
                {
                    Value = kvp.Key.ToString(),
                    Text = kvp.Value
                }));
            }

            // add sorting if flag is true
            if (sortByName)
            {
                selectList = selectList.OrderBy(x => x.Text);
            }

            return selectList;
        }

        /// <summary>
        /// Convert a list of key value pair into a list of selection list items
        /// </summary>
        /// <param name="list">Data Source</param>
        /// <param name="optionLabel">Option Label</param>
        /// <param name="sortByName">Flag To Sort Data By Name</param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList(IList<KeyValuePair<string, string>> list, string optionLabel = null, bool sortByName = true)
        {
            var selectList = InitSelectList(optionLabel);

            if (list != null)
            {
                selectList = selectList.Concat(list.Select(kvp => new SelectListItem()
                {
                    Value = kvp.Key,
                    Text = kvp.Value
                }));
            }

            // add sorting if flag is true
            if (sortByName)
            {
                selectList = selectList.OrderBy(x => x.Text);
            }

            return selectList;
        }

        /// <summary>
        /// Convert a list of lookup string items into a list of selection list items
        /// </summary>
        /// <param name="list">Data Source</param>
        /// <param name="optionLabel">Option Label</param>
        /// <param name="sortByName">Flag To Sort Data By Name</param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList(ILookup<string, string> lookup, string optionLabel = null, bool sortByName = true)
        {
            var selectList = InitSelectList(optionLabel);

            if (lookup != null)
            {
                selectList = selectList.Concat(lookup.Select(lkup => new SelectListItem()
                {
                    Value = lkup.Key.ToString(),
                    Text = lkup.First().ToString()
                })
                );

                // add sorting if flag is true
                if (sortByName)
                {
                    selectList = selectList.OrderBy(x => x.Text);
                }
            }

            return selectList;
        }

        /// <summary>
        /// Initialize a new select list based on the input option label
        /// </summary>
        /// <param name="optionLabel"></param>
        /// <returns></returns>
        private static IEnumerable<SelectListItem> InitSelectList(string optionLabel)
        {
            // add option label select item only if the label is provided
            return (optionLabel == null ? new List<SelectListItem>() : new List<SelectListItem>
            {
                new SelectListItem { Text = optionLabel, Value = null }
            });
        }
        #endregion

        #region Xml Methods
        /// <summary>
        /// Serializes the input object to an xml string
        /// </summary>
        /// <typeparam name="T">Object type name</typeparam>
        /// <param name="obj">class object</param>
        /// <returns></returns>
        public static string SerializeObjectToXmlString<T>(T obj)
        {
            var xmls = new XmlSerializer(typeof(T));
            using (var ms = new MemoryStream())
            {
                var settings = new XmlWriterSettings();
                settings.Encoding = Encoding.UTF8;
                settings.Indent = false;
                settings.ConformanceLevel = ConformanceLevel.Document;

                using (var writer = XmlTextWriter.Create(ms, settings))
                {
                    xmls.Serialize(writer, obj);
                }

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        /// <summary>
        /// Deserializes an xml string to a class object
        /// </summary>
        /// <typeparam name="T">Object type name</typeparam>
        /// <param name="xmlString">xml string of the serialized object</param>
        /// <returns></returns>
        public static T DeserializeXmlStringToObject<T>(string xmlString)
        {
            T obj = default(T);
            var xmls = new XmlSerializer(typeof(T));

            if (!string.IsNullOrEmpty(xmlString))
            {
                xmlString = xmlString.TrimStart('?');

                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
                {
                    obj = (T)xmls.Deserialize(ms);
                }
            }

            return obj;
        }
        #endregion

        #region Recaptcha Methods
        /// <summary>
        /// Returns true if the recaptcha response is valid
        /// </summary>
        /// <param name="privateKey">Private Key of the Recaptcha</param>
        /// <param name="challengeValue">Challenge value</param>
        /// <param name="responseValue">User's Response value</param>
        /// <param name="remoteIp">Remote IP</param>
        /// <returns></returns>
        public static bool IsRecaptchaValid(HttpContextBase httpContext, string responseValue = null)
        {
            bool isValid = false;
            string captchaResponseValue = responseValue;

            if (responseValue == null)
            {
                captchaResponseValue = httpContext.Request.Form["g-recaptcha-response"];
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://www.google.com/recaptcha/api/siteverify");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
              
            var content = new RecaptchaValidationRequest
            {
                SecretKey = ConfigurationManager.AppSettings["reCaptchaPrivateKey"],
                Response = captchaResponseValue,
                RemoteIp = httpContext.Request.UserHostAddress
            };

            // List data response.
            HttpResponseMessage response = client.PostAsync(string.Format("?secret={0}&response={1}&remoteip={2}", content.SecretKey, content.Response, content.RemoteIp), null).Result;
            if (response.IsSuccessStatusCode)
            {
                string jsonMessage;
                using (Stream responseStream = response.Content.ReadAsStreamAsync().Result)
                {
                    jsonMessage = new StreamReader(responseStream).ReadToEnd();
                }

                RecaptchaValidationResponse recaptchaResponse = (RecaptchaValidationResponse)JsonConvert.DeserializeObject(jsonMessage, typeof(RecaptchaValidationResponse));

                isValid = recaptchaResponse.Success;
            }

            return isValid;
        }
        #endregion

        #region Telerik Editor Methods
        public static EditorToolFactory EditorToolBar(EditorToolFactory tools)
        {
            return tools.Clear()
                        .Bold()
                        .Underline()
                        .Separator()
                        .JustifyLeft()
                        .JustifyCenter()
                        .JustifyRight()
                        .JustifyFull()
                        .Separator()
                        .InsertUnorderedList();

                        // Removed because NPOI does not support
                        //.InsertOrderedList()
                        //.Italic()
        }
        #endregion Telerik Editor Methods
    }
}