﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents the page mode
    /// </summary>
    public enum PageMode
    {
        None = 0,
        Create = 1,
        Edit = 2,
        ProjectCreateWizard = 3,
        PhaseCreateWizard = 4,
        PhaseEditWizard = 5,
        Public = 6,
        StandardReportWizard = 7,
        CustomReportWizard = 8,
        MyReportWizard = 9,
        CreateObjection = 10
    }
}