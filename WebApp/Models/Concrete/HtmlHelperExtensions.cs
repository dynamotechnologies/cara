﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI;

using Aquilent.Cara.WebApp.Models.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Class containing all customized html helper methods
    /// </summary>
    public static class HtmlHelperExtensions
    {
        #region Action Link Methods
        /// <summary>
        /// Returns the action link or just the link text depending on the access flag
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="linkText"></param>
        /// <param name="action"></param>
        /// <param name="controller"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="hasAccess"></param>
        /// <returns></returns>
        public static MvcHtmlString PrivilegedActionLink(this HtmlHelper htmlHelper, string linkText, string action, string controller,
            object routeValues, object htmlAttributes, bool hasAccess = false)
        {
            return (hasAccess ? htmlHelper.ActionLink(linkText, action, controller, routeValues, htmlAttributes) : MvcHtmlString.Create(string.Format("<span>{0}</span>", linkText)));
        }

        #endregion

        #region Recaptcha Methods
        /// <summary>
        /// Create the recaptcha control using the public/private key
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string GenerateRecaptcha(this HtmlHelper helper)
        {
            // create the recaptcha control
            var captchaControl = new Recaptcha.RecaptchaControl
                {
                    ID = "recaptcha",
                    Theme = "white",
                    PublicKey = ConfigurationManager.AppSettings["reCaptchaPublicKey"],
                    PrivateKey = ConfigurationManager.AppSettings["reCaptchaPrivateKey"],
                    OverrideSecureMode = true
                };

            // create a html writer
            var htmlWriter = new HtmlTextWriter(new StringWriter());

            captchaControl.RenderControl(htmlWriter);

            // return the control in html string
            return htmlWriter.InnerWriter.ToString();
        }
        #endregion
    }
}