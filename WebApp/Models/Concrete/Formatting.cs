﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Class representing for Formatting
    /// </summary>
    public class Formatting
    {
        private static readonly HashSet<string> Unpluralizables = new HashSet<string>
        { "equipment", "information", "rice", "money", "species", "series", "fish", "sheep", "deer" };

        private static readonly KeyValuePair<string, string>[] Pluralizations = new KeyValuePair<string, string>[]
	    {
            // Start with the rarest cases, and move to the most common
            new KeyValuePair<string, string>("person", "people"),
            new KeyValuePair<string, string>("ox", "oxen"),
            new KeyValuePair<string, string>("child", "children"),
            new KeyValuePair<string, string>("foot", "feet"),
            new KeyValuePair<string, string>("tooth", "teeth"),
            new KeyValuePair<string, string>("goose", "geese"),
            // And now the more standard rules.
            new KeyValuePair<string, string>("(.*)fe?", "$1ves"),         // ie, wolf, wife
            new KeyValuePair<string, string>("(.*)man$", "$1men"),
            new KeyValuePair<string, string>("(.+[aeiou]y)$", "$1s"),
            new KeyValuePair<string, string>("(.+[^aeiou])y$", "$1ies"),
            new KeyValuePair<string, string>("(.+z)$", "$1zes"),
            new KeyValuePair<string, string>("([m|l])ouse$", "$1ice"),
            new KeyValuePair<string, string>("(.+)(e|i)x$", @"$1ices"),    // ie, Matrix, Index
            new KeyValuePair<string, string>("(octop|vir)us$", "$1i"),
            new KeyValuePair<string, string>("(.+(s|x|sh|ch))$", @"$1es"),
            new KeyValuePair<string, string>("(.+)", @"$1s")
	    };

        /// <summary>
        /// Pluralize the input singular word
        /// </summary>
        /// <param name="singular"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Pluralize(string singular, int count = 2)
        {
            var plural = string.Empty;

            // return singular if it's unpluralizable or count is 1
            if (count == 1 || Unpluralizables.Contains(singular))
            {
                plural = singular;
            }
            else
            {
                // find the matching pluralization to replace
                foreach (var pluralization in Pluralizations)
                {
                    if (Regex.IsMatch(singular, pluralization.Key))
                    {
                        plural = Regex.Replace(singular, pluralization.Key, pluralization.Value);
                        break;
                    }
                }
            }

            return plural;
        }
    }
}
