﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Aquilent.Cara.WebApp.Models
{
    public class RecaptchaJsonToken
    {
        public static string CreateSecureToken(string siteSecret)
        {
            string sessionId = Guid.NewGuid().ToString();
            string jsonToken = CreateJsonToken(sessionId);
            return EncryptAes(jsonToken, siteSecret);
        }

        private static string CreateJsonToken(string sessionId)
        {
            DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var totalMillis = (long)((DateTime.UtcNow - Jan1st1970).TotalMilliseconds);

            return string.Format("{{\"session_id\": {0},\"ts_ms\":{1}}}", sessionId, totalMillis);// new TimeSpan(DateTime.Now.Ticks).TotalMilliseconds.ToString());
        }

        private static string EncryptAes(string input, string siteSecret)
        {
            byte[] encrypted;
            using (RijndaelManaged myRijndael = new RijndaelManaged())
            {
                myRijndael.Key = GetKey(siteSecret);
                myRijndael.IV = new byte[16];
                myRijndael.Mode = CipherMode.ECB;
                myRijndael.Padding = PaddingMode.PKCS7;

                ICryptoTransform encryptor = myRijndael.CreateEncryptor();
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(input);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }

                return Convert.ToBase64String(encrypted).Replace('+', '-').Replace('/', '_').Replace("=", "");
            }
        }

        private static byte[] GetKey(string siteSecret)
        {
            byte[] key = Encoding.UTF8.GetBytes(siteSecret);
            HashAlgorithm hash = new SHA1Managed();
            var keyHash = hash.ComputeHash(key);

            byte[] truncatedKeyHash = new byte[16];
            Array.Copy(keyHash, truncatedKeyHash, 16);
            return truncatedKeyHash;
        }
    }
}