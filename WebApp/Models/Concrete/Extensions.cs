﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Framework.Security;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
	public static class Extensions
    {
        #region User TimeZone Methods
        /// <summary>
		/// Converts time in UTC to the time zone of the user.
		/// </summary>
		/// <param name="timeInUtc"></param>
		/// <param name="user"></param>
		/// <returns></returns>
		public static DateTime ConvertToUserTimeZone(this DateTime timeInUtc, User user)
		{
			return TimeZoneInfo.ConvertTimeFromUtc(timeInUtc, user.TimeZoneInfo);
		}

		/// <summary>
		/// Converts the time in the user's time zone to UTC. 
		/// </summary>
		/// <param name="timeInZone"></param>
		/// <param name="user"></param>
		/// <returns></returns>
		public static DateTime ConvertFromUserTimeZone(this DateTime timeInZone, User user)
		{
            return TimeZoneInfo.ConvertTimeToUtc(timeInZone, user.TimeZoneInfo);
		}
        #endregion

        #region Boolean Methods
        /// <summary>
        /// Converts the nullable bool to Yes or No string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="boolean"></param>
        /// <returns></returns>
        public static string ConvertToYesNo(this bool? value)
        {
            return (value.HasValue ? value.Value.ConvertToYesNo() : "No");
        }

        /// <summary>
        /// Converts the bool to Yes or No string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="boolean"></param>
        /// <returns></returns>
        public static string ConvertToYesNo(this bool value)
        {
            return (value ? "Yes" : "No");
        }
        #endregion

        #region String Methods
        /// <summary>
        /// Returns the input text by performing the following conversions:
        /// 1. replace breaks by html line break tags
        /// 2. html encode the text
        /// 3. replace whitespaces or tabs to a single whitespace
        /// 4. for cross browser compatiblity, spaces immediately around line break needs to be trimmed;
        ///    a. all browsers won't render spaces right after <br>
        ///    b. both IE and Chrome would retain and show the spaces right before <br>. But FF will not.
        /// </summary>
        /// <param name="value">String Text</param>
        /// <returns></returns>
        public static string ToHtmlString(this string value)
        {
            return (!string.IsNullOrEmpty(value) ? Utilities.RemoveTabsAndWhitespaces(HttpUtility.HtmlEncode(value)).ReplaceLineBreaks() : string.Empty);
        }

        /// <summary>
        /// Replaces the carriage returns and newline characters with the html line break tags
        /// </summary>
        /// <param name="value">String Text</param>
        /// <returns></returns>
        public static string ReplaceLineBreaks(this string value, string replacement = "<br>")
        {
            return (!string.IsNullOrEmpty(value) ? value.Replace("\r\n", replacement).Replace("\n", replacement).Replace("\r", replacement) : string.Empty);
        }

        /// <summary>
        /// Returns the input text by performing the following conversions:
        /// 1. html decode the text
        /// 2. converts html linebreak tags to \r\n
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToTextString(this string value, string linebreak = "\r\n")
        {
			return (!string.IsNullOrEmpty(value) ? HttpUtility.HtmlDecode(value).Replace("<BR>", linebreak).Replace("<br>", linebreak) : string.Empty);
        }

        /// <summary>
        /// Pluralize the input word
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Pluralize(this string value)
        {            
            return (!string.IsNullOrWhiteSpace(value) ? Formatting.Pluralize(value) : string.Empty);
        }
        #endregion
    }
}