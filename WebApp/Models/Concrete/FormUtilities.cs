﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text.RegularExpressions;

using log4net;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Domain.Aws.DynamoDB;

using Amazon.DynamoDBv2.Model;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Utility helper class for the form actions
    /// </summary>
    public class FormUtilities
    {
        #region Private Members
        private const string ID_START_TAG = "[id:";
        private static ILog _logger = LogManager.GetLogger(typeof(FormUtilities));
        #endregion

        #region Form Set Methods
        /// <summary>
        /// Rename the form set
        /// </summary>
        /// <param name="formSetId">The ID of the Form Set</param>
        /// <param name="name">Name of the Form Set</param>
        /// <returns></returns>
        public static bool RenameFormSet(int formSetId, string name)
        {
            var result = false;

            try
            {
                // get the record
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var formSet = formSetManager.Get(formSetId);

                    // validate the form set name
                    if (formSet != null && (formSetManager.Get(name) == null || formSet.Name == name))
                    {
                        var oldFormSetName = formSet.Name;

                        // set the name
                        formSet.Name = name;

                        // save the data
                        formSetManager.UnitOfWork.Save();

                        // log the changes
                        using (var formLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>())
                        {
                            formLogManager.LogChangeFormSetName(formSet, oldFormSetName, Utilities.GetUserSession().Username);
                            formLogManager.UnitOfWork.Save();
                        }

                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "An error occurred renaming a form set";
                _logger.Error(msg, ex);
                throw new Exception(msg, ex);
            }

            return result;
        }

        /// <summary>
        /// Delete the form set if it does not contain any form letters
        /// </summary>
        /// <param name="formSetId"></param>
        /// <returns></returns>
        public static bool DeleteFormSetIfEmpty(int formSetId)
        {
            var result = false;

            try
            {
                // get the record
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var formSet = formSetManager.Get(formSetId);

                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        var letterCount = letterManager.All.Count(x => x.PhaseId == formSet.PhaseId && x.FormSetId == formSetId);

                        if (formSet != null && letterCount == 0)
                        {
                            // delete the form set
                            formSetManager.Delete(formSet);
                            formSetManager.UnitOfWork.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "An error occurred deleting an empty form set";
                _logger.Error(msg, ex);
                throw new Exception(msg, ex);
            }

            return result;
        }

        /// <summary>
        /// Rename the form set
        /// </summary>
        /// <param name="masterFormId">The ID of the Master Form Letter</param>
        /// <param name="name">Name of the Form Set</param>
        /// <returns></returns>
        public static int CreateFormSetWithMasterForm(int phaseId, int masterFormId, string name)
        {
            var result = 0;

            try
            {
                // set new form set properties
                var formSet = new FormSet { PhaseId = phaseId, Name = name, MasterFormId = masterFormId };

                // get the record
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    // validate the form set name
                    if (formSetManager.Get(name) == null)
                    {
                        formSetManager.Add(formSet);

                        // save the data
                        formSetManager.UnitOfWork.Save();

                        // if the form set is created, set the letter form set id
                        if (formSet != null && formSet.FormSetId > 0)
                        {
                            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                            {
                                var letter = letterManager.Get(masterFormId);

                                // get the old form set if applicable
                                FormSet oldFormSet = null;

                                if (letter.FormSetId.HasValue)
                                {
                                    oldFormSet = new FormSet { FormSetId = letter.FormSetId.Value, Name = letter.FormSet.Name };
                                }

                                if (letter != null)
                                {
                                    // set the form set id
                                    letter.FormSetId = formSet.FormSetId;
                                    // set the letter type id
                                    letter.LetterTypeId = LookupManager.GetLookup<LookupLetterType>().Where(x => x.Name.Equals("Master Form")).First().LetterTypeId;

                                    // save the data
                                    letterManager.UnitOfWork.Save();

                                    // delete the form set if empty
                                    if (oldFormSet != null)
                                    {
                                        DeleteFormSetIfEmpty(oldFormSet.FormSetId);
                                    }

                                    // log the changes
                                    using (var formLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>())
                                    {
                                        formLogManager.LogChangeFormSet(letter, oldFormSet, Utilities.GetUserSession().Username);
                                        formLogManager.UnitOfWork.Save();
                                    }

                                    result = formSet.FormSetId;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "An error occurred creating a form set with the master form";
                _logger.Error(msg, ex);
                throw new Exception(msg, ex);
            }

            return result;
        }

        /// <summary>
        /// Resort the source form set from the target form set
        /// </summary>
        /// <param name="formSetId">The ID of the Form Set (0 is the unique form set)</param>
        /// <param name="compareToFormSetId">The ID of the Form Set to compare to</param>
        /// <returns></returns>
        public static int ResortFormSet(int formSetId, int compareToFormSetId)
        {
            var letterCount = 0;

            try
            {
                // get the form sets to compare
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    // source form set will be null if it's unique
                    FormSet srcFormSet = null;
                    if (formSetId > 0)
                    {
                        srcFormSet = formSetManager.Get(formSetId);
                    }

                    var destFormSet = formSetManager.Get(compareToFormSetId);

                    // resort the form set
                    letterCount = formSetManager.Resort(srcFormSet, destFormSet, Utilities.GetUserSession().Username);
                    formSetManager.UnitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                var msg = "An error occurred resorting a form set";
                _logger.Error(msg, ex);
                throw new Exception(msg, ex);
            }

            return letterCount;
        }

        #region Group Key Methods
        /// <summary>
        /// Parse the element value from the group key value by type (format: [id:NNN]FormSetName (Total Forms: XX))
        /// </summary>
        /// <param name="groupKeyValue">Value</param>
        /// <param name="type">Key: Key Id, Name: Key Display Name, or Total: Grouped Total</param>
        /// <param name="content">Content Name</param>
        /// <returns></returns>
        public static string ParseElementFromKey(string groupKeyValue, string type = "Key", string content = "")
        {            
            var elementValue = string.Empty;

            if (!string.IsNullOrEmpty(groupKeyValue))
            {
                // search for tag pattern of the comment tag
                var pattern = new Regex(Regex.Escape(ID_START_TAG) + "\\d+" + Regex.Escape("]") + ".*?");

                // find all the matches
                var matches = pattern.Matches(groupKeyValue);

                if (matches != null)
                {
                    // for each match
                    foreach (Match match in matches)
                    {
                        foreach (Capture capture in match.Captures)
                        {
                            switch (type)
                            {
                                case Constants.KEY:
                                    elementValue = capture.Value.Replace(ID_START_TAG, string.Empty).Replace("]", string.Empty);
                                    break;
                                case Constants.NAME:
                                    elementValue = groupKeyValue.Substring(capture.Value.Length);
                                    // get the name portion by stripping out the total count
                                    if (!string.IsNullOrWhiteSpace(elementValue))
                                    {
                                        elementValue = elementValue.Substring(0, elementValue.IndexOf(string.Format("(Total {0}:", content))).Trim();
                                    }
                                    break;
                                case Constants.TOTAL:
                                    elementValue = groupKeyValue.Substring(capture.Value.Length);
                                    // get the name portion by stripping out the form set name
                                    if (!string.IsNullOrWhiteSpace(elementValue))
                                    {
                                        elementValue = elementValue.Substring(elementValue.IndexOf(string.Format("(Total {0}:", content)));
                                    }
                                    break;
                            }

                            break;
                        }
                    }
                }
            }

            return elementValue;
        }
        #endregion
        #endregion

        #region Master Form Methods
        /// <summary>
        /// Update the form set to set the input letter to be the master form letter in the set
        /// </summary>
        /// <param name="letterId">The ID of the Letter</param>
        /// <returns></returns>
        public static bool SetLetterToMaster(int letterId)
        {
            var result = false;

            try
            {
                // get the letter
                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    var letter = letterManager.Get(letterId);

                    if (letter != null)
                    {
                        var oldLetterTypeId = letter.LetterTypeId;
                        var lookupLetterType = LookupManager.GetLookup<LookupLetterType>();

                        // set the letter type id
                        var oldLetterType = lookupLetterType[oldLetterTypeId];
                        var masterFormType = lookupLetterType.First(x => x.Name.Equals("Master Form"));
                        letter.LetterTypeId = masterFormType.LetterTypeId;

                        if (oldLetterTypeId == 1) /* Duplicate */
                        {
                            letter.MasterDuplicateId = null;
                        }
                        
                        //update dynamo
                        DynamoDBHelper dynamo = new DynamoDBHelper();
                        bool success = dynamo.Update(
                            new List<KeyValuePair<string, AttributeValue>>()
                            {
                                new KeyValuePair<string, AttributeValue>
                                    ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                                new KeyValuePair<string, AttributeValue>
                                    ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                            },
                            new List<KeyValuePair<string, AttributeValue>>()
                            {
                                new KeyValuePair<string, AttributeValue>
                                    ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letter.LetterTypeId))
                            }
                        );

                        //check for successful result
                        if (!success)
                        {
                            _logger.Error(String.Format(
                                "Could not update letter ({0}, {1}, {2}) in Dynamo",
                                letter.PhaseId,
                                letter.LetterId,
                                letter.LetterTypeId));
                        }

                        // set master form letter id
                        if (letter.FormSet != null)
                        {
                            letter.FormSet.MasterFormId = letterId;

                            // resort the letters
                            using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                            {
                                formSetManager.Resort(letter.FormSet, letter.FormSet, Utilities.GetUserSession().Username);
                                formSetManager.UnitOfWork.Save();
                            }

                            letter.EarlyActionStatusId = letterManager.DetermineEarlyAttentionStatus(letter);
                            letter.LetterStatusId = letterManager.TransitionLetterStatusByLetterType(oldLetterType, masterFormType, letter.LetterStatusId);
                            letterManager.UpdateEarlyAttentionStatus(letter.FormSetId.Value, letter.EarlyActionStatusId.Value);

                            // save the data
                            letterManager.UnitOfWork.Save();

                            result = true;
                        }

                        // log the changes
                        using (var formLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>())
                        {
                            formLogManager.LogChangeLetterType(letter, oldLetterTypeId, Utilities.GetUserSession().Username);
                            formLogManager.UnitOfWork.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "An error occurred setting a letter to be the master form";
                _logger.Error(msg, ex);
                throw new Exception(msg, ex);
            }

            return result;
        }
        #endregion        

        #region Letter Methods
        /// <summary>
        /// Update the letter to set the input form set
        /// </summary>
        /// <param name="letterId">The ID of the Letter</param>
        /// <param name="formSetId">The ID of the Form Set</param>
        /// <returns></returns>
        public static bool UpdateLetterFormSet(int letterId, int formSetId)
        {
            var result = false;
            var lookupLetterType = LookupManager.GetLookup<LookupLetterType>();

            try
            {
                // get the letter
                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    var letter = letterManager.Get(letterId);
                    var oldLetterTypeId = letter.LetterTypeId;

                    // get the new formset
                    using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                    {
                        formSetManager.UnitOfWork = letterManager.UnitOfWork;
                        var formSet = formSetManager.Get(formSetId);

                        if (letter != null && formSet != null)
                        {
                            // get the old form set
                            FormSet oldFormSet = null;

                            if (letter.FormSet != null)
                            {
                                oldFormSet = new FormSet { FormSetId = letter.FormSet.FormSetId, PhaseId = letter.PhaseId, Name = letter.FormSet.Name, };

                                // if it's the last letter in the form set, delete the form set
                                var letterCount = letterManager.All.Count(x => x.PhaseId == letter.PhaseId && x.FormSetId == letter.FormSet.FormSetId);
                                if (letterCount == 1)
                                {
                                    formSetManager.Delete(letter.FormSet);
                                }
                            }

                            // set the form set id
                            // turning lazy loading off prevents fixup collection from trying to load all letters in a formset
                            // when you change the formsetid
                            letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;
                            letter.FormSetId = formSetId;
                            letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = true;

                            // compare with the new form set master and set the letter type id
                            letter.LetterTypeId = letterManager.DetectForm(letter,
                                new List<LetterSenderComposite>
                                {
							        new LetterSenderComposite(formSet.MasterFormLetter)
						        }).LetterTypeId;

                            letterManager.InheritPropertiesFromMasterForm(letter, letter.LetterTypeId == 4 /* Form */);
                            letter.EarlyActionStatusId = letterManager.TransitionEarlyActionStatusByLetterType(lookupLetterType[oldLetterTypeId], lookupLetterType[letter.LetterTypeId], letter);
                            letter.LetterStatusId = letterManager.TransitionLetterStatusByLetterType(lookupLetterType[oldLetterTypeId], lookupLetterType[letter.LetterTypeId], letter.LetterStatusId);

                            // save the data
                            letterManager.UnitOfWork.Save();

                            DynamoDBHelper dynamo = new DynamoDBHelper();

                            bool success = dynamo.Update(
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    new KeyValuePair<string, AttributeValue>
                                        ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                                    new KeyValuePair<string, AttributeValue>
                                        ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                                },
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    new KeyValuePair<string, AttributeValue>
                                        ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letter.LetterTypeId)),
                                    new KeyValuePair<string, AttributeValue>
                                        ("FormSetId", DynamoDBHelper.GetAttributeValue(formSetId))
                                }
                            );

                            //check for successful result
                            if (!success)
                            {
                                _logger.Error(String.Format(
                                    "Could not update letter's ({0}, {1}, {2}, {3}) in Dynamo",
                                    letter.PhaseId,
                                    letter.LetterId,
                                    letter.LetterTypeId,
                                    formSetId));
                            }

                            // log the changes
                            using (var formLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>())
                            {
                                formLogManager.LogChangeFormSet(letter, oldFormSet, Utilities.GetUserSession().Username);
                                formLogManager.UnitOfWork.Save();
                            }

                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "An error occurred sending a letter to another form set";
                _logger.Error(msg, ex);
                throw new Exception(msg, ex);
            }

            return result;
        }
        #endregion
    }
}