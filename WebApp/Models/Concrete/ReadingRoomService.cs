﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Objects;
using System.Linq;

using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using Telerik.Web.Mvc.Infrastructure;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Service class for the Reading Room operations
    /// </summary>
    public class ReadingRoomService
    {
        #region List Control Methods
        /// <summary>
        /// Retrieves a collection of the reading room search results by grid command
        /// </summary>
        /// <param name="command">Grid Command</param>
        /// <param name="count">Output of the total count</param>
        /// <returns></returns>
        public static ICollection<LetterReadingRoomSearchResult> GetLettersReadingRoom(string filterName, GridCommand command, string context, out int total, out int pageNum)
        {
            // get the filter
            var filter = Utilities.GetFilter(filterName, context) as LetterReadingRoomSearchResultFilter;
            if (filter == null)
            {
				filter = new LetterReadingRoomSearchResultFilter { PageNum = 1 };
            }

            // set the paging
            filter.PageNum = command.Page;
            filter.NumRows = command.PageSize;

            // set the sort key
            foreach (var sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            // get the data
            IList<LetterReadingRoomSearchResult> data;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                data = letterManager.GetLettersReadingRoom(filter, out total);

                // Convert the time to the letter's time
                if (data != null && data.Count > 0)
                {
                    TimeZoneInfo tzi;
                    foreach (var letter in data)
                    {
                        if (!string.IsNullOrEmpty(letter.TimeZone))
                        {
                            tzi = TimeZoneInfo.FindSystemTimeZoneById(letter.TimeZone);
                            letter.DateSubmitted = TimeZoneInfo.ConvertTimeFromUtc(letter.DateSubmitted, tzi);
                        }
                    }
                }
            }

			pageNum = command.Page;
			filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.READING_ROOM_MGMT, context, filter);

            // return the data as a list
            return data;
        }

        /// <summary>
        /// Retrieves a collection of the public reading room search result groups by grid command
        /// </summary>
        /// <param name="filterName"></param>
        /// <param name="command"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IEnumerable GetPublicReadingRoomLetters(string filterName, GridCommand command, string context, out int total, out int pageNum)
        {
            // get the filter  
            var filter = Utilities.GetFilter(filterName, context) as LetterReadingRoomSearchResultFilter;
            if (filter == null)
            {
				filter = new LetterReadingRoomSearchResultFilter { PageNum = 1 };
            }

            // set the paging
            filter.PageNum = (command.Page > 0 ? command.Page : 1);
            filter.NumRows = filter.NumRows;

            // set the sort key
            foreach (SortDescriptor sortDescriptor in command.SortDescriptors)
            {
                filter.SortKey = string.Format("{0} {1}", sortDescriptor.Member, sortDescriptor.SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC");
            }

            // get the data
            IList<LetterReadingRoomSearchResult> data;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                data = letterManager.GetLettersReadingRoom(filter, out total);
            }

            // Convert to timezone
            if (data != null && data.Count > 0)
            {
                foreach (var letter in data)
                {
                    if (!string.IsNullOrEmpty(letter.TimeZone))
                    {
                        var tzi = TimeZoneInfo.FindSystemTimeZoneById(letter.TimeZone);
                        letter.DateSubmitted = TimeZoneInfo.ConvertTimeFromUtc(letter.DateSubmitted, tzi);
                    }
                }
            }

            // always apply the key name
            if (!command.GroupDescriptors.Any())
            {
                command.GroupDescriptors.Add(new GroupDescriptor { Member = Constants.READING_ROOM });
            }

			pageNum = command.Page;
			filter.PageNum = command.Page;

			Utilities.SetFilter(Constants.READING_ROOM_MGMT, context, filter);
			
			// apply grouping and return the result
            if (command.GroupDescriptors.Any())
            {
                return ApplyGrouping(data.AsQueryable(), command.GroupDescriptors);
            }

            // return the data as a list
            return data;
        }

        private static IEnumerable<AggregateFunctionsGroup> ApplyGrouping(IQueryable<LetterReadingRoomSearchResult> data, IList<GroupDescriptor> groupDescriptors)
        {
            Func<IEnumerable<LetterReadingRoomSearchResult>, IEnumerable<AggregateFunctionsGroup>> selector = null;

            // apply the grouping
            foreach (var group in groupDescriptors.Reverse())
            {
                if (selector == null)
                {
                    if (group.Member == Constants.READING_ROOM)
                    {
                        selector = g => BuildInnerGroup(g.Select(p => p), p => p.ReadingRoomKeyCount, i => i.ToList());
                    }
                }
                else
                {
                    if (group.Member == Constants.READING_ROOM)
                    {
                        var tempSelector = selector;
                        selector = g => g.GroupBy(p => p.ReadingRoomKeyCount)
                                         .Select(c => new AggregateFunctionsGroup
                                         {
                                             Key = c.Key,
                                             HasSubgroups = true,
                                             Items = tempSelector.Invoke(c).ToList()
                                         });
                    }
                }
            }

            return selector.Invoke(data).ToList();
        }
                
        private static IEnumerable<AggregateFunctionsGroup> BuildInnerGroup<U>(IEnumerable<LetterReadingRoomSearchResult> group, Func<LetterReadingRoomSearchResult, U> groupSelector,
            Func<IEnumerable<LetterReadingRoomSearchResult>, IEnumerable> innerSelector)
        {
            return group.GroupBy(groupSelector)
                    .Select(i => new AggregateFunctionsGroup
                    {
                        Key = i.Key,
                        Items = innerSelector(i)
                    });
        }
        #endregion

        #region Public Reading Room Methods
        /// <summary>
        /// Returns the xml string of the reading room active status result
        /// </summary>
        /// <param name="active"></param>
        /// <returns></returns>
        public static string GetReadingRoomActiveResultXml(bool active)
        {
            return string.Format(@"<publicReadingRoom><active>{0}</active></publicReadingRoom>",
                active.ToString()
                );
        }
        #endregion
    }
}