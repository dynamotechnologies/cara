﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.IO;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Custom action filter attribute which checks for the captcha input from user
    /// </summary>
    public class RecaptchaValidatorAttribute : ActionFilterAttribute
    {
        #region Constants
        private const string CHALLENGE_FIELD_KEY = "recaptcha_challenge_field";
        private const string RESPONSE_FIELD_KEY = "recaptcha_response_field";
        #endregion

        #region Action Methods
        public override void OnActionExecuting(ActionExecutingContext filterContext)   
        {
            filterContext.ActionParameters["captchaValid"] = Utilities.IsRecaptchaValid(filterContext.HttpContext);
           
            base.OnActionExecuting(filterContext);
        }
  

        #endregion

    }
}