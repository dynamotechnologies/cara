﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Custom authorize attribute which checks for the user privileges with the privilege 
    /// codes defined in the attribute property
    /// </summary>
    public class HasPrivilegeAttribute : AuthorizeAttribute
    {
        #region Class Members
        public string Scope { get; set; }
        public string PrivilegeCodes { get; set; }
        #endregion

        #region Override Methods
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // validate the http context object
            if (httpContext == null)
            {
                throw new ArgumentNullException("HttpContext is empty");
            }

            return Utilities.UserHasPrivilege(this.PrivilegeCodes, this.Scope);
        }

        /// <summary>
        /// Display the view imforming the user of an access violation
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                // redirect to the unauthorized page
                filterContext.Result = new RedirectResult("/Error/NotAuthorized");
            }
        }
        #endregion
    }
}