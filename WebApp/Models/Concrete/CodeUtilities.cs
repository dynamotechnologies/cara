﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Utility helper class for the coding
    /// </summary>
    public class CodeUtilities
    {
        #region Code Entity Methods
        /// <summary>
        /// Converts the Code to PhaseCode
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="code">Code</param>
        /// <returns></returns>
        public static PhaseCode ToPhaseCode(int phaseId, Code code)
        {
            PhaseCode phaseCode = null;

            if (code != null)
            {
                // create a phase code from code
                phaseCode = new PhaseCode
                {
                    CodeId = code.CodeId,
                    PhaseCodeId = -1,
                    CodeNumber = code.CodeNumber,
                    CodeName = code.Name,
                    ParentCodeId = code.ParentCodeId,
                    CodeCategoryId = code.CodeCategoryId,
                    PhaseId = phaseId
                };
            }

            return phaseCode;
        }
        #endregion

        #region Phase Code Input Methods
        /// <summary>
        /// Get and parse the expanded nodes of the key in the request cookie
        /// </summary>
        /// <param name="request">HTTP Request</param>
        /// <param name="key">The key of the Cookie</param>
        /// <returns></returns>
        public static string[] GetExpandedNodes(HttpRequestBase request, string key)
        {
            var nodes = new string[] { };

            if (request != null && !string.IsNullOrWhiteSpace(key))
            {
                nodes = request.Cookies[key] != null ? HttpUtility.UrlDecode(request.Cookies[key].Value).Split(';') : new string[] { };
            }               

            return nodes;
        }

        /// <summary>
        /// Set the expanded nodes of the key in the request cookie
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <param name="values"></param>
        public static void SetExpandedNodes(HttpRequestBase request, string key, string value)
        {
            if (request != null && !string.IsNullOrWhiteSpace(key))
            {
                // add the cookie if not exist
                if (request.Cookies[key] == null)
                {
                    request.Cookies.Add(new HttpCookie(key));
                }

                // set the value
                request.Cookies[key].Value = value;
            }
        }
        #endregion

        #region Phase Code Tree Methods

        #region Public Methods
        /// <summary>
        /// Returns the PhaseCodes in a tree view structure
        /// </summary>
        /// <param name="phaseCodes"></param>
        /// <returns></returns>
        public static ICollection<PhaseCode> ToPhaseCodeTree(ICollection<PhaseCode> phaseCodes)
        {
            // tree structure to be returned
            var tree = new List<PhaseCode>();
            // store a list of populated tree codes
            var treeCodes = new List<PhaseCode>();

            if (phaseCodes != null)
            {
                // get the top parent codes
                var parents = phaseCodes.Where(x => x.CodeNumberDisplay.Length <= 3).ToList();

                foreach (var parent in parents)
                {
                    PopulateChildren(parent, phaseCodes, treeCodes);

                    tree.Add(parent);
                    treeCodes.Add(parent);
                }

                // add any codes that don't have direct parent (i.e. when the structure has only codes associated with a phase)
                foreach (var code in phaseCodes.Except(treeCodes))
                {
                    PopulateChildren(code, phaseCodes, treeCodes);

                    // add the code if not already populated
                    if (!treeCodes.Any(x => x.CodeId == code.CodeId))
                    {
                        tree.Add(code);
                    }
                }
            }

            return tree.OrderBy(x => x.CodeNumber).ToList();
        }

        /// <summary>
        /// Returns the PhaseCodeSelects in a tree view structure
        /// </summary>
        /// <param name="phaseCodes"></param>
        /// <returns></returns>
        public static ICollection<PhaseCodeSelect> ToPhaseCodeTree(ICollection<PhaseCodeSelect> phaseCodes)
        {
            // tree structure to be returned
            var tree = new List<PhaseCodeSelect>();
            // store a list of populated tree codes
            var treeCodes = new List<PhaseCodeSelect>();

            if (phaseCodes != null)
            {
                // get the top parent codes
                var parents = phaseCodes.Where(x => x.CodeNumberDisplay.Length <= 3).ToList();

                foreach (var parent in parents)
                {
                    if (parent.CodeId.HasValue)
                    {
                        PopulateChildren(parent, phaseCodes, treeCodes);
                    }

                    tree.Add(parent);
                    treeCodes.Add(parent);
                }

                // add any codes that don't have direct parent (i.e. when the structure has only codes associated with a phase)
                foreach (var code in phaseCodes.Except(treeCodes))
                {
                    PopulateChildren(code, phaseCodes, treeCodes);

                    // add the code if not already populated
                    if (!treeCodes.Any(x => x.CodeId == code.CodeId))
                    {
                        tree.Add(code);
                    }
                }
            }

            return tree.OrderBy(x => x.CodeNumber).ToList();
        }

        /// <summary>
        /// Returns the TemplateCodeSelects in a tree view structure
        /// </summary>
        /// <param name="phaseCodes"></param>
        /// <returns></returns>
        public static ICollection<TemplateCodeSelect> ToTemplateCodeTree(ICollection<TemplateCodeSelect> templateCodes)
        {
            // tree structure to be returned
            var tree = new List<TemplateCodeSelect>();
            // store a list of populated tree codes
            var treeCodes = new List<TemplateCodeSelect>();

            if (templateCodes != null)
            {
                // get the top parent codes
                var parents = templateCodes.Where(x => x.CodeNumberDisplay.Length <= 3).ToList();

                foreach (var parent in parents)
                {
                    if (parent.CodeId > 0)
                    {
                        PopulateChildren(parent, templateCodes, treeCodes);
                    }

                    tree.Add(parent);
                    treeCodes.Add(parent);
                }

                // add any codes that don't have direct parent (i.e. when the structure has only codes associated with a phase)
                foreach (var code in templateCodes.Except(treeCodes))
                {
                    PopulateChildren(code, templateCodes, treeCodes);

                    // add the code if not already populated
                    if (!treeCodes.Any(x => x.CodeId == code.CodeId))
                    {
                        tree.Add(code);
                    }
                }
            }

            return tree.OrderBy(x => x.CodeNumber).ToList();
        }

        /// <summary>
        /// Returns the TemplateCodeSelects in a tree view structure
        /// </summary>
        /// <param name="templateCodes"></param>
        /// <returns></returns>
        public static ICollection<TemplateCodeSelect> ToTemplateCodeTreeByType(ICollection<TemplateCodeSelect> templateCodes)
        {
            // tree structure to be returned
            var tree = new List<TemplateCodeSelect>();

            if (templateCodes != null)
            {
                // group all codes by prefix
                var categories = templateCodes.GroupBy(x => x.CodeCategoryId);

                // populate the tree view for each group
                foreach (var category in categories)
                {
                    // create category group header in phase code object
                    var categoryCode = new TemplateCodeSelect
                    {
                        CodeNumber = LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.CodeCategoryId == category.Key).FirstOrDefault().Name,
                        Name = string.Empty,
                        CodeId = 0
                    };

                    // popupate the tree view and add to the group header
                    categoryCode.ChildCodes = ToTemplateCodeTree((from code in category select code).ToList());

                    // add the tree as a child tot the group
                    tree.Add(categoryCode);
                }
            }

            return tree;
        }

        /// <summary>
        /// Returns the PhaseCodes in a tree view structure
        /// </summary>
        /// <param name="phaseCodes"></param>
        /// <returns></returns>
        public static ICollection<PhaseCode> ToPhaseCodeTreeByType(ICollection<PhaseCode> phaseCodes)
        {
            // tree structure to be returned
            var tree = new List<PhaseCode>();

            if (phaseCodes != null)
            {
                // group all codes by prefix
                var categories = phaseCodes.GroupBy(x => x.CodeCategory.CodeCategoryId);

                // populate the tree view for each group
                foreach (var category in categories)
                {
                    // get the code category by prefix
                    var codeCategory = LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.CodeCategoryId == category.Key).FirstOrDefault();

                    // create category group header in phase code object
                    var categoryCode = new PhaseCode
                    {
                        CodeNumber = codeCategory.Name,
                        CodeName = string.Empty,
                        PhaseCodeId = 0,
                        CodeId = 0,
                        CodeCategoryId = codeCategory.CodeCategoryId,
                        UserId = null
                    };

                    // popupate the tree view and add to the group header
                    categoryCode.ChildCodes = ToPhaseCodeTree((from code in category select code).OrderBy(x => x.CodeNumber).ToList());

                    // add the tree as a child to the group
                    tree.Add(categoryCode);
                }
            }

            return tree.OrderBy(x => x.CodeCategory.Prefix).ToList();
        }

        /// <summary>
        /// Returns the PhaseCodeSelects in a tree view structure
        /// </summary>
        /// <param name="phaseCodes">Phase Codes</param>
        /// <param name="phaseTypeId">Phase Type ID (the categories will be populated based on the value provided)</param>
        /// <returns></returns>
        public static ICollection<PhaseCodeSelect> ToPhaseCodeTreeByType(ICollection<PhaseCodeSelect> phaseCodes, int? phaseTypeId = null)
        {
            // tree structure to be returned
            var tree = new List<PhaseCodeSelect>();
            var codeCategoryId = new List<int>();

            if (phaseCodes != null)
            {
                // group all codes by prefix
                var categories = phaseCodes.GroupBy(x => x.CodeCategory.CodeCategoryId).ToList();

                // populate the tree view for each group
                foreach (var category in categories)
                {
                    // get the code category by prefix
                    var codeCategory = LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.CodeCategoryId == category.Key).FirstOrDefault();

                    // create category group header in phase code object
                    var categoryCode = new PhaseCodeSelect
                    {
                        CodeNumber = codeCategory.Name,
                        CodeName = string.Empty,
                        PhaseCodeId = 0,
                        CodeId = 0,
                        CodeCategoryId = codeCategory.CodeCategoryId
                    };

                    // add the prefixes
                    codeCategoryId.Add(category.Key);

                    // popupate the tree view and add to the group header
                    categoryCode.ChildCodes = ToPhaseCodeTree((from code in category select code).OrderBy(x => x.CodeNumber).ToList());

                    // add the tree as a child tot the group
                    tree.Add(categoryCode);
                }               
            }

            // ------------------------------------------------------------------------------------------------
            // if phase type id is provided, add the categories associated with the phase type
            // that was not already added based on phase codes
            // ------------------------------------------------------------------------------------------------
            if (phaseTypeId.HasValue)
            {
                var categories = from x in LookupManager.GetLookup<LookupPhaseType>().Where(x => x.PhaseTypeId == phaseTypeId.Value)
                                     .FirstOrDefault().CodeCategories where !codeCategoryId.Contains(x.CodeCategoryId) select x;

                foreach (var category in categories)
                {
                    tree.Add(new PhaseCodeSelect
                    {
                        CodeNumber = category.Name,
                        CodeName = string.Empty,
                        PhaseCodeId = 0,
                        CodeId = 0,
                        CodeCategoryId = Convert.ToInt32(category.CodeCategoryId)
                    });
                }
            }
            List<PhaseCodeSelect> retVal = tree.OrderBy(x => x.CodeCategory.Prefix).ToList();
            return retVal;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Populate the children codes for the current phase code recursively
        /// </summary>
        /// <param name="node"></param>
        /// <param name="phaseCodes"></param>
        private static void PopulateChildren(PhaseCode node, ICollection<PhaseCode> phaseCodes, ICollection<PhaseCode> treeCodes)
        {
            ICollection<PhaseCode> codes = null;

            if (node != null)
            {
                // Format: NNN.MMCCDD (the conditional one character addition depends on whether it's the first level code)
                var childCodeNumberLength = GetChildCodeNumberLength(node.CodeNumberDisplay);

                // get a list of direct child codes (e.g. if NNN is parent, get NNN.XX)
                codes = phaseCodes.Where(x => x.CodeNumberDisplay.StartsWith(node.CodeNumberDisplay) && x.CodeNumberDisplay.Length == childCodeNumberLength)
                    .OrderBy(x => x.CodeNumber).ToList();

                // add the child codes to the current code
                node.ChildCodes = codes;                

                // iterate through each codes to populate their child codes
                if (codes != null)
                {
                    foreach (var code in codes)
                    {
                        PopulateChildren(code, phaseCodes, treeCodes);

                        treeCodes.Add(code);
                    }
                }
            }
        }

        /// <summary>
        /// Populate the children codes for the current phase code recursively
        /// </summary>
        /// <param name="node"></param>
        /// <param name="phaseCodes"></param>
        /// <param name="treeCodes"></param>
        private static void PopulateChildren(PhaseCodeSelect node, ICollection<PhaseCodeSelect> phaseCodes, ICollection<PhaseCodeSelect> treeCodes)
        {
            ICollection<PhaseCodeSelect> codes = null;            

            if (node != null)
            {
                // Format: NNN.MMCCDD (the conditional one character addition depends on whether it's the first level code)
                int childCodeNumberLength = GetChildCodeNumberLength(node.CodeNumberDisplay);

                // get a list of direct child codes (e.g. if NNN is parent, get NNN.XX)
                codes = phaseCodes.Where(x => x.CodeNumberDisplay.StartsWith(node.CodeNumberDisplay) && x.CodeNumberDisplay.Length == childCodeNumberLength)
                    .OrderBy(x => x.CodeNumber).ToList();

                // add the child codes to the current code
                node.ChildCodes = codes;

                // iterate through each codes to populate their child codes
                if (codes != null)
                {
                    foreach (var code in codes)
                    {
                        if (code.CodeId.HasValue)
                        {
                            PopulateChildren(code, phaseCodes, treeCodes);

                            treeCodes.Add(code);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Populate the children codes for the current phase code recursively
        /// </summary>
        /// <param name="node"></param>
        /// <param name="phaseCodes"></param>
        /// <param name="treeCodes"></param>
        private static void PopulateChildren(TemplateCodeSelect node, ICollection<TemplateCodeSelect> phaseCodes, ICollection<TemplateCodeSelect> treeCodes)
        {
            ICollection<TemplateCodeSelect> codes = null;

            if (node != null)
            {
                // Format: NNN.MMCC.DD (the conditional one character addition depends on whether it's the first level code)
                var childCodeNumberLength = GetChildCodeNumberLength(node.CodeNumberDisplay);

                // get a list of direct child codes (e.g. if NNN is parent, get NNN.XX)
                codes = phaseCodes.Where(x => x.CodeNumberDisplay.StartsWith(node.CodeNumberDisplay) && x.CodeNumberDisplay.Length == childCodeNumberLength)
                    .OrderBy(x => x.CodeNumber).ToList();

                // add the child codes to the current code
                node.ChildCodes = codes;

                // iterate through each codes to populate their child codes
                if (codes != null)
                {
                    foreach (var code in codes)
                    {
                        PopulateChildren(code, phaseCodes, treeCodes);

                        treeCodes.Add(code);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the length of the child code number of the input code number
        /// </summary>
        /// <param name="codeNumber"></param>
        /// <returns></returns>
        private static int GetChildCodeNumberLength(string codeNumber)
        {
            return (!string.IsNullOrEmpty(codeNumber) ? codeNumber.Length + 2 + (codeNumber.Length == 3 || codeNumber.Length == 8 ? 1 : 0) : 0);
        }
        #endregion

        #endregion
        
        #region Code Number Methods
        /// <summary>
        /// Gets the next available code number in the phase of the selected code category and level
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="codeCategoryId"></param>
        /// <param name="selectedCodeNumber"></param>
        /// <returns></returns>
        public static string GetNextCodeNumber(int phaseId, int codeCategoryId, string selectedCodeNumber)
        {
            const string emptyCode = "000.0000.00";
            var number = string.Empty;
            // get the selected code length (set it to the top level if no number is provided)
            var codeLength = (!string.IsNullOrEmpty(selectedCodeNumber) ? selectedCodeNumber.Length : 3);
            // find the ending code number to match
            var endingCodeNumber = emptyCode.Substring(codeLength);

            // get all codes in the category at the selected code level
            IList<PhaseCodeSelect> codes;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                codes = phaseManager.GetPhaseCodes(
                    new PhaseCodeSelectFilter
                    {
                        PhaseId = phaseId
                    }
                )
                .Where(x => x.CodeCategoryId == codeCategoryId && x.CodeNumber.EndsWith(endingCodeNumber))
                .OrderBy(x => x.CodeNumber).ToList();
            }

            // search for the next available code number
            if (codes != null && codes.Count > 0)
            {
                var found = false;
                // get the code with the lowest code number
                var nextCodeNumber = GetNextCodeNumber(codes.FirstOrDefault().CodeNumber);

                while (!found && !string.IsNullOrEmpty(nextCodeNumber))
                {
                    found = (!codes.Any(x => x.CodeNumber.Equals(nextCodeNumber)));

                    if (found)
                    {
                        number = nextCodeNumber;
                    }
                    else
                    {
                        nextCodeNumber = GetNextCodeNumber(nextCodeNumber);
                    }
                }
            }
            else
            {
                // if no number is available, set the number to the first available number in the code category
                // get the prefix
                var prefix = LookupManager.GetLookup<LookupCodeCategory>()[codeCategoryId].Prefix;

                if (!string.IsNullOrEmpty(prefix))
                {
                    number = string.Format("{0}01.0000.00", prefix);
                }
            }

            return ToDisplayCodeNumber(number);
        }

        /// <summary>
        /// Gets the next available code number of the same level
        /// </summary>
        /// <param name="codeNumber"></param>
        /// <returns></returns>
        private static string GetNextCodeNumber(string codeNumber)
        {
            var nextNumber = string.Empty;

            if (!string.IsNullOrEmpty(codeNumber))
            {
                // get the code number and remove the dots
                var num = ToDisplayCodeNumber(codeNumber).Replace(".", string.Empty);
                
                // parse the code number into an int so that it can be incremented
                var i = 0;

                // if the number can be parsed, increment by one and set the number to be returned
                if (int.TryParse(num, out i))
                {
                    // ensure the code number is still the same level
                    if ((i++).ToString().Length <= codeNumber.Length)
                    {
                        nextNumber = ToCodeNumber(i.ToString());
                    }
                }
            }

            return nextNumber;
        }

        /// <summary>
        /// Returns the next available child code number of the phase and parent code
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="parentCode"></param>
        /// <returns></returns>
        public static string GetNextChildCodeNumber(int phaseId, Code parentCode)
        {
            var codeNumber = string.Empty;

            if (parentCode != null && !string.IsNullOrEmpty(parentCode.CodeNumberDisplay))
            {
                var currCode = string.Empty;

                // find the last direct child of the parent in the phase
                PhaseCodeSelect code;
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    code = phaseManager.GetPhaseCodes(new PhaseCodeSelectFilter
                        {
                            PhaseId = phaseId
                        })
                        .Where(x => x.ParentCodeId == parentCode.CodeId && x.CodeNumber != parentCode.CodeNumber)
                        .OrderByDescending(x => x.CodeNumber).FirstOrDefault();
                }

                // if code is found, get the last two characters
                if (code != null)
                {
                    currCode = code.CodeNumberDisplay.Substring(code.CodeNumberDisplay.Length - 2);
                }
                // otherwise starts with a new sequence
                else
                {
                    currCode = "00";
                }

                if (!string.IsNullOrEmpty(currCode))
                {
                    var codeNum = 0;

                    // convert it to int and increment it by 1
                    if (int.TryParse(currCode, out codeNum))
                    {
                        // validate the sequence
                        if (codeNum < 99)
                        {
                            codeNum++;

                            // set the next code number
                            codeNumber = parentCode.CodeNumberDisplay + ((parentCode.CodeNumberDisplay.Length == 3 || parentCode.CodeNumberDisplay.Length == 8) ? "." : string.Empty) +
                                codeNum.ToString().PadLeft(2, '0');
                        }
                    }
                }
            }

            return codeNumber;
        }

        /// <summary>
        /// Returns the code number in display format
        /// </summary>
        /// <param name="codeNumber"></param>
        /// <returns></returns>
        public static string ToDisplayCodeNumber(string codeNumber)
        {
            return (!string.IsNullOrEmpty(codeNumber) ? codeNumber.Replace(".0000.00", string.Empty).Replace("00.00", string.Empty)
                .Replace(".00", string.Empty) : string.Empty);
        }

        /// <summary>
        /// Returns the code number in full number format (NNN.NNNN.NN)
        /// </summary>
        /// <param name="codeNumberDisplay"></param>
        /// <returns></returns>
        public static string ToCodeNumber(string codeNumberDisplay)
        {
            // pad the number with digit and add the dots
            return (!string.IsNullOrEmpty(codeNumberDisplay) ? codeNumberDisplay.Replace(".", string.Empty)
                .PadRight(9, '0').Insert(3, ".").Insert(8, ".") : string.Empty);
        }
        #endregion
    }
}