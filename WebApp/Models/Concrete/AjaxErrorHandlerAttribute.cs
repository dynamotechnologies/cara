﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using log4net;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Custom authorize attribute which checks for the user privileges with the privilege 
    /// codes defined in the attribute property
    /// </summary>
    public class AjaxErrorHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Handles the exception
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnException(ExceptionContext filterContext)
        {
            // handle the exception   
            if (filterContext.Exception != null)
            {
                // log the exception output
                ILog log = LogManager.GetLogger(this.GetType());
                log.Error("Error occurred in the ajax action", filterContext.Exception);

                filterContext.Result = AjaxError(filterContext.Exception.Message, filterContext);
                filterContext.ExceptionHandled = true;
                
                // Set the response status code to 500
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                // Needed for IIS7.0
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            }
        }

        /// <summary>
        /// Returns an ajax error object with the exception included in the filter content
        /// </summary>
        /// <param name="message"></param>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        protected JsonResult AjaxError(string message, ExceptionContext filterContext)
        {
            // If message is null or empty, then fill with generic message
            if (string.IsNullOrEmpty(message))
            {
                message = "An error occurred while processing your request. Please refresh the page and try again.";
            }

            return new JsonResult
            {
                Data = new { data = message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}