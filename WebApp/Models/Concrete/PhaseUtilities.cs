﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    public static class PhaseUtilities
    {
        /// <summary>
        /// Returns true if the input comment period does not fall within any existing phases of the project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="phaseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static bool IsValidatePeriod(int projectId, int phaseId, DateTime startDate, DateTime endDate)
        {
            bool result = true;

            // get the project
            Project project;
            IList<Phase> phases;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.Get(projectId);
                phases = project.Phases.ToList();
            }

            // check all existing phases to make sure the start/end date falls outside of the phase
            if (project != null && project.Phases != null)
            {
                foreach (Phase phase in phases.Where(x => x.PhaseId != phaseId))
                {
                    result = (result && ((startDate < phase.CommentStart && endDate < phase.CommentStart) ||
                        (startDate > phase.CommentEnd && endDate > phase.CommentEnd)));
                }
            }

            return result;
        }
    }
}