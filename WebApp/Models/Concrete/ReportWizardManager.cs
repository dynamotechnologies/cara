﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using log4net;

using Aquilent.Cara.Configuration;
using Aquilent.Cara.WebApp.Models.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    public class ReportWizardManager
    {
        #region Class Members
        // private static instance of the class
        private static ReportWizardManager instance = null;

        // lists for create and edit wizard steps
        private LinkedList<WizardStepElement> StandardReportWizard;
        private LinkedList<WizardStepElement> CustomReportWizard;
        private LinkedList<WizardStepElement> MyReportWizard;
        #endregion

        #region Properties
        /// <summary>
        /// Get instance property
        /// </summary>
        public static ReportWizardManager Instance
        {
            get{
                if (instance == null)
                {
                    instance = new ReportWizardManager();
                    instance.Init();
                }

                return instance;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialize the steps by reading the config section and populate the wizard step lists
        /// </summary>
        private void Init()
        {
            // read the configuration
            var section = ConfigUtilities.GetSection<WizardStepConfiguration>("wizardStepConfiguration");
            if (section == null)
            {
                ILog logger = LogManager.GetLogger(typeof(ProjectWizardManager));
                logger.Fatal("The configuration section, 'wizardStepConfiguration', could not be found");
            }

            // populate the wizard steps by mode
            StandardReportWizard = new LinkedList<WizardStepElement>(section.Steps.Cast<WizardStepElement>().Where(x => x.Modes.Contains(PageMode.StandardReportWizard.ToString())).ToList());
            CustomReportWizard = new LinkedList<WizardStepElement>(section.Steps.Cast<WizardStepElement>().Where(x => x.Modes.Contains(PageMode.CustomReportWizard.ToString())).ToList());
            MyReportWizard = new LinkedList<WizardStepElement>(section.Steps.Cast<WizardStepElement>().Where(x => x.Modes.Contains(PageMode.MyReportWizard.ToString())).ToList());
        }

        /// <summary>
        /// Get the step of the input mode and step
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="currStep"></param>
        /// <returns></returns>
        public WizardStepElement GetStep(PageMode mode, string currStep)
        {
            var step = new WizardStepElement();
            step.Name = currStep;

            return GetWizard(mode).Find(step).Value;
        }

        /// <summary>
        /// Get the previous step of the input mode and step
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="currStep"></param>
        /// <returns></returns>
        public WizardStepElement GetPrevWizardStep(PageMode mode, string currStep)
        {
            var step = new WizardStepElement();
            step.Name = currStep;

            return GetWizard(mode).Find(step).Previous.Value;
        }

        /// <summary>
        /// Get the next step of the input mode and step
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="currStep"></param>
        /// <returns></returns>
        public WizardStepElement GetNextWizardStep(PageMode mode, string currStep)
        {
            var step = new WizardStepElement();
            step.Name = currStep;

            return GetWizard(mode).Find(step).Next.Value;
        }

        /// <summary>
        /// Return the step number display for the input wizard
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="currStep"></param>
        /// <returns></returns>
        public string GetStepNumber(PageMode mode, string currStep)
        {
            return string.Format("({0} of {1})", GetWizard(mode).ToList().FindIndex(item => item.Name.Equals(currStep)) + 1,
                GetWizard(mode).Count.ToString());
        }

        /// <summary>
        /// Get the wizard step list of the input mode
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public LinkedList<WizardStepElement> GetWizard(PageMode mode)
        {
            return (mode.Equals(PageMode.StandardReportWizard) ? StandardReportWizard : (mode.Equals(PageMode.CustomReportWizard) ? CustomReportWizard :
                MyReportWizard));
        }
        #endregion
    }

    /// <summary>
    /// Represents a project wizard entity
    /// </summary>
    public class ReportWizard
    {
        #region Class Members
        public LinkedList<WizardStepElement> WizardSteps { get; set; }
        public string CurrStep { get; set; }
        public PageMode Mode { get; set; }
        #endregion

        #region Constructors
        public ReportWizard(PageMode mode)
        {
            this.CurrStep = string.Empty;
            this.Mode = mode;
            this.WizardSteps = ProjectWizardManager.Instance.GetWizard(mode);
        }

        public ReportWizard(PageMode mode, string currStep)
        {
            this.CurrStep = currStep;
            this.Mode = mode;
            this.WizardSteps = ProjectWizardManager.Instance.GetWizard(mode);
        }
        #endregion
    }
}