﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Domain.Aws.DynamoDB;

using Amazon.DynamoDBv2.Model;
using log4net;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Utility helper class for the letters
    /// </summary>
    public class LetterUtilities
    {
        #region Constants
        public const string LINK_SYMBOL = "[...]";
        public const string COMMENT_START_TAG = "[comment:";
        public const string COMMENT_END_TAG = "[comment end]";
        public const string AUTO_MARKUP_START_TAG = "[auto-markup:";
        public const string AUTO_MARKUP_END_TAG = "[auto-markup end]";
        private static ILog _logger = LogManager.GetLogger(typeof(LetterUtilities));
        #endregion

        #region Letter Methods
        /// <summary>
        /// Returns a letter with the letter id and text coded by the session term lists
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="letterId">The ID of the letter</param>
        /// <returns></returns>
        public static string GetSessionLetter(Letter letter)
        {
            // code the letter text with term lists stored in the session
            return LetterUtilities.CodeTextBySessionTermList(letter.HtmlCodedText);
        }

        /// <summary>
        /// Update the letter type
        /// </summary>
        /// <param name="letterId">The ID of the Letter</param>
        /// <param name="type">type Name</param>
        /// <returns></returns>
        public static bool UpdateLetterType(int letterId, string type)
        {
            var result = false;

            // get the letter
            Letter letter;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {

                letter = letterManager.Get(letterId);

                if (letter != null)
                {
                    LookupLetterType lookupManagerLookup = LookupManager.GetLookup<LookupLetterType>();

                    int? oldFormSetId = null;
                    var oldLetterTypeId = letter.LetterTypeId;
                    var oldLetterType = lookupManagerLookup.First(x => x.LetterTypeId == oldLetterTypeId);
                    var newLetterType = lookupManagerLookup.First(x => x.Name.Equals(type));

                    // set letter type
                    letter.LetterTypeId = newLetterType.LetterTypeId;

                    if (oldLetterTypeId == 1) /* Duplicate */
                    {
                        letter.MasterDuplicateId = null;
                    }

                    // Determine new letter status and early attention status
                    letter.LetterStatusId = letterManager.TransitionLetterStatusByLetterType(oldLetterType, newLetterType, letter.LetterStatusId);
                    letter.EarlyActionStatusId = letterManager.TransitionEarlyActionStatusByLetterType(oldLetterType, newLetterType, letter);

                    // set the form set id to null
                    if (type.Equals("Unique"))
                    {
                        oldFormSetId = letter.FormSetId;
                        
                        // turning lazy loading off prevents fixup collection from trying to load all letters in a formset
                        // when you change the formsetid
                        letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;
                        letter.FormSetId = null;
                    }

                    letterManager.UnitOfWork.Save();

                    // delete the form set if empty
                    if (oldFormSetId.HasValue)
                    {
                        FormUtilities.DeleteFormSetIfEmpty(oldFormSetId.Value);
                    }

                    DynamoDBHelper dynamo = new DynamoDBHelper();

                    var newAttributes = new List<KeyValuePair<string, AttributeValue>>()
                    {
                        new KeyValuePair<string, AttributeValue>
                            ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letter.LetterTypeId))
                    };

                    if (type.Equals("Unique"))
                    {
                        newAttributes.Add(new KeyValuePair<string, AttributeValue>
                            ("FormSetId", DynamoDBHelper.GetAttributeValue(0)));
                    }

                    bool success = dynamo.Update(
                        new List<KeyValuePair<string, AttributeValue>>()
                        {
                            new KeyValuePair<string, AttributeValue>
                                ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                            new KeyValuePair<string, AttributeValue>
                                ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                        },
                        newAttributes
                    );

                    //check for successful result
                    if (!success)
                    {
                        _logger.Error(String.Format(
                            "Could not update letter's ({0}, {1}, {2}, {3}) in Dynamo",
                            letter.PhaseId,
                            letter.LetterId,
                            letter.LetterTypeId,
                            letter.FormSetId));
                    }

                    // log the changes
                    using (var formLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>())
                    {
                        formLogManager.LogChangeLetterType(letter, oldLetterTypeId, Utilities.GetUserSession().Username);
                        formLogManager.UnitOfWork.Save();
                    }

                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Update the letter status
        /// </summary>
        /// <param name="letterId">The ID of the Letter</param>
        /// <param name="mode">Action Mode</param>
        /// <returns></returns>
        public static bool UpdateStatus(int letterId, string mode, bool commit = false)
        {
            var result = false;
            int letterStatusId = -1;
            if (mode.Equals(Constants.COMPLETE))
            {
                letterStatusId = LookupManager.GetLookup<LookupLetterStatus>().FirstOrDefault(x => x.Name == "Complete").LetterStatusId;
            }
            else if (mode.Equals(Constants.IN_PROGRESS))
            {
                letterStatusId = LookupManager.GetLookup<LookupLetterStatus>().FirstOrDefault(x => x.Name == "In Progress").LetterStatusId;
            }
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                // get the letter
                var letter = letterManager.Get(letterId);

                if (letter != null && !string.IsNullOrEmpty(mode) && letterStatusId != -1)
                {
                    //CARA-1246
                    // set the letter status
                    letter.LetterStatusId = letterStatusId;

                    // --- No longer needed as of CR#335 ---
                    // If this letter is a form master, mark all letters in the form set complete
                    //if (letter.LetterTypeId == LookupManager.GetLookup<LookupLetterType>().First(x => x.Name == "Master Form").LetterTypeId)
                    //{
                    //    letterManager.UpdateLetterStatus(letter.FormSetId.Value, letter.LetterStatusId);
                    //}

                    // update the letter status
                    letter.LastUpdated = DateTime.UtcNow;
                    letter.LastUpdatedBy = Utilities.GetUserSession().Username;

                    if (commit)
                    {
                        letterManager.UnitOfWork.Save();
                    }

                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Return the next letter of the input sequence in the phase
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="sequence">Letter sequence number</param>
        /// <returns></returns>
        public static LetterSearchResult NextLetter(int phaseId, int sequence)
        {
            LetterSearchResult nextLetter;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                // get the letter filter
                LetterSearchResultFilter filter = Utilities.GetFilter(Constants.LETTER, phaseId.ToString()) as LetterSearchResultFilter;

                if (filter == null)
                {
                    filter = new LetterSearchResultFilter();
                }

                // set it to -1 returns all records
                filter.NumRows = -1;

                // get the next letter
                var total = 0;

                nextLetter = letterManager.GetLetters(filter, out total).Where(x => x.PhaseId == phaseId && x.LetterSequence > sequence).FirstOrDefault();
            }

            return nextLetter;

        }

        /// <summary>
        /// Update the letter term confirmation with the input action
        /// </summary>
        /// <param name="letterId">The ID of the letter</param>
        /// <param name="termListId">The ID of the Auto-markup</param>
        /// <param name="termId">The ID of the Auto-markup term</param>
        /// <param name="action">Page Action</param>
        /// <returns></returns>
//public static string UpdateLetterTermConfirmation(int letterId, ArrayList termListId, ArrayList termId, ArrayList statusId, ArrayList annotation, int? cnt)
        public static string UpdateLetterTermConfirmation(int letterId, string termListId, string termId, string statusId, string annotation, int? cnt)
        {
            var output = string.Empty;
            char delimiter = '|';
            bool EarlyAttnRequired = true;
            bool EarlyAttnNR = true;

            //if (letterId > 0 && termId > 0 && !string.IsNullOrEmpty(action) && statusId > 0)

            if (letterId > 0 && cnt.HasValue)
            {
                var arrtermId = termId.Split(delimiter);
                var arrtermListId = termListId.Split(delimiter);
                var arrstatusId = statusId.Split(delimiter);
                var arrannotation = annotation.Split(delimiter);

                int TermId;
                int TermListId;
                int? StatusId;
                string Annotation;
                bool checkedStatus = false;

                for (int j = 0; j < cnt; j++)
                {
                    if (arrstatusId[j].ToString() == "")
                    {
                        EarlyAttnRequired = false;
                        EarlyAttnNR = false;
                        break;
                    }
                    else if (arrstatusId[j].ToString() != "")
                    {
                        if (arrstatusId[j].ToString() == LookupManager.GetLookup<LookupLetterTermConfirmationStatus>().Where(x => x.Name.Equals("Needs Attention")).First().ConfirmationStatusId.ToString())
                        {
                            EarlyAttnRequired = false;
                            EarlyAttnNR = false;
                            break;
                        }
                    }

                    if (arrstatusId[j].ToString() != "" && arrstatusId[j].ToString() != LookupManager.GetLookup<LookupLetterTermConfirmationStatus>().Where(x => x.Name.Equals("Not Required")).First().ConfirmationStatusId.ToString())
                    {
                        EarlyAttnNR = false;
                    }
                }

                for (int i = 0; i < cnt; i++)
                {
                    TermId = Convert.ToInt32(arrtermId[i]);
                    TermListId = Convert.ToInt32(arrtermListId[i]);
                    if (arrstatusId[i].ToString() != "")
                    {
                        StatusId = Convert.ToInt32(arrstatusId[i]);
                    }
                    else
                    {
                        StatusId = null;
                    }
                    Annotation = arrannotation[i].ToString();

                    // get the letter
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        var letter = letterManager.Get(letterId);
                        var oldEarlyAttentionStatusId = letter.EarlyActionStatusId;
                        var letterTerm = letter.LetterTermConfirmations.Where(x => x.TermId == TermId).FirstOrDefault();

                        using (var termListManager = ManagerFactory.CreateInstance<TermListManager>())
                        {
                            var term = termListManager.Get(TermListId).Terms.Where(x => x.TermId == TermId).FirstOrDefault();

                            if (letter != null && letterTerm != null)
                            {
                                if (StatusId != letterTerm.StatusId)
                                {
                                    int LtrTermConfStatus = LookupManager.GetLookup<LookupLetterTermConfirmationStatus>().Where(x => x.Name.Equals("Not Required")).First().ConfirmationStatusId;
                                    //CARA-717: if Auto-markup status=N/A, then REMOVE the auto-markup term from the letter coded text.
                                    if (StatusId == LtrTermConfStatus)
                                    {
                                        if (term != null)
                                        {
                                            letter.HtmlCodedText = LetterUtilities.RemoveAutoMarkupFromText(letter.HtmlCodedText, term);
                                            letter.CodedText = LetterManager.ConvertHtmlToPlainText(letter.HtmlCodedText);
                                        }
                                    }
                                    //CARA-717: if Auto-markup status=Applies/Done, then ADD the auto-markup term from the letter coded text.
                                    else if (letterTerm.StatusId == LtrTermConfStatus)
                                    {
                                        // the auto-markup term needs to be tagged to the letter text again if it was removed
                                        if (term != null)
                                        {
                                            letter.HtmlCodedText = LetterUtilities.AddAutoMarkupToText(letter.HtmlCodedText, term);
                                            letter.CodedText = LetterManager.ConvertHtmlToPlainText(letter.HtmlCodedText);
                                        }
                                    }
                                }
                                // CR-327 dissociate automarkup status from letter status.
                                // set the letter status to be "in-progress"
                                //letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name == Utilities.GetResourceString("InProgress")).FirstOrDefault().LetterStatusId;
                                //UpdateStatus(letter.LetterId, Constants.IN_PROGRESS);

                                //CARA-717:- update the early action required field of the letter if all the early action required terms have been checked
                                if (checkedStatus == false)
                                {

                                    if (EarlyAttnRequired == true)
                                    {
                                        // //****************************
                                        //    //CARA-717:- check if any Coded comments EXISTS and if anyone of them is an Early Attn coded comments before changing the Early Attention Status
                                        bool EarlyAttnCommentCodeExists = false;

                                        using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                                        {
                                            PhaseCodeViewModel pcm = new PhaseCodeViewModel();

                                            var commentIds = commentManager.GetAllComments(letterId).Select(x=>x.CommentId).ToList();
                                            if (commentIds.Count > 0)
                                            {
                                                for (int j = 0; j < commentIds.Count; j++)
                                                {
                                                    using (var commentCodeManager = ManagerFactory.CreateInstance<CommentCodeManager>())
                                                    {
                                                        int commentId = commentIds[j];
                                                        IEnumerable<int> phaseCodeIds = commentCodeManager.All.Where(x => x.CommentId == commentId).Select(x => x.PhaseCodeId);
                                                        
                                                        foreach (int phaseCodeId in phaseCodeIds)
                                                        {
                                                            if (pcm.CheckCodeCategoryId(phaseCodeId) == true)
                                                            {
                                                                EarlyAttnCommentCodeExists = true;
                                                                break;
                                                            }
                                                        }

                                                        if (EarlyAttnCommentCodeExists)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (EarlyAttnCommentCodeExists == true)
                                        {
                                            letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Coded")).First().StatusId;
                                        }
                                        else
                                        {
                                            if (EarlyAttnNR == true)
                                            {
                                                letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Not Required")).First().StatusId; //status = N/A
                                            }
                                            else
                                            {
                                                letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Completed")).First().StatusId;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Auto-Marked")).First().StatusId;  //statusId=2(Needs REview)
                                    }
                                    checkedStatus = true;
                                }

                                letter.LastUpdated = DateTime.UtcNow;
                                letter.LastUpdatedBy = Utilities.GetUserSession().Username;
                                // update the letter term confirmation
                                //CARA 717- Update the below 2 new columns added to the letter term confirmation table as part of the requirement. - By LD 10/5/11
                                letterTerm.StatusId = StatusId;
                                letterTerm.Annotation = Annotation;

                                // save the work
                                letterManager.UnitOfWork.Save();

                                if (letter.LetterTypeId == 3 /*Master*/ && letter.EarlyActionStatusId != oldEarlyAttentionStatusId)
                                {
                                    letterManager.UpdateEarlyAttentionStatus(letter.FormSetId.Value, letter.EarlyActionStatusId.Value);
                                }

                                // set the output result
                                if (letter.EarlyActionStatusId.HasValue)
                                {
                                    output = string.Format("<result><earlyActionStatusId>{0}</earlyActionStatusId><codedText><![CDATA[{1}]]></codedText></result>", GetEarlyActionStatus(Convert.ToInt32(letter.EarlyActionStatusId)),
                                        LetterUtilities.CodeTextBySessionTermList(letter.HtmlCodedText).ToHtmlString());
                                }
                                else
                                {
                                    output = string.Format("<result><earlyActionStatusId>{0}</statusText><earlyActionStatusId><![CDATA[{1}]]></codedText></result>", string.Empty,
                                                                    LetterUtilities.CodeTextBySessionTermList(letter.HtmlCodedText).ToHtmlString());
                                }
                                //<commentId>{0}</commentId>
                            }
                        }
                    }
                }
            } // end of if condition

            return output;

        }


        /// <summary>
        /// Update the letter text to include html encoding
        /// </summary>
        /// <param name="letterId">The ID of the letter</param>
        /// <returns></returns>
        public static void EncodeText(int letterId)
        {
            using (LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                bool isLetterHtml = letterManager.All.Where(x => x.LetterId == letterId).Select(x => x.IsLetterHtml).FirstOrDefault();
                if (!isLetterHtml)
                {
                    Letter letter = letterManager.Get(letterId);
                    if (letter != null)
                    {
                        letter.HtmlCodedText = LetterManager.HtmlEncodeTextAndLineBreaks(letter.CodedText);
                        letter.HtmlText = LetterManager.HtmlEncodeTextAndLineBreaks(letter.Text);
                        letter.HtmlOriginalText = LetterManager.HtmlEncodeTextAndLineBreaks(letter.OriginalText);
                        letter.IsLetterHtml = true;
                        letterManager.UnitOfWork.Save();
                    }
                }
            }
        }
        #endregion

        #region Comment Tag Methods
        /// <summary>
        /// Returns the regular expression used to identify a comment tag
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public static Regex GetCommentTagRegEx(string tagId)
        {
            return new Regex(Regex.Escape(COMMENT_START_TAG) + tagId + "].*?" + Regex.Escape("[") + COMMENT_END_TAG.Substring(1), RegexOptions.Singleline);
        }

        /// <summary>
        /// Removes the link symbols from the input text
        /// </summary>
        /// <param name="text">Text</param>
        /// <returns></returns>
        public static string RemoveLinkSymbolFromText(string text)
        {
            return (!string.IsNullOrEmpty(text) ? text.Replace(LINK_SYMBOL, string.Empty) : string.Empty);
        }

        /// <summary>
        /// Removes the comment tags from the input text by the comment number
        /// </summary>
        /// <param name="text">Letter Text</param>
        /// <param name="letterCommentNumber">Letter Comment Number</param>
        /// <returns></returns>
        public static string RemoveCommentFromText(string text, string letterCommentNumber)
        {
            var result = text;

            if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(letterCommentNumber))
            {
                // search for tag pattern of the comment tag
                var pattern = GetCommentTagRegEx(letterCommentNumber);

                // find all the matches
                var matches = pattern.Matches(text);

                if (matches != null)
                {
                    // replace with the "uncommented" text for each match
                    foreach (Match match in matches)
                    {
                        foreach (Capture capture in match.Captures)
                        {
                            result = result.Replace(capture.Value, GetTextInCommentTag(capture.Value));
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Remove the an automarkup from the input text by term
        /// </summary>
        /// <param name="text"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static string RemoveAutoMarkupFromText(string text, Term term)
        {
            var result = text;

            if (!string.IsNullOrEmpty(text) && term != null)
            {
                // search for tag pattern of the comment tag
                var pattern = new Regex(Regex.Escape(AUTO_MARKUP_START_TAG) + term.TermList.Name.ToString() + "]" + term.Name + Regex.Escape("[") +
                    AUTO_MARKUP_END_TAG.Substring(1), RegexOptions.IgnoreCase);

                // find all the matches
                var matches = pattern.Matches(text);

                if (matches != null)
                {
                    // replace with the "uncommented" text for each match
                    foreach (Match match in matches)
                    {
                        foreach (Capture capture in match.Captures)
                        {
                            result = result.Replace(capture.Value, GetTextInCommentTag(capture.Value));
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Add the an automarkup to the input text with the input term
        /// </summary>
        /// <param name="text"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static string AddAutoMarkupToText(string text, Term term)
        {
            var result = text;

            if (!string.IsNullOrEmpty(text) && term != null)
            {
                var markup = AUTO_MARKUP_START_TAG + "{0}]{1}" + AUTO_MARKUP_END_TAG;
                var regex = new Regex(@"\b" + term.Name + @"\b", RegexOptions.IgnoreCase);
                if (regex.IsMatch(result))
                {
                //remove auto-markup tag if already exists and then re-apply
                    //   result = result.Replace(string.Format(markup, term.TermList.Name,term.Name),term.Name); -- Not required with the changes made in the UpdateLetterTermConfirmation method.
                   result = regex.Replace(result, x => string.Format(markup, term.TermList.Name, x.Value));
                }
            }
            return result;
        }

        /// <summary>
        /// Remove the input tag type from the input text
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tagType">e.g. comment, auto-markup, etc</param>
        /// <returns></returns>
        public static string RemoveTagsFromText(string text, string tagType)
        {
            var result = text;

            if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(tagType))
            {
                // search for tag pattern of the input tag type
                var pattern = new Regex(Regex.Escape("[") + tagType + ":.*?].*?" + Regex.Escape("[") + tagType + " end]", RegexOptions.Singleline);

                // find all the matches
                var matches = pattern.Matches(text);

                if (matches != null)
                {
                    foreach (Match match in matches)
                    {
                        // replace with the text for each match
                        foreach (Capture capture in match.Captures)
                        {
                            result = result.Replace(capture.Value, GetTextInCommentTag(capture.Value));
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the input text with the comment tag
        /// </summary>
        /// <param name="letterCommentNumber">Letter Comment Number</param>
        /// <param name="text">Comment Text</param>
        /// <returns></returns>
        public static string AddCommentTag(string letterCommentNumber, string text)
        {
            return GetCommentStartTag(letterCommentNumber) + text + COMMENT_END_TAG;
        }

        /// <summary>
        /// Returns the input text with the comment tag
        /// </summary>
        /// <param name="letterCommentNumber">Letter Comment Number</param>
        /// <returns></returns>
        public static string AddCommentTag(string letterCommentNumber)
        {
            return GetCommentStartTag(letterCommentNumber);
        }

        /// <summary>
        /// Returns the comment start tag
        /// </summary>
        /// <param name="tagId">The ID of the comment tag</param>
        /// <returns></returns>
        private static string GetCommentStartTag(string tagId)
        {
            return string.Format(COMMENT_START_TAG + "{0}]", tagId);
        }

        /// <summary>
        /// Returns the original text within the comment tag. e.g. [comment:1234]this is a house[...][comment end] returns "this is a house".
        /// </summary>
        /// <param name="text">Text</param>
        /// <returns></returns>
        private static string GetTextInCommentTag(string text)
        {
            var result = text;

            if (!string.IsNullOrEmpty(text))
            {
                // look for the first ] and the last [ character in the text
                int startIndex = text.IndexOf("]");
                int endIndex = text.LastIndexOf("[");

                if (startIndex > -1 && endIndex > 0 && endIndex >= startIndex)
                {
                    result = RemoveLinkSymbolFromText(text.Substring(startIndex + 1, endIndex - startIndex - 1));
                }
            }

            return result;
        }
        #endregion

        #region Comment Methods
        /// <summary>
        /// Add the codes to the comment in the letter
        /// </summary>
        /// <param name="phaseId">The ID of the Phase</param>
        /// <param name="letterId">The ID of the Letter</param>
        /// <param name="letterCommentNumber">Letter comment number</param>
        /// <param name="checkedCodeList">The list of the code IDs</param>
        /// <returns></returns>
        public static Comment AddCommentCodes(int phaseId, int letterId, string letterCommentNumber, string checkedCodeList)
        {
            Comment updatedComment;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                if (phaseId > 0 && !string.IsNullOrEmpty(letterCommentNumber))
                {
                    // get the comment
                    var comment = commentManager.Get(phaseId, letterCommentNumber);

                    var codeIds = Utilities.ToIntArray(checkedCodeList);

                    // add the selected code if it's not already added
                    foreach (int codeId in codeIds)
                    {
                        if (!comment.CommentCodes.Any(x => x.PhaseCodeId == codeId))
                        {
                            var commentCode = new CommentCode { CommentId = comment.CommentId, PhaseCodeId = codeId, Sequence = 0 };

                            commentManager.UnitOfWork.Context.AttachTo("CommentCodes", commentCode);
                            commentManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(commentCode, System.Data.EntityState.Added);
                        }
                    }

                    if (comment.UserId == null)
                    {
                        int? userAssignemnt = CommentUserAssignment(codeIds, phaseId);
                        if (userAssignemnt != null)
                        {
                            comment.UserId = userAssignemnt;
                            comment.DateAssigned = DateTime.UtcNow;
                        }
                    }


                    // save the new codes
                    commentManager.UnitOfWork.Save();
                }

                // get the comment again which populates the new comment codes with properties
                updatedComment = commentManager.Get(phaseId, letterCommentNumber);
                commentManager.UnitOfWork.Save();
                UpdateEarlyAttentionStatus(updatedComment, true);
            }

            return updatedComment;
        }

        //*Added for comment code sort 
        public static bool UpdateCommentCodesSequence(int commentId, string sortedCodeList)
        {
            var result = false;
            using (var commenCodetManager = ManagerFactory.CreateInstance<CommentCodeManager>())
            {
                // commentManager.GetAllComments
                var codeIds = Utilities.ToIntArray(sortedCodeList);
                int sequenceIndex = 1;

                // add the selected code if it's not already added
                foreach (int codeId in codeIds)
                {
                    commenCodetManager.UpdateCommentCodeSequence(commentId, codeId, sequenceIndex);
                    sequenceIndex = sequenceIndex + 1;
                }
                commenCodetManager.UnitOfWork.Save();
                result = true;
            }
            return result;
        }


        /// <summary>
        /// Save an existing comment with data
        /// </summary>
        /// <param name="commentId">The ID of the comment</param>
        /// <param name="sampleStatement">Sample Statement</param>
        /// <param name="noResponseReasonId">No Response Reason ID</param>
        /// <param name="noResponseReasonOther">No Response Reason Other Text</param>
        /// <param name="annotation">Annotation</param>
        /// <returns></returns>
        public static bool SaveComment(int commentId, bool sampleStatement, int? noResponseReasonId, string noResponseReasonOther,
            string annotation)
        {
            var result = false;

            if (commentId > 0)
            {
                using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                {
                    // get the comment
                    var comment = commentManager.Get(commentId);

                    if (comment != null)
                    {
                        // set comment fields
                        comment.SampleStatement = sampleStatement;
                        comment.NoResponseReasonId = noResponseReasonId;
                        comment.NoResponseReasonOther = noResponseReasonOther;
                        comment.Annotation = annotation;
                        comment.LastUpdated = DateTime.UtcNow;
                        comment.LastUpdatedBy = Utilities.GetUserSession().Username;

                        if (comment.Letter.LetterId != 4 /* Not Needed */)
                        {
                            // set letter status to be in progress
                            UpdateStatus(comment.Letter.LetterId, Constants.IN_PROGRESS);
                        }

                        // save the comment
                        commentManager.UnitOfWork.Save();

                        result = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Search the comments of a letter with optional search text
        /// </summary>
        /// <param name="letterId">The ID of the letter</param>
        /// <param name="text">Search Text</param>
        /// <returns></returns>
        public static ICollection<Comment> SearchComment(int letterId, string text = "")
        {
            // parse text to comment id
            var commentNumber = 0;
            int.TryParse(text, out commentNumber);

            // get all comments
            IList<Comment> comments;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                var commentsQ = commentManager.All.Where(x => x.LetterId == letterId);

                // filter by comment id if search is provided
                if (!string.IsNullOrEmpty(text))
                {
                    commentsQ = commentsQ.Where(x => x.CommentNumber == commentNumber);
                }

                if (commentsQ.Any())
                {
                    comments = commentsQ.ToList();
                }
                else
                {
                    comments = new List<Comment>();
                }
                foreach (var comment in comments)
                {
                    var temp = comment.CommentText;
                    var letter = comment.Letter;
                }
            }

            return comments;
        }

        /// <summary>
        /// Add a new comment to the letter with the selected text and checked phase codes
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="letterId">The ID of the letter</param>
        /// <param name="selectedText">Selected letter text</param>
        /// <param name="checkedCodeList">A list of selected phase code IDs</param>
        /// <returns></returns>
        public static string AddComment(int phaseId, int letterId, string selectedText, string checkedCodeList, string codedLetterText, string pendingTag)
        {
            var output = string.Empty;

            if (!string.IsNullOrEmpty(selectedText) && !string.IsNullOrEmpty(checkedCodeList))
            {
                // decode the text
                selectedText = selectedText.ToTextString("\r\n");
                // CR-296 Handling new line characters in comment text
                selectedText = selectedText.Replace("&#x0D;", "\n").TrimStart('\n', '\r').Trim();
                using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                {

                    // create new comment
                    var comment = new Comment
                    {
                        IdentifiedBy = Utilities.GetUserSession().Username,
                        IdentifiedOn = DateTime.UtcNow,
                        LastUpdatedBy = Utilities.GetUserSession().Username,
                        LastUpdated = DateTime.UtcNow,
                        LetterId = letterId,
                        SampleStatement = false
                    };

                    // add comment text (remove the auto-markup tags)
                    comment.CommentTexts = new List<CommentText>
                    {
                        new CommentText { Text = LetterUtilities.RemoveTagsFromText(selectedText, "auto-markup") }
                    };

                    var codes = new List<CommentCode>();
                    var codeIds = Utilities.ToIntArray(checkedCodeList).Distinct().ToArray();

                    bool earlyAttnCommentCodeExists = false;

                    PhaseCodeViewModel pcm = new PhaseCodeViewModel();

                    foreach (int codeId in codeIds)
                    {
                        codes.Add(new CommentCode { PhaseCodeId = codeId, Sequence = 0 });
                        
                        if (pcm.CheckCodeCategoryId(codeId) == true)
                        {
                            earlyAttnCommentCodeExists = true;
                        }

                    }

                    // add codes
                    comment.CommentCodes = codes;
                    int? userAssignment = CommentUserAssignment(codeIds, phaseId);
                    if (comment.UserId != userAssignment)
                    {
                        comment.UserId = userAssignment;
                        comment.DateAssigned = DateTime.UtcNow;
                    }

                    // save comment
                    commentManager.Add(comment);
                    commentManager.UnitOfWork.Save();

                    // refresh the comment to get the comment number
                    commentManager.UnitOfWork.Context.Refresh(RefreshMode.StoreWins, comment);

                    //update the letter text
                    //UpdateLetterText(comment.CommentId, selectedText);
                    UpdateLetterText(comment.CommentId, codedLetterText, pendingTag);
                    //refresh the comment
                    comment = commentManager.Get(comment.CommentId);
                    Letter letter = comment.Letter;

                    //****************************
                    //CARA-717:- check if the added comment is Early Attn coded comment:- If Yes, change the EarlyAttnStatus to 'Coded'
                    /*int oldEarlyAttentionStatusId = letter.EarlyActionStatusId.HasValue? letter.EarlyActionStatusId.Value : 0;
                    if (earlyAttnCommentCodeExists == true)
                    {
                        if ((letter.EarlyActionStatusId == LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Completed")).First().StatusId) || (letter.EarlyActionStatusId == LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Not Required")).First().StatusId))
                        {
                            letter.EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Coded")).First().StatusId;
                        }
                    }
                    //****************************/
                    commentManager.UnitOfWork.Save();
                    UpdateEarlyAttentionStatus(comment, true);
                    
                    //If letter is master form update all forms in formset
                    //if (letter.LetterTypeId == 3 /*Master*/ && letter.EarlyActionStatusId != oldEarlyAttentionStatusId)
                    /*{
                        using (LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            letterManager.UpdateEarlyAttentionStatus(letter.FormSetId.Value, letter.EarlyActionStatusId.Value);
                            letterManager.UnitOfWork.Save();
                        }
                    }*/

                    // set the output result
                    if (letter.EarlyActionStatusId.HasValue)
                    {
                        //output = string.Format("<result><commentId>{0}</commentId><codedText><![CDATA[{1}]]></codedText></result>",
                        //    newComment.LetterCommentNumber, LetterUtilities.CodeTextBySessionTermList(letter.CodedText).ToHtmlString());
                        output = string.Format("<result><commentId>{0}</commentId><earlyActionStatusId>{1}</earlyActionStatusId></result>", comment.LetterCommentNumber, GetEarlyActionStatus(Convert.ToInt32(letter.EarlyActionStatusId)));
                    }
                    else
                    {
                        output = string.Format("<result><commentId>{0}</commentId><earlyActionStatusId>{1}</earlyActionStatusId></result>", comment.LetterCommentNumber, string.Empty);
                    }
                }
            }

            return output;
        }

        /// <summary>
        /// Gets the userid of the comment task assignment based on the codes assigned to the comment
        /// </summary>
        /// <param name="codeIds"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        private static int? CommentUserAssignment(int[] codeIds, int phaseId)
        {
            int? retVal = null;
            using (PhaseCodeManager phaseCodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
            {
                PhaseCode phaseCode = phaseCodeManager
                    .All
                    .Where(x => x.PhaseId == phaseId &&
                        codeIds.Contains(x.PhaseCodeId) &&
                        x.UserId != null &&
                        x.CodeNumber.StartsWith("2"))
                    .OrderBy(x => x.CodeNumber)
                    .FirstOrDefault();
                if (phaseCode != null)
                {
                    retVal = phaseCode.UserId;
                }
                else
                {
                    phaseCode = phaseCodeManager
                       .All
                       .Where(x => x.PhaseId == phaseId &&
                           codeIds.Contains(x.PhaseCodeId) &&
                           x.UserId != null)
                       .OrderBy(x => x.CodeNumber)
                       .FirstOrDefault();
                    if (phaseCode != null)
                    {
                        retVal = phaseCode.UserId;
                    }
                }
            }
            return retVal;
        }

        /// <summary>
        /// Link a selected text an existing comment
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="letterId">The ID of the letter</param>
        /// <param name="commentId">Tje ID of the comment to link to</param>
        /// <param name="selectedText">Selected letter text</param>
        /// <returns></returns>
        public static string AddCommentText(int phaseId, int letterId, int commentId, string selectedText, string codedText)
        {
            var output = string.Empty;

            if (letterId > 0 && commentId > 0 && !string.IsNullOrEmpty(selectedText))
            {
                // decode the text (unneccessary with new editor
                //selectedText = selectedText.ToTextString();

                using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
                {
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {

                        // get the comment
                        var comment = commentManager.Get(commentId);

                        // add the comment text to the existing comment
                        if (comment != null)
                        {

                            // add a new comment text
                            // remove the auto-markup tags from the text if exist
                            var commentText = new CommentText { CommentId = commentId, Text = selectedText };
                            commentManager.UnitOfWork.Context.AttachTo("CommentTexts", commentText);
                            commentManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(commentText, System.Data.EntityState.Added);
                            commentManager.UnitOfWork.Save();

                            LetterUtilities.ReplaceCodedText(letterId, codedText);

                            //refresh the letter
                            Letter letter = letterManager.Get(letterId);

                            // set the output result
                            output = string.Format("<result><commentId>{0}</commentId></result>", comment.LetterCommentNumber);
                        }
                    }
                }
            }

            return output;
        }


        private static void UpdateLetterText(int commentID, string codedLetterText, string pendingTag)
        {
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                // update letter coded text
                if (commentID > 0)
                {
                    // get the comment again for comment number and letter sequence
                    var newComment = commentManager.Get(commentID);

                    if (newComment != null)
                    {
                        // get the letter
                        var letter = newComment.Letter;

                        var codedSelectedText = LetterUtilities.AddCommentTag(newComment.LetterCommentNumber);

                        // set the updated fields
                        // ----------------------------------------------------------------------------------------------------------------------------
                        // Note: when the comment is added, the coded text will be updated to remove multiple whitespaces since the text displayed
                        //       cannot differentiate multiple whitespaces vs single whitespace
                        // ----------------------------------------------------------------------------------------------------------------------------
                        //var codedText = Utilities.RemoveTabsAndWhitespaces(letter.CodedText);
                        var newCodedText = codedLetterText.Replace(pendingTag, codedSelectedText);

                        letter.HtmlCodedText = newCodedText;
                        letter.CodedText = LetterManager.ConvertHtmlToPlainText(letter.HtmlCodedText);
                        letter.LastUpdated = DateTime.UtcNow;
                        letter.LastUpdatedBy = Utilities.GetUserSession().Username;

                        if (letter.LetterStatusId != 4 /* Not Needed */)
                        {
                            letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name == Utilities.GetResourceString("InProgress")).FirstOrDefault().LetterStatusId;
                            UpdateStatus(letter.LetterId, Constants.IN_PROGRESS);
                        }

                        // save the letter
                        commentManager.UnitOfWork.Save();
                    }
                }

            }
        }


        ///<summary>
        /// Replaces the coded text of the letter
        ///</summary>
        ///<param name="commentID">The Id of the new comment</param>
        ///<param name="selectedText">The text of the new comment</param>
        private static void ReplaceCodedText(int letterId, string codedText)
        {
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                // update letter coded text
                if (letterId > 0)
                {
                    // get the comment again for comment number and letter sequence
                    var letter = letterManager.Get(letterId);

                    if (letter != null)
                    {
                        // get the letter
                        letter.CodedText = LetterManager.ConvertHtmlToPlainText(codedText);
                        letter.HtmlCodedText = codedText;
                        letter.LastUpdated = DateTime.UtcNow;
                        letter.LastUpdatedBy = Utilities.GetUserSession().Username;
                        
                        if (letter.LetterStatusId != 4 /* Not Needed */)
                        {
                            letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name == Utilities.GetResourceString("InProgress")).FirstOrDefault().LetterStatusId;
                            UpdateStatus(letter.LetterId, Constants.IN_PROGRESS);
                        }
                        
                        // save the letter
                        letterManager.UnitOfWork.Save();
                    }
                }
            }

        }

        ///<summary>
        /// Update the lettertext with new comment
        ///</summary>
        ///<param name="commentID">The Id of the new comment</param>
        ///<param name="selectedText">The text of the new comment</param>
        private static void UpdateLetterText(int commentID, string selectedText)
        {
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                // update letter coded text
                if (commentID > 0)
                {
                    // get the comment again for comment number and letter sequence
                    var newComment = commentManager.Get(commentID);

                    if (newComment != null)
                    {
                        // get the letter
                        var letter = newComment.Letter;

                        var codedSelectedText = LetterUtilities.AddCommentTag(newComment.LetterCommentNumber, selectedText);

                        // set the updated fields
                        // ----------------------------------------------------------------------------------------------------------------------------
                        // Note: when the comment is added, the coded text will be updated to remove multiple whitespaces since the text displayed
                        //       cannot differentiate multiple whitespaces vs single whitespace
                        // ----------------------------------------------------------------------------------------------------------------------------
                        var codedText = Utilities.RemoveTabsAndWhitespaces(letter.CodedText);
                        var newCodedText = codedText.Replace(selectedText, codedSelectedText);

                        // if the lengths are the same, then the selectedText could not be found.  This is most likely caused by line break differences.
                        if (newCodedText.Length == codedText.Length)
                        {
                            selectedText = selectedText.Replace("\r\n", "\n");
                            codedSelectedText = codedSelectedText.Replace("\r\n", "\n");
                            newCodedText = codedText.Replace(selectedText, codedSelectedText);

                            if (newCodedText.Length == codedText.Length)
                            {
                                throw new DomainException("A comment could not be created from the selected text. Please contact a system administrator.");
                            }
                        }

                        letter.CodedText = newCodedText;
                        letter.LastUpdated = DateTime.UtcNow;
                        letter.LastUpdatedBy = Utilities.GetUserSession().Username;

                        if (letter.LetterStatusId != 4 /* Not Needed */)
                        {
                            letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name == Utilities.GetResourceString("InProgress")).FirstOrDefault().LetterStatusId;
                            UpdateStatus(letter.LetterId, Constants.IN_PROGRESS);
                        }
                                                
                        letter.CodedText = RefreshLetterText(newComment.LetterCommentNumber, newCodedText);

                        // save the letter
                        commentManager.UnitOfWork.Save();
                    }
                }

            }
        }

        /// <summary>
        /// Reorganize the letter text fragments for the input comment (should be called after a new text is added to an existing comment)
        /// </summary>
        /// <param name="comment">Comment</param>
        /// <param name="codedText">Letter Coded Text</param>
        /// <returns></returns>
        private static string RefreshLetterText(string letterCommentNumber, string codedText)
        {
            var newText = codedText;

            if (!string.IsNullOrWhiteSpace(letterCommentNumber) && !string.IsNullOrWhiteSpace(codedText))
            {
                // --------------------------------------------------------------------------------------------------------
                // Scan through the entire letter coded text to find all comment text fragments by the comment number
                //   For each fragment in the fragment collection:
                //     i.	Remove all existing […]’s from the text
                //     ii. All fragments, except the first one found, will have […]inserted after the [comment:NNN-CCC] tag
                //     iii. All fragments, except the last one found, will have […]inserted before the [comment end] tag
                //     iv. Replace the fragment in the coded text with the text constructed from (i – iii)
                // --------------------------------------------------------------------------------------------------------

                // search for tag pattern of the comment tag
                var pattern = GetCommentTagRegEx(letterCommentNumber);

                // find all the matches
                var matches = pattern.Matches(newText);

                if (matches != null)
                {
                    int count = 0;

                    // replace with the "uncommented" text for each match
                    foreach (Match match in matches)
                    {
                        foreach (Capture capture in match.Captures)
                        {
                            // Remove all existing […]’s from the text
                            var fragment = RemoveLinkSymbolFromText(capture.Value);

                            // All fragments, except the first one found, will have […] inserted after the [comment:NNN-CCC] tag
                            if (count > 0)
                            {
                                fragment = fragment.Replace(GetCommentStartTag(letterCommentNumber), GetCommentStartTag(letterCommentNumber) + LINK_SYMBOL);
                            }

                            // All fragments, except the last one found, will have […] inserted before the [comment end] tag
                            if (count < matches.Count - 1)
                            {
                                fragment = fragment.Replace(COMMENT_END_TAG, LINK_SYMBOL + COMMENT_END_TAG);
                            }

                            // Replace the fragment in the coded text with the text constructed
                            newText = newText.Replace(capture.Value, fragment);

                            // increment the count
                            count++;
                        }
                    }
                }
            }

            return newText;
        }

        /// <summary>
        /// Delete comment and update the coded letter text to remove the comment tag
        /// </summary>
        /// <param name="commentId">The ID of the comment</param>
        /// <returns></returns>
        public static string DeleteComment(int commentId)
        {
            var output = string.Empty;
            using (var commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                var comment = commentManager.Get(commentId);
                if (comment != null)
                {
                    // ensure the comment is not the only comment associated with a concern
                    if (comment.ConcernResponseId.HasValue && comment.ConcernResponse.CommentCount == 1)
                    {
                        output = string.Format("<result><orphanConcern>true</orphanConcern></result>");
                    }

                    // update the letter and delete the comment
                    else if (comment.Letter != null)
                    {
                        // update the letter coded text
                        comment.Letter.HtmlCodedText = RemoveCommentFromText(comment.Letter.HtmlCodedText, comment.LetterCommentNumber);
                        comment.Letter.CodedText = LetterManager.ConvertHtmlToPlainText(comment.Letter.HtmlCodedText);

                        if (comment.Letter.LetterStatusId != 4 /* Not Needed */)
                        {
                            // set letter status to be in progress
                            UpdateStatus(comment.Letter.LetterId, Constants.IN_PROGRESS, true);
                        }

                        UpdateEarlyAttentionStatus(comment, false);

                        //CR#380 Delete Custom Comment Code
                        var codes = comment.CommentCodes;
                        int count = comment.CommentCodes.Count;
                        if (count > 0)
                        {
                            using (var commentCodeManager = ManagerFactory.CreateInstance<CommentCodeManager>())
                            {
                                foreach (var codeToTest in codes)
                                {
                                    int commentsWithCode = commentCodeManager.All.Where(y => y.PhaseCodeId == codeToTest.PhaseCodeId && y.CommentId != codeToTest.CommentId).Count();
                                    if (commentsWithCode == 0)
                                    {
                                        codeToTest.PhaseCode.CanRemove = true;
                                    }
                                }

                                commentCodeManager.UnitOfWork.Save();
                            }

                            for (int i = comment.CommentCodes.Count - 1; i >= 0; i--)
                            {
                                var commentCode = comment.CommentCodes.ElementAt(i);

                                commentManager.UnitOfWork.Context.AttachTo("CommentCodes", commentCode);
                                commentManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(commentCode, System.Data.EntityState.Deleted);

                                comment.CommentCodes.Remove(commentCode);
                            }
                        }
                        
                        //commentManager.UnitOfWork.Save();
                        // set the output result before the comment is deleted

                        // set the output result
                        if (comment.Letter.EarlyActionStatusId.HasValue)
                        {
                            output = string.Format("<result><earlyActionStatusId>{0}</earlyActionStatusId><orphanConcern>false</orphanConcern></result>", 
                                GetEarlyActionStatus(comment.Letter.EarlyActionStatusId.Value));
                        }
                        else
                        {
                            output = "<result><earlyActionStatusId></earlyActionStatusId><orphanConcern>false</orphanConcern></result>";
                        }

                        commentManager.UnitOfWork.Context.AttachTo("Comments", comment);
                        commentManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(comment, System.Data.EntityState.Deleted);                        
                        commentManager.UnitOfWork.Save();
                    }
                }
            }

            return output;
        }
  
        private static void UpdateEarlyAttentionStatus(Comment comment, bool includeCommentInComparison = false)
        {
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                var lkuLetterType = LookupManager.GetLookup<LookupLetterType>();
                var letterType = lkuLetterType[comment.Letter.LetterTypeId];
                int? ignoreCommentId = null;
                if (!includeCommentInComparison)
                {
                    ignoreCommentId = comment.CommentId;
                }
                comment.Letter.EarlyActionStatusId = letterManager.TransitionEarlyActionStatusByLetterType(letterType, letterType, comment.Letter, ignoreCommentId);
            }
        }

        /// <summary>
        /// Delete an associated code from the comment
        /// </summary>
        /// <param name="commentId">The ID of the Comment</param>
        /// <param name="phaseCodeId">The ID of the Phase Code</param>
        /// <returns></returns>
        public static Comment DeleteCommentCode(Comment comment, int phaseCodeId, CommentManager commentManager)
        {
             var code = comment.CommentCodes.Where(x => x.PhaseCodeId == phaseCodeId).FirstOrDefault();

             //delete the comment code
             if (code != null)
             {
                 if (comment.Letter.LetterId != 4 /* Not Needed */)
                 {
                     // set letter status to be in progress
                     UpdateStatus(comment.Letter.LetterId, Constants.IN_PROGRESS);
                 }

                 using (var commentCodeManager = ManagerFactory.CreateInstance<CommentCodeManager>())
                 {
                     int commentsWithCode = commentCodeManager.All.Where(x => x.PhaseCodeId == code.PhaseCodeId && x.CommentId != code.CommentId).Count();
                     if (commentsWithCode == 0)
                     {
                         code.PhaseCode.CanRemove = true;
                     }

                     // if the code is an early attention code and there are no other early attention codes on the comment update the early attention status
                     int eaCodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.Name.Equals("Early Attention")).First().CodeCategoryId;
                     bool isEaCode = comment.CommentCodes.Where(x=>x.PhaseCodeId == phaseCodeId).FirstOrDefault().PhaseCode.CodeCategoryId == eaCodeCategoryId;
                     int eaCodeCount = comment.CommentCodes.Where(x => x.PhaseCode.CodeCategoryId == eaCodeCategoryId).Count();

                     if (isEaCode && eaCodeCount == 1)
                     {
                         UpdateEarlyAttentionStatus(comment, false);
                     }

                     commentManager.UnitOfWork.Context.AttachTo("CommentCodes", code);
                     commentManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(code, System.Data.EntityState.Deleted);
                     commentManager.UnitOfWork.Save();
                 }
             }

            
            // return the comment
            return comment;
        }
        #endregion

        #region Auto-markup Methods
        /// <summary>
        /// Returns the coded text after tagging the terms in the term list stored in the session
        /// </summary>
        /// <param name="text">text</param>
        /// <returns></returns>
        public static string CodeTextBySessionTermList(string text)
        {
            // trick: to catch the start and end string terms by word boundary reg exp, add a space before and after the text to tag
            var textToTag = string.Format(" {0} ", text);
            // get the term list ids from the session
            var termListIds = Utilities.GetTermList();

            if (!string.IsNullOrWhiteSpace(textToTag) && termListIds != null)
            {
                using (var termListManager = ManagerFactory.CreateInstance<TermListManager>())
                {
                    // get the term lists
                    var termLists = from x in termListManager.All where termListIds.Contains(x.TermListId) select x;
                    //var markup = "[auto-markup:{0}]{1}[auto-markup end]";  // TODO:  Move to the SystemConfig table
                    var markupStart = "[session auto-markup:{0}]";
                    var markupEnd = "[auto-markup end]";

                    // combine into a master term list sorted by the term name length
                    var masterTermList = new List<Term>();
                    foreach (var termList in termLists)
                    {
                        masterTermList.AddRange(termList.Terms.Where(x=>x.Active));
                    }

                    // add auto-markup
                    foreach (var term in masterTermList.OrderByDescending(x => x.Name.Length))
                    {
                        var regex = new Regex(@"\b" + term.Name + @"\b", RegexOptions.IgnoreCase);

                        var matches = regex.Matches(textToTag);

                        // Used in conjunction with matchIndex to normalize the starting 
                        // index of the match after the auto-markup start and end tags are added
                        var lengthAdjustment = 0;

                        // The index where the match starts in the string (match.Index + lengthAdjustment)
                        var matchIndex = 0;

                        char charBeforeMatch;
                        char charAfterMatch;
                        foreach (object m in matches)
                        {
                            var match = (Match)m;
                            matchIndex = match.Index + lengthAdjustment;
                            charBeforeMatch = textToTag[matchIndex - 1];
                            charAfterMatch = textToTag[matchIndex + match.Length];


                            // We want to make sure that we arent' finding a word that's 
                            // already been tagged:  [auto-markup:Economic]financial[end auto-markup]
                            bool tagMatch = match.Success && (charBeforeMatch != ']' && charBeforeMatch != ':' && charAfterMatch != '[');

                            //special handling for Mail automarkup term
                            //CR372 special handling for mail like terms
                            string[] mailTerms = new string[] { "mail", "mailed", "mailing", "mailings", "mails" };
                            tagMatch = tagMatch && !(
                                mailTerms.Contains(match.Value.ToLower()) && (charBeforeMatch == '@' || charAfterMatch == '@'));
                            if (tagMatch)
                            {
                                var markupStartForTermList = string.Format(markupStart, term.TermList.Name);

                                textToTag = textToTag.Insert(matchIndex + match.Length, markupEnd);
                                textToTag = textToTag.Insert(matchIndex, markupStartForTermList);

                                // match.Index for each match in matches was determined
                                // before any auto-markup tags were added.  So, we need to
                                lengthAdjustment += (markupEnd.Length + markupStartForTermList.Length);
                            }
                        }
                    }
                }
            }

            return textToTag.Trim();
        }
        #endregion

        #region Letter methods
        /// <summary>
        /// Returns EarlyActionStatus for the given statusId
        /// </summary>
        /// <param name="name">The EarlyActionStatusID of the letter</param>
        /// <returns>earlyActionStatus or 0</returns>
        public static string GetEarlyActionStatus(int statusId)
        {
            string earlyActionStatus = string.Empty;

            if (statusId > 0)
            {
                earlyActionStatus = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.StatusId.Equals(statusId)).First().Name;
             }

            return earlyActionStatus;

        }
        #endregion

        #region Misc Methods
        #endregion
    }
}