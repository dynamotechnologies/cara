﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Aquilent.Cara.WebApp.Models
{
        public class RecaptchaValidationResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public string[] ErrorCodes { get; set; }
        }

        public class RecaptchaValidationRequest
        {
            [JsonProperty("secret")]
            public string SecretKey { get; set; }

            [JsonProperty("response")]
            public string Response { get; set; }
        
            [JsonProperty("remoteip")]
            public string RemoteIp { get; set; }
        }
}