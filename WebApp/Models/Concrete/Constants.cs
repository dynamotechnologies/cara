﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Constants for the application
    /// </summary>
    public class Constants
    {
        #region Page Constants
        public const string DETAILS = "Details";
        public const string FILTER = "Filter";
        public const string INPUT = "Input";
        public const string LIST = "List";
        public const string SELECT = "Select";
        #endregion

        #region Action Constants
        public const string ACCEPT = "Accept";
        public const string ACTIVATE = "Activate";
        public const string ADD = "Add";
        public const string APPEND = "Append";
        public const string APPLY = "Apply";
        public const string ARCHIVE = "Archive";
        public const string ASSOCIATE = "Associate";
        public const string BACK = "Back";
        public const string CANCEL = "Cancel";
        public const string CODING = "Coding";
        public const string COMPARE = "Compare";
        public const string COMPLETE = "Complete";
        public const string CLEAR = "Clear";
        public const string CLOSE = "Close";
        public const string CONFIRM = "Confirm";
        public const string CREATE = "Create";
        public const string DEACTIVATE = "Deactivate";
        public const string DELETE = "Delete";
        public const string DOWNLOAD = "Download";
        public const string DOWNLOAD_DMD = DOWNLOAD + DMD;
        public const string EDIT = "Edit";
        public const string ERROR = "Error";
        public const string FIND = "Find";
        public const string FINISH = "Finish";
        public const string FIRST = "First";
        public const string GO = "Go";
        public const string LAST = "Last";
        public const string LINK = "Link";
        public const string MANAGE = "Manage";
        public const string MODE = "Mode";
        public const string MODIFY = "Modify";
        public const string MORE = "More";
        public const string NEXT = "Next";
        public const string NEW = "New";
        public const string NONE = "None";
        public const string OK = "OK";
        public const string PREV = "Prev";
        public const string PREVIEW = "Preview";
        public const string PRINT = "Print";
        public const string PUBLISH = "Publish";
        public const string REDESIGNATE = "Re-designate";
        public const string REMOVE = "Remove";
        public const string RENAME = "Rename";
        public const string RESORT = "Re-sort";
        public const string REUSE = "Reuse";
        public const string REVERT = "Revert";
        public const string RUN = "Run";
        public const string SAVE = "Save";
        public const string SORT = "Sort";
        public const string REORDER = "Reorder";
        public const string SAVE_AND_RETURN = SAVE + " and Return";
        public const string SEARCH = "Search";
        public const string SEND = "Send";
        public const string SHOW_ALL = "Show All";
        public const string UNPUBLISH = "Un-publish";
        public const string UPLOAD = "Upload";
        public const string UPDATE = "Update";
        public const string VIEW = "View";
        #endregion

        #region Name Constants
        public const string ACTION = "Action";
        public const string ACTIVE = "Active";
        public const string ANON = "Anon";
        public const string ARCHIVED = ARCHIVE + "d";
        public const string COMPACT = "Compact";
        public const string CUSTOM = "Custom";
        public const string EXPANDED_PHASE_CODE = "ExpandedPhaseCodes";
        public const string FORM_MGMT = FORM + "Mgmt";
        public const string IN_PROGRESS = "In Progress";
        public const string TITLE = "Title";
        public const string INDEX = "Index";
        public const string NAV = "Nav";
        public const string PRINTABLE = "Printable";
        public const string PUBLIC_COMMENT_FORM = "Public Comment Form";
        public const string STATUS = "Status";
        public const string SUMMARY = "Summary";
        public const string SYSTEM = "System";
        public const string TIME_ZONE = "Time Zone";
        public const string TOTAL = "Total";
        public const string WIZARD = "Wizard";
        #endregion

        #region Type Constants
        public const string ID = "Id";
        public const string KEY = "Key";
        public const string NAME = "Name";
        #endregion

        #region Value Constants
        public const int DEFAULT_PAGE_SIZE = 10;
        public const int MAX_ROW_COUNT = 1000;
        public const int CUSTOM_CODE_ID_SEQ = 50000;
        public const string GRID_HEADER_STYLE = "white-space:pre;";
        public const string NO_RECORD_DISPLAY = "<center><i>No records to display</i></center>";
        #endregion

        #region Entity Constants
        public const string ADMIN = "Admin";
        public const string AUTHOR = "Author";
        public const string DATAMART = "DataMart";
        public const string DMD = "Dmd";
        public const string DOCUMENT = "Document";
        public const string CHANGE_LOG = "ChangeLog";
        public const string CODE = "Code";
        public const string COMMENT_PERIOD = COMMENT + " Period";
        public const string COMMENT = "Comment";
        public const string COMMENT_CODE = COMMENT + CODE;
        public const string COMMENT_INPUT = COMMENT + INPUT;
        public const string CONCERN = "Concern";
        public const string CONCERN_RESPONSE = "Concern" + "Response";
        public const string CRUMB = "Breadcrumb";
		public const string DUPLICATE = "Duplicate";
        public const string EMAIL = "Email";
        public const string FORM = "Form";
        public const string FORMS = FORM + "s";
        public const string FORM_SET = FORM + "Set";
        public const string HOME = "Home";
        public const string LETTER = "Letter";
        public const string LETTERS = LETTER + "s";
        public const string LOCATION = "Location";
        public const string MEMBER = "Member";
		public const string MY_PROJECTS = "MyProjects";
		public const string PHASE = "Phase";
        public const string PHASE_REPORT = PHASE + REPORT;
        public const string PHASE_CODE = PHASE + CODE;
        public const string PROFILE = "Profile";
        public const string PROJECT = "Project";
        public const string PUBLIC = "Public";
        public const string PUBLIC_READING_ROOM = PUBLIC + READING_ROOM;
        public const string PUBLIC_COMMENT_INPUT = PUBLIC + COMMENT + INPUT;
        public const string READING_ROOM = "ReadingRoom";
        public const string READING_ROOM_MGMT = READING_ROOM + "Mgmt";
        public const string READING_ROOM_MGMT_ABBR = READING_ROOM_MGMT + "Abbr"; //CARA-976
        public const string REPORT = "Report";
        public const string RESPONSE = "Response";
        public const string ROLE = "Role";
		public const string STANDARD_REPORTS = "Standard" + REPORT + "s";
        public const string SUPPORT = "Support";
        public const string TASK = "Task";
        public const string TEMPLATE = "Template";
        public const string TERM = "Term";
        public const string TERM_LIST = TERM + LIST;
        public const string UNIT = "Unit";
        public const string USER = "User";
        public const string MAILINGLIST_REPORT = "MailingList";
        public const string TEAMMEMBER_REPORT = "TeamMember";
        public const string UNTIMELYCOMMENTS_REPORT = "UntimelyComments";
        public const string COMMENTERORGTYPE_REPORT = "CommenterOrgType";
        public const string EARLYACTION_REPORT = "EarlyAction";
        public const string RESPONSESTATUS_REPORT = "ResponseStatus";
        public const string COMMENTSRESPONSESBYLETTER_REPORT = "CommentsAndResponsesByLetter";
        public const string CONCERNRESPONSEBYCOMMENTER_REPORT = "ConcernResponseByCommenter";
        public const string COMMENTRESPONSENOCONCERN_REPORT = "CommentResponseNoConcern";
        public const string RESPONSETOCOMMENT_REPORT = "ResponseToComment";
        public const string COMMENTWITHCRINFO_REPORT = "CommentWithCRInfo";
        public const string COMMENTWITHANNOTATION_REPORT = "CommentWithAnnotation";
        public const string CONCERNRESPONSEWITHASSIGNEDTO_REPORT = "ConcernResponseWithAssignedTo";
        #endregion

        #region Class Properties Constants
        public const string COMMENT_ID = COMMENT + ID;
        public const string CONCERN_RESPONSE_ID = CONCERN_RESPONSE + ID;
        public const string CUSTOM_REPORT = CUSTOM + REPORT;
        public const string LIST_DATAMART = LIST + DATAMART;
        public const string DATAMART_USER = DATAMART + USER;
        public const string DATAMART_PROJECT = DATAMART + PROJECT;
        public const string PROJECT_MANAGER = PROJECT + "Manager";
        #endregion
    }
};