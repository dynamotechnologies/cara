﻿using System;
using System.Collections.Generic;
using System.Linq;

using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a breadcrumb
    /// </summary>
    public class Breadcrumb
    {
        #region Class Members
        public string Text { get; set; }
        public List<ActionLink> ActionLinks { get; set; }
        #endregion

        #region Cache
        private static Dictionary<int, string> ProjectCrumb = new Dictionary<int, string>();
        private static Dictionary<int, string> PhaseCrumb = new Dictionary<int, string>();
        private static Dictionary<int, string> LetterCrumb = new Dictionary<int, string>();
        #endregion Cache

        #region Constructors
        public Breadcrumb(object text, Uri url)
        {
            Init((text is string ? text.ToString() : string.Empty), url);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialize the breadcrumb with display text and url
        /// </summary>
        /// <param name="text"></param>
        /// <param name="url"></param>
        private void Init(string text, Uri uri)
        {
            // validate request
            if (uri == null)
            {
                throw new ArgumentNullException("uri");
            }

            // get the url
            var url = uri.AbsolutePath;

            // initialize variables
            this.ActionLinks = new List<ActionLink>();
            this.Text = text;

            // parse the url string and get the breadcrumb links
            if (!string.IsNullOrEmpty(url))
            {
                #region Home
                // home link is always the root
                this.ActionLinks.Add(new ActionLink(Constants.HOME, Constants.INDEX, Constants.HOME));
                #endregion

                #region Project/Phase
                // -----------------------------------------------------------------------------------------------------
                // Note: the project create wizard does not follow the rules of the breadcrumb evaluation using url.
                //       if the projectcreatewizard mode is found in the url query string, skip the breadcrumb trail
                // -----------------------------------------------------------------------------------------------------
                // project
                if (url.Contains("/Project/") && !uri.Query.Contains(PageMode.ProjectCreateWizard.ToString()))
                {
                    Project project = null;

                    // get the id text
                    int projectId = Utilities.GetEntityIdFromUrl(url, Constants.PROJECT);
                    if (projectId > 0)
                    {
                        // add "Projects" action link
                        this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString(Constants.PROJECT).Pluralize(), Constants.LIST, Constants.PROJECT, new { projectId = 0 }));

                        // get project details from db
                        if (!ProjectCrumb.ContainsKey(projectId))
                        {
                            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                            {
                                project = projectManager.Get(projectId);
                                ProjectCrumb.Add(projectId, project.ProjectNameNumber);
                            }
                        }

                        // add "Details" action link only if the target url is not Details
                        if (!url.Contains("/Details/"))
                        {
                            this.ActionLinks.Add(new ActionLink(ProjectCrumb[projectId], Constants.DETAILS, Constants.PROJECT, new { projectId = projectId }));
                        }
                    }

                    // phase
                    if (url.Contains("/Phase/"))
                    {
                        // get the id text
                        var phaseId = Utilities.GetEntityIdFromUrl(url, Constants.PHASE);

                        // add "Details" action link only if the target url is not Details
                        if (phaseId > 0 && !url.EndsWith(string.Format("/Phase/{0}", phaseId.ToString())))
                        {
                            var phaseName = string.Empty;

                            if (!PhaseCrumb.ContainsKey(phaseId))
                            {
                                Phase phase;
                                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                                {
                                    phase = phaseManager.Get(phaseId);
                                }

                                if (phase != null)
                                {
                                    phaseName = phase.PhaseTypeDesc;
                                    PhaseCrumb.Add(phaseId, phaseName);
                                }
                            }

                            this.ActionLinks.Add(new ActionLink(PhaseCrumb[phaseId], Constants.DETAILS, Constants.PHASE, new { projectId = projectId, phaseId = phaseId }));
                        }
                    }

                    // letter
                    if (url.Contains("/Letter/"))
                    {
                        // get the id text
                        var phaseId = Utilities.GetEntityIdFromUrl(url, Constants.PHASE);
                        if (phaseId > -1)
                        {
                            // add letters link
                            this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString(Constants.LETTER).Pluralize(), Constants.LIST, Constants.LETTER, new { projectId = projectId, phaseId = phaseId }));

                            var letterId = Utilities.GetEntityIdFromUrl(url, Constants.LETTER);

                            // if letter presents, add the specific letter link
                            if (letterId > 0)
                            {
                                Letter letter = null;
                                if (!LetterCrumb.ContainsKey(letterId))
                                {
                                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                                    {
                                        letter = letterManager.Get(letterId);
                                        LetterCrumb.Add(letterId, string.Format("{0} #{1}", Constants.LETTER, letter.LetterSequence));
                                    }
                                }

                                this.ActionLinks.Add(new ActionLink(LetterCrumb[letterId], Constants.CODING, Constants.LETTER, new { projectId = projectId, phaseId = phaseId, id = letterId }));
                            }
                        }
                    }

                    // comment
                    if (url.Contains("/Comment/"))
                    {
                        // get the id text
                        var phaseId = Utilities.GetEntityIdFromUrl(url, Constants.PHASE);
                        if (phaseId > -1)
                        {
                            this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString(Constants.COMMENT).Pluralize(), Constants.LIST, Constants.COMMENT, new { projectId = projectId, phaseId = phaseId, mode = Constants.CREATE }));
                        }
                    }

                    // concern
                    if (url.Contains("/Concern/"))
                    {
                        // get the id text
                        var phaseId = Utilities.GetEntityIdFromUrl(url, Constants.PHASE);
                        if (phaseId > -1)
                        {
                            this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString("Concerns"), Constants.LIST, Constants.CONCERN, new { projectId = projectId, phaseId = phaseId }));
                        }
                    }

                    // form
                    if (url.Contains("/Form/"))
                    {
                        
                        var phaseId = Utilities.GetEntityIdFromUrl(url, Constants.PHASE);
                        if (phaseId > -1)
                        {
                            this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString(Constants.TITLE, Constants.FORM + Constants.LIST), Constants.LIST, Constants.FORM, new { projectId = projectId, phaseId = phaseId }));
                        }
                    }
                }
                #endregion

                #region Standard Reports
                                
                if ((url.Contains("/Phase/")) && (url.Contains("/Report/")))
                {
                     var phaseId = Utilities.GetEntityIdFromUrl(url, Constants.PHASE);

                     this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString(Constants.STANDARD_REPORTS), Constants.REPORT, Constants.PHASE));
                  }

                #endregion

                #region My Profile
                #endregion

                #region Reports
                // reports
                if ((url.Contains("/Report/") || url.Contains("/Report/")) && !(url.Contains("/Phase/")))
                {
                    string controller = Constants.REPORT;

                    this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString(Constants.REPORT).Pluralize(), Constants.INDEX, controller));

                    if (url.Contains("/Preview"))
                    {
                        this.ActionLinks.Add(new ActionLink(Utilities.GetResourceString("CustomReports"), "TrafficDirector", "Report", new { action = "Start", ReportId = 0 }));
                    }
                }
                #endregion

                #region Admin
                // admin
                if (url.Contains("/Admin/"))
                {
                    this.ActionLinks.Add(new ActionLink(Constants.ADMIN, Constants.INDEX, Constants.ADMIN));
                }

                if (url.Contains("/User/") && !url.Contains("/List"))
                {
                    this.ActionLinks.Add(new ActionLink(string.Format("CARA {0}", Constants.USER.Pluralize()), Constants.LIST, Constants.USER));
                }

                // coding structure
                if (url.Contains("/Code/"))
                {
                    this.ActionLinks.Add(new ActionLink(string.Format("Master {0}", Utilities.GetResourceString("CodingStructure")), Constants.LIST, Constants.CODE));
                }
                #endregion

                #region Support
                // support
                if (url.Contains("/Support/"))
                {
                    this.ActionLinks.Add(new ActionLink(Constants.SUPPORT, Constants.INDEX, Constants.SUPPORT));
                }
                #endregion
            }
        }
        #endregion
    }

    /// <summary>
    /// Represent an action link
    /// </summary>
    public class ActionLink
    {
        #region Class Members
        public string name;
        public string action;
        public string controller;
        public object routeValues;
        #endregion

        #region Constructor
        public ActionLink(string name, string action, string controller, object routeValues = null)
        {
            this.name = name;
            this.action = action;
            this.controller = controller;
            this.routeValues = routeValues;
        }
        #endregion
    }
}