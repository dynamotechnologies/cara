﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase report view Model
    /// </summary>
    public class PhaseReportViewModel : AbstractViewModel
    {
        #region Properties
        public ICollection<Report> Reports { get; set; }
        public IEnumerable<SelectListItem> LookupReport { get; set; }
        public int PhaseId { get; set; }
        public bool Locked { get; set; }
        public string BrowserName { get; set; }

        public int ActionCellWidth
        {
            get
            {
                int width = 150;

                switch(BrowserName)
                {
                    case "Chrome":
                        width = 125;
                        break;
                    case "Firefox":
                        width = 125;
                        break;
                    default:
                        width = 150;
                        break;
                }

                return width;
            }
        }
        #endregion
    }
}