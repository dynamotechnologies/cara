﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain.Report.Personal;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    public class MyReportModel : AbstractViewModel
    {
        public int myReportId { get; set; }
        public string Name { get; set; }
        public bool Private {get; set;}
        public ICollection<MyReport> MyReports
        {
            get
            {
                IList<MyReport> myReports;
                using (var myReportManager = ManagerFactory.CreateInstance<MyReportManager>())
                {
                    int userId = Utilities.GetUserSession().UserId;
                    myReports = myReportManager.All.Where(x => x.UserId == userId).ToList();
                }

                return myReports;
            }
        }
        public SelectList MyReportListOptions
        {
            get
            {
                return new SelectList(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("All", string.Format("Private & Shared {0}", Utilities.GetResourceString("MyReports"))),
                        new KeyValuePair<string, string>("MyReport", string.Format("Private {0} Only", Utilities.GetResourceString("MyReports"))),
                        new KeyValuePair<string, string>("PublicReport", string.Format("Shared {0} Only", Utilities.GetResourceString("MyReports"))),
                    }, "Key", "Value", "MyReport"
                );
            }
        }
    }
}