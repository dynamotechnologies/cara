﻿using System.Xml;
using System.Configuration;
using System.Linq;

using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain;
using Aquilent.Framework.Security;
using System.Security.Cryptography;
using Aquilent.Cara.Domain.DataMart;
using System.Web;
using System;
using System.Web.Security;
using log4net;
using System.Collections.Generic;

namespace Aquilent.Cara.WebApp.Models
{
    #region Model
    /// <summary>
    ///  This model manages the behavior and data of the authentication domain
    /// </summary>
    public class AuthenticationModel : AbstractModel
    {

    }
    #endregion

    #region Service
    /// <summary>
    /// Login Authentication Service
    /// </summary>
    public static class AuthenticationService
    {
        #region Public Methods
        /// <summary>
        /// Returns the output xml based on the login request validation result of the input service credential and user name
        /// </summary>
        /// <param name="serviceUserName"></param>
        /// <param name="serviceUserPassword"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static string ValidateLoginRequest(string serviceName, string servicePassword, string username)
        {
            bool success = false;
			SecurityToken token = null;

			try
			{
                string hashedServicePassword;
                IList<SecuritySetting> settings;
                using (var securitySettings = ManagerFactory.CreateInstance<SecuritySettingManager>())
                {
                    hashedServicePassword = servicePassword.ComputeHash(new SHA1CryptoServiceProvider());

                    // Get the security settings
                    settings = securitySettings.Find(x => x.Name == "security.auth.servicename" || x.Name == "security.auth.servicepassword").OrderBy(x => x.Name).ToList();
                }

				// Verify the service username and password
                if (!string.IsNullOrEmpty(serviceName) && !string.IsNullOrEmpty(servicePassword) &&
                        serviceName == settings[0].Value &&
                        hashedServicePassword == settings[1].Value)
                {
                    using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                    {
                        // Validate the user info from CARA and DataMart
                        var datamart = new DataMartManager();
                        datamart.UserManager = userManager;

                        var user = datamart.GetUser(username);

                        // If the user is found, create security token with username
                        token = new SecurityToken
                            {
                                User = user,
                                CreationDate = DateTime.UtcNow,
                                Token = Guid.NewGuid()
                            };

                        // Save the token in the repository
                        using (var tokenManager = ManagerFactory.CreateInstance<SecurityTokenManager>())
                        {
                            tokenManager.UnitOfWork = datamart.UserManager.UnitOfWork;
                            tokenManager.Add(token);
                            tokenManager.UnitOfWork.Save();
                        }

                        success = true;

                        datamart.Dispose();
                    }
                }
                else
                {
                    throw new ArgumentException("The service credential provided does not match with the system");
                }
			}
			catch (Exception e)
			{
				success = false;
				token = null;

				ILog log = LogManager.GetLogger(typeof(AuthenticationService));
				log.Error("An error occurred logging in a user", e);
			}

			return GetResultXml(success, token);
        }

        /// <summary>
        /// Returns true if the input token is valid
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool ValidateToken(string token)
        {
			var success = false;

			// validate input security token using UserService
            using (var tokenManager = ManagerFactory.CreateInstance<SecurityTokenManager>())
            {
                var validToken = tokenManager.Validate(new System.Guid(token));

                if (validToken != null && validToken.IsValid)
                {
                    // if it's valid, store the user in the session
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session["SecurityToken"] = validToken;
                    FormsAuthentication.SetAuthCookie(validToken.User.Username, false);
                    success = true;
                }
            }
			
            return success;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Returns the xml string of the authentication validation result
        /// </summary>
        /// <param name="success"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private static string GetResultXml(bool success, SecurityToken token)
        {
            return string.Format(@"<login-token success=""{0}"">{1}</login-token>",
                success.ToString(), // success flag
                (success ? token.Token.ToString() : "An error occurred retrieving the login token"));   // token or error msg
        }
        #endregion
    }
    #endregion
}