﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents an author input view Model
    /// </summary>
    public class ObjectionInfoInputViewModel : AbstractViewModel
    {
        #region Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public LetterObjection ObjectionInfo { get; set; }

        #region Lookups
        /// <summary>
        /// Returns a select list of states
        /// </summary>
        public IEnumerable<SelectListItem> LookupReviewState
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupObjectionReviewStatus>().SelectionList, null, false);
            }
        }

        /// <summary>
        /// Returns a select list of countries
        /// </summary>
        public IEnumerable<SelectListItem> LookupOutcome
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupObjectionOutcome>().SelectionList, string.Empty, false);
            }
        }
        #endregion
        #endregion

        #region Public Methods
        /// <summary>
        /// Save an author from the internal form
        /// </summary>
        /// <returns></returns>
        //public int Save(ModelStateDictionary modelState)
        //{

        //}

        /// <summary>
        /// Validate the author input with defined buinsess rules
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public virtual void Validate(ModelStateDictionary modelState)
        {
            if (ObjectionInfo != null)
            {
                // Check for uniqueness by forest and fiscal year
                using (var objectionInfoManager = ManagerFactory.CreateInstance<LetterObjectionManager>())
                {
                    if (objectionInfoManager.All.Any(x => x.FiscalYear == ObjectionInfo.FiscalYear
                        && x.Region == ObjectionInfo.Region
                        && x.Forest == ObjectionInfo.Forest
                        && x.ObjectionNumber == ObjectionInfo.ObjectionNumber
                        && x.LetterObjectionId != ObjectionInfo.LetterObjectionId))
                    {
                        modelState.AddModelError(string.Empty, "The ObjectionNumber already exists. It must be unique per Fiscal Year and Forest.");
                    }
                }
            }
        }

        public virtual void Save()
        {
            using (var objectionInfoManager = ManagerFactory.CreateInstance<LetterObjectionManager>())
            {
                objectionInfoManager.UnitOfWork.Context.AttachTo("LetterObjections", ObjectionInfo);
                objectionInfoManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(ObjectionInfo, System.Data.EntityState.Modified);
                objectionInfoManager.UnitOfWork.Save();
            }
        }
        #endregion
    }
}