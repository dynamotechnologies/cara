﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aquilent.Cara.WebApp.Models.Abstract;
using System.Collections;

namespace Aquilent.Cara.WebApp.Models
{
    public class StandardReportWithGroupingViewModel : AbstractStandardReportViewModel
    {
        #region Properties
        public IEnumerable ReportData { get; set; }
        #endregion
    }
}