﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a support view Model
    /// </summary>
    public class SupportViewModel : AbstractViewModel
    {
        #region Properties
        public string Mode { get; set; }

        public string Controller
        {
            get
            {
                return Mode;
            }
        }

        public string PalsLoginUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["palsLoginUrl"];
            }
        }

        public string SupportEmailAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["supportEmailAddress"];
            }
        }
        #endregion
    }
}