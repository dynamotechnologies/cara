﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a form set selection view Model
    /// </summary>
    public class FormSetSelectViewModel : AbstractViewModel
    {
        #region Properties
        public ICollection<FormSet> FormSets { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int LetterId { get; set; }
        #endregion        
    }
}