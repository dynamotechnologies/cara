﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter phase code selection view Model
    /// </summary>
    public class LetterPhaseCodeViewModel : AbstractViewModel
    {
        #region Properties
        public int PhaseId { get; set; }
        public string SearchCodeText { get; set; }
        
        public ICollection<PhaseCodeSelect> PhaseCodeTree
        {
            get
            {
                ICollection<PhaseCodeSelect> pcs;
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phaseCodes = phaseManager.GetPhaseCodes(
                        new PhaseCodeSelectFilter
                        {
                            PhaseId = PhaseId,
                            CodeSearch = new CodeSearch(SearchCodeText)
                        }).Where(p => p.PhaseId.HasValue).ToList();

                    PhaseCodeSelect mostUsedCodesCategory = null;
                    if (string.IsNullOrEmpty(SearchCodeText))
                    {
                        var mostUsedCodes = phaseManager.GetMostUsedPhaseCodes(PhaseId);
                        
                        if (mostUsedCodes.Count > 0)
                        {
                            // create category group header in phase code object
                            mostUsedCodesCategory = new PhaseCodeSelect
                            {
                                CodeNumber = "Most Used Codes",
                                CodeName = string.Empty,
                                PhaseCodeId = 0,
                                CodeId = 0,
                                CodeCategoryId = -1, // Testing
                            };

                            // popupate the tree view and add to the group header
                            mostUsedCodesCategory.ChildCodes = mostUsedCodes.Select(x => new PhaseCodeSelect
                                {
                                    CodeName = string.Format("{0} ({1})", x.CodeName, x.TimesUsed),
                                    CodeNumber = x.CodeNumber,
                                    PhaseCodeId = x.PhaseCodeId
                                }).ToList();
                        }
                    }

                    pcs = CodeUtilities.ToPhaseCodeTreeByType(phaseCodes);

                    // Add the mostUsedCategory to the tree
                    if (mostUsedCodesCategory != null)
                    {
                        (pcs as List<PhaseCodeSelect>).Insert(0, mostUsedCodesCategory);
                    }
                }

                return pcs;
            }
        }

        public bool Expand { get; set; }
        #endregion       
    }
}
