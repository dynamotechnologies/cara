﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using System.Web.Mvc;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.Dmd;


namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a document view Model
    /// </summary>
    public class DmdDocumentViewModel : AbstractWizardModel
    {
        #region Properties
        public ICollection<DmdDocument> Documents { get; set; }
        public ICollection<DmdDocument> ProjectDocuments { get; set; }
        public bool ShowButtons { get; set; }
		public int? ProjectDocumentId { get; set; }
        public bool IsPalsProject { get; set; }
        public string PalsProjectUrl
        {
            get
            {
				return ConfigurationManager.AppSettings["palsProjectDocumentUrl"].Replace("[[palsId]]", ProjectDocumentId.ToString());
            }
        }
        #endregion

        /// <summary>
        /// Add attachments to the letteras letter documents
        /// </summary>
        /// <param name="files"></param>
        public void AddDocuments(HttpFileCollectionBase files, HttpRequestBase request, ModelStateDictionary modelState, int projectId, int phaseId)
        {
            try
            {
                using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                {
                    var documents = new List<Document>();

                    // validate file size
                    ValidateDocuments(files, modelState);

                    if (modelState.IsValid && files != null)
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            var hpf = files[i];

                            using (hpf.InputStream)
                            {
                                // save the attachment
                                if (hpf != null && hpf.ContentLength > 0)
                                {
                                    var fileAttachment = new Aquilent.Framework.Documents.File(hpf);
                                    fileAttachment.Uploaded = DateTime.UtcNow;
                                    fileAttachment.UploadedBy = Utilities.GetUserSession().Username;

                                    var documentName = request.Params["DocumentName" + (i + 1)];
                                    var document = new Document { File = fileAttachment, Name = string.IsNullOrWhiteSpace(documentName) ? fileAttachment.OriginalName : documentName };

                                    // add to the document list
                                    documents.Add(document);

                                    // add to the doc manager
                                    docManager.Add(document);
                                }
                            }
                        }

                        // save the documents
                        if (documents.Count > 0)
                        {
                            docManager.UnitOfWork.Save();

                            var dmdManager = new DmdManager();
                            string dmdId;
                            foreach (var document in documents)
                            {
                                dmdId = dmdManager.PutCommentPeriodDocument(Utilities.GetUserSession(), projectId, phaseId, document);
                                if (!string.IsNullOrEmpty(dmdId))
                                {
                                    docManager.UnitOfWork.Context.AttachTo("Documents", document); // not sure this is necessary
                                    document.DmdId = dmdId;
                                }
                                else
                                {
                                    Logger.ErrorFormat("Failed to send comment period document ID {0} to the DMD", document.DocumentId);
                                }
                            }

                            // Save DMD Ids
                            docManager.UnitOfWork.Save();
                        }
                    }

                    using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                    {
                        var project = projectManager.Get(projectId);
                        foreach (var document in documents)
                        {
                            project.ProjectDocuments.Add( new ProjectDocument { ProjectId = projectId, PhaseId = phaseId, DmdId = document.DmdId  });
                        }
                        projectManager.UnitOfWork.Save();
                    }
                }
            }
            catch (InvalidFileTypeException ifte)
            {
                string message = string.Format("This file type is not supported and cannot be uploaded. Please select one of the following supported file types: "
                    + "bmp, doc, docx, gif, htm, html, jpeg, jpg, pdf, png, ppt, pptx, rtf, sgml, tiff, txt, wpd, xls, xlsx, and xml");
                LogError(this.GetType(), message, ifte);

                modelState.AddModelError(string.Empty, message);
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred while adding the {0}.", Utilities.GetResourceString(Constants.DOCUMENT).Pluralize());
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);
            }
        }

        /// <summary>
        /// Validates document size in the file collection against the configured setting
        /// </summary>
        /// <param name="files"></param>
        /// <param name="modelState"></param>
        public void ValidateDocuments(HttpFileCollectionBase files, ModelStateDictionary modelState)
        {
            if (files != null)
            {
                var totalSize = 0;

                // loop through the file collection and sum up the file sizes
                for (int i = 0; i < files.Count; i++ )
                {
                    var hpf = files[i];

                    // add file size (bytes)
                    if (hpf != null && hpf.ContentLength > 0)
                    {
                        totalSize += hpf.ContentLength;
                    }
                }

                if (totalSize > int.Parse(ConfigurationManager.AppSettings["maxFileUploadSize"]))
                {
                    modelState.AddModelError(string.Empty, "Total file upload size cannot excceed " +
                        (int.Parse(ConfigurationManager.AppSettings["maxFileUploadSize"]) / 1024000) + " MBs.");
                }
            }
        }
    }
}