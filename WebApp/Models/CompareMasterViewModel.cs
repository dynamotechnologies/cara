﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DiffPlex.DiffBuilder.Model;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter compare master view Model
    /// </summary>
    public class CompareMasterViewModel : AbstractViewModel
    {
        #region Properties
        public SideBySideDiffModel SideBySideDiffModel { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public Letter MasterLetter { get; set; }
        public Letter Letter { get; set; }
        public Letter NextLetter { get; set; }
        public Letter PrevLetter { get; set; }
        public bool CanEdit { get; set; }
        public int LetterFormSetCount { get; set; }
        public FormSet MasterLetterFormSet { get; set; }
        public bool CanNavigate { get; set; }

        /// <summary>
        /// Returns a select list of form sets
        /// </summary>
        public IEnumerable<SelectListItem> LookupFormSet
        {
            get
            {
                // get the form sets for the phase (Note: key is the master form id)
                IList<KeyValuePair<int, string>> selectList;
                using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                {
                    var formSets = formSetManager.All.Where(x => x.PhaseId == PhaseId);
                    var tempList = formSets.Select(fs => new { Key = fs.MasterFormId, Value = fs.Name });

                    selectList = new List<KeyValuePair<int, string>>(tempList.Count());
                    foreach (var kvp in tempList)
                    {
                        selectList.Add(new KeyValuePair<int, string>(kvp.Key, kvp.Value));
                    }
                }

                return Utilities.ToSelectList(selectList, string.Empty);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a select list of actions
        /// </summary>
        public IEnumerable<SelectListItem> LookupAction
        {
            get
            {
                var list = new List<KeyValuePair<string, string>>();

                if (Letter != null)
                {
                    var isMasterForm = Letter.IsMasterForm;
                    var letterCount = LetterFormSetCount;
                    var formSetId = Letter.FormSetId;
                    var letterTypeName = Letter.LetterTypeName;

                    var masterFormSetId = new Nullable<int>();

                    if (MasterLetter != null)
                    {
                        masterFormSetId = MasterLetter.FormSetId;
                    }

                    if (Letter.LetterTypeId == 1) /* Duplicate */
                    {
                        list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                    }

                    // add this option if it's a form set and 1. not a master form or 2. is a master form and the last letter in the form set
                    if ((formSetId.HasValue && (!isMasterForm || (isMasterForm && letterCount <= 1))) || (MasterLetter.FormSetId != null && Letter.LetterTypeId == 1))
                    {
                        switch (letterTypeName)
                        {
                            case "Duplicate":
                                list.Add(new KeyValuePair<string, string>("Form", "Form"));
                                list.Add(new KeyValuePair<string, string>("Form Plus", "Form Plus"));
                                break;
                            case "Unique":
                                list.Add(new KeyValuePair<string, string>("Form", "Form"));
                                list.Add(new KeyValuePair<string, string>("Form Plus", "Form Plus"));
                                break;
                            case "Form":
                                list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                                list.Add(new KeyValuePair<string, string>("Form Plus", "Form Plus"));
                                break;
                            case "Form Plus":
                                list.Add(new KeyValuePair<string, string>("Unique", "Unique"));
                                list.Add(new KeyValuePair<string, string>("Form", "Form"));
                                break;
                        }   
                    }


                    // add this option if it's not a master
                    if (formSetId.HasValue && !isMasterForm && Letter.LetterTypeId != 1)
                    {
                        list.Add(new KeyValuePair<string, string>("Master", "Make " + Utilities.GetResourceString("MasterForm")));
                    }

                    // add this option if the master form exists
                    if (masterFormSetId.HasValue && (!formSetId.HasValue || (formSetId.HasValue && masterFormSetId.Value != formSetId.Value)))
                    {
                        list.Add(new KeyValuePair<string, string>(Constants.SEND, string.Format("{0} to {1}", Constants.SEND, MasterLetterFormSet.Name)));
                    }
                }

                return Utilities.ToSelectList(list, string.Empty, false);
            }
        }
        #endregion
    }
}