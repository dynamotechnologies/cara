﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Security;
using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a user listing view Model
    /// </summary>
    public class UserListViewModel : AbstractListViewModel
    {
        #region Properties
        public int UserId { get; set; }
        public ICollection<UserSearchResult> Users { get; set; }
        public string Username { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UnitId { get; set; }
        public bool ShowOnlyNonProjectRoles { get; set; }

        public IEnumerable<Aquilent.Cara.Reference.Unit> Units
        {
            get
            {
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();
                return lookupUnit.Where(unit => unit.IsRegion);
            }
        }

        public string UnitName
        {
            get
            {
                string unitName = string.Empty;
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();

                if (!string.IsNullOrEmpty(UnitId))
                {
                    unitName = lookupUnit[UnitId].Name;
                }

                return unitName;
            }
        }

        /// <summary>
        /// Returns a select list of system roles
        /// </summary>
        public IEnumerable<SelectListItem> LookupRole
        {
            get
            {
                IList<Role> roles;
                using (var roleManager = ManagerFactory.CreateInstance<RoleManager>())
                {
                    roles = roleManager.All.Where(r => (r.ProjectRole == false && !r.Name.Equals("Reader"))).ToList();
                }

                return ToRoleSelectList(roles);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as UserSearchResultFilter;

                UserId = filter.UserId;
                Username = filter.Username;
                LastName = filter.LastName;
                FirstName = filter.FirstName;
                UnitId = filter.UnitId;
                ShowOnlyNonProjectRoles = filter.ShowOnlyNonProjectRoles;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new UserSearchResultFilter();

            filter.UserId = UserId;
            filter.Username = Username;
            filter.LastName = LastName;
            filter.FirstName = FirstName;
            filter.UnitId = UnitId;
            filter.ShowOnlyNonProjectRoles = ShowOnlyNonProjectRoles;

            return filter;
        }

        /// <summary>
        /// Return a select list of user roles
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToRoleSelectList(IList<Role> roles)
        {
            IEnumerable<SelectListItem> selectList = new List<SelectListItem>
            {
                new SelectListItem { Text = string.Empty, Value = null }
            };

            if (roles != null)
            {
                selectList = selectList.Concat(roles.Select(role => new SelectListItem()
                    {
                        Value = role.RoleId.ToString(),
                        Text = role.Name.ToString()
                    })
                );
            }

            return selectList;
        }
        #endregion
    }
}