﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a summary view Model
    /// </summary>
    public class SummaryViewModel : AbstractWizardModel
    {
        #region Properties
        public Project Project { get; set; }
        public Phase Phase { get; set; }
        #endregion
    }
}