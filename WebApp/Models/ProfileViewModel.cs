﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Cara.Domain;
using Aquilent.Framework.Security;
using System.Web.Mvc;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a my profile view Model
    /// </summary>
    public class ProfileViewModel : AbstractViewModel
    {

        #region Properties
        private int phaseEndThreshold = -1;
        public User User { get; set; }

        /// <summary>
        /// Returns the configurable setting of the phase end threshold in number of days
        /// </summary>
        public int PhaseEndThreshold
        {
            get
            {
                if (phaseEndThreshold < 0)
                {
                    string phaseEndThresholdString;
                    using (var caraSettingManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                    {
                        phaseEndThresholdString = caraSettingManager.Get("cara.sys.phaseend.threshold").Value;
                    }

                    int.TryParse(phaseEndThresholdString, out phaseEndThreshold);
                }

                return phaseEndThreshold;
            }
        }

        /// <summary>
        /// Returns a list of time zones
        /// </summary>
        public SelectList TimeZones
        {
            get
            {

                return new SelectList(LookupManager.GetLookup<LookupTimeZone>().SelectionList, "Key", "Value");
            }
        }

        /// <summary>
        /// Returns a list of submission values
        /// </summary>
        public SelectList Submissions
        {
            get
            {
                
                return new SelectList (new int[] { 1, 10, 30, 50, 100, 500, 1000 });
            }
        }
        
        /// <summary>
        /// Returns a list of row per listing page
        /// </summary>
        public SelectList Rows
        {
            get
            {
                return new SelectList(new int[] { 10, 25, 50 });
            }
        }

        /// <summary>
        /// Returns a list of letter font size
        /// </summary>
        public SelectList LetterFontSizes
        {
            get
            {
                return new SelectList(new int[] { 10, 12, 14 });
            }
        }

        /// <summary>
        /// Saves the user profile
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            bool result = true;

            try
            {
                // update the user profile data
                using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                {
                    userManager.UnitOfWork.Context.AttachTo("Users", User);
                    userManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(User, System.Data.EntityState.Modified);
                    userManager.UnitOfWork.Save();
                }

                // Update the session info, so the user doesn't have to logout and back in
                // for the changes to take affect
                var sessionUser = Utilities.GetUserSession();
                sessionUser.SearchResultsPerPage = User.SearchResultsPerPage;
                sessionUser.LetterFontSize = User.LetterFontSize;
            }
            catch
            {
                result = false;
            }

            return result;
        }
        #endregion
    }
}