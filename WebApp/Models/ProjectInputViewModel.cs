﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a project input view Model
    /// </summary>
    public class ProjectInputViewModel : AbstractWizardModel
    {
        #region Properties
        public DataMartProjectViewModel DataMartProjectViewModel { get; set; }
        public string ProjectNameOrId { get; set; }
        public bool? IsPalsProject { get; set; }
        public bool IsOrmsProject { get; set; }
        public string NonPalsProjectName { get; set; }
        public string NonPalsDescription { get; set; }
        public string NonPalsUnit { get; set; }
        public string Action { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }

        public string NonPalsUnitName
        {
            get
            {
                string unitName = string.Empty;
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();

                if (!string.IsNullOrEmpty(NonPalsUnit))
                {
                    unitName = lookupUnit[NonPalsUnit].Name;
                }

                return unitName;
            }
        }

        public IEnumerable<Unit> Units
        {
            get
            {
                User currentUser = Utilities.GetUserSession();
                IEnumerable<Unit> lookup = null;
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();
                if (currentUser.HasSystemPrivilege("MSU"))
                {
                    lookup = lookupUnit.Where(unit => unit.IsRegion && unit.Active);
                }
                else
                {
                    // Restrict unit selection by membership ordered by UnitId
                    // Units stored in the user are padded by 0s to make each unitId 8 characters long

                    var units = currentUser.UnitPrivileges.Where(p => p.Key.Trim().Equals("MPHS")).SelectMany(x => x.Value).Distinct().ToList();

                    // Remove any units in the list that already have a parent
                    string tempUnit = "TEMPUNIT";
                    string trimmedUnit;
                    var relevantUnits = new List<string>();
                    foreach (var unitId in units)
                    {
                        if (unitId.EndsWith("0000"))
                        {
                            trimmedUnit = unitId.Substring(0, unitId.Length - 4);
                        }
                        else if (unitId.EndsWith("00"))
                        {
                            trimmedUnit = unitId.Substring(0, unitId.Length - 2);
                        }
                        else
                        {
                            trimmedUnit = unitId;
                        }

                        if (!trimmedUnit.StartsWith(tempUnit))
                        {
                            relevantUnits.Add(trimmedUnit);
                        }

                        tempUnit = trimmedUnit;
                    }

                    lookup = lookupUnit.Where(unit => relevantUnits.Contains(unit.UnitId) && unit.Active);
                }

                return lookup;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the project input model
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="action"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            if (!IsPalsProject.HasValue)
            {
                modelState.AddModelError(string.Empty, "Please select a project type.");
            }
            else if (!IsPalsProject.Value)
            {
                if (string.IsNullOrEmpty(NonPalsProjectName))
                {
                    modelState.AddModelError(string.Empty, Constants.PROJECT + " Name is required.");
                }

                if (string.IsNullOrEmpty(NonPalsDescription))
                {
                    modelState.AddModelError(string.Empty, Constants.PROJECT + " Description is required.");
                }
                else if (NonPalsDescription.Length > 4000)
                {
                    modelState.AddModelError(string.Empty, Constants.PROJECT + "Description may not exceed 4000 characters. You have entered " + NonPalsDescription.Length + " characters.");
                }

                if (string.IsNullOrEmpty(NonPalsUnit) || NonPalsUnit.Length < 4)
                {
                    modelState.AddModelError(string.Empty, "Lead Management Unit is required.");
                }
            }
            else
            {
                if (ProjectId == 0 && string.IsNullOrEmpty(DataMartProjectViewModel.ProjectId))
                {
                    modelState.AddModelError(string.Empty, string.Format("Please select a {0} {1} ID.", Utilities.GetResourceString("PALS"), Constants.PROJECT));
                }
            }
        }

        /// <summary>
        /// Save a project into CARA system
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns>CARA Project ID</returns>
        public int Save(ModelStateDictionary modelState)
        {
            var projectId = 0;
            var isPalsProject = IsPalsProject.HasValue && IsPalsProject.Value;            
            var isExistingProject = ProjectId > 0;

            try
            {
                // save new project
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    var dmManager = new DataMartManager();
                    dmManager.ProjectManager = projectManager;

                    Project caraProject = new Project();
                    if (isExistingProject)
                    {
                        caraProject = projectManager.Get(ProjectId);
                    }
                    else
                    {
                        // get the project by selected PALS project ID
                        if (isPalsProject)
                        {
                            caraProject = dmManager.GetProject(int.Parse(DataMartProjectViewModel.ProjectId));
                        }
                        //Added for third type ORMS
                        else if (IsOrmsProject)
                        {
                            caraProject.ProjectTypeId = LookupManager.GetLookup<LookupProjectType>().First(x => x.Name == "ORMS").ProjectTypeId;
                            caraProject.ProjectNumber = "TempProjectNumber"; // Replaced by database trigger on insert
                            caraProject.AnalysisTypeId = LookupManager.GetLookup<LookupAnalysisType>().First(x => x.Name == "N/A").AnalysisTypeId;
                            caraProject.ProjectStatusId = LookupManager.GetLookup<LookupProjectStatus>().First(x => x.Name == "N/A").ProjectStatusId;
                            caraProject.CommentRuleId = new int?();
                        }

                        else 
                        {
                            caraProject.ProjectTypeId = LookupManager.GetLookup<LookupProjectType>().First(x => x.Name == "Non-PALS").ProjectTypeId;
                            caraProject.ProjectNumber = "TempProjectNumber"; // Replaced by database trigger on insert
                            caraProject.AnalysisTypeId = LookupManager.GetLookup<LookupAnalysisType>().First(x => x.Name == "N/A").AnalysisTypeId;
                            caraProject.ProjectStatusId = LookupManager.GetLookup<LookupProjectStatus>().First(x => x.Name == "N/A").ProjectStatusId;
                            caraProject.CommentRuleId = new int?();
                        }
                    }

                    if (!isPalsProject)
                    {
                        caraProject.Name = NonPalsProjectName;
                        caraProject.Description = NonPalsDescription;
                        caraProject.UnitId = NonPalsUnit;
                    }

                    caraProject.Deleted = false;
                    caraProject.LastUpdated = DateTime.UtcNow;
                    if (caraProject.CreateDate.Year == 1)
                    {
                        caraProject.CreateDate = caraProject.LastUpdated;
                    }
                    caraProject.LastUpdatedBy = Utilities.GetUserSession().Username;

                    // save the CARA project
                    if (!isExistingProject)
                    {
                        dmManager.ProjectManager.Add(caraProject);
                    }

                    dmManager.ProjectManager.UnitOfWork.Save();
                    projectId = caraProject.ProjectId;

                    // link the project to PALS project
                    if (!isExistingProject && isPalsProject && !dmManager.LinkProject(caraProject.ProjectId, int.Parse(caraProject.ProjectNumber)))
                    {
                        modelState.AddModelError(string.Empty, string.Format("An error occurred while linking {0} {1} to {2}.", Constants.PROJECT,
                            caraProject.ProjectNumber, Utilities.GetResourceString("PALS")));
                    }

                    dmManager.Dispose();
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PROJECT, Utilities.GetResourceString("AppNameAbbv"));
                LogError(this.GetType(), message, ex);

                modelState.AddModelError(string.Empty, message);

                projectId = 0;
            }

            return projectId;
        }
        #endregion
    }
}