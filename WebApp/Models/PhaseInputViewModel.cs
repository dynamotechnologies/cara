﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase input view Model
    /// </summary>
    public class PhaseInputViewModel : AbstractWizardModel
    {
        #region Properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public Phase Phase { get; set; }
        public int? Duration { get; set; }
        public int DefaultDuration { get; set; }
        public int DefaultObjectionDuration { get; set; }
        public bool IsPalsProject { get; set; }
        public DateTime OriginalCommentEnd { get; set; }
        
        string selectedTimeZone = string.Empty;
        string configTimeZone = string.Empty;
        
        /// <summary>
        /// NOTE: if Phase.TimeZone is used to bind to ddl (instead of PhaseInputViewModel.SelectedTimeZone), 
        /// modelstate.isvalid returns false because entire Phase object is validated
        /// and form does not contain input fields for all Phase object elements.
        /// </summary>
        public string SelectedTimeZone { get; set; }
        public SelectList TimeZoneList
        {
            get
            {
                //If phase exists retrieve time zone
                var isExistingPhase = PhaseId > 0;
                try
                {
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        if (isExistingPhase)
                        {
                            selectedTimeZone = phaseManager.Get(PhaseId).TimeZone;
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred while creating the {0} in {1}.", Constants.PHASE, Utilities.GetResourceString("AppNameAbbv"));
                    LogError(this.GetType(), message, ex);
                }

                //If project does not exist or if existing project's time zone is null
                //retrieve default time zone from config
                if (String.IsNullOrEmpty(selectedTimeZone))
                {
                    try
                    {
                        configTimeZone = ConfigurationManager.AppSettings["CommentPeriodTimeZoneEnd"];
                    }
                    catch (ConfigurationErrorsException cee)
                    {
                        configTimeZone = "UTC-11"; //key for the default time zone in case config can't be read
                    }
                    selectedTimeZone = configTimeZone;
                }
                return new SelectList(LookupManager.GetLookup<LookupTimeZone>().SelectionList, "Key", "Value", selectedTimeZone);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the phase input model based on action
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="action"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            // validate the input data
            if (Phase.PhaseTypeId <= 0)
            {
                modelState.AddModelError(string.Empty, Utilities.GetResourceString(Constants.PHASE) + " is required.");
            }

            if ((Duration == null || !Duration.HasValue) && !Phase.AlwaysOpen)
            {
                modelState.AddModelError("Duration", "Duration is required.");
            }

            if (Phase.PhaseTypeId == 6 /* Other */ && string.IsNullOrEmpty(Phase.Name))
            {
                modelState.AddModelError("Description", "Description is required when the Comment Period is 'Other'.");
            }

            if (Phase.PhaseTypeId == 3 /* Objection */ )
            {
                if (Phase.ObjectionResponseExtendedBy > 0 && string.IsNullOrWhiteSpace(Phase.ObjectionResponseExtendedJustification))
                {
                    modelState.AddModelError("Reason for Extension", "Reason for Extension is required when the Response Extended By is greater than 0");
                }

                if (!string.IsNullOrWhiteSpace(Phase.ObjectionResponseExtendedJustification))
                {
                    if (Phase.ObjectionResponseExtendedJustification.Length > 1000)
                    {
                        modelState.AddModelError("Reason for Extension", "Reason for Extension is limited to 1000 characters");
                    }
                }
            }

            if (modelState.IsValid)
            {
                DateTime commentEnd = DateTime.MaxValue;
                if (!Phase.AlwaysOpen)
                {
                    commentEnd = Phase.CommentStart.AddDays(Duration.Value > 0 ? Duration.Value - 1 : 0);
                }

                // validate comment periods of the project against the input data
                if (!PhaseUtilities.IsValidatePeriod(ProjectId, PhaseId, Phase.CommentStart, commentEnd))
                {
                    modelState.AddModelError("CommentStart", string.Format("{0} cannot fall within any existing {1}s.",
                        Utilities.GetResourceString(Constants.PHASE), Utilities.GetResourceString(Constants.PHASE)));
                }
            }
        }

        /// <summary>
        /// Returns a list of PhaseTypes based on the project properties
        /// </summary>
        public IEnumerable<SelectListItem> PhaseTypes
        {
            get
            {
                IList<KeyValuePair<int, string>> kvps = LookupManager.GetLookup<LookupPhaseType>()
                    .Where(x => x.Active)
                    .Select(x => new KeyValuePair<int, string>(x.PhaseTypeId,x.Name))
                    .ToList();

                return Utilities.ToSelectList(kvps);
            }
        }

        /// <summary>
        /// Save a phase to a project
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns>Phase ID</returns>
        public int Save(ModelStateDictionary modelState)
        {
            int phaseId = 0;

            try
            {
                // set the data
                if (!Phase.AlwaysOpen)
                {
                    Phase.CommentEnd = Phase.CommentStart.AddDays(Duration.Value > 0 ? Duration.Value : 0);
                }
                else
                {
                    Phase.CommentEnd = DateTime.MaxValue;
                }

                Phase.LastUpdated = DateTime.UtcNow;
                Phase.LastUpdatedBy = Utilities.GetUserSession().Username;
                Phase.TimeZone = SelectedTimeZone;

                // save the phase data
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    // existing phase update
                    if (Phase.PhaseId > 0)
                    {
                        if (Phase.PhaseTypeId == 3 /* Objection */)
                        {
                            // Compute difference between original start and 
                            //var datediff = new TimeSpan(Phase.CommentEnd.Ticks).Subtract(new TimeSpan(OriginalCommentEnd.Ticks));

                            // Recompute the Response Due Date
                            //Phase.ObjectionResponseDue = Phase.ObjectionResponseDue.Value.Add(datediff);

                            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                            {
                                Project project = projectManager.Get(ProjectId);

                                if (project != null)
                                {
                                    var pmlLookup = LookupManager.GetLookup<LookupPhaseMinimumLength>();
                                    var pml = pmlLookup.GetValue(project.AnalysisTypeId, project.CommentRuleId.Value);

                                    if (pml.ObjectionDecisionDueDays == null)
                                    {
                                        Logger.InfoFormat("Project {0} does not have a defined Objection Decision Due Date.", ProjectId);
                                        switch (project.CommentRuleId)
                                        {
                                            case 13: /* 219 (2012) */
                                                pml.ObjectionDecisionDueDays = 90;
                                                break;
                                            case 14: /* 218 (2013) HFRA */
                                                pml.ObjectionDecisionDueDays = 30;
                                                break;
                                            case 15: /* 218 (2013) Non-HFRA */
                                                pml.ObjectionDecisionDueDays = 45;
                                                break;
                                            default:
                                                pml.ObjectionDecisionDueDays = 30;
                                                break;
                                        }
                                    }

                                    Phase.ObjectionResponseDue = Phase.CommentEnd.AddDays(pml.ObjectionDecisionDueDays.Value);
                                    //Phase.ObjectionResponseExtendedBy = 0;

                                    if (Phase.ObjectionResponseDue.Value.DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        Phase.ObjectionResponseDue = Phase.ObjectionResponseDue.Value.AddDays(2);
                                    }
                                    else if (Phase.ObjectionResponseDue.Value.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        Phase.ObjectionResponseDue = Phase.ObjectionResponseDue.Value.AddDays(1);
                                    }
                                }
                            }
                        }

                        phaseManager.UnitOfWork.Context.AttachTo("Phases", Phase);
                        phaseManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(Phase, System.Data.EntityState.Modified);
                        phaseManager.UnitOfWork.Save();

                        phaseId = Phase.PhaseId;
                    }
                    //new phase
                    else
                    {
                        // required data for new phase
                        Phase.ProjectId = ProjectId;
                        Phase.Deleted = false;
                        Phase.Locked = false;
                        Phase.Private = false;

                        // Handle Objections
                        Project project;
                        using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                        {
                            project = projectManager.Get(ProjectId);
                            if (Phase.PhaseTypeId == 3)
                            {
                                if (project != null)
                                {
                                    var pmlLookup = LookupManager.GetLookup<LookupPhaseMinimumLength>();
                                    var pml = pmlLookup.GetValue(project.AnalysisTypeId, project.CommentRuleId.Value);

                                    if (pml.ObjectionDecisionDueDays == null)
                                    {
                                        Logger.InfoFormat("Project {0} does not have a defined Objection Decision Due Date.", ProjectId);
                                        switch (project.CommentRuleId)
                                        {
                                            case 13: /* 219 (2012) */
                                                pml.ObjectionDecisionDueDays = 90;
                                                break;
                                            case 14: /* 218 (2013) HFRA */
                                                pml.ObjectionDecisionDueDays = 30;
                                                break;
                                            case 15: /* 218 (2013) Non-HFRA */
                                                pml.ObjectionDecisionDueDays = 45;
                                                break;
                                            default:
                                                pml.ObjectionDecisionDueDays = 30;
                                                break;
                                        }
                                    }

                                    Phase.ObjectionResponseDue = Phase.CommentEnd.AddDays(pml.ObjectionDecisionDueDays.Value);
                                    //Phase.ObjectionResponseExtendedBy = 0;

                                    if (Phase.ObjectionResponseDue.Value.DayOfWeek == DayOfWeek.Saturday)
                                    {
                                        Phase.ObjectionResponseDue = Phase.ObjectionResponseDue.Value.AddDays(2);
                                    }
                                    else if (Phase.ObjectionResponseDue.Value.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        Phase.ObjectionResponseDue = Phase.ObjectionResponseDue.Value.AddDays(1);
                                    }
                                }
                            }
                        }

                        // add a new phase
                        phaseManager.Add(Phase);
                        phaseManager.UpdateReadingRoomStatus(Phase, Phase.ReadingRoomActive);

                        // add project manager from project
                        var pmr = new PhaseMemberRole
                        {
                            UserId = Utilities.GetUserSession().UserId,
                            PhaseId = Phase.PhaseId,
                            CanRemove = true,
                            Active = true,
                            LastUpdated = DateTime.UtcNow,
                            LastUpdatedBy = Utilities.GetUserSession().Username,
                            RoleId = LookupManager.GetLookup<LookupRole>()[4].RoleId,  // Team Member
                            IsPointOfContact = !project.IsPalsProject
                        };

                        // save the new phase member role
                        phaseManager.AddPhaseMemberRole(pmr);
                        phaseManager.UnitOfWork.Save();

                        // populate codes from coding template to the phase
                        int codingTemplateId;
                        using (var ctManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
                        {
                            codingTemplateId = (Phase.PhaseTypeId == 3) ? (ctManager.Get("Objection").CodingTemplateId) : (ctManager.Get("Basic").CodingTemplateId);
                            phaseManager.InsertPhaseCodes(codingTemplateId, new Nullable<int>(), null, Phase.PhaseId);
                        }

                        var termListManager = ManagerFactory.CreateInstance<TermListManager>();
                        {
                            // add default term lists
                            string termListIds = string.Empty;
                            var requiredTermLists = termListManager.All.Where(x => x.DefaultOn).ToList();
                            foreach (TermList tl in requiredTermLists)
                            {
                                termListIds += (termListIds.Length > 0 ? "," : string.Empty) + tl.TermListId.ToString();
                            }
                            phaseManager.InsertTermLists(Phase.PhaseId, Utilities.ToIdListXml(termListIds));
                        }
                        
                        // link the new phase to PALS project //
                        if (IsPalsProject)
                        {
                            var dmManager = new DataMartManager();
                            if (!dmManager.LinkPhase(ProjectId, Phase))
                            {
                                modelState.AddModelError(string.Empty, string.Format("An error occurred while linking {0} {1} to {2}", Utilities.GetResourceString(Constants.PHASE),
                                    Phase.PhaseTypeDesc, Utilities.GetResourceString("PALS")));
                            }
                            dmManager.Dispose();
                        }
                    }
                    phaseManager.Dispose();
                    phaseId = Phase.PhaseId;
                }
            }
            catch (Exception ex)
            {
                modelState.AddModelError(string.Empty, string.Format("An error occurred while saving the {0} - {1}.", Utilities.GetResourceString(Constants.PHASE),
                    ex.Message));
                phaseId = 0;
            }
            
            return phaseId;
        }
        #endregion
    }
}