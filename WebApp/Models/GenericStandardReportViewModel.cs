﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    public class GenericStandardReportViewModel<T> : AbstractStandardReportViewModel
    {
        #region Properties
        public ICollection<T> ReportData { get; set; }

        private bool _hasPaging = true;
        public bool HasPaging 
        {
            get { return _hasPaging; }
            set { _hasPaging = value; }
        }
        #endregion
    }
}