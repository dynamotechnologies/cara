﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;
using Aquilent.Cara.WebApp.Models.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    public class CustomReportControlModel : AbstractViewModel
    {
        public string Action { set; get; }
        public string Description { get; set; }
        public bool Dirty { get; set; }
        public int ReportId { get; set; }
        public GridCommand Command { get; set; }
        public int FieldId { get; set; }
        public int ProjectFieldId { get; set; }
        public int LetterFieldId { get; set; }
        public int CommenterFieldId { get; set; }
        public string FieldValue { get; set; }
        public string FieldValueName { get; set; }
        public bool FieldValueInclude { get; set; }
        public DateTime? FieldDateFrom { get; set; }
        public DateTime? FieldDateTo { get; set; }
        public int ListId { get; set; }
        public int ColumnGroup { get; set; }
        public int SortColumn1 { get; set; }
        public int SortColumn2 { get; set; }
        public int SortColumn3 { get; set; }
        public bool? Asc1 { get; set; }
        public bool? Asc2 { get; set; }
        public bool? Asc3 { get; set; }


        //deprecated
        public int WizardStep { get; set; }
        public string FieldType { get; set; }
        public string CollapsedListIds { get; set; }

    }
}