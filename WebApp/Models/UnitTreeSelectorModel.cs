﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.WebApp.Models
{
    public class UnitTreeSelectorModel
    {
        public IEnumerable<Unit> Units { get; set; }
        public string UnitIdInputId { get; set; }
        public string UnitNameInputId { get; set; }
        public bool Modal { get; set; }
        public bool Visible { get; set; }
        public bool Closeable { get; set; }
        
        private int width = 350;
        public int Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;
            }
        }

        private int height = 300;
        public int Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value;
            }
        }
    }
}