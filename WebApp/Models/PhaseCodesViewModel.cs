﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase code structure view Model
    /// </summary>
    public class PhaseCodesViewModel : AbstractViewModel
    {
        #region Private Properties
        private ICollection<PhaseCode> phaseCodeTree;
        private string treeViewName = string.Empty;
        #endregion

        #region Public Properties
        public ICollection<PhaseCode> PhaseCodes { get; set; }
        public string TreeViewName
        {
            get
            {
                return (!string.IsNullOrWhiteSpace(treeViewName) ? treeViewName : "TreeView");
            }
            
            set
            {
                treeViewName = value;
            }
        }

        public bool ExpandAll { get; set; }

        /// <summary>
        /// Returns true if a coded comment exists in the phase
        /// </summary>
        public ICollection<PhaseCode> PhaseCodeTree
        {
            get
            {
                // convert the collection of phase codes into a tree structure
                if (phaseCodeTree == null && PhaseCodes != null)
                {
                    phaseCodeTree = CodeUtilities.ToPhaseCodeTreeByType(PhaseCodes);
                }

                return phaseCodeTree;
            }
        }
        #endregion
    }
}