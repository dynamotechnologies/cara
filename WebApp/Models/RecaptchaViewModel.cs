﻿using System;
using System.Configuration;

using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a recaptcha view Model
    /// </summary>
    public class RecaptchaViewModel : AbstractViewModel
    {
        #region Properties
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Returns the Recaptcha public key
        /// </summary>
        public string PublicKey
        {
            get
            {
                return ConfigurationManager.AppSettings["reCaptchaPublicKey"];
            }
        }

        /// <summary>
        /// Returns the Recaptcha private key
        /// </summary>
        public string PrivateKey
        {
            get
            {
                return ConfigurationManager.AppSettings["reCaptchaPrivateKey"];
            }
        }

        public string RemoteIp { get; set; }
        #endregion
    }
}