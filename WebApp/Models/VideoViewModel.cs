﻿using System;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a video view Model
    /// </summary>
    public class VideoViewModel
    {
        #region Properties
        public string Name { get; set; }
        public string Mode { get; set; }

        /// <summary>
        /// Returns the window width based on the playing mode
        /// </summary>
        public int Width
        {
            get
            {
                return Constants.COMPACT.Equals(Mode) ? 355 : 850;
            }
        }

        /// <summary>
        /// Returns the window height based on the playing mode
        /// </summary>
        public int Height
        {
            get
            {
                return Constants.COMPACT.Equals(Mode) ? 245 : 675;
            }
        }

        /// <summary>
        /// Returns the url based on the video name and playing mode
        /// </summary>
        public string Url
        {
            get
            {
                return string.Format("~/Content/Videos/{0}{1}/Play.html", Name, Mode);
            }
        }
        #endregion
    }
}