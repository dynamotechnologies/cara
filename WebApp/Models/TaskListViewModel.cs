﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    public class TaskListViewModel : AbstractViewModel
    {
        public enum TaskMode { CommentsConcerns, ReviewableLetters };
        
        public List<TaskItem> TaskItems { get; set; }
        public TaskMode Mode { get; set; }
        public bool CouldFetchTasks { get; set; }
    }
}