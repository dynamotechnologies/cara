﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Aquilent.Cara.WebApp.Models
{
    public class MakeDuplicateModel
    {
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int LetterId { get; set; }
        public string NextLetterIds { get; set; }
        public int? LettersRemaining { get; set; }

        [DisplayName("Master Duplicate Letter #")]
        public int MasterDuplicateSequence { get; set; }
    }
}
