﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Abstract
{
    /// <summary>
    /// Represents an abstract view page model
    /// </summary>
    public abstract class AbstractViewModel : AbstractModel
    {
        #region View Properties
        /// <summary>
        /// Returns the page title text
        /// </summary>
        virtual public string PageTitle { get; set; }

        /// <summary>
        /// Returns the breab crumb title text
        /// </summary>
        virtual public string BreadCrumbTitle { get; set; }
        #endregion
    }
}