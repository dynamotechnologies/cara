﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Abstract
{
    /// <summary>
    /// Abstract input model for input views
    /// </summary>
    public abstract class AbstractInputViewModel : AbstractViewModel
    {
        #region Input Properties
        public PageMode Mode { get; set; }
        #endregion
    }
}