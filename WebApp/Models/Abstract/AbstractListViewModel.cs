﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Framework;

namespace Aquilent.Cara.WebApp.Models.Abstract
{
    /// <summary>
    /// Abstract view model for filterable list entity (e.g. search results)
    /// </summary>
    public abstract class AbstractListViewModel : AbstractViewModel
    {
        #region Virtual Methods
        /// <summary>
        /// Method to load the filter properties to the associated model properties
        /// </summary>
        /// <param name="filter"></param>
        public abstract void LoadFilterToModel(IFilter iFilter);

        /// <summary>
        /// Method to create the filter object from the associated model properties
        /// </summary>
        /// <returns></returns>
        public abstract IFilter GetFilterFromModel();
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns true if the input action is a submit action
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool IsSubmit(string action)
        {
            return (Constants.FILTER.Equals(action) || Constants.LIST.Equals(action));
        }

        /// <summary>
        /// Returns true if the input action is a show all records action
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool IsShowAll(string action)
        {
            return (!string.IsNullOrEmpty(action) && action.StartsWith(Constants.SHOW_ALL));
        }
        #endregion
    }
}