﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aquilent.Cara.WebApp.Models.Abstract
{
    public abstract class AbstractStandardReportViewModel : AbstractViewModel
    {
        #region Properties
        public int PhaseId { get; set; }
        public int Option { get; set; }

        /// <summary>
        /// Returns a list of available File formats for Export
        /// </summary>
        public SelectList ExportOptions
        {
            get
            {
                return new SelectList(
                    new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("", "Select format..."),
                        new KeyValuePair<string, string>("PDF", "Acrobat (PDF) file"),
                        new KeyValuePair<string, string>("CSV", "CSV (comma delimited)"),
                        new KeyValuePair<string, string>("XLSX", "Excel Worksheet"),
                        new KeyValuePair<string, string>("PPTX", "Powerpoint Presentation"),
                        new KeyValuePair<string, string>("DOCX", "Word Document"),
                    }, "Key", "Value", ""
                );
            }
        }
        #endregion
    }
}