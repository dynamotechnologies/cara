﻿
namespace Aquilent.Cara.WebApp.Models.Abstract
{
    public abstract class AbstractWizardModel : AbstractInputViewModel
    {
        #region Wizard Properties
        public string Description { get; set; }

        public bool IsWizardMode
        {
            get
            {
                return (Mode != PageMode.None ? Mode.ToString().Contains(Constants.WIZARD) : false);
            }
        }
        #endregion
    }
}