﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using log4net;

namespace Aquilent.Cara.WebApp.Models.Abstract
{
    /// <summary>
    /// Represents an abstract model for the MVC models
    /// </summary>
    public abstract class AbstractModel
    {
        #region Protected Properties
        /// <summary>
        /// Logger for the model
        /// </summary>
        protected ILog _logger;

        protected virtual ILog Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = LogManager.GetLogger(GetType());
                }

                return _logger;
            }
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Logs the thrown error to the logger
        /// </summary>
        /// <param name="type">Class Type</param>
        /// <param name="message">Error Message</param>
        /// <param name="ex">Exception</param>
        protected virtual void LogError(Type type, string message, Exception ex)
        {
            // instantiate the logger with the type
            _logger = LogManager.GetLogger(type);
            // log the error message
            _logger.Error(message, ex);
        }

        /// <summary>
        /// Logs the thrown error exception to the logger and throws the exception again
        /// </summary>
        /// <param name="type">Class Type</param>
        /// <param name="message">Error Message</param>
        /// <param name="ex">Exception</param>
        protected virtual void HandleError(Type type, string message, Exception ex)
        {
            // log the error
            LogError(type, message, ex);

            // re-throw the exception
            throw new Exception(message, ex);
        }
        #endregion
    }
}