﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework.Resource;



namespace Aquilent.Cara.WebApp.Models
{
    public class StandardReportViewModel : AbstractViewModel
    {
        public string action { get; set; }
        public string Description { get; set; }
        public ICollection<Project> Projects { get; set; }
        public string ProjectNameOrNumber { get; set; }
        public int PhaseId { get; set; }
        public int ProjectId { get; set; }
        public ICollection<Report> Reports { get; set; }
        public IEnumerable<SelectListItem> LookupReport { get; set; }
        public bool Locked { get; set; }
        public string BrowserName { get; set; }

        public int ActionCellWidth
        {
            get
            {
                int width = 150;

                switch (BrowserName)
                {
                    case "Chrome":
                        width = 125;
                        break;
                    case "Firefox":
                        width = 125;
                        break;
                    default:
                        width = 150;
                        break;
                }

                return width;
            }
        }
        /// <summary>
        /// Validate the phase input model based on action
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="action"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            if (PhaseId == null || PhaseId < 1)
            {
                modelState.AddModelError(string.Empty, Utilities.GetResourceString(Constants.PHASE) + " is required.");
            }
        }
        public StandardReportViewModel()
        {
        }
    }
}