﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Reference;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a report view list model
    /// </summary>
    public class CustomReportListViewModel : AbstractListViewModel
    {
        #region Search Properties
        public ICollection<CustomReportData> CustomReportData { get; set; }
        public bool SubmitterView { get; set; }
        public bool LetterView { get; set; }
        public bool ProjectView { get; set; }
        public string MyReportName { get; set; }
        public int? ReportUserId { get; set; }
        #endregion

        #region Submitter Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string ZipList { get; set; }
        public int? CountryId { get; set; }
        public int? OrganizationId { get; set; }
        public int? OrganizationTypeId { get; set; }
        public int? OfficialRepresentativeTypeId { get; set; }

        public IEnumerable<SelectListItem> LookupState
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupState>().SelectionList, string.Empty);
            }
        }

        public IEnumerable<SelectListItem> LookupCountry
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupCountry>().SelectionList, string.Empty, false);
            }
        }

        public IEnumerable<SelectListItem> LookupOrganization
        {
            get
            {
                IList<KeyValuePair<int, string>> list;
                using (var orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                {
                    list = orgManager.GetSelectList();
                }

                return Utilities.ToSelectList(list, string.Empty);
            }
        }

        public IEnumerable<SelectListItem> LookupOrganizationType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupOrganizationType>().SelectionList, string.Empty);
            }
        }

        public IEnumerable<SelectListItem> LookupOfficialRepresentativeType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupOfficialRepresentativeType>().SelectionList, string.Empty);
            }
        }        
        #endregion

        #region Letter/Comment Properties
        public int? DeliveryTypeId { get; set; }
        public DateTime? DateSubmittedFrom { get; set; }
        public DateTime? DateSubmittedTo { get; set; }
        public int? LetterSequence { get; set; }
        public string CodeSearchText { get; set; }
        public string LetterText { get; set; }
        public string ConcernText { get; set; }
        public string ResponseText { get; set; }

        public IEnumerable<SelectListItem> LookupDeliveryType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupDeliveryType>().SelectionList, string.Empty);
            }
        }
        #endregion
        
        #region Project Information Properties
        public string ProjectNameOrId { get; set; }
        public int? ProjectActivityId { get; set; }
        public int? PhaseTypeId { get; set; }
        public int? AnalysisTypeId { get; set; }
        public string PhaseName { get; set; }
        public string UnitCode { get; set; }
        public int? ProjectTypeId { get; set; }
        public string ProjectStateId { get; set; }
        public string ProjectCountyName { get; set; }

        /// <summary>
        /// Returns a select list of activities
        /// </summary>
        public IEnumerable<SelectListItem> LookupActivity
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupActivity>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of phase types
        /// </summary>
        public IEnumerable<SelectListItem> LookupPhaseType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupPhaseType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of counties
        /// </summary>
        public IEnumerable<SelectListItem> LookupCounty
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupCounty>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of a analysis types
        /// </summary>
        public IEnumerable<SelectListItem> LookupAnalysisType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupAnalysisType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of yes/no
        /// </summary>
        public IEnumerable<SelectListItem> LookupYesNo
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupYesNo>().SelectionList, string.Empty);
            }
        }

		/// <summary>
		/// Returns a select list of counties
		/// </summary>
		public IEnumerable<SelectListItem> LookupProjectType
		{
			get
			{
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupProjectType>().SelectionList, string.Empty);
			}
		}
        #endregion

        #region Methods
        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as CustomReportFilter;

                ReportUserId = filter.ReportUserId;
                FirstName = filter.FirstName;
                LastName = filter.LastName;
                City = filter.City;
                StateId = filter.StateId;
                ZipList = filter.ZipList;
                CountryId = filter.CountryId;
                OrganizationId = filter.OrganizationId;
                OrganizationTypeId = filter.OrganizationTypeId;
                OfficialRepresentativeTypeId = filter.OfficialRepresentativeTypeId;

                DeliveryTypeId = filter.DeliveryTypeId;
                DateSubmittedFrom = filter.DateSubmittedFrom;
                DateSubmittedTo = filter.DateSubmittedTo;
                LetterSequence = filter.LetterSequence;
                CodeSearchText = (filter.CodeSearch == null ? null : filter.CodeSearch.CodeSearchText);
                LetterText = filter.LetterText;
                ConcernText = filter.ConcernText;
                ResponseText = filter.ResponseText;
        
                ProjectNameOrId = filter.ProjectNameOrId;
                ProjectActivityId = filter.ProjectActivityId;
                PhaseTypeId = filter.PhaseTypeId;
                AnalysisTypeId = filter.AnalysisTypeId;
                PhaseName = filter.PhaseName;
                UnitCode = filter.UnitCode;
                ProjectTypeId = filter.ProjectTypeId;
                ProjectStateId = filter.ProjectStateId;
                ProjectCountyName = filter.ProjectCountyName;

                SubmitterView = filter.SubmitterView;
                LetterView = filter.LetterView;
                ProjectView = filter.ProjectView;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new CustomReportFilter();

            filter.ReportUserId = ReportUserId;
            filter.FirstName = FirstName;
            filter.LastName = LastName;
            filter.City = City;
            filter.StateId = StateId;
            filter.ZipList = ZipList;
            filter.CountryId = CountryId;
            filter.OrganizationId = OrganizationId;
            filter.OrganizationTypeId = OrganizationTypeId;
            filter.OfficialRepresentativeTypeId = OfficialRepresentativeTypeId;

            filter.DeliveryTypeId = DeliveryTypeId;
            filter.DateSubmittedFrom = DateSubmittedFrom;
            filter.DateSubmittedTo = DateSubmittedTo;
            filter.LetterSequence = LetterSequence;
            if (!string.IsNullOrEmpty(CodeSearchText))
            {
                filter.CodeSearch = new CodeSearch(CodeSearchText);
            }
            filter.LetterText = LetterText;
            filter.ConcernText = ConcernText;
            filter.ResponseText = ResponseText;

            filter.ProjectNameOrId = ProjectNameOrId;
            filter.ProjectActivityId = ProjectActivityId;
            filter.PhaseTypeId = PhaseTypeId;
            filter.AnalysisTypeId = AnalysisTypeId;
            filter.PhaseName = PhaseName;
            filter.UnitCode = UnitCode;
            filter.ProjectTypeId = ProjectTypeId;
            filter.ProjectStateId = ProjectStateId;
            filter.ProjectCountyName = ProjectCountyName;

            filter.SubmitterView = SubmitterView;
            filter.LetterView = LetterView;
            filter.ProjectView = ProjectView;
			filter.PageNum = 1;
            
            return filter;
        }

        /// <summary>
        /// Returns the filter parameters in display format
        /// </summary>
        /// <returns></returns>
        public string GetFilterDisplay()
        {
			return (GetFilterFromModel() as CustomReportFilter).ParameterListDisplay(CustomReportFilter.DisplayFormat.Html);
        }
        #endregion
    }
}