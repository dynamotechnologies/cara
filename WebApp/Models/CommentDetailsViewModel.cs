﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a comment details view Model
    /// </summary>
    public class CommentDetailsViewModel : AbstractViewModel
    {
        #region Properties
        public Comment Comment { get; set; }
        #endregion
    }
}