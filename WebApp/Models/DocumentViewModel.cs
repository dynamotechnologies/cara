﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;

using Aquilent.Framework.Documents;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a document view Model
    /// </summary>
    public class DocumentViewModel
    {
        #region Properties
        public ICollection<Document> Documents { get; set; }
        public bool ShowButtons { get; set; }
        public bool ShowDeleteButton { get; set; }
        public bool ShowAddContentButton { get; set; }
        public bool HideDownloadAllButton { get; set; }
        public int LetterId { get; set; }
        public bool ShowActions { get; set; }

        /// <summary>
        /// Returns a comma separated list of document ids
        /// </summary>
        public string DocumentIds
        {
            get
            {
                var docIds = string.Empty;

                if (Documents != null && Documents.Count > 0)
                {
                    docIds = string.Join(",", (from d in Documents select d.DocumentId).Distinct().ToArray());
                }

                return docIds;
            }
        }


        /// <summary>
        /// Determines whether the Attachment can be deleted
        /// </summary>
        public bool Deletable(int docId)
        {
            bool isProtected;
            bool isLocked;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                var letter = letterManager.Get(LetterId);
                var letterdoc = letter.LetterDocuments.Where(ld => ld.DocumentId == docId).FirstOrDefault();
                isProtected = letterdoc.Protected.HasValue && letterdoc.Protected.Value;
                isLocked = letter.Phase.Locked;
            }

            bool canDeleteLetters = Utilities.UserHasPrivilege("DLTR", Constants.PHASE);
            bool canDeleteProtectedLetters = Utilities.UserHasPrivilege("DPL", Constants.SYSTEM);

            return !isLocked && ((!isProtected && canDeleteLetters) || (isProtected && canDeleteProtectedLetters));
        }
        #endregion
    }
}