﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a change log listing view Model
    /// </summary>
    public class ChangeLogListViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<FormManagementChangeLog> ChangeLogs { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public bool CanEdit { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Method to load the filter properties to the associated model properties
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as FormManagementChangeLogFilter;

                PhaseId = filter.PhaseId;
            }
        }

        /// <summary>
        /// Method to create the filter object from the associated model properties
        /// </summary>
        /// <returns></returns>
        public override IFilter GetFilterFromModel()
        {
            return new FormManagementChangeLogFilter { PhaseId = PhaseId, PageNum = 1 };
        }
        #endregion
    }
}