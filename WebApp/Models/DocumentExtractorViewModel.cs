﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a document extractor view Model
    /// </summary>
    public class DocumentExtractorViewModel : AbstractViewModel
    {
        #region Properties
        private Document document = null;

        public int LetterId { get; set; }
        public int DocumentId { get; set; }
        public string ExtractedText { get; set; }

        /// <summary>
        /// Get the document object
        /// </summary>
        public Document Document
        {
            get
            {
                if (document == null && DocumentId > 0)
                {
                    // get the document from database
                    using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        document = docManager.Get(DocumentId);
                    }
                }

                return document;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get the extracted text from the model document
        /// </summary>
        public void GetDocumentText()
        {
            var text = string.Empty;

            if (DocumentId > 0 && Document != null)
            {
                // call the text extractor extension from the document
                text = Document.ExtractText();
            }

            ExtractedText = text;
        }

        /// <summary>
        /// Add the extracted text to the letter
        /// </summary>
        /// <returns></returns>
        public bool AddTextToLetter(ModelStateDictionary modelState)
        {
            var result = false;

            if (LetterId > 0 && DocumentId > 0)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(ExtractedText))
                    {
                        // get the letter
                        using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            var letter = letterManager.Get(LetterId);

                            if (letter != null)
                            {
                                // append new text
                                letter.Text += "\r\n\r\n" + ExtractedText;

                                // append the new coded text
                                letter.CodedText += "\r\n\r\n" + letterManager.TagAutoMarkupTerms(letter, ExtractedText);

                                // set the letter status to be "in-progress"
                                if (letter.LetterStatusId != 4 /* Not Needed */)
                                {
                                    letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().Where(x => x.Name == Utilities.GetResourceString("InProgress")).FirstOrDefault().LetterStatusId;
                                }

                                letter.LastUpdatedBy = Utilities.GetUserSession().Username;
                                letter.LastUpdated = DateTime.UtcNow;

                                // attach the object
                                letterManager.UnitOfWork.Context.ObjectStateManager.ChangeObjectState(letter, System.Data.EntityState.Modified);

                                // save the letter
                                letterManager.UnitOfWork.Save();

                                result = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string errMsg = "An error occurred appending the extracted text to the letter.";

                    modelState.AddModelError(string.Empty, errMsg);
                    LogError(this.GetType(), errMsg, ex);
                }
            }

            return result;
        }
        #endregion
    }
}