﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Framework.Security;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using System.Web.Mvc;

namespace Aquilent.Cara.WebApp.Models
{
    public class UserRolesViewModel : AbstractViewModel
    {
        #region Lookups
        private LookupRole lookupRole = LookupManager.GetLookup<LookupRole>();
        private LookupUnit lookupUnit = LookupManager.GetLookup<LookupUnit>();
        #endregion Lookups

        #region Properties
        public User CurrentUser { get; set; }
        public User UserToManage { get; set; }
        public IEnumerable<UserSystemRole> Roles { get; set; }

        public int RoleId { get; set; }
        public string UnitId { get; set; }
        public string UnitName { get; set; }
        #endregion Properties

        #region Methods
        public string GetRoleName(int roleId)
        {
            return lookupRole[roleId].Name;
        }

        public string GetUnitName(string unitId)
        {
            return string.IsNullOrEmpty(unitId) || unitId == "0000" ? string.Empty : lookupUnit[unitId].Name;
        }

        public bool CanRemoveRole(string unitId, int roleId)
        {
            bool canRemove = false;
            if (roleId == 1)
            {
                canRemove = CurrentUser.HasSystemPrivilege("MSU");
            }
            else
            {
                canRemove = CurrentUser.HasSystemPrivilege("MUC") || CurrentUser.HasUnitPrivilege("MUC", unitId);
            }

            return canRemove;
        }

        public IEnumerable<SelectListItem> RolesToAssign
        {
            get
            {
                Func<KeyValuePair<int, string>, bool> query;
                if (CurrentUser.HasSystemPrivilege("MSU"))
                {
                    query = new Func<KeyValuePair<int, string>, bool>(kvp => kvp.Key == 1 || kvp.Key == 2);
                }
                else
                {
                    query = new Func<KeyValuePair<int, string>, bool>(kvp => kvp.Key == 2);
                }

                return Utilities.ToSelectList(lookupRole.SelectionList.Where(query).ToList(), string.Empty, true);
            }
        }

        public IEnumerable<Unit> Units
        {
            get
            {
                IEnumerable<Unit> lookup = null;
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();
                if (CurrentUser.HasSystemPrivilege("MSU"))
                {
                    lookup = lookupUnit.Where(unit => unit.IsRegion);
                }
                else
                {
                    // Restrict unit selection by membership ordered by UnitId
                    // Units stored in the user are padded by 0s to make each unitId 8 characters long
                    var units = CurrentUser.UnitPrivileges.Where(p => p.Key.Trim().Equals("MSU") || p.Key.Trim().Equals("MUC")).SelectMany(x => x.Value).Distinct().ToList();


                    // Remove any units in the list that already have a parent
                    string tempUnit = "TEMPUNIT";
                    string trimmedUnit;
                    var relevantUnits = new List<string>();
                    foreach (var unitId in units)
                    {
                        if (unitId.EndsWith("0000"))
                        {
                            trimmedUnit = unitId.Substring(0, unitId.Length - 4);
                        }
                        else if (unitId.EndsWith("00"))
                        {
                            trimmedUnit = unitId.Substring(0, unitId.Length - 2);
                        }
                        else
                        {
                            trimmedUnit = unitId;
                        }

                        if (!trimmedUnit.StartsWith(tempUnit))
                        {
                            relevantUnits.Add(trimmedUnit);
                        }

                        tempUnit = trimmedUnit;
                    }

                    lookup = lookupUnit.Where(unit => relevantUnits.Contains(unit.UnitId));
                }

                return lookup;
            }
        }
        #endregion Methods
    }
}