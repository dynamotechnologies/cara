﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using System.Web.Routing;
using Aquilent.EntityAccess;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a phase details view Model
    /// </summary>
    public class PhaseDetailsViewModel : AbstractViewModel
    {
        #region Init
        private static DateTime DownloadAllLettersDate;
        static PhaseDetailsViewModel()
        {
            string downloadAllLettersDateString = ConfigurationManager.AppSettings["DownloadAllLettersDate"];
            bool success = false;
            try
            {
                success = DateTime.TryParse(downloadAllLettersDateString, out DownloadAllLettersDate);
            }
            finally
            {
                if (!success)
                {
                    DownloadAllLettersDate = new DateTime(2014, 2, 8);
                }
            }
        }
        #endregion

        #region Properties
        public Phase Phase { get; set; }
        public PhaseSummary PhaseSummary { get; set; }
        public string ProjectNumber { get; set; }
		public RequestContext RequestContext { get; set; }
		public IList<RollUpInfo> LetterStatus
		{
			get
			{
				return GetLetterStatusRollUp();
			}
		}

        //CARA-717: New list to return Early Action Status
        public IList<RollUpInfo> EarlyActionStatus
        {
            get
            {
                return GetEarlyActionStatusRollup();
            }
        }

		public IList<RollUpInfo> LetterType
		{
			get
			{
				return GetLetterTypeRollUp();
			}
		}

		public IList<RollUpInfo> CommentCodeCategories
		{
			get
			{
				return GetCommentCodeCategoryRollUp();
			}
		}

		public IList<RollUpInfo> ConcernResponseStatus
		{
			get
			{
				return GetConcernResponseStatusRollUp();
			}
		}

		public bool CanDelete
        {
            get
            {
                int letterCount = 0;
                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    letterCount = letterManager.All.Count(ltr => ltr.PhaseId == Phase.PhaseId);
                }

                return (Phase != null ? (letterCount == 0 && !Phase.Locked) : false);
            }
        }

        public bool CanDownloadLetters
        {
            get
            {
                return Phase.CommentStart >= DownloadAllLettersDate;
            }
        }

        public bool IsObjection
        {
            get
            {
                return Phase != null && Phase.PhaseTypeId == 3;
            }
        }

        public string NewObjectionResponseDue
        {
            get
            {
                string newDateString = string.Empty;
                DateTime newDate;
                if (IsObjection && Phase.ObjectionResponseDue.HasValue)
                {
                    if (Phase.ObjectionResponseExtendedBy == null)
                    {
                        Phase.ObjectionResponseExtendedBy = 0;
                    }

                    else if (Phase.ObjectionResponseExtendedBy > 0)
                    {
                        var project = Phase.Project; 
                        var objectionDueDays = 0; 

                        switch (project.CommentRuleId)
                        {
                            case 13: /* 219 (2012) */
                                objectionDueDays = 90;
                                break;
                            case 14: /* 218 (2013) HFRA */
                                objectionDueDays = 30;
                                break;
                            case 15: /* 218 (2013) Non-HFRA */
                                objectionDueDays = 45;
                                break;
                            default:
                                objectionDueDays = 30;
                                break;
                        }

                        var objectionWeekendCheck = Phase.CommentEnd.AddDays(objectionDueDays).DayOfWeek;
                        
                        if (objectionWeekendCheck == DayOfWeek.Sunday)
                        {
                            Phase.ObjectionResponseExtendedBy = Phase.ObjectionResponseExtendedBy - 1;
                        }

                        else if (objectionWeekendCheck == DayOfWeek.Saturday)
                        {
                            Phase.ObjectionResponseExtendedBy = Phase.ObjectionResponseExtendedBy - 2;
                        }
                    }

                    newDate = Phase.ObjectionResponseDue.Value.AddDays(Phase.ObjectionResponseExtendedBy.Value);
                    newDateString = newDate.ToString("MM/dd/yyyy (ddd)");
                }

                return newDateString;
            }
        }

        TimeZoneInfo phaseTimeZone;
        string existingTimeZone = string.Empty;
        string configTimeZone = string.Empty;
        string displayTimeZone = string.Empty;
        public string PhaseTimeZone
        {
            get
            {
                if (Phase != null)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(Phase.TimeZone))
                        {
                            try
                            {
                                configTimeZone = ConfigurationManager.AppSettings["CommentPeriodTimeZoneEnd"];
                            }
                            catch (ConfigurationErrorsException cee)
                            {
                                configTimeZone = "UTC-11"; //key for the default time zone in case config can't be read
                            }
                            existingTimeZone = configTimeZone;
                        }
                        else
                        {
                            existingTimeZone = Phase.TimeZone;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = "An error occurred while retrieving the phase time zone";
                        LogError(this.GetType(), message, ex);
                    }
                }
                phaseTimeZone = TimeZoneInfo.FindSystemTimeZoneById(existingTimeZone);
                displayTimeZone = phaseTimeZone.DisplayName;
                return displayTimeZone;
            }
        }
        #endregion

        private IList<RollUpInfo> GetLetterStatusRollUp()
		{
			if (PhaseSummary == null)
			{
				throw new ArgumentNullException();
			}

			UrlHelper url = new UrlHelper(RequestContext);

			IList<RollUpInfo> infoList = new List<RollUpInfo>();

			// New Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = Constants.NEW,
					Count = PhaseSummary.NewLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter="New" })
				}
			);

			// Coding In Progress Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Coding In Progress",
					Count = PhaseSummary.CodingInProgressLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "CodingInProgress" })
				}
			);

			// Coding Complete Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Coding Complete",
					Count = PhaseSummary.CodingCompleteLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "CodingComplete" })
				}
			);

			return infoList;
		}

        private IList<RollUpInfo> GetEarlyActionStatusRollup()
        {
            if (PhaseSummary == null)
            {
                throw new ArgumentNullException();
            }

            UrlHelper url = new UrlHelper(RequestContext);
            
            IList<RollUpInfo> infoList = new List<RollUpInfo>();

            // Action Status: Not Required
            infoList.Add(
                new RollUpInfo
                {
                    Label = "Not Required ",
                    Count = PhaseSummary.Action_NotRequired,
                    ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "NotRequired" })
                }
            );

            // Action Status: Needs Review
            infoList.Add(
                new RollUpInfo
                {
                    Label = "Needs Review ",
                    Count = PhaseSummary.Action_NeedsReview,
                    ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "NeedsReview" })
                }
            );

            // Action Status: Completed
            infoList.Add(
                new RollUpInfo
                {
                    Label = "Completed ",
                    Count = PhaseSummary.Action_Completed,
                    ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Completed" })
                }
            );

            // Action Status: Coded
            infoList.Add(
                new RollUpInfo
                {
                    Label = "Coded ",
                    Count = PhaseSummary.Action_Coded,
                    ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Coded" })
                }
            );

            return infoList;
        }
        private IList<RollUpInfo> GetLetterTypeRollUp()
		{
			if (PhaseSummary == null)
			{
				throw new ArgumentNullException();
			}

			UrlHelper url = new UrlHelper(RequestContext);

			IList<RollUpInfo> infoList = new List<RollUpInfo>();

			// Unique Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Unique " + Utilities.GetResourceString(Constants.LETTER).Pluralize(),
					Count = PhaseSummary.UniqueLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Unique" })
				}
			);

			// Form Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Form " + Utilities.GetResourceString(Constants.LETTER).Pluralize(),
					Count = PhaseSummary.FormLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Form" })
				}
			);

			// Master Form Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Master Form " + Utilities.GetResourceString(Constants.LETTER).Pluralize(),
					Count = PhaseSummary.MasterFormLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "MasterForm" })
				}
			);

			// Form Plus Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Form Plus " + Utilities.GetResourceString(Constants.LETTER).Pluralize(),
					Count = PhaseSummary.FormPlusLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "FormPlus" })
				}
			);

			// Duplicate Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Duplicate " + Utilities.GetResourceString(Constants.LETTER).Pluralize(),
					Count = PhaseSummary.DuplicateLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Duplicate" })
				}
			);

			// Total Letters
			infoList.Add(
				new RollUpInfo
				{
					Label = "Pending " + Utilities.GetResourceString(Constants.LETTER).Pluralize(),
					Count = PhaseSummary.PendingLetters,
					ActionLink = url.Action(Constants.LIST, Constants.LETTER, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Pending" })
				}
			);
			
			return infoList;
		}

		private IList<RollUpInfo> GetCommentCodeCategoryRollUp()
		{
			if (PhaseSummary == null)
			{
				throw new ArgumentNullException();
			}

			UrlHelper url = new UrlHelper(RequestContext);

			IList<RollUpInfo> infoList = new List<RollUpInfo>();

			if (PhaseSummary.HasIssueAction.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = Constants.ACTION,
						Count = PhaseSummary.IssueActionComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "IssueAction", mode = Constants.CREATE })
					}
				);
			}

			if (PhaseSummary.HasRationale.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = "Rationale",
						Count = PhaseSummary.RationaleComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Rationale", mode = Constants.CREATE })
					}
				);
			}

			if (PhaseSummary.HasLocation.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = "Location",
						Count = PhaseSummary.LocationComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Location", mode = Constants.CREATE })
					}
				);
			}

			if (PhaseSummary.HasDocument.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = Utilities.GetResourceString(Constants.DOCUMENT),
						Count = PhaseSummary.DocumentComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Document", mode = Constants.CREATE })
					}
				);
			}

            if (PhaseSummary.HasEarlyAttention.Value)
            {
                infoList.Add(
                    new RollUpInfo
                    {
                        Label = Utilities.GetResourceString("EarlyAction"),
                        Count = PhaseSummary.EarlyAttentionComments,
                        ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "EarlyAttention", mode = Constants.CREATE })
                    }
                );
            }

			if (PhaseSummary.HasOther.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = "Other",
						Count = PhaseSummary.OtherComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Other", mode = Constants.CREATE })
					}
				);
			}

			if (PhaseSummary.HasObjection.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = "Objection",
						Count = PhaseSummary.ObjectionComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "Objection", mode = Constants.CREATE })
					}
				);
			}

            if (PhaseSummary.HasLegalAppeals.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = "Appeals (Legal)",
						Count = PhaseSummary.LegalAppealsComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "LegalAppeals", mode = Constants.CREATE })
					}
				);
			}

			if (PhaseSummary.HasResourceAppeals.Value)
			{
				infoList.Add(
					new RollUpInfo
					{
						Label = "Appeals (Resource)",
						Count = PhaseSummary.ResourceAppealsComments,
						ActionLink = url.Action(Constants.LIST, Constants.COMMENT, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "ResourceAppeals", mode = Constants.CREATE })
					}
				);
			}

			return infoList;
		}

		private IList<RollUpInfo> GetConcernResponseStatusRollUp()
		{
			if (PhaseSummary == null)
			{
				throw new ArgumentNullException();
			}

			UrlHelper url = new UrlHelper(RequestContext);

			IList<RollUpInfo> infoList = new List<RollUpInfo>();

			infoList.Add(
				new RollUpInfo
				{
					Label = Utilities.GetResourceString("InProgress"),
					Count = PhaseSummary.InProgressConcerns,
					ActionLink = url.Action(Constants.LIST, Constants.CONCERN, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter="InProgressConcerns" })
				}
			);

			infoList.Add(
				new RollUpInfo
				{
					Label = "Ready for Response",
					Count = PhaseSummary.ReadyResponses,
					ActionLink = url.Action(Constants.LIST, Constants.CONCERN, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter="ReadyResponses" })
				}
			);

			infoList.Add(
				new RollUpInfo
				{
					Label = "Response in Progress",
					Count = PhaseSummary.InProgressResponses,
					ActionLink = url.Action(Constants.LIST, Constants.CONCERN, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "InProgressResponses" })
				}
			);

			infoList.Add(
				new RollUpInfo
				{
					Label = "Response Complete",
					Count = PhaseSummary.CompleteResponses,
					ActionLink = url.Action(Constants.LIST, Constants.CONCERN, new { projectId = Phase.ProjectId, phaseId = Phase.PhaseId, filter = "CompleteResponses" })
				}
			);

			return infoList;
		}

		public class RollUpInfo
		{
			public string Label { get; set; }
			public string ActionLink { get; set; }
			public int Count { get; set; }
		}
    }
}