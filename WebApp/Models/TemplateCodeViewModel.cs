﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    public class TemplateCodeViewModel : AbstractViewModel
    {
        #region Properties
        public int CodingTemplateId { get; set; }
        public string TemplateName { get; set; }
        public ICollection<TemplateCodeSelect> TemplateCodes { get; set; }
        public bool Selection { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the TemplateCodes in a tree view structure
        /// </summary>
        public ICollection<TemplateCodeSelect> TemplateCodeTree
        {
            get
            {
                // tree structure to be returned
                return CodeUtilities.ToTemplateCodeTreeByType(TemplateCodes);
            }
        }
        #endregion
    }
}