﻿using System;
using System.Linq;
using System.Collections.Generic;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Configuration.Dmd;
using Aquilent.Cara.Configuration;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a public letter view Model
    /// </summary>
    public class PublicLetterViewModel : LetterViewModel
    {
        #region Private Properties
        private ICollection<FileMetaData> fileMetaDataList;
		private DmdServiceConfiguration _dmdConfig = ConfigUtilities.GetSection<DmdServiceConfiguration>("dmdServiceConfiguration");
        #endregion

        #region Properties
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Returns a Letter object by Letter id in the project
        /// </summary>
        private Letter _letter = null;
        public override Letter Letter
        {
            get
            {
                if (LetterId > 0 && _letter == null)
                {
                    // get the letter
                    using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                    {
                        _letter = letterManager.Get(LetterId);
                        // load for the view
                        var project = _letter.Phase.Project; 
                        var organization = _letter.Sender.Organization;
                        var documents = _letter.Documents;
                    }
                }

                // safe guard to ensure the letter is accessible with correct letter id and project number
                if (_letter != null && (!_letter.Phase.ReadingRoomActive || !ProjectNumber.Equals(_letter.Phase.Project.ProjectNumber)))
                {
                    _letter = null;
                }

                return _letter;
            }
        }

        /// <summary>
        /// Returns a collection of Letter Attachments as DMD file meta data
        /// </summary>
        public ICollection<FileMetaData> DmdFileMetaDataList
        {
            get
            {
                if (fileMetaDataList == null)
                {
                    try
                    {
                        if (Letter != null)
                        {
                            DmdManager dmdManager = new DmdManager();
                            fileMetaDataList = new List<FileMetaData>();

                            // get letter attachment dmd ids                            
                            var dmdIds = LetterService.GetDmdIds(Letter);

                            // loop thru each dmd file
                            foreach (var dmdId in dmdIds)
                            {
                                // get the file meta data
                                var fileMetaData = dmdManager.GetFileMetadata(dmdId);
                                // set dmd id as DMD does not include this as a return property
                                fileMetaData.DmdId = dmdId;

                                fileMetaDataList.Add(fileMetaData);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(this.GetType(),
                            string.Format("An error occurred while getting file meta data for the {0} attachments.", Utilities.GetResourceString(Constants.LETTER)),
                            ex);
                    }
                }

                return fileMetaDataList;
            }
        }

		public string DmdFilenameForDisplay(string dmdFileName)
		{
			string filename = dmdFileName;
			var letterFilename = _dmdConfig.LetterDefaults.Filename;
			if (dmdFileName == letterFilename)
			{
				filename = "LetterText.pdf";
			}

			return filename;
		}
        #endregion
    }
}