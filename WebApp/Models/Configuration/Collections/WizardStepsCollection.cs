﻿using System;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    /// <summary>
    /// Collection class for the wizard step configuration
    /// </summary>
    public class WizardStepsCollection : ConfigurationElementCollection
    {
        public WizardStepsCollection()
        {
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new WizardStepElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return base.CreateNewElement(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((WizardStepElement)element).Name;
        }

        public WizardStepElement this[int index]
        {
            get
            {
                return (WizardStepElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public int IndexOf(WizardStepElement fqce)
        {
            return BaseIndexOf(fqce);
        }

        public void Add(WizardStepElement fqce)
        {
            BaseAdd(fqce);
            // Add custom code here.
        }

        public void Remove(WizardStepElement fqce)
        {
            if (BaseIndexOf(fqce) >= 0)
            {
                BaseRemove(fqce.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
            // Add custom code here.
        }
    }
}