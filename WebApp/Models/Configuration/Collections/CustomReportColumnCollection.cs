﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    public class CustomReportColumnCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomReportColumnElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((CustomReportColumnElement)element).Id;
        }

        public CustomReportColumnElement this[int index]
        {
            get
            {
                return (CustomReportColumnElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }
        public int IndexOf(CustomReportColumnElement fqce)
        {
            return BaseIndexOf(fqce);
        }

        public void Add(CustomReportColumnElement fqce)
        {
            BaseAdd(fqce);
            // Add custom code here.
        }

        public void Remove(CustomReportColumnElement fqce)
        {
            if (BaseIndexOf(fqce) >= 0)
            {
                BaseRemove(fqce.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
            // Add custom code here.
        }
    }
}