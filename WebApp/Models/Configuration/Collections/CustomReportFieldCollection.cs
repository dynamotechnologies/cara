﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    public class CustomReportFieldCollection : ConfigurationElementCollection
    {
        public CustomReportFieldCollection()
        {
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomReportFieldElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((CustomReportFieldElement)element).Name;
        }

        public CustomReportFieldElement this[int index]
        {
            get
            {
                return (CustomReportFieldElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public int IndexOf(CustomReportFieldElement fqce)
        {
            return BaseIndexOf(fqce);
        }

        public void Add(CustomReportFieldElement fqce)
        {
            BaseAdd(fqce);
            // Add custom code here.
        }

        public void Remove(CustomReportFieldElement fqce)
        {
            if (BaseIndexOf(fqce) >= 0)
            {
                BaseRemove(fqce.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
            // Add custom code here.
        }
    }
}