﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    /// <summary>
    /// configuration class for the CustomReportFields
    /// </summary>
    public class CustomReportFieldConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("fields", IsDefaultCollection = false)]
        public CustomReportFieldCollection CustomReportfields
        {
            get
            {
                return (CustomReportFieldCollection)base["fields"];
            }
        }
        [ConfigurationProperty("columns", IsDefaultCollection = false)]
        public CustomReportColumnCollection CustomReportColumns
        {
            get
            {
                return (CustomReportColumnCollection)base["columns"];
            }
        }
    }
}