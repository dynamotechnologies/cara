﻿using System;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    /// <summary>
    /// Element class for a wizard step configuration
    /// </summary>
    public class WizardStepElement : ConfigurationElement, IEquatable<WizardStepElement>
    {
        #region Public Property
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("action", IsRequired = true)]
        public string Action
        {
            get
            {
                return (string)this["action"];
            }
            set
            {
                this["action"] = value;
            }
        }

        [ConfigurationProperty("controller", IsRequired = true)]
        public string Controller
        {
            get
            {
                return (string)this["controller"];
            }
            set
            {
                this["controller"] = value;
            }
        }

        [ConfigurationProperty("description", IsRequired = true)]
        public string Description
        {
            get
            {
                return (string)this["description"];
            }
            set
            {
                this["description"] = value;
            }
        }

        [ConfigurationProperty("mode", IsRequired = true)]
        public string Mode
        {
            get
            {
                return (string)this["mode"];
            }
            set
            {
                this["mode"] = value;
            }
        }

        public string[] Modes
        {
            get
            {
                return Mode.Split(',');
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Implements Equals method
        /// </summary>
        /// <param name="ws"></param>
        /// <returns></returns>
        bool IEquatable<WizardStepElement>.Equals(WizardStepElement ws)
        {
            return (this.Name.Equals(ws.Name));
        }
        #endregion
    }
}