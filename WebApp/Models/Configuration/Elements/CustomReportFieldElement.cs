﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    public class CustomReportFieldElement : ConfigurationElement, IEquatable<CustomReportFieldElement>
    {
        #region Public Property
        [ConfigurationProperty("id", IsRequired = true)]
        public int Id
        {
            get
            {
                return (int)this["id"];
            }
            set
            {
                this["id"] = value;
            }
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("fieldType", IsRequired = true)]
        public string FieldType
        {
            get
            {
                return (string)this["fieldType"];
            }
            set
            {
                this["fieldType"] = value;
            }
        }

        [ConfigurationProperty("dataType", IsRequired = true)]
        public string DataType
        {
            get
            {
                return (string)this["dataType"];
            }
            set
            {
                this["dataType"] = value;
            }
        }

        public string ToolTip
        {
            get
            {
                switch (Id)
                {
                    case 15: //letter number
                    case 17: //comment code number
                        return "Seperate values with commas and use hyphens to denote ranges. ex: '101, 110-125'";
                    default:
                        return "Separate values with commas to input multiple values at once, 'AND' and 'OR' logical operators can also be used. ANDed values will appear as separate nodes in the Custom Report Definition.";
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Implements Equals method
        /// </summary>
        /// <param name="hde"></param>
        /// <returns></returns>
        bool IEquatable<CustomReportFieldElement>.Equals(CustomReportFieldElement hde)
        {
            return (this.Id.Equals(hde.Id));
        }
        #endregion
    }
}