﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    public class CustomReportColumnElement : ConfigurationElement, IEquatable<CustomReportFieldElement>
    {
        #region Public Property
        [ConfigurationProperty("id", IsRequired = true)]
        public int Id
        {
            get
            {
                return (int)this["id"];
            }
            set
            {
                this["id"] = value;
            }
        }


        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("groupId", IsRequired = true)]
        public int GroupId
        {
            get
            {
                return (int)this["groupId"];
            }
            set
            {
                this["groupId"] = value;
            }
        }

        #endregion
        #region Public Methods
        /// <summary>
        /// Implements Equals method
        /// </summary>
        /// <param name="hde"></param>
        /// <returns></returns>
        bool IEquatable<CustomReportFieldElement>.Equals(CustomReportFieldElement hde)
        {
            return (this.Id.Equals(hde.Id));
        }
        #endregion

    }
}