﻿using System.Configuration;

namespace Aquilent.Cara.WebApp.Models.Configuration
{
    /// <summary>
    /// configuration class for the wizard steps
    /// </summary>
    public class WizardStepConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("steps", IsDefaultCollection = false)]
        public WizardStepsCollection Steps
        {
            get
            {
                return (WizardStepsCollection)base["steps"];
            }
        }
    }
}