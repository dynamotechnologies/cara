﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain.Report.Personal;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a report view Model
    /// </summary>
    public class ReportViewModel : AbstractViewModel
    {
        #region Properties
        public StndReportViewModel StndReportViewModel { get; set; }
        public CustomReportListViewModel CustomReportListViewModel { get; set; }
        public ICollection<MyReport> MyReports
        {
            get
            {
                IList<MyReport> myReports;
                using (var myReportManager = ManagerFactory.CreateInstance<MyReportManager>())
                {
                    int userId = Utilities.GetUserSession().UserId;
                    myReports = myReportManager.All.Where(x => x.UserId == userId).ToList();
                }

                return myReports;
            }
        }

        /// <summary>
        /// Returns a list of my report list options
        /// </summary>
        public SelectList MyReportListOptions
        {
            get
            {
                return new SelectList(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("All", string.Format("{0} & Shared {1}", Utilities.GetResourceString("MyReports"), Utilities.GetResourceString(Constants.REPORT).Pluralize())),
                        new KeyValuePair<string, string>("MyReport", string.Format("{0} Only", Utilities.GetResourceString("MyReports"))),
                        new KeyValuePair<string, string>("PublicReport", string.Format("Shared {0} Only", Utilities.GetResourceString(Constants.REPORT).Pluralize())),
                    }, "Key", "Value", "MyReport"
                );
            }
        }

        public string SelectedTab { get; set; }
        #endregion
    }
}
