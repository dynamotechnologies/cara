﻿using Aquilent.Cara.Domain;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    public class ProjectNavModel
    {
        public Project Project { get; set; }
        public Phase CurrentPhase;
        public User CurrentUser;
        public string UnitId
        {
            get
            {
                return Project.UnitId;
            }
        }
    }
}