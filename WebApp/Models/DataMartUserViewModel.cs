﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework.Security;
using Aquilent.Framework;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a data mart user view Model
    /// </summary>
    public class DataMartUserViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<User> Users { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string Username { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        public void Validate(ModelStateDictionary modelState)
        {
            if (string.IsNullOrEmpty(UserLastName) && string.IsNullOrEmpty(UserFirstName))
            {
                modelState.AddModelError(string.Empty, "User search criteria is required.");
            }
        }

        /// <summary>
        /// Implements the load filter method
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as DataMartUserFilter;
                
                UserLastName = filter.LastName;
                UserFirstName = filter.FirstName;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new DataMartUserFilter();

            filter.LastName = UserLastName;
            filter.FirstName = UserFirstName;

            return filter;
        }

        /// <summary>
        /// Returns a list of records from the list view model
        /// </summary>
        /// <param name="filter">DataMart User Filter (Use model properties if not provided)</param>
        /// <returns></returns>
        public DataMartUserSearchResults GetListRecords(ModelStateDictionary modelState, DataMartUserFilter filter = null)
        {
            var results = new DataMartUserSearchResults();

            // set up the default filter from the model if not provided in the parameter
            if (filter == null)
            {
                filter = GetFilterFromModel() as DataMartUserFilter;

                // set filter paging info
                if (filter != null)
                {
                    filter.StartRow = 0;
                    filter.NumRows = Constants.DEFAULT_PAGE_SIZE;
                };
            }

            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.UserManager = userManager;

                // search for Users based on filtering
                try
                {
                    results = dmManager.FindUsers(filter);

                    if (results != null)
                    {
                        Users = results.Users.ToList();
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("An error occurred finding the Users from {0}", Utilities.GetResourceString("PALS"));
                    LogError(this.GetType(), message, ex);

                    modelState.AddModelError(string.Empty, message);
                }

                dmManager.Dispose();
            }

            return results;
        }
        #endregion
    }
}