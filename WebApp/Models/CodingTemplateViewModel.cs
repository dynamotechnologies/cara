﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a coding template view Model
    /// </summary>
    public class CodingTemplateViewModel : AbstractWizardModel
    {
        #region Properties
        public ICollection<User> Users { get; set; }
        public ICollection<Project> Projects { get; set; }
        public string ProjectNameOrId { get; set; }
        public string CodingTemplateId { get; set; }
        public string ProjectId { get; set; }
        public string PhaseId { get; set; }
        public string Type { get; set; }

        /// <summary>
        /// Returns a list of coding templates
        /// </summary>
        public ICollection<CodingTemplate> CodingTemplates
        {
            get
            {
                IList<CodingTemplate> list;
                using (var codingTemplateManager = ManagerFactory.CreateInstance<CodingTemplateManager>())
                {
                    list = codingTemplateManager.All.Where(x => x.Name != "Moderate").ToList();
                }

                return list;
            }
        }
        #endregion
    }
}