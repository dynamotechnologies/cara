﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using System.Web.Routing;
using Aquilent.EntityAccess;
using System.Configuration;

namespace Aquilent.Cara.WebApp.Models
{
    public class ConcernTemplateInputViewModel : AbstractWizardModel
    {
        #region properties
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public string ActionString { get; set; }

        [DataType(DataType.MultilineText)]
        public string ConcernText{ get; set; }

        [DataType(DataType.MultilineText)]
        public string ResponseText { get; set; }
        #endregion properties


        /// <summary>
        /// Validate the model
        /// </summary>
        /// <param name="modelState"></param>
        public void Validate(ModelStateDictionary modelState)
        {
        }

    }
}