﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework;
using System.Web.Mvc;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a letter listing view Model
    /// </summary>
    public class LetterListViewModel : AbstractListViewModel
    {
        #region Properties
        public ICollection<LetterSearchResult> Letters { get; set; }
        public int PhaseId { get; set; }
        public int? LetterSequence { get; set; }
        public int? LetterTypeId { get; set; }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }
        public int? LetterStatusId { get; set; }
        public string Keyword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool ViewAll { get; set; }
        public bool? WithinCommentPeriod { get; set; }
        public bool CanEdit { get; set; }
        public int? EarlyActionStatusId { get; set; }

        /// <summary>
        /// Returns a select list of letter types
        /// </summary>
        public IEnumerable<SelectListItem> LookupLetterType
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupLetterType>().SelectionList, string.Empty);
            }
        }

        /// <summary>
        /// Returns a select list of letter statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupLetterStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupLetterStatus>().SelectionList, string.Empty);
            }
        }


        /// <summary>
        /// Returns a select list of early action statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupEarlyActionStatus
        {
            get
            {
                return Utilities.ToSelectList(LookupManager.GetLookup<LookupEarlyActionStatus>().SelectionList, string.Empty, false);
            }
        }

        /// <summary>
        /// Returns a select list of comment period statuses
        /// </summary>
        public IEnumerable<SelectListItem> LookupCommentPeriodStatus
        {
            get
            {
                return new List<SelectListItem> {
                    new SelectListItem { Text = string.Empty, Value = null, Selected = !WithinCommentPeriod.HasValue },
                    new SelectListItem { Text = "Outside " + Utilities.GetResourceString(Constants.PHASE), Value = "False",
                        Selected = WithinCommentPeriod.HasValue && !WithinCommentPeriod.Value },
                    new SelectListItem { Text = "Within " + Utilities.GetResourceString(Constants.PHASE), Value = "True",
                        Selected = WithinCommentPeriod.HasValue && WithinCommentPeriod.Value }
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Implements the load filter method
        /// CARA-717: Modified the below methods to include EarlyActionStatus field - By Latha on 10/13/11
        /// </summary>
        /// <param name="filter"></param>
        public override void LoadFilterToModel(IFilter iFilter)
        {
            if (iFilter != null)
            {
                var filter = iFilter as LetterSearchResultFilter;

                PhaseId = filter.PhaseId;
                LetterTypeId = filter.LetterTypeId;
                DateReceivedFrom = filter.DateReceivedFrom;
                DateReceivedTo = filter.DateReceivedTo;
                LetterStatusId = filter.LetterStatusId;
                LetterSequence = filter.LetterSequence;
                Keyword = filter.Keyword;
                if (!string.IsNullOrEmpty(filter.FirstName))
                {
                    LastName = filter.LastName + ", " + filter.FirstName;
                }
                else
                {
                    LastName = filter.LastName;
                }
                FirstName = filter.FirstName;
                ViewAll = (filter.ViewAll.HasValue ? filter.ViewAll.Value : false);
                WithinCommentPeriod = filter.WithinCommentPeriod;
                EarlyActionStatusId = filter.EarlyActionStatusId;
            }
        }

        /// <summary>
        /// Implements the get filter method
        /// </summary>
        public override IFilter GetFilterFromModel()
        {
            var filter = new LetterSearchResultFilter();

            filter.PhaseId = PhaseId;
            filter.LetterTypeId = LetterTypeId;
            filter.DateReceivedFrom = DateReceivedFrom;
            filter.DateReceivedTo = DateReceivedTo.HasValue ? DateReceivedTo.Value.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999) : DateReceivedTo;

            if (DateReceivedFrom.HasValue || DateReceivedTo.HasValue)
            {
                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    var phase = phaseManager.Get(PhaseId);

                    if (!string.IsNullOrEmpty(phase.TimeZone))
                    {
                        var tzi = TimeZoneInfo.FindSystemTimeZoneById(phase.TimeZone);
                        filter.DateReceivedFrom = filter.DateReceivedFrom.HasValue ? TimeZoneInfo.ConvertTimeToUtc(filter.DateReceivedFrom.Value, tzi) : filter.DateReceivedFrom;
                        filter.DateReceivedTo = filter.DateReceivedTo.HasValue ? TimeZoneInfo.ConvertTimeToUtc(filter.DateReceivedTo.Value, tzi) : filter.DateReceivedTo;
                    }
                }
            }

            filter.LetterStatusId = LetterStatusId;
            filter.LetterSequence = LetterSequence;
            filter.Keyword = Keyword;
            filter.ViewAll = (ViewAll ? ViewAll : new Nullable<bool>());
            filter.WithinCommentPeriod = WithinCommentPeriod;
			filter.PageNum = 1;
            filter.EarlyActionStatusId = EarlyActionStatusId;

            if (!string.IsNullOrEmpty(LastName))
            {
                var submitterName = LastName.Split(',');

                filter.LastName = submitterName[0].Trim();
                if (submitterName.Length > 1)
                {
                    filter.FirstName = submitterName[1].Trim();
                }
            }
            else
            {
                filter.LastName = LastName;
                filter.FirstName = FirstName;
            }
                
            return filter;
        }
        #endregion
    }
}