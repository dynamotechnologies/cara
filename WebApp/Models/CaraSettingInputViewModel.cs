﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.WebApp.Models.Abstract;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a cara setting view Model
    /// </summary>
    public class CaraSettingInputViewModel : AbstractViewModel
    {
        #region Constants
        // max text length for Telerik.Editor Control.
        public const int MAX_TEXT_LENGTH = 1000;
        #endregion

        #region Properties
        public CaraSetting CaraSetting { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Validate text length for external form
        /// </summary>
        public virtual void ValidateText(ModelStateDictionary modelState)
        {
            if (!string.IsNullOrEmpty(CaraSetting.Value) && CaraSetting.Value.Length > MAX_TEXT_LENGTH)
            {
                modelState.AddModelError(string.Empty, string.Format("Welcome Message Error: The message text is limited to {0} characters. Your entry has exceeded this limit by '{1}' characters.", MAX_TEXT_LENGTH, CaraSetting.Value.Length - MAX_TEXT_LENGTH));
             }
        }
        #endregion
    }
}