﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;
using Aquilent.Cara.WebApp.Models.Configuration;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.Domain.Report.Personal;

namespace Aquilent.Cara.WebApp.Models
{
    public class CustomReportViewModel : CustomReportControlModel
    {
        #region properties
        public CustomReportMetaData MetaData;
        public int LastStep { get { return ReportWizardManager.Instance.GetWizard(PageMode.CustomReportWizard).Count - 1; } }
        public List<CustomReportData> ReportData { get; set; }


        //UserOwnsReport is true if the user that created the report is the active user
        //if the report is not a saved repot then reportId is negative and UserOwnsReport is false
        public bool UserOwnsReport
        {
            get
            {
                return ReportId > 0 ?
                    Utilities.GetUserSession().UserId == ManagerFactory.CreateInstance<MyReportManager>().Get(ReportId).UserId
                    : false;
            }
        }

        //ReportName is the name of the saved report
        //If the report has not been saved then ReportId is negative and the name is "New Report"
        public string ReportName
        {
            get
            {
                return ReportId > 0 ?
                    ManagerFactory.CreateInstance<MyReportManager>().Find(x => x.MyReportId == ReportId).FirstOrDefault().Name
                    : "New Report";
            }
        }
        #endregion

        #region Constructors and related methods
        public CustomReportViewModel()
        {
            SetDefaults();
        }

        /// <summary>
        /// Creates a view model from the control model
        /// </summary>
        /// <param name="model">the control model</param>
        public CustomReportViewModel(CustomReportControlModel model)
        {
            Action = model.Action;
            BreadCrumbTitle = model.BreadCrumbTitle;
            Description =  model.Description;
            FieldType = model.FieldType;
            PageTitle = model.PageTitle;
            WizardStep = model.WizardStep;
            Command = model.Command;
            ReportId = model.ReportId;
            Dirty = model.Dirty;
            CollapsedListIds = model.CollapsedListIds;
            FieldId = model.FieldId;
            ProjectFieldId = model.ProjectFieldId;
            LetterFieldId = model.LetterFieldId;
            CommenterFieldId = model.CommenterFieldId;
            FieldValue = model.FieldValue;
            FieldValueName = model.FieldValueName;
            FieldValueInclude = model.FieldValueInclude;
            FieldDateFrom = model.FieldDateFrom;
            FieldDateTo = model.FieldDateTo;
            ListId = model.ListId;
            ColumnGroup = model.ColumnGroup;
            SortColumn1 = model.SortColumn1;
            SortColumn2 = model.SortColumn2;
            SortColumn3 = model.SortColumn3;
            Asc1 = model.Asc1;
            Asc2 = model.Asc2;
            Asc3 = model.Asc3;
            SetDefaults();
        }

        /// <summary>
        /// Sets the default values of all properties if needed
        /// </summary>
        private void SetDefaults()
        {
            if (CollapsedListIds == null)
            {
                CollapsedListIds = String.Empty;
            }

            List<int> colListIds = CollapsedListIds
                .Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(x => Int32.Parse(x))
                .ToList();

            MetaData = new CustomReportMetaData(colListIds);
            CustomReportColumnDataItem column = null;
            if (MetaData.parameters.ColumnDataItems.Any(x => x.SortOrder == 1))
            {
                column = MetaData.parameters.ColumnDataItems.FirstOrDefault(x => x.SortOrder == 1);
                SortColumn1 = column.ListId;
                Asc1 = column.Ascending;
            }
            if (Asc1 == null)
            { Asc1 = true; }

            if (MetaData.parameters.ColumnDataItems.Any(x => x.SortOrder == 2))
            {
                column = MetaData.parameters.ColumnDataItems.FirstOrDefault(x => x.SortOrder == 2);
                SortColumn2 = column.ListId;
                Asc2 = column.Ascending;
            }
            if (Asc2 == null)
            { Asc2 = true; }

            if (MetaData.parameters.ColumnDataItems.Any(x => x.SortOrder == 3))
            {
                column = MetaData.parameters.ColumnDataItems.FirstOrDefault(x => x.SortOrder == 3);
                SortColumn3 = column.ListId;
                Asc3 = column.Ascending;
            }
            if (Asc3 == null)
            { Asc3 = true; }
            
            if (Dirty == null) { Dirty = false; }
            if (WizardStep == null) { WizardStep = 0; }
            
            if (Command == null || Command.PageSize == 0)
            { Command = new GridCommand { PageSize = Utilities.GetUserSession().SearchResultsPerPage };}

        }
        #endregion

        #region selectListItem List methods
        
        /// <summary>
        /// (depricated) Gets a List of fields (formated for display in a dropdown) for the current field type
        /// </summary>
        /// <returns>The list of fields</returns>
        public IEnumerable<SelectListItem> SelectListFields()
        {
            List<KeyValuePair<int, string>> si = new List<KeyValuePair<int, string>>();
            si.Add(new KeyValuePair<int, string>(0, ""));
            List<CustomReportFieldElement> fields = CustomReportFieldManager.GetList(FieldType).OrderBy(x => x.Name).ToList();

            fields.ForEach(element => si.Add(new KeyValuePair<int, string>(element.Id, element.Name)));

            return Utilities.ToSelectList(si, null, false);
        }

        /// <summary>
        /// Gets a List of fields (formated for display in a dropdown) for the specified field type
        /// </summary>
        /// <returns>The list of fields</returns>
        public IEnumerable<SelectListItem> SelectListFields(string customReportFieldType)
        {
            List<KeyValuePair<int, string>> si = new List<KeyValuePair<int, string>>();
            si.Add(new KeyValuePair<int, string>(0, ""));
            CustomReportFieldManager.GetList(customReportFieldType)
                .OrderBy(x => x.Name)
                .ToList()
                .ForEach(element => si.Add(new KeyValuePair<int, string>(element.Id, element.Name)));

            return Utilities.ToSelectList(si, null, false);
        }

        public IEnumerable<SelectListItem> SelectListFieldValues(string customReportFieldType)
        {
            List<KeyValuePair<string, string>> retVal = new List<KeyValuePair<string, string>>();
            retVal.Add(new KeyValuePair<string,string>("0",""));
            int choosenFieldId = 0;
            switch (customReportFieldType)
            {
                case CustomReportFieldType.Project:
                    choosenFieldId = ProjectFieldId;
                    break;
                case CustomReportFieldType.Letter:
                    choosenFieldId = LetterFieldId;
                    break;
                case CustomReportFieldType.Commenter:
                    choosenFieldId = CommenterFieldId;
                    break;
            }

            #region populate dropdown based on fieldId
            switch (choosenFieldId)
            {
                case 1://analysis type
                    LookupAnalysisType lat = LookupManager.GetLookup<LookupAnalysisType>();
                    lat.OrderBy(x => x.Name).ToList().ForEach(at =>
                        retVal.Add(new KeyValuePair<string, string>(at.AnalysisTypeId.ToString(), at.Name)));
                    break;

                case 2: //project activities
                    LookupActivity lact = LookupManager.GetLookup<LookupActivity>();
                    lact.OrderBy(x => x.Name).ToList().ForEach(act =>
                        retVal.Add(new KeyValuePair<string, string>(act.ActivityId.ToString(), act.Name)));
                    break;

                case 3: //project purpose
                    LookupPurpose lpur = LookupManager.GetLookup<LookupPurpose>();
                    lpur.OrderBy(x => x.Name).ToList().ForEach(pur =>
                        retVal.Add(new KeyValuePair<string, string>(pur.PurposeId.ToString(), pur.Name)));
                    break;

                case 7: //state
                    LookupState lsta = LookupManager.GetLookup<LookupState>();
                    lsta.OrderBy(x => x.Name).ToList().ForEach(sta =>
                        retVal.Add(new KeyValuePair<string, string>(sta.StateId, sta.Name)));
                    break;

                case 9: //country
                    LookupCountry lcountry = LookupManager.GetLookup<LookupCountry>();
                    lcountry.OrderBy(x => x.Name).ToList().ForEach(country =>
                        retVal.Add(new KeyValuePair<string, string>(country.CountryId.ToString(), country.Name)));
                    break;

                case 10: //organization
                    using (OrganizationManager orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                    {
                        orgManager.GetSelectList().ToList().ForEach(org =>
                            retVal.Add(new KeyValuePair<string, string>(org.Key.ToString(), org.Value)));
                    }
                    break;

                case 11: //organization type
                    LookupOrganizationType lorgType = LookupManager.GetLookup<LookupOrganizationType>();
                    lorgType.OrderBy(x => x.Name).ToList().ForEach(orgType =>
                        retVal.Add(new KeyValuePair<string, string>(orgType.OrganizationTypeId.ToString(), orgType.Name)));
                    break;

                case 12: //official representative type
                    LookupOfficialRepresentativeType lorepType = LookupManager.GetLookup<LookupOfficialRepresentativeType>();
                    lorepType.OrderBy(x => x.Name).ToList().ForEach(orepType =>
                        retVal.Add(new KeyValuePair<string, string>(orepType.OfficialRepresentativeTypeId.ToString(), orepType.Name)));
                    break;

                case 13: //delivery type
                    LookupDeliveryType ldeliveryType = LookupManager.GetLookup<LookupDeliveryType>();
                    ldeliveryType.OrderBy(x => x.Name).ToList().ForEach(deliveryType =>
                        retVal.Add(new KeyValuePair<string, string>(deliveryType.DeliveryTypeId.ToString(), deliveryType.Name)));
                    break;

                case 17: //comment code
                    CodeManager codeManager = ManagerFactory.CreateInstance<CodeManager>();
                    codeManager.All.ToList().ForEach(code =>
                        retVal.Add(new KeyValuePair<string, string>(code.CodeId.ToString(), code.Name)));
                    break;

                case 22: //project type
                    LookupProjectType lprojectType = LookupManager.GetLookup<LookupProjectType>();
                    lprojectType.OrderBy(x => x.Name).ToList().ForEach(projectType =>
                        retVal.Add(new KeyValuePair<string, string>(projectType.ProjectTypeId.ToString(), projectType.Name)));
                    break;

                case 23: //Letter type
                    LookupLetterType lletterType = LookupManager.GetLookup<LookupLetterType>();
                    lletterType.OrderBy(x => x.Name).ToList().ForEach(letterType =>
                        retVal.Add(new KeyValuePair<string, string>(letterType.LetterTypeId.ToString(), letterType.Name)));
                    break;

                case 24: //Comment Period Type
                    LookupPhaseType lphaseType = LookupManager.GetLookup<LookupPhaseType>();
                    lphaseType.OrderBy(x => x.Name).ToList().ForEach(x =>
                        retVal.Add(new KeyValuePair<string, string>(x.PhaseTypeId.ToString(), x.Name)));
                    break;
            }
            #endregion


            return Utilities.ToSelectList(retVal,null,false);
        }

        /// <summary>
        /// Gets a List of Column Groups (formated for display in a dropdown)
        /// </summary>
        /// <returns>The list of column groups</returns>
        public IEnumerable<SelectListItem> SelectListColumnGroups()
        {
            List<KeyValuePair<int, string>> si = new List<KeyValuePair<int, string>>();
            si.Add(new KeyValuePair<int, string>(0, ""));

            CustomReportFieldManager.GetColumnList().ForEach(element =>
                si.Add(new KeyValuePair<int, string>(element.Id, element.Name)));

            return Utilities.ToSelectList(si, null, false);
        }

        /// <summary>
        /// Gets a List of Columns for the selected column group
        /// </summary>
        /// <returns>The list of column groups</returns>
        public IEnumerable<SelectListItem> SelectListColumns()
        {
            List<KeyValuePair<int, string>> si = new List<KeyValuePair<int, string>>();
            si.Add(new KeyValuePair<int, string>(0, ""));

            if (ColumnGroup > 0)
            {
                CustomReportFieldManager.GetColumnList(ColumnGroup).ForEach(element =>
                    si.Add(new KeyValuePair<int, string>(element.Id, element.Name)));
            }

            return Utilities.ToSelectList(si, null, false);
        }

        public IEnumerable<SelectListItem> SelectListChosenColumns(bool includeBlank = false)
        {
            List<KeyValuePair<int, string>> retVal = new List<KeyValuePair<int, string>>();
            if (includeBlank) { retVal.Add(new KeyValuePair<int, string>(-1, "")); }
            retVal.AddRange(MetaData.ColumnDisplay());
            return Utilities.ToSelectList(retVal, null, false);
        }


        /// <summary>
        /// (deprecated) Fetches the parameter tree as a List of telerik treeview Items
        /// </summary>
        /// <returns>The list of tree view Items</returns>
        public List<TreeViewItem> ParameterTreeDisplay()
        {
            return MetaData.ParameterTreeDisplay();
        }



        public IEnumerable<SelectListItem> OrderOptions()
        {
            List<KeyValuePair<string, string>> si = new List<KeyValuePair<string, string>>();
            si.Add(new KeyValuePair<string, string>("true", "Ascending"));
            si.Add(new KeyValuePair<string, string>("false", "Descending"));
            return Utilities.ToSelectList(si);
        }

        public IEnumerable<Unit> Units
        {
            get
            {
                var lookupUnit = LookupManager.GetLookup<LookupUnit>();
                return lookupUnit.Where(unit => unit.IsRegion);
            }
        }

        public List<KeyValuePair<int, string>> WizardStepList()
        {
            int count = 0;
            List<KeyValuePair<int, string>> retVal = new List<KeyValuePair<int, string>>();
            foreach (WizardStepElement wizardStepElement in ReportWizardManager.Instance.GetWizard(PageMode.CustomReportWizard))
            {
                retVal.Add(new KeyValuePair<int, string>(count,
                    Utilities.GetWizardPageTitle(PageMode.CustomReportWizard, wizardStepElement.Name, wizardStepElement.Name)));
                count++;
            }
            return retVal;
        }


        #endregion

        public CustomReportFieldElement GetFieldElement(string customReportFieldType)
        {
            int choosenFieldId = 0;
            switch (customReportFieldType)
            {
                case CustomReportFieldType.Project:
                    choosenFieldId = ProjectFieldId;
                    break;
                case CustomReportFieldType.Letter:
                    choosenFieldId = LetterFieldId;
                    break;
                case CustomReportFieldType.Commenter:
                    choosenFieldId = CommenterFieldId;
                    break;
            }
            if (choosenFieldId <= 0)
            { return new CustomReportFieldElement { Name = "", Id = 0, DataType = "None", FieldType = customReportFieldType }; }
            return CustomReportFieldManager.GetById(choosenFieldId);
            
        }

        public bool ShowDiv(string customReportFieldType, string customReportFieldDataType)
        {
            int choosenFieldId = 0;
            switch (customReportFieldType)
            {
                case CustomReportFieldType.Project:
                    choosenFieldId = ProjectFieldId;
                    break;
                case CustomReportFieldType.Letter:
                    choosenFieldId = LetterFieldId;
                    break;
                case CustomReportFieldType.Commenter:
                    choosenFieldId = CommenterFieldId;
                    break;
            }
            if (choosenFieldId <= 0)
            { return false; }
            return CustomReportFieldManager.GetById(choosenFieldId).DataType == customReportFieldDataType;
        }

        /// <summary>
        /// Gets the list of all the columns titles
        /// </summary>
        /// <returns>A list of the column titles</returns>
        public List<string> ColumnTitles()
        {
            return MetaData.ColumnDisplay().Select(x => x.Value).ToList();
        }

        /// <summary>
        /// Returns the listId of the specified sort column
        /// </summary>
        /// <param name="sortOrder">The sort column to get (1, 2, or 3)</param>
        /// <returns>The listId of the specified column if it has already been set
        /// If not then it returns the listId of the first, second, or third column
        /// Or the last column if there are not enough columns</returns>
        private int GetSortColumn(int sortOrder)
        {
            if (!MetaData.parameters.ColumnDataItems.Any())
            {
                return 0;
            }
            if (MetaData.parameters.ColumnDataItems.Any(x => x.SortOrder == sortOrder))
            {
                return MetaData.parameters.ColumnDataItems.FirstOrDefault(x => x.SortOrder == sortOrder).ListId;
            }
            if (MetaData.parameters.ColumnDataItems.Count() >= sortOrder)
            {
                return MetaData.parameters.ColumnDataItems.ElementAt(sortOrder - 1).ListId;
            }
            return MetaData.parameters.ColumnDataItems.LastOrDefault().ListId;
        }


        

    } 
}