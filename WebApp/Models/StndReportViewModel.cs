﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Report;
using Aquilent.Cara.WebApp.Models.Abstract;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.WebApp.Models
{
    /// <summary>
    /// Represents a standard report view Model
    /// </summary>
    public class StndReportViewModel : AbstractViewModel
    {
        #region Properties
        public ICollection<Project> Projects { get; set; }
        public string ProjectNameOrNumber { get; set; }
        public int? ReportId { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        #endregion
    }
}