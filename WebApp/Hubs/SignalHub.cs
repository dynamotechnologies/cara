﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Cara.WebApp.Models;
using Aquilent.Framework.Security;

using log4net;


namespace Aquilent.Cara.WebApp
{
    public class SignalHub : Hub
    {
        private readonly SignalServer _signalServer;

        public SignalHub() : this(SignalServer.Instance) { }

        public SignalHub(SignalServer signalServer)
        {
            _signalServer = signalServer;
        }

        private ILog _log = LogManager.GetLogger(typeof(SignalHub));

        //Check to see if the concern / response is being edited (currentUser is the user who wants to check)
        public SignalResponse CheckConcernHubs(int concernResponseId, int currentUserId)
        {
            SignalResponse retVal = new SignalResponse()
            {
                Content = SignalContent.NoResponse,
                Error = ""
            };
            try
            {
                using (ConcernResponseManager crm = ManagerFactory.CreateInstance<ConcernResponseManager>())
                {
                    ConcernResponse concern = crm.Get(concernResponseId);
                    if (concern != null && concern.UserId.HasValue && concern.UserId.Value != currentUserId)
                    {
                        retVal = _signalServer.CheckHubs(Constants.CONCERN_RESPONSE, concernResponseId, concern.UserId.Value);
                        if (retVal.Content == SignalContent.Heartbeat)
                        {
                            using (UserManager um = ManagerFactory.CreateInstance<UserManager>())
                            {
                                User u = um.Get(concern.UserId.Value);
                                retVal.UserName = u.FullName;
                                retVal.Phone = u.Phone;
                                retVal.Email = u.Email;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
                _log.Error(
                    String.Format("Error occured in {0} with concernResponseId {1} and currentUserId {2}",
                        "CheckConcernHubs", concernResponseId, currentUserId),
                    ex);
            }
            return retVal;
        }

        //deprecated: gets heartbeat and sends take message in one call.
        /*public SignalResponse ConcernTakeAssignment(int concernResponseId, int currentUserId)
        {
            SignalResponse retVal = new SignalResponse()
            {
                Content = SignalContent.NoResponse,
                Error = ""
            };
            try
            {
                //check if any user is currently editing the page
                SignalResponse response = CheckConcernHubs(concernResponseId, currentUserId);
                if (response.Content == SignalContent.Error || response.Content == SignalContent.NoResponse)
                {
                    retVal = response;
                }
                else
                {
                    //Prompt that user to allow or deny permission
                    retVal = SendTakeMessage(concernResponseId, currentUserId);
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
            }
            return retVal;
        }*/

        //Send reply to server
        public SignalResponse Reply(string messageId, SignalContent content)
        {
            return _signalServer.Reply(messageId, content);
        }

        //Send prompt to eidting User to allow reassignment
        public SignalResponse SendTakeMessage(int concernResponseId, int currentUserId)
        {
            SignalResponse retVal = new SignalResponse()
            {
                Content = SignalContent.NoResponse,
                Error = ""
            };
            try
            {
                using (ConcernResponseManager crm = ManagerFactory.CreateInstance<ConcernResponseManager>())
                {
                    ConcernResponse concern = crm.Get(concernResponseId);
                    if (concern != null && concern.UserId.HasValue && concern.UserId.Value != currentUserId)
                    {
                        string cFullName, cPhone, cEmail, aFullName, aPhone, aEmail;
                        using (UserManager um = ManagerFactory.CreateInstance<UserManager>())
                        {
                            User currentUser = um.Get(currentUserId);
                            cFullName = currentUser.FullName;
                            cPhone = currentUser.Phone;
                            cEmail = currentUser.Email;
                            User assignedUser = um.Get(concern.UserId.Value);
                            aFullName = assignedUser.FullName;
                            aPhone = assignedUser.Phone;
                            aEmail = assignedUser.Email;
                        }
                        retVal = _signalServer.AllowDeny
                            (Constants.CONCERN_RESPONSE, concernResponseId, concern.UserId.Value, cFullName, cPhone, cEmail);
                        retVal.UserName = aFullName;
                        retVal.Phone = aPhone;
                        retVal.Email = aEmail;
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
                _log.Error(
                    String.Format("Error occured in {0} with concernResponseId {1} and currentUserId {2}",
                        "SendTakeMessage", concernResponseId, currentUserId),
                    ex);
            }
            return retVal;
        }
    }
}