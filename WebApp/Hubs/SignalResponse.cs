﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp
{
    public class SignalResponse
    {
        public string Error;
        public SignalContent Content;
        public string UserName;
        public string Phone;
        public string Email;
    }
}