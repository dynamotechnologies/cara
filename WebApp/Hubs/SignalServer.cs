﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using System.Threading;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

using log4net;

namespace Aquilent.Cara.WebApp
{
    public class SignalServer
    {
        // Singleton instance
        private readonly static Lazy<SignalServer> instance = 
            new Lazy<SignalServer>(() => 
                new SignalServer(GlobalHost.ConnectionManager.GetHubContext<SignalHub>().Clients)); //needs dynamic?

        private ILog _log = LogManager.GetLogger(typeof(SignalHub));

        private ConcurrentDictionary<string, SignalMessage> messages = new ConcurrentDictionary<string,SignalMessage>();
        private readonly object messageLock = new object();
        private volatile bool updateingMessages = false;
        private readonly object logLock = new object();
        private volatile bool logging = false;

        private const int AutoWaitTime = 5000; //5 sec
        private const int UserWaitTimeFactor = 24; //autoWaitTime*24 = 2 minutes
        private const int MessageLifetime = 1800000; // 30 minutes

        private IHubConnectionContext Clients //needs dynamic?
        {
            get;
            set;
        }

        //Provides instance with clients
        private SignalServer(Microsoft.AspNet.SignalR.Hubs.IHubConnectionContext clients)
        {
            Clients = clients;
        }

        //Sends heartbeat request to check for a single contextId / user pair
        public SignalResponse CheckHubs(string context, int contextId, int userId)
        {
            SignalResponse retVal = new SignalResponse()
                {
                    Content = SignalContent.NoResponse,
                    Error = ""
                };
            try
            {
                ClearOldMessages();
                //get a guid for a new message
                Guid messageId = Guid.NewGuid();
                //send heartbeat
                Clients.All.SendHeartBeat(context, contextId, userId, messageId.ToString());

                //Sleep
                Thread.Sleep(AutoWaitTime);

                //check heartbeat

                if (messages.ContainsKey(messageId.ToString()))
                {
                    retVal.Content = SignalContent.Heartbeat;
                    TakeMessage(messageId.ToString());
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
                lock (logLock)
                {
                    logging = true;
                    _log.Error(
                        String.Format("Encountered an error in CheckHubs context:{0} contextId:{1} userId:{2}", 
                            context, contextId, userId), 
                        ex);
                    logging = false;
                }
            }

            return retVal;
        }

        //Sends heartbeat requests to check for multiple  contextId / user pairs
        //returns contextids whose heartbeats were recieved
        public List<int> CheckHubs(string context, List<KeyValuePair<int, int>> contextUserIds)
        {
            List<int> retVal = new List<int>();
            List<KeyValuePair<Guid, int>> guidContextIds = new List<KeyValuePair<Guid, int>>();
            Guid messageId;
            ClearOldMessages();

            //send messages and store Guids
            foreach (KeyValuePair<int, int> contextUserId in contextUserIds)
            {
                messageId = Guid.NewGuid();
                guidContextIds.Add(new KeyValuePair<Guid, int>(messageId, contextUserId.Key));
                try
                {
                    Clients.All.SendHeartBeat(context, contextUserId.Key, contextUserId.Value, messageId.ToString());
                }
                catch(Exception ex)
                {
                    retVal.Add(contextUserId.Key);
                    lock (logLock)
                    {
                        logging = true;
                        _log.Error(
                            String.Format("Encountered an error in CheckHubs(list) context:{0} contextId:{1} userId:{2}",
                                context, contextUserId.Key, contextUserId.Value),
                            ex);
                        logging = false;
                    }
                }
            }
            Thread.Sleep(AutoWaitTime);
            
            //Check Responses
            foreach (KeyValuePair<Guid, int> guidContextId in guidContextIds)
            {
                if (messages.ContainsKey(guidContextId.Key.ToString()))
                {
                    TakeMessage(guidContextId.Key.ToString());
                    retVal.Add(guidContextId.Value);
                }
            }
            return retVal;
        }

        //Sends request for a Allow or Deny decision from a user
        public SignalResponse AllowDeny(string context, int contextId, int userId, string fullName, string phone, string eMail)
        {
            SignalResponse retVal = new SignalResponse()
            {
                Content = SignalContent.NoResponse,
                Error = ""
            };
            try
            {
                ClearOldMessages();
                //get a guid for a new message
                Guid messageId = Guid.NewGuid();
                //send heartbeat
                Clients.All.AllowDeny(context, contextId, userId, fullName, phone, eMail, messageId.ToString());

                //Sleep
                for (int i = 0; i < UserWaitTimeFactor; i++)
                {
                    Thread.Sleep(AutoWaitTime);

                    //check messages
                    if (messages.ContainsKey(messageId.ToString()))
                    {
                        retVal.Content = SignalContent.Heartbeat;
                        retVal = TakeMessage(messageId.ToString());
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
                lock (logLock)
                {
                    logging = true;
                    _log.Error(
                        String.Format("Encountered an error in AllowDeny context:{0} contextId:{1} userId:{2}",
                            context, contextId, userId),
                        ex);
                    logging = false;
                }
            }

            return retVal;
        }

        //Is called by the client to send a reply to the server
        public SignalResponse Reply(string messageId, SignalContent content)
        {
            SignalResponse retVal = new SignalResponse()
            {
                Content = SignalContent.NoResponse,
                Error = ""
            };
            try
            {
                lock (messageLock)
                {
                    if (!updateingMessages)
                    {
                        updateingMessages = true;

                        SignalMessage message = new SignalMessage()
                        {
                            id = new Guid(messageId),
                            Content = content,
                            Updated = DateTime.UtcNow
                        };
                        retVal.Content = content;
                        if (!messages.TryAdd(messageId, message))
                        {
                            retVal.Content = SignalContent.Error;
                            retVal.Error = "Could not add to message collection.";
                        }

                        updateingMessages = false;
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
                lock (logLock)
                {
                    logging = true;
                    _log.Error(
                        String.Format("Encountered an error in Reply content:{0} messageId:{1}",
                            content, messageId),
                        ex);
                    logging = false;
                }
            }

            return retVal;
        }

        //Sends a message to the concern response client to submit with "ForceSave" action
        public void ReassignConcern(int concernResponseId, int oldUserId)
        {
            Clients.All.ReassignConcern(concernResponseId, oldUserId);
            System.Threading.Thread.Sleep(AutoWaitTime);
        }

        public static SignalServer Instance
        {
            get
            {
                return instance.Value;
            }
        }

        //Clears old messages from dictionary
        private void ClearOldMessages()
        {
            lock (messageLock)
            {
                if (!updateingMessages)
                {
                    updateingMessages = true;

                    SignalMessage message;

                    messages.Values
                        .Where(x => x.Updated > DateTime.UtcNow.AddMilliseconds(-1 * MessageLifetime))
                        .Select(x => x.id.ToString())
                        .ToList()
                        .ForEach(x => messages.TryRemove(x, out message));

                    updateingMessages = false;
                }
            }
        }

        //Search dictionary for message, remove it, and return a response
        private SignalResponse TakeMessage(string messageId)
        {
            SignalResponse retVal = new SignalResponse()
            {
                Error = ""
            };
            try
            {
                lock (messageLock)
                {
                    if (!updateingMessages)
                    {
                        updateingMessages = true;

                        SignalMessage message;
                        if (!messages.TryRemove(messageId, out message))
                        {
                            retVal.Content = SignalContent.Error;
                            retVal.Error = "Could not retrieve response from user.";
                        }
                        else
                        {
                            retVal.Content = message.Content;
                        }

                        updateingMessages = false;
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Content = SignalContent.Error;
                retVal.Error = ex.Message;
            }
            return retVal;
        }

    }
}