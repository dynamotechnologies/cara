﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.WebApp
{
    public class SignalMessage
    {
        public Guid id;
        public SignalContent Content;
        public DateTime Updated;
    }

    public enum SignalContent
    {
        Allow = 0,
        Deny = 1,
        Error = 2,
        Heartbeat = 3,
        NoResponse = 4
    }
}