﻿<!--
// -------------------------------------------------------------
// handles the onclick event of the save buttons
// -------------------------------------------------------------
function confirmSave(action) {
    // confirm the save action if there is one or more coded comments
    if ($("#HasCodedComments").val() == "True" && $("#TaskAssignmentContext").val() != "")
    {
        confirmWindowSize(
            "Comments have already been coded in this comment period. Do you wish to update task assignments on existing comments with the new user selection",
            "submitCodeAssignment(\"" + action + "\",\"True\")",
            "submitCodeAssignment(\""+action+ "\",\"False\")",
            300, 90);
    }
    else
    {
        submitCodeAssignment(action,"False");
    }
}

// -------------------------------------------------------------
// submits the code input page to save the selected codes
// -------------------------------------------------------------
function submitCodeAssignment(actionValue, updateComments) {
    document.getElementById("ActionString").value = actionValue;
    document.getElementById("UpdateComments").value = updateComments;
    document.forms[0].submit();
}
// -->