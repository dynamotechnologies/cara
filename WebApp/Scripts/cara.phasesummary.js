﻿<!--

// ------------------------------------------------------------------------
// handles the onclick event of the cancel wizard action
// ------------------------------------------------------------------------
function cancelWizard(msg, url) {
    confirmWindow(msg, "doCancelWizard(\"" + url + "\")");
}

// ------------------------------------------------------------------------
// cancel wizard by redirecting to the input url
// ------------------------------------------------------------------------
function doCancelWizard(url) {
     window.location.href = url;
}

//-->