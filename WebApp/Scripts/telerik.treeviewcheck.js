// script to check/uncheck the child nodes when the parent node is checked/unchecked
$(document).ready(function () {
    $('#TreeView').find("li:has(ul)")
                          .find('> div > .t-checkbox :checkbox')
                          .bind('click', function (e) {
                              var isChecked = $(e.target).is(':checked');
                              var treeView = $($(e.target).closest('.t-treeview')).data('tTreeView');
                              var checkboxes = $(e.target).closest('.t-item')
                                                          .find('.t-checkbox :checkbox');
                              $.each(checkboxes, function (index, checkbox) {
                                  $(checkbox).attr('checked', isChecked ? true : false);
                                  treeView.checkboxClick(e, checkbox);
                              });
                          });
})