﻿<!--

var confirmActionParm = "Confirm=ConfirmRedesignate"

// ---------------------------------------------------------
// compare the form set master
// ---------------------------------------------------------
function compareFormSetMaster() {
    var dropDownList = $("#ddlFormSet").data("tDropDownList");
    var masterFormId = dropDownList.value();
    var navigate = $("#CanNavigate").val();

    if (masterFormId != "")
    {
        // set up parameters
        var parms = {
            projectId: getProjectId(),
            phaseId: getPhaseId(),
            id: getSourceLetterId(),
            targetLetterId: masterFormId,
            navigate: navigate
        };

        // ajax action call to set the letter type
        $.post("/Letter/CompareMaster",
            parms,
            function(data) {
                // refresh the viewer div
                $("#viewerArea").html(data);
            },
            "html");
    }
    else
    {
        alertWindow("Please select a Form Set to compare.");
    }
}

// ---------------------------------------------------------
// perform the redesignate action
// ---------------------------------------------------------
function redesignateLetter() {
    var dropDownList = $("#ddlAction").data("tDropDownList");
    var action = dropDownList.value();
    var letterId = getSourceLetterId();
    var formSetId = getMasterFormSetId();

    switch (action)
    {
        case "Unique":
            confirmWindow(getRedesignateMsg(action), "confirmSetLetterType(" + letterId + ", \"" + formSetId + "\", \"" + action + "\")");
            break;
        case "Form":
            confirmWindow(getRedesignateMsg(action), "confirmSetLetterType(" + letterId + ", \"" + formSetId + "\", \"" + action + "\")");
            break;
        case "Form Plus":
            confirmWindow(getRedesignateMsg(action), "confirmSetLetterType(" + letterId + ", \"" + formSetId + "\", \"" + action + "\")");
            break;
        case "Master":
            confirmWindow(getRedesignateMsg(action), "confirmSetToMaster(" + letterId + ", \"" + formSetId + "\")");
            closeCompareWindow();
            break;
        case "Send":
            closeCompareWindow();
            updateLetterFormSet(letterId, formSetId);
            break;
        default:
            alertWindow("Please select an action.");
            break;
    }

    return false;
}

// -----------------------------------------------------------------------------------
// confirm the set letter type action from compare window
// -----------------------------------------------------------------------------------
function confirmSetLetterType(letterId, formSetId, action) {
    // close the compare window
    closeCompareWindow();
    // set letter type
    setLetterType(letterId, formSetId, action);
}

// -----------------------------------------------------------------------------------
// confirm the set master letter action from compare window
// -----------------------------------------------------------------------------------
function confirmSetToMaster(letterId, formSetId) {
    // close the compare window
    closeCompareWindow();
    // set letter type
    setToMaster(letterId, formSetId);
}

// -----------------------------------------------------------------------------------
// resort the form set by comparing the source form set with target form set
// -----------------------------------------------------------------------------------
function resortFormSet() {
    // get the the source and target formset selections
    var sourceFormSetId = "";
    var compareToFormSetId = "";
        
    // clear the dropdown value
    var ddlSource = $("#SourceFormSetId").data("tDropDownList");
    var ddlTarget = $("#CompareToFormSetId").data("tDropDownList");

    if (ddlSource != null)
    {
        sourceFormSetId = ddlSource.value();
    }

    if (ddlTarget != null)
    {
        compareToFormSetId = ddlTarget.value();
    }

    if (sourceFormSetId == "")
    {
        alertWindow("Please select a Form Set to be re-sorted.");
        return false;
    }
    else if (compareToFormSetId == "")
    {
        alertWindow("Please select a Form Set to be re-sorted against.");
        return false;
    }

    // disable the button
    $("#btnResort").text("Please wait...");
    $("#btnResort").attr("disabled", "disabled");

    // set up parameters (pass phaseId as a request param for priv checking)
    var parms = {
        projectId: getProjectId(),
        phaseId: getPhaseId(),
        id: sourceFormSetId,
        compareToFormSetId: compareToFormSetId
    };

    // ajax action call to set the letter type
    $.post("/Form/ResortFormSet",
        parms,
        function(data)
        {
            if (data)
            {
                if (data != null && data != "")
                {
                    var intRegex = /^\d+$/;

                    // validate the output to make sure it's a number
                    if (intRegex.test(data))
                    {
                        confirmWindowWithCancelCallBack("The re-sorting of the selected Form Set completed. " + data + " letter(s) have been resorted. Do you want to view the Form Change Log", "confirmResortFormSet()", "redirectToUrl(\"" + getRequestUri() + "\", \"\")");
                    }
                    else
                    {
                        alertWindow("An error has occurred while performing the action.");
                    }
                }
            }
            else
            {
                alertWindow("An error has occurred while performing the action.");
            }
        },
        "html");

    return false;
}

// -----------------------------------------------------------------------------------
// confirm the re-sort action
// -----------------------------------------------------------------------------------
function confirmResortFormSet() {
    // redirect to the url for change log
    redirectToUrl(getRequestUri() + "/ChangeLogList" , "");
}

// -----------------------------------------------------------------------------------
// set the letter type
// -----------------------------------------------------------------------------------
function setLetterType(letterId, formSetId, type) {
    // set up parameters (pass phaseId as a request param for priv checking)
    var parms = {
        projectId: getProjectId(),
        phaseId: getPhaseId(),
        id: letterId,
        type: type,
        formSet: formSetId,
        confirmMessage: "Letter Type Updated"
    };

    //CR#308 Not returning to same page of letter inbox
    var parmsForRedirect = {
        projectId: getProjectId(),
        phaseId: getPhaseId(),
        id: letterId,
        formSet: formSetId,        
        nextLetterIds: $("#NextLetterIds").val(),
        lettersRemaining : $("#LettersRemaining").val()
    };

    // ajax action call to set the letter type
    $.post("/Form/UpdateLetterType",
        parms,
        function(data,status)
        {
            if (status === "success")
            {
                // redirect to the url for refresh
                redirectToUrl(getRequestUri(), parmsForRedirect);
            }
            else
            {
                alertWindow("An error has occurred while performing the action.");
            }
        },
        "html");            
}

// -----------------------------------------------------------------------------------
// set the letter to be the master letter of the form set
// -----------------------------------------------------------------------------------
function setToMaster(letterId, formSetId) {
    // set up parameters (pass phaseId as a request param for priv checking)
    var parms = {
        projectId: getProjectId(),
        phaseId: getPhaseId(),
        id: letterId,
        formSet: formSetId,
        confirmMessage: "Set Letter Type To Master."
    };

    //CR#308 Not returning to same page of letter inbox
    var parmsForRedirect = {
        projectId: getProjectId(),
        phaseId: getPhaseId(),
        id: letterId,
        formSet: formSetId,        
        nextLetterIds: $("#NextLetterIds").val(),
        lettersRemaining : $("#LettersRemaining").val()
    };

    // ajax action call to set the letter type
    $.post("/Form/SetLetterToMaster",
        parms,
        function(data,status)
        {
            if (status === "success")
            {
                // redirect to the url for refresh
                redirectToUrl(getRequestUri() , parmsForRedirect);
            }
            else
            {
                alertWindow("An error has occurred while performing the action.");
            }
        },
        "html");
}

// -----------------------------------------------------------------------------------
// update the letter with the new form set
// -----------------------------------------------------------------------------------
function updateLetterFormSet(letterId, formSetId) {
    // set up parameters (pass phaseId as a request param for priv checking)
    var parms = {
        projectId: getProjectId(),
        phaseId: getPhaseId(),
        id: letterId,
        formSet: formSetId
    };
    

    // ajax action call to set the letter type
    $.post("/Form/UpdateLetterFormSet",
        parms,
        function(data)
        {
            if (data)
            { 
                // redirect to the url for refresh
                redirectToUrl(getRequestUri(),parms);
            }
            else
            {
                alertWindow("An error has occurred while performing the action.");
            }
        },
        "html");
}

// -----------------------------------------------------------------------------------
// close the compare window popup
// -----------------------------------------------------------------------------------
function closeCompareWindow() {
    // close the compare window
    var window = $("#WindowCompare").data("tWindow");

    if (window != null)
    {
        window.close();
    }
}

// ---------------------------------------------------------------
// Go to the url
// ---------------------------------------------------------------
function redirectToUrl(url, parms) { 
    var arr = [];
    for (var key in parms) {
        if (parms.hasOwnProperty(key)) {
            arr.push(key + '=' + parms[key]);
        }
    }
    parms = arr.join('&');
    window.location.href = url + (parms != "" ? "?" + parms : "");
}

// ---------------------------------------------------------------
// Return the redesignate action confirmation message
// ---------------------------------------------------------------
function getRedesignateMsg(action) {
    return (action == "Master" ? "All the letters within the Form Set will be re-sorted based on the new Master Form. " : "") +
        "Re-designate the letter to be " + action;
}

// ---------------------------------------------------------------
// Getter methods for the page hidden values
// ---------------------------------------------------------------
function getProjectId() {
    return $("#ProjectId").val();
}

function getPhaseId() {
    return $("#PhaseId").val();
}

function getSourceLetterId() {
    return $("#SourceLetterId").val();
}

function getMasterFormSetId() {
    return $("#MasterFormSetId").val();
}

function getRequestUri() {
    return $("#RequestUri").val();
}

//-->