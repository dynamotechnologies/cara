﻿<!--

$(document).ready(function () {
    $("#printLetter").click(function (event) {

         var dmdId =  $("#Letter_DmdId").val();
         var url = "/Public/DownloadCommentFile?DmdId=" + dmdId + "&pdfDisplay=true";
         var pdfWindow = window.open(url, 'Print', 'toolbar=1,scrollbars=1,location=1,status=1,menubar=1, width='+screen.width+',height='+screen.height+',resizable=1');
         if (pdfWindow != null)
         {
             pdfWindow.document.onreadystatechange = function () {
                  if (pdfWindow.document.readyState == "interactive") {
                        if (pdfWindow.document.title.indexOf("Error") != -1) {
                                pdfWindow.close();
                                alert("The document is not available for printing or doesn't exist.");              
                            }else {
                                pdfWindow.focus();
                            }
                  }          
             };
         }
    });
    $("#printLetterComment").click(function (event) {

         var id =  $("#LetterId").val();
         var url = "/Public/DownloadCommentFileComment?LetterId=" + id + "&pdfDisplay=true";
         var pdfWindow = window.open(url, 'Print', 'toolbar=1,scrollbars=1,location=1,status=1,menubar=1, width='+screen.width+',height='+screen.height+',resizable=1');
         if (pdfWindow != null)
         {
             pdfWindow.document.onreadystatechange = function () {
                  if (pdfWindow.document.readyState == "interactive") {
                        if (pdfWindow.document.title.indexOf("Error") != -1) {
                                pdfWindow.close();
                                alert("The document is not available for printing or doesn't exist.");              
                            }else {
                                pdfWindow.focus();
                            }
                  }          
             };
         }
    });

    $("#lnkAuthorList").click(function (event) {
        event.preventDefault();
    });
});

// -----------------------------------------------------------------------------------------
// update publish status of the letter
// -----------------------------------------------------------------------------------------
function updatePublishStatus(action) {
    if (action == "True")
    {
        // set up parameters
        var parms = {
            projectId: parseInt($("#ProjectId").val()),
            phaseId: parseInt($("#PhaseId").val()),
            id: parseInt($("#LetterId").val()),
            publish: "Publish"
        };

        openWindow("PleaseWait");

        // ajax action call to populate comment input area
        $.post("/Letter/UpdatePublishStatus", 
            parms,
            function(data)
            {
                window.location.href = window.location.pathname + "?tab=Details";
            },
            "text")
            .error(function(data) { closeWindow("PleaseWait"); });
    }
    else
    {
        // open the window for unpublish reason input
        openWindow('WindowUnpublish', '/Project/' + $("#ProjectId").val() + '/Phase/' + $("#PhaseId").val() + '/Letter/UnpublishReasonInput/' + $("#LetterId").val())
    }
}

function confirmUpdatePublishStatus(action, isMasterForm) {
    if (isMasterForm == "True")
    {
        var message = "You are changing the publishing status of a Master Form. This operation affects all of the letters in the form set and may take time to complete.  Please do not navigate away from this page.  Do you wish to continue";
        return confirmWindowSize(message, "updatePublishStatus(\"" + action + "\")", "", 300, 120);
    }
    else
    {
        updatePublishStatus(action);
    }
}

// -------------------------------------------------------------------
// save the unpublish reason input
// -------------------------------------------------------------------
function saveUnpublishReason() {
    var unpublishedReasonId = $("#ddlUnpublishedReason").data("tDropDownList").value();
    var unpublishedReasonOther = $("#UnpublishedReasonOther").val();    

    // validate
    if (unpublishedReasonId == null || unpublishedReasonId == "")
    {
        alertWindow("Unpublish Reason is a required field.");
        return false;
    }
    else if (unpublishedReasonId == "5" && unpublishedReasonOther == "")
    {
        alertWindow("Other Reason is a required field.");
        return false;
    }
    else if (unpublishedReasonId != "5" && unpublishedReasonOther != "")
    {
        alertWindow("Please specify the reason only if Other reason is selected.");
        return false;
    }

    // set up parameters
    var parms = {
        projectId: parseInt($("#ProjectId").val()),
        phaseId: parseInt($("#PhaseId").val()),
        id: parseInt($("#LetterId").val()),
        publish: "Unpublish",
        unpublishedReasonId: parseInt(unpublishedReasonId),
        unpublishedReasonOther: (unpublishedReasonId == "5" ? unpublishedReasonOther : null)
    };

    var windowUnpublish = $("#WindowUnpublish").data("tWindow");
    var windowPleaseWait = $("#PleaseWait").data("tWindow");

    windowUnpublish.title(windowPleaseWait.title());
    windowUnpublish.content(windowPleaseWait.content());
    windowUnpublish.refresh();

    // ajax action call to populate comment input area
    $.post("/Letter/UpdatePublishStatus", 
        parms,
        function(data)
        {
            closeWindow("PleaseWait");
            window.location.href = window.location.pathname + "?tab=Details";
        },
        "text");
}

function deleteLetter(master, dupId)
{
    var deleteLetterUrl = "";

    if (!(typeof(master) === "undefined"))
    {
        deleteLetterUrl += $("#btnDeleteDuplicate" + dupId).attr("href") + "?master=" + master;
    }
    else
    {
        deleteLetterUrl += $("#btnDeleteLetter").attr("href");
    }

    redirectToUrl(deleteLetterUrl);
}

function deleteDuplicates()
{
    var deleteDuplicatesUrl = $("#btnDeleteDuplicates").attr("href");
    redirectToUrl(deleteDuplicatesUrl);
}

function deleteAttachment(letterId, documentId)
{
   confirmWindow("Are you sure you want to permanently delete this attachment", "doDeleteAttachment(" + letterId + ", " + documentId + ")");
}

//CARA-714:- Delete letter attachment for a given letterId and documentId
function doDeleteAttachment(letterId, documentId)
{

 var parms = {
        letterId: letterId,
        documentId: documentId,
        phaseId: $("#PhaseId").val(),
        projectId: $("#ProjectId").val()
     };
     

    // ajax action call to refresh comment area
    $.post("/Letter/DeleteAttachment",
        parms,
        function(data)
        {
            if (data.errorMessage.length > 0)
            {
                alertWindow(data.errorMessage);               
            }
            else
            {
                window.location.reload();
            }
        },
        "json");

}

function deleteDuplicate(letterid, phaseId, projectId)
{
   confirmWindow("Are you sure you want to permanently delete this duplicate letter", "doDeleteAttachment(" + letterId + ", " + documentId + ")");
}

function doDeleteDuplicate(letterid, phaseId, projectId)
{
    document.location = "Project/" + projectId + "/Phase/" + phaseId + "/Letter/DeleteDuplicate/" + letterId
}


//-->