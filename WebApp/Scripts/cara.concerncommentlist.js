﻿<!--

function updateComment(projectId, phaseId, commentId) {
    // set the checkbox control ids
    var rbSampleStatement = "rbSampleStatement_" + commentId;

    // get the checked values
    var sampleStatement = $('input[name=' + rbSampleStatement +']:checked').val();

    // set up parameters
    var parms = {
        sampleStatement: sampleStatement
    };

    // ajax action call to update comment
    $.post("/Project/" + projectId + "/Phase/" + phaseId + "/Comment/UpdateSampleStatement/" + commentId,
        parms,
        function(data)
        {
            if (data == "True")
            {
                UpdateMessage("Sample Comment updated.");
            }
            else
            {
                alertWindow("An error occurred while saving the data.");
            }
        },
        "text");
}

function deleteComment(id, commentId, phaseId) {
    // set up parameters
    var parms = {
        id: id,
        commentId: commentId,
        phaseId: phaseId
    };

    // ajax action call to delete the comment
    $.post("/Concern/DeleteComment",
        parms,
        function(data)
        {
            // render the comment list
            $("#commentListArea").html(data);
            UpdateMessage("Comment Removed");
        },
        "html");
}

//-->