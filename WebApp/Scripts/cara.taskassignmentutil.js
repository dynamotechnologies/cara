﻿<!--
// -------------------------------------------------------------
// Handles on change event of the task assignment user telerik controls
// -------------------------------------------------------------
function tuserOnChange()
{
    var ddl = $(this)
    var userId = ddl.val();
    var contextId = this.name.split("_")[1];
    //var dropdown = document.getElementById("user_"+commentId);
    //var userId = dropdown.options[dropdown.selectedIndex].value;
    UpdateTaskAssignment(contextId, userId);
    changeQueryString("TaskAssignmentContext", $("#TaskAssignmentContext").val());
    changeQueryString("TaskAssignmentUsers", $("#TaskAssignmentUsers").val());
}

// -------------------------------------------------------------
// handles the onclick event of the submit buttons
// -------------------------------------------------------------
function confirmSubmit(action) {
    var TaskAssignmentContext = $("#TaskAssignmentContext").val()
    if (TaskAssignmentContext != "")
    {
        confirmWindow(
            "There may be unsaved changes on this page. If you continue, your changes will be lost. Do you wish to continue",
            "doSubmit(\"" + action + "\")");
    }
    else 
    {
        doSubmit(action);
    }
}

function doSubmit(actionValue) {
    document.getElementById("ActionString").value = actionValue;
    document.forms[0].submit();
}

// -------------------------------------------------------------
// changes query string parameter in grid paging buttons
// -------------------------------------------------------------
function changeQueryString(attributeName, attributeValue)
{
    var buttons = $("div.t-pager a");
    var i=0;
    for(i = 0; i < buttons.length; i++)
    {
        buttons[i].attributes["href"].value =updateQueryAttribute(
            buttons[i].attributes["href"].value, 
            attributeName, 
            attributeValue);
    }
}

function updateQueryAttribute(originalUrl, attributeName, attributeValue)
{
    var arrAttributes = originalUrl.split("&");
    if (arrAttributes.length == 0)
    {
        return "";
    }
    var newUrl = arrAttributes[0];
    var i = 0;
    for (i = 1; i < arrAttributes.length; i++)
    {
        var arrAttr = arrAttributes[i].split("=");
        if (arrAttr[0] != attributeName)
        {
            newUrl = newUrl + "&" + arrAttributes[i];
        }
    }
    newUrl = newUrl + "&" + attributeName  + "=" + attributeValue;
    return newUrl;
}

function UpdateTaskAssignment(contextId, userId)
{
    var contextIds = document.getElementById("TaskAssignmentContext").value;
    var userIds = document.getElementById("TaskAssignmentUsers").value;
    var newContextIds = "";
    var newUserIds = "";
    var arrContextIds = contextIds.split("|");
    var arrUserIds = userIds.split("|");
    var i=0;
    for (i=0;i<arrContextIds.length;i++)
    {
        if (arrContextIds[i] != contextId)
        {
            newContextIds += newContextIds.length > 0 ? "|":"";
            newContextIds += arrContextIds[i];
            newUserIds += newUserIds.length > 0 ? "|":"";
            newUserIds += arrUserIds[i];
        } 
    } 
    newContextIds += newContextIds.length > 0 ? "|":"";
    newContextIds += contextId;
    newUserIds += newUserIds.length > 0 ? "|":"";
    newUserIds += userId;
    document.getElementById("TaskAssignmentContext").value = newContextIds;
    document.getElementById("TaskAssignmentUsers").value = newUserIds;
}

//-->