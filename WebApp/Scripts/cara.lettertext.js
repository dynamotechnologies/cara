﻿<!--

function CaraLetterText(width, length, id)
{
    var self = this;
    this.Width = width;
    this.Length = length;
    this.Id = id;
    this.FontSize = "12px";
    this.AutoMarkupId = "Auto-Markup";
    this.PendingCommentId = "Pending";
    this.PendingTag = "[Comment Pending]";
    this.CurrentId = "";

    var divId = "rootDiv";
    var bookmarkClass = "bookmark";
    var pendingTagHide = "pendingTagHide";
    var pendingTextShow = "highlight";
    var commentTagShow = "commentTagShow";
    var commentTagHide = "commentTagHide";
    var commentTagCurrent = "commentTagCurrent";
    var commentTextShow = "commentTextShow";
    var commentTextHide = "commentTextHide";
    var commentTextCurrent = "commentTextCurrent";
    var linkTagShow = "linkTagShow";
    var linkTagHide = "linkTagHide";
    var linkTagCurrent = "linkTagCurrent";
    var autoMarkupTagHide = "autoMarkupTagHide";
    var autoMarkupTextShow = "autoMarkupTextShow";
    var autoMarkupTextHide = "autoMarkupTextHide";
    var autoMarkupTextCurrent = "autoMarkupTextCurrent";
    var commentIdAttr = "data-commentid";
    var passageIdAttr = "data-passageid";
    var passageTypeAttr = "data-passagetype";
    var isStartTagAttr = "data-isstarttag";
    var bookmarkTypeAttr = "data-bookmarkType";
    var linkTitleText = "comment continues";
    var linkTitleAttrText = " title='" + linkTitleText + "'";
    var ptComment = "Comment";
    var ptAutoMarkup = "Auto-markup";
    var ptPending = "Pending";
    var ptPassage = "Passage";
    var ptSessionAutoMarkup = "Session Auto-markup";
    var btAnchor = "Anchor";
    var btFocus = "Focus";
    var endSpan = "</span>";
    var commentStartTag = "[comment:";
    var commentEndTag = "[comment end]"; 
    var commentLinkTag = "[...]";
    var autoMarkupStartTag = "[auto-markup:";
    var autoMarkupEndTag = "[auto-markup end]";
    var sessionAutoMarkupStartTag = "[session auto-markup:"
    var viewModeId = 3;
    var currentPassageId = 0;
    var pendingPassageId = -1;
    var maxPassageId = 0;
    var pendingText = "";
    var codingTinyMce = new CaraTinyMce(self.Width, self.Length, self.Id, setup);
    var locked = false;
    var lockedMessageDisplayed = false;

    this.Init = function ()
    {
        codingTinyMce.ReadOnly = true;
        codingTinyMce.InitInstanceCallBack = initInstanceCallBack;
        addRootDiv();
        codingTinyMce.Init();
    }

    this.NextComment = function(viewMode)
    {
        return move(true, viewMode);
    }

    this.PrevComment = function(viewMode)
    {
        return move(false, viewMode);
    }

    this.GoToComment = function(commentNumber)
    {
        setCurrentById(ptComment, commentNumber, true);
    }

    this.OnUpdateCurrent = function(currentTag, currentId)
    {
    }

    this.GetViewMode = function()
    {
        return viewModeId;
    }

    this.SetViewMode = function(viewMode)
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return;
        }
        if (viewMode != viewModeId)
        {
            viewModeId = viewMode;
            var dom=tinymce.get(self.Id).dom;
            if (viewModeId < 2)
            {
                //turn off comments
                if (self.CurrentId != self.AutoMarkupId)
                {
                    clearSelection(true);
                }
                dom.addClass(
                    dom.select("." + commentTagShow), commentTagHide);
                dom.addClass(
                    dom.select("." + commentTextShow), commentTextHide);
                dom.addClass(
                    dom.select("." + linkTagShow), linkTagHide);
                dom.removeClass(dom.select("." + commentTagShow), commentTagShow);
                dom.removeClass(dom.select("." + commentTextShow), commentTextShow);
                dom.removeClass(dom.select("." + linkTagShow), linkTagShow);
            }
            else
            {
                //turn on comments
                dom.addClass(dom.select("." + commentTagHide), commentTagShow);
                dom.addClass(dom.select("." + commentTextHide), commentTextShow);
                dom.addClass(dom.select("." + linkTagHide), linkTagShow);
                dom.removeClass(dom.select("." + commentTagHide), commentTagHide);
                dom.removeClass(dom.select("." + commentTextHide), commentTextHide);
                dom.removeClass(dom.select("." + linkTagHide), linkTagHide);
            }
            if (viewModeId == 0 || viewModeId == 2)
            {
                //turn off automarkups
                if (self.CurrentId == self.AutoMarkupId)
                {
                    clearSelection(true);
                }
                dom.addClass(dom.select("." + autoMarkupTextShow), autoMarkupTextHide);
                dom.removeClass(dom.select("." + autoMarkupTextShow), autoMarkupTextShow);
            }
            else
            {
                //turn on autoMarkups
                dom.addClass(dom.select("." + autoMarkupTextHide), autoMarkupTextShow);
                dom.removeClass(dom.select("." + autoMarkupTextHide), autoMarkupTextHide);
            }
        }
    }

    this.SetWidthHeight = function(width, height)
    {
        var iFrame = $("#" + self.Id + "_ifr");
        if (width != null && width != "")
        {
            iFrame.width(width);
        }
        if (height != null && height != "")
        {
            iFrame.height(height);
        }
    }

    this.GetText = function()
    {
        var dom=tinymce.get(self.Id).dom;
        var letterElement = $(dom.getOuterHTML(dom.select("#" + divId)[0]));
        var tag = null;
        var tags = letterElement.find("[" + passageTypeAttr + "='" + ptComment + "'],[" + passageTypeAttr + "='" + ptPending + "']");
        for (i=0;i<tags.length;i++)
        {
            tag = tags[i];
            tag.outerHTML = tag.innerHTML;
        }
        tags = letterElement.find("[" + passageTypeAttr + "='" + ptAutoMarkup + "']");
        for (i=0;i<tags.length;i++)
        {
            tag = tags[i];
            tag.outerHTML = tag.innerHTML;
        }
        tags = letterElement.find("." + autoMarkupTagHide + ":[" + passageTypeAttr + "='" + ptSessionAutoMarkup + "']");
        for (i=0;i<tags.length;i++)
        {
            tag = tags[i];
            tag.outerHTML = "";
        }

        tags = letterElement.find("[" + passageTypeAttr + "='" + ptSessionAutoMarkup + "']");
        for (i=0;i<tags.length;i++)
        {
            tag = tags[i];
            tag.outerHTML = tag.innerHTML;
        }

        tags = letterElement.find('span').attr("data-formtype","plus");
        for (i=0;i<tags.length;i++)
        {
            tag = tags[i];
            tag.outerHTML = tag.innerHTML;
        }

        var letterHTML = letterElement.html();
        return letterHTML;
    }

    this.SetText = function(codedText)
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return;
        }
        clearSelection(true);
        maxPassageId = 0;
        var dom=tinymce.get(self.Id).dom;
        dom.select("#" + divId)[0].innerHTML = codedText;
        addSpansToTextArea();
        self.SetViewMode(viewModeId);
    }

    this.GetTextSelection = function()
    {
        return pendingText;
    }

    this.CreateComment = function(commentNumber, isLink)
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return;
        }
        var dom=tinymce.get(self.Id).dom;
        var linkBeforeId = 0;
        var linkAfterId = 0;

        //get the before and after links passage ids if needed
        if (isLink)
        {
            var nodes = dom.select("." + pendingTagHide + "[" + isStartTagAttr + "],." + commentTagShow + "[" + commentIdAttr + "='" + 
                commentNumber + "'][" + isStartTagAttr + "]");
            var found = false;
            for (i=0; i<nodes.length; i++)
            {
                var node = nodes[i];
                if (dom.getAttrib(node, passageTypeAttr, "") != ptPending)
                {
                    if (found)
                    {
                        linkAfterId = dom.getAttrib(node, passageIdAttr, "0");
                        break;
                    }
                    else
                    {
                        linkBeforeId = dom.getAttrib(node, passageIdAttr, "0");
                    }
                }
                else
                {
                    found = true;
                }
            }
        }

        maxPassageId++;
        var attributes = commentIdAttr + "='" + commentNumber + "' " + passageIdAttr + "='" + maxPassageId + "' " + 
            passageTypeAttr + "='" + ptComment + "'";

        //set OuterHTML for start tag
        var outerStartHTML = getSpanTag(commentTagShow, attributes + " " + isStartTagAttr + "='true'") + commentStartTag + commentNumber + "]" + endSpan;

        var outerEndHTML = getSpanTag(commentTagShow, attributes) + commentEndTag + endSpan;

        if (linkBeforeId != 0)
        {
            outerStartHTML = outerStartHTML + getSpanTag(linkTagShow, attributes + linkTitleAttrText) + commentLinkTag + endSpan;
            if (linkAfterId == 0)
            {
                var beforeEndNode = dom.select("." + commentTagShow + "[" + passageIdAttr + "='" + linkBeforeId + "']:not([" + isStartTagAttr + "])")[0];
                var beforeAttributes = passageIdAttr + "='" + linkBeforeId + "' " + passageTypeAttr + "='" + ptComment + "' " +
                    commentIdAttr + "='" + commentNumber + "'" + linkTitleAttrText;
                dom.setOuterHTML(beforeEndNode, getSpanTag(linkTagShow, beforeAttributes) + commentLinkTag + endSpan + dom.getOuterHTML(beforeEndNode));
            }
        }
        if (linkAfterId != 0)
        {
            outerEndHTML = getSpanTag(linkTagShow, attributes + linkTitleAttrText) + commentLinkTag + endSpan + outerEndHTML;
            if (linkBeforeId == 0)
            {
                var afterStartNode = dom.select("." + commentTagShow + "[" + passageIdAttr + "='" + linkAfterId + "'][" + isStartTagAttr + "])")[0];
                var afterAttributes = passageIdAttr + "='" + linkAfterId + "' " + passageTypeAttr + "='" + ptComment + "' " +
                    commentIdAttr + "='" + commentNumber + "'" + linkTitleAttrText;
                dom.setOuterHTML(afterStartNode, dom.getOuterHTML(afterStartNode) + getSpanTag(linkTagShow, afterAttributes) + commentLinkTag + endSpan);
            }
        }

        //set outer html for start and end tags
        var pendingStart = dom.select("." + pendingTagHide + "[" + isStartTagAttr + "]")[0];
        dom.setOuterHTML(pendingStart, outerStartHTML);
        var pendingEnd = dom.select("." + pendingTagHide + ":not([" + isStartTagAttr + "])")[0];
        dom.setOuterHTML(pendingEnd, outerEndHTML);

        //set attributes for all pending text spans
        var textspans = dom.select("." + pendingTextShow);
        var attribs = {"class": commentTextShow, "data-commentid" : commentNumber, "data-passageid" : maxPassageId, "data-passagetype" : ptComment};
        dom.setAttribs(textspans, attribs);

        clearSelection(false);
        setCurrentById(ptPassage, maxPassageId, true);
    }

    this.DeleteComment = function()
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return false;
        }
        if (self.CurrentId == "" || self.CurrentId == self.AutoMarkupId || self.CurrentId == self.PendingCommentId) //make sure a comment is selected
        {
            return false;
        }
        var dom=tinymce.get(self.Id).dom;
        var commentIdToDelete = self.CurrentId;
        move(false, 2);
        while (self.CurrentId == commentIdToDelete)
        {
            move(false, 2);
        }
        dom.remove(dom.select("." + commentTagShow + "[" + commentIdAttr + "='" + commentIdToDelete + "'],." +
            linkTagShow + "[" + commentIdAttr + "='" + commentIdToDelete + "']"), false);
        dom.remove(dom.select("." + commentTextShow + "[" + commentIdAttr + "='" + commentIdToDelete + "']"), true);

        return true;
    }

    this.SetTextSize = function(textSize)
    {
        self.FontSize = textSize;
        var dom=tinymce.get(self.Id).dom;
        dom.setStyle(dom.select("#" + divId)[0],"font-size", textSize);
    }

    this.ScrollToCurrent = function()
    {
        if (currentPassageId)
        {
            var selector = "[" + passageIdAttr + "='" + currentPassageId + "']";
            if (self.CurrentId == self.AutoMarkupId)
            {
                selector = "." + autoMarkupTextCurrent + selector;
            }
            else if (self.CurrentId == self.PendingCommentId)
            {
                selector = "." + pendingTextShow + selector;
            }
            var dom=tinymce.get(self.Id).dom;
            dom.select(selector)[0].scrollIntoView();
        }
    }

    this.CommitPending = function (ed, e)
    {
        var selection = tinyMCE.activeEditor.selection;
        var sel = selection.getSel();
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
        }
        else if (sel.anchorNode != sel.focusNode || sel.anchorOffset != sel.focusOffset)
        {
            newHighlight();
        }
    }

    this.Lock = function()
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
        }
        else
        {
            locked = true;
        }
    }

    this.Unlock = function()
    {
        locked = false;
        lockedMessageDisplayed = false;
    }

    this.IsLocked = function()
    {
        return locked;
    }

    this.DisplayLockedMessage = function()
    {
        if (!lockedMessageDisplayed)
        {
            alertWindow("The system is busy processing a request.  Please wait a moment and try again.");
            lockedMessageDisplayed = true;
        }
    }

    function setup(ed) 
    {
        ed.on("blur", self.CommitPending);
        ed.on("click", onClick);
    }

    function initInstanceCallBack(ed)
    {
        addSpansToTextArea();
        if (self.CurrentId)
        {
            //need goto comment to actually reinitialize coding panel so fake it inot thinking it changed current id.
            var temp = self.CurrentId;
            self.CurrentId = "";
            self.GoToComment(temp);
            self.ScrollToCurrent();
        }
    }

    function onClick(ed, e)
    {
        carotMove();
    }

    function carotMove()
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return;
        }
        //if the move makes a highlight wait until onBlur
        var highlighted = tinyMCE.activeEditor.selection.getContent();
        if (highlighted)
        {
            return;
        }
        //otherwise determine if the carot is in a visible passage
        var node = tinyMCE.activeEditor.selection.getNode();
        var nodeId = idForNode(node, viewModeId);
        if (nodeId != 0)
        {
            if (nodeId != currentPassageId && nodeId != pendingPassageId)
            {
                setCurrentById(ptPassage, nodeId, true);
            }
        }
        else
        {
            clearSelection(true);
        }
    }

    function idForNode(node, viewMode)
    {
        var dom=tinymce.get(self.Id).dom;
        var retVal = 0;
        var parentNode = parentForNode(node, viewMode);
        if (parentNode != null)
        {
            retVal = dom.getAttrib(parentNode, passageIdAttr, 0);
        }
        return retVal;
    }

    function parentForNode(node, viewMode)
    {
        var dom=tinymce.get(self.Id).dom;
        var retVal = null;
        var selector = "";
        switch (viewMode)
        {
            case 1: //automarkup only
                if (dom.getAttrib(node,passageTypeAttr,"")==ptAutoMarkup || 
                    dom.getAttrib(node,passageTypeAttr,"")==ptSessionAutoMarkup)
                {
                    retVal = node;
                }
                selector = "[" + passageTypeAttr + "='" + ptAutoMarkup + "'],[" + passageTypeAttr + "='" + ptSessionAutoMarkup + "']";
                break;
            case 2: 
                if (dom.getAttrib(node,passageTypeAttr,"")==ptComment)
                {
                    retVal = node;
                }
                selector = "[" + passageTypeAttr + "='" + ptComment + "']";
                break;
            case 3: //comments and auto-markups
                if (dom.getAttrib(node,passageTypeAttr,"")!="")
                {
                    retVal = node;
                }
                selector = "[" + passageTypeAttr + "]";
                break;
        }
        if (retVal == null && viewMode != 0)
        {
            var parents = dom.getParents(node, selector, dom.select("#" + divId)[0]);
            if (parents.length > 0)
            {
                retVal = parents[0];
            }
        }
        return retVal;
    }

    function setCurrentById(passageType, id, doUpdate)
    {
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return;
        }
        var oldCommentId = self.CurrentId;
        var retTag= "";
        var retId = ""; 
        clearSelection(false);
        var dom=tinymce.get(self.Id).dom;
        var selector = ""
        var scrollNode = null;
        var scrollNodePT = "";
        if (passageType != ptComment)
        {
            currentPassageId = id;
            selector = "[" + passageIdAttr + "='" + id + "']";
            scrollNode = dom.select(selector + "[" + isStartTagAttr + "]")[0];
            scrollNodePT = dom.getAttrib(scrollNode,passageTypeAttr,"");
            if (scrollNodePT == ptComment)
            {
                retId = dom.getAttrib(scrollNode,commentIdAttr,"0");
                selector = "[" + commentIdAttr + "='" + retId + "']";
            }
            else if (scrollNodePT == ptPending)
            {
                retId = self.PendingCommentId;
            }
            else if (scrollNodePT == ptAutoMarkup || scrollNodePT == ptSessionAutoMarkup)
            {
                retId = self.AutoMarkupId;
            }
        }
        else //id is comment id
        {
            retId = id;
            scrollNodePT = ptComment;
            selector = "[" + commentIdAttr + "='" + id + "']";
            scrollNode = dom.select(selector+ "[" + isStartTagAttr + "]")[0];
            currentPassageId = dom.getAttrib(scrollNode,passageIdAttr,"0");
        }

        if (scrollNodePT == ptComment)
        {
            var commentTagNodes = dom.select("." + commentTagShow + selector);
            var commentTextNodes = dom.select("." + commentTextShow + selector);
            var linkTagNodes = dom.select("." + linkTagShow + selector);
            dom.addClass(commentTagNodes, commentTagCurrent);
            dom.removeClass(commentTagNodes, commentTagShow);
            dom.addClass(commentTextNodes, commentTextCurrent);
            dom.removeClass(commentTextNodes, commentTextShow);
            dom.addClass(linkTagNodes, linkTagCurrent);
            dom.removeClass(linkTagNodes, linkTagShow);
        }
        else if (scrollNodePT == ptAutoMarkup || scrollNodePT == ptSessionAutoMarkup)
        {
            var markupNodes = dom.select("." + autoMarkupTextShow + selector);
            dom.addClass(markupNodes, autoMarkupTextCurrent);
            dom.removeClass(markupNodes, autoMarkupTextShow);
        }

        var startTagNodes = dom.select("[" + passageIdAttr + "='" + currentPassageId + "'][" + isStartTagAttr + "]"); 
        for (i=0; i<startTagNodes.length; i++)
        {
            retTag = retTag + startTagNodes[i].innerHTML;
        }
        retTag = retTag.replace(sessionAutoMarkupStartTag, autoMarkupStartTag);

        self.CurrentId = retId;
        //if the previous passage and the new passage are both comments and part of the same comment, don't update the comment details
        if (oldCommentId == self.CurrentId && oldCommentId != self.AutoMarkupId)
        {
            doUpdate = false;
        }
        if (doUpdate)
        {
            self.OnUpdateCurrent(retTag, retId);
        }
        return retTag;
    }

    function clearSelection(doUpdate)
    {
        if (currentPassageId != 0)
        {
            var dom=tinymce.get(self.Id).dom;
            var commentTagNodes = dom.select("." + commentTagCurrent);
            var commentTextNodes = dom.select("." + commentTextCurrent);
            var linkTagNodes = dom.select("." + linkTagCurrent);
            var markupNodes = dom.select("." + autoMarkupTextCurrent);
            var pendingTagNodes = dom.select("." + pendingTagHide);
            var pendingTextNodes = dom.select("." + pendingTextShow);
            dom.addClass(commentTagNodes, commentTagShow);
            dom.removeClass(commentTagNodes, commentTagCurrent);
            dom.addClass(commentTextNodes, commentTextShow);
            dom.removeClass(commentTextNodes, commentTextCurrent);
            dom.addClass(linkTagNodes, linkTagShow);
            dom.removeClass(linkTagNodes, linkTagCurrent);
            dom.addClass(markupNodes, autoMarkupTextShow);
            dom.removeClass(markupNodes, autoMarkupTextCurrent);
            dom.remove(pendingTagNodes,false);
            dom.remove(pendingTextNodes,true);
            currentPassageId = 0;
            self.CurrentId = "";
            pendingText = "";
            if (doUpdate)
            {
                self.OnUpdateCurrent("","");
            }
        }
    }


    function newHighlight()
    {
        if (viewModeId == 0 || viewModeId == 1)
        {
            return;
        }
        var selection = tinyMCE.get(self.Id).selection;
        var sel = selection.getSel();
        var dom = tinyMCE.get(self.Id).dom;
        var previousSelection = self.CurrentId;

        //add tags at either end of the selection
        var anchor = sel.anchorNode;
        var anchorOffset = sel.anchorOffset;
        var focus = sel.focusNode;
        var focusOffset = sel.focusOffset;
        if (anchor == focus && anchorOffset == focusOffset)
        {
            return;
        }

        //place bookmarks so that selection position is not affected by clearing selection
        var anchorBookmarkHtml = getSpanTag(bookmarkClass, bookmarkTypeAttr + "='" + btAnchor + "'") + "bookmark" + endSpan;
        var anchorBookmarkNode = getElementFromHtml(anchorBookmarkHtml);
        var focusBookmarkHtml = getSpanTag(bookmarkClass, bookmarkTypeAttr + "='" + btFocus + "'") + "bookmark" + endSpan;
        var focusBookmarkNode = getElementFromHtml(focusBookmarkHtml);

        var success = true;
        if (anchor == focus)
        {
            var firstOffset = anchorOffset;
            var lastOffest = focusOffset;
            if (focusOffset < anchorOffset)
            {
                firstOffset = focusOffset;
                lastOffest = anchorOffset;
            }
            //anchor and foucs are the same text node needs different logic because the split text method disrputs the offset for the next insertion
            if (anchor.nodeType == 3) 
            {
                var datastring = "";
                var dataNode = null;
                var textParent = anchor.parentNode;

                //insert all text before the selection, in front of the node containing the selection
                if (firstOffset > 0)
                {
                    datastring = anchor.data.substr(0,firstOffset);
                    dataNode = document.createTextNode(datastring);
                    textParent.insertBefore(dataNode,anchor);
                }

                //insert the bookmark in front of the node containing the selection.  Refresh the bookmark.
                textParent.insertBefore(anchorBookmarkNode,anchor);

                //insert the selection text in front of the node containing the selection
                if (lastOffest > firstOffset)
                {
                    datastring = anchor.data.substr(firstOffset, lastOffest - firstOffset);
                    dataNode = document.createTextNode(datastring);
                    textParent.insertBefore(dataNode, anchor);
                }
                
                //insert the bookmark in front of the node containing the selection.
                textParent.insertBefore(focusBookmarkNode, anchor);

                //insert the remaining text in front of the node containing the selection.
                if (lastOffest < anchor.data.length)
                {
                    datastring = anchor.data.substr(lastOffest, anchor.data.length - lastOffest);
                    dataNode = document.createTextNode(datastring);
                    textParent.insertBefore(dataNode, anchor);
                }

                //remove the node containing the selection
                dom.remove(anchor, false);
            }
            else //anchor and focus are the same but not a text node
            {
                if (anchor.childNodes.length > lastOffest - 1)
                {
                    if (anchor.childNodes.length == lastOffest)
                    {
                        anchor.appendChild(focusBookmarkNode);
                    }
                    else if(anchor.childNodes.length > lastOffset)
                    {
                        anchor.insertBefore(focusBookmarkNode, anchor.childNodes[lastOffset]);
                    }
                    anchor.insertBefore(anchorBookmarkNode, anchor.childNodes[firstOffset]);
                }
                //find text children and add bookmarks to start of first and end of last
                success = insertBookmarkBeforeAndAfter(anchor, anchorBookmarkHtml, focusBookmarkHtml);
            }

        }
        else 
        {
            //if either anchor or focus nodes are not text nodes insert that bookmark first so that the offset stays correct
            if (anchor.nodeType != 3)
            {
                //check to see if cursor is after the last element
                if (anchor.childNodes.length == anchorOffset)
                {
                    anchor.appendChild(anchorBookmarkNode);
                }
                else if (anchor.childNodes.length > anchorOffset)
                {
                    anchor.insertBefore(anchorBookmarkNode, anchor.childNodes[anchorOffset]);
                }
                else
                {
                    //find text children and add bookmarks to start of first and end of last
                    success = insertBookmarkBeforeAndAfter(anchor, anchorBookmarkHtml, anchorBookmarkHtml);
                }
            }
            if (focus.nodeType != 3)
            {
                //check to see if cursor is after the last element
                if (focus.childNodes.length == focusOffset)
                {
                    focus.appendChild(focusBookmarkNode);
                }
                else if (focus.childNodes.length > focusOffset)
                {
                    focus.insertBefore(focusBookmarkNode, focus.childNodes[focusOffset]);
                }
                else
                {
                    //find text children and add bookmarks to start of first and end of last
                    success = success && insertBookmarkBeforeAndAfter(focus, focusBookmarkHtml, focusBookmarkHtml);
                }
            }
            if (anchor.nodeType == 3)
            {
                var newAnchorNode = anchor.splitText(anchorOffset);
                var anchorParent = anchor.parentNode;
                anchorParent.insertBefore(anchorBookmarkNode,newAnchorNode);
            }
            if (focus.nodeType == 3)
            {
                var newFocusNode = focus.splitText(focusOffset);
                var focusParent = focus.parentNode;
                focusParent.insertBefore(focusBookmarkNode,newFocusNode);
            }
        }

        if (!success)
        {
            //remove bookmark tags
            dom.remove(dom.select("." + bookmarkClass), false);
            return;
        }

        //clear the old highlight
        clearSelection(false);
        currentPassageId = pendingPassageId;

        var attributes = passageTypeAttr + "='" + ptPending + "' " + passageIdAttr + "='" + pendingPassageId + "' " 
            + commentIdAttr + "='" + self.PendingCommentId + "'";
        var startPending = getSpanTag(pendingTagHide,attributes + " " + isStartTagAttr + "='true'") + self.PendingTag + endSpan;
        var endPending = getSpanTag(pendingTagHide,attributes) + commentEndTag + endSpan;
                
        var firstBookmark = dom.select("." + bookmarkClass)[0];
        var firstBookmarkType = dom.getAttrib(firstBookmark, bookmarkTypeAttr, "");
        firstBookmark = dom.select("[" + bookmarkTypeAttr + "='" + firstBookmarkType + "']:last")[0];
        dom.setOuterHTML(firstBookmark, startPending);
        var firstPending = dom.select("[" + passageTypeAttr + "='" + ptPending + "']")[0];
        var lastBookmark = dom.select("." + bookmarkClass + "[" + bookmarkTypeAttr + "!='" + firstBookmarkType + "']")[0];
        dom.setOuterHTML(lastBookmark, endPending);
        var lastPending = dom.select("[" + passageTypeAttr + "='" + ptPending + "']:last")[0];
        
        //remove bookmark tags
        dom.remove(dom.select("." + bookmarkClass), false);

        sel.removeAllRanges();
        
        //move pending tags
        var overlapCommentId = "";

        var startAfterComment = parentForNode(dom.getParent(firstPending,"*",dom.select("#" + divId)[0]), 2);
        if (startAfterComment != null)
        {
            if (dom.hasClass(startAfterComment,commentTextShow) || dom.getAttrib(startAfterComment,isStartTagAttr,false))
            {
                overlapCommentId = dom.getAttrib(startAfterComment, commentIdAttr, "Id not found");
            }
        }

        var endBeforeComment = parentForNode(dom.getParent(lastPending,"*",dom.select("#" + divId)[0]), 2);
        if (endBeforeComment != null)
        {
            if (!dom.getAttrib(endBeforeComment,isStartTagAttr,false))
            {
                overlapCommentId = dom.getAttrib(endBeforeComment, commentIdAttr, "Id not found");
            }
        }
        if (overlapCommentId != "")
        {
            //clear selection
            clearSelection(previousSelection != "");
            alertWindow("Your selection overlaps with comment " + overlapCommentId);
            return;
        }

        var startBeforeMarkup = null;
        if (startAfterComment == null)
        {
            startBeforeMarkup = parentForNode(dom.getParent(firstPending,"*",dom.select("#" + divId)[0]), 1);
            if (startBeforeMarkup != null)
            {
                startBeforeMarkup = dom.select("[" + isStartTagAttr + "][" + passageIdAttr + "='" + dom.getAttrib(startBeforeMarkup, passageIdAttr, 0) + "']")[0];
                startBeforeMarkup.parentNode.insertBefore(firstPending, startBeforeMarkup);
            }
        }
        else
        {
            dom.insertAfter(firstPending, startAfterComment);
        }

        var endAfterMarkup = null;
        if (endBeforeComment == null)
        {
            endAfterMarkup = parentForNode(dom.getParent(lastPending,"*",dom.select("#" + divId)[0]), 1);
            if (endAfterMarkup != null)
            {
                endAfterMarkup = dom.select("." + autoMarkupTagHide + "[" + passageIdAttr + "='" + dom.getAttrib(endAfterMarkup, passageIdAttr, 0) + 
                    "']:not([" + isStartTagAttr + "]):last")[0];
                dom.insertAfter(lastPending, endAfterMarkup);
            } 
        }
        else
        {
            endBeforeComment.parentNode.insertBefore(lastPending, endBeforeComment);
        }

        var ancestor = findCommonAncestor(firstPending, lastPending);
        var response = addPendingTextSpans(ancestor, false, attributes);
        overlapCommentId = response.commentId;
        if (overlapCommentId != self.PendingCommentId)
        {
            //clear selection
            clearSelection(previousSelection != "");
            alertWindow("Your selection overlaps with comment " + overlapCommentId);
            return;
        }

        //check if there is any text in the selection
        var selectedText = response.text;
        if (selectedText == "")
        {
            //clear selection
            clearSelection(previousSelection != "");
            return;
        }
        
        pendingText = response.text;
        self.CurrentId = self.PendingCommentId;
        self.OnUpdateCurrent(self.PendingTag, self.PendingCommentId);
    }

    function insertBookmarkBeforeAndAfter(node, firstBookmarkHtml, lastBookmarkHtml)
    {
        var retVal = false;
        var dom = tinymce.get(self.Id).dom;
        var textNodes = getFirstAndLastTextNode(node);
        if (textNodes.first != null && textNodes.last != null)
        {
            var firstBookmarkNode = getElementFromHtml(firstBookmarkHtml);

            var firstTextParent = textNodes.first.parentNode;
            firstTextParent.insertBefore(firstBookmarkNode, textNodes.first);
            
            var lastBookmarkNode = getElementFromHtml(lastBookmarkHtml);

            dom.insertAfter(lastBookmarkNode, textNodes.last);
            retVal = true;
        }
        return retVal;
    }

    function move(forward, viewMode)
    {
        //Get the current annotation by link id or automarkupTextCurrent
        if (self.IsLocked())
        {
            self.DisplayLockedMessage();
            return;
        }
        var dom=tinymce.get(self.Id).dom;
        var nextTagPassageId = 0;
        var selector = "[" + isStartTagAttr + "]"
        var nodes = dom.select(selector);
        var start;
        var step;

        if (forward)
        {
            start = nodes.length - 1
            step = -1;
        }
        else
        {
            start = 0;
            step = 1;
        }
        for (i=start; i<nodes.length && i>-1; i=i+step)
        {
            var nodePassage = dom.getAttrib(nodes[i],passageIdAttr,-2);
            if (nodePassage == currentPassageId)
            {
                break;
            }
            switch (viewMode)
            {
                case 1:
                    if (dom.getAttrib(nodes[i],passageTypeAttr,"") == ptAutoMarkup ||
                        dom.getAttrib(nodes[i],passageTypeAttr,"") == ptSessionAutoMarkup)
                    {
                        nextTagPassageId = nodePassage;
                    }
                    break;
                case 2:
                    if (dom.getAttrib(nodes[i],passageTypeAttr,"") == ptComment)
                    {
                        nextTagPassageId = nodePassage;
                    }
                    break;
                case 3:
                    nextTagPassageId = nodePassage;
                    break;
            }
        }
        if (nextTagPassageId < 1)
        {
            //no comments or annotations after current
            clearSelection(true);
            retVal = "";
        }
        else
        {
            retVal = setCurrentById(ptPassage, nextTagPassageId, true);
            self.ScrollToCurrent();
        }
        return retVal;
    }


    function addCommentSpans(text) {
        // find all comment tags by regex
        var keys = text.match(/\[comment:.*?\].*?\[comment end\]/gi);

        if (keys != null)
        {
            // for each of the items in the keys, insert the default hiding <span> for the annotation
            jQuery.each(keys, function() {
                // find the comment id and add it to each span as an attribute
                var match = this;
                var commentId = match.substring(
                    this.indexOf(commentStartTag) + commentStartTag.length, 
                    this.indexOf("]"));
                maxPassageId = maxPassageId + 1;
                var attributes = commentIdAttr + "='" + commentId + "' " + passageIdAttr + "='" + maxPassageId + "' " +
                    passageTypeAttr + "='" + ptComment + "'";
                // build the html with comment show style, also add the span around the link tags
                var key = match.replace("]", "]" + endSpan);
                key = key.replace(commentEndTag, getSpanTag(commentTagShow, attributes) + commentEndTag + endSpan);
                key = key.replace(commentOpenTag, getSpanTag(commentTagShow, attributes + " " + isStartTagAttr + "='true'") + commentOpenTag);
                key = key.replace(/\[\.\.\.\]/gi, getSpanTag(linkTagShow, attributes + linkTitleAttrText) + commentLinkTag + endSpan);

                // replace the comment with styled comment
                text = text.replace(match, key);
            });
        }

        return text;
    }

    // -------------------------------------------------------------------
    // add the hidden auto-markup tags in the text
    // -------------------------------------------------------------------
    function addMarkupSpans(text)
    {
        // find all auto-markup tags
        var keys = text.match(/\[auto-markup:.*?\].*?\[auto-markup end\]/gi);
        text = findAutoMarkupTags(keys, text);
        keys = text.match(/\[session auto-markup:.*?\].*?\[auto-markup end\]/gi);
        text = findAutoMarkupTags(keys, text);
        return text;
    }

    function findAutoMarkupTags(keys, text)
    {
        if (keys != null)
        {
            // for each of the items in the keys, insert the default show <span> for the annotation
            jQuery.each(keys, function() {
                // create the markup id and add it to each span as an attribute
                maxPassageId = maxPassageId + 1;
                var startTag = autoMarkupStartTag;
                var amPassageType = ptAutoMarkup;
                if (this.indexOf(sessionAutoMarkupStartTag) > -1)
                {
                    startTag = sessionAutoMarkupStartTag;
                    amPassageType = ptSessionAutoMarkup;
                }
                var markupType = this.substring(
                    this.indexOf(startTag) + startTag.length, 
                    this.indexOf("]"));
                // build the html with comment hide style, also add the span around the link tags
                var attributes = passageIdAttr + "='" + maxPassageId + "' " + passageTypeAttr + "='" + amPassageType + "'";
                var key = this
                    .replace("]", "]" + endSpan + getSpanTag(
                        autoMarkupTextShow, attributes + " title='" + markupType + "'" ))
                    .replace(autoMarkupEndTag, 
                        endSpan + getSpanTag(autoMarkupTagHide, attributes) +
                        autoMarkupEndTag + endSpan)
                    .replace(startTag,
                        getSpanTag(autoMarkupTagHide, attributes + " " + isStartTagAttr + "='true'") + startTag);

                // replace the comment with styled comment
                text = text.replace(this, key);
            });
        }
        return text;
    }

    function addPendingTextSpans(element, foundStart, attribs)
    {
        var elementName = element.nodeName.toUpperCase();
        var nodes = element.childNodes;
        var dom = tinymce.get(self.Id).dom;
        var isStarted = foundStart;
        var response = null;
        var liCount = 0;
        var text = "";
        var i = 0;
        for (i = 0; i < nodes.length; i++)
        {
            var node = nodes[i];
            var nodeName = node.nodeName.toUpperCase();
            if (nodeName == "LI")
            {
                liCount++;
            }

            if (!isStarted)
            {
                if (node.nodeType == 3)
                {
                    continue;
                }
                else if (dom.is(node, "[" + passageTypeAttr + "='" + ptPending + "'][" + isStartTagAttr + "]"))
                {
                    isStarted = true;
                    continue;
                }
                else if (node.childNodes.length > 0)
                {
                    response = addPendingTextSpans(node, false, attribs);
                    isStarted = response.started;
                    if (isStarted)
                    {
                        if (nodeName == "LI")
                        {
                            if (elementName == "OL")
                            {
                                text = text + liCount + ". ";
                            }
                            else if (elementName == "UL")
                            {
                                text = text + "* ";
                            }
                        }
                    }
                    text = text + response.text;
                    if (response.finished)
                    {
                        response.text = text;
                        return response;
                    }
                    else if (isStarted)
                    {
                        if (nodeName == "P" || nodeName == "LI" || nodeName == "TR")
                        {
                            text = text + "\r\n";
                        }
                    }
                }
            }
            else //isStarted = true
            {
                if (nodeName == "BR" || nodeName == "P" || nodeName == "TABLE" || nodeName == "OL" || nodeName == "UL")
                {
                    text = text + "\r\n";
                }
                if (nodeName == "LI")
                {
                    if (elementName == "OL")
                    {
                        text = text + liCount + ". ";
                    }
                    else if (elementName == "UL")
                    {
                        text = text + "* ";
                    }
                }

                if (node.nodeType == 3)
                {
                    text = text + node.data;
                    var newNode = getElementFromHtml(getSpanTag(pendingTextShow, attribs) + endSpan);
                    newNode.appendChild(document.createTextNode(node.data));

                    element.insertBefore(newNode,node);
                    element.removeChild(node);
                    node = newNode;
                }
                else if (dom.is(node, "[" + passageTypeAttr + "='" + ptPending + "']"))
                {
                    return {started : true, finished : true, commentId : self.PendingCommentId, text : text};
                }
                else if (dom.is(node, "[" + passageTypeAttr + "='" + ptComment + "']"))
                {
                    return {started : true, finished : true, commentId : dom.getAttrib(node, commentIdAttr, "Comment Id Not Found"), text : ""};
                }
                else if (dom.is(node, "[" + passageTypeAttr + "='" + ptAutoMarkup + "']") ||
                    dom.is(node, "[" + passageTypeAttr + "='" + ptSessionAutoMarkup + "']"))
                {
                    var passageId = dom.getAttrib(node, passageIdAttr, 0);
                    var markupNodes = dom.select("[" + passageIdAttr + "='" + passageId + "']");
                    var spanHtml = getSpanTag(pendingTextShow, attribs) + endSpan;
                    
                    var spanNode = getElementFromHtml(spanHtml);
                    element.insertBefore(spanNode, node);

                    var j = 0;
                    for (j=0; j<markupNodes.length; j++)
                    {
                        var markupNode = markupNodes[j];
                        if (!dom.hasClass(markupNode, autoMarkupTagHide))
                        {
                            text = text + markupNode.innerHTML;
                        }
                        spanNode.appendChild(markupNodes[j]);
                    }
                }
                else if (node.childNodes.length > 0)
                {
                    response = addPendingTextSpans(node, true, attribs);
                    text = text + response.text;
                    if (response.finished)
                    {
                        response.text = text;
                        return response;
                    }
                    else if (nodeName == "P" || nodeName == "LI" || nodeName == "TR")
                    {
                        text = text + "\r\n";
                    }
                }
            }
        }
        return {started : isStarted, finished : false, commentId : "", text : text};
    }

    function addCommentTextSpans(element, inComment, attribs)
    {
        var nodes = element.childNodes;
        var dom = tinymce.get(self.Id).dom;
        var response = null;
        var passageId = 0;
        var i = 0;
        for (i = 0; i < nodes.length; i++)
        {
            var node = nodes[i];
            if (node.nodeType == 3) //text node
            {
                if (inComment)
                {
                    var newNode = getElementFromHtml(getSpanTag(commentTextShow, attribs) + node.data + endSpan);

                    element.insertBefore(newNode,node);
                    element.removeChild(node);
                    node = newNode;
                }
            }
            else if (dom.getAttrib(node, passageTypeAttr, "") == ptComment) //comment tag
            {
                if (dom.getAttrib(node, isStartTagAttr, false)) //comment start tag
                {
                    inComment = true;
                    passageId = dom.getAttrib(node, passageIdAttr, -2);
                    var commentId = dom.getAttrib(node, commentIdAttr, "Comment Id Not Found");
                    attribs = commentIdAttr + "='" + commentId + "' " + passageIdAttr + "='" + passageId + "' " + passageTypeAttr + "='" + ptComment + "'";
                }
                else if (dom.getAttrib(node, "title", "") != linkTitleText) //comment end tag
                {
                    inComment = false;
                    attribs = "";
                }
            }
            else if (dom.getAttrib(node, passageTypeAttr, "") == ptAutoMarkup ||
                dom.getAttrib(node, passageTypeAttr, "") == ptSessionAutoMarkup) //automarkup
            {
                if (inComment)
                {
                    passageId = dom.getAttrib(node, passageIdAttr, 0);
                    var markupNodes = dom.select("[" + passageIdAttr + "='" + passageId + "']");
                    var spanHtml = getSpanTag(commentTextShow, attribs) + endSpan;
                    
                    var spanNode = getElementFromHtml(spanHtml);
                    element.insertBefore(spanNode, node);

                    var j = 0;
                    for (j=0; j<markupNodes.length; j++)
                    {
                        spanNode.appendChild(markupNodes[j]);
                    }
                }
            }
            else if (node.childNodes.length > 0)
            {
                response = addCommentTextSpans(node, inComment, attribs);
                inComment = response.inComment;
                attribs = response.attribs;
            }
        }
        return {inComment : inComment, attribs : attribs};
    }

    function getFirstAndLastTextNode(node)
    {
        var first = null;
        var last = null;
        if (node.nodeType == 3)
        {
            first = node;
            last = node;
        }
        else if (node.childNodes.length > 0)
        {
            var i = 0;
            var childNode = null;
            var response = null;
            for (i=0; i < node.childNodes.length; i++)
            {
                childNode = node.childNodes[i];
                if (childNode.nodeType == 3)
                {
                    first = childNode;
                    break;
                }
                response = getFirstAndLastTextNode(childNode);
                if (response.first != null)
                {
                    first = response.first;
                    break;
                }
            }
            for (i=node.childNodes.length - 1; i > -1; i--)
            {
                childNode = node.childNodes[i];
                if (childNode.nodeType == 3)
                {
                    last = childNode;
                    break;
                }
                response = getFirstAndLastTextNode(childNode);
                if (response.last != null)
                {
                    last = response.last;
                    break;
                }
            }
        }
        return {first : first, last : last};
    }

    function getSpanTag(cssClass, attributes)
    {
        return "<span class='" + cssClass + "' " + attributes + ">";
    }

    function addSpansToTextArea()
    {
        var dom=tinymce.get(self.Id).dom;
        var root = dom.select("#" + divId)[0];
        var newInnerHTML = root.innerHTML;
        //text = "<div id='" + divId + "' style='font-size:" + self.FontSize + "'>" + text + "</div>"
        newInnerHTML = addCommentSpans(newInnerHTML);
        newInnerHTML = addMarkupSpans(newInnerHTML);
        root.innerHTML = newInnerHTML;
        OverwriteAutomarkupFormatting();
        addCommentTextSpans(root, false, "");
    }

    function OverwriteAutomarkupFormatting()
    {
        var dom=tinymce.get(self.Id).dom;
        var tags = dom.select("." + autoMarkupTextShow + " *:not(." + autoMarkupTagHide + ")");
        var i = 0;
        for (i=0;i<tags.length;i++)
        {
            tag = tags[i];
            tag.outerHTML = tag.innerHTML;
        }

    }

    function addRootDiv()
    {
        var textElement = $("#" + self.Id)[0];
        var text = textElement.value;
        text = "<div id='" + divId + "' style='font-size:" + self.FontSize + "'>" + text + "</div>"
        textElement.value = text;
    }

    function findCommonAncestor(element1, element2)
    {
        var dom=tinymce.get(self.Id).dom;
        var root = dom.select("#" + divId)[0];
        var parents1 = dom.getParents(element1, "*", root);
        var parents2 = dom.getParents(element2, "*", root);
        var retVal = root;
        var i = 0;
        var j = 0;
        for (i = 0; i < parents1.length; i++)
        {
            for (j = 0; j < parents2.length; j++)
            {
                if (parents1[i] == parents2[j])
                {
                    retVal = parents1[i];
                    break;
                }
            }
            if (retVal == parents1[i])
            {
                break;
            }
        }
        return retVal;
    }

    function getElementFromHtml(html)
    {
        var tempDiv = document.createElement('div');
        tempDiv.innerHTML = html;
        var retVal = tempDiv.childNodes[0];
        if (tempDiv.childNodes.length > 1)
        {
            retVal = tempDiv;
        }
        return retVal;
    }


}

// -->