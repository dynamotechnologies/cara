﻿<!--
function CaraTinyMce(width, length, id, setup)
{
    var self = this;
    this.Width = width;
    this.Length = length;
    this.Id = id;
    this.Selector = "#" + id;
    this.Setup = setup;
    this.ReadOnly = false;
    this.UsePowerPaste = true;
    //CR-337 conernText made resizable
    this.statusbar = false;
    this.resize = false;

    this.Update = function ()
    {
        tinymce.get(self.Id).save();
    }
    //CR-337 conernText made resizable
    this.InitInstanceCallBack = function (editor)
    {
        $(editor.getContainer()).find(".mce-path").css("visibility", "hidden");
    }

    function ReadOnlySetup (ed)
    {
        ed.on("cut", preventDefault);
        ed.on("paste", preventDefault);
        ed.on("keydown", readOnlyKeyDown);
        self.Setup(ed);
    }

    this.Init = function ()
    {
        tinymce.init({
            selector: "textarea",
            content_css: "/content/tinymceCustomContent.css",
            skin: "lightgraynofonts",
            plugins: self.UsePowerPaste ? ["searchreplace", "powerpaste", "link"] : ["searchreplace", "paste", "link"],
            toolbar: self.ReadOnly ? false : "cut copy paste | searchreplace | bold italic underline removeformat | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link unlink",
            menubar: false,
            //statusbar: true,
            statusbar: self.statusbar,
            width: self.Width,
            height: self.Length,
            //resize: "both", 
            resize:self.resize,           
            browser_spellcheck: true,
            selector: self.Selector,
            powerpaste_word_import: "clean",
            powerpaste_html_import: "clean",
            paste_word_valid_elements: "b,ul,li,ol,strong,table,tbody,tr,td,br,em,i,u,span,h1,h2,strike,sub,sup,p",
            paste_retain_style_properties: "color text-align",
            setup: self.ReadOnly ? ReadOnlySetup : self.Setup,
            init_instance_callback: self.InitInstanceCallBack
        });
    }

    function readOnlyKeyDown (event)
    {
        var allow = event.ctrlKey || event.altKey;
        //allowable keys Tab:9, Esc:27, PageUp:33, PageDown:34, End:35, Home:36
        var allowableKeyCodes = [9,27,33,34,35,36];
        //allowable ranges Arrows:37-40, FKeys:112-135
        var allowableRanges = [{min:37,max:40},{min:112,max:135}]
        var i=0;
        if (!allow)
        {
            for (i = 0; i < allowableKeyCodes.length; i++)
            {
                if (event.keyCode == allowableKeyCodes[i])
                {
                    allow = true;
                    break;
                }
            }
        }
        if (!allow)
        {
            for (i = 0; i < allowableRanges.length; i++)
            {
                if (event.keyCode > allowableRanges[i].min - 1 && event.keyCode < allowableRanges[i].max + 1)
                {
                    allow = true;
                    break;
                }
            }
        }
        if (!allow)
        {
            event.preventDefault();
        }
    }

    function preventDefault (event)
    {
        event.preventDefault();
    }
}

// -->