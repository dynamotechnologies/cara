﻿<!--

function customCloseWindow(refresh, codeId) {
    var tWindow = $("#WindowCodeCreate").data("tWindow");
    if (tWindow != null) {
        tWindow.close();

        // refresh the parent window
        if (refresh) {
            if (codeId != "")
            {
                // append the newly created code id
                checkCodeListAdd(codeId, true);
            }          

            // submit the form
            submitCodeInput("Save");
        }
    }
}

function dropDownList() {
    return $("#PhaseCode_CodeCategoryId").data("tDropDownList");
}

// -------------------------------------------------------------------------------
// set page control values and visibilities based on the current creation mode
// -------------------------------------------------------------------------------
function setPageMode() {
    var isChild = getCheckedChildValue();

    if (isChild == "True")
    {
        // child mode
        // update selected code label
        $("#selectedCodeLabel").text("Parent Code:");
        // disable code category dropdown
        dropDownList().disable();
        // reset code category value
        dropDownList().value($("#CodeCategoryId").val()); 

        // reset code number value
        $("#PhaseCode_CodeNumber").val($("#CodeNumber").val());
    }
    else
    {
        // sibling mode
        // update selected code label
        $("#selectedCodeLabel").text("Selected Code:");
        // enable code category dropdown
        dropDownList().enable();
        
        // populate the next available number based on the selected code category
        getCodeNumber();
    }

    // hide the mode options if the selected code is not a root level code
    if ($("#SelectedCode_CodeNumberDisplay").val() != null && $("#SelectedCode_CodeNumberDisplay").val() != "" &&
            $("#SelectedCode_CodeNumberDisplay").val().length > 3)
    {
        $("#selectMode").hide();
    }

    // set the code input control visibility
    if ($("#PhaseCode_CodeNumber").val() != null && $("#PhaseCode_CodeNumber").val() != "")
    {
        $("#PhaseCode_CodeName").removeAttr("disabled");
        // show the code number textbox
        $("#PhaseCode_CodeNumber").show();
        $("#btnSave").show();
        // display the error message
        $(".error").hide();
    }
    else
    {
        // if code number is not populated, disable the save button action
        $("#PhaseCode_CodeName").attr("disabled","disabled");
        // hide the code number textbox
        $("#PhaseCode_CodeNumber").hide();
        $("#btnSave").hide();
        // display the error message
        $(".error").show();
    }

    // disable the radio butto options if the selected node is a category root node
    if ($("#SelectedCodeId").val() != null && $("#SelectedCodeId").val() != "0")
    {
        $("#rbChild").removeAttr("disabled");
        $("#rbSibling").removeAttr("disabled");
    }
    else
    {
        $("#rbChild").attr("disabled", "disabled");
        $("#rbSibling").attr("disabled", "disabled");
    }
}

// --------------------------------------------------------------------------
// get the next available code number by selected code and its category
// --------------------------------------------------------------------------
function getCodeNumber() {
    var number = "";
    var phaseId = $("#PhaseId").val();
    var codeCategoryId = dropDownList().value();
    var selectedCodeNumber = $("#SelectedCode_CodeNumberDisplay").val();


    if (codeCategoryId != null && codeCategoryId != "")
    {
        // set up parameters
        var parms = {
            phaseId: phaseId,
            codeCategoryId: codeCategoryId,
            selectedCodeNumber: selectedCodeNumber
        };

        // ajax action call to refresh code number
        $.post("/Code/GetNextCodeNumber",
            parms,
            function(data)
            {
                setCodeNumber(data);

                setPageMode();
            },
            "text");
    }
    else
    {
        alertWindow("Please select a Type.");
    }
}

// ----------------------------------------------------------------------
// set the input code number to the code number textbox
// ----------------------------------------------------------------------
function setCodeNumber(number) {
    $("#PhaseCode_CodeNumber").val(number);
}

// ----------------------------------------------------------------------
// get the checked child selection value
// ----------------------------------------------------------------------
function getCheckedChildValue() {
    return $("input[name='Child']:checked").val();
}

// -------------------------
// save the code
// -------------------------
function saveCode() {
    // get creation mode (children or sibling)
    var isChild = getCheckedChildValue();

    var phaseId = $("#PhaseId").val();
    var codeCategoryId = dropDownList().value();
    var selectedCodeId = $("#SelectedCodeId").val();
    var codeNumber = $("#PhaseCode_CodeNumber").val();
    var codeName = $("#PhaseCode_CodeName").val();    

    // validation
    if (codeCategoryId == "")
    {
        alertWindow("Code Type is a required field.");
        return false;
    }

    if (codeNumber == "")
    {
        alertWindow("Code Number is a required field.");
        return false;
    }

    if (codeName == "")
    {
        alertWindow("Code Description is a required field.");
        return false;
    }

    // confirm if there are coded comments for the comment period
    if ($("#HasCodedComments").val() == "False")
    {
        // create the phase code
        createPhaseCode(phaseId, isChild, selectedCodeId, codeCategoryId, codeNumber, codeName);
    }
    else if ($("#HasCodedComments").val() == "True")
    {
        // confirm the action before creating the phase code
        confirmWindow("Comments have already been coded with the coding structure you are about to change. Are you sure you want to change the coding structure",
            "createPhaseCode(" + phaseId + ", \"" + isChild + "\", " + selectedCodeId + ", " + codeCategoryId + ", \"" + codeNumber + "\", \"" + escape(codeName) + "\")");
    }
}

// -----------------------------------------
// create the phase code
// -----------------------------------------
function createPhaseCode(phaseId, isChild, selectedCodeId, codeCategoryId, codeNumber, codeName) {
    // set up parameters
    var parms = {
        phaseId: phaseId,
        selectedCodeId: (isChild == "True" ? selectedCodeId : null),
        codeCategoryId: codeCategoryId,
        codeNumber: codeNumber,
        codeName: unescape(codeName)
    };

    // ajax action call to create the phase code
    $.post("/Phase/CreatePhaseCode",
        parms,
        function(data)
        {
            // close the window
            customCloseWindow(true, data);
        },
        "text");
}

//-->