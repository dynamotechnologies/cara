﻿<!--

// -------------------------------------------------------------------------------------------------------
// Triggers a dummy keystroke to validate the input text and disable the save button if validation fails
// -------------------------------------------------------------------------------------------------------
function validateDocText() {
    var e = jQuery.Event("keyup");
    e.keyCode = 37; // # Some key code value
    $("#ExtractedText").trigger(e);

    return !($("#btnSave").attr("disabled"));
}

//-->