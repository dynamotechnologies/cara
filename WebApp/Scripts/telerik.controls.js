function fixGridIconTitle() {
    // add title to the icons that don't have alt text (for 508 compliant)
    $(".t-refresh").attr("title", "Refresh");
    $(".t-arrow-first").attr("title", "First page");
    $(".t-arrow-prev").attr("title", "Prev page");
    $(".t-arrow-next").attr("title", "Next page");
    $(".t-arrow-last").attr("title", "Last page");
}

function hideInsertImageButton() {
    // hide insert image icon on rich text editor
    $(".t-insertImage").hide();
}

// ---------------------------------------------
// fix telerik control UI issues
// ---------------------------------------------
function fixTelerikControls() {
    fixGridIconTitle();

    hideInsertImageButton();
}