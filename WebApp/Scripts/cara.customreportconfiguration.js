﻿<!--
function TextFieldKeyPress(fieldType)
{
    var x;
    if(window.event) // IE8 and earlier
	{
	    x=event.keyCode;
	}
    else if(event.which) // IE9/Firefox/Chrome/Opera/Safari
	{
	    x=event.which;
	}
    if (x==13)
    {
        AddStringValue(fieldType, 1);
    }
}

function AddStringValue(fieldType, include)
{
    var value = document.getElementById(fieldType+"StringFieldValue").value;
    var dropdown = $("#" + fieldType + "FieldId").data("tDropDownList");
    var name = dropdown.text();
    if (value)
    {
        document.getElementById("FieldId").value = dropdown.value();
        document.getElementById("FieldValue").value = value;
        document.getElementById("FieldValueInclude").value = include;
        document.getElementById("Action").value = "Add";
        document.forms[0].submit();
    }
    else
    {
        alertWindow("Value for \"" + name + "\" is required for filter."); 
    }
}

function AddDateValue(fieldType)
{
    var from = document.getElementById(fieldType + "FromDateFieldValue").value;
    var to = document.getElementById(fieldType + "ToDateFieldValue").value;
    if (from == "" && to == "")
    {
        alertWindow("Please enter a \"from\" and/or \"to\" date");
        return;
    }
    if (!(from == "") && !(to == ""))
    {
        var fromDate = new Date(from);
        var toDate = new Date(to);
        if (toDate < fromDate)
        {
            alertWindow("To date can not be before From date");
            return;
        }
    } 
    document.getElementById("FieldId").value = $("#" + fieldType + "FieldId").data("tDropDownList").value();
    document.getElementById("FieldDateFrom").value = from;
    document.getElementById("FieldDateTo").value = to;
    document.getElementById("FieldValueInclude").value = "true";
    document.getElementById("Action").value = "Add";
    document.forms[0].submit();
}

function AddDropdownValue(fieldType, include)
{
    document.getElementById("FieldId").value = $("#" + fieldType + "FieldId").data("tDropDownList").value();
    var dropdown = $("#" + fieldType + "DropdownFieldValue").data("tDropDownList");
    var value = dropdown.value();
    var fieldName = $("#" + fieldType + "FieldId").data("tDropDownList").text();
    var valueName = dropdown.text();
    if (value != 0)
    {
        document.getElementById("FieldValue").value = value;
        document.getElementById("FieldValueName").value = valueName;
        document.getElementById("FieldValueInclude").value = include;
        document.getElementById("Action").value = "Add";
        document.forms[0].submit();
    }
    else
    {
        alertWindow("Value for \"" + fieldName + "\" is required for filter."); 
    }
}

function AddUnitValue()
{
    var unitId = $("#" + "ProjectFieldId").data("tDropDownList").value();
    document.getElementById("FieldId").value = unitId;
    if (unitId == "")
    { return; }
    document.getElementById("FieldValueInclude").value = "true";
    document.getElementById("Action").value = "Add";
    document.forms[0].submit();
}

function FieldSelect(fieldType)
{
    var value = $("#" + fieldType + "FieldId").data("tDropDownList").value();
    var fieldname = $("#" + fieldType + "FieldId").data("tDropDownList").text();
    document.getElementById(fieldType + "TextFieldDiv").setAttribute("style", "display: none");
    document.getElementById(fieldType + "StringFieldValue").value = "";
    document.getElementById(fieldType + "DropdownFieldDiv").setAttribute("style", "display: none");
    $("#" + fieldType + "DropdownFieldValue").data("tDropDownList").select(0);
    if (fieldType == "Project")
    { document.getElementById("ProjectUnitDiv").setAttribute("style", "display: none"); }
    if (fieldType == "Letter")
    {
        document.getElementById("LetterDateFieldDiv").setAttribute("style", "display: none");
        document.getElementById("LetterFromDateFieldValue").value = "";
        document.getElementById("LetterToDateFieldValue").value = "";
    }

    if (value == 0)
    { return; }

    $.getJSON("/Report/GetFieldMetaData?fieldId=" + value, function(data)
        {
            var dataType = data.dataType;
            var showdiv = "";
            var labelid = "";
            switch (dataType)
            {
                case "dropdown":
                    showdiv="DropdownFieldDiv";
                    labelid = "DropdownFieldLabel";
                    $("#" + fieldType + "DropdownFieldValue").data("tDropDownList").dataBind(data.values);
                    break;
                case "string":
                case "number":
                    showdiv="TextFieldDiv";
                    labelid = "StringFieldLabel";
                    document.getElementById(fieldType + "TextFieldTool").setAttribute("title", data.toolTip);
                    document.getElementById(fieldType + "TextFieldTool").setAttribute("alt",fieldname);
                    break;
                case "date":
                    showdiv="DateFieldDiv";
                    break;
                case "treedropdown":
                    showdiv="UnitDiv";
                    break;
            }
            if (dataType != ""){document.getElementById(fieldType + showdiv).removeAttribute("style");}
            if (labelid != "")
            {
                var labelElement = document.getElementById(fieldType + labelid)
                if (labelElement.textContent == undefined)
                { labelElement.innerText = fieldname + ":"; }
                else
                {labelElement.textContent = fieldname + ":";}
            }
        });
}

function ProjectFieldSelect()
{
    FieldSelect("Project");
}

function LetterFieldSelect()
{
    FieldSelect("Letter");
}

function CommenterFieldSelect()
{
    FieldSelect("Commenter");
}

function UnitSelected()
{
    closeUnitTreeSelector();
    AddUnitValue();
}

function ShowUnitDialog()
{
    document.getElementById("SelectUnitTop").setAttribute("onclick","UnitSelected()");
    document.getElementById("SelectUnitBottom").setAttribute("onclick","UnitSelected()");
    $('#UnitSelectorWindow').data('tWindow').center().open();
}

function RemoveItem(listId)
{
    document.getElementById("ListId").value = listId;
    document.getElementById("Action").value = "Remove";
    document.forms[0].submit();
}

function GetColumns()
{
    var columnGroup = $("#columnGroup").data("tDropDownList")
    $.getJSON("/Report/GetColumns?id=" + columnGroup.value(), function(data)
    {
        $("#columns").data("tDropDownList").dataBind(data);
    });
}

function AddColumn()
{
    document.getElementById("Action").value = "AddColumn";
    document.getElementById("FieldValue").value = $("#columns").data("tDropDownList").value();
    $("#columns").data("tDropDownList").select(0);
    document.forms[0].submit();
}

function MoveUp(listId)
{
    document.getElementById("ListId").value = listId;
    document.getElementById("Action").value = "MoveUp";
    document.forms[0].submit();
}

function MoveDown(listId)
{
    document.getElementById("ListId").value = listId;
    document.getElementById("Action").value = "MoveDown";
    document.forms[0].submit();
}

function RunReport()
{
    document.getElementById("Action").value = "Run Report";
    document.forms[0].submit();
}

function ClearAllValues()
{
    document.getElementById("Action").value = "ClearAllValues";
    document.forms[0].submit();
}

function ClearAllColumns()
{
    document.getElementById("Action").value = "ClearAllColumns";
    document.forms[0].submit();
}
//-->