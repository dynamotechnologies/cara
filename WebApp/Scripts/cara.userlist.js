﻿<!--

function updateUser(userId) {
    // set the checkbox control ids
    var chkSuperUser = "chkRoleSuperUser_" + userId;
    var chkCoordinator = "chkRoleCoordinator_" + userId;
    var roles = "";

    // get the checked values
    var superUser = $('input[name=' + chkSuperUser +']:checked').val();
    var coordinator = $('input[name=' + chkCoordinator +']:checked').val();

    if (superUser != null)
    {
        roles = roles + superUser;
    }

    if (coordinator != null)
    {
        roles = roles + (roles.length > 0 ? ",": "") + coordinator;
    }

    // set up parameters
    var parms = {
        id: userId,
        roles: roles
    };

    // ajax action call to update comment
    $.post("/User/Save",
        parms,
        function(data)
        {
            if (data == "True")
            {
                //alertWindow("Data saved.");
            }
            else
            {
                alertWindow("An error occurred while saving the data.");
            }
        },
        "text");
}

//-->