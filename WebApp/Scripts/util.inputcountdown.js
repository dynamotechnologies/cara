﻿<!--
function Countdown(textareaId, spanId, maxLength)
{
    var self = this;
    this.TextArea = $("#" + textareaId);
    this.Span = $("#" + spanId);
    this.MaxLength = maxLength;

    this.Update = function ()
    {
        var remaining = self.MaxLength - self.TextArea.val().length;

        if (remaining < 0)
        {
            self.Span.text('Exceeded the ' + self.MaxLength + ' character limit by ' + Math.abs(remaining) + ' character(s).');
            self.Span.addClass("error");
        }
        else
        {
            self.Span.text(remaining + ' of ' + self.MaxLength + ' character(s) remaining.');
            self.Span.removeClass("error");
        }
    }

    this.Init = function ()
    {
        self.TextArea.change(self.Update);
        self.TextArea.keyup(self.Update);
        self.Update();
    }
}

// -->