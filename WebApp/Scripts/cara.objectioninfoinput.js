﻿<!--
function ObjectionInfoForm(form, windowName, submitButton, errorDiv, activeTab)
{
    var self = this;
    this.Form = form;
    this.SubmitButton = submitButton;
    this.ErrorDiv = errorDiv;
    this.WindowName = windowName; // optional
    this.ActiveTab = activeTab;

    this.Submit = function ()
    {
        var postData = self.Form.serializeArray();
        var formData = new FormData(self.Form[0]);
        var formURL = self.Form.attr("action");
        var jqhxr =  $.ajax(
        {
            url : formURL,
            type : "POST",
            data : formData,
            contentType : false,
            processData : false,
            success : self.SubmitSuccess
        });
        //.fail(self.SubmitFail);
        //e.preventDefault(); //STOP default action
        //e.unbind(); //unbind. to stop multiple form submit.
    }
    
    this.Init = function ()
    {
        self.SubmitButton.click(self.Submit);
    }

    this.SubmitSuccess = function(data, textStatus, jqxhr) 
    {
        if (data)
        {
            self.ErrorDiv.text("");
            self.ErrorDiv.append("<ul>");
            for (i = 0; i < data.errorMessages.length; i++)
            {
                self.ErrorDiv.append("<li>" + data.errorMessages[i] + "</li>");
            }
            self.ErrorDiv.append("</ul>");
        }
        else
        {
            closeWindow(self.WindowName);
            window.location.href = window.location.pathname + "?tab=" + self.ActiveTab;
        }
    }

    this.SubmitFail = function(jqxhr, textStatus, errorThrown) 
    {
        alertWindow(data.ErrorMessage);
    }
}

// -->