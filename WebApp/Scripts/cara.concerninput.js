﻿<!--

var concernTinyMce = new CaraTinyMce(700, 600, "ConcernText", function(ed) { ed.on("change", handleFormChanged); });
var responseTinyMce = new CaraTinyMce(700, 600, "ResponseText", function(ed) { ed.on("change", handleFormChanged); });
var largeWindowWidth = 350;
var largeWindowHeight = 125;

$(document).ready(function ()
{   
    //CR-337 conernText made resizable    
    concernTinyMce.resize = "both";
    concernTinyMce.statusbar = true;
    concernTinyMce.Init();
    responseTinyMce.Init();   
    
});   

var isChanged=false;
var statusUpdated=false;
var addClickEvent = true;
var openClients = false;

// ------------------------------------------------------------------------
// Code for notifying user if another user takes the CR
// ------------------------------------------------------------------------
$(function ()
{
    var connection = $.connection;
    var hub = connection.signalHub; // the generated client-side hub proxy
    
    // Add a client-side hub method that the server will call
    hub.client.reassignConcern = function (concernResponseId, oldUserId)
    {
        var currentUserId = $("#currentUserId").val();
        var currentCRId = $("#ConcernResponse_ConcernResponseId").val();
        if (currentUserId == oldUserId && currentCRId == concernResponseId)
        {
            doSaveConcern("ForceSave");
        }
    }

    hub.client.sendHeartBeat = function (context, contextId, userId, messageId)
    {
        var currentUserId = $("#currentUserId").val();
        var currentCRId = $("#ConcernResponse_ConcernResponseId").val();
        if (context == "ConcernResponse" && contextId == currentCRId && userId == currentUserId)
        {
            //3 = heartbeat
            //If the server encounters an error this will fail silently
            //This is desired as the user took no action and sees no result of a successful execution
            hub.server.reply(messageId, 3).done(function (response){});
        }
    }

    hub.client.allowDeny = function (context, contextId, userId, fullName, phone, eMail, messageId)
    {
        var currentUserId = $("#currentUserId").val();
        var currentCRId = $("#ConcernResponse_ConcernResponseId").val();
        if (context == "ConcernResponse" && contextId == currentCRId && userId == currentUserId)
        {
            var confirmMessage = fullName +
                " has requested to re-assign this Concern/Response.  Do you want to save your work and grant the request";

            confirmWindowWithCancelCallBack(
                confirmMessage, 
                "AllowCallBack(\""+ messageId+"\")", 
                "DenyCallBack(\""+ messageId+"\")");
        }
    }

    // Start the connection
    $.connection.hub.start().done();

});


//*************REMOVED CARA_5.7*************
// ------------------------------------------------------------------------
// handles the onclick event of the assign to self button
// This is used to add a step where the user is notified once they knew someone was editing
// ------------------------------------------------------------------------
//*************REMOVED CARA_5.7*************


//*************REMOVED CARA_5.7*************
// ------------------------------------------------------------------------
// handles the onclick event of the assign to self button
// ------------------------------------------------------------------------ 
//*************REMOVED CARA_5.7*************



// ------------------------------------------------------------------------
// functions for Allow / Deny dialog call back
// ------------------------------------------------------------------------
function AllowCallBack(messageId)
{
    AllowDenyCallBack(0,messageId);
}
function DenyCallBack(messageId)
{
    AllowDenyCallBack(1,messageId);
}
function AllowDenyCallBack(reply, messageId)
{
    var hub = $.connection.signalHub;
    hub.server.reply(messageId, reply).done(function (response)
    {
        //handle error if denial was sent
        if (response.Content == 2 && reply == 1)
        {
            alertWindow("An error occured while sending denial.  Your current changes will be saved and the Concern Response will be reassigned.<br/>" +
                "Error Information: " + response.Error);
        }
    });
}

// ------------------------------------------------------------------------
// handles when the content in the Concern and Response editors, the assigned user, or the status changes
// ------------------------------------------------------------------------
function handleFormChanged(registerChange) {
    if (typeof registerChange == "undefined")
    {
        registerChange = true;
    }

    // breadcrumb links, top nav links, log out button, add more comments button, left nav links
    if (!isChanged)
    {
        if (addClickEvent)
        {
            $("div.breadcrumb a, #navigation a, #btnLogout, #btnAddComments, #ProjectNav a")
                .click(function () { confirmNavigation(this); return false; });
            addClickEvent = false;
        }

        if (registerChange)
        {
            isChanged = true;
        }
    }
}

// ------------------------------------------------------------------------
// handles when the status changes
// ------------------------------------------------------------------------
function handleStatusChanged() {
    // breadcrumb links, top nav links, log out button, add more comments button, left nav links
    statusUpdated = true;
    handleFormChanged();
}

// ------------------------------------------------------------------------
// opens content changed warning dialog box
// ------------------------------------------------------------------------
function confirmNavigation(link) {
    confirmWindowSize("There may be unsaved changes on this page. If you continue, your changes will be lost. Do you wish to continue", "goNavigation(\"" + link + "\")", "", 300, 80);
}

// ------------------------------------------------------------------------
// executed if "Yes" is clicked in the confirmWindow via callback
// ------------------------------------------------------------------------
function goNavigation(link) {
    window.location.href = link;
    return true;
}


// ------------------------------------------------------------------------
// handles the onclick event of the concern/response input action
// ------------------------------------------------------------------------
function saveConcern(action) {
    switch(action)
    {
        case "Save":
        case "Save and Return":
            if (!statusUpdated && isChanged)
            {
                confirmWindow("Concern/Response data has changed but status was not explicitly set. Do you wish to continue", "doSaveConcern(\"" + action + "\")");
            }
            else
            {
                doSaveConcern(action);
            }
            break;
        case "Delete":
            confirmWindow("Concern/Response will be deleted. Do you wish to continue", "doSaveConcern(\"" + action + "\")");
            break;
        case "AssignToSelf":
            doSaveConcern(action);
            break;
        default :
            doSaveConcern(action);
            break;
            
    }
}

// ------------------------------------------------------------------------
// save concern/response by performing a form submit
// ------------------------------------------------------------------------
function doSaveConcern(action) {
    $("#action").val(action);
    var assignedToOther = $("#assignedToOther").val();
    if (!assignedToOther)
    {
        concernTinyMce.Update();
        responseTinyMce.Update();
    }
    document.forms[0].submit();
}

var selectedNodeElement;
function setSelectedNode(e) {
    var treeview = $('#TreeView').data('tTreeView');
    selectedNodeElement = e.item;
    var phaseCodeId = treeview.getItemValue(selectedNodeElement);

    if (phaseCodeId > 0)
    {
        $('#ucbtnSelectCodeTop').removeAttr('disabled').removeClass('buttonDisabled').addClass('button');
        $('#ucbtnSelectCodeBottom').removeAttr('disabled').removeClass('buttonDisabled').addClass('button');
    }
    else
    {
        $('#ucbtnSelectCodeTop').attr('disabled', true).removeClass('button').addClass('buttonDisabled');
        $('#ucbtnSelectCodeBottom').attr('disabled', true).removeClass('button').addClass('buttonDisabled');
    }
}

// Retrieve the 
function assignCode() {
    var treeview = $('#TreeView').data('tTreeView');
    var phaseCodeId = treeview.getItemValue(selectedNodeElement);
    var codeNameAndDescription = treeview.getItemText(selectedNodeElement);

    // Assign values on the ConcernInput form
    $('#ConcernResponse_PhaseCodeId').val(phaseCodeId);
    $('#CodeNumberAndDescription').val(codeNameAndDescription);
    $('#spnCodeNumber').text(codeNameAndDescription);

    handleFormChanged();
    $('#ModalWindow').data('tWindow').close();
}
//-->