﻿<!--

var htmlTextArea = new CaraTinyMce("100%", "100%", "Letter_HtmlText", function(ed){ed.on("change", htmlTextAreaOnChange);});
var newTextArea = new CaraTinyMce("100%", "100%", "NewText", function(ed){ed.on("change", newTextAreaOnChange);});
var confirmationMessage = "If you select &quot;Yes&quot;, the version of this letter in the Public Reading Room will be replaced with this version (text formatting will not be displayed in the Public Reading Room). Do you want to continue";

// ------------------------------------------------------------------------
// handles the onchange event of the htmlTextArea
// ------------------------------------------------------------------------
function htmlTextAreaOnChange(event)
{
    $("#Changed").val("True");
    stripMailTo(htmlTextArea.Id);
}

// ------------------------------------------------------------------------
// handles the onchange event of the newTextArea
// ------------------------------------------------------------------------
function newTextAreaOnChange(event)
{
    stripMailTo(newTextArea.Id);
}

function stripMailTo(id)
{
    var dom = tinymce.get(id).dom;
    var mailToNodes = dom.select("a[href^='mailto:']");
    dom.remove(mailToNodes,true);
}

// ------------------------------------------------------------------------
// handles the onchange event of the submitted date element
// ------------------------------------------------------------------------
function DateChange(){
    //get the date value
    var date = $('#Letter_DateSubmitted').data('tDatePicker').value();
    var commentStart = new Date(document.getElementById("Phase_CommentStart").value);
    var commentEnd = new Date(document.getElementById("Phase_CommentEnd").value);
    var within = $('#WithinCommentPeriod').data('tDropDownList')
    if (date < commentStart || date > commentEnd)
    {
        //set to no and enable
        within.select(0);
        within.enable();
    }
    else
    {
        //set to yes and disable
        within.select(1);
        within.disable();
    }
}

// ------------------------------------------------------------------------
// handles the onclick event of the edit letter action
// ------------------------------------------------------------------------
function updateLetter() {
    if ($('#WithinCommentPeriod').data('tDropDownList').value() == "")
    {
        alertWindow("Please select if the letter is within the Comment Period");
    }
    else
    {
        confirmWindowSize(confirmationMessage, "doUpdateLetter(true)", "doUpdateLetter(false)", 300, 85);
    }
}

// ------------------------------------------------------------------------
// edit letter by performing a form submit
// ------------------------------------------------------------------------
function doUpdateLetter(repostToDmd) {
    var dmdId = $('#Letter_DmdId').val();
    if (dmdId)
    {
        $('#RepostToDmd').val(repostToDmd);
    }
    else
    {
        $('#RepostToDmd').val(false);
    }
    htmlTextArea.Update();
    if (!isOriginalTextEditable())
    {
        newTextArea.Update();
    }
    document.forms[0].submit();
}

function revertLetterWarning() {
    return confirmWindow(
        "The letter will be reverted back to its original text. All comments associated with this letter will be lost. Do you wish to continue",
        "doRevert()");
}

function doRevert(repostToDmd) {
    if (typeof(repostToDmd) === "undefined")
    {
        return confirmWindowSize(confirmationMessage, "doRevert(true)", "doRevert(false)", 300, 85);
    }

    var dmdId = $('#Letter_DmdId').val();
    var doRepost = false;
    if (dmdId && repostToDmd)
    {
        doRepost = true;
    }

    // Redirect to the revert action in the letter controller
    location.href = $("#btnRevert").attr("href") + "?RepostToPRR=" + doRepost
}

function deleteAuthor(projectId, phaseId, letterId, authorId) {
    /*var url = location.href.split("Edit");
    url = url[0] + letterId + "/Author/Delete?authorId=" + authorId;*/
    var url = "/Project/" + projectId + "/Phase/" + phaseId + "/Letter/" + letterId + "/Author/Delete?authorId=" + authorId;
    location.href = url;
}

// ------------------------------------------------------------------------
// initialize controls
// ------------------------------------------------------------------------
$(document).ready(function () {
    if (!isOriginalTextEditable())
    {
        htmlTextArea.ReadOnly = true;
        newTextArea.Init();
    }

    htmlTextArea.Init();

});


// ------------------------------------------------------------------------
// gets the readonly setting
// ------------------------------------------------------------------------
function isOriginalTextEditable()
{
    var retVal = $("#OriginalTextEditable").val()
    return retVal.toUpperCase() == "TRUE";
}

//-->
