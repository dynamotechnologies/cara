﻿function validateCommentForm () {
	$(".validation-summary-errors-client").empty(); //empty any errors already exists
	$(".validation-summary-errors").empty(); //empty any errors already exists
	
	var firstName = document.getElementById('Author_FirstName').value;
	var lastName = document.getElementById('Author_LastName').value;
	var commentText = document.getElementById('Letter_Text').value;
	var attachment = document.getElementById('FileUpload1').value;
	var contactMethod = document.getElementById('Author_ContactMethod').value;
	var emailAddress = document.getElementById('Author_Email').value;
	var address1 = document.getElementById('Author_Address1').value;
	var city = document.getElementById('Author_City').value;
	var zipCode = document.getElementById('Author_ZipPostal').value;
	var state = document.getElementById('Author_StateId').value;
	var orgType = document.getElementById('OrganizationCheck').checked;
	var orgName = document.getElementById('OrganizationName').value;
	
	var errorCaptured = false;
	var errorList = '';
	
	if (commentText === '' && attachment === '' ) {
		errorCaptured = true;
		errorList = errorList + "<li>You cannot submit the form that has no comment text and no attachments.</li>";
	}
	
	if (contactMethod === 'Email') {
		if (emailAddress === '' ) {
			errorCaptured = true;
			errorList = errorList + "<li>Email List Error Message:  If your preferred contact method is \"Email\" you must provide a valid Email address.</li>";
		} else {
			if (! validateEmail(emailAddress)) {
				errorList = errorList + "<li>Email List Error Message:  If your preferred contact method is \"Email\" you must provide a valid Email address.</li>";
			}
		}
	}	
	
	
	if (contactMethod === 'Mail') {
		if (firstName === ''
			|| lastName === ''
			|| city === '' 
			|| zipCode === '' 
			|| address1 === ''
			|| state === '') {
			errorCaptured = true; 
			errorList = errorList + "<li>Mailing List Error Message:  Subscribing to the mailing list requires a Full Name and Address 1, City, State and Zip/Postal Code.  Please enter a name and address to be added to the mailing list.</li>";
		}	
	}

	if (orgType === true && orgName === '') {
		errorCaptured = true;
		errorList = errorList + "<li>Organization Error Message:  \"Organization Name\" is required if an \"Organization Type\" is selected.</li>";		
	}
	
	if (! validateCaptcha()) {
		errorCaptured = true;
		errorList = errorList + "<li>You did not enter the reCAPTCHA correctly. Please try again.</li>";	
	}
	
	if (errorCaptured) {
		$(".validation-summary-errors-client").append("<ul>" + errorList + "</ul>");
		return false;
	} 
	return true;
}

function validateCaptcha() {
	var captchaEntered = document.getElementById("commentInput").elements["g-recaptcha-response"].value; //get the form first and then the captcha server response
	if (captchaEntered === '') {
		return false;
	} else {
		return true;
	}
}

function validateEmail(email) {
    var emailRegExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegExpression.test(email);
}

function validateCommentFormsp() {
    $(".validation-summary-errors-client").empty(); //empty any errors already exists
    $(".validation-summary-errors").empty(); //empty any errors already exists

    var firstName = document.getElementById('Author_FirstName').value;
    var lastName = document.getElementById('Author_LastName').value;
    var commentText = document.getElementById('Letter_Text').value;
    var attachment = document.getElementById('FileUpload1').value;
    var contactMethod = document.getElementById('Author_ContactMethod').value;
    var emailAddress = document.getElementById('Author_Email').value;
    var address1 = document.getElementById('Author_Address1').value;
    var city = document.getElementById('Author_City').value;
    var zipCode = document.getElementById('Author_ZipPostal').value;
    var state = document.getElementById('Author_StateId').value;
    var orgType = document.getElementById('OrganizationCheck').checked;
    var orgName = document.getElementById('OrganizationName').value;

    var errorCaptured = false;
    var errorList = '';

    if (commentText === '' && attachment === '') {
        errorCaptured = true;
        errorList = errorList + "<li>No puede enviar el formulario sin texto de comentario ni adjuntos.</li>";
    }

    if (contactMethod === 'Email') {
        if (emailAddress === '') {
            errorCaptured = true;
            errorList = errorList + "<li>Mensaje de error de la lista de correo electrónico: Si el método de contacto preferido es \"Correo electrónico \", debe proporcionar una dirección de correo electrónico válida.</li>";
        } else {
            if (!validateEmailsp(emailAddress)) {
                errorList = errorList + "<li>Mensaje de error de la lista de correo electrónico: Si el método de contacto preferido es \"Correo electrónico \", debe proporcionar una dirección de correo electrónico válida.</li>";
            }
        }
    }


    if (contactMethod === 'Mail') {
        if (firstName === ''
			|| lastName === ''
			|| city === ''
			|| zipCode === ''
			|| address1 === ''
			|| state === '') {
            errorCaptured = true;
            errorList = errorList + "<li>Mensaje de error de la lista de correo: La suscripción a la lista de correo requiere un nombre completo y dirección 1, ciudad, estado y código postal. Por favor ingrese un nombre y dirección para ser agregado a la lista de correo.</li>";
        }
    }

    if (orgType === true && orgName === '') {
        errorCaptured = true;
        errorList = errorList + "<li>Mensaje de error de la organización: \"Nombre de la organización  \" es necesario si está seleccionado \"Tipo de organización \".</li>";
    }

    if (!validateCaptchasp()) {
        errorCaptured = true;
        errorList = errorList + "<li>No ha introducido correctamente el reCAPTCHA. Vuelve a intentarlo.</li>";
    }

    if (errorCaptured) {
        $(".validation-summary-errors-client").append("<ul>" + errorList + "</ul>");
        return false;
    }
    return true;
}

function validateCaptchasp() {
    var captchaEntered = document.getElementById("commentInput").elements["g-recaptcha-response"].value; //get the form first and then the captcha server response
    if (captchaEntered === '') {
        return false;
    } else {
        return true;
    }
}

function validateEmailsp(email) {
    var emailRegExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegExpression.test(email);
}