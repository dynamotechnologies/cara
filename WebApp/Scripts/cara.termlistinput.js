﻿<!--

var ActionTypes = { "Default": "", "Finish": "Finish" };
var action = ActionTypes.Finish;

// -------------------------------------------------------------
// handles the keypress event of the document
// -------------------------------------------------------------
$(document).keypress(function(e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if (action == ActionTypes.Finish)
        {
            $("#btn" + action).focus();
            return true;
        }
    }
});

// -------------------------------------------------------------
// handles the checkbox click event
// -------------------------------------------------------------
function onClickCheckbox(showMsg) {
    var result = true;

    if (showMsg)
    {
        result = false;
        alertWindow("This Auto-markup selection cannot be changed.");
    }

    return result;
}

//-->