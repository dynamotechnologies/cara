﻿<!--

function setUrl(chk, key) {
    // get the checkbox control id and split into elements
    var nameParts = chk.name.split('_');

    // set the checkbox control ids
    var chkSuperUser = "chkRoleSuperUser_" + nameParts[1];
    var chkCoordinator = "chkRoleCoordinator_" + nameParts[1];

    // get the checked values
    var superUser = $('input[name=' + chkSuperUser +']').is(':checked');
    var coordinator = $('input[name=' + chkCoordinator +']').is(':checked');

    // search and set the base url
    var searchKey = new String(key + "/" + nameParts[1]);
    var newUrl = new String($("a#btnUpdate_" + nameParts[1])[0]);
    var queryString = "?roles=";

    // trim the roles query string if present
    newUrl = newUrl.substring(0, newUrl.indexOf(searchKey) + searchKey.length);
    
    // append the checked roles as query string
    if (superUser)
    {
        queryString = queryString + "SuperUser";
    }

    if (coordinator)
    {
        if (!queryString.match(/=$/))
        {
            queryString = queryString + ",";
        }
        queryString = queryString + "Coordinator";
    }

    // set the url action
    $("a#btnUpdate_" + nameParts[1]).attr("href", newUrl + queryString);
}

//-->