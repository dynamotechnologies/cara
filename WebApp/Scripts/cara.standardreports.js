﻿<!--

var expandAllText = "Expand All";
var collapseAllText = "Collapse All";

$(document).ready(function () {
    $("table p").css("margin-top","0px");
    $("table p").css("margin-bottom","20px");
});

function exportReport(url) {
 
 var expType = $('#ExportOptions').val();
 
 if (expType == '')  {
    alertWindow('Select an export format');
    window.location.href = "#";
  }
  else {
    url = url.replace('~',expType);
    window.location.href = url;
   }
}

function reportDisplay(url) {
 
 var displayType = $('#DisplayOptions').val();

 if (displayType == "0")  {
    alert('Select report display option');
    window.location.href = "#";
  }
  else {
    url = url.replace('~',displayType);
    window.location.href = url;
   }
}


function onDataBound(e) {
    
    if ($("#btnExpand").text() == collapseAllText)
    {
        $("#btnExpand").text(expandAllText);
        onClickExpandAll($("#btnExpand"));
    }
}

function onError(e)
{
    alertWindow('Sorting is unavailable for this report');
}



// ------------------------------------------------------------
// Handles on click event of the "expand all" grid command
// ------------------------------------------------------------
function onClickExpandAll(button) {
    var text = $(button).text();
    var expand = false;

    // replace button text
    if (text == expandAllText)
    {
        $(button).text(collapseAllText);
        expand = true;
    }
    else
    {
        $(button).text(expandAllText);
    }

    var grid = $("#Grid").data("tGrid");
  
   if (grid != null)
    {
        $("#Grid").find('tr.t-grouping-row').each(function() {
            expand ? grid.expandGroup($(this)) : grid.collapseGroup($(this));
        });
        
         $("#Grid").find('tr.t-detail-row').each(function() {
            expand ? grid.expandGroup($(this)) : grid.collapseGroup($(this));
        });
      
    }

   
}




//-->