﻿<!--

function closeEditWindow(refresh) {
    var tWindow = $("#WindowCodeEdit").data("tWindow");
    if (tWindow != null) {
        tWindow.close();

        // refresh the parent window
        if (refresh) {
            // submit the form (to save any checked codes)
            document.forms[0].submit();
        }
    }
}

// -------------------------------------------------------------------------------
// set page control values and visibilities based on the current mode
// -------------------------------------------------------------------------------
function setEditPageMode() {
    // disable code category dropdown
    $("#PhaseCode_CodeCategoryId").attr("disabled","disabled");
}

// -------------------------
// update an existing code
// -------------------------
function updateCode() {
    var phaseCodeId = $("#PhaseCode_PhaseCodeId").val();
    var codeName = $("#PhaseCode_CodeName").val();
    var phaseId = $("#PhaseId").val();

    if (codeName == "")
    {
        alertWindow("Code Description is a required field.");
        return false;
    }
    
    // set up parameters
    var parms = {
        phaseCodeId: phaseCodeId,
        codeName: codeName,
        phaseId: phaseId
    };

    // ajax action call to update the phase code
    $.post("/Phase/UpdatePhaseCode",
        parms,
        function(data)
        {
            // close the window
            closeEditWindow(true);
        },
        "text");
}

//-->