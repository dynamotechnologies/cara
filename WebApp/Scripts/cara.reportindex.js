﻿<!--

function onSelect(e) {
    var item = $(e.item);
    // set the selected tab value
    $("#SelectedTab").val(item.find('> .t-link').text());
}

// -------------------------------------------------------
// update the alternate checkbox of the input checkbox
// -------------------------------------------------------
function updateCheckboxValue(checkbox) {
    var altIdChar = "1";
    var altId = "";
    var id = $(checkbox).attr("id");

    if (id != null && id != "")
    {
        // set the alternate control id based on the input checkbox id
        if (id.substring(id.length - 1, id.length) == altIdChar)
        {
            altId = id.substring(0, id.length - 1);
        }
        else
        {
            altId = id + altIdChar;
        }

        // get checked value
        var checked = $(checkbox).is(':checked');

        // set checked value of the alternate control
        $("#" + altId).attr("checked", checked);
    }
}

//-->