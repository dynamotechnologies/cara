﻿<!--

// regular expression for valid code number input
var regex = new RegExp(/(^\d{3}\*{0,1}$)|(^\d{3}\.\d{1,4}\*{0,1}$)|(^\d{3}\.\d{4}\.\d{1,2}\*{0,1}$)/);
// form element ids
var eIdCodeSearchText = "#CodeSearchText";
var eIdCodeRange = "#codeRange";
var eIdCodeRangeFromId = "#codeRangeFrom";
var eIdCodeRangeToId = "#codeRangeTo";
var eIdCodeNameId = "#codeName";
var eIdCodeNumberId = "#codeNumber";
// max range count
var MAX_RANGE_COUNT = 20;
// flag indicates caller is letter coding page
var isLetterCoding = (window.location.pathname.indexOf("Letter/Coding") > 0);

// ---------------------------------------------------------
// initialize the code range input togglings
// ---------------------------------------------------------
function parseFilterText() {
    // get the code search text from the textbox field
    var filterText = $(eIdCodeSearchText).val();
    var rangeCount = 0;

    if (filterText != null && filterText != "")
    {
        // split the text into elements
        var elements = filterText.split(',');

        for(var i = 0; i < elements.length; i++)
        {
            // ----------------------------------------------------------------------
            // inspect the element
            // 1. if it does not contain numbers, it's a description field
            // 2. if it does not contain dashes, it's a single code search
            // 3. if it contains dashes, it's a code range search
            // ----------------------------------------------------------------------
            var trimmedElement = jQuery.trim(elements[i]);
            var codeNumber = trimmedElement.match(regex);

            //if (codeNumber != null && codeNumber.length > 0)
            {
                if (trimmedElement.indexOf('-') > 0)
                {
                    // code range search
                    var keys = trimmedElement.split('-');

                    if (keys != null && keys.length > 0)
                    {
                        // increment the range count
                        rangeCount++;

                        // show the area at index
                        $(eIdCodeRange + rangeCount).show();
                        
                        // set the values at index
                        $(eIdCodeRangeFromId + rangeCount).val(jQuery.trim(keys[0]));
                        $(eIdCodeRangeToId + rangeCount).val(jQuery.trim(keys[1]));
                    }
                }
                else if (codeNumber != null && codeNumber.length > 0)
                {
                    // single code search
                    $(eIdCodeNumberId).val(trimmedElement);
                }
            else
            {
                // description                    
                $(eIdCodeNameId).val(trimmedElement);
            }
            }
        }
    }
}

// ---------------------------------------------------------
// clear all settings and restore the default layout
// ---------------------------------------------------------
function clearFilter() {
    // clear the code desc and number values
    $(eIdCodeNameId).val("");
    $(eIdCodeNumberId).val("");
    
    for (var i = 1; i <= MAX_RANGE_COUNT; i++)
    {
        var codeRangeId = eIdCodeRange + i;

        // if the area exists
        if ($(codeRangeId).length > 0)
        {
            if (i > 1)
            {
                $(codeRangeId).hide();
            }
            else
            {
                $(codeRangeId).show();
            }

            // clear the range values
            $(eIdCodeRangeFromId + i).val("");
            $(eIdCodeRangeToId + i).val("");
        }
    }
}

// ---------------------------------------------------------
// initialize the code range input togglings
// ---------------------------------------------------------
function initialize() {
    // set Ok button text based on page
    if (isLetterCoding)
    {
        $("#btnSave").attr("value","Find");
    }

    for (var i = 1; i <= MAX_RANGE_COUNT; i++)
    {
        var codeRangeId = eIdCodeRange + i;

        // if the area exists
        if ($(codeRangeId).length > 0)
        {
            var showId = "#show" + i;
            var hideId = "#hide" + i;

            // hide the upload control
            if (i > 1) {
                $(codeRangeId).hide();
            }

            // add click event for "-" button
            if ($(hideId).length > 0) {
                $(hideId).click(function (event) {
                    event.preventDefault();

                    // set file upload control id
                    var id = "#" + event.target.id.replace("hide", "codeRange");

                    // clear the range from/to values
                    var fromId = id.replace("codeRange", "codeRangeFrom");
                    var toId = id.replace("codeRange", "codeRangeTo");

                    $(fromId).val("");
                    $(toId).val("");
                    
                    // clear the selection
                    nid = $(id);
                    nid.replaceWith(nid.clone(true));
                    // hide the control
                    $(id).hide();
                });
            }

            // add click event for "+" button
            if ($(showId).length > 0) {
                $(showId).click(function (event) {
                    // parse the number and find the next index
                    var i = parseInt(event.target.id.replace("show", "")) + 1;

                    event.preventDefault();
                    $(eIdCodeRange + i).show();
                });
            }
        }
    }

    // parse the filter text from the page
    parseFilterText();
}

// -------------------------------------------------------------------------
// get the user input filter values and generate the search code criteria
// -------------------------------------------------------------------------
function saveFilter() {
    // get the inputs
    var codeNumber = $(eIdCodeNumberId).val();
    var codeName = $(eIdCodeNameId).val();
    var codeRange = "";
    var codeSearchText = "";

    // set code name if value is present
    if (codeName != null && codeName != "")
    {
        codeSearchText = codeSearchText + (codeSearchText.length > 0 ? ", " : "") + codeName;
    }

    // code number validation and assignment
    if (codeNumber != null && codeNumber != "")
    {
        var keys = codeNumber.match(regex);
    
        // code number
        if (keys == null || (keys != null && keys[0] != codeNumber))
        {
            alertWindow("Code Number value must be numeric (e.g. 110 or 110*).");
            return false; 
        }
        else
        {
            // set code number if value is present
            codeSearchText = codeSearchText + (codeSearchText.length > 0 ? ", " : "") + codeNumber;
        }
    }

    // code range
     for (var i = 1; i <= MAX_RANGE_COUNT; i++)
     {
        var codeRangeId = eIdCodeRange + i;

        // if the area exists
        if ($(codeRangeId).length > 0)
        {
            var codeRangeFrom = $(eIdCodeRangeFromId + i).val();
            var codeRangeTo = $(eIdCodeRangeToId + i).val();

            if (codeRangeFrom != "" && codeRangeTo != "")
            {
                var keysFrom = codeRangeFrom.match(regex);
                var keysTo = codeRangeTo.match(regex);

                if (!(keysFrom == null || (keysFrom != null && keysFrom[0] != codeRangeFrom)) &&
                    !(keysTo == null || (keysTo != null && keysTo[0] != codeRangeTo)))
                {
                    //if (codeRangeTo >= codeRangeFrom)
                    //{
                        // set code number range if value is present
                        codeSearchText = codeSearchText + (codeSearchText.length > 0 ? ", " : "") + codeRangeFrom + " - " + codeRangeTo;
                    //}
                    //else
                    //{
                    //    alertWindow("Code Number Range To value must be greater than Code Number Range From value. (e.g. 110 - 120)");
                    //    return false;
                    //}
                }
                else
                {
                    alertWindow("Code Number Range values must be numeric. (e.g. 110 or 110*)");
                    return false;
                }
            }
            else if ((codeRangeFrom == "" && codeRangeTo != "") || (codeRangeFrom != "" && codeRangeTo == ""))
            {
                alertWindow("You must specify a value for both Code Number Range From and To.");
                return false;
            }
        }
    }

    // send the code search text to the textbox field
    $(eIdCodeSearchText).val(codeSearchText);

    // if call from letter coding page, also call search codes method to populate the code tree
    if (isLetterCoding)
    {
        searchCodes();
    }

    // close window
    closeWindow("WindowCodeFilter");
}

//-->