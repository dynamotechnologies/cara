﻿<!--

var htmlTextArea = new CaraTinyMce("100%", "100%", "Letter_HtmlText", setup);

$(document).ready(function ()
{
        // initialize controls
        init();
});

// ------------------------------------------------------------------------
// handles the initializtion of controls
// ------------------------------------------------------------------------
function init()
{
    FileUploadInit();
    htmlTextArea.Init();
}

// ------------------------------------------------------------------------
// Setup function for TinyMce
// ------------------------------------------------------------------------
function setup(ed)
{
    //need to strip out mail to links due to automarkup limitations
    ed.on("change",stripMailTo);
}

function stripMailTo(event)
{
    var dom = tinymce.get(htmlTextArea.Id).dom;
    var mailToNodes = dom.select("a[href^='mailto:']");
    dom.remove(mailToNodes,true);
}

// ------------------------------------------------------------------------
// handles the onclick event of the create new letter action
// ------------------------------------------------------------------------
function createLetter(action) {
    //confirmWindow("Letter will be submitted. Do you wish to continue", "doCreateLetter(\"" + action + "\")");
    doCreateLetter(action);
 }

// ------------------------------------------------------------------------
// create a new letter by performing a form submit
// ------------------------------------------------------------------------
function doCreateLetter(action) {
    $("#action").val(action);
    htmlTextArea.Update();
    document.forms[0].submit();
}

// ------------------------------------------------------------------------
// handles the email validation oo the create new letter action
// ------------------------------------------------------------------------

function validateEmail(){    
   var a = $("#Author_Email").val();    
   var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;     
   
   if(filter.test(a)){
       return true;     
      }     
   else{
      alert('The Email field must contain a valid email address');
      return false;     
      } 
 }

//-->
