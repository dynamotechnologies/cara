﻿<!--

var ActionTypes = { "Default": "", "SearchUsers": "SearchUsers", "SearchProjects" : "SearchProjects" };
var action = ActionTypes.Default;

// -------------------------------------------------------------
// handles the keypress event of the text inputs
// -------------------------------------------------------------
$(document).ready(function() {
    $("#txtFirstName").keypress(function(e) {
        action = ActionTypes.SearchUsers;
        return true;
    });

    $("#txtLastName").keypress(function(e) {
        action = ActionTypes.SearchUsers;
        return true;
    });

    $("#txtProjectNameOrId").keypress(function(e) {
        action = ActionTypes.SearchProjects;
        return true;
    });
});

// -------------------------------------------------------------
// handles the keypress event of the document
// -------------------------------------------------------------
$(document).keypress(function(e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if (action == ActionTypes.SearchUsers || action == ActionTypes.SearchProjects)
        {
            $("#btn" + action).focus();
            return true;
        }
    }
});

// ---------------------------------------------------------
// show and hide the adding member panels
// ---------------------------------------------------------
function showHide(radio) {
    var selected = radio.value;

    if (selected == "Add") {
        $("#addArea")[0].style.display = 'block';
        $("#importArea")[0].style.display = 'none';
        // set action type
        action = ActionTypes.SearchUsers;
    }
    else if (selected == "Reuse") {
        $("#addArea")[0].style.display = 'none';
        $("#importArea")[0].style.display = 'block';
        // set action type
        action = ActionTypes.SearchProjects;
    }
    else {
        $("#addArea")[0].style.display = 'none';
        $("#importArea")[0].style.display = 'none';
        // set action type
        action = ActionTypes.Default;
    }
}

// ---------------------------------------------------------
// update phase member status or role
// ---------------------------------------------------------
function updateMember(phaseId, userId, action) {
    var parms = null;
    var updateAction = "UpdateMember";

    if (action == "Role")
    {
        displayAction = "role updated";
        // get the checkbox control id
        var rbRole = "rbRole_" + userId;

        // get the checked value
        var role = $('input[name=' + rbRole +']:checked').val();

        // set up parameters and action name
        parms = {
            phaseId: phaseId,
            userId: userId,
            action: action,
            roleName: role
        };
    }
    else
    {
        // set up parameters and action name
        parms = {
            phaseId: phaseId,
            userId: userId,
            action: action,
            roleName: ""
        };
    }

    // ajax action call to update member
    $.post("/Phase/" + updateAction,
        parms,
        function(data)
        {
            $("#memberListArea").html(data);
            UpdateMessage("Team Member Updated");
        },
        "html");
}

// ---------------------------------------------------------
// delete an existing member from the phase
// ---------------------------------------------------------
function deleteMember(phaseId, userId) {
    // set up parameters
    var parms = {
        phaseId: phaseId,
        userId: userId
    };

    // ajax action call to update comment
    $.post("/Phase/DeleteMember",
        parms,
        function(data)
        {
            $("#memberListArea").html(data);
            UpdateMessage("Team Member Removed");
        },
        "html");
}

// ---------------------------------------------------------
// delete an existing member from the phase
// ---------------------------------------------------------
function makePointOfContact(phaseId, userId) {
    // set up parameters
    var parms = {
        phaseId: phaseId,
        userId: userId
    };

    // ajax action call to update comment
    $.post("/Phase/ChangePointOfContact",
        parms,
        function(data)
        {
            $("#memberListArea").html(data);
            UpdateMessage("Team Member made Point Of Contact");
        },
        "html");
}

// ---------------------------------------------------------
// Change Early-Attention newsletter subscription for user
// ---------------------------------------------------------
function changeEarlyAttentionNewsletterSubscription(phaseId, userId, currentSubscribeSetting) {
 
    // set up parameters
    var parms = {
        phaseId: phaseId,
        userId: userId,
        currentSubscribeSetting: currentSubscribeSetting
    };
 
    // ajax action call to update comment
    $.post("/Phase/ChangeEarlyAttentionNewsletterSubscription",
        parms,
        function(data)
        {
            $("#memberListArea").html(data);
            UpdateMessage("Early-Attention Newsletter subscription status changed");
        },
        "html");
}
//-->