﻿<!--

// --------------------------------------------------------------------------------
// the submit button needs to be a regular link button so that it can be disabled
// prior to submitting the form
// --------------------------------------------------------------------------------
function submitForm() {
    $("#btnSubmit").text("Please wait...");
    document.forms[0].submit();
    $("#btnSubmit").attr("disabled", "disabled");
}

window.onload = function() {
    window.history.replaceState("", "Upload Letters", location.href.split("?")[0]);
}

$(document).ready(function() {
    $("#File").click(function() {
        $(".message").hide();
    });
});
//-->