﻿<!--

// -------------------------------------------------------------
// Hide the report option grid header display
// -------------------------------------------------------------
function hideHeader(e) {
    for (var i = 1; i <= 20; i++) {
        $("#ReportOptions_" + i + " .t-header").hide();
    }
}

// -------------------------------------------------------------
// Remove the expand and minus icons from the grid control
// -------------------------------------------------------------
function hideExpandIcons(e) {
    $('td.t-hierarchy-cell a.t-plus').hide();
    $('td.t-hierarchy-cell a.t-minus').hide();    
}

//-->