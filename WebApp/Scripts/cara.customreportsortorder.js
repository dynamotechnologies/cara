﻿<!--
function UpdateSortOrder()
{
    var collapsed = document.getElementById("CollapsedListIds").value;
    var column1 = $("#SortColumn1").data("tDropDownList").value();
    var column2 = -1;
    if (!isHidden("Column2Div"))
    {column2=$("#SortColumn2").data("tDropDownList").value();}
    var column3 = -1;
    if (!isHidden("Column3Div"))
    {column3=$("#SortColumn3").data("tDropDownList").value();}
    var asc1 = $("#Order1").data("tDropDownList").value();
    var asc2 = $("#Order2").data("tDropDownList").value();
    var asc3 = $("#Order3").data("tDropDownList").value();
    
    $.ajax({
      dataType: "json",
      url: "/Report/UpdateSortOrder",
      data: {
        column1:column1,
        column2:column2,
        column3:column3,
        asc1:asc1,
        asc2:asc2,
        asc3:asc3,
        collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
        }});
}

function GetSelectListValue(selectListName)
{
    var selectListOptions = document.getElementById(selectListName).options;
    return selectListOptions[selectListOptions.selectedIndex].value;
}

function isHidden (divName)
{
    var element = document.getElementById(divName);
    var hasStyle = element.hasAttribute("style");
    if (!hasStyle){return false;}
    var style = element.getAttribute("style");
    if (style.indexOf("display: none") > -1){return true;}
    return false;
}
//-->