﻿<!--

// ------------------------------------------------------------------------
// handles the onclick event of the remove document action
// ------------------------------------------------------------------------
function unassociateDocument(url) {
    confirmWindow("Remove this document", "doUnassociateDocument(\"" + url + "\")");
}

// ------------------------------------------------------------------------
// remove document by redirecting to the input url
// ------------------------------------------------------------------------
function doUnassociateDocument(url) {
     window.location.href = url;
}

//-->