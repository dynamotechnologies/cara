﻿<!--

function saveMyReport() {
    var name = $("#Name").val();
    var isPrivate = $("input[name='Private']:checked").val();
    var reportId = $("#myReportId").val();

    // validate
    if (name == null || name == "")
    {
        alertWindow("Name is a required field.");
        return false;
    }

    // set up parameters
    var parms = {
        name: name,
        isPrivate: isPrivate,
        reportId: reportId
    };

    // ajax action call to populate comment input area
    $.post("/Report/SaveMyReport", 
        parms,
        function(data)
        {
            closeWindow("WindowMyReport");
        },
        "text");
}

//-->