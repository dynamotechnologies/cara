﻿<!--

// variables
var cssUpdated = "updated";
var textBox = null;
var blinkCount = 3 * 2; // two blinks, 3*2 for three blinks
var expandAllText = "Expand All";
var collapseAllText = "Collapse All";

$(document).ready(function () {
        // get the updated concern response id
        var updatedId = $("#Updated").val();

        //set up navigation confirmation
        $("div.breadcrumb a, #navigation a, #btnLogout, #ProjectNav a, a.button")
            .click(function ()
            {
                if ($("div.t-pager a").length > 0 && $("div.t-pager a:first").attr("href").search("TaskAssignmentContext=")>=0)
                {
                    confirmNavigation(this);
                    return false;
                }
                else
                {
                    return true;
                }
            });

        // if the value if found, search for the sequence textbox
        if (updatedId != "")
        {
            var textBox = $("#txtSequence" + updatedId);
            
            if (textBox != null)
            {
                // add the updated css class
                textBox.addClass(cssUpdated);
                
                // blink the textbox
                do
                {
                   $("." + cssUpdated)["fade" + (blinkCount % 2 == 0 ? "Out" : "In")]("slow");
                }
                while (--blinkCount);
            }
        }
    });

// ------------------------------------------------------------------------
// opens content changed warning dialog box
// ------------------------------------------------------------------------
function confirmNavigation(link) {
    confirmWindowSize("There may be unsaved changes on this page. If you continue, your changes will be lost. Do you wish to continue", "goNavigation(\"" + link + "\")", "", 300, 80);
}

// ------------------------------------------------------------------------
// executed if "Yes" is clicked in the confirmWindow via callback
// ------------------------------------------------------------------------
function goNavigation(link) {
    window.location.href = link;
    return true;
}

function onDataBound(e) {
    if ($("#btnExpand").text() == collapseAllText)
    {
        $("#btnExpand").text(expandAllText);
        onClickExpandAll($("#btnExpand"));
    }
}

// ------------------------------------------------------------
// Handles on click event of the "expand all" grid command
// ------------------------------------------------------------
function onClickExpandAll(button) {
    var text = $(button).text();
    var expand = false;

    if (text == expandAllText)
    {
        $(button).text(collapseAllText);
        expand = true;
    }
    else
    {
        $(button).text(expandAllText);
    }

    var grid = $("#List").data("tGrid");

    if (grid != null)
    {
        grid.$rows().each(function ()
        {
            if (expand)
            {
                grid.expandRow(this);
            }
            else
            {
                grid.collapseRow(this);
            }
        });
    }
}

// -------------------------------------------------------
// Submits the grid values
// -------------------------------------------------------
function submitSequenceNumbers()
{
    // Create a new form
    $("#contentArea").append("<form id=\"Sequences\" />");
    //var sequenceForm = $("#Sequences");
    $("#Sequences").attr("method", "post");
    $("#Sequences").attr("action", "");
    $("#Sequences").attr("style", "visibility:hidden");
    $("#Sequences").append("<input name=\"action\" value=\"Update\" />");
    var contextIds = document.getElementById("TaskAssignmentContext").value;
    var userIds = document.getElementById("TaskAssignmentUsers").value;
    $("#Sequences").append("<input name=\"TaskAssignmentContext\" value=\"" + contextIds + "\" />");
    $("#Sequences").append("<input name=\"TaskAssignmentUsers\" value=\"" + userIds + "\" />");
    $("input[name^='txtSequence']").each(function(index)
        {
            $(this).clone().appendTo($("#Sequences"));
        });
    $("#Sequences").submit();
}
//-->