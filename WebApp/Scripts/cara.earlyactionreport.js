﻿<!--

var expandAllText = "Expand All";
var collapseAllText = "Collapse All";


function exportReport(url) {
 
 var expType = $('#ExportOptions').val();

 if (expType == '')  {
    alert('Select an export format');
    window.location.href = "#";
  }
  else {
    url = url.replace('~',expType);
    window.location.href = url;
   }
}


function onDataBound(e) 
{
    
    if ($("#btnExpand").text() == collapseAllText)
    {
        $("#btnExpand").text(expandAllText);
        onClickExpandAll($("#btnExpand"));
    }
 }

function onError(e)
{
   
}



// ------------------------------------------------------------
// Handles on click event of the "expand all" grid command
// ------------------------------------------------------------
function onClickExpandAll(button) {
    var text = $(button).text();
    var expand = false;

    // replace button text
    if (text == expandAllText)
    {
        $(button).text(collapseAllText);
        expand = true;
    }
    else
    {
        $(button).text(expandAllText);
    }

    var grid = $("#Grid").data("tGrid");
  
   if (grid != null)
    {
        $("#Grid").find('tr.t-grouping-row').each(function() {
            expand ? grid.expandGroup($(this)) : grid.collapseGroup($(this));
        });
        
         $("#Grid").find('tr.t-detail-row').each(function() {
            expand ? grid.expandGroup($(this)) : grid.collapseGroup($(this));
        });

       
      
    }

    if (grid != null)
    {
        grid.$rows().each(function () {
            if (expand)
            {
                grid.expandRow(this);
            }
            else
            {
                grid.collapseRow(this);
            }
        });
    }
}

// This fixes the problem reported in CARA-875 (rows with details not collapsing properly within a group)
$(document).ready(function() {
    $('td.t-detail-cell').each(function (i) { $(this).addClass('t-group-cell'); });
});


//-->