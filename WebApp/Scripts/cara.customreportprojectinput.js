﻿<!--
function SearchInputKeyPress()
{
    var x;
    if(window.event) // IE8 and earlier
	{
	    x=event.keyCode;
	}
    else if(event.which) // IE9/Firefox/Chrome/Opera/Safari
	{
	    x=event.which;
	}
    if (x==13)
    {
        SubmitForm("Search");
    }
}
function ProjectOptionClick(selection)
{
    //alert("start ProjectOption("+selection+")");
    if (selection == "CommentPeriod")
    {
        document.getElementById("CommentPeriodFilter").removeAttribute("style");
        document.getElementById("CrossProjectFilter").setAttribute("style","display: none");
    }
    else if (selection == "CrossProject")
    {
        document.getElementById("CommentPeriodFilter").setAttribute("style","display: none");
        document.getElementById("CrossProjectFilter").removeAttribute("style");
    }
    else
    {
        document.getElementById("CommentPeriodFilter").setAttribute("style","display: none");
        document.getElementById("CrossProjectFilter").setAttribute("style","display: none");
    }
    //alert("finish ProjectOption("+selection+")");
}
function SelectProject(projectId)
{
    //find all the phase check boxes  
    for(i = 0; i<document.forms[0].elements.length; i++)
    {
        var element = document.forms[0].elements[i];
        var idArr = element.id.split("-");
        if (idArr[0] == "Phase")
        {
            //if the phase is associated with the selected project enable the checkbox
            if (idArr[2] == projectId.toString())
            {
                element.disabled = false;
                element.checked = true;
            }
            //otherwise disable and uncheck the checkbox
            else
            {
                element.setAttribute("disabled", "true");
                element.checked = false;
            }
        }
    }
}
function SubmitForm(action)
{
    document.getElementById("Action").value = action;
    var projectOptionElements = document.getElementsByName("ProjectOption");
    var radProject = false;
    var radFilter = false;
    var radAll = false;
    var currentStep = document.getElementById("WizardStep").value;
    for (i=0;i<projectOptionElements.length; i++)
    {
        var currentElement = projectOptionElements[i];
        if (currentElement.value == "CommentPeriod")
        {radProject = currentElement.checked;}
        else if (currentElement.value == "CrossProject")
        {radFilter = currentElement.checked;}
        else if (currentElement.value == "AllProjects")
        {radAll = currentElement.checked;}
    }
    if (radFilter)
    {
        ChangeStep(currentStep+1);
        return;
    }
    var first = true;
    var selectedPhaseIds = "";
    if (radProject)
    {
        for (j=0; j<document.forms[0].elements.length; j++)
        {
            var element = document.forms[0].elements[j];
            var idArr = element.id.split("-");
            if (idArr[0] == "Phase")
            {
                if (element.checked)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        selectedPhaseIds = selectedPhaseIds + "|";
                    }
                    selectedPhaseIds = selectedPhaseIds + idArr[1];
                }
            }
        }
        if (first)
        {
            // CARA-1092 Calling AddCommentPeriods first to clear any previously selected
            // comment periods from the server-side custom report filter
            $.ajax({
                type: "POST",
                url: "/Report/AddCommentPeriods/",
                data: {PhaseIdList:"|"},
                cache: false,
                success: 
                    function (data)
                    {
                        document.forms[0].submit();
                    }});
            return;
        }
    }
    $.ajax({
        type: "POST",
        url: "/Report/AddCommentPeriods/",
        data: {PhaseIdList:selectedPhaseIds},
        cache: false,
        success: 
            function (data)
            {
                if (action == "Next")
                {ChangeStep(currentStep+1);}
                else
                {document.forms[0].submit();}
            }});
}
//-->