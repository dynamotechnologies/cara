﻿<!--

//variables for testing only
var testCommentId = 0;
var noInfo = "No Information";

// tag constants
var commentOpenTag = "[comment:";
var autoMarkupOpenTag = "[auto-markup:";

// css style name constants for all tags
var commentIdName = "commentid";

var noTagMsg = "No Add'l Tag Found";
var pendingCommentMsg = "Pending Comment";
var noComment = "No Comment Selected";
var NotNeededStatus = "Not Needed";

// constants
var codingWindowHeight = 400;
var maxCodeCount = 10;
var maxIssueActionCodeCount = 10;
var codingWindowOffset = 250;
var minCodingAreaWidth = 275;

var codingLetterText = new CaraLetterText("100%", 265, "Letter_HtmlCodedText");


$(document).ready(function () {
        // initialize controls
        init();


        // Set Link to Master button visibility
        enableCompareMasterButton();

        // initialize the term list tree view
        $("#termListTree").treeview({
			        animated: "medium",
			        control: "#sidetreecontrol",
			        prerendered: true,
			        persist: "location"
		        });
                
        // Font size change event
        $("#ddlFontSize").change(function () {
            setLetterFontSize(getFontSize());
        });
        
        // Text mode change event
        $("#ddlTextMode").change(function () {
            var value = getTextMode();
            
            // set the button visibility
            if (value == 0)
            {
                enableCommentButtons(false);
                setCurrentCommentText("");
            }
            else
            {
                if (codingLetterText.GetViewMode() == 0)
                {
                    setCurrentCommentText("No Comment Selected");
                }
                enableCommentButtons(true);
            }

            // Set Text Mode
            codingLetterText.SetViewMode(value);
        });
    });

// -------------------------------------------------------------------
// Handles page keypress event
// -------------------------------------------------------------------
$(document).keypress(function(e) {    
    if (e.ctrlKey && e.shiftKey)
    {
        switch (e.keyCode)
        {
            case (1):
                // "a" is pressed, focus on coding area
                $("#CodeSearchText").focus();
                return false;
                break;
            case (12):
                // "l" is pressed, focus on comment area
                //TODO: set focus on the letter coding area
                //$("#commentArea").focus();
                return false;
                break;
        }
    }
});

// -------------------------------------------------------------------
// initialize the controls on the page
// -------------------------------------------------------------------
function init() {
    // set the font size if default letter font size is specified
    setFontSize();
    codingLetterText.FontSize=getFontSize()+"px";
    
    // get url params from the url
    var urlParams = getUrlVars();
    // parse the comment number from the url
    if (urlParams != null && urlParams.length > 0 && urlParams["commentId"])
    {
        codingLetterText.CurrentId = urlParams["commentId"];
    }
    
    codingLetterText.OnUpdateCurrent = updateCurrent;
    codingLetterText.Init();

    // set current comment display text
    setCurrentCommentText("No Comment Selected");
}

// -------------------------------------------------------------------
// set the current comment label text with input text
// -------------------------------------------------------------------
function setCurrentCommentText(text) {
    // fix browser issue of html() not working on positioned element in Chrome
    if (jQuery.uaMatch(navigator.userAgent).browser == "webkit")
    {
        $("#currentComment").hide();
        $("#currentComment").html(text);
        $("#currentComment").show(100);
    }
    else
    {
        $("#currentComment").html(text);
    }
}

function updateCurrent(currentTag, currentId)
{
    if (getTextMode() != 0)
    {
        if (currentId == codingLetterText.PendingCommentId)
        {
            setCurrentCommentText(pendingCommentMsg);
            setCommentInputAreaHtml(pendingCommentMsg);
        }
        else if (currentId == codingLetterText.AutoMarkupId)
        {
            setCurrentCommentText(currentTag);
            setCommentInputAreaHtml(noComment);
        }

        else if (currentId)
        {
            setCurrentCommentText(currentTag);
            setCommentInputArea(currentId);
        }
        else
        {
            setCurrentCommentText(noComment);
            setCommentInputAreaHtml(noComment);
        }
    }
}

// ---------------------------------------------------------------
// set the comment area focus to the current index
// ---------------------------------------------------------------
function setCommentFocus() {
    codingLetterText.ScrollToCurrent();
}

function getCommentNumberFromDisplay()
{
    var retVal = codingLetterText.CurrentId;
    if (retVal == codingLetterText.AutoMarkupId)
    {
        retVal = "";
    }
    return retVal;
}

// -------------------------------------------------------------------
// get the current font size
// -------------------------------------------------------------------
function getFontSize() {
    return $.trim($("#ddlFontSize").val());
}

// -------------------------------------------------------------------
// set the font size of the comment area
// -------------------------------------------------------------------
function setLetterFontSize(value) {
    //TODO: set letter font size
    //$("#commentArea").css("font-size", value.toString() + "px");
    codingLetterText.SetTextSize(value.toString() + "px");
}

// -------------------------------------------------------------------
// set the letter font size of by the user default value
// -------------------------------------------------------------------
function setFontSize() {
    if ($("LetterFontSize").val() != "")
    {
        $("#ddlFontSize").val($("#LetterFontSize").val());
    }
}

// -------------------------------------------------------------------
// set the current text view mode
// -------------------------------------------------------------------
function getTextMode()
{
    var strValue = $.trim($("#ddlTextMode").val());
    var retVal = 3;
    switch (strValue)
    {
        case "Markup":
            retVal = 1;
            break;
        case "Comment":
            retVal = 2;
            break;
        case "CommentMarkup":
            retVal = 3;
            break;
        case "None":
            retVal = 0;
            break;
    }
    return retVal;
}

// -------------------------------------------------------------------
// get the no response reason dropdown
// -------------------------------------------------------------------
function getDdlResponseReason() {
    return $("#Comment_NoResponseReasonId").data("tDropDownList");
}


// -------------------------------------------------------------------
// set the compare to master button visibility
// -------------------------------------------------------------------
function enableCompareMasterButton() {
    $("#btnCompareMaster").hide();

    if ($("input#Letter_LetterTypeId").val() != "3")
    {
        $("#btnCompareMaster").show();
    }
}

// -------------------------------------------------------------------
// set the comment button visibility
// -------------------------------------------------------------------
function enableCommentButtons(value) {
    if (value)
    {
        showPrevCommentButtons(true);
        showNextCommentButtons(true);
    }
    else
    {
        showPrevCommentButtons(false);
        showNextCommentButtons(false);
    }
}

// --------------------------------------------------
// set the previous comment buttons visibility
// --------------------------------------------------
function showPrevCommentButtons(show) {
    if (show)
    {
        $("#btnPrevComment").show();
    }
    else
    {
        $("#btnPrevComment").hide();
    }
}

// --------------------------------------------------
// set the next comment buttons visibility
// --------------------------------------------------
function showNextCommentButtons(show) {
    if (show)
    {
        $("#btnNextComment").show();
    }
    else
    {
        $("#btnNextComment").hide();
    }
}

// -------------------------------------------------------------------
// set the current comment to be the next comment
// -------------------------------------------------------------------
function nextComment()
{
    var tag = codingLetterText.NextComment(getTextMode());
    if (!tag)
    {  
        setCurrentCommentText(noTagMsg);
    }
}

function nextCommentOnly()
{
    var tag = codingLetterText.NextComment(2);
    if (!tag)
    {  
        setCommentInputArea("");
    }
}

// -------------------------------------------------------------------
// set the current comment to be the next comment
// -------------------------------------------------------------------
function prevComment()
{
    var tag = codingLetterText.PrevComment(getTextMode());
    if (!tag)
    {  
        setCurrentCommentText(noTagMsg);
    }
}

function prevCommentOnly()
{
    var tag = codingLetterText.PrevComment(2);
    if (!tag)
    {  
        setCommentInputArea("");
    }
}

// -----------------------------------------------------------------------
// load current comment data by the current comment display text
// -----------------------------------------------------------------------
function loadCurrentCommentData() {
    // parse the current comment number from the comment tag
    var commentNumber = getCommentNumberFromDisplay();

    // set the current comment data
    if (commentNumber != null && commentNumber != "")
    {
        // load the comment into the div
        setCommentInputArea(commentNumber);
    }
    else
    {
        // load an empty comment
        setCommentInputArea("");
    }

    return commentNumber;
}

// ---------------------------------------------------------------
// ajax post to load comment input area div with comment data
// ---------------------------------------------------------------
function setCommentInputArea(commentNumber) {
    if (commentNumber != null && commentNumber != "")
    {
        // set up parameters
        var parms = { 
            phaseId: $("#PhaseId").val(),
            letterCommentNumber: commentNumber
        }; 

        // ajax action call to populate comment input area
        $.post("/Letter/CommentInput", 
            parms,
            function(data)
            {
                setCommentInputAreaHtml(data);
                setCommentAreaSize();
            },
            "html");
    }
    else
    {
        setCommentInputAreaHtml(noComment);
    }
}

// -------------------------------------------------------------------
// set the comment area html with input text
// -------------------------------------------------------------------
function setCommentInputAreaHtml(text) {
    $("#commentInputArea").html(text);
}

// -------------------------------------------------------------
// return the current screen mode ("Full Screen" or "Restore")
// -------------------------------------------------------------
function getScreenMode() {
    return ($("#imgScreenSize").attr("src").indexOf("fullscreen") > 0 ? "Full Screen" : "Restore");
}

// -------------------------------------------------------------
// set the screen mode ("Full Screen" or "Restore")
// -------------------------------------------------------------
function setScreenMode(mode) {
    var id = "#imgScreenSize";
     
    if (mode == "Full Screen")
    {
        // set image properties
        var imgSrc = $(id).attr("src").replace("fullscreen", "restore");
        $(id).attr("src", imgSrc);
        $(id).attr("alt", "Restore");
        $(id).attr("title", "Restore");
        
        $("#displayContainer").removeClass("codingContainer").addClass("fullCodingContainer");
        //TODO change next line for new comment area
        //$("#commentArea").removeClass("commentArea").addClass("fullCommentArea");
        $("#attachments").hide();
        $("#subTitle").show();

        fixCommentAreaHeight();        
        $(window).resize(fixCommentAreaHeight);
    }
    else if (mode == "Restore")
    {
        // set image properties
        var imgSrc = $(id).attr("src").replace("restore", "fullscreen");
        $(id).attr("src", imgSrc);
        $(id).attr("alt", "Full Screen");
        $(id).attr("title", "Full Screen");

        $("#displayContainer").removeClass("fullCodingContainer").addClass("codingContainer");
        //TODO change match new control name
        
        codingLetterText.SetWidthHeight("","265px");
        $("#attachments").show();
        $("#subTitle").hide();

        $(window).unbind("resize");
    }
}

function fixCommentAreaHeight()
{
    var height = $("#displayContainer").height() - (
        100 + // buffer
        $("#subTitle").height() +
        $("#toolbar").height() +
        $("#actionArea").height() +
        $("#codingLegend").height());

    codingLetterText.SetWidthHeight("",height);

}

// -------------------------------------------------------------
// set the letter coding window size (Full Screen/Restore)
// -------------------------------------------------------------
function setWindowSize() {
    setScreenMode(getScreenMode());
}

// ----------------------------------------------------------------------
// resize the comment area control sizes based on current screen mode
// ----------------------------------------------------------------------
function setCommentAreaSize() {
    if (getScreenMode() == "Restore")
    {
         $("#Comment_Annotation").width(375);
    }
    else
    {
         $("#Comment_Annotation").width(175);
    }
}


// ---------------------------------------------------------------
// search phase codes by keyword
// ---------------------------------------------------------------
function searchCodes() {
    setCodeListArea($("#CodeSearchText").val());
}

// ---------------------------------------------------------------
// empty the code search textbox and show all phase codes
// ---------------------------------------------------------------
function showAllCodes() {
    $("#CodeSearchText").val("");

    setCodeListArea("");
}

// ---------------------------------------------------------------
// ajax post to load code list area div with phase code data
// ---------------------------------------------------------------
function setCodeListArea(searchCodeText) {
    // set up parameters
    var parms = {
        phaseId: $("#PhaseId").val(),
        searchCodeText: searchCodeText
    }; 

    // ajax action call to populate comment input area
    $.post("/Letter/SearchCode", 
        parms,
        function(data) { $("#codeListArea").html(data); },
        "html");
}

// ---------------------------------------------------------------
// ajax post to delete comment code
// ---------------------------------------------------------------
function deleteCommentCode(isLast, commentId, phaseCodeId) {
    if (isLast == "False")
    {
        // set up parameters (pass phaseId as a request param for priv checking)
        var parms = {
            commentId: commentId,
            phaseCodeId: phaseCodeId,
            phaseId: $("#PhaseId").val()
        };

        // ajax action call to populate comment input area
        $.post("/Letter/DeleteCommentCode", 
            parms,
            function(data) { setCommentInputAreaHtml(data); UpdateMessage("Comment Code Removed");},
            "html");
    }
    else
    {
        // if the comment code is last, delete the entire comment
        deleteComment(commentId);
    }
}


// ---------------------------------------------------------------
// add a new comment codes to a new comment or existing comment
// ---------------------------------------------------------------
function addCodes(phaseId, letterId) {
    //make sure pending selection has been committed
    codingLetterText.CommitPending(null,null);
    var treeview = $("#TreeView").data("tTreeView");
    var checkedCodes = $("#TreeView :checked").closest(".t-item");
    var checkedCodeList = "";
    var codeCount = 0;
    var issueActionCodeCount = 0;

    // basic validation
    if (checkedCodes != null && checkedCodes.length > 0)
    {
        // convert the checked codes into a comma separated list of ids
        $.each(checkedCodes, function(index, code) {
            checkedCodeList = checkedCodeList + (checkedCodeList != "" ? "," : "") + treeview.getItemValue(checkedCodes[index]);

            // increment the I/A code count if the text begins with "1"
            issueActionCodeCount = issueActionCodeCount + (treeview.getItemText(checkedCodes[index]).substring(0, 1) == "1" ? 1 : 0);
        });

        // set the code count
        codeCount = codeCount + checkedCodes.length;
    }
    else
    {
        alertWindow("Please select a code.");
        return false;
    }

    // validation against the number of codes added
    // find out the code count of an existing comment
    if ($("#commentCodeCount").val() != null)
    {
        codeCount = codeCount + parseInt($("#commentCodeCount").val());
    }

    // find out the I/A code count of an existing comment
    if ($("#issueActionCodeCount").val() != null)
    {
        issueActionCodeCount = issueActionCodeCount + parseInt($("#issueActionCodeCount").val());
    }

    if (codeCount > maxCodeCount)
    {
        alertWindow("You have exceeded the limit of " + maxCodeCount + " codes for each comment.");
        return false;
    }

    //CARA-847:-Allow users to add multiple issue/action codes
//    if (issueActionCodeCount > maxIssueActionCodeCount)
//    {
//        alertWindow("You have exceeded the limit of " + maxIssueActionCodeCount + " Issue or Action code for each comment.");
//        return false;
//    }    

    // add a new comment
    var currentCommentNumber = codingLetterText.CurrentId;
    if (currentCommentNumber)
    {
        if (currentCommentNumber == codingLetterText.AutoMarkupId)
        {
            alertWindow("No comment is selected.");
            return false;
        }
        if (currentCommentNumber == codingLetterText.PendingCommentId)
        {
            CreateNewCommentId(phaseId, letterId, codingLetterText.GetTextSelection(), checkedCodeList);
            clearCodeTreeView();
        }
        else
        {
            addCommentCodesCall(phaseId, letterId, currentCommentNumber, checkedCodeList);
            clearCodeTreeView();
        }
    }
    else
    {
        alertWindow("No comment is selected.");
        return false;
    }
}


// ---------------------------------------------------------------
// Create New Comment Server side and get comment Id
// ---------------------------------------------------------------
function CreateNewCommentId(phaseId, letterId, textSelection, checkedCodeList)
{
    if (codingLetterText.IsLocked())
    {
        codingLetterText.DisplayLockedMessage();
        return;
    }
    var retVal = "";
    
        parms = {
            phaseId: phaseId,
            letterId: letterId,
            selectedText: htmlEncode(textSelection),
            checkedCodeList: checkedCodeList,
            codedLetterText: htmlEncode(codingLetterText.GetText()),
            pendingTag: codingLetterText.PendingTag
        };

        action = "AddComment";
        codingLetterText.Lock();
        $.post("/Letter/" + action, 
            parms,
            function(data)
            {
                var xml = $.createXMLDocument(data);

                // parse the result xml
                retVal = $(xml).find("commentId").text();
                codingLetterText.Unlock();
                codingLetterText.CreateComment(retVal, false);


                // update status display
                setLetterStatusName("In Progress");

                //Get early action status from data
                var earlyActionStatusText = $(xml).find("earlyActionStatusId").text();
                setEarlyActionRequired(earlyActionStatusText);
            },"text");
    return retVal;
}


// ---------------------------------------------------------------
// update the codes for the comment
// ---------------------------------------------------------------
function addCommentCodesCall(phaseId, letterId, commentNumber, checkedCodeList)
{
    if (codingLetterText.IsLocked())
    {
        codingLetterText.DisplayLockedMessage();
        return;
    }
    parms = {
        phaseId: phaseId,
        letterId: letterId,
        letterCommentNumber: commentNumber,
        checkedCodeList: checkedCodeList
    };

    action = "AddCommentCodes";
    codingLetterText.Lock();
    $.post("/Letter/" + action, 
            parms,
            function(data) 
            {
                codingLetterText.Unlock();
                if (data != "")
                {
                    setCommentInputAreaHtml(data);

                    // clear the code tree view
                    clearCodeTreeView();

                    // update status display
                    setLetterStatusName("In Progress");
                    UpdateMessage("Updated Comment Codes");
                }
                else
                {
                    alertWindow("Error occurred while adding the codes.");
                }
            },"html");
}

// ---------------------------------------------------------------
// confirm and save the comment
// ---------------------------------------------------------------
function saveComment(commentId) {
    var sampleStatement = $("#Comment_SampleStatement").attr("checked");
    var noResponseRequired = $("#Comment_NoResponseRequired").attr("checked");
    var noResponseReasonId = getDdlResponseReason().value();
    var noResponseReasonOther = $("#Comment_NoResponseReasonOther").val().substring(0, 50);
    var annotation = $("#Comment_Annotation").val();

    // confirmation message for non-empty annotation
    var callback = "doSaveComment(" + commentId + ", " + sampleStatement + ", " + noResponseRequired + ", \"" + noResponseReasonId + "\", \"" + escape(noResponseReasonOther) + "\", \"" + escape(annotation) + "\")";
    
    if (annotation != null && annotation != "")
    {
        confirmWindowSize("WARNING - Annotations will be saved as part of the project record when the comment period is archived.\n\nAre you sure you want to save this comment with an annotation",
            callback, "", 500, 80);
    }
    else
    {
        eval(callback);
    }
}

// ---------------------------------------------------------------
// save the comment by the comment id and user's input data
// ---------------------------------------------------------------
function doSaveComment(commentId, sampleStatement, noResponseRequired, noResponseReasonId, noResponseReasonOther, annotation) {
    // validation
    if (noResponseRequired && getDdlResponseReason().value() == "")
    {
        alertWindow("No Further Response Reason is required.");
        return false;
    }
    else if (!noResponseRequired && getDdlResponseReason().value() != "")
    {
        alertWindow("You must check the No Further Response Required to select the No Further Response Reason.");
        return false;
    }
    if (getDdlResponseReason().text() == "Other (If Other, please specify)" &&
        $("#Comment_NoResponseReasonOther").val() == "")
    {
        alertWindow("No Further Response Reason text is required.");
        return false;
    }
    else if (getDdlResponseReason().text() != "Other (If Other, please specify)" &&
        $("#Comment_NoResponseReasonOther").val() != "")
    {
        alertWindow("No Further Response Reason text is not valid for the selected reason.");
        return false;
    }

    // set up parameters
    var parms = {
        commentId: commentId,
        sampleStatement: sampleStatement,
        noResponseReasonId: (noResponseReasonId == "" ? null : noResponseReasonId),
        noResponseReasonOther: unescape(noResponseReasonOther),
        annotation: unescape(annotation),
        phaseId: $("#PhaseId").val()
    };

    // ajax action call to populate comment input area
    $.post("/Letter/SaveComment",
        parms,
        function(data)
        {
            if (data == "True")
            {
                // update status display
                setLetterStatusName("In Progress");

                UpdateMessage("Data saved.");
            }
            else
            {
                alertWindow("An error occurred while saving the data.");
            }
        },
        "text");

    setCommentAreaSize();
}

// ---------------------------------------------------------------
// uncheck all nodes in the code tree view
// ---------------------------------------------------------------
function clearCodeTreeView() {
    var treeView = $("#TreeView").data("tTreeView");
    var allCheckboxes = $("#TreeView").find("li").find("> div > .t-checkbox :checkbox");
    
    $.each(allCheckboxes, function(index, checkbox) {
        $(checkbox).attr("checked", false);
        treeView.checkboxClick(null, checkbox);
    });
}

// ---------------------------------------------------------------
// confirm and delete the comment
// ---------------------------------------------------------------
function deleteComment(commentId) {
    confirmWindow("Comment will be deleted. Do you wish to continue?", "doDeleteComment(" + commentId + ")");
}

// ---------------------------------------------------------------
// delete the comment by the comment id
// ---------------------------------------------------------------
function doDeleteComment(commentId) {
    if (codingLetterText.IsLocked())
    {
        codingLetterText.DisplayLockedMessage();
        return;
    }
    // set up parameters (pass phaseId as a request param for priv checking)
    var parms = {
        commentId: commentId,
        phaseId: $("#PhaseId").val()
    };

    // ajax action call to populate comment input area
    $.post("/Letter/DeleteComment", 
        parms,
        function(data)
        {
            // parse the result xml
            var orphanConcern = getOrphanedConcernIndicatorFromXml(data);

            if (orphanConcern == "false")
            {
                //TODO change letter coding text
                // update status display
                setLetterStatusName("In Progress");

                //update EarlyActionStatus display
                var earlyActionStatusText = getEarlyActionStatusTextFromXml(data);
                setEarlyActionRequired(earlyActionStatusText);

                codingLetterText.DeleteComment();

                //set the confirmation message
                UpdateMessage("Comment Deleted.");
            }
            else
            {
                alertWindowSize("The comment could not be deleted because it is the only comment associated with a concern/response. Please delete the concern/response first.", 300, 80);
            }
        },
        "text");
}

// ------------------------------------------------------------------------
// show the controls on the page (were initially hidden during loading)
// ------------------------------------------------------------------------
function showControlsOnLetter(divVal, display) {
    if (display== "show"){
    // show all controls
    $("#annotationTextarea_"+divVal).show();
    $("#show_"+divVal).hide();
    $("#hide_"+divVal).show();
    }
    else{
    $("#annotationTextarea_"+divVal).hide();
     $("#show_"+divVal).show();
    $("#hide_"+divVal).hide();
    }
 
}

// ---------------------------------------------------------------
// confirm and update the letter term status
// ---------------------------------------------------------------
//function updateLetterTerm(letterId, termListId, termId, action) {
function updateLetterTerm() {
    //confirmWindow("Are you sure", "doUpdateLetterTerm(" + letterId + ", " + termListId  + ", " + termId  + ", " +  action + ")");
    //  confirmWindow("Are you sure", "doUpdateLetterTerm()");
    doUpdateLetterTerm();
}

// ---------------------------------------------------------------
// update the letter term status
// CARA-717: Modified the below function to update new columns StatusId/Annotation in the Letter term status. - By LD 10/4/11
// ---------------------------------------------------------------
//function doUpdateLetterTerm(letterId, termListId, termId, action) {
function doUpdateLetterTerm()
{
    if (codingLetterText.IsLocked())
    {
        codingLetterText.DisplayLockedMessage();
        return;
    }
    //using the delimiter...

    var arrStatusId;
    var arrTermListId;
    var arrTermId;
    var arrAnnotation;

    if ($("#ltcListCount").val() > 0)
    {
        for (var i = 0; i < $("#ltcListCount").val(); i++) 
        {
            if (i==0)
            {
                arrStatusId = $("#statusId_".concat(i)).val();
                arrTermListId = $("#termListId_".concat(i)).val(); 
                arrTermId = $("#termId_".concat(i)).val();
                arrAnnotation = $("#annotation_".concat(i)).val();
            }
            else 
            {
                arrStatusId = arrStatusId + "|" + $("#statusId_".concat(i)).val();
                arrTermListId = arrTermListId + "|" + $("#termListId_".concat(i)).val(); 
                arrTermId = arrTermId + "|" + $("#termId_".concat(i)).val();
                arrAnnotation = arrAnnotation + "|" + $("#annotation_".concat(i)).val();
            }
        }//for
    }//if


    var parms = {
    letterId: $("#letterId").val(),
    termListId:arrTermListId,
    termId: arrTermId,
    statusId:arrStatusId,
    annotation:arrAnnotation,
    cnt: $("#ltcListCount").val(),
    phaseId: $("#PhaseId").val()
    };
     

    // ajax action call to refresh comment area
    codingLetterText.Lock();
    $.post("/Letter/UpdateLetterTerm",
        parms,
        function(data)
        {
            codingLetterText.Unlock();
            var codedText = getCodedTextFromXml(data);
        
            var earlyActionStatusText = getEarlyActionStatusTextFromXml(data);

            //TODO needs updating
            // set the comment area with the new coded text
            codingLetterText.SetText(codedText);

            // CR-327 dissociate automarkup status from letter status.
            // update status display
            //setLetterStatusName("In Progress");

            //update EarlyActionStatus display
            setEarlyActionRequired(earlyActionStatusText);
            UpdateMessage("Auto-Markup changes saved");
                      
        },
        "text");
}

// ------------------------------------------------------------------------------------
// extract the coded text from the input xml document (to preserve the line breaks)
// ------------------------------------------------------------------------------------
function getOrphanedConcernIndicatorFromXml(xmlData)
{
    var orphanedConcern = "false";

    if (xmlData != null && xmlData != "")
    {
        var xml = $.createXMLDocument(xmlData);
        var key = "orphanConcern";
    
        if (xml != null)
        {
            // get the xml text
            orphanedConcern = $(xml).find(key).text();
        }
    }

    return orphanedConcern;
}

// ------------------------------------------------------------------------------------
// extract the coded text from the input xml document (to preserve the line breaks)
// ------------------------------------------------------------------------------------
function getCodedTextFromXml(xmlData) {
    var codedText = "";
    
    if (xmlData != null && xmlData != "")
    {
        // create the DOM xml document from the data
        var xml = $.createXMLDocument(xmlData);
        var key = "codedText";
    
        if (xml != null)
        {
            // get the xml text
            codedText = $(xml).find(key).text();
            //decode html
            codedText = $('<div>').html(codedText).text();
        }
    }

    return codedText;
}

// ------------------------------------------------------------------------------------
// extract the coded text from the input xml document (to preserve the line breaks)
// ------------------------------------------------------------------------------------
function getEarlyActionStatusTextFromXml(xmlData) {
    var earlyActionStatusText = "";
    
    if (xmlData != null && xmlData != "")
    {
        // create the DOM xml document from the data
        var xml = $.createXMLDocument(xmlData);
        var key = "earlyActionStatusId";

        if (xml != null)
        {
            // get the xml text
            earlyActionStatusText = $(xml).find(key).text();
        }
    }

    return earlyActionStatusText;
}



// ------------------------------------------------------------------------
// save the selected term lists to the session and refresh the letter text
// ------------------------------------------------------------------------
function saveTermList(letterId, termListIdList) {
    if (codingLetterText.IsLocked())
    {
        codingLetterText.DisplayLockedMessage();
        return;
    }
    // set up parameters (pass phaseId as a request param for priv checking)
    var parms = {
        letterId: letterId,
        termListIdList: termListIdList,
        phaseId: $("#PhaseId").val()
    };
    codingLetterText.Lock();
    // ajax action call to populate comment input area
    $.post("/Letter/SaveTermList", 
        parms,
        function(data)
        {
            codingLetterText.Unlock();
            // parse the result xml
            var codedText = getCodedTextFromXml(data);

            // set the comment area with the new coded text
            codingLetterText.SetText(codedText);

            //set the confirmation message
            UpdateMessage("Auto-Markup lists updated");
        },
        "text");
}

// ---------------------------------------------------------------
// handles the link text button click
// ---------------------------------------------------------------
function linkText(projectId, phaseId, letterId)
{
    
    // open window to select a comment
    openWindow('WindowLinkText', '/Project/' + projectId + '/Phase/' + phaseId + '/Letter/LinkText/' + letterId);
}

// -------------------------------------------------------------------------------------
// ajax post to add selected text to the input comment and refresh the letter text
// -------------------------------------------------------------------------------------
function addCommentText(phaseId, letterId, commentId) {
    if (phaseId != null && phaseId != "" && letterId != null & letterId != "" && commentId != null & commentId != "")
    {
        //check to see if the system is busy
        if (codingLetterText.IsLocked())
        {
            codingLetterText.DisplayLockedMessage();
            return;
        }
        // get highlighted text
        var highlight = codingLetterText.GetTextSelection();
        var commentNumber = $("#CommentNumber_" + commentId)[0].innerHTML;
        
        codingLetterText.CreateComment(commentNumber, true);
        
        // validation
        if (highlight == "")
        {
            alertWindow("Please select some text from the letter.");
            return false;
        }

        // set up parameters
        var parms = {
            phaseId: phaseId,
            letterId: letterId,
            commentId: commentId,
            selectedText: highlight,
            codedText: htmlEncode(codingLetterText.GetText())
        };

        // ajax action call to populate comment input area
        $.post("/Letter/AddCommentText", 
            parms,
            function(data)
            {

                // update status display
                setLetterStatusName("In Progress");
            },
            "Text");
    }
}

// ---------------------------------------------------------------
// set the letter status display to be the input status name
// ---------------------------------------------------------------
function setLetterStatusName(statusName) {

    var isIE = false;
    var currentStatus = "";
    if (document.getElementById("LetterStatusName").textContent == undefined)
    {
        currentStatus = document.getElementById("LetterStatusName").innerText;
        isIE = true;
    }
    else
    {
        currentStatus = document.getElementById("LetterStatusName").textContent;
    }
    if (currentStatus == NotNeededStatus)
    {
        return;
    }

    var nextAction = "Complete";
    if (statusName != "In Progress")
    {
        nextAction = "In Progress";
    }



    document.getElementById("UpdateStatus").setAttribute("value", nextAction);
                
    if (!isIE)
    {
        document.getElementById("LetterStatusName").textContent=statusName;
        document.getElementById("LetterStatusNameDetails").textContent=statusName;
    }
    else
    {
        document.getElementById("LetterStatusName").innerText=statusName;
        document.getElementById("LetterStatusNameDetails").innerText=statusName;
    }
}


// ---------------------------------------------------------------
// set the letter early action required to be the database status name
// ---------------------------------------------------------------
function setEarlyActionRequired(statusName) {
   $("#EarlyActionRequired").text(statusName);
}

// ---------------------------------------------------------------
// return the html version of the input text string
// ---------------------------------------------------------------
function toHtmlString(text) {
    return (text != null ? htmlEncode(text).replace(/(\r\n|[\r\n])/g, lineBreak) : "");
}

// ---------------------------------------------------------------
// return the single line text version of the input text string
// ---------------------------------------------------------------
function toSingleLineText(text) {
    return (text != null ? text.replace(/(\r\n|[\r\n])/g, "") : "");
}

// ---------------------------------------------------------------
// returns the window url query string params in an array
// ---------------------------------------------------------------
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    
    return vars;
}

// ---------------------------------------------------------------
// returns the html encoded text
// ---------------------------------------------------------------
function htmlEncode(text) {
    return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

// ---------------------------------------------------------------
// returns the input text by encoding the html linebreak tags
// ---------------------------------------------------------------
function htmlEncodeLinebreaks(text) {
    return text.replace(/<BR>/gi, htmlEncode(lineBreak));
}

// ---------------------------------------------------------------
// convert XML in a string type, into an XML Document type
// ---------------------------------------------------------------
jQuery.createXMLDocument = function(string) {
    var browserName = navigator.appName;
    var doc;

    if (browserName == 'Microsoft Internet Explorer')
    {
        doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.async = 'false'
        doc.loadXML(string);
    }
    else
    {
        doc = (new DOMParser()).parseFromString(string, 'text/xml');
    }

    return doc;
}

// -------------------------------------------------------------------------------
// add tab index and keyboard handling events for the phase code tree view
// -------------------------------------------------------------------------------
function CodeTree_OnLoad(e)
{
    $(".t-icon", this).attr("tabindex", "0");
    var treeview = $(this).data("tTreeView");
    $(this).delegate('.t-plus, .t-minus', 'keydown', $.telerik.delegate(treeview, TreeViewKeyboardNav));
}

// -------------------------------------------------------------------------------
// add tab index and keyboard handling events for the tree view
// -------------------------------------------------------------------------------
function TreeViewKeyboardNav(e, element)
{
    var $element = $(element),
        $item = $element.closest('.t-item');
 
    if ($element.hasClass('t-plus-disabled') || $element.hasClass('t-minus-disabled'))
        return;
 
    if (e.type == "keydown" && typeof(e.keyCode) != "undefined") {
        switch (e.keyCode) {
            case 13: // Enter, item will be toggled
                break;
            case 37: // <-
                if($element.hasClass('t-plus')) { return; }
                break;
            case 39: // ->
                if($element.hasClass('t-minus')) { return; }
                break;
            default:
                return;
            break;
        }
    }
 
    this.nodeToggle(e, $item);
}

// -------------------------------------------------------------------------------
// Displays a warning message if a "Save" button is clicked before codes are applied to a comment
// -------------------------------------------------------------------------------
function warnUnappliedCodes() {
    var proceed = true;
    var codeCheckCount = $("#codeListArea input[type=\"checkbox\"]:checked").length;
    if (codeCheckCount > 0) {
        // display warning message
        alertWindow("Warning: You have checked codes in the Coding tab that have not been applied to a comment.");
        proceed = false;
    }
    return proceed;
}

function printSummary() {
    $("#letterSummaryPrint").printElement();

    return false;
}

function openWindowSummary() {
    $('#WindowSummary').data('tWindow').center().open();
    
    return false;
}

function showLoading() {
  $("#loading").show();
}

function hideLoading() {
  $("#loading").hide();
}

function updateStatus(letterid, phaseId)
{
    if (!warnUnappliedCodes())
    {
        return;
    }

	//CARA-1227
    var currentStatus = "";
    if (document.getElementById("LetterStatusName").textContent == undefined)
    {
        currentStatus = document.getElementById("LetterStatusName").innerText;
    }
    else
    {
        currentStatus = document.getElementById("LetterStatusName").textContent;
    }
    if (currentStatus == NotNeededStatus)
    {
        return;
    }

    openWindow("PleaseWait");

    $.ajax({
      dataType: "json",
      url: "/Letter/UpdateStatus",
      data: { id:letterid, currentStatus:currentStatus, phaseId:phaseId },
      cache: false,
      success:
        function (data)
        {
            closeWindow("PleaseWait");
            if (data.success)
            {
                setLetterStatusName(data.newStatus);
                UpdateMessage("Status set to " + data.newStatus);
//                var nextAction = "Complete";
//                if (data.newStatus != "In Progress")
//                {
//                    nextAction = "In Progress";
//                }

//                document.getElementById("UpdateStatus").setAttribute("value", nextAction);
//                
//                if (!isIE)
//                {
//                    document.getElementById("LetterStatusName").textContent=data.newStatus;
//                    document.getElementById("LetterStatusNameDetails").textContent=data.newStatus;
//                }
//                else
//                {
//                    document.getElementById("LetterStatusName").innerText=data.newStatus;
//                    document.getElementById("LetterStatusNameDetails").innerText=data.newStatus;
//                }
            }
            else
            {
                alertWindow(data.error);  
            }
        }});
}

//-->
