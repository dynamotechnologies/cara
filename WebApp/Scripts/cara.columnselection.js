﻿<!--
function Validate(step)
{
    if (document.getElementById("chosenColumns").options.length <=0)
    {
        alertWindow("Please select at least one column to display");
        return;
    }
    ChangeStep(step);
}
function GetColumns()
{
    var columnGroup = $("#columnGroup").data("tDropDownList")
    $.getJSON("/Report/GetColumns?id=" + columnGroup.value(), function(data)
    {
        $("#columns").data("tDropDownList").dataBind(data);
    });
}

function Move(direction)
{
    var options = document.getElementById("chosenColumns").options;
    var collapsed = document.getElementById("CollapsedListIds").value;
    var listIds = "";
    var any = false;
    var indexs = new Array();
    while (options.selectedIndex >= 0)
    {
        if ((direction && options.selectedIndex == 0) || 
            (!direction && options.selectedIndex == options.length - 1))
        {
            for (i=0; i<indexs.length; i++)
            {document.getElementById("chosenColumns").options[indexs[i]].selected = true;}
            return;
        }
        indexs.push(options.selectedIndex);
        
        if (any)
        {
            listIds = listIds + "|" + options[options.selectedIndex].value.toString();
        }
        else
        {
            listIds = options[options.selectedIndex].value.toString();
            any = true;
        }
        options[options.selectedIndex].selected = false;
    }
    if (!any){return;}

    $.ajax({
      dataType: "json",
      url: "/Report/MoveColumns",
      data: {listIds:listIds, direction:direction, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
            if(data.error == "")
            {
                DisplayColumns(data.list);
                var adjust = 1;
                if (direction){adjust = -1;}
                for (i=0; i<indexs.length; i++)
                {document.getElementById("chosenColumns").options[indexs[i]+adjust].selected = true;}
            }
        }});
}

function Add(columnElement)
{
    var id = $("#columns").data("tDropDownList").value();
    var collapsed = document.getElementById("CollapsedListIds").value;
    if (id <= 0){return;}
    $.ajax({
      dataType: "json",
      url: "/Report/AddColumn",
      data: {columnId:id, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
            if (data.error=="")
            {
                DisplayColumns(data.list);
                $("#columns").data("tDropDownList").select(0);
            }
        }});
}

function Clear()
{
    var collapsed = document.getElementById("CollapsedListIds").value;
    var options = document.getElementById("chosenColumns").options;
    var listIds = "";
    var any = false;
    while (options.selectedIndex >= 0)
    {
        if (any)
        {
            listIds = listIds + "|" + options[options.selectedIndex].value.toString();
        }
        else
        {
            listIds = options[options.selectedIndex].value.toString();
            any = true;
        }
        options[options.selectedIndex].selected = false;
    }
    if (!any){return;}
    $.ajax({
      dataType: "json",
      url: "/Report/ClearColumns",
      data: {listIds:listIds, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
            if (data.error == "")
            {
                DisplayColumns(data.list);
            }
        }});
    
}

function ClearAll()
{
    var collapsed = document.getElementById("CollapsedListIds").value;
    $.ajax({
      dataType: "json",
      url: "/Report/ClearAllColumns",
      data: {collapsedListIds:collapsed},
      cache: false,
      success:
        function(data)
        {
            UpdateAndDisplay(data);
            if (data.error == "")
            {
                DisplayColumns(data.list);
            }
        }});
    
}

function DisplayColumns(data)
{
    var options = document.getElementById("chosenColumns").options;
    for (i = options.length - 1; i >= 0; i--)
    {
        options.remove(i);
    }
    for (i = 0; i < data.length; i++)
    {
        var displayItem = data[i];
        var option = document.createElement("option");
        option.text = displayItem.Value;
        option.value = displayItem.Key;
        options.add(option);
    }
    
}
//-->