﻿<!--


function RemoveValues()
{
    var treeview = $("#TreeView").data("tTreeView");
    var node = $('#TreeView .t-state-selected').closest('li');
    var collapsed = document.getElementById("CollapsedListIds").value;
    var listIds = treeview.getItemValue(node);
    if (listIds == "" || listIds == "-1")
    { return; }
    $.ajax({
      dataType: "json",
      url: "/Report/RemoveFieldValue/",
      data: {listIds:listIds, collapsedListIds:collapsed},
      cache: false,
      success: 
        function (data)
        {
            UpdateAndDisplay(data);
            if (document.getElementById("chosenColumns") != null && data.error == "")
            {DisplayColumns(data.list);}
        }});
}

function UpdateAndDisplay(responseJson)
{
    if (responseJson.error == "")
    {
        $("#TreeView").data("tTreeView").bindTo(responseJson.tree);
        document.getElementById("CollapsedListIds").value = responseJson.collapsedListIds;
        document.getElementById("Dirty").value = true;
    }
    else
    {
        alertWindow(responseJson.error);
    }
}

function CollapseNode(e)
{
    var treeview = $("#TreeView").data("tTreeView");
    var id = treeview.getItemValue(e.item);
    var collapseList = document.getElementById("CollapsedListIds").value;
    if (collapseList == "")
    { collapseList = id.toString(); }
    else
    { collapseList = collapseList + "|" + id.toString(); }
    document.getElementById("CollapsedListIds").value = collapseList;
}

function ExpandNode(e)
{
    var treeview = $("#TreeView").data("tTreeView");
    var id = treeview.getItemValue(e.item);
    var collapseList = document.getElementById("CollapsedListIds").value;
    var arrCollapseList = collapseList.split("|");
    collapseList = "";
    var first = true;
    for (i = 0; i < arrCollapseList.length; i++)
    {
        if (arrCollapseList[i] != id.toString())
        {
            if (!first)
            { collapseList += "|"; }
            collapseList += arrCollapseList[i];
            first = false;
        }
    }
    document.getElementById("CollapsedListIds").value = collapseList;
}
//-->