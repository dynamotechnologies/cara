﻿<!--

var expandAllText = "Expand All";
var collapseAllText = "Collapse All";

// -----------------------------------------------------------------------------------
// perform the letter action in the form set based on the selected dropdownlist value
// -----------------------------------------------------------------------------------
function doLetterAction(letterId, formSetId) { 
    if (letterId != null)
    {
        var dropDownList = $("#ddlLetterAction_" + letterId).data("tDropDownList");
        var action = "";

        // get the selected action from the dropdown list
        if (dropDownList != null)
        {
            action = dropDownList.value();
        }

        // process the action by value
        switch (action)
        {
            case "Unique":
                confirmWindow(getRedesignateMsg(action), "setLetterType(" + letterId + ", " + formSetId + ", \"" + action + "\")");
                break;
            case "Form":
                confirmWindow(getRedesignateMsg(action), "setLetterType(" + letterId + ", " + formSetId + ", \"" + action + "\")");
                break;
            case "Form Plus":
                confirmWindow(getRedesignateMsg(action), "setLetterType(" + letterId + ", " + formSetId + ", \"" + action + "\")");
                break;
            case "Master":
                confirmWindow(getRedesignateMsg(action), "setToMaster(" + letterId + ", " + formSetId + ")");
                break;
            case "Send":
                openFormSetSelectWindow(letterId, formSetId);
                break;
            case "Create":
                openFormSetCreateWindow(letterId, formSetId);
                break;
        }

        // clear the dropdown value
        if (dropDownList != null)
        {
            dropDownList.value("");
        }
    }
}

// -----------------------------------------------------------------------------------
// set the letter to be the master letter of the form set
// -----------------------------------------------------------------------------------
function saveFormSet(mode, phaseId, formSetId, masterFormId, name) { 
    var result = false;
    var actionName = "";
    var id = "";
    var parms = "";
    var maxNameLength = 50;

    // validate the input
    if (name == null || name == "")
    {
        alertWindow("Form Set Name is a required field.");
        return false;
    }
    if (name !== null)
    {
        if (name.length > maxNameLength)
        {
            alertWindow("Form Set Name can be no more than " +maxNameLength+ " characters.");
            return false;
        }
    }

    // set up parameters (pass phaseId as a request param for priv checking)
    if (mode == "Edit")
    {
        actionName = "UpdateFormSet";

        parms = {
            projectId: getProjectId(),
            phaseId: getPhaseId(),
            id: formSetId,
            name: name
        }; 
    }
    else if (mode == "Create")
    {
        actionName = "CreateFormSetWithMasterForm";

        parms = {
            projectId: getProjectId(),
            phaseId: getPhaseId(),
            id: masterFormId,
            name: name
        };
    }

    if (actionName != "" && parms != null)
    {
        // ajax action call to set the letter type
        $.post("/Form/" + actionName,
            parms,
            function(data)
            {
                if (data != "")
                {
                    // redirect to the url for refresh
                    parms = {formSet: data};
                    redirectToUrl(getRequestUri() , parms);
                    result = true;
                }
                else
                {
                    alertWindow("The Form Set Name already exists.");
                }
            },
            "html");
    }
    else
    {
        alertWindow("Invalid form set action.");
    }

    return result;
}

// -----------------------------------------------------------------------------------
// send the letter to another form set
// -----------------------------------------------------------------------------------
function openFormSetSelectWindow(letterId, formSetId) {
    // open window to select a form set
    openActionWindow('WindowSelectFormSet', getFormAction('SelectFormSet'), letterId, 'formSet=' + formSetId);
}

// -----------------------------------------------------------------------------------
// create a new form set with the letter
// -----------------------------------------------------------------------------------
function openFormSetCreateWindow(letterId, formSetId) {
    // open window to create the form set
    openActionWindow('WindowCreateFormSet', getFormAction('CreateFormSet'), letterId, 'formSet=' + formSetId);
}

// -----------------------------------------------------------------------------------
// edit an existing form set with the letter
// -----------------------------------------------------------------------------------
function openFormSetEditWindow(formSetId) {
    // open window to edit the form set
    openActionWindow('WindowEditFormSet', getFormAction('EditFormSet'), formSetId, '');
}

// -----------------------------------------------------------------------------------
// compare the letter to the form master letter
// -----------------------------------------------------------------------------------
function openCompareMasterWindow(letterId, masterFormId, navigate) {
    openActionWindow('WindowCompare', getLetterAction('CompareMaster'), letterId, "&targetLetterId=" + masterFormId + "&navigate=" + navigate);
}

// -----------------------------------------------------------------------------------
// compare one formsetmaster to another
// -----------------------------------------------------------------------------------
function openCompareMaster2MasterWindow(masterFormId, navigate) {
    var compareTo = $("#CompareTo" + masterFormId).data("tDropDownList").value();
    openCompareMasterWindow(masterFormId, compareTo, navigate);
}

// -----------------------------------------------------------------------------------
// open the popup window based on the window name, action name, and id
// -----------------------------------------------------------------------------------
function openActionWindow(windowName, actionName, id, parms) {
    openWindow(windowName, '/Project/' + getProjectId() + '/Phase/' + getPhaseId() + actionName + '/' + id +
        (parms != "" ? "?" + parms : ""));
}

function getFormAction(actionName) { 
    return "/Letter/Form/" + actionName;
}

function getLetterAction(actionName) {
    return "/Letter/" + actionName;
}

// ------------------------------------------------------------
// Handles on click event of the "expand all" grid command
// ------------------------------------------------------------
function onClickExpandAll(button) {
    var text = $(button).text();
    var expand = false;

    // replace button text
    if (text == expandAllText)
    {
        $(button).text(collapseAllText);
        expand = true;
    }
    else
    {
        $(button).text(expandAllText);
    }

    var grid = $("#List").data("tGrid");

    if (grid != null)
    {
        $("#List").find('tr.t-grouping-row').each(function() {
            expand ? grid.expandGroup($(this)) : grid.collapseGroup($(this));
        });
    }
}

// ---------------------------------------------------------------
// returns the html encoded text
// ---------------------------------------------------------------
function htmlEncode(text) {
    return text.replace(/&/g, ampersand).replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function requestFormLetters(projectId, phaseId, formSetId) {
    $.post("/Project/" + projectId + "/Phase/" + phaseId + "/DownloadFormLetters", {projectId:projectId, phaseId:phaseId, formSetId:formSetId},
        function (data)
        {
            if (data.toLowerCase() == "true")
            {
                alertWindow("Your request to download all letters was logged. You will receive an e-mail with a link that will instruct you on how to download the letters.");
            }
            else
            {
                
                if (window.confirm('Your request to download all letters was not successful. Please open a ticket by clicking OK else click Cancel')) 
                {
                window.location.href='https://ems-team.usda.gov/sites/fs-emnepa-sd/Lists/eMNEPASupport/Item/newifs.aspx';
                }
            }
        });
}

//-->