﻿<!--

$(document).ready(function() {
    // ------------------------------------------------------------------------------------------------------
    // set up input element action events to validate the input text
    // ------------------------------------------------------------------------------------------------------
    $("input").bind("keyup paste", validateTextHandler);
});

function validateTextHandler(e) {
    var obj = $(this);

    if (e.type == "paste")
    {
        // note: trick to get the correct text to validate from DOM
        setTimeout(function(){ validateText(obj) }, 1);
    }
    else
    {
        validateText(obj);
    }
}

// ------------------------------------------------------------------------------------------------------
// validates the text of the input control and raises the warning if harmful contents are detected
// ------------------------------------------------------------------------------------------------------
function validateText(obj) {
    // set up input element key up event to validate the text
    var submitButtons = $("form").find("input[type='submit']");
    var buttons = $("form").find("input[type='button']");
	var pattern = new RegExp('<((.*?)>|\\S+)');
	result = pattern.test(obj.val());

	if (result == 1)
	{
	    var titleText = "HTML-like tags were found.  Please remove any paired < and > characters.";

	    obj.data('tipText', titleText)
	    $('<p class="tooltip"></p>')
	    .text(titleText)
	    .appendTo('body')
	    .css('top', (obj.position().top - 12) + 'px')
	    .css('left', (obj.position().left + obj.width() + 20) + 'px')
	    .fadeIn('slow');
	    obj.css('color', 'red');
            
        // disable submit buttons
        if (submitButtons != null && submitButtons.length > 0)
        {
            submitButtons.attr("disabled", "disabled");
            submitButtons.removeClass("button");
            submitButtons.addClass("buttonDisabled");
        }

        // disable other buttons
        if (buttons != null && buttons.length > 0)
        {
            buttons.attr("disabled", "disabled");
            buttons.removeClass("button");
            buttons.addClass("buttonDisabled");
        }

        // disable <a> link buttons
        var linkButton = $(".button");

        linkButton.removeClass("button");
        linkButton.addClass("buttonDisabled");
        linkButton.attr("disabled", "disabled");
        linkButton.each(function() {
            obj.bind("click", disableLinkButton);
        });

        // disable key press submit using enter key
        $("input").keypress(function (e) {
            var code = null;
            code = (e.keyCode ? e.keyCode : e.which);
            return (code == 13) ? false : true;
        });
	}
	else
	{
		obj.css('color', 'black');
		$('.tooltip').remove();

        // enable submit buttons
        if (submitButtons != null && submitButtons.length > 0)
        {
            submitButtons.removeAttr('disabled');
            submitButtons.removeClass("buttonDisabled");
            submitButtons.addClass("button");
        }

        // enable buttons
        if (buttons != null && buttons.length > 0)
        {
            buttons.removeAttr('disabled');
            buttons.removeClass("buttonDisabled");
            buttons.addClass("button");
        }

        // enable <a> link buttons
        var linkButton = $(".buttonDisabled");

        linkButton.removeClass("buttonDisabled");
        linkButton.addClass("button");
        linkButton.removeAttr("disabled");
        $(".button").each(function() {
            obj.unbind("click", disableLinkButton);
        });

        // enable key press submit
        $("input").unbind("keypress");
        $("input").keypress(function (e) {
            var code = null;
            code = (e.keyCode ? e.keyCode : e.which);
            return true;
        });
	}
}

function disableLinkButton(e) {
    // cancels the event
    e.preventDefault();

    return false;
}

//-->