﻿<!--

var ActionTypes = { "Default": "", "Prev": "Prev", "Next": "Next", "Save": "Save", "Add": "Add", "Remove" : "Remove" };
var Buttons = { "AddCode": "input#addCode", "AddCode1": "input#addCode1", "EditCode": "input#editCode", "EditCode1": "input#editCode1",
    "Collapse1": "#btnCollapse1", "Collapse2": "#btnCollapse2", "Expand1": "#btnExpand1", "Expand2" : "#btnExpand2" };
var action = ActionTypes.Next;
var keyboardAccess = false;
var currIndex = -1;
var SELECTED_CSS_NAME = "t-state-selected";

$(document).ready(function () {
    // clear the checked code id list every time the page loads
    $("#CheckedCodeIdList").val("");

    // enable the tree view item text that are disabled so that
    // the code can be selected to create custom code
    $("#TreeView .t-item").each(function() {
        var $item = $(this), isCollapsed = !$item.find("> .t-group, > .t-content").is(":visible");
        
        $item.find("> div > .t-in")
                .addClass("t-state-default")
                .removeClass("t-state-disabled")
            .end()
            .find("> div > .t-icon")
                .toggleClass("t-plus", isCollapsed)
                .removeClass("t-plus-disabled")
                .toggleClass("t-minus", !isCollapsed)
                .removeClass("t-minus-disabled");
    });

    // hide the add custom code button
    $(Buttons.AddCode).hide();
    $(Buttons.AddCode1).hide();

    // hide the edit custom code button
    $(Buttons.EditCode).hide();
    $(Buttons.EditCode1).hide();
});

// -------------------------------------------------------------------
// Handles page keypress event
// -------------------------------------------------------------------
$(document).keyup(function(e) {    
    if (e.ctrlKey && e.shiftKey)
    {
        switch (e.keyCode)
        {
            case (65):
                // "a" is pressed, highlight the first visible code
                keyboardAccess = true;
                selectNodeByKey(ActionTypes.Next);

                return false;
                break;
            case (85):
                if (keyboardAccess)
                {
                    // "u" is pressed, highlight the previous visible code
                    selectNodeByKey(ActionTypes.Prev);
                }

                return false;
                break;
            case (76):
                if (keyboardAccess)
                {
                    // "l" is pressed, highlight the next visible code
                    selectNodeByKey(ActionTypes.Next);
                }

                return false;
                break;
            case (83):
                // "s" is pressed, open the add custom code window
                if ($(Buttons.AddCode).is(":visible"))
                {
                    $(Buttons.AddCode).click();
                    return false;
                }
                                
                break;
            case (69):
                // "e" is pressed, open the edit custom code window
                if ($(Buttons.EditCode).is(":visible"))
                {
                    $(Buttons.EditCode).click();
                    return false;
                }

                break;
        }
    }
    else if (keyboardAccess)
    {
        switch (e.keyCode)
        {
            case (27):
                // escape is pressed
                removeSelectedNode();

                // reset keyboard access variables
                currIndex = -1;
                keyboardAccess = false;

                // hide the add custom code button
                $(Buttons.AddCode).hide();
                $(Buttons.AddCode1).hide();

                // hide the edit custom code button
                $(Buttons.EditCode).hide();
                $(Buttons.EditCode1).hide();

                return false;
                break;
        }
    }
});

// ------------------------------------------------------------------------
// handles the select node event by current index
// ------------------------------------------------------------------------
function selectNodeByKey(actionType) {
    var oldCurrIndex = currIndex;

    // update the current index by action type
    if (actionType == ActionTypes.Prev)
    {
        currIndex--;
    }
    else if (actionType == ActionTypes.Next)
    {
        currIndex++;
    }
    else
    {
        return false;
    }

    var item = getTreeViewItemByIndex();
    if (item != null)
    {
        // get the selected code data
        var value = treeView().getItemValue(item);
        var text = treeView().getItemText(item);

        removeSelectedNode();
        setSelectedNode(item);

        // call select event handler
        handleSelectedNodeEvent(value, text);
    }
    else
    {
        // revert the current index change if item doesn't exist
        currIndex = oldCurrIndex;
    }
}

// -------------------------------------------------------------
// returns tree view item by current index
// -------------------------------------------------------------
function getTreeViewItemByIndex() {
    return $("li:visible", treeView().element)[currIndex];
}

// -------------------------------------------------------------
// removes any tree view selected item
// -------------------------------------------------------------
function removeSelectedNode() {
    // remove css
    $("#TreeView").find(".t-in").removeClass(SELECTED_CSS_NAME);
}

// -------------------------------------------------------------
// sets input tree view item as the selected node
// -------------------------------------------------------------
function setSelectedNode(item) {
    if (item != null)
    {
        // add css
        $(item).find(">div>.t-in").addClass(SELECTED_CSS_NAME);

        // set the top scroll position
        var topPos = $("." + SELECTED_CSS_NAME).position().top;
        $(window).scrollTop(topPos - 100);
    }
}

// -------------------------------------------------------------
// handles the onclick event of the save buttons
// -------------------------------------------------------------
function confirmSave() {
    // confirm the save action if there is one or more coded comments
    if ($("#HasCodedComments").val() == "True")
    {
        confirmWindow("Comments have already been coded with the coding structure you are about to change. Are you sure you want to change the coding structure", "submitCodeInput(\"Save\")");
    }
    else
    {
        submitCodeInput("Save");
    }
}

// -------------------------------------------------------------
// submits the code input page to save the selected codes
// -------------------------------------------------------------
function submitCodeInput(actionValue) {
    document.getElementById("ActionString").value = actionValue;
    document.forms[0].submit();
}

// -------------------------------------------------------------
// handles the keypress event of the document
// -------------------------------------------------------------
$(document).keypress(function(e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if (action == ActionTypes.Next)
        {
            $("#btn" + action).focus();
            return true;
        }
    }
});

// -------------------------------------------------------------
// Return the treeview control object
// -------------------------------------------------------------
function treeView() {
    return $('#TreeView').data('tTreeView');
}

// ------------------------------------------------------------------------
// expand/collapse all nodes in the treeview
// ------------------------------------------------------------------------
function toggleNodes(mode) {
    if (mode == "Expand")
    {
        // expand all nodes
        treeView().expand("li:has(ul)");

        // update the button visibilities
        $(Buttons.Expand1).hide();
        $(Buttons.Expand2).hide();
        $(Buttons.Collapse1).show();
        $(Buttons.Collapse2).show();
    }
    else
    {
        // collapse all nodes
        treeView().collapse("li:has(ul)");

        // clear the selected nodes in the cookie
        clearCookie("ExpandedPhaseCodes");

        // update the button visibilities
        $(Buttons.Expand1).show();
        $(Buttons.Expand2).show();
        $(Buttons.Collapse1).hide();
        $(Buttons.Collapse2).hide();
    }
}

// ------------------------------------------------------------------------
// handles onchecked event of the code input tree view
// ------------------------------------------------------------------------
function onChecked(e) {
    // give a warning if the custom code is unchecked
    var codeId = treeView().getItemValue(e.item)
    if (isCustom(codeId) && !e.checked)
    {
        confirmWindowWithCancelCallBack("You have unchecked a custom code. It will permanently delete the custom code and the codes under it after you proceed. Are you sure", "checkCodeListAdd(" + codeId + ", false)", "checkCode(" + codeId + ")");
        
    }
    else
    {
        checkCodeListAdd(treeView().getItemValue(e.item), e.checked);
    }
}

// ------------------------------------------------------------------------
// get and check the tree view node by input code id
// ------------------------------------------------------------------------
function checkCode(codeId) {
    var node = $("li:has(> div > input[value='"+ codeId + "'])", treeView().element);
    treeView().nodeCheck(node, true);
}

// ------------------------------------------------------------------------
// Add the codeId to the relavent list
// ------------------------------------------------------------------------
function checkCodeListAdd(codeId, checked)
{
    document.getElementById("NodeChangeCheck").value = addOrRemoveFromList(document.getElementById("NodeChangeCheck").value, codeId, checked);
    document.getElementById("NodeChangeUncheck").value = addOrRemoveFromList(document.getElementById("NodeChangeUncheck").value, codeId, !checked);

}

function addOrRemoveFromList(list, value, add)
{
    var retVal = "";
    var arrList = list.split(",");
    for (i = 0; i<arrList.length; i++)
    {
        if (arrList[i] != value)
        {
            retVal = retVal + ((retVal.length > 0)?("," + arrList[i]):arrList[i]);
        }
    }
    if (add)
    {
        retVal = retVal + ((retVal.length > 0)?("," + value):value);
    }
    return retVal;
}

// ------------------------------------------------------------------------
// handles onselect mouse event of the code input tree view
// ------------------------------------------------------------------------
function onSelect(e) {
    // reset keyboard access variables
    currIndex = -1;
    keyboardAccess = false;

    // get the selected code data
    var value = treeView().getItemValue(e.item);
    var text = treeView().getItemText(e.item);
    
    // call select event handler
    handleSelectedNodeEvent(value, text);
}

// ------------------------------------------------------------------------
// handles the selected node event with the selected code id
// ------------------------------------------------------------------------
function handleSelectedNodeEvent(selectedItemValue, selectedItemText) {
    if (selectedItemValue != "" && selectedItemText != "")
    {

        // update the onclick function of the add custom code button with selected code id
        $(Buttons.AddCode).unbind("click");
        $(Buttons.AddCode1).unbind("click");
        $(Buttons.AddCode).click(function(event) { addCode(getUrl(selectedItemValue, selectedItemText, "Create"), selectedItemText); });
        $(Buttons.AddCode1).click(function(event) { addCode(getUrl(selectedItemValue, selectedItemText, "Create"), selectedItemText); });

        // update the onclick function of the edit custom code button with selected code id
        $(Buttons.EditCode).unbind("click");
        $(Buttons.EditCode1).unbind("click");
        $(Buttons.EditCode).click(function(event) { editCode(getUrl(selectedItemValue, selectedItemText, "Edit"), selectedItemText); });
        $(Buttons.EditCode1).click(function(event) { editCode(getUrl(selectedItemValue, selectedItemText, "Edit"), selectedItemText); });

        // show the add custom code button
        $(Buttons.AddCode).show();
        $(Buttons.AddCode1).show();

        // enable edit button if the custom code is selected
        if (isCustom(selectedItemValue))
        {
            $(Buttons.EditCode).show();
            $(Buttons.EditCode1).show();
        }
        else
        {
            $(Buttons.EditCode).hide();
            $(Buttons.EditCode1).hide();
        }
    }
}

// ------------------------------------------------------------------------
// gets url pattern of open window
// ------------------------------------------------------------------------
function getUrl(selectedItemValue, selectedItemText, mode) {
    return ("/Project/" + $("#ProjectId").val() + "/Phase/" + $("#PhaseId").val() + "/" + mode + "Code?id=" + selectedItemValue
        + (mode == "Create" ? "&name=" + selectedItemText : ""));
}

// ------------------------------------------------------------------------
// handles add code button onclick event
// ------------------------------------------------------------------------
function addCode(url, codeId) {
    // validate the selected code
    var treeview = $("#TreeView");
    var selectedItem = treeview.find(".t-state-selected").closest(".t-item");
    var name = treeview.data("tTreeView").getItemText(selectedItem);

    if (name != "")
    {
        // parse the code number
        name = name.substring(0, name.indexOf(" "));
        if (name != "")
        {
            if (name.length >= 10)
            {
                alertWindow("You cannot create custom codes at the last code level.");
                return false;
            }
        }
    }

    // add the code id to the expanded node collection
    addItemToCookie("ExpandedPhaseCodes", codeId, ActionTypes.Add);

    // if validation succeed, open the popup window to add code
    openWindow("WindowCodeCreate", url);
}

// ------------------------------------------------------------------------
// handles edit code button onclick event
// ------------------------------------------------------------------------
function editCode(url, codeId) {
    // add the code id to the expanded node collection
    addItemToCookie("ExpandedPhaseCodes", codeId, ActionTypes.Add);

    // open the popup window to edit code
    openWindow("WindowCodeEdit", url);
}

// ------------------------------------------------------------------------
// returns true if the input code id value is greater than the thershold
// ------------------------------------------------------------------------
function isCustom(value) {
    return ((value != null && value != "") ? value > 50000 : false);
}

// ------------------------------------------------------------------------
// create code window onload event to turn off the caching
// ------------------------------------------------------------------------
function WindowCodeCreate_OnLoad() {
    $.ajaxSetup({ cache: false });
}

// ------------------------------------------------------------------------
// edit code window onload event to turn off the caching
// ------------------------------------------------------------------------
function WindowCodeEdit_OnLoad() {
    $.ajaxSetup({ cache: false });
}

// ------------------------------------------------------------------------
// add tab index and keyboard handling events for the tree view
// ------------------------------------------------------------------------
function onLoad(e) {
    $(".t-icon", this).attr("tabindex", "0");
    var treeview = $(this).data("tTreeView");
    $(this).delegate('.t-plus, .t-minus', 'keydown', $.telerik.delegate(treeview, TreeViewKeyboardNav));
}

// ------------------------------------------------------------------------
// add tab index and keyboard handling events for the tree view
// ------------------------------------------------------------------------
function TreeViewKeyboardNav(e, element) {
    var $element = $(element),
        $item = $element.closest('.t-item');
 
    if ($element.hasClass('t-plus-disabled') || $element.hasClass('t-minus-disabled'))
        return;
 
    if (e.type == "keydown" && typeof(e.keyCode) != "undefined") {
        switch (e.keyCode) {
            case 13: // Enter, item will be toggled
                break;
            case 37: // <-
                if($element.hasClass('t-plus')) { return; }
                break;
            case 39: // ->
                if($element.hasClass('t-minus')) { return; }
                break;
            default:
                return;
            break;
        }
    }
 
    this.nodeToggle(e, $item);
}

// ------------------------------------------------------------------------
// open the preview window of the code tree with selected codes
// ------------------------------------------------------------------------
function previewCodeTree() {
    var checkedCodes = $("#TreeView :checked").closest(".t-item");
    var checkedCodeIdList = document.getElementById("NodeChangeCheck").value;
    var uncheckedCodeIdList = document.getElementById("NodeChangeUncheck").value;
    var startEmpty = document.getElementById("NoChecks").value;

    if (checkedCodes.length > 0)
    {
        // open window for preview
        openWindow('WindowPreview', "/Project/" + $("#ProjectId").val() + "/Phase/" + $("#PhaseId").val() + "/PreviewCodes?startEmpty=" +
            startEmpty + "&checkedCodeIds=" + checkedCodeIdList + "&uncheckedCodeIds=" + uncheckedCodeIdList);
    }
    else
    {
        alertWindow("Please select a code.");
        return false;
    }
}

// --------------------------------------------------------------------
// Update code tree view state
// --------------------------------------------------------------------
function updateTreeViewState(e)
{
    // get the code name
    var codeName = treeView().getItemText(e.item);
    var action = e.type == "expand" ? ActionTypes.Add : ActionTypes.Remove;

    // add the code id to the expanded node collection
    addItemToCookie("ExpandedPhaseCodes", codeName, action);
}

// --------------------------------------------------------------------
// Add the input item value to the cookie by key
// --------------------------------------------------------------------
function addItemToCookie(cookieKey, itemValue, action) {
    var cookieObject = $.cookie(cookieKey);
    cookieObject = cookieObject ? cookieObject.split(';') : [];
 
    // find the code id in the collection stored in the cookie   
    var itemIndex = $.inArray(itemValue, cookieObject);
    
    // store the value if appropriate
    if (action == ActionTypes.Add && itemIndex == -1)
    {
        cookieObject.push(itemValue);
    }
    else if (action == ActionTypes.Remove && itemIndex >= 0)
    {
        cookieObject.splice(itemIndex, 1);
    }

    // save to the cookie
    $.cookie(cookieKey, cookieObject.join(';'));
}

// --------------------------------------------------------------------
// clear the collection of the cookie by key
// --------------------------------------------------------------------
function clearCookie(cookieKey) {
    // empty the cookie value
    $.cookie(cookieKey, "");
}


// ------------------------------------------------------------------------
// handles Reset Coding Structure button
// ------------------------------------------------------------------------

function resetCode()
{
    confirmWindowSize(
    "Resetting the Coding Structure will remove all existing codes as well as ALL COMMENTS on letters in this comment period.  Are you sure you want to do this?",
    "submitCodeInput(\"Reset\")", "", 300, 100);
    return false;
}



//-->