﻿<!--

$(document).ready(function() {
    var checkbox = $("#NoResponseRequired");

    showHideReasons(checkbox.is(':checked'));
});

// -----------------------------------------------------------------------
// Set no response reason area visibility based on input checked value
// -----------------------------------------------------------------------
function showHideReasons(checked) {
    // show the no response reason dropdown if checked
    if (checked)
    {
        $("#lblNoResponseReasonId").show();
        $("#ddlNoResponseReasonId").show();
    }
    else
    {
        $("#lblNoResponseReasonId").hide();
        $("#ddlNoResponseReasonId").hide();

        // set the dropdown value
        var dropDownList = $("#NoResponseReasonId").data("tDropDownList");

        if (dropDownList != null)
        {
            dropDownList.value("");
        }
    }
}

// ----------------------------------------------------------
// Handles delete comment onclick event
// ----------------------------------------------------------
function deleteComment(commentId) {
    var projectId = $("#ProjectId").val();
    var phaseId = $("#PhaseId").val();

    // send the comment deletion request
    window.location.href = "/Project/" + projectId + "/Phase/" + phaseId + "/Comment/Delete/" + commentId.toString();
}

// ----------------------------------------------------------
// Handles no response checkbox on click event
// ----------------------------------------------------------
function onCheckedNoResponse(checkbox) {
    var checked = $(checkbox).is(':checked');

    showHideReasons(checked);
}

//-->