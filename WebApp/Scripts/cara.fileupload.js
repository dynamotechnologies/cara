﻿<!--

$(document).ready(function () {
        FileUploadInit();
    });

function FileUploadInit()
{
        for (var i = 1; i <= 20; i++) {
            var fileId = "#fileUpload" + i;

            // if the area exists
            if ($(fileId).length > 0) {
                var showId = "#show" + i;
                var hideId = "#hide" + i;

                // show the first upload control
                if (i == 1)
                {
                    $(fileId).show();
                }

                // add click event for "-" button
                if ($(hideId).length > 0) {
                    $(hideId).click(function (event) {
                        event.preventDefault();
                        // set file upload control id
                        var id = "#" + event.target.id.replace("hide", "fileUpload");

                        // clear the selection
                        nid = $(id);
                        nid.replaceWith(nid.clone(true));
                        // hide the control
                        $(id).hide();
                    });
                }

                // add click event for "+" button
                if ($(showId).length > 0) {
                    $(showId).click(function (event) {
                        // parse the number and find the next index
                        var i = parseInt(event.target.id.replace("show", "")) + 1;

                        event.preventDefault();
                        $("#fileUpload" + i).show();
                    });
                }
            }
        }
}

//-->