﻿<!--

// ------------------------------------------------------------------------
// handles the onload event of the Grid on the Letter list page
// ------------------------------------------------------------------------
function resetGridPager() {
    var grid = $("#List").data("tGrid");
    var firstItem = grid.firstItemInPage();
    var totalItems = grid.total;

    if (totalItems < firstItem)
    {
      grid.pageTo(1);
    }
}

//-->