﻿<!--

var sessionRefreshUrl = "/SessionRefresh.aspx";  // This variable is not used, but its value is the action for the form on this page
var sessionWarnTimer;
var sessionFinalTimer;
var warningWindow;

// Clears the warning and final timers 
function clearTimers() {
      clearTimeout(sessionWarnTimer);
      clearTimeout(sessionFinalTimer);
}

// Starts the session warning timer
// If this window is a popup window, it clears the parent timers
function sessionStartTimer() {
        clearTimers();

        // Close the warning window if it is still open.
        if (warningWindow != null && !warningWindow.closed) {
                warningWindow.close();
        }
        
        sessionWarnTimer = setTimeout("sessionWarn()", sessionTimeout - sessionWarning);
}

// Set the final timer and warn the user that the session is going to expire
function sessionWarn() {
        sessionFinalTimer = setTimeout("closeWarningWindow()", sessionWarning);
        warnUser();
}

// Opens the window warning the user that the session is going to expire
function warnUser() {
        warningWindow = window.open("", "refreshWin", "resizable,height=140,width=420");
        //warningWindow = openWindow ("", "refreshWin", "height=172,width=420", 20, 20, self)
        warningWindow.document.write('<!DOCTYPE html public "-//w3c//dtd html 4.0 transitional//en" >\n');
        warningWindow.document.write('<html>\n');
        warningWindow.document.write('  <head>\n');
        warningWindow.document.write('    <title>Session Timeout Warning</title>\n');
        warningWindow.document.write('    <link rel="stylesheet" type="text/css" href="/Content/cara.css" >\n');
        warningWindow.document.write('  </head>\n');
        warningWindow.document.write('  <body>\n');
        warningWindow.document.write('   <form action=' + sessionRefreshUrl + ' name="refreshForm" method="post">\n');
        warningWindow.document.write('    <table border="0" cellpadding="0" cellspacing="0" width="100%" summary="This table is used to format the content area">\n');
        warningWindow.document.write('      <tr>\n');
        warningWindow.document.write('        <td width="100%">\n');
        warningWindow.document.write('            <table class="filterArea" border="0" cellpadding="0" cellspacing="0" width="95%" summary="This table is used to format the content area">\n');
        warningWindow.document.write('              <tr>\n');
        warningWindow.document.write('                <td width="100%" class="text">\n');
        warningWindow.document.write('                    Your session is about to expire because of extended inactivity.  Click the <b>OK</b> button to continue working.<br><br>\n');
        warningWindow.document.write('                                  <a class="button" href="javascript:refreshForm.submit()">OK</a>&nbsp;\n');
        warningWindow.document.write('                                  <a class="button" href="javascript:self.close()">Cancel</a>\n');
        warningWindow.document.write('                </td>\n');
        warningWindow.document.write('              </tr>\n');
        warningWindow.document.write('            </table>\n');
        warningWindow.document.write('        </td>\n');
        warningWindow.document.write('      </tr>\n');
        warningWindow.document.write('    </table>\n');
        warningWindow.document.write('   </form>\n');
        warningWindow.document.write('  </body>\n');
        warningWindow.document.write('</html>\n');

        warningWindow.document.close();
}

// Closes the warning window if the final timer completes
function closeWarningWindow() {
        warningWindow.close();
}

//-->
