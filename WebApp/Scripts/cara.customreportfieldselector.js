﻿<!--
function TextFieldKeyPress()
{
    var x;
    if(window.event) // IE8 and earlier
	{
	    x=event.keyCode;
	}
    else if(event.which) // IE9/Firefox/Chrome/Opera/Safari
	{
	    x=event.which;
	}
    if (x==13)
    {
        AddStringValue(1);
    }
}
function AddStringValue(include)
{
    var fieldId = document.getElementById("FieldId").value;
    var value = document.getElementById("StringFieldValue").value;
    var collapsed = document.getElementById("CollapsedListIds").value;
    document.getElementById("StringFieldValue").value = "";
    $.ajax({
      dataType: "json",
      url: "/Report/AddFieldValue",
      data: {fieldId:fieldId, not:include==0, value:value, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
        }}); 
}
function AddDateValue()
{
    var from = document.getElementById("FromDateFieldValue").value;
    var to = document.getElementById("ToDateFieldValue").value;
    var collapsed = document.getElementById("CollapsedListIds").value;
    if (from == "" && to == "")
    {
        alertWindow("Please enter a \"from\" and/or \"to\" date");
        return;
    }
    if (!(from == "") && !(to == ""))
    {
        var fromDate = new Date(from);
        var toDate = new Date(to);
        if (toDate < fromDate)
        {
            alertWindow("To date can not be before From date");
            return;
        }
    } 
    var fieldId = document.getElementById("FieldId").value;
    var not = false;
    document.getElementById("FromDateFieldValue").value = "";
    document.getElementById("ToDateFieldValue").value = "";
    $.ajax({
      dataType: "json",
      url: "/Report/AddFieldValue",
      data: {fieldId:fieldId, not:not, from:from, to:to, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
        }}); 
}

function AdddropdownValue(include)
{
    var fieldId = document.getElementById("FieldId").value;
    var dropdown = $("#DropdownFieldValue").data("tDropDownList");
    var value = dropdown.value();
    var name = dropdown.text();
    var collapsed = document.getElementById("CollapsedListIds").value;
    dropdown.select(0);
    $.ajax({
      dataType: "json",
      url: "/Report/AddFieldValue",
      data: {fieldId:fieldId, not:include==0, value:value, name:name, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
            UpdateAndDisplay(data);
        }});
}

function AddUnitValue()
{
    var fieldId = document.getElementById("FieldId").value;
    var unitId = document.getElementById("UnitId").value;
    var collapsed = document.getElementById("CollapsedListIds").value;
    if (unitId == "")
    { return; }
    var not = false;

    var name = document.getElementById("UnitName").value;
    $.ajax({
      dataType: "json",
      url: "/Report/AddFieldValue",
      data: {fieldId:fieldId, not:not, value:unitId, name:name, collapsedListIds:collapsed},
      cache: false,
      success:
        function (data)
        {
             document.getElementById("UnitId").value = "";
             document.getElementById("UnitName").value = "";
            UpdateAndDisplay(data);
        }});
}

function FieldSelect()
{
    var value = $("#FieldId").data("tDropDownList").value();
    var fieldname = $("#FieldId").data("tDropDownList").text();
    document.getElementById("TextFieldDiv").setAttribute("style", "display: none");
    document.getElementById("StringFieldValue").value = "";
    document.getElementById("DropdownFieldDiv").setAttribute("style", "display: none");
    $("#DropdownFieldValue").data("tDropDownList").select(0);
    document.getElementById("TreeDropdownFieldDiv").setAttribute("style", "display: none");
    document.getElementById("UnitName").value = "";
    document.getElementById("UnitId").value = "";
    document.getElementById("DateFieldDiv").setAttribute("style", "display: none");
    if (value == 0)
    {return;}

    $.getJSON("/Report/GetFieldMetaData?fieldId=" + value, function(data)
        {
            var dataType = data.dataType;
            var showdiv = "";
            var labelid = "";
            switch (dataType)
            {
                case "dropdown":
                    showdiv="DropdownFieldDiv";
                    labelid = "DropdownFieldLabel";
                    break;
                case "string":
                case "number":
                    showdiv="TextFieldDiv";
                    labelid = "StringFieldLabel";
                    document.getElementById("textFieldTool").setAttribute("title", data.toolTip);
                    document.getElementById("textFieldTool").setAttribute("alt",fieldname);
                    break;
                case "date":
                    showdiv="DateFieldDiv";
                    break;
                case "treedropdown":
                    showdiv="TreeDropdownFieldDiv";
                    break;
            }
            if (dataType != ""){document.getElementById(showdiv).removeAttribute("style");}
            if (labelid != "")
            {
                var labelElement = document.getElementById(labelid)
                if (labelElement.textContent == undefined)
                { labelElement.text = fieldname; }
                else
                {labelElement.textContent = fieldname;}
            }
            if (dataType == "dropdown")
            {
                $("#DropdownFieldValue").data("tDropDownList").dataBind(data.values);
            }
        });
}

function UnitSelected()
{
    closeUnitTreeSelector();
    AddUnitValue();
}

function ShowUnitDialog()
{
    document.getElementById("SelectUnitTop").setAttribute("onclick","UnitSelected()");
    document.getElementById("SelectUnitBottom").setAttribute("onclick","UnitSelected()");
    $('#UnitSelectorWindow').data('tWindow').center().open();
}
//-->