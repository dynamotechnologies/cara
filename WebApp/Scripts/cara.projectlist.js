﻿<!--
// -------------------------------------------------------------------
// Handles browser window resize event
// -------------------------------------------------------------------
$(window).resize(function() {
    var minWidth = 1400;
    var windowWidth = $(window).width();
    
    // if the window width is smaller than the min. width, set the window width to the min. width
    windowWidth = (windowWidth < minWidth ? minWidth : windowWidth);
        
    $("#header").width(windowWidth);
    $("#main").width(windowWidth);
    $("#footer").width(windowWidth);
    
});

//jQuery.fn.zoom = function(fn) {
//  jQuery(document).keydown(function(e){
//    switch (true) {
//      case jQuery.browser.mozilla || jQuery.browser.msie :
//        if (e.ctrlKey && (
//          e.which == 187 ||
//          e.which == 189 ||
//          e.which == 107 ||
//          e.which == 109 ||
//          e.which == 96  ||
//          e.which == 48
//        )) fn();
//        break;
//      case jQuery.browser.opera :
//        if (
//          e.which == 43 ||
//          e.which == 45 ||
//          e.which == 42 ||
//          (e.ctrlKey && e.which == 48)
//        ) fn();
//        break;
//      case jQuery.browser.safari :
//        if (e.metaKey && (
//          e.charCode == 43 ||
//          e.charCode == 45
//        )) fn();
//        break;
//    }
//    return;
//  });

//  jQuery(document).bind('mousewheel', function(e){
//    if (e.ctrlKey) fn();
//  });

//  jQuery(document).bind('DOMMouseScroll', function(e){
//    if (e.ctrlKey) fn();
//  });
//};

// -------------------------------------------------------------------
// Handles browser window zoom event
// -------------------------------------------------------------------
//jQuery().zoom(function() {
//    setColumnHeaderGroups();
//});

function setColumnHeaderGroups() {
    // offset for header group margins (default values are for IE8 and FF 3.6)
    var offsetPals = 3;
    var offsetCara = 1;
    var offsetLetterStatus = 2; 
    var offsetLetterType = 2;
    
    if ($.browser.msie)
    {
        // IE7
        if ($.browser.version == "7.0")
        {
            offsetPals = -9;
            offsetCara = -2;
            offsetLetterStatus = -10;
        }
    }
    else
    {
        // chrome and safari
        var browserName = jQuery.uaMatch(navigator.userAgent).browser;
        if (browserName == "webkit")
        {
            offsetPals = 4;
            offsetCara = 1;
            offsetLetterStatus = 4;
        }
    }

    // initialize the col group header
    var headers = $("#ProjectList").find("th.t-header");
    var headerwidth = new Array();
    var i;
    for (i= 0; i<13; i++)
    {
        headerwidth[i] = $(headers[0]).attr("clientWidth");
    }

    // set the width based on client width of each subheaders (note: the width value is way off and cannot be used for calculation)
    var palsWidth = $(headers[0]).attr("clientWidth") + $(headers[1]).attr("clientWidth") + $(headers[2]).attr("clientWidth") + $(headers[3]).attr("clientWidth") + offsetPals;
    var caraOffset = $(headers[4]).attr("clientWidth") + offsetCara;
    var statusOffest = $(headers[5]).attr("clientWidth") + $(headers[6]).attr("clientWidth") + $(headers[7]).attr("clientWidth") + $(headers[8]).attr("clientWidth") + offsetLetterStatus;
    var typeOffset = $(headers[9]).attr("clientWidth") + $(headers[10]).attr("clientWidth") + $(headers[11]).attr("clientWidth") + $(headers[12]).attr("clientWidth") + offsetLetterType;
    $("#cgPalsInfo").width(palsWidth);
    $("#cgCaraInfo").width(caraOffset);
    $("#cgLetterStatus").width(statusOffest);
    $("#cgLetterType").width(typeOffset);
    //alert("Pals=" + palsWidth + ", cara=" + caraOffset + ", status=" + statusOffest + ", type=" + typeOffset);
    
 
}

function AdjustMyProjectHeaders(e)
{
      var tabStrip = $("#HomeTabs").data("tTabStrip");
      
      tabStrip.select($(".t-item", tabStrip.element)[1]);
      setColumnHeaderGroups();
      tabStrip.select($(".t-item", tabStrip.element)[0]);
      
}

//-->