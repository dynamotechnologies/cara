﻿<!--

function deleteMyReport(myReportId) {
    // set up parameters
    var parms = {
        myReportId: myReportId,
        viewOption: myReportDropDownList().value()
    };

    // ajax action call to update my report list
    $.post("/Report/DeleteMyReport",
        parms,
        function(data)
        {
            $("#myReportListArea").html(data);
        },
        "html");
}

function updateMyReportList() {
    // set up parameters
    var parms = {
        viewOption: myReportDropDownList().value()
    };

    // ajax action call to get my report list based on options
    $.post("/Report/GetMyReportList",
        parms,
        function(data)
        {
            $("#myReportListArea").html(data);
        },
        "html");
}

function myReportDropDownList() {
    return $("#MyReportListOption").data("tDropDownList");
}

//-->