﻿<!--

var ActionTypes = { "Default": "", "Next": "Next" };
var action = ActionTypes.Next;

// -------------------------------------------------------------
// handles the keypress event of the document
// -------------------------------------------------------------
$(document).keypress(function(e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if (action == ActionTypes.Next)
        {
            $("#btn" + action).focus();
            return true;
        }
    }
});

function showHide(radio) {
    var selected = radio.value;

    if (selected == "Add") {
        $("#addArea")[0].style.display = 'block';
        $("#importArea")[0].style.display = 'none';
    }
    else if (selected == "Reuse") {
        $("#addArea")[0].style.display = 'none';
        $("#importArea")[0].style.display = 'block';
    }
}

//-->