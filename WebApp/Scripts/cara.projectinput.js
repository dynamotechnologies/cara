﻿<!--

var ActionTypes = { "Default": "", "SearchUsers": "SearchUsers", "SearchProjects" : "SearchProjects" };
var action = ActionTypes.Default;

// -------------------------------------------------------------
// handles the keypress event of the text inputs
// -------------------------------------------------------------
$(document).ready(function() {
    $("#ProjectManagerFirstName").keypress(function(e) {
        action = ActionTypes.SearchUsers;
        return true;
    });

    $("#ProjectManagerLastName").keypress(function(e) {
        action = ActionTypes.SearchUsers;
        return true;
    });

    $("#ProjectNameOrId").keypress(function(e) {
        action = ActionTypes.SearchProjects;
        return true;
    });

    // set page focus
    setPageFocus();

    updateCountdown();
    $('#NonPalsDescription').change(updateCountdown);
    $('#NonPalsDescription').keyup(updateCountdown);

    $("input[name='rbUsername']").change(addProjectManager);
});

// -------------------------------------------------------------
// handles the keypress event of the document
// -------------------------------------------------------------
$(document).keypress(function(e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if (action == ActionTypes.SearchUsers || action == ActionTypes.SearchProjects)
        {
            $("#btn" + action).focus();
            return true;
        }
    }
});

// -------------------------------------------------------------
// set the page focus by the submit action
// -------------------------------------------------------------
function setPageFocus() {
    // User search, set focus to the add PM button
    if ($("#Action").val() == "Search")
    {
        // focus on the add PM button
        $("#btnAddPm").focus();
    }
}

function showHide(radio) {
    var selected = radio.value;

    if (selected == "True")
    {
        $("#palsProject")[0].style.display = 'block';
        $("#nonPalsProject")[0].style.display = 'none';
    }
    else if (selected == "False")
    {
        $("#palsProject")[0].style.display = 'none';
        $("#nonPalsProject")[0].style.display = 'block';

        alertWindowSize("By selecting 'No' you are stating that this project will never be added to PALS. If your project will be tracked in PALS in the future, you should go back to PALS, enter the project, then come back to CARA.", 400, 85);
    }
}

function addProjectManager() {
    // set project manager user id in the hidden field
    var username = $("input[name='rbUsername']:checked").val();

    if (username != null && username != "")
    {
        $("#ProjectManagerUsername").val(username);
    }
    else
    {
        alertWindow("Please select a project manager to add.");
    }
}

function updateCountdown()
{
    var maxLength = 4000;
    var remaining = maxLength - jQuery('#NonPalsDescription').val().length;
    jQuery('#countdown').text(remaining + ' of ' + maxLength + ' characters remaining.');

    if (remaining < 0)
    {
        jQuery('#countdown').addClass("error");
    }
    else
    {
        jQuery('#countdown').removeClass("error");
    }
}

//-->