﻿<!--

var ActionTypes = { "Default": "", "Next": "Next" };
var action = ActionTypes.Default;

// -------------------------------------------------------------
// handles the keypress event of the text inputs
// -------------------------------------------------------------
$(document).ready(function() {
    $("input[type=text]").keypress(function(e) {
        action = ActionTypes.Next;
        return true;
    });

    // Set End Date
    setEndDate();
    //setObjectionResponseNewDueDate($("#Phase_ObjectionResponseDue").val());
    alwaysOpen();
    var countdown = new Countdown("Phase_ObjectionResponseExtendedJustification", "countdown", 1000).Init();
});

// -------------------------------------------------------------
// handles the keypress event of the document
// -------------------------------------------------------------
$(document).keypress(function(e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if (action == ActionTypes.Next)
        {
            $("#btn" + action).focus();
            return true;
        }
    }
});

function setDuration(e) {
    // get the current phase type selection
    var defaultDuration = 0;

    // set the duration value based on selected phase type
    var selectedText = $("#Phase_PhaseTypeId").data("tDropDownList").text();
    if (selectedText == "Objection")
    {
        defaultDuration = $("#DefaultObjectionDuration").val();
    }
    else if (selectedText == "Notice of Availability")
    {
        defaultDuration = $("#DefaultDuration").val();
    }
    
    if (selectedText != $("#Phase_PhaseTypeName").val())
    {
        $("#Duration").val(defaultDuration > 0 ? defaultDuration.toString() : "");
    }

    setEndDate();
    setPublicViews();
    setObjectionFieldVisibility();
}

function setObjectionFieldVisibility()
{
    var selectedText = $("#Phase_PhaseTypeId").data("tDropDownList").text();
    if (selectedText == "Objection")
    {
        $(".phaseObjectionField").addClass("showPhaseObjectionField").removeClass("phaseObjectionField");
    }
    else
    {
        $(".showPhaseObjectionField").addClass("phaseObjectionField").removeClass("showPhaseObjectionField");
    }
}

function setPublicViews(e)
{
    var selectedText = $("#Phase_PhaseTypeId").data("tDropDownList").text();
    if (selectedText == "Objection")
    {
        $("input[name='Phase.ReadingRoomActive']").val(["False"]);
        $("input[name='Phase.PublicCommentActive']").val(["False"]);
    }
}

function setEndDate(e)
{
    var datepickerValue = $("#Phase_CommentStart").attr("value");
    var startDate = new Date(datepickerValue);
    var duration = $("#Duration");
    var endDate = $("#EndDate");
    var objDueDate = $("#ObjectionResponseNewDueDate");
    var objDuration = $("#DefaultObjectionDuration");
    var objExtended = $("#Phase_ObjectionResponseExtendedBy");
    var phaseTypeId = $("#Phase_PhaseTypeId");
    var cannotBeDetermined = isNaN(startDate) || isNaN(duration.val()) ;

    if (cannotBeDetermined)
    {
        endDate.text("Cannot be determined");
    }
    else
    {
        durationValue = duration.val() == 0 ? 0 : duration.val();

        var msPerDay = 24 * 60 * 60 * 1000;
        var endDateMsValue = startDate.getTime() + durationValue * msPerDay; // Converts days to milliseconds
        var endDateValue = new Date(endDateMsValue);

        // Handle daylight savings time
        if (endDateValue.getHours() == 23)
        {
            endDateMsValue = endDateMsValue + (msPerDay/24);
            endDateValue = new Date(endDateMsValue);
        }
        else if (endDateValue.getHours() == 1)
        {
            endDateMsValue = endDateMsValue - (msPerDay/24);
            endDateValue = new Date(endDateMsValue);
        }

        var weekday=new Array(7);
        weekday[0]="Sun";
        weekday[1]="Mon";
        weekday[2]="Tue";
        weekday[3]="Wed";
        weekday[4]="Thu";
        weekday[5]="Fri";
        weekday[6]="Sat";

        var day = weekday[endDateValue.getDay()];

        var month = endDateValue.getMonth() + 1;
        var monthString = month < 10 ? "0" + month : month;

        var dateValue  = endDateValue.getDate();
        var dateValueString = dateValue  < 10 ? "0" + dateValue : dateValue;

        var year = endDateValue.getFullYear();

        var dateString = monthString + "/" + dateValueString + "/" + year;

        endDate.text(dateString + " (" + day + ")");

        // Compute changes to objection response due date
        if (phaseTypeId.val() == 3)
        {
            var objDurationMs = (parseInt(objDuration.val()) + parseInt(objExtended.val())) * msPerDay;
            var objDueDateValue = new Date(DlstAdjust(endDateMsValue + objDurationMs));
            if (objDueDateValue.getDay() == 0 || objDueDateValue.getDay() == 6)
            {
                $("#DurationExtMessage").show();
                var newObjDueDateMs = DlstAdjust(endDateMsValue + objDurationMs + msPerDay);
                objDueDateValue = new Date(newObjDueDateMs);
                var newExtensionDays = parseInt(objExtended.val()) + 1;
                if (objDueDateValue.getDay() == 0)
                {
                    newObjDueDateMs = DlstAdjust(newObjDueDateMs + msPerDay);
                    objDueDateValue = new Date(newObjDueDateMs);
                    newExtensionDays++;
                }
                if (parseInt(objExtended.val()) != 0)
                {
                    objExtended.val(newExtensionDays);
                }
            }
            else
            {
                $("#DurationExtMessage").hide();
            }
            day = weekday[objDueDateValue.getDay()];

            month = objDueDateValue.getMonth() + 1;
            monthString = month < 10 ? "0" + month : month;

            dateValue  = objDueDateValue.getDate();
            dateValueString = dateValue  < 10 ? "0" + dateValue : dateValue;

            year = objDueDateValue.getFullYear();

            dateString = monthString + "/" + dateValueString + "/" + year;

            objDueDate.text(dateString + " (" + day + ")");
        }
    }
}

function DlstAdjust(dateMsValue)
{
    var msPerDay = 24 * 60 * 60 * 1000;
    var retVal = dateMsValue;
    var retDateVal = new Date(retVal);
    if (retDateVal.getHours() == 23)
    {
        retVal = retVal + (msPerDay/24);
    }
    else if (retDateVal.getHours() == 1)
    {
        retVal = retVal - (msPerDay/24);
    }
    return retVal;
    
}

function setObjectionResponseNewDueDate(dueDate)
{
    var startDate = new Date(dueDate);
    var duration = $("#Phase_ObjectionResponseExtendedBy");
    var endDate = $("#ObjectionResponseNewDueDate");
    var cannotBeDetermined = isNaN(startDate) || isNaN(duration.val()) ;

    if (cannotBeDetermined)
    {
        endDate.text("Cannot be determined");
    }
    else
    {
        durationValue = duration.val() == 0 ? 0 : duration.val();

        var endDateValue = CalcEndDateValue(startDate, durationValue);

        if (endDateValue.getDay() == 0 || endDateValue.getDay() == 6)
        {
            $("#DurationExtMessage").show();
            var newDuration = 1 + parseInt(duration.val());
            durationValue++;
            if (endDateValue.getDay() == 6)
            {
                newDuration++;
                durationValue++;
            }
            duration.val(newDuration);
            endDateValue = CalcEndDateValue(startDate, durationValue);
        }
        else
        {
            $("#DurationExtMessage").hide();
        }

        var weekday=new Array(7);
        weekday[0]="Sun";
        weekday[1]="Mon";
        weekday[2]="Tue";
        weekday[3]="Wed";
        weekday[4]="Thu";
        weekday[5]="Fri";
        weekday[6]="Sat";

        var day = weekday[endDateValue.getDay()];
        var month = endDateValue.getMonth() + 1;
        var monthString = month < 10 ? "0" + month : month;

        var dateValue  = endDateValue.getDate();
        var dateValueString = dateValue  < 10 ? "0" + dateValue : dateValue;

        var year = endDateValue.getFullYear();

        var dateString = monthString + "/" + dateValueString + "/" + year;

        endDate.text(dateString + " (" + day + ")");
    }
}

function CalcEndDateValue(startDate, durationValue)
{

        var msPerDay = 24 * 60 * 60 * 1000;
        var endDateMsValue = startDate.getTime() + durationValue * msPerDay; // Converts days to milliseconds
        var endDateValue = new Date(endDateMsValue);

        // Handle daylight savings time
        if (endDateValue.getHours() == 23)
        {
            endDateMsValue = endDateMsValue + (msPerDay/24);
            endDateValue = new Date(endDateMsValue);
        }
        else if (endDateValue.getHours() == 1)
        {
            endDateMsValue = endDateMsValue - (msPerDay/24);
            endDateValue = new Date(endDateMsValue);
        }
        return endDateValue;
}

var savedDurationValue;
function alwaysOpen()
{
    var isAlwaysOpen = $("#Phase_AlwaysOpen").is(":checked");
    var duration = $("#Duration");
    savedDurationValue = duration.val();
    var endDate = $("#EndDate");

    if (isAlwaysOpen)
    {
        duration.val("");
        duration.attr("disabled", "disabled");
        endDate.text("N/A");
    }
    else
    {
        duration.val(savedDurationValue);
        duration.removeAttr("disabled");
        setEndDate();
    }
}

//-->