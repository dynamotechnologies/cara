﻿<!--

var alertDialog;
var dialogConfirm;

// -------------------------------------------------------------------
// Open window dialog by input url
// -------------------------------------------------------------------
function openWindow(windowName, contentUrl) {
    var tWindow = $("#" + windowName).data("tWindow");
  
    if (tWindow != null)
    {
        if (contentUrl != null)
        {
            tWindow.ajaxRequest(contentUrl);
        }
        tWindow.center().open();
    }
}

//*Added for comment code sort
// -------------------------------------------------------------------
// Open window dialog by input url 
// -------------------------------------------------------------------
function popupCenter(windowName, url, phaseId, letterCommentNumber) {
          
        var cWindow = $("#" + windowName).data("cWindow");
         if (phaseId === null || phaseId === '0') {
            phaseId = document.getElementById("PhaseId").value;
        }
        
        if (cWindow != null) {
            if (url != null) {
                cWindow.ajaxRequest(url + "?phaseId=" + phaseId + "&letterCommentNumber=" + letterCommentNumber);
            }
            cWindow.center().open();
        }
    }

// -------------------------------------------------------------------
// close window dialog by window name
// -------------------------------------------------------------------
function closeWindow(windowName) {
    var tWindow = $("#" + windowName).data("tWindow");

    if (tWindow != null) {
        tWindow.close();
    }
}

// -------------------------------------------------------------------
// Alert window dialog in standard size
// -------------------------------------------------------------------
function alertWindow(message) {
    alertWindowSize(message, 300, 75);
}

function alertWindowSize(message, width, height) {
    alertDialog = $.telerik.window.create({
        title: "Information",
        html: message + "<br/><br/>" + "<div id='windowAlert' align='center' onmouseover='this.focus();'>" +
            "<button class=button id=btnAlertOk onmouseover='this.focus();' onclick='alertDialog.destroy();return false;'>OK</button></div>",
        modal: true,
        resizable: false,
        draggable: false,
        width: width,
        height: height,
        visible: false,
        open: setAlertWindowFocus()
    })
    .data('tWindow');
          
   alertDialog.center().open();
}

// --------------------------------------------------------------------------------------
// Confirmation window dialog in standard size with "yes" callback function only
// --------------------------------------------------------------------------------------
function confirmWindow(message, callbackFunctionYes) {
   return confirmWindowSize(message, callbackFunctionYes, "", 300, 75);
}

// --------------------------------------------------------------------------------------
// Confirmation window dialog in standard size with "yes" callback function only
// --------------------------------------------------------------------------------------
function confirmWindowWithCancelCallBack(message, callbackFunctionYes, callbackFunctionNo) {
   return confirmWindowSize(message, callbackFunctionYes, callbackFunctionNo, 300, 75);
}

// -------------------------------------------------------------------
// Confirmation window dialog in input defined size
// -------------------------------------------------------------------
function confirmWindowSize(message, callbackFunctionYes, callbackFunctionNo, width, height) {
    dialogConfirm = $.telerik.window.create({
        title: "Confirmation",
        html: message + "?<br/><br/>" +
        "<div id='windowConfirm' align='center'>" +
        "<button class=button id=btnConfirmYes onclick='dialogConfirm.destroy();" + callbackFunctionYes + (callbackFunctionYes != "" ? ";" : "") + "return true;'>Yes</button>" +
        "    " +
        "<button class=button onclick='dialogConfirm.destroy();" + callbackFunctionNo + (callbackFunctionNo != "" ? ";" : "") + "return false;'>No</button></div>",
        modal: true,
        resizable: false,
        draggable: false,
        width: width,
        height: height,
        visible: false,
        open: setConfirmWindowFocus()
    })
    .data('tWindow');
          
   dialogConfirm.center().open();

   return false;
}

// ------------------------------------------------------------------------
// set focus of the window to the first ontrol available on the page
// ------------------------------------------------------------------------
function setWindowFocus(e) {
    setTimeout(
        function()
        {
            // select the first available input field
            var selector = $("#" + e.target.id).find(":input:visible:enabled:first");

            // select the first available button if input field does not exist
            if (selector == null || selector.length == 0)
            {
                selector = $("#" + e.target.id).find(".button:first");
            }

            // trigger the focus
            selector.trigger("focus");
        }
        , 1500);
}

// ------------------------------------------------------------------------
// set focus of the alert window to the ok button
// ------------------------------------------------------------------------
function setAlertWindowFocus() {
    triggerControlFocus("windowAlert", "btnAlertOk");
}

// ------------------------------------------------------------------------
// set focus of the alert window to the ok button
// ------------------------------------------------------------------------
function setConfirmWindowFocus() {
    triggerControlFocus("windowConfirm", "btnConfirmYes");
}

// ------------------------------------------------------------------------
// trigger focus event of the control by id inside a div
// ------------------------------------------------------------------------
function triggerControlFocus(divNameOrId, controlNameOrId) {
    setTimeout(
        function()
        {
            $("#" + divNameOrId).find("#" + controlNameOrId).trigger("focus");
        }
        , 500);
}

//-->
