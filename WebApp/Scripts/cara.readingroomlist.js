﻿<!--

var ButtonText = { "CheckAll": "Check All", "UncheckAll": "Uncheck All" };
var toggledLetters = new Array();
var letterIds = new Array();
var isChanged=false;
var loading = false;

//------------------------------------------------------------------
// Handles displaying alert when navigating away from the page
//------------------------------------------------------------------
function handleFormChanged() {
    // breadcrumb links, top nav links, log out button, filter button, show all letters button, left nav links
    if(!isChanged)
    {
        $("div.breadcrumb a, #navigation a, #ProjectNav a, #btnLogout, #linkPublicReadingRoom")
            .click(function () { confirmNavigation(this); return false; });
        $("#List a").click(function () { confirmNavigation(this); return false; });
        isChanged = true;
    }
}

// ------------------------------------------------------------------------
// opens content changed warning dialog box
// ------------------------------------------------------------------------
function confirmNavigation(link) {
    confirmWindowSize("There may be unsaved changes on this page. If you continue, your changes will be lost. Do you wish to continue", "goNavigation(\"" + link + "\")", "", 300, 80);
}

// ------------------------------------------------------------------------
// executed if "Yes" is clicked in the confirmWindow via callback
// ------------------------------------------------------------------------
function goNavigation(link) {
    window.location.href = link;
    return true;
}


// ------------------------------------------------------------
// Handles on click event of the "check all" grid command
// ------------------------------------------------------------
function onClickCheckAll(button, isMgmt) {
    var text = $(button).text();
    var check = false;

    if (text == ButtonText.CheckAll)
    {
        $(button).text(ButtonText.UncheckAll);
        check = true;
    }
    else
    {
        $(button).text(ButtonText.CheckAll);
    }

    // check/uncheck all checkboxes
    $(':checkbox').each(function()
        {
            // perform action only if the checkbox is enabled
            if (!$(this).attr("disabled"))
            {
                $(this).attr("checked", check);

                // raise proper checkbox check event
                if (isMgmt)
                {
                    onClickCheckbox(this);
                }
                else
                {
                    onClickCheckboxDmd(this);
                }
            }
        }
    );
}

// --------------------------------------------------------------
// handles the Search Results Per Page dropdown change event
// --------------------------------------------------------------
function onChangeSearchResultsPerPage(e) {
    var paramName = "SearchResultsPerPage";
    var url = window.location.href;

    var index = url.indexOf(paramName);

    // remove the query string param if already exists
    if (index > 0)
    {        
        url = url.substring(0, index - 1);

        var endIndex = url.indexOf("&", index + 1);

        if (endIndex > -1)
        {
            url = url + url.substring(endIndex);
        }
    }

    // add the param
    url = url + "&" + paramName + "=" + e.value;

    // redirect to the url
    window.location.href = url;
}

// -------------------------------------------------------
// update letter id list with the letter checkbox value
// -------------------------------------------------------
function onClickCheckbox(checkbox) {
    // get the letter id value
    var letterId = $(checkbox).val();
    var checked = $(checkbox).is(':checked');    

    if (letterId != null)
    {
        var dropDownList = getDropDownList(letterId);
        var textbox = getTextbox(letterId);

        // if checkbox is checked, reset the unpublished reason controls
        if (checked)
        {
            // reset the unpublish reason fields
            if (dropDownList != null && textbox != null)
            {
                dropDownList.value("");
                textbox.hide();
                textbox.val("");
            }
        }

        // update the letter
        updateToggledLetters(letterId, checked, dropDownList.value(), textbox.val());
    }
    handleFormChanged();
}

// -------------------------------------------------------
// update letter id list with the Letter ID checkbox value
// -------------------------------------------------------
function onClickCheckboxDmd(checkbox) {
    // get the letter id value
    var letterId = $(checkbox).val();
    var checked = $(checkbox).is(':checked');

    if (letterId != null && letterId != "")
    {    
        // if letter id is not found in the list, add the item if it's checked
        if (checked)
        {
            letterIds.push(letterId);
        }
        // if letter id exists in the list, rmeove the item if it's unchecked
        else
        {
            letterIds.splice( $.inArray(letterId, letterIds), 1 );
        }
    }
}

// ------------------------------------------------------------------------------------
// updates the toggled letter collection for the input letter id with the new values
// ------------------------------------------------------------------------------------
function updateToggledLetters(letterId, checked, unpublishedReasonId, unpublishedReasonOther) {
    var found = false;

    if (letterId != null)
    {
         $.each(toggledLetters, function(index)
        {
            // if letter is found in the list, update the flag
            if (letterId == this.LetterId.toString())
            {
                this.Checked = checked.toString();
                this.UnpublishedReasonId = unpublishedReasonId;
                this.UnpublishedReasonOther = unpublishedReasonOther;
                found = true;                
            }

            return (!found);
        });

        // if letter is not found in the list, add the item
        if (!found)
        {
            var item = { "LetterId": letterId.toString(), "Checked": checked.toString(), "UnpublishedReasonId" : unpublishedReasonId, "UnpublishedReasonOther" : unpublishedReasonOther };
            toggledLetters.push(item);
        }

        // update the updated letter list
        updateLetterList();
    }
}

// -----------------------------------------------------------------------------------------------------------------------
// handles the onChange event of the dropdownlist to enable/disable the checkbox/textbox based on the dropdown state
// -----------------------------------------------------------------------------------------------------------------------
function onChangeDdl(e) {
    var id = e.target.id;
    // get the letter id from the control name
    var letterId = id.split("_")[1];

    if (letterId != "")
    {
        // get the controls
        var textbox = getTextbox(letterId);
        var checked = getCheckbox(letterId).attr("checked");

        // if a unpublished reason is selected, uncheck the publish checkbox
        if (checked && e.value != "")
        {
            checked = false;
            getCheckbox(letterId).attr("checked", checked);
        }

        // if the "other" option is chosen, display the textbox, else clear the textbox and hide it
        if (e.value == "5" && textbox != null)
        {
            textbox.show();                
        }
        else
        {
            textbox.hide();
            textbox.val("");
        }
        
        // update the letter
        updateToggledLetters(letterId, checked, e.value, textbox.val());
    }
    if (!loading)
    {
        handleFormChanged();
    }

    return true;
}

// ---------------------------------------------------------------------
// handles the onkeyup event of the textbox to update the letter data
// ---------------------------------------------------------------------
function onKeyUpTextbox(e) {
    var id = e.id;
    // get the letter id from the control name
    var letterId = id.split("_")[1];

    if (letterId != "")
    {
        // get the controls
        var dropDownList = getDropDownList(letterId);
        var checked = getCheckbox(letterId).attr("checked");

        // update the letter
        updateToggledLetters(letterId, checked, dropDownList.value(), e.value);
    }

    return true;
}

// ------------------------------------------------------------------------------
// update the updated letter list value of the form hidden field
// ------------------------------------------------------------------------------
function updateLetterList() {
    var arr = jQuery.map(toggledLetters, function(obj){
          return (obj.LetterId + ";" + obj.Checked + ";" + obj.UnpublishedReasonId + ";" + obj.UnpublishedReasonOther);
        });

    if (arr != null)
    {
        // update the hidden variable value
        $("#txtUpdatedLetterList").val(arr.join(","))
    }
}

// ------------------------------------------------------------------------------
// grid data bound method to handle the events for bounding the data
// ------------------------------------------------------------------------------
function onDataBound(e) {
    // handle the toggled letter checked values
    $(':checkbox').each(function()
        {
            // update the letter data item if the letter is found in the collection
            var letterId = $(this).val();
            var found = false;

            if (letterId != null)
            {
                $.each(toggledLetters, function(index)
                {
                    // if letter is found in the collection, update the data, else just use database value
                    if (letterId == this.LetterId.toString())
                    {                        
                        var dropDownList = getDropDownList(letterId);
                        if (dropDownList != null)
                        {
                            dropDownList.value(this.UnpublishedReasonId);
                        }

                        getCheckbox(letterId).attr("checked", this.Checked == "false" ? false : true);
                        getTextbox(letterId).val(this.UnpublishedReasonOther);

                        if (this.UnpublishedReasonId == "5")
                        {
                            getTextbox(letterId).show();
                        }

                        found = true;
                    }

                    return (!found);
                });
            }
        }
    );
}

// ------------------------------------------------------------------------------
// grid row data bound method to handle the events for bounding the row data
// ------------------------------------------------------------------------------
function onRowDataBound(e) {
    loading = true;
    var dataItem = e.dataItem;

    dataItem = dataItem || {};
    var letterId = dataItem.LetterId;

    if (letterId != null)
    {
        var dropDownList = getDropDownList(letterId);

        // fix the dropdownlist clienttemplate flaw from telerik to bind the selected value properly
        if (dropDownList != null)
        {
            // set the selected value to the reason ID value of the row
            dropDownList.value(dataItem.UnpublishedReasonId);
        }
    }
    loading = false;
}

// --------------------------------------------------
// open window to display unpublished message
// --------------------------------------------------
function openUnpublishedWindow() {
    alertWindowSize("Content that in some contexts can be offensive or threatening, or that may identify sensitive biological or cultural sites, is automatically flagged for human review prior to being posted on this web site. Most flagged comments are cleared for posting within a few days of being received.",
        400, 100);

        
    return false;
}

// --------------------------------------------------
// open windoe to display unavailable message
// --------------------------------------------------
function openUnavailableWindow() {
    alertWindowSize("It may take up to 24 hours for the letter you are trying to view to be available for download. Please try again later.",
        400, 100);
    return false;
}

    function submitRecaptcha() {
        var result = false;

        var parms = {
            responseValue: grecaptcha.getResponse()
        };

        // ajax action call to populate comment input area
        $.post("/Public/RecaptchaVerify",
            parms,
            function (data) {
                if (data != "" && data.toLowerCase() == "true") {
                    // close the window
                    closeWindowRecaptcha();
                    
                    // download the letter attachments
                    if ($("#DownloadLettersUrl").val() != "") {
                        window.location.href = $("#DownloadLettersUrl").val();
                    }
                }
                else {
                    alertWindow("You did not enter the reCAPTCHA correctly. Please try again.");
                    grecaptcha.reset();
                }
            },
            "html");

        return result;
    }

    function closeWindowRecaptcha() {
        Recaptcha.destroy();
        closeWindow("WindowRecaptcha");
        return false;
    }


// --------------------------------------------------
// handle download buttonc click event
// --------------------------------------------------
function downloadLetters() {
    // clear url value
    $("#DownloadLettersUrl").val("");

    if (letterIds != null && letterIds.length > 0)
    {
        var letterIdList = "";

        $.each(letterIds, function(index)
        {
            // if letter is found in the list, update the flag
            letterIdList = letterIdList + (letterIdList == "" ? "" : ",") + this;
        });

        //CARA-1288
        //var downloadUrl = window.location.href.replace("ReadingRoom", "/DownloadLetters") + "&letterIdList=" + letterIdList;
        var downloadUrl = window.location.href.replace(/\/ReadingRoom/i, "/DownloadLetters") + "&letterIdList=" + letterIdList;

        // if more than one letters is checked, load recaptcha before download
        if (letterIdList.indexOf(",") > 0)
        {
            // save the url so that the recaptcha control can access the action link after verification
            $("#DownloadLettersUrl").val(downloadUrl);

            // open the recaptcha window
            openWindow("WindowRecaptcha", "/Public/RecaptchaInput");
        }
        else
        {
            window.location.href = downloadUrl;
        }
    }
    else
    {
        alertWindow("Please select one or more letters to download.");
    }

    return false;
}

function getDropDownList(letterId) {
    return $("#ddlUnpublishedReason_" + letterId).data("tDropDownList");
}

function getCheckbox(letterId) {
    return $("#chkPublish_" + letterId);
}

function getTextbox(letterId) {
    return $("#txtUnpublishReasonOther_" + letterId);
}

//-->