﻿<!--
var expandAllText = "Expand All";
var collapseAllText = "Collapse All";
$(document).ready(function()
{
    $("div.breadcrumb a, #navigation a, #btnLogout, #ProjectNav a, a.button")
        .click(function ()
        {
            if ($("div.t-pager a").length > 0 && $("div.t-pager a:first").attr("href").search("TaskAssignmentContext=")>=0)
            {
                confirmNavigation(this);
                return false;
            }
            else
            {
                return true;
            }
        });

        //remove the default column built by Telerik detailview
        if ($("#List").length > 0){
            $("#List th.t-hierarchy-cell.t-header").remove();
            $("#List td.t-hierarchy-cell").remove();

            //remove excessive padding around header links preventing grid column resizing
            $("#List th.t-header").css("padding", "3px 8px 3px 8px");
            $("#List a.t-link").css("padding", "0 0 0 0");
        }
});

// ------------------------------------------------------------------------
// opens content changed warning dialog box
// ------------------------------------------------------------------------
function confirmNavigation(link) {
    confirmWindowSize("There may be unsaved changes on this page. If you continue, your changes will be lost. Do you wish to continue", "goNavigation(\"" + link + "\")", "", 300, 80);
}

// ------------------------------------------------------------------------
// executed if "Yes" is clicked in the confirmWindow via callback
// ------------------------------------------------------------------------
function goNavigation(link) {
    window.location.href = link;
    return true;
}

function updateCommentIdList(checkbox) {
    // get the comment id value
    var commentId = $(checkbox).val();
    var checked = $(checkbox).is(':checked');
    
    if (commentId != null)
    {
        if (checked)
        {
            // add the item to the id list
            $("#CommentIdList").val($("#CommentIdList").val() + ($("#CommentIdList").val() != "" ? "," : "") + commentId);
        }
        else
        {
            // remove it from the list if the item is unchecked
            removeIdFromList(commentId);
        }
        //TODO change commentId list in url
        changeQueryString("CommentIdList", $("#CommentIdList").val());
    }
}

function removeIdFromList(commentId) {
    // split the id list into an array
    var arr = $("#CommentIdList").val().split(",");

    if (arr != null)
    {
        // remove the comment id from the array (assume id is unique)
        arr.splice($.inArray(commentId, arr), 1);
    }

    // update the list
    $("#CommentIdList").val(arr.join(","));
}


function onDataBound(e) {
    $(':checkbox').each(function()
        {
            // check the checkbox if the comment id is in the list
            var commentId = $(this).val();
            if (commentId != null)
            {
                var arr = $("#CommentIdList").val().split(",");
                if (arr != null)
                {
                    // if the comment id is found in the array, check the checkbox
                    if ($.inArray(commentId.toString(), arr) != -1)
                    {
                        $("#chkCommentId_" + commentId).attr("checked", true);
                    };
                }
            }           
        }
    );
    
    if ($("#btnExpand").text() == collapseAllText)
    {
        $("#btnExpand").text(expandAllText);
        onClickExpandAll($("#btnExpand"));
    }
}

// ------------------------------------------------------------
// Handles on click event of the "expand all" grid command
// ------------------------------------------------------------
function onClickExpandAll(button) {
    var text = $(button).text();
    var expand = false;

    if (text == expandAllText)
    {
        $(button).text(collapseAllText);
        expand = true;
    }
    else
    {
        $(button).text(expandAllText);
    }

    var grid = $("#List").data("tGrid");

    if (grid != null)
    {
        grid.$rows().each(function () {
            if (expand)
            {
                grid.expandRow(this);
            }
            else
            {
                grid.collapseRow(this);
            }
        });
    }
}
// -->