﻿<!--
$(document).ready(function () {
    $("div.t-grid table tr:nth-child(even)").addClass("t-alt");
})

function deletePhase(projectId, phaseId) {
    // send the phase deletion request
    window.location.href = setActionUrl(projectId, phaseId, "Delete");
}

function archivePhase(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdateStatus", "type=Archive");
}

function activatePhase(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdateStatus", "type=Activate");
}

function setToTeamOnly(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdateAccessStatus", "type=TeamOnly");
}

function setToAll(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdateAccessStatus", "type=All");
}

function activateReadingRoom(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdateReadingRoomStatus", "type=Activate");
}

function deactivateReadingRoom(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdateReadingRoomStatus", "type=Deactivate");
}

function activateCommentInput(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdatePublicCommentStatus", "type=Activate");
}

function deactivateCommentInput(projectId, phaseId) {
    // send the url request
    window.location.href = setActionUrl(projectId, phaseId, "UpdatePublicCommentStatus", "type=Deactivate");
}

function setActionUrl(projectId, phaseId, action, queryString) {
    return "/Project/" + projectId + "/Phase/" + phaseId + "/" + action + (queryString != "" ? "?" : "") + queryString;
}

function requestAllLetters(projectId, phaseId) {
    $.post("/Project/" + projectId + "/Phase/" + phaseId + "/DownloadAllLetters", {projectId:projectId, phaseId:phaseId},
        function (data)
        {
            if (data.toLowerCase() == "true")
            {
                alertWindow("Your request to download all letters was logged. You will receive an e-mail with a link that will instruct you on how to download the letters.");
            }
            else
            {
                //alertWindow("Your request to download all letters was not successful. Please contact emnepa@fs.fed.us for further assistance.");
                
                if (window.confirm('Your request to download all letters was not successful. Please open a ticket by clicking OK else click Cancel')) 
                    {
                    window.location.href='https://ems-team.usda.gov/sites/fs-emnepa-sd/Lists/eMNEPASupport/Item/newifs.aspx';
                    }
            }
        });
}

function updateObjectionResponseExtensionDate(projectId, phaseId)
{
    var days = parseInt($("#extendResponseBy").val());
    var oldDays = parseInt($('#extendedInDays').text());

    if (isNaN(days))
    {
        $("#ExtensionError").show();
        $("#ExtensionError").text("Extend Response By must be a number");
    }
    else if (days < oldDays)
    {
        $("#ExtensionError").show();
        $("#ExtensionError").text("New Response Due Date must be greater than " + oldDays);
    }
    else
    {
        $("#ExtensionError").text("&nbsp;");
        $("#ExtensionError").hide();
        $.post("/Project/" + projectId + "/Phase/" + phaseId + "/ExtendObjectionResponseDueDate", { days:days },
            function (data) { responseObjectionResponseExtensionDate(data) });
    }
}

function responseObjectionResponseExtensionDate(data)
{
    if (data.Data.success)
    {
        $('#extendedInDays').text($("#extendResponseBy").val());
        var responseDueDate = Date.parse($("#responseDueDate").text());
        var extendedDueDate = new Date(responseDueDate);
        extendedDueDate.setDate(extendedDueDate.getDate() + parseInt($("#extendResponseBy").val()));
        $("#newResponseDueDate").text(extendedDueDate.getMonth() + 1 + "/" + extendedDueDate.getDate() + "/" + extendedDueDate.getFullYear());

        closeWindow("ExtendResponseDueDate");
    }
    else
    {
        $("#ExtensionError").show();
        $("#ExtensionError").text(data.Data.error);
    }
}
//-->