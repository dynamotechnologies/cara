﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Aquilent.Framework.Resource
{
    public abstract class AbstractLookup<TKey, TValue> : ILookup, IEnumerable<TValue>
    {
        #region Private Members
        private Dictionary<TKey, TValue> lookup;
        private List<KeyValuePair<TKey, string>> selection;
        #endregion

        #region Properties
		/// <summary>
		/// References the class that knows how to load the data for a specific lookup
		/// </summary>
		public ILookupDataLoader DataLoader { get; set; }

        /// <summary>
        /// Returns the value associated with the key.
        /// If the key does not exist in the lookup, null is returned
        /// </summary>
        public virtual TValue this[TKey key]
        {
            get
            {
				TValue val = default(TValue);
                lock (this)
                {
                    PopulateIfEmpty();

                    if (lookup.ContainsKey(key))
                    {
                        val = lookup[key];
                    }
                }

                return val;
            }
        }

        /// <summary>
        /// Returns the collection
        /// </summary>
		public IList<KeyValuePair<TKey, string>> SelectionList
        {
            get
            {
                PopulateIfEmpty();

                return selection;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method takes care of loading the data and populating the lookup
        /// </summary>
        protected void Populate()
        {
            lock (this)
            {
                // Get the list of data from its source
				IList<TValue> oList = LoadValues();

                if (oList == null)
                {
                    oList = new List<TValue>();
                }

                // Initialize the lookup and the collection
                lookup = new Dictionary<TKey,TValue>();
				selection = new List<KeyValuePair<TKey, string>>();

                // Load data values into the lookup and the collection
                KeyValuePair<TKey, TValue> lkvp;
				KeyValuePair<TKey, string> skvp;
				foreach (TValue item in oList)
                {
                    // Add key/value pair(s) to the lookup
                    lkvp = PrepareForLookup(item);
					lookup.Add(lkvp.Key, lkvp.Value);

                    // Add item to the collection
					skvp = PrepareForSelection(item);
					selection.Add(skvp);
                }
            }
        }

        /// <summary>
        /// Used to give inherited classes a convenient way to populate
        /// without having access to the collection and the lookup variables
        /// </summary>
        protected void PopulateIfEmpty()
        {
            if (selection == null || lookup == null)
            {
                Populate();
            }
        }

        /// <summary>
        /// Clears the lookup data
        /// </summary>
        public virtual void Clear()
        {
            lock (this)
            {
                lookup = null;
                selection = null;
            }
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// This utility method loads lookup values (e.g., from a database, textfile, etc.)
        /// </summary>
        protected abstract IList<TValue> LoadValues();

        /// <summary>
        /// This utility method generates an item to be stored in the lookup dictionary.  Presumably,
		/// the TKey is derived from a unique quality of TValue.  For instance, for a lookup
		/// of counties, TValue = "County" and TKey = "County.CountyId"
        /// </summary>
        /// <param name="item">object to be converted to a key/value pair</param>
        /// <returns>a key/value pairs to store in the hashtable</returns>
        protected abstract KeyValuePair<TKey, TValue> PrepareForLookup(TValue item);

		/// <summary>
		/// This utility method generates an item to be stored in the selection list. The selection
		/// list is intended to be used as the key value pairs for a drop down control
		/// in the UI.
		/// </summary>
		/// <param name="item">object to be converted to a suitable form to store in the collection</param>
		/// <returns>an object to be stored in the collection</returns>
		protected abstract KeyValuePair<TKey, string> PrepareForSelection(TValue item);
        #endregion

		#region IEnumberable Methods
		public IEnumerator<TValue> GetEnumerator()
		{
			PopulateIfEmpty();
			return lookup.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			PopulateIfEmpty();
			return lookup.Values.GetEnumerator();
		}
		#endregion IEnumberable Methods
	}
}
