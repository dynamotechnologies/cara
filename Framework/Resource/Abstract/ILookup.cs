﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Resource
{
	public interface ILookup
	{
		ILookupDataLoader DataLoader { get; set; }
		void Clear();
	}
}
