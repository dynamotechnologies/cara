﻿using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Aquilent.Cara.Configuration.LookupManager;
using Aquilent.Cara.Configuration;
using log4net;
using System.Linq;

namespace Aquilent.Framework.Resource
{
    /// <summary>
    /// Manages caching of all lookup values within the framework.
    /// </summary>
    public class LookupManager
    {
        #region Member Variables
		private static Dictionary<string, ILookup> _lookups = new Dictionary<string, ILookup>();
		private static ILog _logger = LogManager.GetLogger(typeof(LookupManager));
		private const string _configSectionName = "lookupManagerConfiguration";
        #endregion

        #region Public Methods
		public static void Add(ILookup lookup)
		{
			_lookups.Add(lookup.GetType().FullName, lookup);
		}

		/// <summary>
		/// Clears the lookup cache for each lookup in the manager.
		/// </summary>
        public static void Clear()
        {
            lock (_lookups)
            {
				foreach (ILookup lookup in _lookups.Values)
				{
					lookup.Clear();
				}
            }
        }

		public static T GetLookup<T>() where T : ILookup
		{
			if (_lookups == null || _lookups.Count == 0)
			{
				LoadLookups();
			}

			Type lookupType = typeof(T);
			string lookupTypeName = lookupType.FullName;
			T requestedLookup = default(T);

			try
			{
				if (_lookups.ContainsKey(lookupTypeName))
				{
					requestedLookup = (T) _lookups[lookupTypeName];
				}
				else
				{
					_logger.ErrorFormat("'{0}' is not found in the LookupManager.", lookupTypeName);
				}
			}
			catch (InvalidCastException ice)
			{
				_logger.ErrorFormat("'{0}' does not correspond to a lookup class of the requested type.", lookupTypeName);
				throw ice;
			}

			return requestedLookup;

		}
        #endregion

        #region Utility Methods
		private static void LoadLookups()
		{
			LookupManagerConfiguration lmConfig = ConfigUtilities.GetSection<LookupManagerConfiguration>(_configSectionName);
			if (lmConfig == null)
			{
				string message = String.Format("No configuration section or appSetting exists named {0}", _configSectionName);
				_logger.FatalFormat(message);
				throw new ConfigurationErrorsException(message);
			}

			foreach (LookupClassElement fqce in lmConfig.Lookups)
			{
				try
				{
					var iLookup = (ILookup) fqce.Unwrap();

					if (! string.IsNullOrEmpty(fqce.LookupLoaderType))
					{
						iLookup.DataLoader = (ILookupDataLoader) fqce.UnwrapLookupLoaderType();
					}

					_lookups.Add(fqce.ClassName, iLookup);
				}
				catch (InvalidCastException ice)
				{
					_logger.Error("Error loading Lookup: " + fqce.ToString(), ice);
				}
			}
		}
        #endregion
    }
}
