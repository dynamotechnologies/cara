﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace Aquilent.Framework.Resource
{
    public class LookupTimeZone : AbstractLookup<string, TimeZoneInfo>
    {
		public LookupTimeZone()
			: base()
		{
			DataLoader = new TimeZoneDataLoader();
		}

        #region Overidden Abstract Methods
		protected override IList<TimeZoneInfo> LoadValues()
        {
			return DataLoader.Load().OfType<TimeZoneInfo>().ToList();
        }

		protected override KeyValuePair<string, TimeZoneInfo> PrepareForLookup(TimeZoneInfo item)
        {
			return new KeyValuePair<string, TimeZoneInfo>(item.Id, item);
        }

		protected override KeyValuePair<string, string> PrepareForSelection(TimeZoneInfo item)
		{
			return new KeyValuePair<string, string>(item.Id, item.DisplayName);
		}
        #endregion

		public TimeZoneInfo GetDefaultTimeZone()
		{
			string tzid;
			string defaultTimeZoneId = "Eastern Standard Time";

			tzid = ConfigurationManager.AppSettings["userDefaultTimeZone"];

			if (string.IsNullOrEmpty(tzid))
			{
				tzid = defaultTimeZoneId;
			}

			return this[tzid];
		}

		private class TimeZoneDataLoader : ILookupDataLoader
		{
			public IQueryable Load()
			{
				IList<TimeZoneInfo> list = TimeZoneInfo.GetSystemTimeZones();

				return list.AsQueryable();
			}
		}
	}
}
