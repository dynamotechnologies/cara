﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Aquilent.Framework.Resource
{
	public class CannedTextManager : AbstractEntityManager<CannedText>
	{
		private ResourceSettingManager ResourceSettingManager { get; set; }

		public CannedTextManager(IRepository<CannedText> repository, ResourceSettingManager resourceSettingManager)
			: base(repository)
		{
			ResourceSettingManager = resourceSettingManager;
            ResourceSettingManager.UnitOfWork = UnitOfWork;
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a user with the given user ID
		/// </summary>
		/// <param name="name">The ID of the user</param>
		/// <returns>User or null</returns>
		public override CannedText Get(int uniqueId)
		{
			CannedText ct = All.Where(x => x.CannedTextId == uniqueId).FirstOrDefault();
			ct.CannedTextManager = this;
			ct.ResourceSettingManager = ResourceSettingManager;

			return ct;
		}

		/// <summary>
		/// Returns a user with the given username
		/// </summary>
		/// <param name="uniqueName">The username of the user</param>
		/// <returns>User or null</returns>
		public override CannedText Get(string uniqueName)
		{
			CannedText ct = All.Where(x => x.Name == uniqueName).FirstOrDefault();
			ct.CannedTextManager = this;
			ct.ResourceSettingManager = ResourceSettingManager;

			return ct;
		}
		#endregion Override AbstractEntityService members

		#region Custom Methods
		/// <summary>
		/// This method recursively resolves the tokens in a canned text
		/// with the defined final value.  The CannedText object defines the characters
		/// it uses to mark the beginning and end of a token.  For instance, here is an
		/// example of a canned text whosse start token = "{{" and end token = "}}":
		///	
		///		This is an example of a canned text template.  Here is the definition
		///		of a token: {{replace_me}}.  And here is an example of how to embed
		///		another canned text within a canned text: {{include:canned.text.name}}.
		/// 
		/// </summary>
		/// <param name="cannedTextName">The name of the canned text to resolve all tokens.</param>
		/// <returns>The text with all tokens replaced.</returns>
		public string Resolve(string cannedTextName, IDictionary<string, string> supportingInfo = null)
		{
			string resolvedText = string.Empty;

			// Retrieve the canned text from the database
			var ct = Get(cannedTextName);
			if (ct != null)
			{
				if (supportingInfo != null)
				{
					ct.SupportingInfo = supportingInfo;
				}

				resolvedText = ct.Resolve();
			}

			return resolvedText;
		}
		#endregion
	}
}
