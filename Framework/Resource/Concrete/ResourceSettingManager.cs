﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Linq.Expressions;

namespace Aquilent.Framework.Resource
{
	public class ResourceSettingManager : AbstractEntityManager<ResourceSetting>
	{
		public ResourceSettingManager(IRepository<ResourceSetting> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a user with the given user ID
		/// </summary>
		/// <param name="name">The ID of the user</param>
		/// <returns>User or null</returns>
		public override ResourceSetting Get(int uniqueId)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Returns a setting with the given name.  Note the setting name must start with "security."
		/// </summary>
		/// <param name="uniqueName">The setting</param>
		/// <returns>SecuritySetting or null</returns>
		public override ResourceSetting Get(string uniqueName)
		{
			return All.Where(x => x.Name.ToLower().StartsWith("resource.") && x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members
	}
}
