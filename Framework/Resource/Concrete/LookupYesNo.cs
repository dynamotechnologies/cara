﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Aquilent.Framework.Resource
{
    public class LookupYesNo : AbstractLookup<string, string>
    {
		public LookupYesNo()
			: base()
		{
			DataLoader = new YesNoDataLoader();
		}

        #region Overidden Abstract Methods
        protected override IList<string> LoadValues()
        {
			return DataLoader.Load().OfType<string>().ToList();
        }

        protected override KeyValuePair<string, string> PrepareForLookup(string item)
        {
            return new KeyValuePair<string,string>(item, item);
        }

		protected override KeyValuePair<string, string> PrepareForSelection(string item)
		{
			return new KeyValuePair<string, string>(item, item);
		}
        #endregion

		private class YesNoDataLoader : ILookupDataLoader
		{
			public IQueryable Load()
			{
				var list = new List<string>();
				list.Add("Yes");
				list.Add("No");

				return list.AsQueryable();
			}
		}
    }
}
