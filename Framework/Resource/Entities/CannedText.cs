//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Framework.Resource
{
    public partial class CannedText
    {
        #region Primitive Properties
    
        public virtual int CannedTextId
        {
            get;
            set;
        }
    
        public virtual string Name
        {
            get;
            set;
        }
    
        public virtual string Value
        {
            get;
            set;
        }
    
        public virtual string TokenEnd
        {
            get;
            set;
        }
    
        public virtual string TokenStart
        {
            get;
            set;
        }
    
        public virtual string Substituter
        {
            get;
            set;
        }

        #endregion
    }
}
