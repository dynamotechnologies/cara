﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Aquilent.Framework.Resource
{
	public partial class CannedText
	{
		#region Member Variables
		private IDictionary<string, string> _supportingInfo;
		private const string _pattern = @"((include:(?<templateName>[.\w]*))|(setting:(?<resourceName>[.\w]*))|(?<tokenName>[.\w]*))";
		#endregion Member Variables

		#region Properties
		public CannedTextManager CannedTextManager { get; set; }
		public ResourceSettingManager ResourceSettingManager { get; set; }

		/// <summary>
		/// This property is used to store objects that the ITokenReplacer can have
		/// access to in the ITokenReplacer.ReplaceTokens() method.  When using the
		/// CannedTextManager.Resolve method, the SupportingInfo will be passed down
		/// to embedded templates, so those canned texts will also be able to provide
		/// SupportingInfo to the TokenReplacer.
		/// </summary>
		public IDictionary<string, string> SupportingInfo
		{
			get
			{
				if (_supportingInfo == null)
				{
					_supportingInfo = new Dictionary<string, string>();
				}

				return _supportingInfo;
			}

			set
			{
				_supportingInfo = value;
			}
		}

		/// <summary>
		/// Returns the token start delimiter escaped to be used in a regular expression
		/// </summary>
		public string TokenStartRegexEscape
		{
			get
			{
				return EscapeRegexSpecialCharacters(TokenStart);
			}
		}

		/// <summary>
		/// Returns the token end delimiter escaped to be used in a regular expression
		/// </summary>
		public string TokenEndRegexEscape
		{
			get
			{
				return EscapeRegexSpecialCharacters(TokenEnd);
			}
		}
		#endregion Properties

		#region Methods
		/// <summary>
		/// Escape Regex special characters in start and end tokens
		/// [, \, ^, $, ., |, ?, *, +, (, )
		/// </summary>
		private string EscapeRegexSpecialCharacters(string tokenDelimiter)
		{
			tokenDelimiter = tokenDelimiter.Replace(@"\", @"\\");
			tokenDelimiter = tokenDelimiter.Replace("[", @"\[");
			tokenDelimiter = tokenDelimiter.Replace("^", @"\^");
			tokenDelimiter = tokenDelimiter.Replace("$", @"\$");
			tokenDelimiter = tokenDelimiter.Replace(".", @"\.");
			tokenDelimiter = tokenDelimiter.Replace("|", @"\|");
			tokenDelimiter = tokenDelimiter.Replace("?", @"\?");
			tokenDelimiter = tokenDelimiter.Replace("*", @"\*");
			tokenDelimiter = tokenDelimiter.Replace("+", @"\+");
			tokenDelimiter = tokenDelimiter.Replace("(", @"\(");
			tokenDelimiter = tokenDelimiter.Replace(")", @"\)");

			return tokenDelimiter;
		}

		/// <summary>
		/// Appends and prepends the token start and end delimeters to the value passed in.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public string GetTokenRegexEscape(string value)
		{
			return TokenStartRegexEscape + value + TokenEndRegexEscape;
		}

		/// <summary>
		/// This method recursively resolves the tokens in a canned text
		/// with the defined final value.  The CannedText object defines the characters
		/// it uses to mark the beginning and end of a token.  For instance, here is an
		/// example of a canned text whose start token = "{{" and end token = "}}":
		///
		/// Examples:
		///		Definition of a token: {{replace_me}}.
		///		Canned text embedded within a another canned text: {{include:canned.text.name}}
		///		Resource setting embeded in a canned text within a canned text: {{setting:resource.setting.name}}
		/// </summary>
		/// <param name="ct">The canned text to resolve all tokens.</param>
		/// <returns>The text with all tokens replaced.</returns>
		public string Resolve()
		{
			// Recursively replace embedded canned texts templates
			// If TokenStart == {{ and TokenEnd == }}, then the 
			// regex pattern = {{((include:(?<templateName>[.\w]*))|(setting:(?<resourceName>[.\w]*))|(?<tokenName>[.\w]*))}}
			var pattern = GetTokenPattern();
			Regex regex = new Regex(pattern);
			string resolvedText = Value;

			foreach (Match match in regex.Matches(resolvedText))
			{
				if (match.Groups["templateName"].Success && match.Groups["templateName"].Value != Name)
				{
					// Retrieve the embedded canned text
					var embeddedCannedText = CannedTextManager.Get(match.Groups["templateName"].Value);

					if (embeddedCannedText != null)
					{
						// Pass the supporting info into the embedded canned text
						embeddedCannedText.SupportingInfo = SupportingInfo;

						var resolvedEmbeddedText = embeddedCannedText.Resolve();
						resolvedText = resolvedText.Replace(match.Value, resolvedEmbeddedText);
					}
				}

				else if (match.Groups["resourceName"].Success)
				{
					var setting = ResourceSettingManager.Get(match.Groups["resourceName"].Value);
					if (setting != null)
					{
						var resolvedSetting = ResolveSetting(setting, SupportingInfo, regex);
						resolvedText = resolvedText.Replace(match.Value, resolvedSetting);
					}
				}

				else if (match.Groups["tokenName"].Success && SupportingInfo.ContainsKey(match.Groups["tokenName"].Value))
				{
					resolvedText = resolvedText.Replace(match.Value, SupportingInfo[match.Groups["tokenName"].Value].ToString());
				}
			}

			return resolvedText;
		}

		/// <summary>
		/// Resolves any canned text templates or other resource settings embedded in a resource setting.
		/// </summary>
		/// <param name="setting"></param>
		/// <param name="supportingInfo"></param>
		/// <param name="regex"></param>
		/// <returns></returns>
		private string ResolveSetting(ResourceSetting setting, IDictionary<string, string> supportingInfo, Regex regex)
		{
			string settingText = setting.Value;
			foreach (Match match in regex.Matches(settingText))
			{
				if (match.Groups["templateName"].Success)
				{
					// Retrieve the embedded canned text
					var embeddedCannedText = CannedTextManager.Get(match.Groups["templateName"].Value);

					if (embeddedCannedText != null)
					{
						// Pass the supporting into the embedded canned text
						embeddedCannedText.SupportingInfo = supportingInfo;

						var resolvedCannedText = embeddedCannedText.Resolve();
						settingText = settingText.Replace(match.Value, resolvedCannedText);
					}
				}

				else if (match.Groups["resourceName"].Success)
				{
					var embeddedSetting = ResourceSettingManager.Get(match.Groups["resourceName"].Value);
					if (embeddedSetting != null)
					{
						var resolvedSetting = ResolveSetting(embeddedSetting, supportingInfo, regex);
						settingText = settingText.Replace(match.Value, resolvedSetting);
					}
				}

				else if (match.Groups["tokenName"].Success && SupportingInfo.ContainsKey(match.Groups["tokenName"].Value))
				{
					settingText = settingText.Replace(match.Value, SupportingInfo[match.Groups["tokenName"].Value].ToString());
				}
			}

			return settingText;
		}

		/// <summary>
		/// Generates the proper pattern based on the start and end token characters
		/// </summary>
		/// <param name="ct"></param>
		/// <returns></returns>
		private string GetTokenPattern()
		{
			var startToken = TokenStartRegexEscape;
			var endToken = TokenEndRegexEscape;

			return startToken + _pattern + endToken;
		}
		#endregion Methods
	}
}
