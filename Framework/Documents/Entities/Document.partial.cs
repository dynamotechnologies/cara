﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Documents
{
	public partial class Document
	{
		public bool HasFile
		{
			get
			{
				return File != null;
			}
		}
	}
}
