//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Framework.Documents
{
    public partial class File
    {
        #region Primitive Properties
    
        public virtual int FileId
        {
            get;
            set;
        }
    
        public virtual string StoragePath
        {
            get;
            set;
        }
    
        public virtual System.DateTime Uploaded
        {
            get;
            set;
        }
    
        public virtual string UploadedBy
        {
            get;
            set;
        }
    
        public virtual string OriginalName
        {
            get;
            set;
        }
    
        public virtual string ContentType
        {
            get;
            set;
        }
    
        public virtual int Size
        {
            get;
            set;
        }
    
        public virtual string ContentHash
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<Document> Documents
        {
            get
            {
                if (_documents == null)
                {
                    var newCollection = new FixupCollection<Document>();
                    newCollection.CollectionChanged += FixupDocuments;
                    _documents = newCollection;
                }
                return _documents;
            }
            set
            {
                if (!ReferenceEquals(_documents, value))
                {
                    var previousValue = _documents as FixupCollection<Document>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupDocuments;
                    }
                    _documents = value;
                    var newValue = value as FixupCollection<Document>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupDocuments;
                    }
                }
            }
        }
        private ICollection<Document> _documents;

        #endregion
        #region Association Fixup
    
        private void FixupDocuments(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Document item in e.NewItems)
                {
                    item.File = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Document item in e.OldItems)
                {
                    if (ReferenceEquals(item.File, this))
                    {
                        item.File = null;
                    }
                }
            }
        }

        #endregion
    }
}
