﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using Aquilent.Cara.Configuration.FileStore;
using Aquilent.Cara.Configuration;

namespace Aquilent.Framework.Documents
{
	public partial class File
	{
		#region Member Variables
        protected Stream inputStream;
        private FileInfo fileInfo;
		#endregion Member Variables

		#region Properties
        public virtual Stream InputStream
        {
            get
            {
                // Deferring opening of the stream until its needed
                // if the FileTdc represents a physical file
                if (fileInfo != null)
                {
                    // ensures stream is only open once
                    if (inputStream == null)
                    {
                        inputStream = fileInfo.OpenRead();
                    }
                }

                return inputStream;
            }
        }

        /// <summary>
        /// Returns true if the File contains a reference
        /// to the file contents that need to be saved to the
        /// file store
        /// </summary>
        public bool HasContents
        {
            get
            {
                if (fileInfo != null || InputStream != null)
                {
                    return true;
                }

                return false;
            }
        }

		public string Extension
		{
			get
			{
				int index = OriginalName.LastIndexOf(".");

				string extension = string.Empty;
				if (index != -1 && index < OriginalName.Length - 1)
				{
					extension = OriginalName.Substring(index + 1).ToLower();
				}

				return extension;
			}
		}

        /// <summary>
        /// Returns the display name of the file content type
        /// </summary>
        public string ContentTypeName
        {
            get
            {
                string name = string.Empty;

                if (!string.IsNullOrEmpty(ContentType))
                {
                    // Load the config file
                    FileStoreConfiguration fsc = ConfigUtilities.GetSection<FileStoreConfiguration>("fileStoreConfiguration");

                    if (fsc != null)
                    {
                        // get the name by content type
                        name = fsc.FileExtensions.Cast<FileExtensionConfigElement>().Where(x => x.Extension == ContentType.ToLower()).FirstOrDefault().Name;
                    }
                }

                return name;
            }
        }
		#endregion Properties

        #region Constructors
        public File()
        {
        }

        public File(HttpPostedFileBase file)
        {
            if (file.ContentLength == 0)
            {
                throw new DocumentException("You cannot upload an empty file");
            }


            inputStream = file.InputStream;
            ContentType = file.ContentType;
            Size = file.ContentLength;
            OriginalName = file.FileName.Substring(
                    file.FileName.LastIndexOf(@"\") + 1);
        }

		/// <summary>
        /// Create a File from a stream.  This contructor is primarily intended
        /// for developers and all other FileTdc parameters (contentType, size, originalName)
        /// must be specified manually.
        ///
        /// Example Code for Developers:
        ///
        /// // Create FileInfo object and open the stream
        /// FileInfo fileInfo = new FileInfo(filepath);
        /// Stream inputStream = fileInfo.OpenRead();
        ///
        /// // Create FileTdc and use FileInfo object to populate FileTdc properties
        /// File file = new File(inputStream);
        /// file.Size = Convert.ToInt32(fileInfo.Length);
        /// file.OriginalName = fileInfo.Name;
        /// </summary>
        /// <param name="inputStream"></param>
        public File(Stream inputStream)
        {
            this.inputStream = inputStream;
        }

        /// <summary>
        /// Creates a FileTdc from an absolute file path and
        /// a content type.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="contentType"></param>
        public File(string filepath, string contentType)
        {
            InitConstructor(filepath, contentType, filepath);
        }

        public File(string filepath, string contentType, string newName)
        {
            InitConstructor(filepath, contentType, newName);
        }

        public void InitConstructor(string filepath, string contentType, string newName)
        {
            this.fileInfo = new FileInfo(filepath);

            ContentType = contentType;
            Size = Convert.ToInt32(fileInfo.Length);

            // Setting OriginalName before checking newName
            // because OriginalName detects invalid file types
            this.OriginalName = this.fileInfo.Name;
            if (! filepath.Equals(newName))
            {
                this.OriginalName = newName;
            }

            // Deferring opening of the file stream until its needed.
            // The file shouldn't be open longer than it needs to be
        }
        #endregion

        #region Static Utility Methods
        /// <summary>
        /// This utility method formats the specified input size (in bytes)
        /// to convert it to KB, MB, GB, etc. as appropriate
        /// </summary>
        /// <param name="isize">a valid number of bytes</param>
        /// <returns>formatted size with appropriate units</returns>
        public static string FormatSize(int isize)
        {
            int factor = 1;
            double size = (double) isize;

            while (size > 1000)
            {
                size /= 1024.0;
                factor++;
            }

            size += 0.005;
            size = ((int) (size * 100)) / 100.0;

            string unit = String.Empty;
            switch (factor)
            {
                case 1: unit = "Bytes"; break;
                case 2: unit = "KB"; break;
                case 3: unit = "MB"; break;
                case 4: unit = "GB"; break;
                case 5: unit = "TB"; break;
            }

            return size + " " + unit;
        }
        #endregion
    }
}
