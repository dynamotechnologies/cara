﻿using System.IO;

namespace Aquilent.Framework.Documents
{
	public interface ITextExtractor
	{
		string ExtractText(Stream stream);
	}
}
