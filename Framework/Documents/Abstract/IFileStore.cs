﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Aquilent.Cara.Configuration.FileStore;

namespace Aquilent.Framework.Documents
{
	/// <summary>
	/// This interface represents the operations needed to
	/// store files.
	/// </summary>
	public interface IFileStore
	{
		/// <summary>
		/// Returns the configuration for this filestore.
		/// </summary>
		FileStoreConfiguration Configuration { get; }

		/// <summary>
		/// Initializes the IFileStore with Parameters
		/// </summary>
		/// <param name="parameters"></param>
		void Init();

		/// <summary>
		/// Saves a file to the file store
		/// </summary>
		/// <param name="file"></param>
		/// <returns>The file storage path</returns>
		string SaveFile(File file);

		/// <summary>
		/// Deletes a file from the file store
		/// </summary>
		/// <param name="file"></param>
		void DeleteFile(File file);

		/// <summary>
		/// Opens a stream to a file in the file store for reading
		/// </summary>
		/// <param name="file"></param>
		/// <returns></returns>
		Stream OpenRead(File file);

		/// <summary>
		/// Opens a stream to a file in the file store for reading
		/// </summary>
		/// <param name="file"></param>
		/// <returns></returns>
		Stream OpenRead(string storagePath);

		/// <summary>
		/// Dynamically creates a zip file containing the requested documents
		/// and streams the zip file to the output stream
		/// </summary>
		/// <param name="documentIds"></param>
		/// <param name="outputStream"></param>
		void StreamZippedDocuments(IList<Document> documents, Stream outputStream);

		/// <summary>
		/// Compares the extension of the file to the list of valid extensions
		/// </summary>
		/// <param name="file"></param>
		/// <returns></returns>
		bool IsValidFileType(File file);

		/// <summary>
		/// Strips characters from a string that would otherwise be invalid in a Windows
		/// file name.  These characters are defined in the filestore configuration file.
		/// </summary>
		/// <param name="fileName">the string from which to strip</param>
		/// <returns>the stripped string</returns>
		string RemoveInvalidFileNameCharacters(string fileName);

	}
}
