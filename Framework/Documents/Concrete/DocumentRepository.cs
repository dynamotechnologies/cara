﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Data.Objects;
using log4net;

namespace Aquilent.Framework.Documents
{
	public class DocumentRepository : EFRepository<Document>
	{
		private ILog _logger = LogManager.GetLogger(typeof(DocumentRepository));

		#region Constructor
		public DocumentRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public override void Delete(Document entity)
		{
			if (entity.HasFile)
			{
				var fileRepository = new EFRepository<File>(UnitOfWork);
				var documents = entity.File.Documents;
				var file = fileRepository.All.First(x => x.FileId == entity.FileId);

				if (documents.Count == 1)
				{
					// NOTE: If UnitOfWork.Save never executes, the physical file will no longer exist after DeleteFile is called.
					// Not sure how to handle if there's a problem.  This is an acceptable risk.
					DocumentManager dm = ((DocumentManager) Manager);
					fileRepository.Delete(file);
					dm.DeleteFile(file);
					_logger.InfoFormat("The physical file, {0}, associated with FileID={1} was deleted in anticipation of the File entity being deleted", file.StoragePath, file.FileId);
				}
			}

			base.Delete(entity);
		}

		#endregion
	}
}
