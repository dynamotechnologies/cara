﻿using System.IO;

namespace Aquilent.Framework.Documents
{
	public class NoTextExtractor : ITextExtractor
	{
		public string ExtractText(Stream stream)
		{
			return string.Empty;
		}
	}
}
