﻿using System.IO;

namespace Aquilent.Framework.Documents
{
	public class TextExtractor : ITextExtractor
	{
		public string ExtractText(Stream stream)
		{
			var s = new StreamReader(stream);

			return s.ReadToEnd();
		}
	}
}
