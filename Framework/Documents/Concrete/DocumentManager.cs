﻿using Aquilent.Cara.Configuration.FileStore;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using ICSharpCode.SharpZipLib.Zip;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Aquilent.Framework.Documents
{
	public class DocumentManager : AbstractEntityManager<Document>
	{
		#region Member Varibles
		internal IFileStore FileStore { get; set; }
		private ILog _logger = LogManager.GetLogger(typeof(DocumentManager));
		#endregion Member Variables

		#region Constructor
		public DocumentManager(IRepository<Document> repository, IFileStore fileStore)
			: base(repository)
		{
			FileStore = fileStore;
			repository.Manager = this;
		}
		#endregion

		#region AbstractEntityManager overrides
		public override Document Get(int uniqueId)
		{
            return Repository.ObjectQuery.Include("File").FirstOrDefault(x => x.DocumentId == uniqueId);
		}

		public override Document Get(string uniqueName)
		{
			throw new NotImplementedException();
		}

        /// <summary>
        /// Retreives a list of documents with the IDs
        /// </summary>
        /// <param name="documentIds">a list of integers</param>
        /// <param name="include">which other relationship(s) to eager load (defaults to null)</param>
        /// <returns>List of users</returns>
        public IList<Document> Get(IList<int> documentIds, string include = null)
        {
            var documents = All;
            if (!string.IsNullOrEmpty(include))
            {
                documents = Repository.ObjectQuery.Include(include);
            }

            var query = from x in documents
                        where documentIds.Contains(x.DocumentId)
                        select x;

            return query.ToList();
        }

		public override void Add(Document entity)
		{
			try
			{
				if (entity.HasFile && entity.File.HasContents)
				{
					SaveFile(entity);
				}
				else
				{
					throw new ApplicationException("The Document doesn't have any file contents");
				}

				base.Add(entity);
			}
            catch (InvalidFileTypeException ex)
            {
                _logger.Error("Invalid file type error occurred adding the document", ex);
                throw new InvalidFileTypeException("Invalid file type error occurred adding the document", ex);
            }
			catch (Exception e)
			{
				_logger.Error("An error occurred adding the document", e);
				throw new DocumentException("An error occurred adding the document", e);
			}
		}

		public override void Delete(Document entity)
		{
			try
			{
				if (! entity.HasFile)
				{
					throw new ApplicationException("The Document doesn't have any file contents");
				}

				base.Delete(entity);
			}
			catch (Exception e)
			{
				_logger.Error("An error occurred deleting the document", e);
				throw new DocumentException("An error occurred deleting the document", e);
			}
		}
		#endregion AbstractEntityManager overrides

		#region Methods
		public void Delete(int documentId)
		{
			Document document = Get(documentId);
			Delete(document);
		}

		public void DownloadFile(Document document, Stream outputStream)
		{
			Stream stream = null;

			try
			{
				if (document == null)
				{
					throw new DocumentException("Requested document could not be found");
				}

				if (document.HasFile)
				{
					File file = document.File;

					stream = FileStore.OpenRead(file);
					WriteStreamToStream(stream, outputStream);
				}
			}
			catch (Exception ex)
			{
				_logger.Error("An error occurred while downloading the document", ex);
				throw new DocumentException("An error occurred while downloading the document", ex);
			}
			finally
			{
				if (stream != null)
				{
					stream.Close();
				}
			}
		}

		/// <summary>
		/// Writes a document to the stream.
		/// </summary>
		/// <param name="documentId">Unique document identifier</param>
		/// <param name="outputStream">An output stream</param>
		public void DownloadFile(
			int documentId,
			Stream outputStream)
		{
			Document document = Get(documentId);
			DownloadFile(document, outputStream);
		}

		/// <summary>
		/// Writes a document to the HTTP Response.  Automatically sets the
		/// content type of the Response to the file being streamed.
		/// </summary>
		/// <param name="documentId">Unique document identifier</param>
		/// <param name="response">The HTTP response</param>
		public void DownloadFile(
			int documentId,
			HttpResponse response)
		{
			Stream stream = null;
			try
			{
				Document document = Get(documentId);

				if (document == null)
				{
					throw new DocumentException("Requested document could not be found");
				}

				if (document.HasFile)
				{
					File file = document.File;

					response.ContentType = file.ContentType;
					response.AddHeader("content-disposition",
						"attachment;filename=" + file.OriginalName);

					stream = FileStore.OpenRead(file);
					WriteStreamToStream(stream, response.OutputStream);
				}
				else
				{
					throw new ApplicationException("The Document doesn't have any file contents");
				}
			}
			catch (Exception ex)
			{
				throw new DocumentException("An error occurred while downloading the document", ex);
			}
			finally
			{
				if (stream != null)
				{
					stream.Close();
				}
			}
		}

		/// <summary>
		/// Writes a collection of documents to the outputStream
		/// in a compressed, zip format.
		/// </summary>
		/// <param name="documents">Collection of documents to package</param>
		/// <param name="outputStream">The stream to which the zip file are written</param>
		public void ZipDocuments(int[] documentIds, Stream outputStream)
		{
			try
			{
				if (outputStream == null || !outputStream.CanWrite)
				{
					throw new ArgumentException("The stream cannot be null and must be writable", "outputStream");
				}

				// Filter out links
                IList<Document> documents = Get(documentIds).Where(x => x.FileId != null).ToList();

				FileStore.StreamZippedDocuments(documents, outputStream);
			}
			catch (Exception ex)
			{
				throw new DocumentException("An error occurred while creating the ZIP file", ex);
			}
		}

		/// <summary>
		/// Shortcut method for calling ZipDocuments in a web environment.
		/// Automatically sets the content type of the Response to
		/// a zip file before streaming the Zip file.
		/// </summary>
		/// <param name="documents">Collection of documents to package</param>
		/// <param name="outputStream">The output stream</param>
		/// <param name="zipFilename">The name of the zip file to
		///     stream to the client.  If the zipFilename does not
		///     end in ".zip", it will be automatically added</param>
		public void DownloadZippedDocuments(int[] documentIds, HttpResponse response, string zipFilename)
		{
			// Add .zip extenstion, if necessary
			if (zipFilename.ToLower().LastIndexOf(".zip") == -1)
			{
				zipFilename = zipFilename + ".zip";
			}

			response.ContentType = "application/x-zip-compressed";
			response.AddHeader("content-disposition",
				"attachment;filename=" + zipFilename);
			ZipDocuments(documentIds, response.OutputStream);
		}

		internal void DeleteFile(File file)
		{
			try
			{
				FileStore.DeleteFile(file);
			}
			catch (Exception e)
			{
				_logger.ErrorFormat("An error occurred deleting " +
						"file (ID: {0}, Path: {1}): {2} ",
						file.FileId,
						file.StoragePath,
						e.Message);
			}
		}
		#endregion

		#region File-specific Operations
		private void SaveFile(Document document)
		{
			// Only replace the file if a new file was uploaded
			if (document.HasFile && document.File.HasContents)
			{
				var file = document.File;

				// Check to see if the file type is valid
				if (!FileStore.IsValidFileType(file))
				{
					string ext = string.IsNullOrEmpty(file.Extension) ? "{{No Extension}}" : file.Extension;
					_logger.Error("Error saving file: Invalid file type for '" + file.OriginalName + "'");
                    throw new InvalidFileTypeException("File types with the extension '" + ext + "' are not permitted");
				}

				// Remove the invalid characters from the file original and display names
				file.OriginalName = FileStore.RemoveInvalidFileNameCharacters(file.OriginalName);
				document.Name = FileStore.RemoveInvalidFileNameCharacters(document.Name);

				// Compute the hash code of the file's contents
                file.ContentHash = file.InputStream.ComputeHash(new SHA512CryptoServiceProvider());

				var query =
						from d in Repository.ObjectQuery.Include("File")
						where d.File.ContentHash == file.ContentHash
							&& d.File.ContentType == file.ContentType
							&& d.File.OriginalName == file.OriginalName
							&& d.File.Size == file.Size
							&& d.FileId != null
						select d;
				var duplicates = query.ToList();

				if (duplicates != null && duplicates.Count == 0) // not a duplicate
				{
					file.InputStream.Position = 0; // resets the stream to the beginning
					file.StoragePath = FileStore.SaveFile(file); // NOTE: If UnitOfWork.Save never executes, the physical file will not be associated with a File entity.
				}
				else
				{
					// Logically, there should only be 1 duplicate, so we just use the first one
					document.File = duplicates[0].File;
				}

				//file.InputStream.Close();
				//file.OnSaved(); -- leftover from APS.  not sure if needed...leaving here for now.
			}
		}
		#endregion

		#region Utility Methods
		/// <summary>
		/// Returns the mime type of the given extension
		/// </summary>
		/// <param name="extension"></param>
		/// <returns></returns>
		public string ContentType(string extension)
		{
			string contentType = string.Empty;
			if (!string.IsNullOrEmpty(extension))
			{
				var extensionElement = FileStore.Configuration.FileExtensions.Cast<FileExtensionConfigElement>().Where(x => x.Extension == extension.ToLower()).FirstOrDefault();

				if (extensionElement != null)
				{
					contentType = extensionElement.MimeType;
				}
			}
			
			return contentType;
		}

		public static void WriteStreamToStream(Stream input, Stream output)
		{
			// Open the stream and store it in the buffer
			int bufferSize = 1048576;
			int numBytesRead;
			byte[] buffer = new byte[bufferSize];

			while (true)
			{
				numBytesRead = input.Read(buffer, 0, bufferSize);

				if (numBytesRead == 0)
				{
					break;
				}

				output.Write(buffer, 0, numBytesRead);
			}
		}

		public static void WriteZipFile(
				ZipOutputStream zipStream, IList<Document> documents, IFileStore filestore)
		{
			File file;
			Stream stream = null;
			ZipEntry zipEntry;
			try
			{
				foreach (Document document in documents)
				{
					if (document.HasFile)
					{
						file = document.File;
						stream = filestore.OpenRead(document.File);

						// Add the file to the zip file
						zipEntry = new ZipEntry(file.OriginalName);
						zipStream.PutNextEntry(zipEntry);

						DocumentManager.WriteStreamToStream(stream, zipStream);
						zipStream.CloseEntry();
						stream.Close();
					}
				}
			}
			catch
			{
				if (stream != null)
				{
					stream.Close();
				}

				throw;
			}
		}
		#endregion

	}
}
