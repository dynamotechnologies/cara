﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.IO;

namespace Aquilent.Framework.Documents
{
	public static class TextExtractorFactory
	{
        public static bool IsExtractable(string contentType)
        {
            return !(GetExtractor(contentType) is NoTextExtractor);
        }
        
		public static ITextExtractor GetExtractor(string contentType)
		{
			ITextExtractor extractor = null;

			// hard code this for now...ultimately, this should be determined via custom config section
			switch (contentType)
			{
				case "application/pdf":
					extractor = new PdfTextExtractor();
					break;
				case "text/plain":
					extractor = new TextExtractor();
					break;

				default:
					extractor = new NoTextExtractor();
					break;
			}

			return extractor;
		}

		#region Extension methods
		public static string ExtractText(this Document document, ITextExtractor extractor = null)
		{
            string text = string.Empty;

            // get the extractor if not provided in the parameter
            if (extractor == null)
            {
                extractor = TextExtractorFactory.GetExtractor(document.File.ContentType);
            }

            using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
            {
                using (MemoryStream stream = new MemoryStream(new byte[document.File.Size], true))
                {
                    documentManager.DownloadFile(document, stream);
                    stream.Position = 0;
                    text = extractor.ExtractText(stream);
                }
            }

			return text;
		}
		#endregion Extension methods
	}
}
