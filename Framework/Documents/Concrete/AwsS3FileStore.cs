﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Configuration.FileStore;
using Amazon.S3;
using Amazon.S3.IO;
using Aquilent.Cara.Configuration;
using System.IO;
using System.Configuration;
using ICSharpCode.SharpZipLib.Zip;

namespace Aquilent.Framework.Documents
{
    public class AwsS3FileStore : IFileStore
    {
		#region Members
		public FileStoreConfiguration Configuration { get; private set; }
		#endregion Members

		#region Properties
        /// <summary>
        /// Read-only.  Returns the name of the AWS S3 Bucket
        /// </summary>
        public string Bucket { get { return UploadFolder; } }

        /// <summary>
		/// Read-only.  Returns the path to the upload folder
		/// </summary>
		public string UploadFolder { get; private set; }

		/// <summary>
		/// Returns the subfolder per month and year.  Format:  YYYYMM
		/// </summary>
		private string UploadSubFolder
		{
			get
			{
				DateTime now = DateTime.Now;
				string uploadSubFolder = now.Year +	now.Month.ToString().PadLeft(2, '0');

                using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, Bucket);
				    
                    var subfolder = rootDirectory.GetDirectory(uploadSubFolder);

                    // Create the subfolder if necessary
				    if (!subfolder.Exists)
				    {
					    rootDirectory.CreateSubdirectory(uploadSubFolder);
				    }
                }

				return uploadSubFolder;
			}
		}

		/// <summary>
		/// Read-only.  Timestamp in the format ddhhmmss
		///   dd = date of month
		///   hh = hour
		///   mm = minutes
		///   ss = seconds
		/// </summary>
		private string Timestamp
		{
			get
			{
				DateTime now = DateTime.Now;
				return now.Day.ToString().PadLeft(2, '0') +
					now.Hour.ToString().PadLeft(2, '0') +
					now.Minute.ToString().PadLeft(2, '0') +
					now.Second.ToString().PadLeft(2, '0');
			}
		}
		#endregion

		#region Constructor
		public AwsS3FileStore(string configSectionName)
		{
			Configuration = ConfigUtilities.GetSection<FileStoreConfiguration>(configSectionName);
			Init();
		}
		#endregion Constructor

		#region IFileStore methods
		/// <summary>
		/// Parameters required:
		///   uploadFolder - The root directory where uploaded files are stored
		/// </summary>
		/// <param name="parameters"></param>
		public void Init()
		{
			UploadFolder = Configuration.UploadFolder.DirectoryPath;
		}

		public string SaveFile(File file)
		{
			string storagePath = GetStoredFilePath(file, new Random().Next(1000));

            using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                // ---------------------------------------
                // Save the input file to the storage path
                //
                //		NOTE:  The calling method is responsible 
                //			   for managing file.InputStream
                // ---------------------------------------
                S3FileInfo fileInfo = new S3FileInfo(client, Bucket, storagePath);
                using (Stream fileStream = fileInfo.OpenWrite())
                {
                    // Read from the input file and write to the output file
                    DocumentManager.WriteStreamToStream(file.InputStream, fileStream);
                }
            }

			return storagePath;
		}

		public void DeleteFile(File file)
		{
            using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                try
                {
                    S3FileInfo fileToDelete = new S3FileInfo(client, Bucket, file.StoragePath);
                    fileToDelete.Delete();
                }
                catch (AmazonS3Exception as3e)
                {
                    throw new DocumentException(string.Format("An error occurred trying to delete {0} from bucket {1}", file.StoragePath, Bucket), as3e);
                }
            }
		}

		public Stream OpenRead(File file)
		{
			return OpenRead(file.StoragePath);
		}

		public Stream OpenRead(string storagePath)
		{
            Stream stream = null;
            using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                try
                {
                    S3FileInfo fileInfo = new S3FileInfo(client, Bucket, storagePath);
                    stream = fileInfo.OpenRead();
                }
                catch (AmazonS3Exception as3e)
                {
                    throw new DocumentException(string.Format("An error occurred trying to read {0} from bucket {1}", storagePath, Bucket), as3e);
                }
            }

            return stream;
		}

        public void StreamZippedDocuments(IList<Document> documents, Stream outputStream)
        {
            using (var zipOutputStream = new ZipOutputStream(outputStream))
            {
                zipOutputStream.IsStreamOwner = false;
                DocumentManager.WriteZipFile(zipOutputStream, documents, this);
            }
        }

		public bool IsValidFileType(File file)
		{
			var fileExtensions = Configuration.FileExtensions.Cast<FileExtensionConfigElement>();
			return (fileExtensions.Count(x => x.Extension == file.Extension) > 0);
		}

		public string RemoveInvalidFileNameCharacters(string fileName)
		{
			StringBuilder outName = new StringBuilder();
			foreach (char c in fileName.ToCharArray())
			{
				bool isCharValid = Configuration.InvalidCharacters.Cast<KeyValueConfigurationElement>().Count(x => x.Key == Convert.ToInt32(c).ToString()) == 0;
				if (isCharValid)
				{
					outName.Append(c);
				}
			}

			return outName.ToString();
		}
		#endregion IFileStore methods

		#region Utility Methods
		private string GetStoredFilePath(File file, int uniqueInt)
		{
			return
                UploadSubFolder + @"\" + 
                Timestamp + "_" +
				uniqueInt + "_" +
				file.OriginalName;
		}
		#endregion
	}
}
