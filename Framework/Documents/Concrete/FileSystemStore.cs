﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using Aquilent.Cara.Configuration.FileStore;
using Aquilent.Cara.Configuration;
using log4net;
using System.Configuration;

namespace Aquilent.Framework.Documents
{
	public class FileSystemStore : IFileStore
	{
		#region Members
		public FileStoreConfiguration Configuration { get; private set; }
		#endregion Members

		#region Properties
		/// <summary>
		/// Read-only.  Returns the path to the upload folder
		/// </summary>
		public string UploadFolder { get; private set; }

		/// <summary>
		/// Read-only.  Returns the path to the temporary zip folder
		/// </summary>
		public string TempZipFolder { get; private set; }

		/// <summary>
		/// Returns the subfolder per month and year.  Format:  YYYYMM
		/// </summary>
		private string UploadSubFolder
		{
			get
			{
				DateTime now = DateTime.Now;
				string uploadSubFolder = now.Year +	now.Month.ToString().PadLeft(2, '0');

				// Create the subfolder if necessary
				if (!Directory.Exists(UploadFolder + @"\" +	uploadSubFolder))
				{
					Directory.CreateDirectory(UploadFolder + @"\" +	uploadSubFolder);
				}

				return uploadSubFolder;
			}
		}

		/// <summary>
		/// Read-only.  Timestamp in the format ddhhmmss
		///   dd = date of month
		///   hh = hour
		///   mm = minutes
		///   ss = seconds
		/// </summary>
		private string Timestamp
		{
			get
			{
				DateTime now = DateTime.Now;
				return now.Day.ToString().PadLeft(2, '0') +
					now.Hour.ToString().PadLeft(2, '0') +
					now.Minute.ToString().PadLeft(2, '0') +
					now.Second.ToString().PadLeft(2, '0');
			}
		}
		#endregion

		#region Constructor
		public FileSystemStore(string configSectionName)
		{
			Configuration = ConfigUtilities.GetSection<FileStoreConfiguration>(configSectionName);
			Init();
		}
		#endregion Constructor

		#region IFileStore methods
		/// <summary>
		/// Parameters required:
		///   uploadFolder - The root directory where uploaded files are stored
		///   tempZipFolder - The directory where temporary zip files are stored
		/// </summary>
		/// <param name="parameters"></param>
		public void Init()
		{
			UploadFolder = Configuration.UploadFolder.DirectoryPath;
			TempZipFolder = Configuration.TempZipFolder.DirectoryPath;
		}

		public string SaveFile(File file)
		{
			// Delete temp zip files containg
			// a file that is being updated.
			if (file.HasContents)
			{
				TempZipFile.FindAndDeleteWithFile(file, TempZipFolder);
			}

			string storagePath = GetStoredFilePath(file, new Random().Next(1000));

			// ---------------------------------------
			// Save the input file to the storage path
			//
			//		NOTE:  The calling method is responsible 
			//			   for managing file.InputStream
			// ---------------------------------------
			using (FileStream fileStream = new FileStream(
				GetAbsoluteStoragePath(storagePath),
				FileMode.Create, FileAccess.Write))
			{
				// Read from the input file and write to the output file
				DocumentManager.WriteStreamToStream(file.InputStream, fileStream);
			}

			return storagePath;
		}

		public void DeleteFile(File file)
		{
			System.IO.File.Delete(GetAbsoluteStoragePath(file.StoragePath));
		}

		public Stream OpenRead(File file)
		{
			return OpenRead(file.StoragePath);
		}

		public Stream OpenRead(string storagePath)
		{
			return System.IO.File.OpenRead(GetAbsoluteStoragePath(storagePath));
		}

		public void StreamZippedDocuments(IList<Document> documents, Stream outputStream)
		{
			ZipOutputStream zipOutputStream = null;
			Stream zipInputStream = null;
			try
			{
				TempZipFile zipFile = new TempZipFile(documents, TempZipFolder);
				// 260 characters is the maximum number of characters allowed
				// in the full path of a file in Windows.  If the path is greater
				// than 260, the temp zip file is not created and is directly
				// streamed to the outputStream
				if (zipFile.Path.Length <= 255)
				{
					if (!zipFile.Exists)
					{
						zipOutputStream = new ZipOutputStream(zipFile.OpenOutputStream());
						DocumentManager.WriteZipFile(zipOutputStream, documents, this);
						zipOutputStream.Close();
					}

					zipInputStream = zipFile.OpenInputStream();
					DocumentManager.WriteStreamToStream(zipInputStream, outputStream);
					zipInputStream.Close();
				}
				else
				{
					zipOutputStream = new ZipOutputStream(outputStream);
					DocumentManager.WriteZipFile(zipOutputStream, documents, this);
					zipOutputStream.Close();
				}
			}
			finally
			{
				// Close the streams, if necessary
				if (zipOutputStream != null)
				{
					zipOutputStream.Close();
				}

				if (zipInputStream != null)
				{
					zipInputStream.Close();
				}
			}
		}

		public bool IsValidFileType(File file)
		{
			var fileExtensions = Configuration.FileExtensions.Cast<FileExtensionConfigElement>();
			return (fileExtensions.Count(x => x.Extension == file.Extension) > 0);
		}

		public string RemoveInvalidFileNameCharacters(string fileName)
		{
			StringBuilder outName = new StringBuilder();
			foreach (char c in fileName.ToCharArray())
			{
				bool isCharValid = Configuration.InvalidCharacters.Cast<KeyValueConfigurationElement>().Count(x => x.Key == Convert.ToInt32(c).ToString()) == 0;
				if (isCharValid)
				{
					outName.Append(c);
				}
			}

			return outName.ToString();
		}
		#endregion IFileStore methods

		#region Utility Methods
		private string GetStoredFilePath(File file, int uniqueInt)
		{
			return UploadSubFolder + @"\" +
				Timestamp + "_" +
				uniqueInt + "_" +
				file.OriginalName;
		}

		public string GetAbsoluteStoragePath(string storagePath)
		{
			return UploadFolder + @"\" + storagePath;
		}

		private void WriteZipFile(ZipOutputStream zipStream, IList<Document> documents)
		{
			Stream fileStream;
			ZipEntry zipEntry;
			foreach (Document document in documents)
			{
				if (document.HasFile)
				{
					File file = document.File;
					fileStream = OpenRead(file.StoragePath);

					// Add the file to the zip file
					zipEntry = new ZipEntry(file.OriginalName);
					zipStream.PutNextEntry(zipEntry);

					DocumentManager.WriteStreamToStream(fileStream, zipStream);
					zipStream.CloseEntry();
					fileStream.Close();
				}
			}
		}
		#endregion

		#region Inner-Class: TempZipFile
		private class TempZipFile
		{
			#region Constants
			private const string PREFIX = "attachments_";
			private const string SUFFIX = ".zip";
			#endregion

			#region Private Members
			private string tempZipFolder;
			private StringBuilder filename = new StringBuilder();
			private string path = null;
			#endregion

			#region Properties
			public string Path
			{
				get
				{
					if (path == null)
					{
						path = tempZipFolder + @"\" + filename;
					}

					return path;
				}
			}

			public bool Exists
			{
				get
				{
					if (System.IO.File.Exists(Path))
					{
					    return true;
					}

					return false;
				}
			}
			#endregion

			#region Constructors
			public TempZipFile(IList<Document> documents, string tempZipFolder)
			{
				this.tempZipFolder = tempZipFolder;

				// Sort list of IDs
				List<int> ids = new List<int>();
				foreach (Document doc in documents)
				{
					ids.Add(doc.DocumentId);
				}
				ids.Sort();
				StringBuilder documentIds = new StringBuilder();
				// Create filename
				foreach (int id in ids)
				{
					documentIds.Append(id);
					documentIds.Append("_");
				}

                SHA512 sha512 = new SHA512CryptoServiceProvider();
				string hashedIds = Convert.ToBase64String(sha512.ComputeHash(ASCIIEncoding.ASCII.GetBytes(documentIds.ToString())));

				// All hashcodes end with ==
				// It seems that the only other characters that appear in a hashcode are alphanumeric and forward slashes
				// Windows automagically converts forward slashses to back slashes in file names
				// That is why we have to replace forward slashes
				hashedIds = hashedIds.Replace("/", "_");

				filename.Append(PREFIX);
				filename.Append(hashedIds);
				filename.Append(SUFFIX);
			}
			#endregion

			#region Methods
			/// <summary>
			/// Returns an IO stream to read from
			/// </summary>
			public Stream OpenInputStream()
			{
				return System.IO.File.OpenRead(Path);
			}

			/// <summary>
			/// Returns an IO stream to write to
			/// </summary>
			public Stream OpenOutputStream()
			{
				return System.IO.File.OpenWrite(Path);
			}

			/// <summary>
			/// Removes any temp zip files that contains a document
			/// with a given documentId
			/// </summary>
			/// <param name="documentId"></param>
			public static void FindAndDeleteWithFile(
					File file,
					string zipFolderPath)
			{
				string[] zipfilenames = Directory.GetFiles(
					zipFolderPath, "*.zip");
				bool flag = false;
				foreach (string zipfile in zipfilenames)
				{
					flag = false;
					FileStream localZipFileStream = System.IO.File.OpenRead(zipfile);
					ZipInputStream localZipFile = new ZipInputStream(localZipFileStream);
					ZipEntry localZipEntry = localZipFile.GetNextEntry();
					while (localZipEntry != null)
					{
						if (localZipEntry.Name.IndexOf(file.OriginalName) != -1)
						{
							localZipEntry = null;
							flag = true;
							localZipFileStream.Close();
						}
						else
						{
							localZipEntry = localZipFile.GetNextEntry();
						}
					}
					if (flag == true)
					{
						try
						{
							System.IO.File.Delete(zipfile);
						}
						catch (Exception e)
						{
							LogManager.GetLogger(typeof(TempZipFile)).Info(
								"Could not " +
								"delete the file " + localZipEntry.Name + ": " +
								e.Message);
						}
					}
				}
			}
			#endregion
		}
		#endregion

	}
}
