﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Amazon.S3;
using Amazon.S3.IO;

namespace Aquilent.Framework.Documents.Web.Mvc
{
    public class S3FileResult : ActionResult
    {
        private string bucket;
        private string filename;
        private string mimeType;

        public S3FileResult(string bucket, string filename)
        {
            if (string.IsNullOrEmpty(bucket) == null)
            {
                throw new ArgumentNullException("bucket");
            }

            if (string.IsNullOrEmpty(filename))
            {
                throw new ArgumentNullException("filename");
            }

            this.bucket = bucket;
            this.filename = filename;
        }

        public S3FileResult(string bucket, string filename, string mimeType)
            : this(bucket, filename)
        {
            this.mimeType = mimeType;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase response = context.HttpContext.Response;
            response.Clear();

            if (!string.IsNullOrEmpty(mimeType))
            {
                response.ContentType = mimeType;
            }
            else
            {
                response.ContentType = "application/octet-stream";
            }

            response.Headers.Add("content-disposition", "attachment;  filename=" + filename);

            using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, bucket);
                var s3FileInfo = rootDirectory.GetFile(filename);

                if (s3FileInfo.Exists)
                {
                    var fileStream = s3FileInfo.OpenRead();
                    DocumentManager.WriteStreamToStream(fileStream, response.OutputStream);
                }
                else
                {
                    response.StatusCode = 404;
                }
            }
            response.End();
        }

        public static S3FileResult File(string bucket, string filename, string mimeType = null)
        {
            S3FileResult result;

            if (mimeType == null)
            {
                return new S3FileResult(bucket, filename);
            }
            else
            {
                return new S3FileResult(bucket, filename, mimeType);
            }
        }
    }
}