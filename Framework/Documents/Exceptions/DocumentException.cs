﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Documents
{
	public class DocumentException : FrameworkException
	{
		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public DocumentException()
			: base()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public DocumentException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public DocumentException(string message, Exception innerException) :
			base(message, innerException)
		{
		}
		#endregion        
	}
}
