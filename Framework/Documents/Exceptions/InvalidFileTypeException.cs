﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Documents
{
	public class InvalidFileTypeException : DocumentException
	{
		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public InvalidFileTypeException()
			: base()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public InvalidFileTypeException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
        public InvalidFileTypeException(string message, Exception innerException) :
			base(message, innerException)
		{
		}
		#endregion        
	}
}
