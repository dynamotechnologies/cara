﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Aquilent.Framework.Services;

namespace Aquilent.Framework.Services.Samples
{
    public class ServiceTaskSample3 : IServiceTask
    {
        public string Name
        {
            get { return "Service Task Sample 3"; }
        }

        public void RunServiceTask()
        {
            log4net.ThreadContext.Properties["ThreadContext"] = Name;
            var logger = LogManager.GetLogger(this.GetType());

            logger.Info(Name);
        }
    }
}
