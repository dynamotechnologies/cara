﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Collections;
using Aquilent.Framework.Configuration.ServiceTasks;
using System.Timers;
using System.Globalization;

namespace Aquilent.Framework.Services
{
    public class ServiceTaskSchedule
    {
        #region Constants
        private const string dateFormat = "HH:mm:ss";
        #endregion

        #region Private Members

        private ILog logger;

        #region Scheduling
        private Queue startTimes;
        private DateTime dtStartTime;

        private bool tooEarly = false;
        private DateTime nextRunTime = DateTime.MinValue; // time for next run

        private IList<IServiceTask> tasks;
        #endregion Scheduling

        #endregion Private Members

        #region Constructor
        public ServiceTaskSchedule(Schedule schedule)
        {
            logger = LogManager.GetLogger(typeof(ServiceTaskSchedule));

            Name = schedule.Name;
            RebootDelay = new TimeSpan(0, schedule.RebootDelay, 0);
            RunOnStart = schedule.RunOnStart;
            ShowNextRunTime = schedule.ShowNextRunTime;
            Interval = new TimeSpan(0, schedule.Interval, 0);
            StartTime = schedule.StartTime;

            if (CheckScheduleValidity())
            {
                InitializeServiceTasks(schedule);
                InitializeTimer();
            }
            else
            {
                Logger.ErrorFormat("[{1}] Invalid Schedule: {0}", schedule, Name);
            }
        }
  
        #endregion Constructor

        #region Initializers
        private void InitializeTimer()
        {
            Timer = new Timer();
            //((System.ComponentModel.ISupportInitialize)(Timer)).BeginInit();
            Timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            //((System.ComponentModel.ISupportInitialize)(Timer)).EndInit();
        }

        private void InitializeServiceTasks(Schedule schedule)
        {
            if (schedule.ServiceTasks != null && schedule.ServiceTasks.Length > 0)
            {
                if (tasks == null)
                {
                    tasks = new List<IServiceTask>(schedule.ServiceTasks.Length);
                }

                foreach (var st in schedule.ServiceTasks)
                {
                    try
                    {
                        var task = (IServiceTask)st.Unwrap(); 
                        tasks.Add(task);
                    }
                    catch (InvalidCastException)
                    {
                        Logger.WarnFormat("[{1}] Error casting {0} to IServiceTask", st.ToString(), Name);
                    }
                }
            }
        }

        /// <summary>
        /// Checks the service schedule for validity
        /// </summary>
        private bool CheckScheduleValidity()
        {
            Logger.DebugFormat("[{0}] Checking schedule validity", Name);
            bool isValid = true;

            if (!string.IsNullOrEmpty(ScheduledTimes))
            {
                isValid = ParseScheduledTimes(ScheduledTimes);
                if (Interval.TotalMinutes == 0)
                {
                    Logger.InfoFormat("[{0}] Implicit schedule (interval) will be ignored since explicit schedule (scheduledTimes) takes precedence", Name);
                }

                if (string.IsNullOrEmpty(StartTime))
                {
                    Logger.InfoFormat("[{0}] StartTime will be ignored since explicit schedule (scheduledTimes) takes precedence", Name);
                }
            }
            else
            {
                if (Interval.TotalMinutes == 0)
                {
                    Logger.ErrorFormat("[{0}] If no explicit schedule was given, then interval must be defined", Name);
                    isValid = false;
                }

                isValid = ParseStartTime(StartTime);
                Logger.DebugFormat("[{0}] Interval: {1}", Name, Interval.TotalMinutes + " minute" + (Interval.TotalMinutes == 1 ? "" : "s"));
            }

            Logger.DebugFormat("[{0}] Schedule is {1}", Name, (isValid ? "" : "in") + "valid");
            return isValid;
        }

        /// <summary>
        /// Splits comma-separated times into a queue of time, sorted.
        /// </summary>
        private bool ParseScheduledTimes(string scheduledTimes)
        {
            Logger.DebugFormat("[{0}] Parsing scheduled times", Name);
            bool isValid = true;

            // If explicit schedule given, parse into list
            if (string.IsNullOrWhiteSpace(scheduledTimes))
            {
                isValid = false;
                Logger.ErrorFormat("[{0}] scheduledTimes cannot be empty", Name);
            }
            else
            {
                DateTimeFormatInfo dtInfo = new DateTimeFormatInfo();
                List<DateTime> tempSchedule = new List<DateTime>();

                string[] scheduledTimesArray
                    = scheduledTimes.Split(new string[] { ",", ", " }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string scheduledTime in scheduledTimesArray)
                {
                    // convert to date. ignore badly formed dates
                    try
                    {
                        tempSchedule.Add(DateTime.ParseExact(scheduledTime, dateFormat, dtInfo));
                    }
                    catch (Exception ex)
                    {
                        isValid = false;
                        Logger.ErrorFormat("[{2}] Error parsing scheduled time '{0}': {1}", scheduledTime, ex.Message, Name);
                        Logger.InfoFormat("[{1}] Correct format is: {0}", dateFormat, Name);
                    }
                }

                // Sort and add to queue
                tempSchedule.Sort();
                startTimes = new Queue(tempSchedule.Count);
                foreach (DateTime tempTime in tempSchedule)
                {
                    startTimes.Enqueue(tempTime);
                }
                tempSchedule.Clear();

                if (startTimes == null || startTimes.Count == 0)
                {
                    Logger.ErrorFormat("[{0}] There must be at least 1 valid time on the explicit schedule (scheduledTimes)", Name);
                    isValid = false;
                }
                else
                {
                    foreach (DateTime t in startTimes)
                    {
                        Logger.DebugFormat("[{0}] Schedule: {1}", Name, t.ToString());
                    }
                }
            }

            Logger.DebugFormat("[{0}] Parsed schedule is {1}", Name, (isValid ? "" : "in") + "valid");
            return isValid;
        }

        /// <summary>
        /// Parses the startTime
        /// </summary>
        private bool ParseStartTime(string sStartTime)
        {
            Logger.DebugFormat("[{0}] Parsing start time", Name);
            bool isValid = true;

            // if no start date was specified, default to now
            if (sStartTime == null)
            {
                dtStartTime = DateTime.Now;
            }
            else
            {
                // if exact start time was specified (no wildcards)
                if (sStartTime.IndexOf('#') == -1)
                {
                    try
                    {
                        DateTimeFormatInfo dtInfo = new DateTimeFormatInfo();
                        dtStartTime = DateTime.ParseExact(sStartTime, dateFormat, dtInfo);
                    }
                    catch (Exception)
                    {
                        isValid = false;
                        Logger.ErrorFormat("[{0}] StartTime '{1}' is not valid", Name, sStartTime);
                        Logger.InfoFormat("[{1}] Correct format is: {0}", dateFormat, Name);
                    }
                }

                else // if start date has wildcards (##)
                {
                    DateTime now = DateTime.Now;

                    int hours = now.Hour;
                    if (sStartTime.Substring(0, 2).IndexOf('#') == -1)
                    {
                        try
                        {
                            hours = Convert.ToInt32(sStartTime.Substring(0, 2));
                            if (hours < 0 || hours >= 24)
                            {
                                throw new FormatException();
                            }
                        }
                        catch (FormatException)
                        {
                            Logger.ErrorFormat("[{0}] Hours in StartTime '{1}' are not valid", Name, sStartTime);
                            isValid = false;
                        }
                    }

                    int minutes = now.Minute;
                    if (sStartTime.Substring(3, 2).IndexOf('#') == -1)
                    {
                        try
                        {
                            minutes = Convert.ToInt32(sStartTime.Substring(3, 2));
                            if (minutes < 0 || minutes >= 60)
                            {
                                throw new FormatException();
                            }
                        }
                        catch (FormatException)
                        {
                            Logger.ErrorFormat("[{0}] Minutes in StartTime '{1}' are not valid", Name, sStartTime);
                            isValid = false;
                        }
                    }

                    int seconds = now.Second;
                    if (sStartTime.Substring(6, 2).IndexOf('#') == -1)
                    {
                        try
                        {
                            seconds = Convert.ToInt32(sStartTime.Substring(6, 2));
                            if (seconds < 0 || seconds >= 60)
                            {
                                throw new FormatException();
                            }
                        }
                        catch (FormatException)
                        {
                            Logger.ErrorFormat("[{0}] Seconds in StartTime '{1}' are not valid", Name, sStartTime);
                            isValid = false;
                        }
                    }

                    dtStartTime = new DateTime(now.Year, now.Month, now.Day, hours, minutes, seconds);
                }
            }

            Logger.DebugFormat("[{0}] {1}", Name, isValid ? "Start time: " + dtStartTime.FormatDate() : "Invalid");

            return isValid;
        }
        #endregion Initializers


        #region Properties
        public Timer Timer { get; set; }
        public IList<IServiceTask> ServiceTasks
        {
            get
            {
                return tasks;
            }
        }

        protected string Name { get; set; }
        protected bool RunOnStart { get; set; }
        protected bool ShowNextRunTime { get; set; }
        protected TimeSpan RebootDelay { get; set; }
        protected TimeSpan Interval { get; set; }
        protected string StartTime { get; set; }
        protected string ScheduledTimes { get; set; }

        /// <summary>
        /// Provides access to the logger for this service
        /// </summary>
        protected ILog Logger
        {
            get
            {
                return logger;
            }
        }
        #endregion

        #region Methods


        /// <summary>
        /// Determines the next scheduled run time and sets the timer interval
        /// appropriately. If there's an exception, service is re-scheduled for
        /// the next minute.
        /// </summary>
        public virtual void Schedule()
        {
            try
            {
                double nextInterval;

                if (RunOnStart && nextRunTime.Equals(DateTime.MinValue))
                {
                    int ticks = Environment.TickCount;
                    Logger.DebugFormat("[{2}] Environment.TickCount = {0}, rebootDelay.Ticks = {1}", ticks, RebootDelay.Ticks, Name);
                    if (ticks >= 0 && ticks < RebootDelay.Ticks / (float)TimeSpan.TicksPerMillisecond)
                    {
                        // schedule service to run at 'rebootDelay' seconds after the computer last booted
                        nextInterval = RebootDelay.Ticks - ticks;
                        Logger.DebugFormat("[{0}] Machine rebooted {1} milliseconds ago", Name, ticks);
                    }
                    else
                    {
                        // this trick is used to prevent long-running services from hanging
                        // when the service is started (if runOnStart is true)
                        nextInterval = 1000; // 1 sec
                    }

                    nextRunTime = DateTime.Now.AddMilliseconds(nextInterval);
                }
                else
                {
                    if (tooEarly)
                    {
                        // in case the timer elapses too early,
                        // just reschedule for the previously determined nextRunTime
                        // (make sure that time has not passed while code was executing)
                        nextInterval = nextRunTime.Subtract(DateTime.Now).TotalMilliseconds;
                        if (nextInterval < 0)
                        {
                            nextInterval = 1;
                        }
                    }
                    else
                    {
                        // explicit schedule takes priority
                        if (startTimes != null)
                        {
                            nextInterval = GetNextInterval(startTimes);
                        }
                        else
                        {
                            nextInterval = GetNextInterval(dtStartTime, Interval);
                        }
                    }
                }

                Logger.DebugFormat("[{0}] Setting interval to {1} milliseconds", Name, nextInterval);

                Timer.AutoReset = false;

                // CARA-1301
                // Do comparison to avoid ArgumentOutOfRangeException
                // If this is not true, then Timer.Interval defaults to 100 ms
                if (nextInterval > 0.0 && nextInterval <= Convert.ToDouble(Int32.MaxValue))
                {
                    Timer.Interval = nextInterval;
                }
                else
                {
                    Timer.Interval = 1000;
                    Logger.WarnFormat("Timer interval is not valid ({0}), defaulting to 1 second", nextInterval);
                }

                Timer.Start();

                if (!tooEarly)
                {
                    if (ShowNextRunTime)
                    {
                        Logger.InfoFormat("[{0}] Next scheduled run time: {1}", Name, nextRunTime.FormatDate());
                    }
                    else
                    {
                        Logger.DebugFormat("[{0}] Next scheduled run time: {1}", Name, nextRunTime.FormatDate());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("[{0}] Error calculating next run time", Name);
                HandleException(ex);
            }
        }

        /// <summary>
        /// Gets the interval (in ms) to the next scheduled run time
        /// </summary>
        /// <param name="scheduledTimes">List of explicit schedule</param>
        protected virtual double GetNextInterval(Queue schedule)
        {
            DateTime now = DateTime.Now;
            DateTime nextRunDate = nextRunTime;

            double oneMin = new TimeSpan(0, 1, 0).TotalMilliseconds;

            // Only re-scheduled for a later date if we haven't passed the last run time
            // (in case the service was paused). Leave a 1-minute window for timing issues
            if (nextRunDate.Subtract(now).TotalMilliseconds < oneMin)
            {
                nextRunDate = (DateTime)schedule.Dequeue();
                while (nextRunDate.CompareTo(now) < 0)
                {
                    schedule.Enqueue(nextRunDate.AddDays(1));       // add 24 hours and re-queue
                    nextRunDate = (DateTime)schedule.Dequeue();    // grab next date
                }

                // 24 hours and re-queue the last removed date
                schedule.Enqueue(nextRunDate.AddDays(1));
                nextRunTime = nextRunDate;
            }

            double interval = nextRunDate.Subtract(now).TotalMilliseconds;
            return interval;
        }

        /// <summary>
        /// Gets the next interval (in ms) for the service.
        /// </summary>
        /// <param name="startTime">start time</param>
        /// <param name="interval">interval in minutes</param>
        protected virtual double GetNextInterval(DateTime startTime, TimeSpan interval)
        {
            double timeLeft = 0;
            DateTime now = DateTime.Now;

            // Only re-scheduled for a later date if we have passed the next run time
            // (in case timer woke up too early, reschedule same date)
            if (nextRunTime.CompareTo(now) <= 0)
            {
                timeLeft = startTime.Subtract(now).TotalMilliseconds;

                while (timeLeft <= 0)
                {
                    startTime = startTime.AddMinutes(interval.TotalMinutes);
                    timeLeft = startTime.Subtract(now).TotalMilliseconds;
                }

                nextRunTime = startTime;
            }
            else
            {
                timeLeft = nextRunTime.Subtract(now).TotalMilliseconds;
            }

            return timeLeft;
        }
        #endregion Methods

        #region Virtual Methods for Scheduling
        /// <summary>
        /// Runs this service and schedules the next run
        /// </summary>
        protected virtual void RunAndSchedule()
        {
            // only perform task if timer did not wake up too early
            if (!tooEarly)
            {
                PerformTask();
            }

            Schedule();
        }

        /// <summary>
        /// Does the following:
        ///     1. calls ServiceTask
        /// </summary>
        public virtual void PerformTask()
        {
            if (tasks.Count > 0)
            {
                log4net.ThreadContext.Properties["ScheduleName"] = Name;

                foreach (IServiceTask task in tasks)
                {
                    log4net.ThreadContext.Properties["ServiceTaskName"] = task.Name.Replace(" ", string.Empty);
                    //logger = AbstractWindowsService.InitializeLogger(this.GetType());

                    try
                    {
                        Logger.InfoFormat("[{1}] In service task: {0}", task.Name, Name);
                        task.RunServiceTask();
                        Logger.InfoFormat("[{1}] Done with service task: {0}", task.Name, Name);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("[{0}] An error occurred while performing service task: {1}", Name, ex.Message);
                        HandleException(ex);
                    }
                }
            }
            else
            {
                Logger.WarnFormat("[{0}] There are no tasks assigned to run for this windows service.", Name);
            }
        }
        #endregion

        #region Error Handling & Reporting
        /// <summary>
        /// outputs exception information to logs and sends e-mail notification
        /// </summary>
        protected virtual void HandleException(Exception ex)
        {
            StringBuilder errorMsg = new StringBuilder();

            errorMsg.Append("===============================================\r\n");
            errorMsg.Append("INNER EXCEPTION TRACE:\r\n");
            errorMsg.Append(GetExceptionMessage4Log(ex));
            errorMsg.Append("\r\n");

            Exception ix = ex;
            while (ix.InnerException != null)
            {
                ix = ix.InnerException;
                errorMsg.Append(GetExceptionMessage4Log(ix));
                errorMsg.Append("\r\n");
            }

            errorMsg.Append("STACK TRACE:\r\n");
            errorMsg.Append(ix.StackTrace);
            errorMsg.Append("\r\n");
            errorMsg.Append("-----------------------------------------------\r\n");

            Logger.Error(errorMsg.ToString());
        }

        /// <summary>
        /// Formats and returns the exception's message for logging purposes
        /// </summary>
        protected virtual string GetExceptionMessage4Log(Exception ex)
        {
            StringBuilder message = new StringBuilder();
            message.AppendFormat("- {0}: ", ex.GetType().ToString());
            message.Append(ex.Message);

            return message.ToString();
        }

        /// <summary>
        /// Handles error messages by determining whether they need
        /// to be output to the logs and sent to the sysadmin via e-mail
        /// </summary>
        protected virtual void HandleError(string code, string errorMessage)
        {
            Logger.Error(errorMessage);
        }
        #endregion

        #region Timer Event Handler
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Timer.Stop();

            tooEarly = (nextRunTime.CompareTo(DateTime.Now) > 0);
            logger.DebugFormat("[{0}] Timer elapsed {1}", Name, (tooEarly ? " too early" : ""));

            RunAndSchedule();
        }
        #endregion

    }
}
