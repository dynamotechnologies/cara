﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Services
{
    public static class DateUtilities
    {
        public static string FormatDate(this DateTime t)
        {
            return t.ToString("yyyy-MM-dd HH:mm:ss." +
                (t.Millisecond < 100 ? "0" : "") +
                (t.Millisecond < 10 ? "0" : "") +
                 t.Millisecond);
        }
    }
}
