﻿//================================================================================
// U.S. Government Restricted Rights Notice.  The Task Order Management Software
// and Vendor Portal solutions and the documentation thereof (collectively, the
// "Software") were developed at private expense by, and are trade secrets of,
// Aquilent Corporation and are "restricted computer software" as defined in the
// clause at FAR # 52.227-19, entitled "COMMERCIAL COMPUTER SOFTWARE - RESTRICTED
// RIGHTS (JUN 1987)."  The Software may not be used, reproduced or disclosed by
// the Government except as provided in subparagraph (c)(2) of that clause.
//
// Copyright 2002-2007, Aquilent, Inc.
// Unpublished - rights reserved under the copyright laws of the United States.
//================================================================================

using System;
using System.Text;
using System.Timers;
using System.Net.Mail;
using System.Collections;
using System.ServiceProcess;
using System.Configuration;
using System.Globalization;
using System.Reflection;

using log4net;
using Aquilent.Framework.Configuration.ServiceTasks;
using System.Collections.Generic;
using Aquilent.Framework.Services;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.WindowsService;
using System.IO;
using log4net.Config;

namespace Aquilent.Framework.Services
{
	/// <summary>
	/// Abstract base class for windows services. Performs all
	/// service related tasks leaving child classes to perform just
	/// authentication and business logic related functions
	/// </summary>
	public class AbstractWindowsService : ServiceBase
	{

        #region Private Members
		private ILog logger;
        private IList<ServiceTaskSchedule> schedules;
		#endregion Private Members

		#region Public Properties
        protected IList<ServiceTaskSchedule> Schedules
        {
            get
            {
                return schedules;
            }
        }

		/// <summary>
		/// Provides access to the logger for this service
		/// </summary>
		protected ILog Logger
		{
			get
			{
				return logger;
			}
		}
		#endregion

        #region Initialization
		/// <summary>
		/// Performs initialization required by service
		/// </summary>
		/// <param name="logName">Name of logger defined in xml file</param>
		/// <param name="svcName">Name of this service</param>
        public void Init(string svcName)
        {
			InitializeComponent();
			ServiceName = svcName;
            
            log4net.ThreadContext.Properties["ServiceTaskName"] = "Master";
            logger = InitializeLogger(this);

            logger.Debug("Init");

            try
            {
                var scheduler = ConfigUtilities.GetSection<ServiceTaskSchedulerConfiguration>("serviceTaskScheduler");
                if (scheduler.Schedules != null && scheduler.Schedules.Length > 0)
                {
                    schedules = new List<ServiceTaskSchedule>(scheduler.Schedules.Length);
                    foreach (Schedule s in scheduler.Schedules)
                    {
                        if (s.Active)
                        {
                            var serviceTaskSchedule = new ServiceTaskSchedule(s);
                            schedules.Add(serviceTaskSchedule);
                        }
                        else
                        {
                            logger.InfoFormat("{0} is not active", s.Name);
                        }
                    }
                }
            }
            catch (ConfigurationErrorsException cee)
            {
                logger.Error(cee.Message);
            }
        }
  
        public static ILog InitializeLogger(object objectThatLogs)
        {
            var logger = LogManager.GetLogger(objectThatLogs.GetType());

            string log4netFile = ConfigurationManager.AppSettings["log4net.configfile"];

            FileInfo fi = null;
            if (!string.IsNullOrEmpty(log4netFile))
            {
                fi = new FileInfo(log4netFile);
            }

            if (fi != null && fi.Exists)
            {
                XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netFile));
            }
            else
            {
                logger.Warn("No valid log4net configuration file was found");
                BasicConfigurator.Configure();
            }

            return logger;
        }

        #endregion

        #region Component Designer generated code
        /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CanPauseAndContinue = true;
		}
		#endregion

        #region OnEvent Methods (start, stop, pause, continue)
        /// <summary>
		/// Start this service
		/// </summary>
		protected override void OnStart(string[] args)
		{
			StartUp("starting");
		}

		/// <summary>
		/// Continue this service
		/// </summary>
		protected override void OnContinue()
		{
			StartUp("resuming");
		}

        private void StartUp(string text)
        {
            logger.Debug("----------------------------------------------------");
            logger.InfoFormat("{0} is {1}", ServiceName, text);
        
            foreach (var s in schedules)
            {
                s.Schedule();
            }
        }

		/// <summary>
		/// Pause this service
		/// </summary>
		protected override void OnPause()
		{
			logger.Info(ServiceName + " is pausing");

            foreach (var s in schedules)
            {
                s.Timer.Stop();
            }
        }

		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			logger.Info(ServiceName + " is stopping");

            foreach (var s in schedules)
            {
                s.Timer.Stop();
            }
		}
		#endregion

	}
}
