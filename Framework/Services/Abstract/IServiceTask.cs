﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Services
{
	/// <summary>
	/// An IServiceTask object is intended to be a discrete task that is performed
	/// by a Windows Service.
	/// </summary>
	public interface IServiceTask
	{
		string Name { get; }
		void RunServiceTask();
	}
}
