﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using Aquilent.Framework.Services;
using log4net;
using System.Globalization;
using System.Collections;
using System.Configuration;
using System.Threading;

namespace Aquilent.Framework.Configuration.ServiceTasks
{
    [XmlType]
	public class Schedule
    {
        #region Private Members

        #region Related to Configuration
        private TimeSpan rebootDelay;
        private string startTime;
        private int interval;
        private string scheduledTimes;
        private bool active = true;
        #endregion Related to Configuration

        private DateTime nextRunTime = DateTime.MinValue; // time for next run
        private ILog logger;
        #endregion Private Members

        #region Configuration Properties
        /// <summary>
        /// Identifier for the schedule
        /// </summary>
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

		/// <summary>
		/// Time span to wait after server reboot
		/// before the service starts to run.
		/// </summary>
        [XmlAttribute(AttributeName = "rebootDelay", DataType = "int")]
		public int RebootDelay
		{
			get
            {
                return (int) rebootDelay.TotalMinutes;
            }

			set
			{
                if (value < 4 && value > 60)
                {
                    throw new ConfigurationErrorsException(string.Format("rebootDelay must be between 4 and 60. ({0})", ToString()));
                }

                rebootDelay = new TimeSpan(0, value, 0);
			}
		}

		/// <summary>
		/// true, to run the service on startup, then resume schedule
		/// false, to wait for next scheduled time
		/// </summary>
        [XmlAttribute(AttributeName = "runOnStart", DataType = "boolean")]
        public bool RunOnStart { get; set; }

		/// <summary>
		/// Display the next run time in the log when the current run completes
		/// </summary>
        [XmlAttribute(AttributeName = "showNextRunTime", DataType = "boolean")]
        public bool ShowNextRunTime { get; set; }

		/// <summary>
		/// Indicate whether the schedule is active
		/// </summary>
        [XmlAttribute(AttributeName = "active", DataType = "boolean")]
        public bool Active {
            get
            {
                return active;
            }

            set
            {
                active = value;
            }
        }

		/// <summary>
		/// Indicates the time to start the service the first time
		/// 
		/// Format:  hh:mm:ss, where you can use ## as a wildcard,
		/// so ##:05:00, will always start the service at the next 5 minutes
		/// past the hour after the service has started.
		/// </summary>
        [XmlAttribute(AttributeName = "startTime")]
        public string StartTime
		{
			get
			{
				return startTime;
			}

			set
			{
                string pattern = "[0-9#]{2}:[0-9#]{2}:[0-9#]{2}";
                if (!Regex.IsMatch(value, pattern))
                {
                    throw new ConfigurationErrorsException(string.Format("startTime must be in the following format:  HH:MM:SS with # used as a wildcard ({0})", ToString()));
                }

                startTime = value;
			}
		}

		/// <summary>
		/// Minutes between runs
		/// 
		/// Note: Must be used with &lt;startTime&gt;
		/// </summary>
        [XmlAttribute(AttributeName = "interval", DataType = "int")]
        public int Interval
		{
			get
			{
				return interval;
			}

			set
			{
                if (value <= 0)
                {
                    throw new ConfigurationErrorsException(string.Format("interval must be greater than 0.({0})", ToString()));
                }

                interval = value;
			}
		}

		/// <summary>
		/// Comma delimited list of explicit times to run the service
		/// 
		/// Note: Takes precedence over &lt;startTime&gt; and  &lt;interval&gt; and &lt;frequencey&gt;
		/// </summary>
        [XmlAttribute(AttributeName = "scheduledTimes")]
		public string ScheduledTimes
		{
			get
			{
				return scheduledTimes;
			}

			set
			{
                string pattern = @"(\d{1,2}:\d{2}:\d{2},){0,1}(\d{1,2}:\d{2}:\d{2})";
                if (!Regex.IsMatch(value, pattern))
                {
                    throw new ConfigurationErrorsException(string.Format("scheduledTimes must be a comma delimited list of times:  HH:MM:SS, HH:MM:SS ({0})", ToString()));
                }

                scheduledTimes = value;
            }
		}

        [XmlElement(ElementName = "serviceTask")]
        public ServiceTask[] ServiceTasks { get; set; }
        #endregion  Configuration Properties


        public override string ToString()
		{
			return string.Format("Name: {6}, Interval: {0}, RebootDelay: {1}, RunOnStart: {2}, ScheduledTimes: {3}, ShowNextRunTime: {4}, StartTime: {5}, Active: {7}",
				Interval, RebootDelay, RunOnStart, ScheduledTimes, ShowNextRunTime, StartTime, Name, Active);
		}
	}
}
