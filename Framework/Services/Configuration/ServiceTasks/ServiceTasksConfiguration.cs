﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Aquilent.Framework.Configuration.ServiceTasks
{
	public class ServiceTasksConfiguration : ConfigurationSection
	{
		[ConfigurationProperty("tasks", IsDefaultCollection = false)]
		public TasksCollection Tasks
		{
			get
			{
				return (TasksCollection) base["tasks"];
			}
		}

	}
}
