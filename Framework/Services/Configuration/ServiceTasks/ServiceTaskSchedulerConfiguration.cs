﻿using System.Xml.Serialization;

namespace Aquilent.Framework.Configuration.ServiceTasks
{
    [XmlRoot(ElementName = "serviceTaskScheduler", Namespace = "")]
	public class ServiceTaskSchedulerConfiguration
	{
        [XmlElement(ElementName = "schedule")]
        public Schedule[] Schedules { get; set; }
	}
}
