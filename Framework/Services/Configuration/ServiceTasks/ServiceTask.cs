﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Runtime.Remoting;
using System.Xml.Serialization;

namespace Aquilent.Framework.Configuration.ServiceTasks
{
	/// <summary>
	/// This represents a configuration element that contains two parts of a fully qualified class name:
	///   class name and assembly name.
	/// </summary>
    [XmlType]
	public class ServiceTask : IObjectHandle
	{
		/// <summary>
		/// The name of the assembly.
		/// 
		/// Example:
		///   Assembly.Name, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
		/// </summary>
        [XmlAttribute(AttributeName = "assembly")]
		public string AssemblyName { get; set; }

		/// <summary>
		/// The name of the class.
		/// 
		/// Example:
		///   Name.Space.Class.Name
		/// </summary>
        [XmlAttribute(AttributeName = "class")]
        public string ClassName { get; set; }

		#region IObjectHandle Members

		/// <summary>
		/// Returns an instance of the type represented by this configuration element.
		/// </summary>
		/// <returns>The unwrapped object.</returns>
		public object Unwrap()
		{
			return Activator.CreateInstance(AssemblyName, ClassName).Unwrap();
		}

		#endregion

		public override string ToString()
		{
			return string.Format("Assembly: {0}, Class: {1}", AssemblyName, ClassName);
		}
	}
}
