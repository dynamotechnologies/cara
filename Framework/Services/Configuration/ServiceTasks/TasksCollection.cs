﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Aquilent.Cara.Configuration;

namespace Aquilent.Framework.Configuration.ServiceTasks
{
	public class TasksCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new FullyQualifiedClassElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((FullyQualifiedClassElement) element).ClassName;
		}
	}
}
