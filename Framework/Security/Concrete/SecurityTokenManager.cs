﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
	public class SecurityTokenManager : AbstractEntityManager<SecurityToken>, IAuthentication
	{
        public const string NonUnit = "0000";

		public SecurityTokenManager(IRepository<SecurityToken> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a user with the given security token ID
		/// </summary>
		/// <param name="name">The ID of the security token</param>
		/// <returns>SecurityToken or null</returns>
		public override SecurityToken Get(int uniqueId)
		{
			return Repository.ObjectQuery.Include("User").Where(x => x.SecurityTokenID == uniqueId).FirstOrDefault();
		}

		/// <summary>
		/// Returns a user with the given username
		/// </summary>
		/// <param name="uniqueName">The username of the user</param>
		/// <returns>User or null</returns>
		public override SecurityToken Get(string uniqueName)
		{
			var token = new Guid(uniqueName);
			return Repository.ObjectQuery.Include("User").Where(x => x.Token == token).FirstOrDefault();
		}

		#endregion Override AbstractEntityService members

		#region IAuthentication Methods
		public SecurityToken Validate(string username, string password)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Validates a user against a security token
		/// </summary>
		/// <param name="securityToken"></param>
		/// <returns>The validated security token</returns>
		/// <exception cref="SecurityTokenException">Throws a SecurityTokenException if the token is not valid.</exception>
		public SecurityToken Validate(Guid securityToken)
		{
            var token = Get(securityToken.ToString());
            SecuritySetting setting;
            using (var settingManager = ManagerFactory.CreateInstance<SecuritySettingManager>())
            {
                setting = settingManager.Get("security.securitytoken.lifespan");
            }

			double lifespan = Convert.ToDouble(setting.Value);

			if (token != null)
			{
				if (token.AuthenticationDate.HasValue)
				{
					throw new SecurityTokenException(String.Format("This security token, {0},  has already been used.", securityToken));
				}
				else if (token.CreationDate.AddDays(lifespan) < DateTime.UtcNow)
				{
					throw new SecurityTokenException(String.Format("This security token, {0},  has expired.", securityToken));
				}
				else
				{
                    // set the token information
					token.AuthenticationDate = DateTime.UtcNow;
					token.ExpirationDate = token.AuthenticationDate.Value.AddDays(lifespan);

                    if (token.User != null)
                    {
                        // get the user privileges
                        IList<UserPrivilege> systemPrivs;
                        IList<UserPrivilege> unitPrivs;
                        IList<UserPrivilege> phasePrivs;
                        using (var privManager = ManagerFactory.CreateInstance<PrivilegeManager>())
                        {
                            var userPrivs = privManager.GetUserPrivileges(token.User.UserId);
                            systemPrivs = userPrivs.Where(x => x.PhaseId == 0 && x.UnitId == NonUnit).ToList();
                            unitPrivs = userPrivs.Where(x => x.PhaseId == 0 && x.UnitId != NonUnit).ToList();
                            phasePrivs = userPrivs.Where(x => x.PhaseId != 0).ToList();
                        }

                        #region Cache System Privileges
                        token.User.SystemPrivileges = new HashSet<string>();
                        foreach (UserPrivilege up in systemPrivs)
                        {
                            token.User.SystemPrivileges.Add(up.PrivilegeCode.Trim());
                        }
                        #endregion Cache System Privileges

                        #region Cache Unit Privileges
                        token.User.UnitPrivileges = new Dictionary<string, IList<string>>();
                        var privCodes = unitPrivs.Select(p => p.PrivilegeCode).Distinct();

                        foreach (string p in privCodes)
                        {
                            token.User.UnitPrivileges.Add(new KeyValuePair<string,IList<string>>(p.Trim(),
                                (from x in unitPrivs 
                                 where x.PrivilegeCode == p 
                                 select x.UnitId.PadRight(8, '0')) // Ensure we're storing 8 digit unit IDs
                                .ToList()));
                        }
                        #endregion Cache Unit Privileges

                        #region Cache Phase Privileges
                        token.User.PhasePrivileges = new Dictionary<string, IList<int>>();
                        privCodes = phasePrivs.Select(p => p.PrivilegeCode).Distinct();

                        foreach (string p in privCodes)
                        {
                            token.User.PhasePrivileges.Add(new KeyValuePair<string,IList<int>>(p.Trim(),
                                (from x in phasePrivs where x.PrivilegeCode == p select x.PhaseId).ToList()));
                        }
                        #endregion Cache Phase Privileges

                    }

					UnitOfWork.Save();
				}
			}

			return token.Clone();
		}

		/// <summary>
		/// Invalidates the security token and logs when the user logged out.
		/// </summary>
		/// <param name="token"></param>
		/// <param name="logoutMethod">Defaults to "User"</param>
		public void Invalidate(SecurityToken token, string logoutMethod = "User")
		{
			token.LogoutDate = DateTime.UtcNow;
			token.LogoutMethod = logoutMethod;
			UnitOfWork.Save();
		}
		#endregion IAuthentication Methods
    }
}
