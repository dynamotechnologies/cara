﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
	public class LookupRoleByName : AbstractLookup<string, Role>
	{
		protected override IList<Role> LoadValues()
		{
			return DataLoader.Load().OfType<Role>().ToList();
		}

		protected override KeyValuePair<string, Role> PrepareForLookup(Role item)
		{
			return new KeyValuePair<string, Role>(item.Name, item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(Role item)
		{
			return new KeyValuePair<string, string>(item.Name, item.Name);
		}
	}
}
