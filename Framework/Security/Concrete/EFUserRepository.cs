﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
    /// <summary>
    /// Class for a privilege repository backed by the interface privilege repository.
    /// </summary>
    public class EFUserRepository : EFRepository<User>, IUserRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFUserRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for security context
        /// </summary>
        public ISecurityContext SecurityContext
        {
            get
            {
                return (UnitOfWork.Context is ISecurityContext ? (UnitOfWork.Context as ISecurityContext) : null);
            }
        }

        #region User Methods
        /// <summary>
        /// Implements the GetUserUsers interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<UserSearchResult> GetUsers(UserSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return SecurityContext.GetUsers(filter, total);
        }
        #endregion
    }
}
