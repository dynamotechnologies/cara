﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Linq.Expressions;

namespace Aquilent.Framework.Security
{
	public class PrivilegeManager : AbstractEntityManager<Privilege>
	{
		public PrivilegeManager(IPrivilegeRepository repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a privilege with the given privilege ID
		/// </summary>
		/// <param name="name">The ID of the privilege</param>
		/// <returns>Privilege or null</returns>
		public override Privilege Get(int uniqueId)
		{
			return All.Where(x => x.PrivilegeId == uniqueId).FirstOrDefault();
		}

		/// <summary>
		/// Returns a privilege with the given code
		/// </summary>
		/// <param name="uniqueName">The code of the privilege</param>
		/// <returns>Privilege or null</returns>
		public override Privilege Get(string uniqueName)
		{
			return All.Where(x => x.Code == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

        #region User Privilege Methods
        /// <summary>
        /// Returns a list of user system and phase privileges
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<UserPrivilege> GetUserPrivileges(int userId)
        {
            return (Repository as IPrivilegeRepository).GetUserPrivileges(userId);
        }
        #endregion
    }
}
