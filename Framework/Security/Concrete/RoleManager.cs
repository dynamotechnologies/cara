﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Linq.Expressions;

namespace Aquilent.Framework.Security
{
	public class RoleManager : AbstractEntityManager<Role>
	{
		public RoleManager(IRepository<Role> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a role with the given role ID
		/// </summary>
		/// <param name="name">The ID of the role</param>
		/// <returns>Role or null</returns>
		public override Role Get(int uniqueId)
		{
			return All.Where(x => x.RoleId == uniqueId).FirstOrDefault();
		}

		/// <summary>
		/// Returns a role with the given role name
		/// </summary>
		/// <param name="uniqueName">The name of the role</param>
		/// <returns>Role or null</returns>
		public override Role Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members
	}
}
