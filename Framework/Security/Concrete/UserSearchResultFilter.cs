﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Security
{
    /// <summary>
    /// Filter class for UserSearchResult
    /// </summary>
    public class UserSearchResultFilter : IFilter
    {
        #region Public Members
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UnitId { get; set; }
        public bool ShowOnlyNonProjectRoles { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }

        public string ParentUnitId
        {
            get
            {
                string parentUnitId = UnitId;
                if (!string.IsNullOrWhiteSpace(UnitId))
                {
                    if (UnitId.EndsWith("000000"))
                    {
                        parentUnitId = UnitId.Substring(0, UnitId.Length - 6);
                    }
                    else if (UnitId.EndsWith("0000"))
                    {
                        parentUnitId = UnitId.Substring(0, UnitId.Length - 4);
                    }
                    else if (UnitId.EndsWith("00"))
                    {
                        parentUnitId = UnitId.Substring(0, UnitId.Length - 2);
                    }
                }

                return parentUnitId;
            }
        }
        #endregion
    }
}
