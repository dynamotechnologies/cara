﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
    /// <summary>
    /// Class for a privilege repository backed by the interface privilege repository.
    /// </summary>
    public class EFPrivilegeRepository : EFRepository<Privilege>, IPrivilegeRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFPrivilegeRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for security context
        /// </summary>
        public ISecurityContext SecurityContext
        {
            get
            {
                return (UnitOfWork.Context is ISecurityContext ? (UnitOfWork.Context as ISecurityContext) : null);
            }
        }

        #region Privilege Methods
        /// <summary>
        /// Implements the GetUserPrivileges interface
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<UserPrivilege> GetUserPrivileges(int userId)
        {
            // call the method from unit of work context
            return SecurityContext.GetUserPrivileges(userId);
        }
        #endregion
    }
}
