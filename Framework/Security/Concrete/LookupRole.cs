﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
	public class LookupRole : AbstractLookup<int, Role>
	{
		protected override IList<Role> LoadValues()
		{
			return DataLoader.Load().OfType<Role>().ToList();
		}

		protected override KeyValuePair<int, Role> PrepareForLookup(Role item)
		{
			return new KeyValuePair<int, Role>(item.RoleId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(Role item)
		{
			return new KeyValuePair<int, string>(item.RoleId, item.Name);
		}
	}
}
