﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Linq.Expressions;

namespace Aquilent.Framework.Security
{
	public class FakeSecurityTokenRepository : IRepository<SecurityToken>
	{
		private IList<SecurityToken> _list = new List<SecurityToken>();

		public FakeSecurityTokenRepository(IUnitOfWork unitOfWork)
		{
			UnitOfWork = unitOfWork;
		}

		#region IRepository members
		public AbstractEntityManager<SecurityToken> Manager { get; set; }
		public IUnitOfWork UnitOfWork {	get; set; }
		public System.Data.Objects.IObjectSet<SecurityToken> ObjectSet
		{
			get { throw new NotImplementedException(); }
		}

		public System.Data.Objects.ObjectQuery<SecurityToken> ObjectQuery
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<SecurityToken> All
		{
			get
			{
				return _list.AsQueryable();
			}
		}

		public void Add(SecurityToken entity)
		{
			if (_list.Count == 0)
			{
				entity.SecurityTokenID = 1;
			}
			else
			{
				entity.SecurityTokenID = All.Max(x => x.SecurityTokenID) + 1;
			}

			entity.CreationDate = DateTime.UtcNow;

			_list.Add(entity);
		}

		public void Delete(SecurityToken entity)
		{
			_list.Remove(entity);
		}
		#endregion IRepository members
	}
}
