﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using System.Linq.Expressions;

namespace Aquilent.Framework.Security
{
	public class FakeUserRepository : IRepository<User>
	{
		private IList<User> _users = new List<User>();

		public FakeUserRepository(IUnitOfWork unitOfWork)
		{
			User[] temp = new User[]
			{
				new User
				{
					UserId = 1,
					FirstName = "Chris",
					LastName = "Cooley",
					Email = "latha.davuluri@aquilent.com",
					Username = "ccooley"
				},
				new User
				{
					UserId = 2,
					FirstName = "Han",
					LastName = "Solo",
					Email = "latha.davuluri@aquilent.com",
					Username = "hsolo"
				},
				new User
				{
					UserId = 3,
					FirstName = "Bilbo",
					LastName = "Baggins",
					Email = "latha.davuluri@aquilent.com",
					Username = "bbaggins"
				},
				new User
				{
					UserId = 4,
					FirstName = "Jason",
					LastName = "Vorhees",
					Email = "latha.davuluri@aquilent.com",
					Username = "jvorhees"
				}
			};

			_users = temp.ToList();

			UnitOfWork = unitOfWork;
		}


		#region IRepository members
		public AbstractEntityManager<User> Manager { get; set; }
		public IUnitOfWork UnitOfWork {	get; set; }
		public System.Data.Objects.IObjectSet<User> ObjectSet
		{
			get { throw new NotImplementedException(); }
		}

		public System.Data.Objects.ObjectQuery<User> ObjectQuery
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<User> All
		{
			get
			{
				return _users.AsQueryable();
			}
		}

		public void Add(User entity)
		{
			_users.Add(entity);
		}

		public void Delete(User entity)
		{
			_users.Remove(entity);
		}
		#endregion IRepository members
	}
}
