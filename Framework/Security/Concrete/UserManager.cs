﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Aquilent.EntityAccess;


namespace Aquilent.Framework.Security
{
	public class UserManager : AbstractEntityManager<User>
	{
		public UserManager(IUserRepository repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a user with the given user ID
		/// </summary>
		/// <param name="name">The ID of the user</param>
		/// <returns>User or null</returns>
		public override User Get(int uniqueId)
		{
			return All.Where(x => x.UserId == uniqueId).FirstOrDefault();
		}

		/// <summary>
		/// Returns a user with the given username
		/// </summary>
		/// <param name="uniqueName">The username of the user</param>
		/// <returns>User or null</returns>
		public override User Get(string uniqueName)
		{
			return All.Where(x => x.Username == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

		#region Custom Methods
		/// <summary>
		/// Retreives a list of users with the IDs
		/// </summary>
		/// <param name="userIds">a list of integers</param>
		/// <param name="include">which other relationship(s) to eager load (defaults to null)</param>
		/// <returns>List of users</returns>
		public IList<User> Get(IList<int> userIds, string include = null)
		{
			var users = All;
			if (! string.IsNullOrEmpty(include))
			{
				users = Repository.ObjectQuery.Include(include);
			}

			var query = from x in users
						where userIds.Contains(x.UserId)
						select x;

			return query.ToList();
		}

        /// <summary>
        /// Retreives a list of users for listing page
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <returns></returns>
        public IList<UserSearchResult> GetUsers(UserSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<UserSearchResult> list = (Repository as IUserRepository).GetUsers(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }
		#endregion
	}
}
