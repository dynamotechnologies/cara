﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Security
{
	public class SecurityTokenException : ApplicationException
	{
		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public SecurityTokenException()
			: base()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public SecurityTokenException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public SecurityTokenException(string message, Exception innerException) :
			base(message, innerException)
		{
		}
		#endregion        
	}
}
