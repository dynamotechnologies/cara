﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Aquilent.Framework.Security
{
	public static class CryptoStringExtensions
	{
		public static string ComputeHash(this string valueToHash, HashAlgorithm hashAlgorithm)
		{
			if (hashAlgorithm == null)
			{
				throw new ArgumentException("The value cannot be null", "hashAlgorithm");
			}

			byte[] valueInBytes = ASCIIEncoding.ASCII.GetBytes(valueToHash);
			byte[] result = hashAlgorithm.ComputeHash(valueInBytes);

			return Convert.ToBase64String(result);
		}

		public static string ComputeHash(this Stream streamToHash, HashAlgorithm hashAlgorithm)
		{
			if (hashAlgorithm == null)
			{
				throw new ArgumentException("The value cannot be null", "hashAlgorithm");
			}

			byte[] result = hashAlgorithm.ComputeHash(streamToHash);

			return Convert.ToBase64String(result);
		}
	}
}
