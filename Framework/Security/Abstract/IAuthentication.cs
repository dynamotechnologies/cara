﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Security
{
	/// <summary>
	/// An interface to define an authentication scheme
	/// </summary>
	public interface IAuthentication
	{
		/// <summary>
		/// Validate with a user name and password
		/// </summary>
		/// <param name="username">The username</param>
		/// <param name="password">The password</param>
		/// <returns>A validated user, otherwise null</returns>
		SecurityToken Validate(string username, string password);

		/// <summary>
		/// Validate with a security token
		/// </summary>
		/// <param name="securityToken">The security token</param>
		/// <returns>A validated user, otherwise null</returns>
		SecurityToken Validate(Guid securityToken);

		/// <summary>
		/// Logs a user out of the system
		/// </summary>
		/// <param name="user">The User to invalidate</param>
		/// <param name="logoutMethod">'User' if the user initiated the logout, 'Expired', if the session expired due to inactivity</param>
		void Invalidate(SecurityToken user, string logoutMethod);
	}
}
