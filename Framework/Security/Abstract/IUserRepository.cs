﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
	/// <summary>
    /// An interface to define an user repository
	/// </summary>
    public interface IUserRepository : IRepository<User>
    {
        #region User Interfaces
        /// <summary>
        /// Returns a list of users
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<UserSearchResult> GetUsers(UserSearchResultFilter filter, ObjectParameter total);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the security context
        /// </summary>
        ISecurityContext SecurityContext { get; }
        #endregion
	}
}
