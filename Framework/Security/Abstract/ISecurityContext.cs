﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
	/// <summary>
    /// An interface to define a security context
	/// </summary>
    public interface ISecurityContext
	{
        #region Security Methods
        /// <summary>
        /// Returns a list of user system and phase privileges
        /// </summary>
        /// <param name="userId">user ID</param>
        /// <returns></returns>
        IList<UserPrivilege> GetUserPrivileges(int userId);

        /// <summary>
        /// Returns a list of users based on filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<UserSearchResult> GetUsers(UserSearchResultFilter filter, ObjectParameter total);
        #endregion
	}
}
