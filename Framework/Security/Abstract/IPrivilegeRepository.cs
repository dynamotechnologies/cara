﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Framework.Security
{
	/// <summary>
    /// An interface to define an privilege repository
	/// </summary>
    public interface IPrivilegeRepository : IRepository<Privilege>
    {
        #region Privilege Interfaces
        /// <summary>
        /// Returns a list of user system and phase privileges
        /// </summary>
        /// <param name="userId">user ID</param>
        /// <returns></returns>
        IList<UserPrivilege> GetUserPrivileges(int userId);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the security context
        /// </summary>
        ISecurityContext SecurityContext { get; }
        #endregion
	}
}
