﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Framework.Security.Ninject
{
	public class FakeSecurityModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IUnitOfWork>()
				.To<FakeUnitOfWork>();

			Bind<IRepository<User>>()
				.To<FakeUserRepository>();

			Bind<IRepository<SecurityToken>>()
				.To<FakeSecurityTokenRepository>();
		}
	}
}
