﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Framework.Security.Entities;
using Aquilent.Framework.Resource;

namespace Aquilent.Framework.Security
{
	[Serializable]
    [MetadataType(typeof(UserMetadata))]
	public partial class User
	{
		/// <summary>
		/// Retrieves the name of the user using "First Last" Format (e.g., Bill Gates)
		/// </summary>
		public string FullName
		{
			get
			{
				return string.Format("{0} {1}", FirstName, LastName);
			}
		}

		/// <summary>
		/// Retrieves the name of the user using "Last, First" Format (e.g., Gates, Bill)
		/// </summary>
		public string FullNameLastFirst
		{
			get
			{
				return string.Format("{0}, {1}", LastName, FirstName);
			}
		}

		/// <summary>
		/// Gets the TimeZoneInfo object associated with the user's TimeZone
		/// </summary>
		public TimeZoneInfo TimeZoneInfo
		{
			get
			{
				return (TimeZone != null ? LookupManager.GetLookup<LookupTimeZone>()[TimeZone] : null);
			}
		}

		/// <summary>
		/// Returns whether the user has a role that has a privilege with the supplied code
        /// 
        /// Note: This provides no context regarding the type of role
		/// </summary>
		/// <param name="privilegeCode">the privilege code to check for</param>
		/// <returns>true, if the user has the privilege, false otherwise</returns>
		public bool HasPrivilege(string privilegeCode)
		{
            bool result = false;

            result = HasSystemPrivilege(privilegeCode);

            if (!result)
            {
                result = UnitPrivileges.Any(x => x.Key == privilegeCode);
            }

            if (!result)
            {
                result = PhasePrivileges.Any(x => x.Key == privilegeCode);
            }
            
			return result;
		}

        /// <summary>
        /// Returns true if the user has a system role with the supplied privilege code
        /// </summary>
        /// <param name="privilegeCode"></param>
        /// <returns></returns>
        public bool HasSystemPrivilege(string privilegeCode)
        {
            return SystemPrivileges.Contains(privilegeCode);
        }

        /// <summary>
        /// Returns true if the user has a phase-based role with the supplied privilege code
        /// </summary>
        /// <param name="privilegeCode"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public bool HasPhasePrivilege(string privilegeCode, int phaseId)
        {
            bool result = false;

            // get a list of phase IDs with the privilege code
            var privileges = PhasePrivileges.Where(p => p.Key.Trim().Equals(privilegeCode)).FirstOrDefault();

            if (privileges.Key != null && privileges.Value != null)
            {
                // returns true if the phase ID is found
                result = privileges.Value.Contains(phaseId);
            }

            return result;
        }

        /// <summary>
        /// Returns true if the user has a phase-based role with the supplied privilege code
        /// </summary>
        /// <param name="privilegeCode"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public bool HasUnitPrivilege(string privilegeCode, string unitId)
        {
            bool result = false;

            int unitIdLength = unitId.Length;
            string unit2 = unitId.PadRight(8, '0');
            string unit4 = unitId.PadRight(8, '0');
            string unit6 = unitId.PadRight(8, '0');
            string unit8 = unitId.PadRight(8, '0'); // Ensure the unit is 8 digits
            
            // Forest Level
            if (unitIdLength == 8)
            {
                unit6 = unitId.Substring(0, 6).PadRight(8, '0');
            }

            // Region Level
            if (unitIdLength >= 6)
            {
                unit4 = unitId.Substring(0, 4).PadRight(8, '0');
            }

            // National Level
            if (unitIdLength >= 4)
            {
                unit2 = unitId.Substring(0, 2).PadRight(8, '0');
            }
            
            // get a list of phase IDs with the privilege code
            var privileges = UnitPrivileges.Where(p => p.Key.Trim().Equals(privilegeCode)).FirstOrDefault();

            if (privileges.Key != null && privileges.Value != null)
            {
                // returns true if the privilege is found in the unitId's hierarchy
                result = privileges.Value.Any(x => x == unit2 || x == unit4 || x == unit6 || x == unit8);
            }

            return result;
        }

		/// <summary>
		/// Returns the list of units that the user is assigned to
		/// </summary>
		public List<string> Units
		{
			get
			{
                List<string> units = null;
                if (UnitPrivileges != null)
                {
                    units = UnitPrivileges.SelectMany(x => x.Value).Distinct().ToList();
                }

                return units;
			}
		}

        /// <summary>
        /// Returns a comma separated list of unit names that the user is assigned to
        /// </summary>
        public string UnitNameList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (Units != null)
                {
                    foreach (string unit in Units)
                    {
                        sb.Append((!string.IsNullOrEmpty(sb.ToString()) ? ", " : string.Empty) + unit);
                    }
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// System privileges of the user (privilege codes)
        /// </summary>
        public HashSet<string> SystemPrivileges { get; set; }

        /// <summary>
        /// Phase privileges of the user (privilege codes and phase IDs)
        /// </summary>
        public IDictionary<string, IList<int>> PhasePrivileges { get; set; }

        /// <summary>
        /// Unit privileges of the user (privilege codes and Unit IDs)
        /// </summary>
        public IDictionary<string, IList<string>> UnitPrivileges { get; set; }
	}
}
