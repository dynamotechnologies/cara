﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Security
{
	public partial class UserSearchResult
    {
        #region Public Properties
        /// <summary>
		/// Retrieves the name of the user using "First Last" Format (e.g., Bill Gates)
		/// </summary>
		public string FullName
		{
			get
			{
				return string.Format("{0} {1}", FirstName, LastName);
			}
		}

		/// <summary>
		/// Retrieves the name of the user using "Last, First" Format (e.g., Gates, Bill)
		/// </summary>
		public string FullNameLastFirst
		{
			get
			{
				return string.Format("{0}, {1}", LastName, FirstName);
			}
		}

        /// <summary>
        /// Returns Checked string if the user has super user role
        /// </summary>
        public string IsSuperUserChecked
        {
            get
            {
                return (IsSuperUser > 0 ? "Checked" : string.Empty);
            }
        }

        /// <summary>
        /// Returns Checked string if the user has coordinator role
        /// </summary>
        public string IsCoordinatorChecked
        {
            get
            {
                return (IsCoordinator > 0 ? "Checked" : string.Empty);
            }
        }

        /// <summary>
        /// Returns Disabled string if the user does not have MSU privilege
        /// </summary>
        public string CanUpdateSuperUserDisabled
        {
            get
            {
                return (CanUpdateSuperUser > 0 ? string.Empty : "Disabled");
            }
        }

        /// <summary>
        /// Returns Disabled string if the user does not have MUC privilege
        /// </summary>
        public string CanUpdateCoordinatorDisabled
        {
            get
            {
                return (CanUpdateCoordinator > 0 ? string.Empty : "Disabled");
            }
        }

        public string RoleUnitList
        {
            get
            {
                return UnitNameText != null ? UnitNameText.Replace("|", "<br />") : string.Empty;
            }
        }
        #endregion
    }
}
