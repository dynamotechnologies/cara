﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Framework.Security.Entities;

namespace Aquilent.Framework.Security
{
	[Serializable]
	public partial class SecurityToken
	{
		public Nullable<DateTime> ExpirationDate {	get; internal set;	}

		/// <summary>
		/// Returns true if the security token is valid
		/// </summary>
		public bool IsValid
		{
			get
			{
				var valid = false;
				if (ExpirationDate.HasValue && DateTime.UtcNow < ExpirationDate.Value)
				{
					valid = true;
				}
				
				return valid;
			}
		}

		public override string ToString()
		{
			return Token.ToString();
		}

		//public SecurityToken Clone()
		//{
		//    return
		//        new SecurityToken
		//        {
		//            AuthenticationDate = this.AuthenticationDate,
		//            CreationDate = this.CreationDate,
		//            ExpirationDate = this.ExpirationDate,
		//            LogoutDate = this.LogoutDate,
		//            SecurityTokenID = this.SecurityTokenID,
		//            Token = this.Token,
		//            UserId = this.UserId,
		//            User = this.User
		//        };
		//}
	}
}
