﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Aquilent.Framework.Security.Entities
{
    /// <summary>
    /// Metadata for the user entity
    /// </summary>
    public class UserMetadata
    {
        #region Metadata Fields
        [Required, StringLength(100), DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(100), DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Lead Mgmt. Units")]
        public int UnitNameList { get; set; }

        [Required, StringLength(100)]
        [RegularExpression(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", ErrorMessage = "Email Not in Valid Format")]
        public string Email { get; set; }
        #endregion
    }
}
