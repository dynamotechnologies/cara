//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Framework.Security
{
    public partial class User
    {
        #region Primitive Properties
    
        public virtual int UserId
        {
            get;
            set;
        }
    
        public virtual string FirstName
        {
            get;
            set;
        }
    
        public virtual string LastName
        {
            get;
            set;
        }
    
        public virtual string Username
        {
            get;
            set;
        }
    
        public virtual bool NotifyAddedProject
        {
            get;
            set;
        }
    
        public virtual bool NotifySubmissionsReceived
        {
            get;
            set;
        }
    
        public virtual bool NotifyCommentPeriodOpen
        {
            get;
            set;
        }
    
        public virtual int SubmissionsRecv
        {
            get;
            set;
        }
    
        public virtual bool NotifyCommentPeriodEnd
        {
            get;
            set;
        }
    
        public virtual string TimeZone
        {
            get;
            set;
        }
    
        public virtual string Email
        {
            get;
            set;
        }
    
        public virtual int SearchResultsPerPage
        {
            get;
            set;
        }
    
        public virtual string Phone
        {
            get;
            set;
        }
    
        public virtual string Title
        {
            get;
            set;
        }
    
        public virtual int LetterFontSize
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<SecurityToken> SecurityTokens
        {
            get
            {
                if (_securityTokens == null)
                {
                    var newCollection = new FixupCollection<SecurityToken>();
                    newCollection.CollectionChanged += FixupSecurityTokens;
                    _securityTokens = newCollection;
                }
                return _securityTokens;
            }
            set
            {
                if (!ReferenceEquals(_securityTokens, value))
                {
                    var previousValue = _securityTokens as FixupCollection<SecurityToken>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupSecurityTokens;
                    }
                    _securityTokens = value;
                    var newValue = value as FixupCollection<SecurityToken>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupSecurityTokens;
                    }
                }
            }
        }
        private ICollection<SecurityToken> _securityTokens;
    
        public virtual ICollection<UserSystemRole> UserSystemRoles
        {
            get
            {
                if (_userSystemRoles == null)
                {
                    var newCollection = new FixupCollection<UserSystemRole>();
                    newCollection.CollectionChanged += FixupUserSystemRoles;
                    _userSystemRoles = newCollection;
                }
                return _userSystemRoles;
            }
            set
            {
                if (!ReferenceEquals(_userSystemRoles, value))
                {
                    var previousValue = _userSystemRoles as FixupCollection<UserSystemRole>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupUserSystemRoles;
                    }
                    _userSystemRoles = value;
                    var newValue = value as FixupCollection<UserSystemRole>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupUserSystemRoles;
                    }
                }
            }
        }
        private ICollection<UserSystemRole> _userSystemRoles;

        #endregion
        #region Association Fixup
    
        private void FixupSecurityTokens(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (SecurityToken item in e.NewItems)
                {
                    item.User = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (SecurityToken item in e.OldItems)
                {
                    if (ReferenceEquals(item.User, this))
                    {
                        item.User = null;
                    }
                }
            }
        }
    
        private void FixupUserSystemRoles(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (UserSystemRole item in e.NewItems)
                {
                    item.User = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (UserSystemRole item in e.OldItems)
                {
                    if (ReferenceEquals(item.User, this))
                    {
                        item.User = null;
                    }
                }
            }
        }

        #endregion
    }
}
