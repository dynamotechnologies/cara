﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Framework.Security;
using Aquilent.EntityAccess;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.IO;

namespace Aquilent.Cara.Domain.UnitTests
{
	[TestClass]
	public class DataMartTests
	{
		private DataMartManager _dmManager;
		private UserManager _userManager;
		private ProjectManager _projectManager;
		private IDataMart _datamart;

		[TestInitialize]
		public void Initialize()
		{
			_userManager = ManagerFactory.CreateInstance<UserManager>();
			_projectManager = ManagerFactory.CreateInstance<ProjectManager>();
			_dmManager = ManagerFactory.CreateInstance<DataMartManager>();

			_dmManager.UserManager = _userManager;
			_dmManager.ProjectManager = _projectManager;

			//ChannelFactory<IDataMart> factory =
			//  new ChannelFactory<IDataMart>("DataMartService");
			//_datamart = factory.CreateChannel();

			//_dmManager = new DataMartManager(_datamart, _userManager, _projectManager);
		}

		[TestCleanup]
		public void CleanUp()
		{
			//((IDisposable) _datamart).Dispose();
		}

		[TestMethod]
		public void Find_A_User_By_Username()
		{
			// Arrange:  Define the username to look for
			var username = "SuperUser1";

			// Act: Look for a user
			var user = _dmManager.GetUser(username);

			// Act: Verify that the user also exists in the usermanager
			var caraUser = _userManager.Get(username);

			// Assert: Make sure the correct user was returned
			Assert.AreEqual(username, user.Username);
			Assert.AreEqual(caraUser.Username, user.Username);
		}

		[TestMethod]
		public void Sync_A_User_With_Known_Changes()
		{
			// Arrange:  Define the username to look for
			var username = "SuperUser1";

			// Act: Look for a user
			var user = _dmManager.GetUser(username);

			// Act: Verify that the user also exists in the usermanager
			var caraUser = _userManager.Get(username);

			// Assert: Make sure the correct user was returned
			Assert.AreEqual(username, user.Username);
			Assert.AreEqual(caraUser.Username, user.Username);
		}

		//[TestMethod]
		public void Find_A_User_By_Username_CaseInsensitive()
		{
			// Arrange:  Define the username to look for
			var username = "SuperUser1";

			// Act: Look for a user
			var user = _dmManager.GetUser(username.ToLower());

			// Assert: Make sure the correct user was returned
			Assert.IsNotNull(user);
			Assert.AreEqual(username, user.Username);
		}

		[TestMethod]
		public void Find_User_Using_LastName_Fragment()
		{
			// Arrange:  Define the username to look for
			var lastnameFrag = "Use";

			// Act: Look for a user
			var userQueryable = _dmManager.FindUsers(null, lastnameFrag);
			int count = userQueryable.Count();
			int rightLastNameCount = userQueryable.Select(x => x.LastName.StartsWith(lastnameFrag)).Count();

			// Assert: Make sure all of the returned items match
			Assert.AreNotEqual(0, count);
			Assert.AreEqual(count, rightLastNameCount);
		}

		[TestMethod]
		public void Find_User_Using_FirstName_Fragment()
		{
			// Arrange:  Define the username to look for
			var firstnameFrag = "Super";

			// Act: Look for a user
			var userQueryable = _dmManager.FindUsers(firstnameFrag, null);
			int count = userQueryable.Count();
			int rightFirstNameCount = userQueryable.Select(x => x.FirstName.StartsWith(firstnameFrag)).Count();

			// Assert: Make sure all of the returned items match
			Assert.AreNotEqual(0, count);
			Assert.AreEqual(count, rightFirstNameCount);
		}

		[TestMethod]
		public void Find_Project_Using_Name_Fragment()
		{
			// Arrange:  Define the username to look for
			var nameFrag = "Lorem";
			var filter = new DataMartProjectFilter
			{
				PartialName = nameFrag
			};

			// Act: Look for a user
			var projectList = _dmManager.FindProjects(filter);
			int count = projectList.Count();
			int nameCount = projectList.Select(x => x.Name.StartsWith(nameFrag)).Count();

			// Assert: Make sure all of the returned items match
			Assert.AreEqual(2, count);
			Assert.AreEqual(count, nameCount);
		}

		[TestMethod]
		public void Get_Project_Using_ProjectID()
		{
			// Arrange:  Define the username to look for
			var projectId = 10001;

			// Act: Look for a user
			var project = _dmManager.GetProject(projectId);

			// Assert: Make sure all of the returned items match
			Assert.IsNotNull(project);
			Assert.AreEqual(projectId.ToString(), project.ProjectNumber);
		}

		[TestMethod]
		public void Get_User_From_Fake_Datamart_Service()
		{
			// Arrange: Set up the DataMartService proxy
			string username = "Reader2";

			ChannelFactory<IDataMart> factory =
			  new ChannelFactory<IDataMart>("DataMartService");

			// Act:  Retrieve the user
			DataMartUser reader;
			var dataMart = factory.CreateChannel();
			reader = dataMart.GetUser(username);
			((IDisposable) dataMart).Dispose();

			// Assert: Make sure the reader user matches
			Assert.IsNotNull(reader);
			Assert.AreEqual(username, reader.Username);
		}

		[TestMethod]
		public void Get_Project_From_Fake_Datamart_Service()
		{
			// Arrange: Set up the DataMartService proxy
			string projectId = "10001";

			ChannelFactory<IDataMart> factory =
			  new ChannelFactory<IDataMart>("DataMartService");

			// Act:  Retrieve the user
			DataMartProject project;
			var dataMart = factory.CreateChannel();
			project = dataMart.GetProject(projectId);
			((IDisposable) dataMart).Dispose();

			// Assert: Make sure the reader user matches
			Assert.IsNotNull(project);
			Assert.AreEqual(projectId, project.ProjectId);
		}

		[TestMethod]
		public void Find_Projects_In_Fake_Datamart_Service()
		{
			// Arrange: Set up the DataMartService proxy
			string unitcode = "110102";

			ChannelFactory<IDataMart> factory =
			  new ChannelFactory<IDataMart>("DataMartService");

			// Act:  Retrieve the user
			DataMartProjectList projects;
			var dataMart = factory.CreateChannel();
			projects = dataMart.FindProjects(null, null, null, unitcode);
			((IDisposable) dataMart).Dispose();

			// Assert: Make sure the reader user matches
			Assert.IsNotNull(projects);
			Assert.AreEqual(2, projects.Count);
		}

		[TestMethod]
		public void Temp_Call_Datamart_Service()
		{
			// Arrange: Set up the DataMartService proxy
			ChannelFactory<IDataMart> factory =
			  new ChannelFactory<IDataMart>("DataMartService");

			var phase = new DataMartLetterInfo { PhaseId = 32342, Name = "Formal - Test Phase" };

			// Act:  Retrieve the user
			var dataMart = factory.CreateChannel();
			//dataMart.LinkPhaseDocument("20032", 32342, docList);
			((IDisposable) dataMart).Dispose();

			// Assert: Make sure the reader user matches
			//Assert.IsNotNull(projects);
			//Assert.AreEqual(2, projects.Count);
		}

	}
}
