﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using Aquilent.Cara.Domain.Ingress;
using GemBox.Spreadsheet;
using Aquilent.Framework.Documents;
using HtmlAgilityPack;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain.UnitTests
{
	[TestClass]
	public class IngressTests
	{
		private ZipFile _fdmsZipFile;
		private ZipFile _lotusZipFile;
		private DocumentManager _documentManager;
		private OrganizationManager _organizationManager;

		[TestInitialize]
		public void Init()
		{
			SpreadsheetInfo.SetLicense("ETFV-FT6C-6R7H-ACFA");

			var fdmsFile = new FileInfo(@"c:\Users\jsutton\Documents\SVN\USDA-CARA\Source\Domain\Cara.Domain.UnitTests\Properties\APHIS-2008-0060_15-07-2010_09-31-21-931.zip");
			_fdmsZipFile = new ZipFile(fdmsFile.OpenRead());

			var lotusFile = new FileInfo(@"c:\Users\jsutton\Documents\SVN\USDA-CARA\Source\Domain\Cara.Domain.UnitTests\Properties\lotusexport.zip");
			_lotusZipFile = new ZipFile(lotusFile.OpenRead());

			_documentManager = ManagerFactory.CreateInstance<DocumentManager>();
			_organizationManager = ManagerFactory.CreateInstance<OrganizationManager>();
		}

		[TestMethod]
		public void Check_If_Fdms_File_Is_Valid()
		{
			// Arrange: set up the zip file 
			FdmsLetterExtractor extractor = new FdmsLetterExtractor("Username", _fdmsZipFile, _documentManager, _organizationManager);

			// Act:  Check if it is valid
			var isValid = extractor.IsValid;

			// Assert:  Ensure it is valid
			Assert.IsTrue(isValid);
		}

		[TestMethod]
		public void Check_If_Lotus_File_Is_Valid()
		{
			// Arrange: set up the zip file 
			LotusNotesLetterExtractor extractor = new LotusNotesLetterExtractor("Username", _lotusZipFile, _documentManager);

			// Act:  Check if it is valid
			var isValid = extractor.IsValid;

			// Assert:  Ensure it is valid
			Assert.IsTrue(isValid);
		}

		[TestMethod]
		public void Extract_Letters_And_Attachments_From_Fdms_Export_File()
		{
			// Arrange: set up the zip file 
			FdmsLetterExtractor extractor = new FdmsLetterExtractor("Username", _fdmsZipFile, _documentManager, _organizationManager);

			// Act:  Extract the letters
			var letters = extractor.Extract();

			// Assert:  The number of letters
			Assert.AreEqual(4, letters.Count);

			// Assert:  The number of documents (letters + attachments)
			Assert.AreEqual(7, letters.Sum(x => x.Documents.Count));
		}

		[TestMethod]
		public void Extract_Letters_And_Attachments_From_Lotus_Export_File()
		{
			// Arrange: set up the zip file 
			LotusNotesLetterExtractor extractor = new LotusNotesLetterExtractor("Username", _lotusZipFile, _documentManager);

			// Act:  Extract the letters
			var letters = extractor.Extract();

			// Assert:  The number of letters
			Assert.AreEqual(11, letters.Count);

			// Assert:  The number of documents (letters + attachments)
			Assert.AreEqual(13, letters.Sum(x => x.Documents.Count));
		}
	}
}
