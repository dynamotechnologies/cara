﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Framework
{
	public class FrameworkException : ApplicationException
	{
		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public FrameworkException()
			: base()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public FrameworkException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public FrameworkException(string message, Exception innerException) :
			base(message, innerException)
		{
		}
		#endregion        

		public override string StackTrace
		{
			get
			{
				string trace = string.Empty;

				// Get stacktrace from non-null innerexception
				if (InnerException != null)
				{
					trace += InnerException.StackTrace;
				}

				trace += base.StackTrace;

				return trace;
			}
		}
	}
}
