﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Objects;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Aquilent.EntityAccess
{
	/// <summary>
	/// Static class used to produce unit of work objects
	/// </summary>
	public static class EFUnitOfWorkFactory
	{
		/// <summary>
		/// Creates a new unit of work
		/// </summary>
		/// <typeparam name="T">The object context you want to initialize the unit of work with</typeparam>
		/// <param name="connectionStringKey">The name of the connection string in the configuration file.</param>
		/// <returns></returns>
		public static IUnitOfWork StartNew<T>() where T : ObjectContext
		{
			// Get connection string
			// TODO:  Use encrypted connection strings...use enterprise library API?
            //string connectionString = ConnectionStringManager.Instance.GetConnectionString(connectionStringKey);

			// Create the ObjectContext
			T objectContext = Activator.CreateInstance<T>();
			//objectContext.Connection.ConnectionString = connectionString;

			// Return the unit of work
			return new EFUnitOfWork(objectContext);
		}
	}
}
