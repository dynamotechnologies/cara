﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.EntityManager;
using System.Reflection;
using Ninject;
using log4net;

namespace Aquilent.EntityAccess
{
	public static class ManagerFactory
	{
        private static EntityManagerConfiguration _section = null;
        private static Dictionary<string, IKernel> _moduleElementDictionary = new Dictionary<string, IKernel>();

		/// <summary>
		/// Instantiates a Manager class using the specified NinjectModule specified via configuration.
		/// Since an AbstractEntityManager needs an IRepository and IUnitOfWork, the configuration specifies
		/// a NinjectModule for each configured manager that is used to instantiate the manager.
		/// </summary>
		/// <typeparam name="TMgr">Descends from AbstractEntityManager&lt;TEntity&gt;</typeparam>
		/// <typeparam name="TEntity">The type of entity being managed</typeparam>
		/// <returns>A manager</returns>
		public static TMgr CreateInstance<TMgr>() where TMgr : class
		{
            if (_section == null)
            {
                _section = ConfigUtilities.GetSection<EntityManagerConfiguration>("entityManagerConfiguration");

                if (_section == null)
                {
                    ILog logger = LogManager.GetLogger(typeof(ManagerFactory));
                    logger.Fatal("The configuration section, 'entityManagerConfiguration', could not be found");
                }
            }

			Type tmgr = typeof(TMgr);

            IKernel kernel;
            if (!_moduleElementDictionary.ContainsKey(tmgr.FullName))
            {

                var moduleElement = _section.Modules.Cast<ManagerClassElement>().Where(x => x.ClassName == tmgr.FullName).First();
                var module = moduleElement.UnwrapNinjectModule();
                kernel = new StandardKernel(module);

                _moduleElementDictionary.Add(tmgr.FullName, kernel);
            }
            else
            {
                kernel = _moduleElementDictionary[tmgr.FullName];
            }

			return kernel.Get<TMgr>();
		}
	}
}
