﻿using System;
using System.Data.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace Aquilent.EntityAccess
{
	/// <summary>
	/// Fake unit of work to use for unit testing
	/// </summary>
	public class FakeUnitOfWork : IUnitOfWork
	{
		#region IUnitOfWork members
		public ObjectContext Context
		{
			get	{ throw new NotImplementedException(); }
			set	{ throw new NotImplementedException(); }
		}

		public bool LazyLoadingEnabled { get; set; }
		public bool ProxyCreationEnabled { get; set; }
		public string ConnectionString { get; set; }

		public void Save()
		{
			// Do nothing
		}
		#endregion IUnitOfWork members

        public void Dispose()
        {
            // Do Nothing
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event UnitOfWorkEventHandler Saved;
    }
}
