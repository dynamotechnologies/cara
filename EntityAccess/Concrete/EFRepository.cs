﻿using System.Data.Objects;
using System.Linq;
using System.Collections.Generic;
using System.Transactions;
using System;
using System.ComponentModel;
using System.Reflection;

namespace Aquilent.EntityAccess
{
    /// <summary>
    /// Base class for a repository backed by the entity framework.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Properties
        private IUnitOfWork _unitOfWork = null;
        public virtual IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
            set
            {
                #region Copy event handlers to new unit of work
                // TODO:  Create utility method to handle this per event handler
                if (_unitOfWork != null)
                {
                    // Copy PropertyChanged Handlers
                    FieldInfo handler = typeof(EFUnitOfWork).GetField("PropertyChanged", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);
                    var sourceDelegate = handler.GetValue(_unitOfWork) as Delegate;

                    if (sourceDelegate != null)
                    {
                        foreach (var d in sourceDelegate.GetInvocationList())
                        {
                            var addDelegate = d as PropertyChangedEventHandler;
                            value.PropertyChanged += addDelegate;
                        }
                    }

                    // Copy Saved Handlers
                    handler = typeof(EFUnitOfWork).GetField("Saved", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);
                    sourceDelegate = handler.GetValue(_unitOfWork) as Delegate;

                    if (sourceDelegate != null)
                    {
                        foreach (var d in sourceDelegate.GetInvocationList())
                        {
                            var addDelegate = d as UnitOfWorkEventHandler;
                            value.Saved += addDelegate;
                        }
                    }
                }
                #endregion Copy event handlers to new unit of work

                _unitOfWork = value;
            }
        }

        public virtual AbstractEntityManager<TEntity> Manager { get; set; }

        private IObjectSet<TEntity> _objectset;
        public IObjectSet<TEntity> ObjectSet
        {
            get
            {
                if (_objectset == null && UnitOfWork is EFUnitOfWork)
                {
                    _objectset = UnitOfWork.Context.CreateObjectSet<TEntity>();
                }

                return _objectset;
            }
        }

        public virtual ObjectQuery<TEntity> ObjectQuery
        {
            get
            {
                return (ObjectQuery<TEntity>)ObjectSet;
            }
        }

        public virtual IQueryable<TEntity> All
        {
            get
            {
                return ObjectSet.AsQueryable();
            }
        }
        #endregion Properties

        #region Constructor
        public EFRepository(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            UnitOfWork.PropertyChanged += (s, e) =>
            {
                // Clear the object set so it can be recreated
                // if the context gets reassigned
                if (e.PropertyName == "Context")
                {
                    _objectset = null;
                }
            };
        }
        #endregion

        #region Methods
        public virtual void Add(TEntity entity)
        {
            ObjectSet.AddObject(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            ObjectSet.DeleteObject(entity);
        }
        #endregion Methods
    }
}
