﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.ComponentModel;
using System.Data.SqlClient;
using log4net;
using System.Configuration;

namespace Aquilent.EntityAccess
{
	/// <summary>
	/// Basis for a unit of work backed by the entity framework
	/// </summary>
	public class EFUnitOfWork : IUnitOfWork, INotifyPropertyChanged
	{
        private ObjectContext _objectContext;
        public ObjectContext Context
        {
            get { return _objectContext; }
            set
            {
                if (value != _objectContext)
                {
                    _objectContext = value;
                    OnPropertyChanged("Context");
                }
            }
        }

		public EFUnitOfWork(ObjectContext context)
		{
            _objectContext = context;
		}

		public void Save()
		{
            var e = new UnitOfWorkEventArgs();
            System.Data.EntityState entityState;
            if (Saved != null)
            {
                PopulateUnitOfWorkEventArgs(e, System.Data.EntityState.Added, "Added");
                PopulateUnitOfWorkEventArgs(e, System.Data.EntityState.Deleted, "Deleted");
                PopulateUnitOfWorkEventArgs(e, System.Data.EntityState.Modified, "Modified");
            }

            int maxRetryAttempts;
            if (!int.TryParse(ConfigurationManager.AppSettings["UnitOfWorkSaveMaxRetryAttempts"], out maxRetryAttempts))
            {
                maxRetryAttempts = 5; // Default
            }

            for (int i = 0; i < maxRetryAttempts; i++)
            {
                try
                {
                    // Save the changes
                    Context.SaveChanges();
                    OnSaved(e);
                    break;
                }
                catch (Exception exception)
                {
                    SqlException sqlException = exception as SqlException;
                    Exception inner = null;
                    if (sqlException == null)
                    {
                        inner = exception.InnerException;
                        while (inner != null && !(inner is SqlException))
                        {
                            inner = inner.InnerException;
                        }

                        sqlException = inner as SqlException;
                    }

                    if (sqlException != null && sqlException is SqlException)
                    {
                        // Retry on timeouts (-2) and deadlocks (1205)
                        if (sqlException.Number == -2 || sqlException.Number == 1205)
                        {
                            string sqlExceptionReason = sqlException.Number == -2 ? "Timeout" : "Deadlock";
                            string message = string.Format("{0}: Attempt {1} of {2} failed because of {3} ({4})",
                                GetType().Name,
                                i + 1,
                                maxRetryAttempts,
                                sqlExceptionReason,
                                sqlException.Number);

                            ILog log = LogManager.GetLogger(GetType());
                            log.Warn(message);

                            if (i + 1 >= maxRetryAttempts)
                            {
                                // The save attempt failed
                                throw new ApplicationException(
                                    string.Format("{2}: Reached maximum number of failed saved attempts due to {0} ({1})",
                                        sqlExceptionReason,
                                        sqlException.Number,
                                        GetType().Name),
                                    exception);
                            }
                        }
                        else
                        {
                            // The save attempt failed
                            throw new ApplicationException(
                                string.Format("{2}: Encountered SQL Exception: {0} ({1})",
                                    sqlException.Message,
                                    sqlException.Number,
                                    GetType().Name),
                                exception);
                        }
                    }
                    else
                    {
                        throw exception;
                    }
                }
            }
		}

        private void PopulateUnitOfWorkEventArgs(UnitOfWorkEventArgs e, System.Data.EntityState entityState, string key)
        {
            // Remember which entities were added prior to calling Context.SaveChanges()
            var entitiesInState = Context.ObjectStateManager.GetObjectStateEntries(entityState);
            IList<object> entitiesList = new List<object>(entitiesInState.Count());

            foreach (var entity in entitiesInState)
            {
                entitiesList.Add(entity.Entity);
            }

            e.Add(key, entitiesList);
        }

		public bool LazyLoadingEnabled
		{
			get { return Context.ContextOptions.LazyLoadingEnabled; }
			set { Context.ContextOptions.LazyLoadingEnabled = value; }
		}

		public bool ProxyCreationEnabled
		{
			get { return Context.ContextOptions.ProxyCreationEnabled; }
			set { Context.ContextOptions.ProxyCreationEnabled = value; }
		}

		public string ConnectionString
		{
			get { return Context.Connection.ConnectionString; }
			set { Context.Connection.ConnectionString = value; }
		}

        public void Dispose()
        {
            Context.Dispose();
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected void OnSaved(UnitOfWorkEventArgs e)
        {
            UnitOfWorkEventHandler handler = Saved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event UnitOfWorkEventHandler Saved;
    }
}