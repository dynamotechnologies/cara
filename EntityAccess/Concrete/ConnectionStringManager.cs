﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Aquilent.EntityAccess
{
    /// <summary>
    /// Class to store and provide connection strings
    /// </summary>
    public class ConnectionStringManager
    {
        #region Class Members
        private static ConnectionStringManager service = null;
        private Dictionary<string, string> connectionStrings = new Dictionary<string, string>();
        private FileSystemWatcher fileWatcher = new FileSystemWatcher();
		private ILog logger = LogManager.GetLogger(typeof(ConnectionStringManager));
        #endregion

        #region Constructor
        /// <summary>
        /// Private constructor
        /// </summary>
        private ConnectionStringManager()
        {
            InitFileWatcher();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Get instance property
        /// </summary>
        public static ConnectionStringManager Instance
        {
            get
            {
                if (service == null)
                {
                    service = new ConnectionStringManager();
                }

                return service;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to get the connection string from the config file
        /// </summary>
        /// <param name="connectionStringKey"></param>
        /// <returns></returns>
        public string GetConnectionString(string connectionStringKey)
        {
			try
			{
				// add the connection string to the collection if not assigned
				if (!connectionStrings.ContainsKey(connectionStringKey) && !string.IsNullOrEmpty(connectionStringKey))
				{
					FileConfigurationSource configFile = new FileConfigurationSource(ConfigurationManager.AppSettings["connectionStringFile"]);
					DatabaseProviderFactory dbFactory = new DatabaseProviderFactory(configFile);

					connectionStrings.Add(connectionStringKey, dbFactory.Create(connectionStringKey).ConnectionString);
				}
			}
			catch (Exception ex)
			{
				string msg = string.Format("An error occurred retrieving the connection string with key, {0}", connectionStringKey);
				logger.FatalFormat(msg);
				throw new ApplicationException(msg, ex);
			}

            return connectionStrings[connectionStringKey];
        }
        #endregion

        #region Event Handling Methods
        /// <summary>
        /// Initialize the file watcher
        /// </summary>
        private void InitFileWatcher()
        {
            // get the config path
            string filePath = ConfigurationManager.AppSettings["connectionStringFile"];

            if (!string.IsNullOrEmpty(filePath))
            {
                // get the folder path
                fileWatcher.Path = filePath.Substring(0, filePath.LastIndexOf(@"\"));
                // get the filter (file name)
                fileWatcher.Filter = filePath.Substring(filePath.LastIndexOf(@"\") + 1);
                // add change event handler
                fileWatcher.Changed += new FileSystemEventHandler(fileWatcher_Changed);
                // enable raising the events
                fileWatcher.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// Clear the connection strings if the config file is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fileWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            connectionStrings.Clear();
        }
        #endregion
    }
}

