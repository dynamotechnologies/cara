﻿using System.Data.Objects;
using System;
using System.ComponentModel;

namespace Aquilent.EntityAccess
{
	/// <summary>
	/// Represents a unit of work and is the basis of the Unit of Work pattern
	/// </summary>
	public interface IUnitOfWork : IDisposable
	{
		/// <summary>
		/// The context used by the unit of work to store insert, delete, updates
		/// </summary>
		ObjectContext Context { get; set; }

		/// <summary>
		/// To get/set the lazy loading enabled property of the context
		/// </summary>
		bool LazyLoadingEnabled { get; set; }

		/// <summary>
		/// To get/set the proxy creation enabled property of the context
		/// </summary>
		bool ProxyCreationEnabled { get; set; }

		/// <summary>
		/// To get/set the connection string of the context
		/// </summary>
		string ConnectionString { get; set; }

		/// <summary>
		/// Saves any changes registered with the unit of work
		/// </summary>
		void Save();

        /// <summary>
        /// Allows other objects to subscribe to property changes
        /// </summary>
        event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Allows other objects to be notified when Save is called
        /// </summary>
        event UnitOfWorkEventHandler Saved;
	}

    public delegate void UnitOfWorkEventHandler(object sender, UnitOfWorkEventArgs e);
}