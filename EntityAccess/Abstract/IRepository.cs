﻿using System.Linq;
using System.Data.Objects;

namespace Aquilent.EntityAccess
{
	/// <summary>
	/// Defines an interface to for the repository to be used in the Unit of Work pattern
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface IRepository<TEntity> where TEntity : class
	{
		AbstractEntityManager<TEntity> Manager { get; set; }

		/// <summary>
		/// The repository's unit of work object
		/// </summary>
		IUnitOfWork UnitOfWork { get; set; }

		/// <summary>
		/// The objectset managed by the repository
		/// </summary>
		IObjectSet<TEntity> ObjectSet { get; }

		/// <summary>
		/// The objectquery managed by the repository
		/// </summary>
		ObjectQuery<TEntity> ObjectQuery { get; }

		/// <summary>
		/// Returns the entire entity collection for further querying
		/// </summary>
		IQueryable<TEntity> All { get; }

		/// <summary>
		/// Adds an entity to the repository
		/// </summary>
		/// <param name="entity"></param>
		void Add(TEntity entity);

		/// <summary>
		/// Deletes an entity from the repository
		/// </summary>
		/// <param name="entity"></param>
		void Delete(TEntity entity);
	}
}
