﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Transactions;
using System.Data.Objects;
using System.Configuration;

namespace Aquilent.EntityAccess
{
	/// <summary>
	/// Defines an abstract class that implements all of the functions that a
	/// the business layer would need to interface with the repository.
	/// </summary>
	/// <typeparam name="TEntity">The entity that the service operates on.</typeparam>
	public abstract class AbstractEntityManager<TEntity> : IDisposable where TEntity : class 
	{
		#region Properties
		protected IRepository<TEntity> Repository { get; set; }

		public IUnitOfWork UnitOfWork
		{
			get
			{
				return Repository.UnitOfWork;
			}

			set
			{
                Repository.UnitOfWork.Dispose();  // Dispose of the current UnitOfWork first
				Repository.UnitOfWork = value;
			}
		}
		#endregion Properties

		#region Constructor
		public AbstractEntityManager(IRepository<TEntity> repository)
		{
			Repository = repository;
		}
		#endregion Constructor

		#region Methods
		/// <summary>
		/// Returns the entire repository for further querying
		/// </summary>
		public virtual IQueryable<TEntity> All
		{
			get
			{
				return Repository.All;
			}
		}

		/// <summary>
		/// Adds a new entity to the repository
		/// </summary>
		/// <param name="entity"></param>
		public virtual void Add(TEntity entity)
		{
			Repository.Add(entity);
		}

		/// <summary>
		/// Removes an entity from the repository
		/// </summary>
		/// <param name="entity"></param>
		public virtual void Delete(TEntity entity)
		{
			Repository.Delete(entity);
		}

		/// <summary>
		/// Retrieves a collection of entities that matches the collection
		/// for further querying
		/// </summary>
		/// <param name="predicate">An expression that defines the search criteria.</param>
		/// <returns></returns>
		public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
		{
			return All.Where(predicate.Compile()).AsQueryable();
		}

        /// <summary>
        /// Performs bulk loading of entities more efficiently by
        /// wrapping the operation in a transaction and periodically
        /// saving changes rather than waiting until after all of the
        /// entities are added to the context.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="bulkLoadBatchSize"></param>
        public virtual void BulkLoad(IList<TEntity> entities, int bulkLoadBatchSize = 100)
        {
            var context = UnitOfWork.Context;
            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Suppress, new TimeSpan(0, 10, 0)))
            {
                for (int i = 0; i < entities.Count; i++)
                {
                    var entityToInsert = entities[i];
                    context = AddToContext(context, entityToInsert, i + 1, bulkLoadBatchSize, true);
                }

                UnitOfWork.Save();

                transaction.Complete();
            }
        }

        private ObjectContext AddToContext(ObjectContext context, TEntity entity, int count, int commitCount, bool recreateContext)
        {
            Add(entity);

            if (count % commitCount == 0)
            {
                UnitOfWork.Save();

                if (recreateContext)
                {
                    context.Dispose();
                    context = Activator.CreateInstance(context.GetType()) as ObjectContext;
                    UnitOfWork.Context = context;
                }
            }

            return context;
        }
		#endregion Methods

		#region Abstract Methods
		/// <summary>
		/// Retrieves an object of type TEntity with a unique ID
		/// </summary>
		/// <param name="uniqueId"></param>
		/// <returns>An object of type TEntity with the given uniqueId</returns>
		public abstract TEntity Get(int uniqueId);

		/// <summary>
		/// Retrieves an object of type TEntity with a unique name string
		/// </summary>
		/// <param name="uniqueName"></param>
		/// <returns>An object of type TEntity with the given uniqueId</returns>
		public abstract TEntity Get(string uniqueName);
		#endregion Abstract Methods

        #region IDisposable interface
        public void Dispose()
        {
            UnitOfWork.Dispose();
        }
        #endregion IDisposable interface
    }
}
