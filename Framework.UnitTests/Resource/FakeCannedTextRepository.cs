﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Framework.UnitTests.Resource
{
	public class FakeCannedTextRepository : IRepository<CannedText>
	{
		private IList<CannedText> _cannedTexts = new List<CannedText>();

		public FakeCannedTextRepository(IUnitOfWork unitOfWork)
		{
			CannedText[] temp = new CannedText[]
			{
				new CannedText {
					CannedTextId = 1,
					Name = "test.template",
					TokenStart = "{{",
					TokenEnd = "}}",
					Value = "One token: {{hello}}, and one embedded template: {{include:test.embedded.template}}."
				},
				new CannedText {
					CannedTextId = 2,
					Name = "test.embedded.template",
					TokenStart = "{{",
					TokenEnd = "}}",
					Value = "Another embedded template: {{include:test.embedded.template.2}}."
				},
				new CannedText {
					CannedTextId = 3,
					Name = "test.embedded.template.2",
					TokenStart = "{{",
					TokenEnd = "}}",
					Value = "Just replacing one token: {{world}}."
				},
				new CannedText {
					CannedTextId = 4,
					Name = "test.template.regex.sp.tokens",
					TokenStart = "[[",
					TokenEnd = "]]",
					Value = "Just replacing one token: [[world]]."
				},
				new CannedText {
					CannedTextId = 5,
					Name = "test.template.regex.sp.tokens.2",
					TokenStart = "[[",
					TokenEnd = "]]",
					Value = "Just replacing one token: [[world]] and one template [[include:test.template.regex.sp.tokens]]."
				},

			};

			_cannedTexts = temp.ToList();

			UnitOfWork = unitOfWork;
		}


		#region IRepository members
		public AbstractEntityManager<CannedText> Manager { get; set; }
		public IUnitOfWork UnitOfWork {	get; set; }
		public System.Data.Objects.IObjectSet<CannedText> ObjectSet
		{
			get { throw new NotImplementedException(); }
		}

		public System.Data.Objects.ObjectQuery<CannedText> ObjectQuery
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<CannedText> All
		{
			get
			{
				return _cannedTexts.AsQueryable();
			}
		}

		public void Add(CannedText entity)
		{
			_cannedTexts.Add(entity);
		}

		public void Delete(CannedText entity)
		{
			_cannedTexts.Remove(entity);
		}
		#endregion IRepository members
	}
}
