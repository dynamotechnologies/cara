﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject.Modules;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Ninject;

namespace Aquilent.Framework.UnitTests.Resource
{
	[TestClass]
	public class ResourceTests
	{
		private CannedTextManager _manager;

		[TestInitialize]
		public void TestInit()
		{
			IKernel kernel = new StandardKernel(new ResourceTestModule());
			_manager = kernel.Get<CannedTextManager>();
		}

		[TestMethod]
		public void Verify_Token_Replacement()
		{
			// Arrange:
			var templateName = "test.template";

			// Act:  Do the replacement
			var replacedText = _manager.Resolve(templateName);

			// Assert:
			string expectedText = "One token: |Replaced hello token|, and one embedded template: Another embedded template: Just replacing one token: |Replaced world token|...";
			Assert.AreEqual(expectedText, replacedText);
		}

		[TestMethod]
		public void Verify_Token_Replacement_With_Supporting_Info()
		{
			// Arrange:  Get the canned text and add supporting info
			var templateName = "test.template";
			var cannedText = _manager.Get(templateName);
			cannedText.SupportingInfo.Add("hello", "|Replaced hello token with supporting info|");

			// Act:  Do the replacement
			var replacedText = cannedText.Resolve();

			// Assert:
			string expectedText = "One token: |Replaced hello token with supporting info|, and one embedded template: Another embedded template: Just replacing one token: |Replaced world token|...";
			Assert.AreEqual(expectedText, replacedText);
		}

		[TestMethod]
		public void Verify_Token_Replacement_When_Tokens_Contain_Regex_Special_Characters()
		{
			// Arrange:
			var templateName = "test.template.regex.sp.tokens";
			var cannedText = _manager.Get(templateName);
			Assert.AreEqual("[[", cannedText.TokenStart);

			// Act: 
			var replacedText = cannedText.Resolve();

			// Assert:
			string expectedText = "Just replacing one token: |Replaced world token|.";
			Assert.AreEqual(expectedText, replacedText);
		}

		[TestMethod]
		public void Verify_Token_Replacement_When_Tokens_Contain_Regex_Special_Characters_And_Has_An_Embedded_CannedText()
		{
			// Arrange:
			var templateName = "test.template.regex.sp.tokens.2";
			var cannedText = _manager.Get(templateName);
			Assert.AreEqual("[[", cannedText.TokenStart);

			// Act: 
			var replacedText = cannedText.Resolve();

			// Assert:
			string expectedText = "Just replacing one token: |Replaced world token| and one template Just replacing one token: |Replaced world token|..";
			Assert.AreEqual(expectedText, replacedText);
		}

		/// <summary>
		/// Dependency Injection Setup via Ninject
		/// </summary>
		private class ResourceTestModule : NinjectModule
		{
			public override void Load()
			{
				Bind<IRepository<CannedText>>()
					.To<FakeCannedTextRepository>();
				Bind<IUnitOfWork>()
					.To<FakeUnitOfWork>();
			}
		}
	}
}
