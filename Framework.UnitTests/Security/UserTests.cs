﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Ninject;
using Ninject.Modules;

namespace Aquilent.Framework.UnitTests.Security
{
	[TestClass]
	public class UserTests
	{
		private IUnitOfWork _unitOfWork;
		private IUserRepository _repository;
		private UserManager _manager;

		[TestInitialize]
		public void TestInit()
		{
			IKernel kernel = new StandardKernel(new SecurityTestModule());
			_unitOfWork = kernel.Get<IUnitOfWork>();
			_repository = kernel.Get<IUserRepository>();
			_manager = new UserManager(_repository);
		}

		[TestMethod]
		public void Verify_User_Full_Name_Property()
		{
			// Arrange:  Create a new user
			User user = new User
			{
				FirstName = "Jed",
				LastName = "Sutton"
			};

			// Act: Get the full name property
			string fullName = user.FullName;

			// Assert: Check that the full name is what was expected
			Assert.AreEqual("Jed Sutton", fullName);
		}

		[TestMethod]
		public void Verify_User_Full_Name_Last_First_Property()
		{
			// Arrange:  Create a new user
			User user = new User
			{
				FirstName = "Jed",
				LastName = "Sutton"
			};

			// Act: Get the full name property
			string fullName = user.FullNameLastFirst;

			// Assert: Check that the full name is what was expected
			Assert.AreEqual("Sutton, Jed", fullName);
		}

		[TestMethod]
		public void Verify_Results_Of_Get_By_UserId()
		{
			// Arrange:  Set up user repository and unit of work

			// Act:  Get a user
			User user = _manager.Get(1);

			// Assert: Check that we got the user we were looking for
			Assert.IsNotNull(user);
			Assert.AreEqual(user.UserId, 1);
		}

		[TestMethod]
		public void Verify_Results_Of_Get_By_Username()
		{
			// Arrange:  Set up user repository and unit of work
			string username = "bbaggins";

			// Act:  Get a user
			User user = _manager.Get(username);

			// Assert: Check that we got the user we were looking for
			Assert.IsNotNull(user);
			Assert.AreEqual(user.Username, username);
		}

		//[TestMethod]
		//public void Verify_Connection_To_Database()
		//{
		//    var unitOfWork = EFUnitOfWorkFactory.StartNew<SecurityModel>("SecurityModel");

		//    Assert.IsNotNull(unitOfWork);
		//}

		[TestMethod]
		public void Verify_Adding_User_To_The_Repository()
		{
			// Arrange:  Create a new user to add
			var user = new User
			{
				Username = "bsimpson",
				FirstName = "Bart",
				LastName = "Simpson",
				Email = "jed.sutton@aquilent.com",
			};

			// Act:  Add the new user
			_manager.Add(user);
			_unitOfWork.Save();

			// Assert:
			User theUserJustAdded = _manager.Get("bsimpson");
			Assert.AreEqual(user.Username, theUserJustAdded.Username);
		}

		[TestMethod]
		public void Verify_Modifying_User_To_The_Repository()
		{
			// Arrange: set up the service
			string email = "bgates@microsoft.com";

			// Act:  Update a user
			User user = _manager.Get(2);
			user.Email = email;
			_unitOfWork.Save();

			// Assert: Verify that the email address is the same as we set.
			user = _manager.Get(2);
			Assert.AreEqual(email, user.Email);
		}

		[TestMethod]
		public void Retrieve_Lots_Of_Users_Via_UserIds()
		{
			// Arrange: Define the user IDs to retrieve
			var userIds = new int[] { 2, 3 };

			// Act: get the users
			IList<User> users = _manager.Get(userIds);

			// Assert: verify the results
			Assert.AreEqual(users.Count, 2);
			Assert.AreEqual(users[0].UserId, 2);
			Assert.AreEqual(users[1].UserId, 3);
		}

		[TestMethod]
		public void Instantiate_UserManager_From_Configuration()
		{
			// Arrange:  nothing to do

			// Act: get the usermanager
			var manager = ManagerFactory.CreateInstance<UserManager>();

			// Assert: make sure the manager is not null
			Assert.IsNotNull(manager);
		}

		/// <summary>
		/// Dependency Injection Setup via Ninject
		/// </summary>
		private class SecurityTestModule : NinjectModule
		{
			public override void Load()
			{
				Bind<IRepository<User>>()
					.To<FakeUserRepository>();
				Bind<IUnitOfWork>()
					.To<FakeUnitOfWork>();
			}
		}
	}
}
