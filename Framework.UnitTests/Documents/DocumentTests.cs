﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject.Modules;
using Aquilent.Framework.Documents;
using Aquilent.EntityAccess;
using Ninject;
using System.Configuration;

namespace Aquilent.Framework.UnitTests.Documents
{
	[TestClass]
	public class DocumentTests
	{
		private DocumentManager _manager;
		private IFileStore _fileStore;

		[TestInitialize]
		public void TestInit()
		{
			IKernel kernel = new StandardKernel(new DocumentsTestModule());
			_manager = kernel.Get<DocumentManager>();
			_fileStore = kernel.Get<IFileStore>();
		}

		[TestMethod]
		public void Add_Document_And_File()
		{
			// Arrange: set up a document and file
			var document = new Document();
			document.Name = "Test File";
			int documentIdBefore = document.DocumentId;

			var file = new File(@"..\..\..\..\Framework.UnitTests\Documents\Files\Uploads\quickbrown.txt", "text/plain");
			file.UploadedBy = "jsutton";
			file.Uploaded = DateTime.UtcNow;
			document.File = file;

			// Act: add the document and file to the repository
			_manager.Add(document);
			_manager.UnitOfWork.Save();
			
			int documentIdAfter = document.DocumentId;
			int fileId = document.FileId.GetValueOrDefault(-1);

			// Assert: Check the documentId
			Assert.AreNotEqual(documentIdBefore, documentIdAfter);
			Assert.AreNotEqual(-1, fileId);
		}

		[TestMethod]
		public void Retrieve_Document_And_File()
		{
			// Arrange:  Set up a file to stream the requested file to
			var downloadFileTo = @"..\..\..\..\Framework.UnitTests\Documents\Files\Downloads\test.txt";
			System.IO.FileInfo fi = new System.IO.FileInfo(downloadFileTo);
			System.IO.Stream outputStream = fi.OpenWrite();

			// Arrange:  Get the document
			var document = _manager.Get(1);

			// Act: Download the file
			_manager.DownloadFile(1, outputStream);
			outputStream.Close();
			fi.Refresh();

			// Assert: that the file exists and the sizes matches
			Assert.IsTrue(fi.Exists);
			Assert.AreEqual(document.File.Size, Convert.ToInt32(fi.Length));

			// After:  Delete the downloaded file
			System.IO.File.Delete(downloadFileTo);
		}

		[TestMethod]
		public void Retrieve_Document_And_File_In_A_Zip_File()
		{
			// Arrange:  Set up a file to stream the requested file to
			var downloadFileTo = @"..\..\..\..\Framework.UnitTests\Documents\Files\Downloads\test.zip";
			System.IO.FileInfo fi = new System.IO.FileInfo(downloadFileTo);
			System.IO.Stream outputStream = fi.OpenWrite();

			// Act:  Request the zipped document
			_manager.ZipDocuments(new int[] { 1 }, outputStream);
			outputStream.Close();
			fi.Refresh();

			// Assert:  Ensure that the file exists
			Assert.IsTrue(fi.Exists);

			// After:  Delete the downloaded file
			System.IO.File.Delete(downloadFileTo);
		}

		[TestMethod]
		public void Delete_Document_And_File()
		{
			// Arrange: set up a document and file
			var document = new Document();
			document.Name = "Test File";
			int documentIdBefore = document.DocumentId;

			var file = new File(@"..\..\..\..\Framework.UnitTests\Documents\Files\Uploads\quickbrown.txt", "text/plain");
			file.UploadedBy = "jsutton";
			file.Uploaded = DateTime.UtcNow;
			document.File = file;
			_manager.Add(document);
			_manager.UnitOfWork.Save();
			var documentId = document.DocumentId;
			var filePath = document.File.StoragePath;

			// Act: delete the document
			_manager.Delete(document);
			_manager.UnitOfWork.Save();

			// Act: See if the document still exists
			var documentShouldBeNull = _manager.Get(documentId);

			// Act: get the fileinfo of the uploaded document
			System.IO.FileInfo fi = new System.IO.FileInfo(((FileSystemStore) _fileStore).GetAbsoluteStoragePath(filePath));

			// Assert:  Make sure the document record no longer exists
			Assert.IsNull(documentShouldBeNull);
			Assert.IsFalse(fi.Exists);
		}

		[TestMethod]
		[ExpectedException(typeof(DocumentException))]
		public void Cannot_Add_File_With_Invalid_Extension()
		{
			// Arrange: set up a document and file
			var document = new Document();
			document.Name = "Test File";
			int documentIdBefore = document.DocumentId;

			var file = new File(@"..\..\..\..\Framework.UnitTests\Documents\Files\Uploads\quickbrown.xyz", "text/plain");
			file.UploadedBy = "jsutton";
			file.Uploaded = DateTime.UtcNow;
			document.File = file;

			// Act and Assert:  This should throw the expected exception
			_manager.Add(document);
		}

		[TestMethod]
		public void Replace_Invalid_Characters_In_Filename()
		{
			// Arrange: Find the file
			var di = new System.IO.DirectoryInfo(@"..\..\..\..\Framework.UnitTests\Documents\Files\Uploads");
			var fi = di.EnumerateFiles().Where(x => x.Name.StartsWith("FileWith")).First();

			// Act:  Add the document, which should filter out the invalid characters
			var newName = _fileStore.RemoveInvalidFileNameCharacters(fi.Name);

			// Assert: the names are different
			Assert.AreNotEqual(fi.Name, newName);
		}

		[TestMethod]
		public void Validate_That_Duplicate_Files_Are_Being_Reused()
		{
			// Arrange: set up a document and file
			var document1 = new Document();
			document1.Name = "Test File";

			var file1 = new File(@"..\..\..\..\Framework.UnitTests\Documents\Files\Uploads\quickbrown.txt", "text/plain");
			file1.UploadedBy = "jsutton";
			file1.Uploaded = DateTime.UtcNow;
			document1.File = file1;
			_manager.Add(document1);
			_manager.UnitOfWork.Save();

			// Arrange: add a new document referencing the same file
			var document2 = new Document();
			document2.Name = "Test File 2";

			var file2 = new File(@"..\..\..\..\Framework.UnitTests\Documents\Files\Uploads\quickbrown.txt", "text/plain");
			file2.UploadedBy = "jsutton";
			file2.Uploaded = DateTime.UtcNow;
			document2.File = file2;
			_manager.Add(document2);
			_manager.UnitOfWork.Save();

			// Assert:  Make sure the document record no longer exists
			Assert.AreEqual(document1.FileId, document2.FileId);

			// After:  Cleanup
			_manager.Delete(document1);
			_manager.Delete(document2);
			_manager.UnitOfWork.Save();
		}

		/// <summary>
		/// Dependency Injection Setup via Ninject
		/// </summary>
		private class DocumentsTestModule : NinjectModule
		{
			public override void Load()
			{
				Bind<IRepository<Document>>()
					.To<DocumentRepository>();

				Bind<IUnitOfWork>()
					.To<EFUnitOfWork>()
					.WithConstructorArgument("context", new Aquilent.Framework.Documents.CaraDocumentsEntities());

				Bind<IFileStore>()
					.To<FileSystemStore>()
					.WithConstructorArgument("configSectionName", "fileStoreConfiguration");
			}
		}
	}
}
