﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;
using Aquilent.Cara.Reference;

namespace Configuration.UnitTests
{
	public class AnalysisTypeLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			var list = new List<AnalysisType>();

			list.Add(new AnalysisType { AnalysisTypeId = 1, Name = "Mississippi" });
			list.Add(new AnalysisType { AnalysisTypeId = 2, Name = "Two" });
			list.Add(new AnalysisType { AnalysisTypeId = 3, Name = "Three" });

			return list.AsQueryable();
		}
	}
}
