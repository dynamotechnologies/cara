﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Configuration.UnitTests
{
	[TestClass]
	public class LookupManagerConfigurationTests
	{
		[TestMethod]
		public void Get_YesNoLookup_From_LookupManager()
		{
			// Arrange, Act
			var yesno = LookupManager.GetLookup<LookupYesNo>();

			// Assert
			Assert.IsNotNull(yesno);
		}

		[TestMethod]
		public void Verify_Yes_Value_In_YesNoLookup()
		{
			// Arrange
			var yesno = LookupManager.GetLookup<LookupYesNo>();

			// Act
			string value = yesno["Yes"];

			// Assert
			Assert.AreEqual("Yes", value);
		}

		[TestMethod]
		public void Verify_Number_Of_Items_In_YesNoLookup()
		{
			// Arrange
			var yesno = LookupManager.GetLookup<LookupYesNo>();

			// Act
			int count = yesno.SelectionList.Count;

			// Assert
			Assert.AreEqual(2, count);
		}

		[TestMethod]
		public void Verify_Number_Of_Items_In_AnalysisTypeLookup()
		{
			// Arrange
			var lookup = LookupManager.GetLookup<LookupAnalysisType>();

			// Act
			int count = lookup.SelectionList.Count;

			// Assert
			Assert.AreEqual(3, count);
		}

		[TestMethod]
		public void Verify_The_First_Item_In_AnalysisTypeLookup()
		{
			// Arrange
			var lookup = LookupManager.GetLookup<LookupAnalysisType>();
			var first = new AnalysisType { AnalysisTypeId = 1, Name = "Mississippi" };

			// Act
			var fromLookup = lookup[first.AnalysisTypeId];

			// Assert
			Assert.AreEqual(first.AnalysisTypeId, fromLookup.AnalysisTypeId);
			Assert.AreEqual(first.Name, fromLookup.Name);
		}
	}
}
