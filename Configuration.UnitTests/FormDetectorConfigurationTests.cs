﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aquilent.Cara.Configuration.FormDetection;
using Aquilent.Cara.Configuration;

namespace Configuration.UnitTests
{
	[TestClass]
	public class FormDetectorConfigurationTests
	{
		[TestMethod]
		public void Load_Form_Detection_Configuration_Section()
		{
			// Arrange: Taken care of in TestInitialize
			string section = "formDetectionConfiguration";

			// Act:  Load the config file
			FormDetectionConfiguration fsc = ConfigUtilities.GetSection<FormDetectionConfiguration>(section);

			// Assert:  Check to see if the config file loaded
			Assert.IsNotNull(fsc);
		}

		[TestMethod]
		public void Confirm_Form_Detection_Configuration_Settings()
		{
			// Arrange: Taken care of in TestInitialize
			string section = "formDetectionConfiguration";
			FormDetectionConfiguration fsc = ConfigUtilities.GetSection<FormDetectionConfiguration>(section);

			// Act:  Check some values
			var numLetterSizes = fsc.LetterSizes.Length;

			// Assert:  Check to see if the config file loaded
			Assert.AreEqual(2, numLetterSizes);
		}

	}
}
