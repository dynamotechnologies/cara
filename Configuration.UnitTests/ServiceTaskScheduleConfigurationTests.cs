﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Configuration.FileStore;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Aquilent.Cara.Configuration;
using Aquilent.Framework.Configuration.ServiceTasks;

namespace Configuration.UnitTests
{
	[TestClass]
	public class ServiceTaskScheduleConfigurationTests
	{
		[TestMethod]
		public void Load_File_Store_Configuration_Section()
		{
			// Arrange: Taken care of in TestInitialize
            string section = "serviceTasksConfiguration";

			// Act:  Load the config file
            ServiceTaskSchedulerConfiguration fsc = ConfigUtilities.GetSection<ServiceTaskSchedulerConfiguration>(section);

			// Assert:  Check to see if the config file loaded
			Assert.IsNotNull(fsc);
		}
	}
}
