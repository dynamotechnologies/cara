﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Configuration.FileStore;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.WindowsService;

namespace Configuration.UnitTests
{
	[TestClass]
	public class FileStoreConfigurationTests
	{
		[TestMethod]
		public void Load_File_Store_Configuration_Section()
		{
			// Arrange: Taken care of in TestInitialize
			string section = "fileStoreConfiguration";

			// Act:  Load the config file
			FileStoreConfiguration fsc = ConfigUtilities.GetSection<FileStoreConfiguration>(section);

			// Assert:  Check to see if the config file loaded
			Assert.IsNotNull(fsc);
		}

		[TestMethod]
		public void Load_Service_Configuration_Section()
		{
			// Arrange: Taken care of in TestInitialize
			string section = "windowsServiceConfiguration";

			// Act:  Load the config file
			WindowsServiceConfiguration fsc = ConfigUtilities.GetSection<WindowsServiceConfiguration>(section);

			// Assert:  Check to see if the config file loaded
			Assert.IsNotNull(fsc);
		}
	}
}
