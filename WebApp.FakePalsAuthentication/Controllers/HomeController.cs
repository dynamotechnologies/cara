﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using System.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Linq;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Aquilent.Cara.WebApp.FakePalsAuthentication.Models;

namespace WebApp.FakePalsAuthentication.Controllers
{
	[HandleError]
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewData["Message"] = "Welcome to the PALS Authentication Faker!";

            FakePalsModel model;
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dataMart = new DataMartManager();
                dataMart.UserManager = userManager;
                ILookupDataLoader unitLoader = new UnitLoader();
                var unitLookup = new LookupUnit();
                unitLookup.DataLoader = unitLoader;

                ViewData["LookupUnit"] = unitLookup;

                model = new FakePalsModel
                {
                    Users = dataMart.FindUsers(new DataMartUserFilter(), true),
                    ShowManualLogin = Convert.ToBoolean(ConfigurationManager.AppSettings["showManualLogin"]),
                    ShowUserTable = Convert.ToBoolean(ConfigurationManager.AppSettings["showUserTable"])
                };

                dataMart.Dispose();
            }

			return View(model);
		}

		public RedirectResult Validate()
		{
			var protocol = "http";
			if (Request.ServerVariables["HTTPS"] != "off" && !string.IsNullOrWhiteSpace(Request.ServerVariables["HTTPS"]))
			{
				protocol += "s";
			}

			string username = Request.Params["username"];
			TempData["username"] = username;

			var authRequestUrl = new StringBuilder();


			authRequestUrl.Append(ConfigurationManager.AppSettings["caraHome"]);
			authRequestUrl.Append("/");
			authRequestUrl.Append(ConfigurationManager.AppSettings["caraAuthRequestUrl"]);
			authRequestUrl.Append("?servicename=");
			authRequestUrl.Append(ConfigurationManager.AppSettings["caraAuthUsername"]);
			authRequestUrl.Append("&servicepassword=");
			authRequestUrl.Append(Url.Encode(ConfigurationManager.AppSettings["caraAuthPassword"]));
			authRequestUrl.Append("&username=");
			authRequestUrl.Append(username);

			string authRequestUrlString = authRequestUrl.ToString();
			var request = WebRequest.Create(authRequestUrlString);
			var response = request.GetResponse();

			string redirectUrl = "~/Home/ValidationError";
			if (! response.ContentType.Contains("text/xml"))
			{
				TempData["message"] = "The content of the response should be 'text/xml', not '" + response.ContentType + "'";
			}
			else
			{
				Stream responseStream = response.GetResponseStream();
				var xdoc = XDocument.Load(responseStream, LoadOptions.None);
				var success = (bool) xdoc.Root.Attribute("success");

				if (!success)
				{
					TempData["message"] = xdoc.Root.Value;
				}
				else
				{
					var loginUrl = new StringBuilder();
					loginUrl.Append(ConfigurationManager.AppSettings["caraHome"]);
					loginUrl.Append("/");
					loginUrl.Append(ConfigurationManager.AppSettings["caraLoginUrl"]);
					loginUrl.Append("?token=");
					loginUrl.Append(xdoc.Root.Value);

					redirectUrl = loginUrl.ToString();
				}
			}

			return Redirect(redirectUrl);
		}

		public ActionResult ValidationError()
		{
			ViewData["message"] = TempData["message"];
			ViewData["username"] = TempData["username"];

			return View();
		}

		public ActionResult About()
		{
			return View();
		}
	}
}
