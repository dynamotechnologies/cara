﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Error
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Sorry, an error occurred while processing your request.
    </h2>
	<br />
	<%: Model.Exception.Message %><br />
	<%= Model.Exception.StackTrace.Replace(" at ", "<br/> at ") %>

	<% if (Model.Exception.InnerException != null)
	{ %>
	<%: Model.Exception.InnerException.Message%><br />
	<%= Model.Exception.InnerException.StackTrace.Replace(" at ", "<br/> at ")%>
	<% } %>
</asp:Content>
