﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Aquilent.Cara.WebApp.FakePalsAuthentication.Models.FakePalsModel>" %>
<%@ Import Namespace="Aquilent.Cara.Domain.DataMart" %>
<%@ Import Namespace="Aquilent.Cara.Reference" %>
<%@ Import Namespace="Aquilent.Framework.Security" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: ViewData["Message"] %></h2>

	<% 
	if (Model.ShowUserTable)
	{ %>
    <p>
        To log into the CARA application, please choose from one of the users below:
    </p>
	<table>
	  <tr>
		<th>Username</th>
		<th>Last Name</th>
		<th>First Name</th>
		<th>Unit</th>
	  </tr>
		<% 
		foreach (User dmu in Model.Users.Users)
		{ %>
		<tr>
		  <td><%: Html.ActionLink(dmu.Username, "Validate", new { username = dmu.Username }) %></td>
		  <td><%: dmu.LastName%></td>
		  <td><%: dmu.FirstName%></td>
		  <td><% if (dmu.Units.Count() > 0) {%> <%: (ViewData["LookupUnit"] as LookupUnit)[dmu.Units[0]].Name %> <% } %></td>
		</tr>
	<%  } %>
	</table>
	<%
	} %>

	<% 
		if (Model.ShowManualLogin)
		{ %>
    <p>
        To log into the CARA application, please enter your user name and click the "Login" button:
    </p>

	<% using (Html.BeginForm("Validate", "Home"))
	{
	%>
		<%: Html.TextBoxFor(x => x.Username)%>
		<input type="submit" value="Login" />
	<%
		}
		}%>

</asp:Content>
