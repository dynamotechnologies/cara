﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    About Us
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>About</h2>
    <p>
	<ul>
        <% foreach (string s in Request.ServerVariables)
		   { %>
		<li><b><%: s %>:</b> <%: Request.ServerVariables[s] %></li>
		<%} %>
		</ul>
    </p>
</asp:Content>
