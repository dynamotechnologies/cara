﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aquilent.Framework.Security;
using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.WebApp.FakePalsAuthentication.Models
{
	public class FakePalsModel
	{
		public DataMartUserSearchResults Users { get; set; }
		public bool ShowUserTable { get; set; }
		public bool ShowManualLogin { get; set; }
		public string Username { get; set; }
	}
}