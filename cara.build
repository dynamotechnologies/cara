<?xml version="1.0"?>
<project name="CARA" default="help" 
    xmlns="http://nant.sf.net/release/0.90/nant.xsd">

  <property name="nant.settings.currentframework" value="net-4.0" />
  
  <!-- Loads the nant-contrib tasks -->
  <loadtasks assembly="c:/program files/nantcontrib-0.91/bin/NAnt.Contrib.Tasks.dll" />

  <!-- Record the time the build started -->
  <tstamp property="build.date" pattern="yyyyMMddHHmmss" />

  <!-- GLOBAL properties -->
  <property name="nant.onfailure" value="fail" />

  <!-- project properties -->
  <property name="build.config" value="Debug" overwrite="false" />
  <property name="build.config.lower" value="${string::to-lower(build.config)}" readonly="true" />
  <property name="environments" value="Test Production" />

  <!-- project constant properties -->
  <property name="vs2012-dir" 
		value="${environment::get-variable('VS110COMNTOOLS')}..\..\Common7\IDE" />
		<!-- value="${environment::get-variable('VS100COMNTOOLS')}..\..\Common7\IDE" /> -->
  <property name="installers-root" value="..\Installers" readonly="true" />
  <property name="nantlog-dir" value="${directory::get-current-directory()}\_nantlogs" readonly="true" />
  <property name="svn-repo" value="USDA-CARA" />
  <property name="svn.wc" value=".." />

  <!-- Construct build log file name -->
  <property name="buildlog" value="${nantlog-dir}\${build.date}_buildlog.txt" readonly="true" />  
  <echo message="Build log saved to: ${buildlog}" />

  <!-- temporary global fail message -->
  <property name="fail.message" value="The build failed" />

  <!-- * ******************************
       * Target:  help
       * -->
  <target name="help" description="About this build file">
    <echo>
      This nant buildfile is used to build and create a deployment package for CARA 
    </echo>
    <echo>
      Command-line properties:
        build.config:  [Release] | Debug
		svn.un: SVN Username
		svn.pw: SVN Password
            
      all:       Performs the following targets:  get, version, clean, build, test, document, publish
        
      get:       Get latest source from Subversion
       
      version:   Indicate build version (requires version.rn to exist)
          
      clean:     Removes all of the binaries compiled for the build
        Sub-Targets:
          clean.cara.webapp:                Cleans the CARA web application
          clean.fakepalsauth.webapp:        Cleans the Fake PALS Authentication web application
          clean.cara.services:              Cleans the CARA windows services
      build:     Builds all of the build targets
        Sub-Targets:
          build.cara.webapp:                Builds the Bid web application
          build.fakepalsauth.webapp:        Builds the Fake PALS Authentication web application
          <!-- build.cara.services:              Builds the TOMS windows services  - Removed bcs vdproj files were deprecated and don't work in vs2012. Hopefully this just affects the msi file that I guess we don't need -->
          build.utilities.passwordhash:     Builds the password hash utility
      
      test:      Run NUnit tests, etc. (TODO)
      
      document:  Generate documentation (TODO)
        
      package:   Create the deployment package (requires version.rn to exist)
        Sub-Targets:
          package.webapps:             Zips all of the webapps into the Installers\WebApp folder
          package.zipwebapp:           Zips a webapp and checks into StarTeam
    </echo>
  </target>
  
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  all
       * -->
  <target name="all" description="Perform all of the main targets"
  		depends="clean,build,test,document,package" />
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  clean
       * -->
  <target name="clean" description="Cleans all objects created by build targets in this file" depends="
          clean.cara.webapp,
          clean.cara.services">
    <echo>Cleaned everything</echo>
  </target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  clean.cara.services
       * -->
  <target name="clean.cara.services" description="Cleans the CARA Windows Services">
    <echo message="Cleaning 'CARA Windows Services Setup' ..." />
    <exec failonerror="false" 
        workingdir="Domain\Cara.Domain.WindowsServiceSetup\" 
        basedir="${vs2012-dir}" 
        program="devenv.exe"
        verbose="false">
      <arg value="&quot;Cara.Domain.WindowsServiceSetup.sln&quot;" />
      <arg line="/Clean" />
    </exec>
    
    <!-- Delete the build objects (for some reason this doesn't get done by VisualStudio - Clean -->
    <delete>
      <fileset basedir="Domain\Cara.Domain.WindowsServiceSetup\Cara.Domain.WindowsServiceSetup\${build.config}">
        <include name="*" />
      </fileset>
    </delete>
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  clean.cara.webapp
       * -->
  <target name="clean.cara.webapp" description="Cleans the CARA Web Application">
    <echo message="Cleaning 'CARA WebApp' ..." />
    <msbuild project="WebApp\CARA - WebApp.sln" target="Clean" verbosity="Quiet" />
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  clean.fakepalsauth.webapp
       * -->
  <target name="clean.fakepalsauth.webapp" description="Cleans the Fake PALS authentication Web Application">
    <echo message="Cleaning 'Fake PALS Authentication WebApp' ..." />
    <msbuild project="WebApp.FakePalsAuthentication\WebApp.FakePalsAuthentication.sln" target="Clean" verbosity="Quiet" />
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  verify.sourcecontrol
       * -->
  <target name="verify.sourcecontrol"> 
    <exec program="svn" workingdir="${svn.wc}" output="${nantlog-dir}\svn-info.xml" failonerror="false" resultproperty="svnerror">
      <arg line="info --xml --username ${svn.un} --password ${svn.pw}" />
    </exec>
    <xmlpeek file="${nantlog-dir}\svn-info.xml" property="svn.url" xpath="info/entry/url" />
    <property name="version" value="${epic::get-release-number(svn.url)}" />
    
    <fail message="The folder, '${path::get-full-path(svn.wc)}', is not under version control." if="${svnerror != '0'}" />
  </target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  get
       * -->
  <target name="get" description="Get the source code from source control"
        depends="build.setup, verify.sourcecontrol">
    <echo>Get: ${svn.url}</echo>

    <!-- check to ensure that your working copy is clean -->
    <if test="${svnerror == '0' and build.config.lower == 'release'}">
      <exec program="svn" workingdir="${svn.wc}" output="${nantlog-dir}\svn-status.xml">
        <arg line="status --xml --username ${svn.un} --password ${svn.pw}" />" />
      </exec>
      
      <xmlpeek file="${nantlog-dir}\svn-status.xml" property="svn.target" xpath="status/target" failonerror="false" />
      <if test="${string::contains(svn.target, 'entry')}">
        <property name="unclean-version" value="WARNING::Your working copy is not clean, which is highly recommended for a Release build." />
        <echo message="${unclean-version}" />
        <loadfile file="${nantlog-dir}\svn-status.xml" property="svn.status" />
        <echo>${svn.status}</echo>
        <echo />
      </if>
    </if>

    <!-- Update the working copy -->
    <exec program="svn" workingdir="${svn.wc}" output="${nantlog-dir}\svn-update.xml" failonerror="false" resultproperty="svnerror">
      <arg line="update --username ${svn.un} --password ${svn.pw}" />
    </exec>
    <loadfile file="${nantlog-dir}\svn-update.xml" property="svn-update" />
    <echo>${svn-update}</echo>
    
    <!-- Remove temp files -->
    <echo>Remove temporary files</echo>
    <delete>
      <fileset basedir="${nantlog-dir}">
        <include name="svn*.xml" />
      </fileset>
    </delete>
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  build
       * -->
  <target name="build" description="Compile the application" depends="
      build.setup,
      build.cara.webapp,
      build.utilities.passwordhash,
      build.utilities.logzipper">

      <!-- build.cara.services,  originally up in the list above with "Compile4 the application", removed bcs it was breaking the build afer I removed the WindowsServiceSetup solution that had a vdproj file -->

      
    <!-- Copy aquilent assemblies except for test and webapp dlls
    <copy todir="${installers-root}\Assemblies\" if="${build.config.lower == 'release'}" overwrite="true" flatten="true">
      <fileset basedir=".">
        <include name="**/bin/**Aquilent*.dll"/>
        <include name="**/bin/**Microsoft.Practices*.dll"/>
        <exclude name="**/bin/**WebApp*.dll"/>
        <exclude name="**/bin/**UnitTests*.dll"/>
        <exclude name="**/bin/**Aquilent.Cara.Reporting.dll"/>
      </fileset>
    </copy> -->
    
    <!-- Copy 3rd party assemblies
    <copy todir="${installers-root}\Assemblies\" if="${build.config.lower == 'release'}" overwrite="true" flatten="true">
      <fileset basedir="Components">
        <include name="CookComputing.XmlRpcV2.dll" />
        <include name="DiffPlex.dll" />
        <include name="HtmlAgilityPack.dll" />
        <include name="ICSharpCode.SharpZipLib.dll" />
        <include name="itextsharp.dll" />
        <include name="log4net.dll" />
        <include name="Ninject.dll" />
        <include name="Recaptcha.dll" />
        <include name="Telerik.Web.Mvc.dll" />
      </fileset>
    </copy> -->

    <!-- Copy configuration folder -->
    <if test="${build.config.lower == 'release'}">
      <copy todir="${installers-root}\Config\Common" overwrite="true" flatten="true">
        <fileset basedir="Domain\Cara.Configuration">
          <include name="**" />
        </fileset>
      </copy>
      
      <!-- Create versions for each environment -->
      <copy file="${installers-root}\Config\Common\fileStore.config" tofile="${installers-root}\Config\Common\fileStore.Test.config" />
      <xmlpoke file="${installers-root}\Config\Common\fileStore.Test.config"
        xpath="/configuration/fileStoreConfiguration/uploadFolder/@dir"
        value="caratest-documents" />
      <!--<xmlpoke file="${installers-root}\Config\Common\fileStore.Test.config"
        xpath="/configuration/fileStoreConfiguration/tempZipFolder/@dir"
        value="C:\USDA-CARA\Common\Documents\TempZips" />-->
         
      <copy file="${installers-root}\Config\Common\fileStore.config" tofile="${installers-root}\Config\Common\fileStore.Production.config" />
      <xmlpoke file="${installers-root}\Config\Common\fileStore.Production.config"
        xpath="/configuration/fileStoreConfiguration/uploadFolder/@dir"
        value="caraprod-documents" />
      <!--<xmlpoke file="${installers-root}\Config\Common\fileStore.Production.config"
        xpath="/configuration/fileStoreConfiguration/tempZipFolder/@dir"
        value="D:\USDA-CARA\Common\Documents\TempZips" />-->
    </if>
    
    <echo>Built everything</echo>
    
    <if test="${property::exists('unclean-version')}">
      <echo message="${unclean-version}" />
    </if>
  </target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  build.setup
       * -->
   <target name="build.setup">
     <mkdir dir="${nantlog-dir}" />
   </target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  build.cara.services
       * -->
 
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  build.cara.webapp
       * -->
  <target name="build.cara.webapp" description="Builds the CARA Web Application"
       depends="build.setup">
    <echo message="Building 'Web - CARA WebApp' [${build.config}] ..." />
    <msbuild project="WebApp\CARA - WebApp.sln" target="Build" verbosity="Quiet">
      <property name="Configuration" value="${build.config}" />
    </msbuild>

    <if test="${build.config.lower == 'release'}">
      <foreach item="String" in="${environments}" delim=" " property="env">

        <echo message="Transforming web.config for ${env} ..." />
        <msbuild project="WebApp\Cara.WebApp.csproj" target="TransformWebConfig" verbosity="Quiet">
          <property name="Configuration" value="${env}.Web" />
		</msbuild>
      
        <copy file="WebApp\obj\${env}\TransformWebConfig\transformed\Web.config" tofile="${installers-root}\Config\CaraWebApp\Web.${env}.config" />
      </foreach>
      
      <!-- create versions of the log4net files for deployment -->
      <copy file="WebApp\Data\CARAWebApp.log4net" tofile="${installers-root}\Config\Common\CARAWebApp.Test.log4net" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Test.log4net"
        xpath="/log4net/appender[@name='RollingFileWebApp']/file/@value"
        value="D:\USDA-CARA\Common\Logs\CaraWebApp.txt" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Test.log4net"
        xpath="/log4net/appender[@name='RollingFileExternal']/file/@value"
        value="D:\USDA-CARA\Common\Logs\DmdRepository.txt" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Test.log4net"
        xpath="/log4net/appender[@name='RollingFileDataMartUndo']/file/@value"
        value="D:\USDA-CARA\Common\Logs\DataMartUndo.txt" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Test.log4net"
        xpath="//level/@value"
        value="DEBUG" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Test.log4net"
        xpath="/log4net/logger[@name='Aquilent.Cara.Domain.DataMart.Behaviors']/level/@value"
        value="OFF" />

      <copy file="WebApp\Data\CARAWebApp.log4net" tofile="${installers-root}\Config\Common\CARAWebApp.Production.log4net" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Production.log4net"
        xpath="/log4net/appender[@name='RollingFileWebApp']/file/@value"
        value="D:\USDA-CARA\Common\Logs\CaraWebApp.txt" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Production.log4net"
        xpath="/log4net/appender[@name='RollingFileExternal']/file/@value"
        value="D:\USDA-CARA\Common\Logs\DmdRepository.txt" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Production.log4net"
        xpath="/log4net/appender[@name='RollingFileDataMartUndo']/file/@value"
        value="D:\USDA-CARA\Common\Logs\DataMartUndo.txt" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Production.log4net"
        xpath="//level/@value"
        value="INFO" />
      <xmlpoke file="${installers-root}\Config\Common\CARAWebApp.Production.log4net"
        xpath="/log4net/logger[@name='Aquilent.Cara.Domain.DataMart.Behaviors']/level/@value"
        value="OFF" />
    </if>
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  build.fakepalsauth.webapp
       * -->
  <target name="build.fakepalsauth.webapp" description="Builds the Fake PALS Authentication Web Application"
       depends="build.setup">
    <echo message="Building 'Web - Fake PALS Authentication' [${build.config}] ..." />
    <msbuild project="WebApp.FakePalsAuthentication\WebApp.FakePalsAuthentication.sln" target="Build" verbosity="Quiet">
      <property name="Configuration" value="${build.config}" />
    </msbuild>
    
    <if test="${build.config.lower == 'release'}">
      <foreach item="String" in="${environments}" delim=" " property="env">
        <echo message="Transforming web.config for ${env} ..." />
        <msbuild project="WebApp.FakePalsAuthentication\WebApp.FakePalsAuthentication.csproj" target="TransformWebConfig" verbosity="Quiet">
          <property name="Configuration" value="${env}" />
        </msbuild>
      
        <copy file="WebApp.FakePalsAuthentication\obj\${env}\TransformWebConfig\transformed\Web.config" tofile="${installers-root}\Config\FakePalsAuthWebApp\Web.${env}.config" />
      </foreach>
    </if>
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  build.utilities.passwordhash
       * -->
  <target name="build.utilities.passwordhash">
    <echo message="Building 'Utility - PasswordHash' [${build.config}] ..." />
    <msbuild project="Utilities\PasswordHash\PasswordHash.sln" target="Build" verbosity="Quiet">
      <property name="Configuration" value="${build.config}" />
    </msbuild>
    
    <copy file="Utilities\PasswordHash\bin\Release\PasswordHash.exe"
          tofile="${installers-root}\Utilities\PasswordHash\PasswordHash.exe"
          if="${build.config.lower == 'release'}" />
  </target>

  <!-- * ******************************
       * Target:  build.utilities.logzipper
       * -->
  <target name="build.utilities.logzipper">
    <echo message="Building 'Utility - LogZipper' [${build.config}] ..." />
    <msbuild project="Utilities\CaraLogZipper\CaraLogZipper.sln" target="Build" verbosity="Quiet">
      <property name="Configuration" value="${build.config}" />
    </msbuild>
    
    <copy todir="${installers-root}\Utilities\CaraLogZipper"
          if="${build.config.lower == 'release'}">
      <fileset basedir="Utilities\CaraLogZipper\bin\${build.config}">
        <include name="CaraLogZipper.exe" />
        <include name="ICSharpCode.SharpZipLib.dll" />
      </fileset>
    </copy>
  </target>
  
  <!-- * ******************************
       * Target:  test
       * -->
  <target name="test">
    <echo>TODO: Implement "test" target to run unit tests</echo>
  </target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  document
       * -->
  <target name="document"><echo message="TODO" /></target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  package
       * -->
  <target name="package">
    <copy file="Configuration\encryptconfigsection.build"
      tofile="${installers-root}\Utilities\EncryptConfigSections\encryptconfigsection.build" />
  
    <copy todir="${installers-root}\NAnt" if="${build.config.lower == 'release'}">
      <fileset basedir="${nant::get-base-directory()}">
        <include name="*" />
      </fileset>
    </copy>
  
	  <!-- WebApps zipped up and checked in -->
    <call target="package.webapps" />
    
	  <!-- Clean up from last time -->
    <property name="package.tempfolder" value="${installers-root}\Package" />
    <delete dir="${package.tempfolder}" />
    
    <!-- Copy files to packaging area -->
    <echo message="" />
    <copy todir="${package.tempfolder}\${version}">
      <fileset basedir="${installers-root}\..">
        <include name="Installers\**" />
      </fileset>
    </copy>

	  <!-- Write the release.ini file -->
	  <echo message="${version}" file="${package.tempfolder}\${version}\Installers\release.ini" />

    <!-- Move jobs from the source folder to the schema folder
    <echo message="Move jobs to Schema folder" />
    <copy todir="${package.tempfolder}\${version}\Installers\Schema\Jobs" flatten="true">
      <fileset basedir=".">
        <include name="**\Jobs\*.sql" />
      </fileset>
    </copy> -->
    
    <!-- Clean up Installers folder -->      
    <echo message="Clean up Installers folder" />
    <attrib normal="true">
      <fileset basedir="${package.tempfolder}\${version}\Installers">
        <include name="**/*.*" />
      </fileset>
    </attrib>
    
    <delete>
      <fileset basedir="${package.tempfolder}\${version}\Installers">
        <include name="*.*" />
        <!--<include name="Schema\**" />
        <include name="Deployment\Source\" />
        <include name="Deployment\Assemblies\Documentation\" />
        <exclude name="Deployment" />
        <exclude name="Schema\*.*" />
        <exclude name="Schema\${version.rn} Upgrade Scripts\**" />
        <exclude name="Schema\Jobs\**" />
        <exclude name="Schema\Assemblies\**" />
        <exclude name="Schema\Utilities\**" />-->
        <exclude name="deploy.build" />
        <exclude name="release.ini" />
      </fileset>
    </delete>
    
    <zip zipfile="..\DeploymentPackages\${version}.zip" includeemptydirs="true">
      <fileset basedir="${package.tempfolder}">
        <include name="**" />
      </fileset>
    </zip>
    
    <delete dir="${installers-root}\Package" />
    
    <if test="${property::exists('unclean-version')}">
      <echo message="${unclean-version}" />
    </if>
  </target>
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  package.webapps
       * -->
  <target name="package.webapps" description="Packages the webapps for deployment"
      depends="package.cara.webapp" />
  <!-- * ****************************** * -->
  
  <!-- * ******************************
       * Target:  package.cara.webapp
       * -->
  <target name="package.cara.webapp" if="${target::has-executed('build.cara.webapp')}">
    <property name="web-zipfile-name" value="CARA WebApp.zip" />
    <property name="webapp-path" value="WebApp" />
    <call target="package.zipwebapp.net" />
  </target>
  <!-- * ****************************** * -->

    <!-- * ******************************
       * Target:  package.fakepalsauth.webapp
       * -->
  <target name="package.fakepalsauth.webapp" if="${target::has-executed('build.cara.webapp')}">
    <property name="web-zipfile-name" value="FakePalsAuth WebApp.zip" />
    <property name="webapp-path" value="WebApp.FakePalsAuthentication" />
    <call target="package.zipwebapp.net" />
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  package.zipwebapp.net
       * -->
  <target name="package.zipwebapp.net" description="Zips the webapps for deployment">
    <if test="${not property::exists('web-zipfile-name')}">
      <fail message="The property 'web-zipfile-name' could not be found." />
    </if>

    <if test="${not property::exists('webapp-path')}">
      <fail message="The property 'webapp-path' could not be found." />
    </if>

    <property name="web-zipfile" value="${installers-root}\WebApp\${web-zipfile-name}" />
    <delete file="${web-zipfile}" />
    <zip zipfile="${web-zipfile}" includeemptydirs="false">
      <fileset basedir="${webapp-path}">
        <include name="**" />
        <exclude name="obj\**" />
        <exclude name="**\*.cs"/>
        <exclude name="**\*.resx"/>
        <exclude name="**\*.csproj"/>
        <exclude name="**\*.sln"/>
        <exclude name="**\*.suo"/>
        <exclude name="**\*.webinfo"/>
        <exclude name="**\*.pdb"/>
        <exclude name="**\*.user"/>
        <exclude name="**\*.bak"/>
        <exclude name="**\web.*.config"/>
        <exclude name="TestResults\**"/>
      </fileset>
    </zip>
  </target>
  <!-- * ****************************** * -->

  <!-- * ******************************
       * Target:  fail
       * -->
  <target name="fail"><echo message="${fail.message}" /></target>
  <!-- * ****************************** * -->
  
  <script language="C#" prefix="epic">
    <references>
      <include name="System.dll" />
    </references>
    <imports>
      <import namespace="System.Text.RegularExpressions" />
    </imports>
    <code>
      <![CDATA[
        [Function("get-release-number")]
        public static string GetReleaseNumber(string svnUrl)
        {
            string releaseNumber = "USDA-CARA";
            
            // Verify the pattern
            Regex rx = new Regex(@"v\d+.\d+.\d+");
            var match = rx.Match(svnUrl);
            
            if (match.Success)
            {
                releaseNumber = string.Format("USDA-CARA {0}", match.Value);
            }
            
            return releaseNumber;
        }
      ]]>
    </code>
  </script>  
</project>
