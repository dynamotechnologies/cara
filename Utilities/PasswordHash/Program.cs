﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PasswordHash
{
	class Program
	{
		static void Main(string[] args)
		{
			bool generateHash = true;
			StringBuilder usage = new StringBuilder();
			usage.Append("usage: passwordhash [options]\n");
			usage.Append("options:\n");
			usage.Append("\t/? or /h: displays help\n");
			usage.Append("\t/p:[password]: password to hash\n");
			usage.Append("\n");
			usage.Append("examples:\n\tpasswordhash /p:P@55W0rD\n\n");

			bool displayHelp = args.Contains("/?") || args.Contains("/h");
			if (displayHelp)
			{
				Console.Write(usage.ToString());
				generateHash = false;
			}

			var pOption = args.FirstOrDefault(arg => arg.StartsWith("/p"));
			string password = string.Empty;
			string hashedPassword;

			if (!displayHelp && pOption == null)
			{
				Console.WriteLine("The /p switch is required");
				Console.WriteLine(string.Empty);
				Console.Write(usage.ToString());
				generateHash = false;
			}
			
			if (generateHash && pOption != null)
			{
				if (pOption.IndexOf(":") != 2 || pOption.Length <= 3)
				{
					Console.WriteLine("The format for the /p switch is:  /p:[password]");
					generateHash = false;
				}
				else
				{
					password = pOption.Substring(3);
				}
			}

			if (generateHash)
			{
				var hashAlgorithm = new System.Security.Cryptography.SHA1CryptoServiceProvider();
				byte[] valueInBytes = ASCIIEncoding.ASCII.GetBytes(password);
				byte[] result = hashAlgorithm.ComputeHash(valueInBytes);
				hashedPassword = Convert.ToBase64String(result);

				Console.WriteLine(string.Format("Hashed Password: {0}", hashedPassword));
			}
		}
	}
}
