﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aquilent.Cara.Domain.Report;
using Aquilent.EntityAccess;
using Aquilent.Cara.Reporting;
using HtmlAgilityPack;
using log4net;
using Aquilent.Cara.Domain;
using System.IO;
using Aquilent.Cara.Domain.Aws.DynamoDB;
using Amazon.DynamoDBv2.Model;
using Aquilent.Cara.Domain.Aws.Sqs;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Domain.DataMart;

namespace ScratchPad
{
    class Program
    {
        static ILog logger;
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            logger = log4net.LogManager.GetLogger(typeof(Program));
            logger.Info("Start Program");

            var isTestRun = true;
            if (args.Length == 3)
            {
                isTestRun = bool.Parse(args[2]);
            }

            if (isTestRun)
            {
                logger.Info("***This is a test run***");
            }
            else
            {
                logger.Info("***This is NOT a test run***");
            }
                

            Scenario1(int.Parse(args[0]), int.Parse(args[1]), isTestRun);
            //Scenario3(int.Parse(args[0]), int.Parse(args[1]));
            //Scenario2(int.Parse(args[0]), int.Parse(args[1]), isTestRun);

            logger.Info("Finish Program");
            Console.Read();
        }

        private static void Scenario1(int projectId, int phaseId, bool isTestRun)
        {
            int failures = 0;
            DmdManager dmdManager = new DmdManager();
            var datamartManager = new DataMartManager();

            int i = 0;
            while (true)
            {
                var letterManager = ManagerFactory.CreateInstance<LetterManager>();
                var query = letterManager.All
                    .Where(x => x.PhaseId == phaseId)
                    //.Where(x => x.LetterTypeId == 1)
                    .Where(x => x.Authors.Any(y => y.LastName == "Services" && y.FirstName == "KnowWho"));

                if (isTestRun)
                {
                    query = query.OrderBy(x => x.LetterId).Skip(i);
                }
                else
                {
                    query = query.OrderBy(x => x.LetterId).Skip(failures);
                }

                IList<Letter> letters = query.Take(100).ToList();

                int count = letters.Count();
                if (count == 0)
                {
                    break;
                }
                else
                {
                    i += count;
                }

                foreach (var letter in letters)
                {
                    var lineList = new List<string>();

                    // Fill the list from the letter text
                    using (StringReader reader = new StringReader(letter.Text))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            lineList.Add(line);
                        }
                    }

                    if (lineList.Count > 4)
                    {
                        try
                        {
                            int lineCount = lineList.Count;
                            string line1 = lineList[lineList.Count - 5];
                            string line2 = lineList[lineList.Count - 4];
                            string line3 = lineList[lineList.Count - 3];
                            string line4 = lineList[lineList.Count - 2];
                            string line5 = lineList[lineList.Count - 1];

                            string[] firstAndLast = line1.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            string first = firstAndLast.First();
                            string last = firstAndLast.Last();

                            string[] cityStateZip = line3.Split(',');
                            string city = cityStateZip[0];
                            string[] statezip = cityStateZip[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            string state = statezip[0];
                            string zip = statezip[1].TrimEnd('-');

                            if (state.Length == 2 && !string.IsNullOrEmpty(line1.Trim()))
                            {
                                var sender = letter.Authors.First(x => x.Sender);

                                // Update the sender variable
                                sender.FirstName = first;
                                sender.LastName = last;
                                sender.Address1 = line2;
                                sender.City = city;
                                sender.StateId = state;
                                sender.ZipPostal = zip;
                                sender.Email = line4;
                                sender.Phone = line5.Length > 20 ? line5.Substring(0, 20) : line5;

                                logger.InfoFormat("Letter: {8}\n{0} {1}\n{2}\n{3}, {4} {5}\n{6}\n{7}\n",
                                    sender.FirstName,
                                    sender.LastName,
                                    sender.Address1,
                                    sender.City,
                                    sender.StateId,
                                    sender.ZipPostal,
                                    sender.Email,
                                    sender.Phone,
                                    letter.LetterId);

                                if (!isTestRun)
                                {
                                    // Update the letter in Dynamo
                                    logger.InfoFormat("Updating Dynamo");
                                    UpdateDynamo(letter, first, last);

                                    // Save the changes
                                    logger.InfoFormat("Saving Letter");
                                    letter.LetterTypeId = 9;
                                    letter.MasterDuplicateId = new int?();
                                    letter.FormSetId = new int?();
                                    letterManager.UnitOfWork.Save();

                                    // Add the letter to the form detector
                                    logger.InfoFormat("Sending to Form Detection");
                                    SendToFormDetectionQueue(letter);

                                    // Delete the letter from the DMD
                                    logger.InfoFormat("Sending to Delete DMD Queue");
                                    DeleteFromDmd(letter, dmdManager);

                                    // Send the letter to the DMD
                                    logger.InfoFormat("Sending to Send DMD Queue");
                                    SendToDmd(letter);

                                    // Update the mailing list
                                    logger.InfoFormat("Updating mailing list");
                                    UpdateMailingList(projectId, sender, datamartManager);
                                }
                            }
                            else
                            {
                                logger.WarnFormat("Skipping letter {0} ({1}) because it did not conform", letter.LetterId, letter.LetterSequence);
                                failures++;
                            }
                        }
                        catch (Exception ex)
                        {
                            failures++;
                            logger.ErrorFormat("ERROR:  Processing letter {0} ({3}) failed: {1}: {2}", letter.LetterId, ex.Message, ex.StackTrace, letter.LetterSequence);
                        }
                    }
                    else
                    {
                        logger.WarnFormat("Skipping letter {0} ({1}) because it had no content", letter.LetterId, letter.LetterSequence);
                        failures++;
                    }
                }

                letterManager.Dispose();
            }

            logger.InfoFormat("Total failures: {0}", failures);
            datamartManager.Dispose();
        }

        private static void Scenario3(int projectId, int phaseId)
        {
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                int i = 0;
                while (true)
                {
                    var query = letterManager.All
                        .Where(x => x.PhaseId == phaseId)
                        .Where(x => x.LetterTypeId == 1)
                        .OrderBy(x => x.LetterId)
                        //.Skip(i)
                        .Take(100);

                    IList<Letter> letters = query.ToList();

                    int count = letters.Count();
                    if (count == 0)
                    {
                        break;
                    }
                    else
                    {
                        i += count;
                    }

                    foreach (var letter in letters)
                    {
                        logger.InfoFormat("Updating letter #{0}", letter.LetterId);

                        // Update the letter in Dynamo
                        UpdateDynamo(letter, letter.Sender.FirstName, letter.Sender.LastName);

                        // Save the changes
                        letter.LetterTypeId = 9;
                        letter.MasterDuplicateId = null;
                        letterManager.UnitOfWork.Save();

                        // Add the letter to the form detector
                        SendToFormDetectionQueue(letter);
                    }
                }
            }
        }

        private static void Scenario2(int projectId, int phaseId, bool isTestRun)
        {
            DmdManager dmdManager = new DmdManager();
            var datamartManager = new DataMartManager();
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                int i = 0;
                while (true)
                {
                    IList<Letter> letters = letterManager.All
                        .Where(x => x.PhaseId == phaseId)
                        .Where(x => x.FormSetId == 5315)
                        .Where(x => x.Authors.Any(y => y.Email == "myvoice@oneclickpolitics.com"))
                        .OrderBy(x => x.LetterId)
                        .Skip(i)
                        .Take(100)
                        .ToList();

                    int count = letters.Count();
                    if (count == 0)
                    {
                        break;
                    }
                    else
                    {
                        i += count;
                    }

                    foreach (var letter in letters)
                    {
                        var lineList = new List<string>();

                        // Fill the list from the letter text
                        using (StringReader reader = new StringReader(letter.Text))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                lineList.Add(line);
                            }
                        }

                        int lineCount = lineList.Count;
                        string line1 = lineList[lineList.Count - 7];
                        string line2 = lineList[lineList.Count - 6];
                        string line3 = lineList[lineList.Count - 5];

                        //logger.Info(line1);
                        //logger.Info(line2);
                        //logger.Info(line3);

                        string[] firstAndLast = line1.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string first = firstAndLast.First();
                        string last = firstAndLast.Last();

                        string[] cityStateZip = line3.Split(',');
                        string city = cityStateZip.First();
                        string[] statezip = cityStateZip.Last().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string state = statezip[0];
                        string zip = statezip[1].TrimEnd('-');

                        var sender = letter.Authors.First(x => x.Sender);

                        // Update the sender variable
                        //sender.FirstName = first;
                        //sender.LastName = last;
                        sender.Address1 = line2;
                        sender.City = city;
                        sender.StateId = state;
                        sender.ZipPostal = zip;

                        logger.InfoFormat("Letter: {6}\n{0} {1}\n{2}\n{3}, {4} {5}\n",
                            sender.FirstName,
                            sender.LastName,
                            sender.Address1,
                            sender.City,
                            sender.StateId,
                            sender.ZipPostal,
                            letter.LetterId);

                        if (!isTestRun)
                        {
                            // Delete the letter from the DMD
                            //DeleteFromDmd(letter, dmdManager);

                            // Send the letter to the DMD
                            //SendToDmd(letter);

                            // Save the changes
                            letterManager.UnitOfWork.Save();

                            // Update the mailing list
                            UpdateMailingList(projectId, sender, datamartManager);
                        }
                    }
                }
            }

            datamartManager.Dispose();
        }

        private static void UpdateMailingList(int projectId, Author sender, DataMartManager datamartManager)
        {
            // Update the mailing list
            datamartManager.DeleteSubscriber(projectId, sender.AuthorId);
            datamartManager.AddSubscriber(projectId, sender);
        }
  
        private static void SendToDmd(Letter letter)
        {
            // Send the letter to the DMD
            string queueName;
            var sqs = SqsHelper.GetQueue("cara.queue.dmdsend", out queueName);

            var sqsRequest = new CreateQueueRequest { QueueName = queueName };
            var createQueueResponse = sqs.CreateQueue(sqsRequest);
            string queueUrl = createQueueResponse.QueueUrl;

            var sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = queueUrl,
                MessageBody = string.Format("LetterId,{0}", letter.LetterId)
            };
            sqs.SendMessage(sendMessageRequest);
        }
  
        private static void DeleteFromDmd(Letter letter, DmdManager dmdManager)
        {
            string queueName;
            var sqs = SqsHelper.GetQueue("cara.queue.dmddelete", out queueName);

            var sqsRequest = new CreateQueueRequest { QueueName = queueName };
            var createQueueResponse = sqs.CreateQueue(sqsRequest);
            string queueUrl = createQueueResponse.QueueUrl;

            var sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = queueUrl,
                MessageBody = letter.DmdId
            };
        }
        
        private static void SendToFormDetectionQueue(Letter letter)
        {
            // Add the letter to the form detector
            string fdQueueName;
            var fdsqs = SqsHelper.GetQueue("cara.queue.formdetection", out fdQueueName);
            
            var fdSqsRequest = new CreateQueueRequest { QueueName = fdQueueName };
            var fdCreateQueueResponse = fdsqs.CreateQueue(fdSqsRequest);
            string fdQueueUrl = fdCreateQueueResponse.QueueUrl;
            
            var fdSendMessageRequest = new SendMessageRequest
            {
                QueueUrl = fdQueueUrl,
                MessageBody = letter.LetterId.ToString()
            };
            fdsqs.SendMessage(fdSendMessageRequest);
        }
  
        private static void UpdateDynamo(Letter letter, string first, string last)
        {
            // Update the letter in Dynamo
            var dynamo = new DynamoDBHelper();
            bool success = dynamo.Update(
                new List<KeyValuePair<string, AttributeValue>>()
                {
                    //indexes
                    new KeyValuePair<string, AttributeValue>("PhaseId",DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                    new KeyValuePair<string, AttributeValue>("LetterId",DynamoDBHelper.GetAttributeValue(letter.LetterId))
                },
                new List<KeyValuePair<string, AttributeValue>>()
                {
                    //new values
                    new KeyValuePair<string, AttributeValue>("AuthorName",DynamoDBHelper.GetAttributeValue(string.Format("{0}|{1}", first, last))), // Replace author name
                });
        }
    }
}


