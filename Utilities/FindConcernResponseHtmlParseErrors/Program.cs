﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aquilent.Cara.Domain.Report;
using Aquilent.EntityAccess;
using Aquilent.Cara.Reporting;
using HtmlAgilityPack;
using log4net;

namespace ScratchPad
{
    class Program
    {
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger(typeof(Program));
            logger.Info("Start Program");

            int total;
            int phaseId = int.Parse(args[0]);
            int option = 3;
            int pageNum = -1;

            total = 0;
            int numRows = 0;

            IList<ResponseToCommentReportData> data;
            HtmlDocument htmlDocument;
            using (var reportManager = ManagerFactory.CreateInstance<ReportManager>())
            {
                reportManager.UnitOfWork.Context.CommandTimeout = ReportUtilities.GetCommandTimeout();
                data = reportManager.GetResponseToCommentReport(phaseId, option, pageNum, numRows, out total).ToList();
                logger.InfoFormat("Report contains {0} rows", data.Count);

                foreach (var item in data)
                {
                    if (item.ConcernText != null)
                    {
                        htmlDocument = new HtmlDocument();
                        htmlDocument.LoadHtml(item.ConcernText.Trim());

                        if (htmlDocument.ParseErrors.Count() > 0)
                        {
                            logger.ErrorFormat("Concern {0}:{1}", item.ResponseId, item.ResponseNumber);
                            LogParseErrors(htmlDocument.ParseErrors);
                        }
                    }

                    htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(item.ResponseText.Trim());

                    if (htmlDocument.ParseErrors.Count() > 0)
                    {
                        logger.ErrorFormat("Response {0}:{1}", item.ResponseId, item.ResponseNumber);
                        LogParseErrors(htmlDocument.ParseErrors);
                    }

                    foreach (var comment in item.Comments)
                    {
                        htmlDocument = new HtmlDocument();
                        htmlDocument.LoadHtml(comment.ConcatenatedCommentText.Trim());

                        if (htmlDocument.ParseErrors.Count() > 0)
                        {
                            logger.ErrorFormat("Comments {0}:{1}:{2}", item.ResponseId, item.ResponseNumber, comment.UniqueNumber);
                            LogParseErrors(htmlDocument.ParseErrors);
                        }
                    }
                }
            }

            Console.Read();
        }

        static void LogParseErrors(IEnumerable<HtmlParseError> parseErrors)
        {
            ILog logger = log4net.LogManager.GetLogger(typeof(Program));

            foreach (var error in parseErrors)
            {
                logger.ErrorFormat("\t{0}:{1}:{2}", error.Code.ToString(), error.Reason, error.SourceText);
            }
        }
    }
}


