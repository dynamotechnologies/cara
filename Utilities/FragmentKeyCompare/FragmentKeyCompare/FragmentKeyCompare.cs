﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using Amazon.DynamoDBv2.DocumentModel;

using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Aws.DynamoDB;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.FormDetection;
using log4net;
using log4net.Config;

// Add using statements to access AWS SDK for .NET services. 
// Both the Service and its Model namespace need to be added 
// in order to gain access to a service. For example, to access
// the EC2 service, add:
// using Amazon.EC2;
// using Amazon.EC2.Model;

namespace Aquilent.Cara.Domain.Ingress.FormUtilities
{
    public class FragmentKeyCompare
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FragmentKeyCompare));

        public static void Main(string[] args)
        {
            BasicConfigurator.Configure();

            try
            {
                if (args.Length == 3)
                {
                    int phaseId = int.Parse(args[0]);
                    int letterId1 = int.Parse(args[1]);
                    int letterId2 = int.Parse(args[2]);

                    var formDetectionConfig = ConfigUtilities.GetSection<FormDetectionConfiguration>("formDetectionConfiguration");
                    var formLetterDetector = new FormLetterDetector(formDetectionConfig);

                    var letter1FragKeys = GetFragmentKeys(phaseId, letterId1);
                    var letter2FragKeys = GetFragmentKeys(phaseId, letterId2);

                    var letter1RelFragKeys = formLetterDetector.GetRelevantFragmentKeys(letter1FragKeys);
                    var letter2RelFragKeys = formLetterDetector.GetRelevantFragmentKeys(letter2FragKeys);

                    List<string> shortList = letter1RelFragKeys.Count() < letter2RelFragKeys.Count() ? letter1RelFragKeys : letter2RelFragKeys;
                    List<string> longList = letter1RelFragKeys.Count() < letter2RelFragKeys.Count() ? letter2RelFragKeys : letter1RelFragKeys;

                    List<string> matches = shortList.Where(x => longList.Contains(x)).ToList();

                    Console.WriteLine();
                    Console.WriteLine("Letter 1: Total Frags = {0}, Relevant Frags = {1}", letter1FragKeys.Count, letter1RelFragKeys.Count);
                    Console.WriteLine("Letter 2: Total Frags = {0}, Relevant Frags = {1}", letter2FragKeys.Count, letter2RelFragKeys.Count);
                    Console.WriteLine("Matches:");

                    foreach (string s in matches)
                    {
                        Console.WriteLine("\t{0}", s);
                    }

                    var confidence = Math.Min(Convert.ToDouble(matches.Count()) / shortList.Count(), 1.0);
                    var minConfidence = formDetectionConfig.LetterSizes.First(x => x.MinFragments <= shortList.Count && x.MaxFragments >= shortList.Count).MinConfidence;
                    var gradeName = formDetectionConfig.ConfidenceGrades.First(x => x.MinConfidence <= confidence && x.MaxConfidence >= confidence).GradeName;
                    Console.WriteLine("Confidence: {0} ({1}) [Min Confidence: {2}]", confidence, gradeName, minConfidence);
                }
                else
                {
                    // Deleting from Dynamo
                    var helper = new DynamoDBHelper();
                    FileInfo fi = new FileInfo(@"Data\letters.txt");
                    using (var fs = fi.OpenRead())
                    {
                        using (var tr = new StreamReader(fs))
                        {
                            string line;
                            while (true)
                            {
                                line = tr.ReadLine();
                                if (string.IsNullOrEmpty(line))
                                {
                                    break;
                                }

                                var parts = line.Split('\t');
                                List<KeyValuePair<string, AttributeValue>> primaryKey = new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    new KeyValuePair<string, AttributeValue>("PhaseId", new AttributeValue { N = parts[0] }),
                                    new KeyValuePair<string, AttributeValue>("LetterId", new AttributeValue { N = parts[1] })
                                };

                                helper.Remove(primaryKey);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("\tFragmentKeyCompare [phaseId] [letterId1] [letterId2]");
                Console.WriteLine("\tOr:");
                Console.WriteLine("\tFragmentKeyCompare");
                Console.WriteLine("\t\tDeletes records found in Data\\letters.txt, where each line is in the format [phaseId]tab[letterId]");
            }

            Console.WriteLine("Done.");
            Console.ReadLine();
        }
  
        private static List<string> GetFragmentKeys(int phaseId, int letterId)
        {
            var helper = new DynamoDBHelper();

            List<KeyValuePair<string, AttributeValue>> result = helper.GetItem(new List<KeyValuePair<string, AttributeValue>>()
            {
                new KeyValuePair<string, AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(phaseId)),
                new KeyValuePair<string, AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(letterId))
            });

            List<string> rawFragKeys = DynamoDBHelper.GetStringSetAttribute(result, "FragmentKeys");
            rawFragKeys = rawFragKeys.OrderBy(x => x).ToList();

            return rawFragKeys;
        }
    }
}