﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace CaraLogZipper
{
	class Program
	{
		public static DateTime LastMonth
		{
			get
			{
				DateTime now = DateTime.Now;
				int lastMonth = now.Month > 1 ? now.Month - 1 : 12;
				int theYearForLastMonth = lastMonth != 12 ? now.Year : now.Year - 1;

				return new DateTime(theYearForLastMonth, lastMonth, 1);
			}
		}

		public static DirectoryInfo ArchiveDir(string parentPath)
		{
			var archiveFolder = new DirectoryInfo(string.Format(@"{0}\Archive", parentPath));

			if (!archiveFolder.Exists)
			{
				archiveFolder.Create();
			}

			return archiveFolder;
		}

		static void Main(string[] args)
		{
			try
			{
				string path = args[0];
				if (path.EndsWith("\\") || path.EndsWith("\\\""))
				{
					throw new ApplicationException("Path must not end with '\\'");
				}

				DateTime archiveMonth = LastMonth;
				if (args.Length == 3)
				{
					archiveMonth = new DateTime(Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), 1);
				}

				var dirInfo = new DirectoryInfo(path);

				if (!dirInfo.Exists)
				{
					throw new ApplicationException(string.Format("The path, {0}, does not exist!", path));
				}

				// Get all of the files created last month
				var filesFromLastMonth = dirInfo.GetFiles().Where(fi => fi.Name.Contains(string.Format(".{0}{1}", archiveMonth.Year, archiveMonth.Month < 10 ? "0" + archiveMonth.Month : archiveMonth.Month.ToString()))).ToList();

				var archiveFile = new FileInfo(
					string.Format(
						@"{0}\{1}-{2}.zip",
						ArchiveDir(path).FullName,
						archiveMonth.Year,
						archiveMonth.Month < 10 ? "0" + archiveMonth.Month : archiveMonth.Month.ToString()
					));

				if (filesFromLastMonth.Count > 0)
				{
					if (archiveFile.Exists)
					{
						throw new ApplicationException(string.Format("The Log Zipper has already been run for {0}-{1}", archiveMonth.Year, archiveMonth.Month));
					}
					else
					{
						// Write the files to the zip file
						using (var zs = new ZipOutputStream(archiveFile.OpenWrite()))
						{
							foreach (var file in filesFromLastMonth)
							{
								var zipEntry = new ZipEntry(file.Name);
								zs.PutNextEntry(zipEntry);
								var contents = File.ReadAllBytes(file.FullName);
								zs.Write(contents, 0, contents.Length);
								zs.CloseEntry();
							}
						}
					}

					// Delete all of the files
					foreach (var file in filesFromLastMonth)
					{
						file.Delete();
					}

				}
			}
			catch (Exception ae)
			{
				Console.WriteLine(ae.Message);
			}
		}
	}
}
