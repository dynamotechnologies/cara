﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Aquilent.Cara.Domain.Dmd;
using log4net.Config;

namespace SendToDmd
{
    class Program
    {
        public static readonly string NO_ATTACHMENTS = "noatt";
        static void Main(string[] args)
        {
            try
            {
                // Configure log4net
                BasicConfigurator.Configure();

                var arguments = ParseArguments(args);

                var dmdManager = new DmdManager();
                var sender = new Sender(dmdManager);

                foreach (var letterId in arguments.LetterIds)
                {
                    sender.SendLetter(letterId, arguments.SendAttachments.First(kvp => kvp.Key == letterId).Value);
                }

                foreach (var docId in arguments.DocumentIds)
                {
                    sender.SendDocument(docId);
                }
            }
            catch (ArgumentException ae)
            {
                Console.WriteLine(ae.Message);
                Console.WriteLine(string.Empty);
                ShowHelp();
            }

            Console.WriteLine("Press 'q' to quit");
            ConsoleKeyInfo c;
            do
            {
                c = Console.ReadKey();
            } while (c.KeyChar != 'q');
        }

        /// <summary>
        /// Parse Arguments
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Arguments ParseArguments(string[] args)
        {
            var arguments = new Arguments();

            if (args.Length == 0)
            {
                throw new ArgumentException("No arguments were passed in");
            }

            foreach (var arg in args)
            {
                if (!arg.StartsWith("/"))
                {
                    throw new ArgumentException("No arguments were passed in");
                }

                if (arg.StartsWith("/l"))
                {
                    var pieces = arg.Split('=');

                    if (pieces.Length == 2)
                    {
                        var morePieces = pieces[1].Split(':');

                        int letterId;
                        if (int.TryParse(morePieces[0], out letterId))
                        {
                            arguments.LetterIds.Add(letterId);
                        }
                        else
                        {
                            throw new ArgumentException("Illegal argument found:  " + arg);
                        }

                        if (morePieces.Length == 2 && morePieces[1] == NO_ATTACHMENTS)
                        {
                            arguments.SendAttachments.Add(new KeyValuePair<int, bool>(letterId, false));
                        }
                        else
                        {
                            arguments.SendAttachments.Add(new KeyValuePair<int, bool>(letterId, true));
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Illegal argument found:  " + arg);
                    }
                }

                if (arg.StartsWith("/d"))
                {
                    var pieces = arg.Split('=');

                    if (pieces.Length == 2)
                    {
                        int letterId;
                        if (int.TryParse(pieces[1], out letterId))
                        {
                            arguments.DocumentIds.Add(letterId);
                        }
                        else
                        {
                            throw new ArgumentException("Illegal argument found:  " + arg);
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Illegal argument found:  " + arg);
                    }
                }
            }

            return arguments;
        }

        static void ShowHelp()
        {
            Console.WriteLine("usage:  SendToDmd [/l={letterid}] [/l={letterid}:noatt] [/d={documentid}]");
            Console.WriteLine("\tletterid is an integer");
            Console.WriteLine("\tnoatt means do not send the letter attachments with the letter");
            Console.WriteLine("\tdocumentid is an integer");
        }
    }
}
