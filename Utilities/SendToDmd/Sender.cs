﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;

namespace SendToDmd
{
    public class Sender
    {
        private ILog _logger = LogManager.GetLogger(typeof(Sender));
        private DmdManager _dmdManager;

        public Sender(DmdManager dmdManager)
        {
            _dmdManager = dmdManager;
        }

        public void SendLetter(int letterId, bool sendAttachments)
        {
            try
            {
                // Use the objectcontext directly
                using (var lettertManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    var letter = lettertManager.Get(letterId);

                    using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        _dmdManager.PutLetter(letter, documentManager, sendAttachments);
                    }
                }
            }
            catch (LetterException ae)
            {
                _logger.Error(ae.Message);
                throw new ApplicationException("An error occurred sending letter " + letterId, ae);
            }
        }

        public void SendDocument(int documentId)
        {
            try
            {
                // Use the objectcontext directly
                using (var domainContext = new CaraDomainEntities())
                {
                    var letterDocument = domainContext.LetterDocuments.First(ld => ld.DocumentId == documentId);
                    var letter = letterDocument.Letter;
                    var projectNumber = letter.Phase.Project.ProjectNumber;

                    using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        var document = documentManager.Get(documentId);
                        _dmdManager.PutAttachment(letter, projectNumber, document, documentManager);
                    }
                }
            }
            catch (LetterException ae)
            {
                _logger.Error(ae.Message);
                throw new ApplicationException("An error occurred sending document " + documentId, ae);
            }
        }
    }
}
