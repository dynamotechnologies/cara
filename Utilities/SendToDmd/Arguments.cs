﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendToDmd
{
    public class Arguments
    {
        private IList<int> _letterIds = new List<int>();
        public IList<int> LetterIds
        {
            get { return _letterIds; }
        }

        private IList<int> _documentIds = new List<int>();
        public IList<int> DocumentIds
        {
            get { return _documentIds; }
        }

        private IList<KeyValuePair<int, bool>> _sendAttachments = new List<KeyValuePair<int, bool>>();
        public IList<KeyValuePair<int, bool>> SendAttachments
        {
            get { return _sendAttachments; }
        }
    }
}
