﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Aquilent.Cara.Configuration.FormDetection;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.EntityAccess;
using log4net;
using log4net.Config;
using System.Configuration;
using Aquilent.Cara.Domain.DataModel;

namespace DynamoDataPopulater
{
    class Program
    {
        static void Main(string[] args)
        {
            ILog logger = LogManager.GetLogger(ConfigurationManager.AppSettings.Get("Logger"));
            InitializeLogger(logger);
            try
            {
                int minLetterId = 0;
                int maxLetterId = int.MaxValue;
                if (args != null && args.Count() > 0 && !String.IsNullOrWhiteSpace(args[0]))
                {
                    minLetterId = Int32.Parse(args[0]);
                }
                if (args != null && args.Count() > 1 && !String.IsNullOrWhiteSpace(args[1]))
                {
                    maxLetterId = Int32.Parse(args[1]);
                }
                int months = Convert.ToInt32(ConfigurationManager.AppSettings.Get("NumberOfMonths"));
                int dbBatchSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("DatabaseBatchSize"));
                int dbStartBatchIndex = Convert.ToInt32(ConfigurationManager.AppSettings.Get("DatabaseStartBatchIndex"));
                int retrylimit = Convert.ToInt32(GetConfigSetting("cara.dynamo.retrylimit"));
                int timeForRetry = Convert.ToInt32(GetConfigSetting("cara.dynamo.timeforretry"));

                #region starting message
                string message = "Starting letter fragment update";
                if (minLetterId > 0)
                {
                    message = message + " minimum lettr id " + minLetterId.ToString();
                }
                if (maxLetterId < int.MaxValue)
                {
                    message = message + " maximum lettr id " + maxLetterId.ToString();
                }
                logger.Info(message);
                #endregion

                FormDetectionDataManager fddm = new FormDetectionDataManager(logger);
                using (LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    ((CaraDomainEntities) letterManager.UnitOfWork.Context).Letters.MergeOption = System.Data.Objects.MergeOption.NoTracking;

                    IQueryable<Letter> letters = null;
                    if (months > 0)
                    {
                        DateTime minEndDate = DateTime.Now.AddMonths(-1 * months);
                        logger.InfoFormat("Finding letters with comment periods ending after {0}", minEndDate);
                        letters = letterManager.All
                            .Where(x => (x.Phase.CommentEnd > minEndDate || x.Phase.AlwaysOpen) &&
                                x.LetterId > minLetterId && x.LetterId < maxLetterId && x.LetterStatusId != 9)
                            .OrderBy(x=>x.LetterId);
                        logger.InfoFormat("{0} letters found", letters.Count());
                    }
                    else
                    {
                        letters = letterManager.All
                            .Where(x => x.LetterId > minLetterId && x.LetterId < maxLetterId && x.LetterStatusId != 9)
                            .OrderBy(x => x.LetterId);
                        logger.InfoFormat("{0} letters found", letters.Count());
                    }
                    FormLetterDetector fld = new FormLetterDetector(new FormDetectionConfiguration());
                    int batchNumber = dbStartBatchIndex;
                    List<Letter> batchLetters = null;
                    do
                    {
                        batchLetters = letters.Skip(batchNumber * dbBatchSize).Take(dbBatchSize).ToList();
                        logger.DebugFormat("Creating Fragment Keys for batch {0}", batchNumber);
                        batchLetters.ForEach(x =>
                            fddm.Add(fddm.CreateDataForLetter(x, fld.FragmentKeys(x.Text))));
                        logger.DebugFormat("Finished Creating Fragment Keys for batch {0}", batchNumber);
                        bool success = false;
                        for (int i = 0; i < retrylimit; i++)
                        {
                            success = fddm.Save();
                            if (!success)
                            {
                                logger.ErrorFormat("Complete Save attempt {0} for batch {1} failed.", i + 1, batchNumber);
                                System.Threading.Thread.Sleep(timeForRetry);
                                continue;
                            }
                            break;
                        }
                        if (!success)
                        {
                            message = "The following letters were not updated.";
                            fddm.UnprocessedItems
                                .Select(x => x.LetterId)
                                .ToList()
                                .ForEach(x => message = message + "\n" + x.ToString());
                            logger.Error(message);
                        }
                        fddm.ClearCache();
                        batchNumber++;
                    } while (batchLetters.Count > 0);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error occurred in Dynamo Data Populater", ex);
            }
            logger.InfoFormat("Finished");
            //Console.ReadLine();
        }


        public static ILog InitializeLogger(object objectThatLogs)
        {
            var logger = LogManager.GetLogger(objectThatLogs.GetType());

            string log4netFile = ConfigurationManager.AppSettings["log4net.configfile"];

            FileInfo fi = null;
            if (!string.IsNullOrEmpty(log4netFile))
            {
                fi = new FileInfo(log4netFile);
            }

            if (fi != null && fi.Exists)
            {
                XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netFile));
            }
            else
            {
                logger.Warn("No valid log4net configuration file was found");
                BasicConfigurator.Configure();
            }

            return logger;
        }

        private static string GetConfigSetting(string key)
        {
            string returnValue = "";
            using (CaraSettingManager settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                CaraSetting setting = settingsManager.Get(key);
                if (setting != null)
                {
                    returnValue = setting.Value;
                }
                else
                {
                    throw new Exception(string.Format("The configuration setting {0} could not be found", key));
                }
            }
            return returnValue;
        }
    }
}
