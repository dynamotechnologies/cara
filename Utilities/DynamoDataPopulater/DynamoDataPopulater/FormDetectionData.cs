﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aquilent.Cara.Domain;
using System.Text;

namespace DynamoDataPopulater
{
    class FormDetectionData
    {
        #region Properties
        public int LetterId;
        public string AuthorFirstName;
        public string AuthorLastName; 
        public int FormSetId;
        public int LetterTypeId;
        public int PhaseId;
        public int Size;
        public List<string> FragmentKeys;
        public FormDetectionDataStatus Status;
        public string Text;


        public string AuthorName
        {
            get
            {
                if (AuthorFirstName == null || AuthorLastName == null)
                {
                    return null;
                }
                return string.Format("{0}|{1}", AuthorFirstName.Replace("|", "\\|"), AuthorLastName.Replace("|", "\\|"));
            }

            set
            {
                string[] splitToken = new string[] { "\\|" };
                string[] nameFragments = value.Split(splitToken, StringSplitOptions.None);
                string firstName = string.Empty;
                string lastName = string.Empty;
                bool foundPipe = false;
                foreach (string nameFragment in nameFragments)
                {
                    if (!foundPipe)
                    {
                        if (firstName.Length > 0)
                        {
                            firstName = firstName + "|";
                        }

                        if (nameFragment.Contains('|'))
                        {
                            firstName = firstName + nameFragment.Split('|')[0];
                            lastName = nameFragment.Split('|')[1];
                            foundPipe = true;
                        }
                        else
                        {
                            firstName = firstName + nameFragment;
                        }
                    }
                    else
                    {
                        lastName = string.Format("{0}|{1}", lastName, nameFragment);
                    }
                }

                AuthorFirstName = firstName;
                AuthorLastName = lastName;
            }
        }


        #endregion Properties

        #region Constructors
        public FormDetectionData(Letter letter)
        {
            if (letter.Sender != null)
            {
                AuthorFirstName = letter.Sender.FirstName;
                AuthorLastName = letter.Sender.LastName;
            }
            FormSetId = letter.FormSetId.HasValue ? letter.FormSetId.Value : 0;
            LetterId = letter.LetterId;
            LetterTypeId = letter.LetterTypeId;
            PhaseId = letter.PhaseId;
            Size = letter.Size.HasValue ? letter.Size.Value: 0;
            Status = FormDetectionDataStatus.Missing;
            Text = letter.Text;
        }
        public FormDetectionData()
        { }
        #endregion Constructors

        #region methods
        #endregion //methods
    }

    public enum FormDetectionDataStatus
    {
        Retrieved = 0,
        Updated = 1,
        Added = 2,
        Deleted = 3,
        Missing = 4
    }
}
