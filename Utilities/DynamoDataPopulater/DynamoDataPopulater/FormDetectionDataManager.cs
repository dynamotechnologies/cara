﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using Aquilent.Cara.Reference;
using Aquilent.Cara.Domain;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Aws.DynamoDB;

using log4net;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;

namespace DynamoDataPopulater
{
    class FormDetectionDataManager
    {
        private List<FormDetectionData> dataList = new List<FormDetectionData>();
        public List<FormDetectionData> UnprocessedItems
        {
            get
            {
                return dataList.Where(x => x.Status != FormDetectionDataStatus.Retrieved).ToList();
            }
        }
        private ILog _logger;

        #region Properties for Dynamo functions
        private int batchWriteLimit; // number of items to add to each Batch write or delete request
        private int batchReadLimit; //number of results to return from each query request
        private int retryLimit; //number of times to retry a query if it fails
        private int timeForRetry; //length of time (milliseconds) to wait between request retries
        private string tableName;
        private AmazonDynamoDBClient client = null;
        private DynamoDBHelper helper = null;
        #endregion //Properties for Dynamo functions

        public FormDetectionDataManager(ILog log = null)
        {
            if (log != null)
            {
                _logger = log;
            }
            else
            {
                _logger = LogManager.GetLogger(typeof(FormDetectionDataManager));
            }
            helper = new DynamoDBHelper(log);

        }

        public void Add(FormDetectionData data)
        {
            FormDetectionData oldData = null;
            FormDetectionDataStatus oldStatus = FormDetectionDataStatus.Missing;
            if (dataList.Any(x => x.LetterId == data.LetterId))
            {
                oldData = dataList.FirstOrDefault(x => x.LetterId == data.LetterId);
                oldStatus = oldData.Status;
            }
            switch (oldStatus)
            {
                case FormDetectionDataStatus.Added:
                case FormDetectionDataStatus.Missing:
                case FormDetectionDataStatus.Deleted:
                    data.Status = FormDetectionDataStatus.Added;
                    break;
                case FormDetectionDataStatus.Retrieved:
                    if (GetItemByData(oldData) == GetItemByData(data))
                    {
                        data.Status = FormDetectionDataStatus.Retrieved;
                    }
                    else
                    {
                        data.Status = FormDetectionDataStatus.Updated;
                    }
                    break;
                case FormDetectionDataStatus.Updated:
                    data.Status = FormDetectionDataStatus.Updated;
                    break;
            }
            dataList.RemoveAll(x => x.LetterId == data.LetterId);
            dataList.Add(data);
        }

        public bool Save()
        {
            bool retVal = false;
            List<FormDetectionData> itemsToAdd = dataList
                .Where(x => x.Status == FormDetectionDataStatus.Added || x.Status == FormDetectionDataStatus.Updated).ToList();
            List<FormDetectionData> itemsToDelete = dataList.Where(x => x.Status == FormDetectionDataStatus.Deleted).ToList();
            //List<FormDetectionData> itemsToUpdate = dataList.Where(x => x.Status == FormDetectionDataStatus.Updated).ToList();
            List<FormDetectionData> successfulItems = new List<FormDetectionData>();
            if (itemsToAdd.Any())
            {
                successfulItems = helper
                    .Put(itemsToAdd.Select(x => GetItemByData(x)).ToList())
                    .Select(x=>GetDataFromItem(x))
                    .ToList();
                dataList
                    .Where(listItem => successfulItems.Any(successItem => successItem.LetterId == listItem.LetterId))
                    .ToList().ForEach(listItem => listItem.Status = FormDetectionDataStatus.Retrieved);
            }
            if (itemsToDelete.Any())
            {
                successfulItems = helper
                    .Remove(itemsToDelete.Select(x => GetKeys(x)).ToList())
                    .Select(x => GetDataFromItem(x))
                    .ToList();
                dataList.RemoveAll(listItem => successfulItems.Any(successItem => successItem.LetterId == listItem.LetterId));
            }
            retVal = !dataList
                .Any(x => x.Status == FormDetectionDataStatus.Added || 
                    x.Status == FormDetectionDataStatus.Deleted || 
                    x.Status == FormDetectionDataStatus.Updated);
            if (!retVal)
            {
                string message = "The following letters were not updated in DynamoDB: ";
                dataList
                    .Where(x => x.Status == FormDetectionDataStatus.Added ||
                        x.Status == FormDetectionDataStatus.Deleted ||
                        x.Status == FormDetectionDataStatus.Updated)
                    .Select(x => x.LetterId)
                    .ToList()
                    .ForEach(x => message = message + "\n" + x.ToString());
                _logger.Error(message);
            }
            return retVal;
        }

        public void ClearCache()
        {
            dataList = new List<FormDetectionData>();
        }

        public FormDetectionData CreateDataForLetter(Letter letter)
        {
            Author sender = letter.Authors.First(x => x.Sender == true);
            FormDetectionData data = new FormDetectionData()
            {
                LetterId = letter.LetterId,
                PhaseId = letter.PhaseId,
                AuthorFirstName = sender.FirstName,
                AuthorLastName = sender.LastName,
                LetterTypeId = letter.LetterTypeId,
                Size = letter.Size.HasValue ? letter.Size.Value : 0,
                FormSetId = letter.FormSetId.HasValue ? letter.FormSetId.Value : 0,
                Status = FormDetectionDataStatus.Added
            };
            return data;
        }

        public FormDetectionData CreateDataForLetter(Letter letter, List<string> fragmentKeys)
        {
            Author sender = letter.Authors.First(x => x.Sender == true);
            FormDetectionData data = new FormDetectionData()
            {
                LetterId = letter.LetterId,
                PhaseId = letter.PhaseId,
                AuthorFirstName = sender.FirstName,
                AuthorLastName = sender.LastName,
                LetterTypeId = letter.LetterTypeId,
                Size = letter.Size.HasValue ? letter.Size.Value : 0,
                FormSetId = letter.FormSetId.HasValue ? letter.FormSetId.Value : 0,
                Status = FormDetectionDataStatus.Added,
                FragmentKeys = fragmentKeys
            };
            return data;
        }

        public FormDetectionData RetrieveFormDetectionData(Letter letter)
        {
            FormDetectionData retVal = null;
            if (dataList.Any(x => x.LetterId == letter.LetterId))
            {
                retVal = dataList.FirstOrDefault(x => x.LetterId == letter.LetterId);
            }
            else
            {
                retVal = GetDataFromItem(helper.GetItem(GetKeys(letter)));
                if (retVal == null)
                {
                    retVal = new FormDetectionData(letter);
                }
                else
                {
                    retVal.Status = FormDetectionDataStatus.Retrieved;
                    dataList.Add(retVal);
                }
            }
            return retVal;
        }


        public void Update(FormDetectionData data)
        {
            FormDetectionData oldData = null;
            FormDetectionDataStatus oldStatus = FormDetectionDataStatus.Missing;
            if (dataList.Any(x => x.LetterId == data.LetterId))
            {
                oldData = dataList.FirstOrDefault(x => x.LetterId == data.LetterId);
                oldStatus = oldData.Status;
            }
            switch (oldStatus)
            {
                case FormDetectionDataStatus.Added:
                case FormDetectionDataStatus.Missing:
                case FormDetectionDataStatus.Deleted:
                    data.Status = FormDetectionDataStatus.Added;
                    break;
                case FormDetectionDataStatus.Retrieved:
                    if (GetItemByData(oldData) == GetItemByData(data))
                    {
                        data.Status = FormDetectionDataStatus.Retrieved;
                    }
                    else
                    {
                        data.Status = FormDetectionDataStatus.Updated;
                    }
                    break;
                case FormDetectionDataStatus.Updated:
                    data.Status = FormDetectionDataStatus.Updated;
                    break;
            }
            dataList.RemoveAll(x => x.LetterId == data.LetterId);
            dataList.Add(data);
        }

        public void Remove(FormDetectionData data)
        {
            dataList.RemoveAll(x => x.LetterId == data.LetterId);
            data.Status = FormDetectionDataStatus.Deleted;
            dataList.Add(data);
        }

        #region DynamoDB access Methods
        private List<KeyValuePair<string, AttributeValue>> GetItemByData(FormDetectionData data)
        {
            List<KeyValuePair<string, AttributeValue>> returnValue = new List<KeyValuePair<string, AttributeValue>>()
                {
                    new KeyValuePair<string, AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(data.LetterId)),
                    new KeyValuePair<string, AttributeValue>("FormSetId", DynamoDBHelper.GetAttributeValue(data.FormSetId)),
                    new KeyValuePair<string, AttributeValue>("AuthorName", DynamoDBHelper.GetAttributeValue(data.AuthorName)),
                    new KeyValuePair<string, AttributeValue>("LetterTypeId", DynamoDBHelper.GetAttributeValue(data.LetterTypeId)),
                    new KeyValuePair<string, AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(data.PhaseId)),
                    new KeyValuePair<string, AttributeValue>("Size", DynamoDBHelper.GetAttributeValue(data.Size))
                };
            returnValue.RemoveAll(x => x.Value.S == "");
                if (data.FragmentKeys.Any())
                {
                    returnValue.Add(new KeyValuePair<string, AttributeValue>
                        ("FragmentKeys", DynamoDBHelper.GetAttributeValue(data.FragmentKeys)));
                }

                return returnValue;
        }

        private List<KeyValuePair<string, AttributeValue>> GetKeys(FormDetectionData data)
        {
            List<KeyValuePair<string, AttributeValue>> returnValue = new List<KeyValuePair<string, AttributeValue>>()
                {
                    new KeyValuePair<string, AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(data.LetterId)),
                    new KeyValuePair<string, AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(data.PhaseId))
                };

            return returnValue;
        }

        private List<KeyValuePair<string, AttributeValue>> GetKeys(Letter letter)
        {
            List<KeyValuePair<string, AttributeValue>> returnValue = new List<KeyValuePair<string, AttributeValue>>()
                {
                    new KeyValuePair<string, AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId)),
                    new KeyValuePair<string, AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId))
                };

            return returnValue;
        }

        private FormDetectionData GetDataFromItem(List<KeyValuePair<string, AttributeValue>> item)
        {
            if (item == null)
            {
                return null;
            }

            FormDetectionData data = new FormDetectionData()
            {
                LetterId = DynamoDBHelper.GetIntAttribute(item, "LetterId"),
                FormSetId = DynamoDBHelper.GetIntAttribute(item, "FormSetId"),
                AuthorName = DynamoDBHelper.GetStringAttribute(item, "AuthorName"),
                LetterTypeId = DynamoDBHelper.GetIntAttribute(item, "LetterTypeId"),
                PhaseId = DynamoDBHelper.GetIntAttribute(item, "PhaseId"),
                Size = DynamoDBHelper.GetIntAttribute(item, "Size"),
                FragmentKeys = DynamoDBHelper.GetStringSetAttribute(item, "FragmentKeys")
            };
            return data;
        }

        #endregion


    }
}
