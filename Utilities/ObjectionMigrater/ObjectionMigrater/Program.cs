﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Configuration;

using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.Aws.Sqs;

using log4net;
using log4net.Config;
using Microsoft.VisualBasic.FileIO;
namespace ObjectionMigrater
{
    class Program
    {
        private static readonly string queueNameKey = "cara.queue.letterupload";
        static ILog logger = LogManager.GetLogger(typeof(Program));
        readonly static string commentPeriodName = "Migrated Objection Period";
        static void Main(string[] args)
        {
            int startOnRow = 2;
            if (args != null && args.Count() > 0 && !String.IsNullOrWhiteSpace(args[0]))
            {
                if (!int.TryParse(args[0], out startOnRow))
                {
                    startOnRow = 2;
                }
            }
            int stopOnRow = -1;
            if (args != null && args.Count() > 1 && !String.IsNullOrWhiteSpace(args[1]))
            {
                if (!int.TryParse(args[1], out stopOnRow))
                {
                    stopOnRow = -1;
                }
            }
            logger.InfoFormat(
                "Starting on row {0} ending on row {1}.",
                startOnRow,
                stopOnRow < 0 ? "end" : stopOnRow.ToString());
            int currentRow = 0;
            string filepath = "";
            filepath = ConfigurationManager.AppSettings.Get("dataFilePath");
            InitializeLogger(logger);
            try
            {
                List<int> letterIds = new List<int>();

                using (TextFieldParser txtParser = new TextFieldParser(filepath))
                {
                    txtParser.HasFieldsEnclosedInQuotes = true;
                    txtParser.Delimiters = new string[]{","};
                    //skip the specified number of lines
                    for (int i = 0; i < startOnRow - 1; i++)
                    {
                        currentRow++;
                        txtParser.ReadFields();
                        if (txtParser.EndOfData)
                        {
                            break;
                        }
                    }

                    while (!txtParser.EndOfData && (stopOnRow < 0 || currentRow < stopOnRow))
                    {
                        currentRow++;
                        string[] fields = txtParser.ReadFields();
                        if (fields.Count() < 30)
                        {
                            logger.ErrorFormat(
                                "Row {0} only has {1} field values, skipping that row", 
                                currentRow, 
                                fields.Count());
                            continue;
                        }
                        string lastUpdatedBy = fields[28]; //updated for scrub
                        string projectNumber = fields[0]; //updated for scrub
                        
                        Project project = null;
                        using (ProjectManager projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                        {
                            #region Get / Create the project.
                            if (projectManager.Find(x => x.ProjectNumber == projectNumber).Any())
                            {
                                project = projectManager.Find(x => x.ProjectNumber == projectNumber).FirstOrDefault();
                            }
                            else
                            {
                                //create new project
                                project = CreateProject(projectNumber, projectManager, lastUpdatedBy);
                            }
                            #endregion

                            using (PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                            {
                                #region Get / Create the comment period
                                Phase phase = phaseManager.All
                                    .Where(x => x.ProjectId == project.ProjectId && x.Name == commentPeriodName)
                                    .FirstOrDefault();
                                if (phase == null)
                                {
                                    phase = CreateObjection(projectManager, project, fields);
                                }
                                #endregion

                                #region Get Letter properties
                                #region LetterStatus
                                int letterStatusId = 1;
                                LookupLetterStatus lkuLetterStatus = LookupManager.GetLookup<LookupLetterStatus>();
                                switch (fields[11].Trim().ToLower())
                                {
                                    case "open":
                                        letterStatusId = lkuLetterStatus.FirstOrDefault(x => x.Name == "New").LetterStatusId;
                                        break;
                                    case "under review":
                                        letterStatusId = lkuLetterStatus.FirstOrDefault(x => x.Name == "In Progress").LetterStatusId;
                                        break;
                                    case "closed":
                                        letterStatusId = lkuLetterStatus.FirstOrDefault(x => x.Name == "Complete").LetterStatusId;
                                        break;
                                }
                                #endregion

                                #region Meeting Info
                                string meetingInfo = "";
                                if (fields[15].Trim().ToLower() == "a")
                                {
                                    meetingInfo += "Actual ";
                                }
                                else if (fields[15].Trim().ToLower() == "e")
                                {
                                    meetingInfo += "Estimated ";
                                }
                                meetingInfo = string.Format("{0}Meeting Date: {1}", meetingInfo, fields[14]);
                                #endregion

                                #region Review status
                                LookupObjectionReviewStatus lkuObjectionReviewStatus =
                                    LookupManager.GetLookup<LookupObjectionReviewStatus>();
                                ObjectionReviewStatus status = lkuObjectionReviewStatus
                                    .FirstOrDefault(x => x.Name.Trim().ToUpper() == fields[20].Trim().ToUpper());
                                if (status == null)
                                {
                                    status = lkuObjectionReviewStatus
                                    .FirstOrDefault(x => x.Name == "New");
                                }
                                int reviewedStatusId = status.ObjectionReviewStatusId;
                                #endregion

                                #region Response Signer 
                                string signerFirstName = fields[23];
                                string signerLastName = fields[24];
                                #endregion


                                #endregion

                                #region Create Letter Objection
                                string unitId = phase.Project.UnitId;
                                string regulationNumber = "N/A";
                                if (phase.Project.CommentRuleId.HasValue)
                                {
                                    int regulationId = phase.Project.CommentRuleId.Value;
                                    regulationNumber = LookupManager.GetLookup<LookupCommentRule>()[regulationId].Name.Contains("218") ? "218" : "219";
                                }

                                var now = DateTime.Parse(fields[27]);
                                var thisYear = now.Year.ToString().Substring(2, 2);
                                var nextYear = (now.Year + 1).ToString().Substring(2, 2);

                                LetterObjection letterObjection = new LetterObjection
                                {
                                    FiscalYear = now.Month <= 9 ? thisYear : nextYear,
                                    Region = unitId.Substring(2, 2),
                                    Forest = unitId.Substring(4, 2),
                                    OutcomeId = null, //in file field[7] but not formated properly ... user entry
                                    MeetingInfo = meetingInfo,
                                    RegulationNumber = regulationNumber,
                                    ReviewStatusId = reviewedStatusId,
                                    SignerFirstName = signerFirstName,
                                    SignerLastName = signerLastName,
                                    ObjectionDecisionMakerId = null,
                                    ResponseDate = String.IsNullOrWhiteSpace(fields[25]) ?
                                        null :
                                        DateTime.Parse(fields[25]) as DateTime?,
                                    DmdId = fields[26]
                                };
                                #endregion

                                #region Create Authors
                                List<Author> authors = new List<Author>();
                                Author sender = new Author()
                                {
                                    FirstName = "Anon",
                                    LastName = "Anon",
                                    Sender = true
                                };
                                Organization senderOrg = null;
                                Author a2 = null;
                                Organization o2 = null;
                                Author a3 = null;
                                Organization o3 = null;

                                #region create sender
                                string firstName = fields[2];
                                string lastName = fields[3];
                                string orgName = fields[4];
                                if (!
                                    (String.IsNullOrWhiteSpace(firstName) &&
                                    String.IsNullOrWhiteSpace(lastName) &&
                                    String.IsNullOrWhiteSpace(orgName)))
                                {
                                    //create the first author
                                    if (String.IsNullOrWhiteSpace(firstName) && String.IsNullOrWhiteSpace(lastName))
                                    {
                                        firstName = "Anon";
                                        lastName = "Anon";
                                    }
                                    if (!String.IsNullOrWhiteSpace(orgName))
                                    {
                                        senderOrg = new Organization()
                                        {
                                            Name = orgName
                                        };
                                    }
                                    sender = new Author()
                                    {
                                        FirstName = firstName,
                                        LastName = lastName,
                                        Organization = senderOrg,
                                        Sender = true
                                    };
                                }
                                #endregion //Create Sender

                                #region create second author
                                firstName = fields[5];
                                lastName = fields[6];
                                orgName = fields[7];
                                if (!
                                    (String.IsNullOrWhiteSpace(firstName) &&
                                    String.IsNullOrWhiteSpace(lastName) &&
                                    String.IsNullOrWhiteSpace(orgName)))
                                {
                                    //create the first author
                                    if (String.IsNullOrWhiteSpace(firstName) && String.IsNullOrWhiteSpace(lastName))
                                    {
                                        firstName = "Anon";
                                        lastName = "Anon";
                                    }
                                    if (!String.IsNullOrWhiteSpace(orgName))
                                    {
                                        o2 = new Organization()
                                        {
                                            Name = orgName
                                        };
                                    }
                                    a2 = new Author()
                                    {
                                        FirstName = firstName,
                                        LastName = lastName,
                                        Organization = o2,
                                        Sender = false
                                    };
                                }
                                #endregion //Create Second Author

                                #region create Third author
                                firstName = fields[8];
                                lastName = fields[9];
                                orgName = fields[10];
                                if (!
                                    (String.IsNullOrWhiteSpace(firstName) &&
                                    String.IsNullOrWhiteSpace(lastName) &&
                                    String.IsNullOrWhiteSpace(orgName)))
                                {
                                    //create the first author
                                    if (String.IsNullOrWhiteSpace(firstName) && String.IsNullOrWhiteSpace(lastName))
                                    {
                                        firstName = "Anon";
                                        lastName = "Anon";
                                    }
                                    if (!String.IsNullOrWhiteSpace(orgName))
                                    {
                                        o3 = new Organization()
                                        {
                                            Name = orgName
                                        };
                                    }
                                    a3 = new Author()
                                    {
                                        FirstName = firstName,
                                        LastName = lastName,
                                        Organization = o3,
                                        Sender = false
                                    };
                                }
                                #endregion //Create Second Author

                                authors.Add(sender);
                                if (a2 != null)
                                {
                                    authors.Add(a2);
                                }
                                if (a3 != null)
                                {
                                    authors.Add(a3);
                                }
                                #endregion //Create Authors

                                #region Create Letter
                                Letter letter = new Letter()
                                {
                                    Authors = authors,
                                    LetterStatusId = letterStatusId,
                                    Text = fields[12],
                                    CodedText = fields[12],
                                    OriginalText = fields[12],
                                    WithinCommentPeriod = true,
                                    LetterTypeId = 2, //unique
                                    DateSubmitted = DateTime.Parse(fields[27]),
                                    DateEntered = DateTime.UtcNow,
                                    LastUpdated = DateTime.Parse(fields[29]),
                                    LastUpdatedBy = fields[28],
                                    PublishToReadingRoom = true,
                                    LetterObjection = letterObjection,
                                    PhaseId = phase.PhaseId,
                                    DmdId = fields[13]
                                };
                                LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>();
                                letterManager.UnitOfWork = phaseManager.UnitOfWork;
                                letterManager.FixLetterText(letter);
                                #endregion

                                phase.Letters.Add(letter);
                                phaseManager.UnitOfWork.Save();

                                //add letter to list to remove from "send to DMD" queue
                                letterIds.Add(letter.LetterId);

                                #region send objection to datamart
                                var updatedObjection = new Objection
                                {
                                    ObjectionId = letterObjection.LetterObjectionId,
                                    Objector = letter.Sender.FullNameLastFirst,
                                    PalsId = int.Parse(projectNumber),
                                    ProjectType = "nepa",
                                    ResponseDate = letterObjection.ResponseDate.ToString(),
                                    CommentRule = letterObjection.RegulationNumber,
                                    DmdId = letterObjection.DmdId
                                };
                                var dmManager = new DataMartManager();
                                bool datamartObjectionResult = dmManager.AddObjection(updatedObjection);

                                DmdManager dmdManager = new DmdManager();

                                // Publish the file
                                dmdManager.PublishFile(letterObjection.DmdId);
                                #endregion //send objection to datamart


                            } // using phase manager
                        } // using project manager
                    }// while unread text in csv
                }// using text parser

                // Remove the entries from the queue

                // Create a reference to the queue
                string queueName;
                var sqs = SqsHelper.GetQueue(queueNameKey, out queueName);
                CreateQueueRequest sqsRequest = new CreateQueueRequest { QueueName = queueName };
                CreateQueueResponse createQueueResponse = sqs.CreateQueue(sqsRequest);
                string queueUrl = createQueueResponse.QueueUrl;


                //Get the messages from the queue
                ReceiveMessageRequest receiveMessageRequest =
                    new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = 10 };
                var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

                #region make a list of messages to delete
                List<DeleteMessageBatchRequestEntry> dmdSendEntries = new List<DeleteMessageBatchRequestEntry>();

                if (receiveMessageResponse.Messages != null)
                {
                    foreach (Message message in receiveMessageResponse.Messages)
                    {
                        string[] body = message.Body.Split(",".ToCharArray());
                        if (body.Count() > 1)
                        {
                            int bodyLetterId;
                            if (int.TryParse(body[1], out bodyLetterId))
                            {
                                if (letterIds.Contains(bodyLetterId))
                                {
                                    dmdSendEntries.Add
                                        (new DeleteMessageBatchRequestEntry 
                                        { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                                }
                            }
                        }
                    }
                }
                #endregion

                var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmdSendEntries };
                sqs.DeleteMessageBatch(deleteRequest);
            }// try
            catch (Exception ex)
            {
                logger.Error(String.Format("Error occurred in Objection Migrater processing row {0}", currentRow), ex);
            }
            logger.InfoFormat("Finished");
            Console.ReadLine();
        }

        public static Project CreateProject(string projectNumber, ProjectManager projectManager, string lastUpdatedBy)
        {
            Project caraProject = null;
            var dmManager = new DataMartManager();
            dmManager.ProjectManager = projectManager;

            caraProject = dmManager.GetProject(int.Parse(projectNumber));

            caraProject.Deleted = false;
            caraProject.LastUpdated = DateTime.UtcNow;
            if (caraProject.CreateDate.Year == 1)
            {
                caraProject.CreateDate = caraProject.LastUpdated;
            }
            caraProject.LastUpdatedBy = lastUpdatedBy;

            // save the CARA project
            dmManager.ProjectManager.Add(caraProject);

            dmManager.ProjectManager.UnitOfWork.Save();
            int projectId = caraProject.ProjectId;

            // link the project to PALS project
            if (!dmManager.LinkProject(caraProject.ProjectId, int.Parse(caraProject.ProjectNumber)))
            {
                logger.ErrorFormat("An error occurred while linking project {0} to Pals.",
                        caraProject.ProjectNumber);
            }

            dmManager.Dispose();
            return caraProject;
        }

        public static Phase CreateObjection(ProjectManager projectManager, Project caraProject, string[] fields)
        {
            string lastUpdatedBy = fields[28];
            Phase retVal = null;

            Phase phase = null;
            var dmManager = new DataMartManager();

            try
            {
                // save new project and new phase
                dmManager.ProjectManager = projectManager;


                int palsId = int.Parse(caraProject.ProjectNumber);

                #region Create the new Phase
                var milestones = dmManager.GetProjectMilestones(palsId);
                var milestoneStartDate = milestones.FirstOrDefault(x => x.MilestoneType == Milestone.OBJECTION_START_DATE);
                if (milestoneStartDate == null)
                {
                    logger.ErrorFormat("Project {0} does not have a known Estimated or Actual Objection Period Legal notice date",
                        palsId);
                    throw new ApplicationException();
                }

                var commentStartDate = DateTime.Parse(fields[25]);

                // Compute the end date
                var lkuPhaseMinLength = LookupManager.GetLookup<LookupPhaseMinimumLength>();
                var pml = lkuPhaseMinLength.GetValue(caraProject.AnalysisTypeId, caraProject.CommentRuleId.Value);

                if (!pml.ObjectionPeriodMinimumDays.HasValue || !pml.ObjectionDecisionDueDays.HasValue)
                {
                    logger.ErrorFormat("Project {0} does not have a defined Objection Period length and/or Objection Decision Due Date",
                        palsId);
                    throw new ApplicationException();
                }

                var commentEndDate = commentStartDate.AddDays(pml.ObjectionPeriodMinimumDays.Value);
                var objectionResponseDue = commentEndDate.AddDays(pml.ObjectionDecisionDueDays.Value);

                using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                {
                    phase = new Phase
                    {
                        ProjectId = caraProject.ProjectId,
                        PhaseTypeId = 3, // Objection
                        AlwaysOpen = false,
                        Deleted = false,
                        CommentStart = commentStartDate,
                        CommentEnd = commentEndDate,
                        ObjectionResponseDue = objectionResponseDue,
                        ObjectionResponseExtendedBy = 0,
                        PublicCommentActive = false,
                        ReadingRoomActive = false,
                        Locked = false,
                        Private = false,
                        LastUpdated = DateTime.UtcNow,
                        LastUpdatedBy = lastUpdatedBy,
                        Name = commentPeriodName
                    };

                    if (!IsValidatePeriod(caraProject.ProjectId, phase.PhaseId, phase.CommentStart, phase.CommentEnd))
                    {
                        logger.ErrorFormat("This Objection period for Project {0} overlaps an existing comment period (Start Date: {1}, End Date: {2})",
                                palsId,
                                phase.CommentStart.ToString("d"),
                                phase.CommentEnd.ToString("d"));

                        throw new ApplicationException();
                    }

                    // TODO:  Add point of contact as a team member.  As of now, that person is 
                    //        mentioned by name in the Datamart and we need the person's shortname.
                    #region Add team members
                    // -------------------------------------------------
                    // Add users defined in the Datamart
                    // Primary Project Manager, Secondary Project Manager, DataEntryPerson, PointOfContact
                    // -------------------------------------------------
                    DataMartProjectFilter filter = new DataMartProjectFilter()
                        {
                            ProjectId = palsId.ToString()
                        };

                    var results = dmManager.FindProjects(filter);
                    var peopleToAdd = new List<string>();

                    // Add the current user
                    peopleToAdd.Add(lastUpdatedBy);

                    string primaryProjectManager = results.Projects[0].NepaInfo.PrimaryProjectManager;
                    if (!string.IsNullOrEmpty(primaryProjectManager) && !peopleToAdd.Contains(primaryProjectManager))
                    {
                        peopleToAdd.Add(primaryProjectManager);
                    }

                    string secondaryProjectManager = results.Projects[0].NepaInfo.SecondaryProjectManager;
                    if (!string.IsNullOrEmpty(secondaryProjectManager) && !peopleToAdd.Contains(secondaryProjectManager))
                    {
                        peopleToAdd.Add(secondaryProjectManager);
                    }

                    string dataEntryPerson = results.Projects[0].NepaInfo.DataEntryPerson;
                    if (!string.IsNullOrEmpty(dataEntryPerson) && !peopleToAdd.Contains(dataEntryPerson))
                    {
                        peopleToAdd.Add(dataEntryPerson);
                    }

                    foreach (var shortname in peopleToAdd)
                    {
                        using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                        {
                            dmManager.UserManager = userManager;

                            try
                            {
                                var userId = dmManager.GetUser(shortname).UserId;

                                phase.PhaseMemberRoles.Add(new PhaseMemberRole
                                {
                                    UserId = userId,
                                    CanRemove = true,
                                    Active = true,
                                    LastUpdated = DateTime.UtcNow,
                                    LastUpdatedBy = lastUpdatedBy,
                                    RoleId = 4,  // Team Member
                                    IsPointOfContact = false
                                });
                            }
                            catch (Exception ex)
                            {
                                string message = string.Format("An error occurred adding a team member '{0}' to an objection period: {1}", shortname, palsId);
                                logger.Error(message, ex);
                            }
                        }
                    }
                    #endregion Add team members

                    phaseManager.Add(phase);
                    phaseManager.UnitOfWork.Save();

                    // Add the Objection coding structure
                    phaseManager.InsertPhaseCodes(5 // Objection Template
                       , null, null, phase.PhaseId);

                    // link the new phase to PALS project //
                    if (!dmManager.LinkPhase(palsId, phase))
                    {
                        logger.ErrorFormat("An error occurred while linking Comment Period {0} to Pals",
                            phase.PhaseTypeDesc);
                    }

                }
                #endregion Create the new Phase
                retVal = phase;
            }
            catch (Exception ex)
            {
                logger.Error("Exception: ", ex);
            }
            return retVal;
        }

        public static bool IsValidatePeriod(int projectId, int phaseId, DateTime startDate, DateTime endDate)
        {
            bool result = true;

            // get the project
            Project project;
            IList<Phase> phases;
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                project = projectManager.Get(projectId);
                phases = project.Phases.ToList();
            }

            // check all existing phases to make sure the start/end date falls outside of the phase
            if (project != null && project.Phases != null)
            {
                foreach (Phase phase in phases.Where(x => x.PhaseId != phaseId))
                {
                    result = (result && ((startDate < phase.CommentStart && endDate < phase.CommentStart) ||
                        (startDate > phase.CommentEnd && endDate > phase.CommentEnd)));
                }
            }

            return result;
        }

        public static ILog InitializeLogger(object objectThatLogs)
        {
            var logger = LogManager.GetLogger(objectThatLogs.GetType());

            string log4netFile = ConfigurationManager.AppSettings["log4net.configfile"];

            FileInfo fi = null;
            if (!string.IsNullOrEmpty(log4netFile))
            {
                fi = new FileInfo(log4netFile);
            }

            if (fi != null && fi.Exists)
            {
                XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netFile));
            }
            else
            {
                logger.Warn("No valid log4net configuration file was found");
                BasicConfigurator.Configure();
            }

            return logger;
        }

        private static string GetConfigSetting(string key)
        {
            string returnValue = "";
            using (CaraSettingManager settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                CaraSetting setting = settingsManager.Get(key);
                if (setting != null)
                {
                    returnValue = setting.Value;
                }
                else
                {
                    throw new Exception(string.Format("The configuration setting {0} could not be found", key));
                }
            }
            return returnValue;
        }
    }
}
