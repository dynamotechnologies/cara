IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'carauser')
CREATE LOGIN [carauser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [carauser] FOR LOGIN [carauser]
GO
GRANT CONNECT TO [carauser]
