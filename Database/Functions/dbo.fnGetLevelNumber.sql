SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Latha Davuluri>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnGetLevelNumber]
(
	-- Add the parameters for the function here
	  @codeNumber VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @level INT
   	Declare @firstValue INT
	Declare @secondValue INT
	Declare @thirdValue INT
	Declare @fourthValue INT
	
	  SELECT @firstValue = SUBSTRING(@codeNumber,5,2),
             @secondValue = SUBSTRING(@codeNumber,7,2),
             @thirdValue = SUBSTRING(@codeNumber,10,1),
             @fourthValue = SUBSTRING(@codeNumber,11,1)

		
		IF (@firstValue = 0 AND @secondValue = 0 AND @thirdValue = 0 AND @fourthValue = 0)
		 Begin
			Select @level = 1
		 End
         ELSE IF (@firstValue > 0 AND @secondValue = 0 AND @thirdValue = 0 AND @fourthValue = 0)
		 Begin
			Select @level = 2
		 End
         ELSE If (@firstValue > 0 AND @secondValue > 0 AND @thirdValue = 0 AND @fourthValue = 0)
		 Begin
			Select @level = 3
		 End 
         ELSE If (@firstValue > 0 AND @secondValue > 0 AND @thirdValue > 0 AND @fourthValue = 0)
		 Begin
			Select @level = 4
		 End 
         ELSE If (@firstValue > 0 AND @secondValue > 0 AND @thirdValue > 0 AND @fourthValue > 0)
		 Begin
			Select @level = 5
		 End   
	

	-- Return the result of the function
	RETURN @level

END

GO
GRANT EXECUTE ON  [dbo].[fnGetLevelNumber] TO [carauser]
GO
