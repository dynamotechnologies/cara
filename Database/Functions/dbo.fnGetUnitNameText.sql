
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Returns a comma separated string of all units associated with a user
CREATE FUNCTION [dbo].[fnGetUnitNameText]
(
	@userId INT
)
RETURNS varchar(1000)
AS
BEGIN
	DECLARE @result varchar(1000)
	
	SELECT @result = coalesce(@result + '|', '') + r.Name + ' on ' + ut.Name
	FROM dbo.[User] u
		INNER JOIN dbo.UserSystemRole usr ON u.UserId = usr.UserId
		INNER JOIN dbo.Role r ON usr.RoleId = r.RoleId
		INNER JOIN dbo.Unit ut ON usr.UnitId = ut.UnitId
	WHERE u.UserId = @userId
		AND usr.UnitId != '0000'
		AND r.RoleId != 5
	ORDER BY ut.Name
	
	IF EXISTS (
		SELECT 1 
		FROM dbo.UserSystemRole
		WHERE UserId = @userId
			AND RoleId = 1
			AND UnitId = '0000')
	BEGIN
		IF @result IS NOT NULL
			SET @result = 'System Admin|' + @result
		ELSE
			SET @result = 'System Admin'
	END

	RETURN @result
END

GO


GRANT EXECUTE ON  [dbo].[fnGetUnitNameText] TO [carauser]
GO
