SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Returns a comma separated string of all comment codes associated with a comment
CREATE FUNCTION [dbo].[fnGetCommentCodeText]
(
	@commentId INT
)
RETURNS varchar(4000)
AS
BEGIN
	DECLARE @result varchar(4000)
	
	SELECT @result = coalesce(@result + '|', '') + pc.CodeNumber + ' ' + pc.CodeName
	FROM dbo.CommentCode cc
		INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
	WHERE cc.CommentId = @commentId
	ORDER BY pc.CodeNumber

	RETURN REPLACE(REPLACE(REPLACE(@result, '.0000.00', ''), '00.00', ''), '.00', '')
END
GO
GRANT EXECUTE ON  [dbo].[fnGetCommentCodeText] TO [carauser]
GO
