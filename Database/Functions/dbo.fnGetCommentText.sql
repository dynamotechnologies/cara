SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Returns a comma separated string of all comment text associated with a comment
CREATE FUNCTION [dbo].[fnGetCommentText]
(
	@commentId INT
)
RETURNS varchar(MAX)
AS
BEGIN
	DECLARE @result varchar(max)
	
	SELECT @result = coalesce(@result + '[...]', '') + cc.[Text]
	FROM dbo.CommentText cc
	WHERE cc.CommentId = @commentId

	-- limit the text to be the first 200 characters
	/*IF LEN(@result) > 200
	BEGIN
		SET @result = SUBSTRING(@result, 0, 200) + '...'
	END*/

	RETURN @result
END

GO
GRANT EXECUTE ON  [dbo].[fnGetCommentText] TO [carauser]
GO
