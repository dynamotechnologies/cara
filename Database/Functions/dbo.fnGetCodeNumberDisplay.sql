SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Returns a code number in display format (without padding zero's and dot's)
CREATE FUNCTION [dbo].[fnGetCodeNumberDisplay]
(
	@codeNumber VARCHAR(20)
)
RETURNS varchar(20)
AS
BEGIN
	RETURN REPLACE(REPLACE(REPLACE(@codeNumber, '.0000.00', ''), '00.00', ''), '.00', '')
END



GO
GRANT EXECUTE ON  [dbo].[fnGetCodeNumberDisplay] TO [carauser]
GO
