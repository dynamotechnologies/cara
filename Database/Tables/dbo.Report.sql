CREATE TABLE [dbo].[Report]
(
[ReportId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArtifactTypeId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Report] ADD
CONSTRAINT [FKArtifactTypeReport] FOREIGN KEY ([ArtifactTypeId]) REFERENCES [dbo].[ArtifactType] ([ArtifactTypeId])
GO
ALTER TABLE [dbo].[Report] ADD CONSTRAINT [PKReport] PRIMARY KEY CLUSTERED  ([ReportId]) ON [PRIMARY]
GO
