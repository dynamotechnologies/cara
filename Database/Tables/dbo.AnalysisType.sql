CREATE TABLE [dbo].[AnalysisType]
(
[AnalysisTypeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnalysisType] ADD CONSTRAINT [PKAnalysisType] PRIMARY KEY CLUSTERED  ([AnalysisTypeId]) ON [PRIMARY]
GO
