CREATE TABLE [dbo].[SecurityToken]
(
[SecurityTokenID] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[Token] [uniqueidentifier] NOT NULL CONSTRAINT [DFSecurityTokenToken] DEFAULT (newid()),
[CreationDate] [datetime2] NULL CONSTRAINT [DFSecurityTokenCreationDate] DEFAULT (getutcdate()),
[AuthenticationDate] [datetime2] NULL,
[LogoutDate] [datetime2] NULL,
[LogoutMethod] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

ALTER TABLE [dbo].[SecurityToken] ADD
CONSTRAINT [FKUserSecurityToken] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[SecurityToken] ADD CONSTRAINT [PKSecurityToken] PRIMARY KEY CLUSTERED  ([SecurityTokenID]) ON [PRIMARY]
GO
