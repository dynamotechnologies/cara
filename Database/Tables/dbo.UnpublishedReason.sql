CREATE TABLE [dbo].[UnpublishedReason]
(
[UnpublishedReasonId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TermListId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[UnpublishedReason] ADD
CONSTRAINT [FKUnpublishedReason_TermList] FOREIGN KEY ([TermListId]) REFERENCES [dbo].[TermList] ([TermListId])
GO
ALTER TABLE [dbo].[UnpublishedReason] ADD CONSTRAINT [PKUnpublishedReason] PRIMARY KEY CLUSTERED  ([UnpublishedReasonId]) ON [PRIMARY]
GO
