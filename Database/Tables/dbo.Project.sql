CREATE TABLE [dbo].[Project]
(
[ProjectId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[UnitId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectStatusId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime2] NOT NULL CONSTRAINT [DFProjectCreateDate] DEFAULT (getutcdate()),
[Description] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectTypeId] [int] NOT NULL,
[ProjectNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnalysisTypeId] [int] NOT NULL,
[SOPAUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PalsProjectManager1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommentRuleId] [int] NULL,
[ProjectDocumentId] [int] NULL,
[ReadingRoomText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TRIProjectNumber] ON [dbo].[Project]
    AFTER INSERT
AS
    BEGIN
		DECLARE @projectId INT
		DECLARE @projectTypeId INT
		
		SELECT @projectId = ProjectId, @projectTypeId = ProjectTypeId
		FROM INSERTED
		
		IF @projectTypeId = 2
		BEGIN
			UPDATE dbo.Project
				SET ProjectNumber = 'NP-' + CAST(@projectId AS VARCHAR(20))
			WHERE ProjectId = @projectId
		END
		
		ELSE IF @projectTypeId = 3
		
		BEGIN
			UPDATE dbo.Project
				SET ProjectNumber = 'ORMS-' + CAST(@projectId AS VARCHAR(20))
			WHERE ProjectId = @projectId
		END
    END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIUProjectAudit] ON [dbo].[Project]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO ProjectAudit     
        INSERT  INTO dbo.ProjectAudit
                ( ProjectId ,
                  UnitId ,
                  ProjectStatusId ,
                  Name ,
                  CreateDate ,
                  Description ,
                  ProjectManagerId ,
                  PalsProjectManager1 ,
                  ProjectTypeId ,
                  ProjectNumber ,
                  AnalysisTypeId ,
                  SOPAUser ,
                  ProjectDocumentId,
                  CommentRuleId ,
                  Deleted ,
                  LastUpdated ,
                  LastUpdatedBy
                )
                SELECT  ProjectId ,
                        UnitId ,
                        ProjectStatusId ,
                        Name ,
                        CreateDate ,
                        Description ,
                        NULL ,
                        PalsProjectManager1 ,
                        ProjectTypeId ,
                        ProjectNumber ,
                        AnalysisTypeId ,
                        SOPAUser ,
		                ProjectDocumentId,
                        CommentRuleId ,
                        Deleted ,
                        LastUpdated ,
                        LastUpdatedBy
                FROM    INSERTED
                
    END
GO
ALTER TABLE [dbo].[Project] ADD CONSTRAINT [PKProject] PRIMARY KEY CLUSTERED  ([ProjectId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDXProjectUnitId] ON [dbo].[Project] ([UnitId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Project] ADD CONSTRAINT [FKAnalysisTypeProject] FOREIGN KEY ([AnalysisTypeId]) REFERENCES [dbo].[AnalysisType] ([AnalysisTypeId])
GO
ALTER TABLE [dbo].[Project] ADD CONSTRAINT [FKCommentRuleProject] FOREIGN KEY ([CommentRuleId]) REFERENCES [dbo].[CommentRule] ([CommentRuleId])
GO
ALTER TABLE [dbo].[Project] ADD CONSTRAINT [FKProjectStatusProject] FOREIGN KEY ([ProjectStatusId]) REFERENCES [dbo].[ProjectStatus] ([ProjectStatusId])
GO
ALTER TABLE [dbo].[Project] ADD CONSTRAINT [FKProjectTypeProject] FOREIGN KEY ([ProjectTypeId]) REFERENCES [dbo].[ProjectType] ([ProjectTypeId])
GO
ALTER TABLE [dbo].[Project] ADD CONSTRAINT [FKUnitProject] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([UnitId])
GO
CREATE FULLTEXT INDEX ON [dbo].[Project] KEY INDEX [PKProject] ON [CARAProject]
GO
ALTER FULLTEXT INDEX ON [dbo].[Project] ADD ([Name] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[Project] ADD ([Description] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[Project] ADD ([ProjectNumber] LANGUAGE 1033)
GO
