CREATE TABLE [dbo].[PhaseArtifact]
(
[PhaseId] [int] NOT NULL,
[ArtifactTypeId] [int] NOT NULL,
[DmdId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseArtifact] ADD CONSTRAINT [PKPhaseArtifact] PRIMARY KEY CLUSTERED  ([PhaseId], [ArtifactTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseArtifact] ADD CONSTRAINT [FKArtifactTypePhaseArtifact] FOREIGN KEY ([ArtifactTypeId]) REFERENCES [dbo].[ArtifactType] ([ArtifactTypeId])
GO
ALTER TABLE [dbo].[PhaseArtifact] ADD CONSTRAINT [FKPhasePhaseArtifact] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId])
GO
