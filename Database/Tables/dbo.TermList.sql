CREATE TABLE [dbo].[TermList]
(
[TermListId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Required] [bit] NOT NULL,
[EarlyAction] [bit] NULL,
[HarmfulContent] [bit] NOT NULL CONSTRAINT [DF_TermList_HarmfulContent] DEFAULT ((0)),
[DefaultOn] [bit] NOT NULL CONSTRAINT [DF_TermList_Default] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TermList] ADD CONSTRAINT [PKTermList] PRIMARY KEY CLUSTERED  ([TermListId]) ON [PRIMARY]
GO
