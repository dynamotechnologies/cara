CREATE TABLE [dbo].[LetterTermConfirmation]
(
[LetterId] [int] NOT NULL,
[TermId] [int] NOT NULL,
[StatusId] [int] NULL,
[Annotation] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[LetterTermConfirmation] ADD
CONSTRAINT [FK_StatusLetterTermConfirmation] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[LetterTermConfirmationStatus] ([ConfirmationStatusId])
ALTER TABLE [dbo].[LetterTermConfirmation] ADD
CONSTRAINT [FKLetterLetterTermConfirm] FOREIGN KEY ([LetterId]) REFERENCES [dbo].[Letter] ([LetterId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LetterTermConfirmation] ADD CONSTRAINT [PKLetterTermConfirmation] PRIMARY KEY CLUSTERED  ([LetterId], [TermId]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LetterTermConfirmation] ADD CONSTRAINT [FKTermLetterTermConfirm] FOREIGN KEY ([TermId]) REFERENCES [dbo].[Term] ([TermId])
GO
