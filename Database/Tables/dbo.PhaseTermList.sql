CREATE TABLE [dbo].[PhaseTermList]
(
[TermListId] [int] NOT NULL,
[PhaseId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[PhaseTermList] ADD
CONSTRAINT [FKPhasePhaseTermList] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId]) ON DELETE CASCADE
ALTER TABLE [dbo].[PhaseTermList] ADD 
CONSTRAINT [PKPhaseTermList] PRIMARY KEY CLUSTERED  ([TermListId], [PhaseId]) ON [PRIMARY]

ALTER TABLE [dbo].[PhaseTermList] ADD
CONSTRAINT [FKTermListPhaseTermList] FOREIGN KEY ([TermListId]) REFERENCES [dbo].[TermList] ([TermListId])
GO
