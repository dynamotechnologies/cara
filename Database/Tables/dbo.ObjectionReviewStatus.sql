CREATE TABLE [dbo].[ObjectionReviewStatus]
(
[ObjectionReviewStatusId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ObjectionReviewStatus] ADD CONSTRAINT [PK_ObjectionReviewStatus] PRIMARY KEY CLUSTERED  ([ObjectionReviewStatusId]) ON [PRIMARY]
GO
