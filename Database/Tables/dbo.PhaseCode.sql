CREATE TABLE [dbo].[PhaseCode]
(
[PhaseCodeId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PhaseId] [int] NOT NULL,
[CodeCategoryId] [int] NOT NULL,
[CodeId] [int] NOT NULL,
[ParentCodeId] [int] NULL,
[CodeName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CanRemove] [bit] NOT NULL CONSTRAINT [DFPhaseCodeCanRemove] DEFAULT ('True'),
[UserId] [int] NULL
)
ALTER TABLE [dbo].[PhaseCode] ADD
CONSTRAINT [FK_PhaseCode_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIUPhaseCodeAudit] ON [dbo].[PhaseCode]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO PhaseCodeAudit      
        INSERT  INTO dbo.PhaseCodeAudit
                ( PhaseCodeId ,
                  PhaseId ,
                  CodeCategoryId ,
                  CodeId ,
                  ParentCodeId ,
                  CodeName ,
                  CodeNumber ,
                  CanRemove ,
                  UserId ,
                  LastUpdated
                )
                SELECT  PhaseCodeId ,
                        PhaseId ,
                        CodeCategoryId ,
                        CodeId ,
                        ParentCodeId ,
                        CodeName ,
                        CodeNumber ,
                        CanRemove ,
                        UserId ,
                        SYSDATETIME()
                FROM    INSERTED
    END
GO


ALTER TABLE [dbo].[PhaseCode] ADD
CONSTRAINT [FKPhasePhaseCode] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId]) ON DELETE CASCADE

ALTER TABLE [dbo].[PhaseCode] ADD
CONSTRAINT [FKCodeCategoryPhaseCode] FOREIGN KEY ([CodeCategoryId]) REFERENCES [dbo].[CodeCategory] ([CodeCategoryId])

GO
ALTER TABLE [dbo].[PhaseCode] ADD CONSTRAINT [PKPhaseCode] PRIMARY KEY CLUSTERED  ([PhaseCodeId]) ON [PRIMARY]
GO
