CREATE TABLE [dbo].[ProjectActivity]
(
[ProjectId] [int] NOT NULL,
[ActivityId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[ProjectActivity] ADD
CONSTRAINT [FKProjectProjectActivity] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]) ON DELETE CASCADE
ALTER TABLE [dbo].[ProjectActivity] ADD 
CONSTRAINT [PKProjectActivity] PRIMARY KEY CLUSTERED  ([ProjectId], [ActivityId]) ON [PRIMARY]
ALTER TABLE [dbo].[ProjectActivity] ADD
CONSTRAINT [FKActivityProjectActivity] FOREIGN KEY ([ActivityId]) REFERENCES [dbo].[Activity] ([ActivityId])

GO
