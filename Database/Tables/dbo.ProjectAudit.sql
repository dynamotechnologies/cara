CREATE TABLE [dbo].[ProjectAudit]
(
[ProjectId] [int] NULL,
[ProjectAuditId] [int] NOT NULL IDENTITY(1, 1),
[UnitId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectStatusId] [int] NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime2] NULL,
[Description] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectManagerId] [int] NULL,
[PalsProjectManager1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectTypeId] [int] NULL,
[ProjectNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnalysisTypeId] [int] NULL,
[SOPAUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommentRuleId] [int] NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProjectDocumentId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectAudit] ADD CONSTRAINT [PKProjectAudit] PRIMARY KEY CLUSTERED  ([ProjectAuditId]) ON [PRIMARY]
GO
