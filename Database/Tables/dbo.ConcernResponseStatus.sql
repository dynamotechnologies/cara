CREATE TABLE [dbo].[ConcernResponseStatus]
(
[ConcernResponseStatusId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [smallint] NULL
)
GO
ALTER TABLE [dbo].[ConcernResponseStatus] ADD CONSTRAINT [PKConcernResponseStatus] PRIMARY KEY CLUSTERED  ([ConcernResponseStatusId]) ON [PRIMARY]
GO
