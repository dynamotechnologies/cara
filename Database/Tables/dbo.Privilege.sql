CREATE TABLE [dbo].[Privilege]
(
[PrivilegeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Privilege] ADD CONSTRAINT [PKPrivilege] PRIMARY KEY CLUSTERED  ([PrivilegeId]) ON [PRIMARY]
GO
