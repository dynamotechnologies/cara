CREATE TABLE [dbo].[ProjectPurpose]
(
[ProjectId] [int] NOT NULL,
[PurposeId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[ProjectPurpose] ADD
CONSTRAINT [FKProjectProjectPurpose] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProjectPurpose] ADD CONSTRAINT [PKProjectPurpose] PRIMARY KEY CLUSTERED  ([ProjectId], [PurposeId]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProjectPurpose] ADD CONSTRAINT [FKPurposeProjectPurpose] FOREIGN KEY ([PurposeId]) REFERENCES [dbo].[Purpose] ([PurposeId])
GO
