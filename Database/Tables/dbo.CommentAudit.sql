CREATE TABLE [dbo].[CommentAudit]
(
[CommentAuditId] [int] NOT NULL IDENTITY(1, 1),
[CommentId] [int] NULL,
[LetterId] [int] NULL,
[ConcernResponseId] [int] NULL,
[IdentifiedOn] [datetime2] NULL,
[IdentifiedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoResponseReasonId] [int] NULL,
[NoResponseReasonOther] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Annotation] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SampleStatement] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentNumber] [int] NULL,
[UserId] [int] NULL,
[DateAssigned] [datetime2] NULL
)
GO
ALTER TABLE [dbo].[CommentAudit] ADD CONSTRAINT [PKCommentAudit] PRIMARY KEY CLUSTERED  ([CommentAuditId]) ON [PRIMARY]
GO
