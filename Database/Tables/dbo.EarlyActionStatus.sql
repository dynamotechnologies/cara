CREATE TABLE [dbo].[EarlyActionStatus]
(
[StatusId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF_EarlyActionStatus_Sequence] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EarlyActionStatus] ADD CONSTRAINT [PK_EarlyActionStatus] PRIMARY KEY CLUSTERED  ([StatusId])
GO
