CREATE TABLE [dbo].[LetterDocument]
(
[DocumentId] [int] NOT NULL,
[LetterId] [int] NOT NULL,
[Protected] [bit] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IDXLetterLetterId] ON [dbo].[LetterDocument] ([LetterId]) ON [PRIMARY]

ALTER TABLE [dbo].[LetterDocument] ADD
CONSTRAINT [FKDocumentLetterDocument] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[Document] ([DocumentId])

ALTER TABLE [dbo].[LetterDocument] ADD
CONSTRAINT [FKLetterLetterDocument] FOREIGN KEY ([LetterId]) REFERENCES [dbo].[Letter] ([LetterId]) ON DELETE CASCADE SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Create triggers for table LetterDocument

CREATE TRIGGER [dbo].[TRIDLetterDocumentSize]
  ON [dbo].[LetterDocument]
  AFTER DELETE, INSERT
  AS
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE l 
    SET l.Size = lettersize
    FROM dbo.Letter l
    LEFT OUTER JOIN (SELECT (ISNULL(f.Size,0) + ISNULL(l.SIZE,0)) AS lettersize, l.LetterId
                        FROM letter l 
                        LEFT OUTER JOIN dbo.LetterDocument ld
                            ON l.LetterId = ld.LetterId
                        LEFT OUTER JOIN dbo.Document d
                            ON ld.DocumentId = d.DocumentId
                        LEFT OUTER JOIN dbo.[File] f
                            ON d.FileId = f.FileId) ls
    on l.letterid = ls.LetterId
    WHERE l.letterid IN (SELECT letterid FROM INSERTED UNION SELECT letterid FROM DELETED)
GO


ALTER TABLE [dbo].[LetterDocument] ADD 
CONSTRAINT [PKLetterDocument] PRIMARY KEY CLUSTERED  ([DocumentId], [LetterId]) ON [PRIMARY]


GO
