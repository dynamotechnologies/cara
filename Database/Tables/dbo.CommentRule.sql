CREATE TABLE [dbo].[CommentRule]
(
[CommentRuleId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommentRule] ADD CONSTRAINT [PKCommentRule] PRIMARY KEY CLUSTERED  ([CommentRuleId]) ON [PRIMARY]
GO
