CREATE TABLE [dbo].[PhaseMemberRole]
(
[PhaseId] [int] NOT NULL,
[UserId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[Active] [bit] NOT NULL,
[CanRemove] [bit] NOT NULL CONSTRAINT [DFPhaseMemberRoleCanRemove] DEFAULT ('True'),
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsPointOfContact] [bit] NOT NULL CONSTRAINT [DF_PhaseMemberRole_IsPointOfContact] DEFAULT ((0))
) ON [PRIMARY]
ALTER TABLE [dbo].[PhaseMemberRole] ADD 
CONSTRAINT [PKPhaseMemberRole] PRIMARY KEY CLUSTERED  ([PhaseId], [UserId]) ON [PRIMARY]SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIUPhaseMemberRoleAudit] ON [dbo].[PhaseMemberRole]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO PhaseMemberRoleAudit     
        INSERT  INTO dbo.PhaseMemberRoleAudit
                ( PhaseId ,
                  UserId ,
                  RoleId ,
                  CanRemove ,
                  Active ,
                  LastUpdated ,
                  LastUpdatedBy,
                  IsPointOfContact
                )
                SELECT  PhaseId ,
                        UserId ,
                        RoleId ,
                        CanRemove ,
                        Active ,
                        LastUpdated ,
                        LastUpdatedBy,
                        IsPointOfContact
                FROM    INSERTED
                
    END
GO


ALTER TABLE [dbo].[PhaseMemberRole] ADD
CONSTRAINT [FKPhasePhaseMemberRole] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId]) ON DELETE CASCADE


ALTER TABLE [dbo].[PhaseMemberRole] ADD
CONSTRAINT [FKRolePhaseMemberRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([RoleId])
ALTER TABLE [dbo].[PhaseMemberRole] ADD
CONSTRAINT [FKUserPhaseMemberRole] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
