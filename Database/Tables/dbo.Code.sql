CREATE TABLE [dbo].[Code]
(
[CodeId] [int] NOT NULL IDENTITY(1, 1),
[ParentCodeId] [int] NULL,
[CodeCategoryId] [int] NOT NULL,
[Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Code] ADD
CONSTRAINT [FKCodeCategoryCode] FOREIGN KEY ([CodeCategoryId]) REFERENCES [dbo].[CodeCategory] ([CodeCategoryId])
ALTER TABLE [dbo].[Code] ADD
CONSTRAINT [FKCodeCode] FOREIGN KEY ([ParentCodeId]) REFERENCES [dbo].[Code] ([CodeId])
GO
ALTER TABLE [dbo].[Code] ADD CONSTRAINT [PKCode] PRIMARY KEY CLUSTERED  ([CodeId]) ON [PRIMARY]
GO
