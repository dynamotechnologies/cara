CREATE TABLE [dbo].[WebCommentPageInfo]
(
[WebCommentPageInfoId] [int] NOT NULL IDENTITY(1, 1),
[ProjectId] [int] NULL,
[PalsId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentRuleId] [int] NOT NULL,
[ContactName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewspaperOfRecord] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewspaperOfRecordUrl] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressStreet1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressStreet2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressCity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressState] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressZip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime2] NULL,
[LastUpdatedDate] [datetime2] NOT NULL,
[CommentEmail] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectUrl] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[WebCommentPageInfo] ADD
CONSTRAINT [FK_WebCommentPageInfo_Unit] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([UnitId])
ALTER TABLE [dbo].[WebCommentPageInfo] ADD
CONSTRAINT [FKProjectWebCommentPageInfo] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]) ON DELETE SET NULL
CREATE UNIQUE NONCLUSTERED INDEX [IDXWebCommentPagePalsId] ON [dbo].[WebCommentPageInfo] ([PalsId]) ON [PRIMARY]



ALTER TABLE [dbo].[WebCommentPageInfo] ADD
CONSTRAINT [FKCommentRuleWebCommentPage] FOREIGN KEY ([CommentRuleId]) REFERENCES [dbo].[CommentRule] ([CommentRuleId])
GO
ALTER TABLE [dbo].[WebCommentPageInfo] ADD CONSTRAINT [PKWebCommentPageInfo] PRIMARY KEY CLUSTERED  ([WebCommentPageInfoId]) ON [PRIMARY]
GO
