CREATE TABLE [dbo].[ProjectType]
(
[ProjectTypeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectType] ADD CONSTRAINT [PKProjectType] PRIMARY KEY CLUSTERED  ([ProjectTypeId]) ON [PRIMARY]
GO
