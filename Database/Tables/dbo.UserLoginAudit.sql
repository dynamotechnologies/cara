CREATE TABLE [dbo].[UserLoginAudit]
(
[UserLoginAuditId] [int] NOT NULL IDENTITY(1, 1),
[UserLoginId] [int] NULL,
[UserId] [int] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserLoginAudit] ADD CONSTRAINT [PKUserLoginAudit] PRIMARY KEY CLUSTERED  ([UserLoginAuditId]) ON [PRIMARY]
GO
