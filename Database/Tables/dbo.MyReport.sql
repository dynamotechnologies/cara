CREATE TABLE [dbo].[MyReport]
(
[MyReportId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NULL,
[Parameters] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime2] NULL,
[Private] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MyReport] ADD CONSTRAINT [PKMyReport] PRIMARY KEY CLUSTERED  ([MyReportId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MyReport] ADD CONSTRAINT [FKUserMyReport] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
