CREATE TABLE [dbo].[ObjectionOutcome]
(
[ObjectionOutcomeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ObjectionOutcome] ADD CONSTRAINT [PK_ObjectionOutcome] PRIMARY KEY CLUSTERED  ([ObjectionOutcomeId]) ON [PRIMARY]
GO
