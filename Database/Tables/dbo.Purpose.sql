CREATE TABLE [dbo].[Purpose]
(
[PurposeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Purpose__Code__19A2444E] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Purpose] ADD CONSTRAINT [PKPurpose] PRIMARY KEY CLUSTERED  ([PurposeId]) ON [PRIMARY]
GO
