CREATE TABLE [dbo].[RolePrivilege]
(
[RoleId] [int] NOT NULL,
[PrivilegeId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[RolePrivilege] ADD 
CONSTRAINT [PKRolePrivilege] PRIMARY KEY CLUSTERED  ([RoleId], [PrivilegeId]) ON [PRIMARY]
ALTER TABLE [dbo].[RolePrivilege] ADD
CONSTRAINT [FKPrivilegeRolePrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [dbo].[Privilege] ([PrivilegeId])
ALTER TABLE [dbo].[RolePrivilege] ADD
CONSTRAINT [FKRoleRolePrivilege] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([RoleId])
GO
