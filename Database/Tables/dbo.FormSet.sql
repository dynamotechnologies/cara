CREATE TABLE [dbo].[FormSet]
(
[FormSetId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PhaseId] [int] NOT NULL,
[MasterFormId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LetterCount] [int] NOT NULL CONSTRAINT [DF_FormSet_LetterCount] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormSet] ADD CONSTRAINT [PK_FormSet] PRIMARY KEY CLUSTERED  ([FormSetId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IXFormSet] ON [dbo].[FormSet] ([MasterFormId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormSet] ADD CONSTRAINT [FKFormSet_Letter] FOREIGN KEY ([MasterFormId]) REFERENCES [dbo].[Letter] ([LetterId])
GO
ALTER TABLE [dbo].[FormSet] ADD CONSTRAINT [FKFormSet_Phase] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId])
GO
