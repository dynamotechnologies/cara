CREATE TABLE [dbo].[Location]
(
[ProjectId] [int] NOT NULL,
[AreaId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AreaType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Location] ADD
CONSTRAINT [FKProjectLocation] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]) ON DELETE CASCADE

ALTER TABLE [dbo].[Location] ADD 
CONSTRAINT [PKLocation] PRIMARY KEY CLUSTERED  ([ProjectId], [AreaId]) ON [PRIMARY]
ALTER TABLE [dbo].[Location] ADD
CONSTRAINT [CCLocationAreaType] CHECK (([AreaType]='Unit' OR [AreaType]='County' OR [AreaType]='State'))






GO
