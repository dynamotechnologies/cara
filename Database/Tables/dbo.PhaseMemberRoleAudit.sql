CREATE TABLE [dbo].[PhaseMemberRoleAudit]
(
[PhaseMemberRoleAuditId] [int] NOT NULL IDENTITY(1, 1),
[PhaseId] [int] NULL,
[UserId] [int] NULL,
[RoleId] [int] NULL,
[CanRemove] [bit] NULL,
[Active] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsPointOfContact] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseMemberRoleAudit] ADD CONSTRAINT [PKPhaseMemberRoleAudit] PRIMARY KEY CLUSTERED  ([PhaseMemberRoleAuditId]) ON [PRIMARY]
GO
