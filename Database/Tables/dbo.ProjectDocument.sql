CREATE TABLE [dbo].[ProjectDocument]
(
[ProjectId] [int] NOT NULL,
[PhaseId] [int] NULL,
[DmdId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF_ProjectDocument_Sequence] DEFAULT ((0)),
[PrimarySequence] [int] NOT NULL CONSTRAINT [DF_ProjectDocument_PrimarySequence] DEFAULT ((0))
) ON [PRIMARY]
ALTER TABLE [dbo].[ProjectDocument] ADD 
CONSTRAINT [PKProjectDocument] PRIMARY KEY CLUSTERED  ([ProjectId], [DmdId]) ON [PRIMARY]

ALTER TABLE [dbo].[ProjectDocument] ADD
CONSTRAINT [FKPhaseProjectDocument] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId])

ALTER TABLE [dbo].[ProjectDocument] ADD
CONSTRAINT [FKProjectProjectDocument] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]) ON DELETE CASCADE




GO
