CREATE TABLE [dbo].[Author]
(
[AuthorId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sender] [bit] NOT NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prefix] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipPostal] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [int] NULL,
[ProvinceRegion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactMethod] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LetterId] [int] NOT NULL,
[OrganizationId] [int] NULL,
[OfficialRepresentativeTypeId] [int] NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IDXAuthorSenderLetterId] ON [dbo].[Author] ([Sender], [LetterId]) INCLUDE ([FirstName], [LastName], [OrganizationId]) ON [PRIMARY]

ALTER TABLE [dbo].[Author] ADD
CONSTRAINT [FKLetterAuthor] FOREIGN KEY ([LetterId]) REFERENCES [dbo].[Letter] ([LetterId]) ON DELETE CASCADE SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create TRIGGER [dbo].[TRIUAuthorAudit]
  ON [dbo].[Author]
  AFTER INSERT, UPDATE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    INSERT INTO dbo.AuthorAudit
            ( AuthorId ,
              Sender ,
              FirstName ,
              LastName ,
              MiddleName ,
              Prefix ,
              Suffix ,
              Title ,
              Address1 ,
              Address2 ,
              City ,
              StateId ,
              ZipPostal ,
              CountryId ,
              ProvinceRegion ,
              Email ,
              Phone ,
              ContactMethod ,
              LetterId ,
              OrganizationId ,
              OfficialRepresentativeTypeId ,
              Deleted ,
              LastUpdated ,
              LastUpdatedBy
            )            
    SELECT AuthorId ,
            Sender ,
            FirstName ,
            LastName ,
            MiddleName ,
            Prefix ,
            Suffix ,
            Title ,
            Address1 ,
            Address2 ,
            City ,
            StateId ,
            ZipPostal ,
            CountryId ,
            ProvinceRegion ,
            Email ,
            Phone ,
            ContactMethod ,
            LetterId ,
            OrganizationId ,
            OfficialRepresentativeTypeId ,
            Deleted ,
            LastUpdated ,
            LastUpdatedBy FROM INSERTED
       
END
GO

ALTER TABLE [dbo].[Author] ADD
CONSTRAINT [CCAuthorContactMethod] CHECK (([ContactMethod]='Mail' OR [ContactMethod]='Email' OR [ContactMethod] IS NULL))
ALTER TABLE [dbo].[Author] ADD
CONSTRAINT [FKOfficialRepresentativeTypeAuthor] FOREIGN KEY ([OfficialRepresentativeTypeId]) REFERENCES [dbo].[OfficialRepresentativeType] ([OfficialRepresentativeTypeId])

ALTER TABLE [dbo].[Author] ADD
CONSTRAINT [FKCountryAuthor] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryId])

ALTER TABLE [dbo].[Author] ADD
CONSTRAINT [FKOrganizationAuthor] FOREIGN KEY ([OrganizationId]) REFERENCES [dbo].[Organization] ([OrganizationId])
ALTER TABLE [dbo].[Author] ADD
CONSTRAINT [FKStateAuthor] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([StateId])
GO
ALTER TABLE [dbo].[Author] ADD CONSTRAINT [PKAuthor] PRIMARY KEY CLUSTERED  ([AuthorId]) ON [PRIMARY]
GO
