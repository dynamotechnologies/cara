CREATE TABLE [dbo].[PhaseMinimumLength]
(
[AnalysisTypeId] [int] NOT NULL,
[CommentRuleId] [int] NOT NULL,
[PhaseMinimumDays] [int] NULL,
[ObjectionPeriodMinimumDays] [int] NULL,
[ObjectionDecisionDueDays] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseMinimumLength] ADD CONSTRAINT [Key1] PRIMARY KEY CLUSTERED  ([AnalysisTypeId], [CommentRuleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseMinimumLength] ADD CONSTRAINT [FKAnalysisTypePhaseMinLen] FOREIGN KEY ([AnalysisTypeId]) REFERENCES [dbo].[AnalysisType] ([AnalysisTypeId])
GO
ALTER TABLE [dbo].[PhaseMinimumLength] ADD CONSTRAINT [FKCommentRulePhaseMinLen] FOREIGN KEY ([CommentRuleId]) REFERENCES [dbo].[CommentRule] ([CommentRuleId])
GO
