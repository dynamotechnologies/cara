CREATE TABLE [dbo].[ArtifactType]
(
[ArtifactTypeId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KeyFlag] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ArtifactType] ADD CONSTRAINT [PKArtifactType] PRIMARY KEY CLUSTERED  ([ArtifactTypeId]) ON [PRIMARY]
GO
