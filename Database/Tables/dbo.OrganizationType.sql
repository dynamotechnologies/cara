CREATE TABLE [dbo].[OrganizationType]
(
[OrganizationTypeId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentOrganizationTypeId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[OrganizationType] ADD
CONSTRAINT [FKOrganizationTypeOrgType] FOREIGN KEY ([ParentOrganizationTypeId]) REFERENCES [dbo].[OrganizationType] ([OrganizationTypeId])
GO
ALTER TABLE [dbo].[OrganizationType] ADD CONSTRAINT [PKOrganizationType] PRIMARY KEY CLUSTERED  ([OrganizationTypeId]) ON [PRIMARY]
GO
