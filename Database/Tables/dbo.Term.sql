CREATE TABLE [dbo].[Term]
(
[TermId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[TermListId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Term] ADD
CONSTRAINT [FK_Term_TermList] FOREIGN KEY ([TermListId]) REFERENCES [dbo].[TermList] ([TermListId])
GO
ALTER TABLE [dbo].[Term] ADD CONSTRAINT [PKTerm] PRIMARY KEY CLUSTERED  ([TermId]) ON [PRIMARY]
GO
