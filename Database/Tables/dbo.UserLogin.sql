CREATE TABLE [dbo].[UserLogin]
(
[UserLoginId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]CREATE TRIGGER [dbo].[TRIUUserLoginAudit] ON [dbo].[UserLogin]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO UserLoginAudit   
        INSERT  INTO dbo.UserLoginAudit
                ( UserLoginId ,
                  UserId ,
                  LastUpdated ,
                  LastUpdatedBy
                )
                SELECT  UserLoginId ,
                        UserId ,
                        LastUpdated ,
                        LastUpdatedBy
                FROM    INSERTED
                
    END 
GO

ALTER TABLE [dbo].[UserLogin] ADD
CONSTRAINT [FKUserUserLogin] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [PKUserLogin] PRIMARY KEY CLUSTERED  ([UserLoginId]) ON [PRIMARY]
GO
