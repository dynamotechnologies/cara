CREATE TABLE [dbo].[Statesp]
(
[StateId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Statesp] ADD CONSTRAINT [PKStateSp] PRIMARY KEY CLUSTERED  ([StateId])
GO
ALTER TABLE [dbo].[Statesp] ADD CONSTRAINT [FK_Statesp_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([StateId])
GO
