CREATE TABLE [dbo].[FormManagementChangeLog]
(
[LogId] [int] NOT NULL IDENTITY(1, 1),
[PhaseId] [int] NOT NULL,
[LetterNumber] [int] NOT NULL,
[Date] [datetime2] NOT NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[FormManagementChangeLog] ADD 
CONSTRAINT [PK_FormManagementChangeLog_1] PRIMARY KEY CLUSTERED  ([LogId]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FormManagementChangeLog] ADD CONSTRAINT [FK_FormManagementChangeLog_Phase] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId])
GO
