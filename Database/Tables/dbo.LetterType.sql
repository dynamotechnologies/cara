CREATE TABLE [dbo].[LetterType]
(
[LetterTypeId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LetterType] ADD CONSTRAINT [PKLetterType] PRIMARY KEY CLUSTERED  ([LetterTypeId]) ON [PRIMARY]
GO
