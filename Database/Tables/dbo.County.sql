CREATE TABLE [dbo].[County]
(
[CountyId] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[County] ADD
CONSTRAINT [FKStateCounty] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([StateId])
GO
ALTER TABLE [dbo].[County] ADD CONSTRAINT [PKCounty] PRIMARY KEY CLUSTERED  ([CountyId]) ON [PRIMARY]
GO
