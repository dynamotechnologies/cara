CREATE TABLE [dbo].[CodingTemplate]
(
[CodingTemplateId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodingTemplate] ADD CONSTRAINT [PKCodingTemplate] PRIMARY KEY CLUSTERED  ([CodingTemplateId]) ON [PRIMARY]
GO
