CREATE TABLE [dbo].[PhaseAudit]
(
[PhaseAuditId] [int] NOT NULL IDENTITY(1, 1),
[PhaseId] [int] NULL,
[ProjectId] [int] NULL,
[PhaseTypeId] [int] NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locked] [bit] NULL,
[Private] [bit] NULL,
[CommentStart] [datetime2] NULL,
[CommentEnd] [datetime2] NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReadingRoomActive] [bit] NULL,
[PublicCommentActive] [bit] NULL,
[AlwaysOpen] [bit] NULL,
[ObjectionResponseDue] [datetime2] NULL,
[ObjectionResponseExtendedBy] [int] NULL,
[ConcernTemplate] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseTemplate] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PublicFormText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectionResponseExtendedJustification] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseAudit] ADD CONSTRAINT [PKPhaseAudit] PRIMARY KEY CLUSTERED  ([PhaseAuditId]) ON [PRIMARY]
GO
