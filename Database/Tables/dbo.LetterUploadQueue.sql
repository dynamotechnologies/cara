CREATE TABLE [dbo].[LetterUploadQueue]
(
[LetterUploadQueueId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[PhaseId] [int] NOT NULL,
[StoredFilename] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Filename] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileSize] [int] NOT NULL,
[UploadDate] [datetime] NOT NULL CONSTRAINT [DF_LetterUploadQueue_UploadDate] DEFAULT (getutcdate()),
[ProcessDate] [datetime] NULL,
[Status] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LetterCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LetterUploadQueue] ADD CONSTRAINT [PK_LetterUploadQueue] PRIMARY KEY CLUSTERED  ([LetterUploadQueueId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LetterUploadQueue] ADD CONSTRAINT [FK_LetterUploadQueue_Phase] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId])
GO
ALTER TABLE [dbo].[LetterUploadQueue] ADD CONSTRAINT [FK_LetterUploadQueue_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
