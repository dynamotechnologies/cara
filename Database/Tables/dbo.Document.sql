CREATE TABLE [dbo].[Document]
(
[DocumentId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileId] [int] NULL,
[DmdId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[Document] ADD
CONSTRAINT [FKFileDocument] FOREIGN KEY ([FileId]) REFERENCES [dbo].[File] ([FileId])
GO
ALTER TABLE [dbo].[Document] ADD CONSTRAINT [PKDocument] PRIMARY KEY CLUSTERED  ([DocumentId]) ON [PRIMARY]
GO
