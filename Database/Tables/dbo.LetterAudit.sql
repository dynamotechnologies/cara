CREATE TABLE [dbo].[LetterAudit]
(
[LetterAuditId] [int] NOT NULL IDENTITY(1, 1),
[LetterId] [int] NULL,
[PhaseId] [int] NULL,
[LetterStatusId] [int] NULL,
[LetterSequence] [int] NULL,
[Text] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodedText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WithinCommentPeriod] [bit] NULL,
[LetterTypeId] [int] NULL,
[CommonInterestClassId] [int] NULL,
[Size] [int] NULL,
[DateSubmitted] [datetime2] NULL,
[DateEntered] [datetime2] NULL,
[DeliveryTypeId] [int] NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentSequence] [int] NULL,
[FormSetId] [int] NULL,
[PublishToReadingRoom] [bit] NULL,
[DmdId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnpublishedReasonId] [int] NULL,
[UnpublishedReasonOther] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Protected] [bit] NULL,
[EarlyActionStatusId] [int] NULL,
[OriginalText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MasterDuplicateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[LetterAudit] ADD CONSTRAINT [PKLetterAudit] PRIMARY KEY CLUSTERED  ([LetterAuditId]) ON [PRIMARY]
GO
