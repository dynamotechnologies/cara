CREATE TABLE [dbo].[CommentCode]
(
[CommentId] [int] NOT NULL,
[PhaseCodeId] [int] NOT NULL,
[Sequence] [int] NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRICommentCode]
ON  [dbo].[CommentCode]
AFTER INSERT
AS 
BEGIN   
SET NOCOUNT ON;

DECLARE @commentID AS INT, @phaseCodeID AS INT, @sequence AS INT

SELECT @commentID = [CommentId] , @phaseCodeID = [PhaseCodeId]  FROM inserted 
SELECT @sequence = max ([Sequence]) from [dbo].[CommentCode] where [CommentId] = @commentID

UPDATE [dbo].[CommentCode] 
SET  [Sequence] = @sequence + 1 
WHERE  [CommentId]=  @commentID AND [PhaseCodeId]= @phaseCodeID

END


GO





SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- Create triggers for table Comment

CREATE TRIGGER [dbo].[TRDCommentCodeCanRemove]
  ON [dbo].[CommentCode]
  AFTER DELETE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @DeletePhaseCodes TABLE ( PhaseCodeId INT, Active BIT NULL )
    
    INSERT INTO @DeletePhaseCodes (PhaseCodeId, Active)
    SELECT PhaseCodeId, 1
    FROM DELETED
    
    UPDATE @DeletePhaseCodes
    SET Active = 0
    WHERE PhaseCodeId NOT IN (SELECT PhaseCodeId FROM dbo.ConcernResponse)
		AND PhaseCodeId NOT IN (SELECT PhaseCodeId FROM dbo.CommentCode)
    
	UPDATE dbo.PhaseCode
	SET CanRemove = 'True'
	WHERE PhaseCodeId IN (SELECT PhaseCodeId FROM @DeletePhaseCodes WHERE Active = 0)
END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[TRDCommentCodePhaseDashboard]
   ON  [dbo].[CommentCode]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 1)
	BEGIN
		WITH CodeCat_1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 1
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_1 = CodeCategory_1 - a.Total
		FROM CodeCat_1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 2)
	BEGIN
		WITH CodeCat_2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 2
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_2 = CodeCategory_2 - a.Total
		FROM CodeCat_2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 3)
	BEGIN
		WITH CodeCat_3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 3
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_3 = CodeCategory_3 - a.Total
		FROM CodeCat_3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 4)
	BEGIN
		WITH CodeCat_4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 4
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_4 = CodeCategory_4 - a.Total
		FROM CodeCat_4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 5)
	BEGIN
		WITH CodeCat_5 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 5
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_5 = CodeCategory_5 - a.Total
		FROM CodeCat_5 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 6)
	BEGIN
		WITH CodeCat_6 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 6
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_6 = CodeCategory_6 - a.Total
		FROM CodeCat_6 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 7)
	BEGIN
		WITH CodeCat_7 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 7
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_7 = CodeCategory_7 - a.Total
		FROM CodeCat_7 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 8)
	BEGIN
		WITH CodeCat_8 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 8
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_8 = CodeCategory_8 - a.Total
		FROM CodeCat_8 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE pc.CodeCategoryId = 9)
	BEGIN
		WITH CodeCat_9 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM DELETED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 9
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_9 = CodeCategory_9 - a.Total
		FROM CodeCat_9 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
END


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[TRICommentCodePhaseDashboard]
   ON  [dbo].[CommentCode]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 1)
	BEGIN
		WITH CodeCat_1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 1
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_1 = CodeCategory_1 + a.Total
		FROM CodeCat_1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 2)
	BEGIN
		WITH CodeCat_2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 2
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_2 = CodeCategory_2 + a.Total
		FROM CodeCat_2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 3)
	BEGIN
		WITH CodeCat_3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 3
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_3 = CodeCategory_3 + a.Total
		FROM CodeCat_3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 4)
	BEGIN
		WITH CodeCat_4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 4
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_4 = CodeCategory_4 + a.Total
		FROM CodeCat_4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 5)
	BEGIN
		WITH CodeCat_5 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 5
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_5 = CodeCategory_5 + a.Total
		FROM CodeCat_5 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 6)
	BEGIN
		WITH CodeCat_6 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 6
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_6 = CodeCategory_6 + a.Total
		FROM CodeCat_6 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 7)
	BEGIN
		WITH CodeCat_7 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 7
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_7 = CodeCategory_7 + a.Total
		FROM CodeCat_7 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 8)
	BEGIN
		WITH CodeCat_8 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 8
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_8 = CodeCategory_8 + a.Total
		FROM CodeCat_8 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM INSERTED i INNER JOIN PhaseCode pc ON i.PhaseCodeId = pc.PhaseCodeId WHERE CodeCategoryId = 9)
	BEGIN
		WITH CodeCat_9 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
			(
				SELECT DISTINCT pc.PhaseId, cc.CommentId, pc.CodeCategoryId
				FROM INSERTED cc
					INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
				WHERE pc.CodeCategoryId = 9
			) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET CodeCategory_9 = CodeCategory_9 + a.Total
		FROM CodeCat_9 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
END


GO




SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Create triggers for table Comment

CREATE TRIGGER [dbo].[TRIUCommentCodeCanRemove]
  ON [dbo].[CommentCode]
  AFTER INSERT, UPDATE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE dbo.PhaseCode
    SET CanRemove = 'False'
    WHERE PhaseCodeId IN
    (SELECT PhaseCodeId FROM INSERTED)
END

GO


ALTER TABLE [dbo].[CommentCode] ADD
CONSTRAINT [FKCommentCommentCode] FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId]) ON DELETE CASCADE
GO



ALTER TABLE [dbo].[CommentCode] ADD 
CONSTRAINT [PKCommentCode] PRIMARY KEY CLUSTERED  ([PhaseCodeId], [CommentId]) ON [PRIMARY]

ALTER TABLE [dbo].[CommentCode] ADD
CONSTRAINT [FKPhaseCodeCommentCode] FOREIGN KEY ([PhaseCodeId]) REFERENCES [dbo].[PhaseCode] ([PhaseCodeId])
GO
