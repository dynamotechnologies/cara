CREATE TABLE [dbo].[Letter]
(
[LetterId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PhaseId] [int] NOT NULL,
[LetterStatusId] [int] NOT NULL,
[Text] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodedText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WithinCommentPeriod] [bit] NOT NULL,
[LetterTypeId] [int] NOT NULL,
[DateSubmitted] [datetime2] NOT NULL,
[DateEntered] [datetime2] NOT NULL,
[DeliveryTypeId] [int] NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommonInterestClassId] [int] NULL,
[LetterSequence] [int] NULL,
[Size] [int] NULL,
[CommentSequence] [int] NULL CONSTRAINT [DFLetterCommentSequence] DEFAULT ((1)),
[FormSetId] [int] NULL,
[PublishToReadingRoom] [bit] NOT NULL CONSTRAINT [DF_Letter_PublishToReadingRoom] DEFAULT ((1)),
[DmdId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnpublishedReasonId] [int] NULL,
[UnpublishedReasonOther] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Protected] [bit] NULL,
[EarlyActionStatusId] [int] NULL,
[OriginalText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Confidence] [float] NULL,
[LetterObjectionId] [int] NULL,
[MasterDuplicateId] [int] NULL,
[IsLetterHtml] [bit] NOT NULL CONSTRAINT [DF_Letter_isCodedTextHTML] DEFAULT ((0)),
[HtmlText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HtmlOriginalText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HtmlCodedText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TRDLetterPhaseDashboard]
   ON  [dbo].[Letter]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterStatusId = 1)
	BEGIN
		WITH LtrStatus1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterStatusId = 1
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_1 = LetterStatus_1 - a.Total
		FROM LtrStatus1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterStatusId = 2)
	BEGIN
		WITH LtrStatus2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterStatusId = 2
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_2 = LetterStatus_2 - a.Total
		FROM LtrStatus2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterStatusId = 3)
	BEGIN
		WITH LtrStatus3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterStatusId = 3
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_3 = LetterStatus_3 - a.Total
		FROM LtrStatus3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END	

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 1)
	BEGIN
		WITH LtrType1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterTypeId = 1
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_1 = LetterType_1 - a.Total
		FROM LtrType1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 2)
	BEGIN
		WITH LtrType2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterTypeId = 2
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_2 = LetterType_2 - a.Total
		FROM LtrType2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 3)
	BEGIN
		WITH LtrType3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterTypeId = 3
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_3 = LetterType_3 - a.Total
		FROM LtrType3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 4)
	BEGIN
		WITH LtrType4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterTypeId = 4
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_4 = LetterType_4 + a.Total
		FROM LtrType4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 5)
	BEGIN
		WITH LtrType5 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterTypeId = 5
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_5 = LetterType_5 - a.Total
		FROM LtrType5 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 9)
	BEGIN
		WITH LtrType9 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE LetterTypeId = 9
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_9 = LetterType_9 - a.Total
		FROM LtrType9 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 1)
	BEGIN
		WITH ErlyActn1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE EarlyActionStatusId = 1
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_1 = EarlyActionStatus_1 - a.Total
		FROM ErlyActn1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 2)
	BEGIN
		WITH ErlyActn2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE EarlyActionStatusId = 2
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_2 = EarlyActionStatus_2 - a.Total
		FROM ErlyActn2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 3)
	BEGIN
		WITH ErlyActn3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE EarlyActionStatusId = 3
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_3 = EarlyActionStatus_3 - a.Total
		FROM ErlyActn3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 4)
	BEGIN
		WITH ErlyActn4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM DELETED
			WHERE EarlyActionStatusId = 4
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_4 = EarlyActionStatus_4 - a.Total
		FROM ErlyActn4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TRILetterPhaseDashboard]
   ON  [dbo].[Letter]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterStatusId = 1)
	BEGIN
		WITH LtrStatus1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterStatusId = 1
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_1 = LetterStatus_1 + a.Total
		FROM LtrStatus1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterStatusId = 2)
	BEGIN
		WITH LtrStatus2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterStatusId = 2
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_2 = LetterStatus_2 + a.Total
		FROM LtrStatus2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterStatusId = 3)
	BEGIN
		WITH LtrStatus3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterStatusId = 3
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_3 = LetterStatus_3 + a.Total
		FROM LtrStatus3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END	

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 1)
	BEGIN
		WITH LtrType1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterTypeId = 1
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_1 = LetterType_1 + a.Total
		FROM LtrType1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 2)
	BEGIN
		WITH LtrType2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterTypeId = 2
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_2 = LetterType_2 + a.Total
		FROM LtrType2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 3)
	BEGIN
		WITH LtrType3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterTypeId = 3
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_3 = LetterType_3 + a.Total
		FROM LtrType3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 4)
	BEGIN
		WITH LtrType4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterTypeId = 4
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_4 = LetterType_4 + a.Total
		FROM LtrType4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 5)
	BEGIN
		WITH LtrType5 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterTypeId = 5
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_5 = LetterType_5 + a.Total
		FROM LtrType5 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 9)
	BEGIN
		WITH LtrType9 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE LetterTypeId = 9
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_9 = LetterType_9 + a.Total
		FROM LtrType9 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 1)
	BEGIN
		WITH ErlyActn1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE EarlyActionStatusId = 1
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_1 = EarlyActionStatus_1 + a.Total
		FROM ErlyActn1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 2)
	BEGIN
		WITH ErlyActn2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE EarlyActionStatusId = 2
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_2 = EarlyActionStatus_2 + a.Total
		FROM ErlyActn2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 3)
	BEGIN
		WITH ErlyActn3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE EarlyActionStatusId = 3
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_3 = EarlyActionStatus_3 + a.Total
		FROM ErlyActn3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 4)
	BEGIN
		WITH ErlyActn4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM INSERTED
			WHERE EarlyActionStatusId = 4
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_4 = EarlyActionStatus_4 + a.Total
		FROM ErlyActn4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRILetterSequence]
   ON  [dbo].[Letter]
   AFTER INSERT
AS 
BEGIN
	
	DECLARE @LS INT;
	
	SET NOCOUNT ON;
	
	-----------------------------------
	-- Grab current sequence plus one
	-----------------------------------
	
	SELECT @LS = CurrentSequenceNumber + 1 
				FROM dbo.Sequence 
				WHERE PhaseId = (SELECT MAX(PhaseId) FROM INSERTED)
				AND Name = 'Letter Count';

	-----------------------------------
	-- Update sequence table
	-----------------------------------
					
	UPDATE dbo.Sequence
    SET CurrentSequenceNumber = @LS
    WHERE PhaseId = (SELECT MAX(PhaseId) FROM INSERTED)
    AND Name = 'Letter Count';

	-----------------------------------
	-- Update Letter to new sequence number
	-----------------------------------

    UPDATE dbo.Letter
    SET LetterSequence = @LS
    WHERE LetterId = (SELECT MAX(INSERTED.LetterId) FROM INSERTED)
    
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRIUDLetterCountFormSet]
   ON  [dbo].[Letter]
   AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @insertedCount INT
	DECLARE @deletedCount INT
	
	SELECT @insertedCount = COUNT(*) FROM INSERTED
	SELECT @deletedCount = COUNT(*) FROM DELETED
	
	-- INSERT
	IF (@insertedCount > 0 AND @deletedCount = 0)
	BEGIN
		WITH LtrCountByFormSet_I (FormSetId, Total)
		AS
		(
			SELECT FormSetId, COUNT(*)
			FROM INSERTED
			GROUP BY FormSetId
		)
		UPDATE dbo.FormSet SET LetterCount = LetterCount + a.Total
		FROM LtrCountByFormSet_I AS a
			 INNER JOIN dbo.FormSet pd ON a.FormSetId = pd.FormSetId
	END

	-- DELETE
	ELSE IF (@insertedCount = 0 AND @deletedCount > 0)
	BEGIN
		WITH LtrCountByFormSet_D (FormSetId, Total)
		AS
		(
			SELECT FormSetId, COUNT(*)
			FROM DELETED
			GROUP BY FormSetId
		)
		UPDATE dbo.FormSet SET LetterCount = LetterCount - a.Total
		FROM LtrCountByFormSet_D AS a
			 INNER JOIN dbo.FormSet pd ON a.FormSetId = pd.FormSetId
	END

	-- UPDATE
	ELSE IF (@insertedCount > 0 AND @deletedCount > 0)
	BEGIN
		WITH LtrCountByFormSet_U_Add (FormSetId, Total)
		AS
		(
			SELECT i.FormSetId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.FormSetId != d.FormSetId
			GROUP BY i.FormSetId
		)
		UPDATE dbo.FormSet SET LetterCount = LetterCount + a.Total
		FROM LtrCountByFormSet_U_Add AS a
			 INNER JOIN dbo.FormSet pd ON a.FormSetId = pd.FormSetId

		;
		WITH LtrCountByFormSet_U_Sub (FormSetId, Total)
		AS
		(
			SELECT d.FormSetId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.FormSetId != d.FormSetId
			GROUP BY d.FormSetId
		)
		UPDATE dbo.FormSet SET LetterCount = LetterCount - a.Total
		FROM LtrCountByFormSet_U_Sub AS a
			 INNER JOIN dbo.FormSet pd ON a.FormSetId = pd.FormSetId
	END
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRIULetterAudit] ON [dbo].[Letter]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO LetterAudit      
        INSERT  INTO dbo.LetterAudit
                ( LetterId ,
                  PhaseId ,
                  LetterStatusId ,
                  LetterSequence ,
                  Text ,
                  CodedText ,
                  WithinCommentPeriod ,
                  LetterTypeId ,
                  CommonInterestClassId ,
                  Size ,
                  DateSubmitted ,
                  DateEntered ,
                  DeliveryTypeId ,
                  Deleted ,
                  CommentSequence ,
                  LastUpdated ,
                  LastUpdatedBy,
                  FormSetId,
                  PublishToReadingRoom,
                  DmdId,
                  UnpublishedReasonId,
                  UnpublishedReasonOther,
                  Protected,
                  EarlyActionStatusId,
                  OriginalText,
                  MasterDuplicateId
                )
                SELECT  LetterId ,
                        PhaseId ,
                        LetterStatusId ,
                        LetterSequence ,
                        Text ,
                        CodedText ,
                        WithinCommentPeriod ,
                        LetterTypeId ,
                        CommonInterestClassId ,
                        Size ,
                        DateSubmitted ,
                        DateEntered ,
                        DeliveryTypeId ,
                        Deleted ,
                        CommentSequence ,
                        LastUpdated ,
                        LastUpdatedBy,
                        FormSetId,
				        PublishToReadingRoom,
		                DmdId,
		                UnpublishedReasonId,
		                UnpublishedReasonOther,
		                Protected,
		                EarlyActionStatusId,
		                OriginalText,
		                MasterDuplicateId
                FROM    INSERTED
                
    END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRIULetterCanRemove]
  ON [dbo].[Letter]
  AFTER INSERT, UPDATE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE pmr
    SET pmr.CanRemove = 'False'
    FROM dbo.PhaseMemberRole pmr, dbo.[User] u
    WHERE pmr.UserId = u.UserId
    and u.UserName IN (SELECT LastUpdatedBy FROM INSERTED)
       
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Create triggers for table Letter

CREATE TRIGGER [dbo].[TRIULetterSize]
  ON [dbo].[Letter]
  AFTER INSERT, UPDATE
  AS
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @FileSize INT
        
    -- Insert statements for trigger here
    SELECT @FileSize = ISNULL(SUM(f.Size), 0)
    FROM dbo.[FILE] f, dbo.Document d, dbo.LetterDocument ld
    WHERE f.fileid = d.fileid
    AND ld.documentid = d.documentid
    AND ld.letterid = (select MAX(INSERTED.letterid) FROM INSERTED)

    UPDATE dbo.Letter 
    SET [SIZE] = ISNULL(LEN((SELECT MAX(INSERTED.TEXT) FROM INSERTED)), 0) + @FileSize
    WHERE LetterId = (select MAX(INSERTED.letterid) FROM INSERTED)
       


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TRULetterPhaseDashboard]
   ON  [dbo].[Letter]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterStatusId = 1) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterStatusId = 1)
	BEGIN
		WITH LtrStatus1_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterStatusId = 1 AND d.LetterStatusId != 1
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_1 = LetterStatus_1 + a.Total
		FROM LtrStatus1_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrStatus1_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterStatusId != 1 AND d.LetterStatusId = 1
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_1 = LetterStatus_1 - a.Total
		FROM LtrStatus1_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterStatusId = 2) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterStatusId = 2)
	BEGIN
		WITH LtrStatus2_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterStatusId = 2 AND d.LetterStatusId != 2
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_2 = LetterStatus_2 + a.Total
		FROM LtrStatus2_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrStatus2_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterStatusId != 2 AND d.LetterStatusId = 2
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_2 = LetterStatus_2 - a.Total
		FROM LtrStatus2_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterStatusId = 3) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterStatusId = 3)
	BEGIN
		WITH LtrStatus3_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterStatusId = 3 AND d.LetterStatusId != 3
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_3 = LetterStatus_3 + a.Total
		FROM LtrStatus3_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrStatus3_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterStatusId != 3 AND d.LetterStatusId = 3
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterStatus_3 = LetterStatus_3 - a.Total
		FROM LtrStatus3_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 1) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 1)
	BEGIN
		WITH LtrType1_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId = 1 AND d.LetterTypeId != 1
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_1 = LetterType_1 + a.Total
		FROM LtrType1_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrType1_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId != 1 AND d.LetterTypeId = 1
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_1 = LetterType_1 - a.Total
		FROM LtrType1_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 2) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 2)
	BEGIN
		WITH LtrType2_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId = 2 AND d.LetterTypeId != 2
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_2 = LetterType_2 + a.Total
		FROM LtrType2_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrType2_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId != 2 AND d.LetterTypeId = 2
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_2 = LetterType_2 - a.Total
		FROM LtrType2_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 3) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 3)
	BEGIN
		WITH LtrType3_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId = 3 AND d.LetterTypeId != 3
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_3 = LetterType_3 + a.Total
		FROM LtrType3_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrType3_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId != 3 AND d.LetterTypeId = 3
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_3 = LetterType_3 - a.Total
		FROM LtrType3_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 4) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 4)
	BEGIN
		WITH LtrType4_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId = 4 AND d.LetterTypeId != 4
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_4 = LetterType_4 + a.Total
		FROM LtrType4_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrType4_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId != 4 AND d.LetterTypeId = 4
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_4 = LetterType_4 - a.Total
		FROM LtrType4_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 5) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 5)
	BEGIN
		WITH LtrType5_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId = 5 AND d.LetterTypeId != 5
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_5 = LetterType_5 + a.Total
		FROM LtrType5_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrType5_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId != 5 AND d.LetterTypeId = 5
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_5 = LetterType_5 - a.Total
		FROM LtrType5_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE LetterTypeId = 9) OR EXISTS (SELECT 1 FROM INSERTED WHERE LetterTypeId = 9)
	BEGIN
		WITH LtrType9_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId = 9 AND d.LetterTypeId != 9
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_9 = LetterType_9 + a.Total
		FROM LtrType9_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH LtrType9_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.LetterTypeId != 9 AND d.LetterTypeId = 9
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET LetterType_9 = LetterType_9 - a.Total
		FROM LtrType9_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 1) OR EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 9)
	BEGIN
		WITH ErlyActn1_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId = 1 AND d.EarlyActionStatusId != 1
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_1 = EarlyActionStatus_1 + a.Total
		FROM ErlyActn1_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH ErlyActn1_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId != 1 AND d.EarlyActionStatusId = 1
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_1 = EarlyActionStatus_1 - a.Total
		FROM ErlyActn1_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 2) OR EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 2)
	BEGIN
		WITH ErlyActn2_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId = 2 AND d.EarlyActionStatusId != 2
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_2 = EarlyActionStatus_2 + a.Total
		FROM ErlyActn2_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH ErlyActn2_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId != 2 AND d.EarlyActionStatusId = 2
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_2 = EarlyActionStatus_2 - a.Total
		FROM ErlyActn2_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 3) OR EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 3)
	BEGIN
		WITH ErlyActn3_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId = 3 AND d.EarlyActionStatusId != 3
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_3 = EarlyActionStatus_3 + a.Total
		FROM ErlyActn3_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH ErlyActn3_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId != 3 AND d.EarlyActionStatusId = 3
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_3 = EarlyActionStatus_3 - a.Total
		FROM ErlyActn3_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE EarlyActionStatusId = 4) OR EXISTS (SELECT 1 FROM INSERTED WHERE EarlyActionStatusId = 4)
	BEGIN
		WITH ErlyActn4_Add (PhaseId, Total)
		AS
		(
			SELECT i.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId = 4 AND d.EarlyActionStatusId != 4
			GROUP BY i.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_4 = EarlyActionStatus_4 + a.Total
		FROM ErlyActn4_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH ErlyActn4_Sub (PhaseId, Total)
		AS
		(
			SELECT d.PhaseId, COUNT(*)
			FROM DELETED d
				INNER JOIN INSERTED i ON i.LetterId = d.Letterid
			WHERE i.EarlyActionStatusId != 4 AND d.EarlyActionStatusId = 4
			GROUP BY d.PhaseId
		)
		UPDATE dbo.PhaseDashboard SET EarlyActionStatus_4 = EarlyActionStatus_4 - a.Total
		FROM ErlyActn4_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
END

GO
ALTER TABLE [dbo].[Letter] ADD CONSTRAINT [PKLetter] PRIMARY KEY CLUSTERED  ([LetterId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDXLetterDeletedLetterTypeId] ON [dbo].[Letter] ([Deleted], [LetterTypeId]) INCLUDE ([DateEntered], [DateSubmitted], [DmdId], [LetterId], [LetterSequence], [PhaseId], [PublishToReadingRoom], [Size], [UnpublishedReasonId], [UnpublishedReasonOther]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDXLetterFormSetId] ON [dbo].[Letter] ([FormSetId]) INCLUDE ([CodedText], [CommentSequence], [CommonInterestClassId], [DateEntered], [DateSubmitted], [Deleted], [DeliveryTypeId], [DmdId], [EarlyActionStatusId], [LastUpdated], [LastUpdatedBy], [LetterId], [LetterSequence], [LetterStatusId], [LetterTypeId], [OriginalText], [PhaseId], [Protected], [PublishToReadingRoom], [Size], [Text], [UnpublishedReasonId], [UnpublishedReasonOther], [WithinCommentPeriod]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDXLetterPhaseIdDeleted] ON [dbo].[Letter] ([PhaseId], [Deleted]) INCLUDE ([DateSubmitted], [EarlyActionStatusId], [LetterId], [LetterSequence], [LetterStatusId], [LetterTypeId], [Size], [WithinCommentPeriod]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDXLetterPhaseIdFormSetIdLetterSequenceLetterTypeId] ON [dbo].[Letter] ([PhaseId], [FormSetId], [LetterSequence], [LetterTypeId]) INCLUDE ([CodedText], [CommentSequence], [CommonInterestClassId], [DateEntered], [DateSubmitted], [Deleted], [DeliveryTypeId], [DmdId], [EarlyActionStatusId], [LastUpdated], [LastUpdatedBy], [LetterId], [LetterStatusId], [OriginalText], [Protected], [PublishToReadingRoom], [Size], [Text], [UnpublishedReasonId], [UnpublishedReasonOther], [WithinCommentPeriod]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Letter] ADD CONSTRAINT [FKLetter_FormSet] FOREIGN KEY ([FormSetId]) REFERENCES [dbo].[FormSet] ([FormSetId])
GO

ALTER TABLE [dbo].[Letter] ADD CONSTRAINT [FK_Letter_Letter] FOREIGN KEY ([MasterDuplicateId]) REFERENCES [dbo].[Letter] ([LetterId])
GO

CREATE FULLTEXT INDEX ON [dbo].[Letter] KEY INDEX [PKLetter] ON [CARAProject]
GO
ALTER FULLTEXT INDEX ON [dbo].[Letter] ADD ([Text] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[Letter] ADD ([CodedText] LANGUAGE 1033)
GO
