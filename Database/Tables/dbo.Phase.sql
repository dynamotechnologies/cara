CREATE TABLE [dbo].[Phase]
(
[PhaseId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ProjectId] [int] NOT NULL,
[PhaseTypeId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Locked] [bit] NOT NULL,
[Private] [bit] NOT NULL,
[CommentStart] [datetime2] NOT NULL,
[CommentEnd] [datetime2] NOT NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReadingRoomActive] [bit] NOT NULL CONSTRAINT [DF_Phase_ReadingRoomActive] DEFAULT ((1)),
[PublicCommentActive] [bit] NOT NULL CONSTRAINT [DF_Phase_PublicCommentActive] DEFAULT ((1)),
[AlwaysOpen] [bit] NOT NULL CONSTRAINT [DF_Phase_AlwaysOpen] DEFAULT ((0)),
[ObjectionResponseDue] [datetime2] NULL,
[ObjectionResponseExtendedBy] [int] NULL,
[ConcernTemplate] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseTemplate] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PublicFormText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectionResponseExtendedJustification] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressStreet1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressStreet2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressCity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressState] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailAddressZip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommentEmail] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRDNullPhaseProjectDocument]
   ON  dbo.Phase
   AFTER DELETE
AS 
BEGIN
	
	SET NOCOUNT ON;

    UPDATE dbo.ProjectDocument
    SET PhaseId = NULL
    WHERE PhaseId IN (SELECT PhaseId FROM DELETED)

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIPhaseCreateSequence]
   ON  dbo.Phase
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO dbo.Sequence
            ( PhaseId ,
              Name ,
              CurrentSequenceNumber
            )
    SELECT PhaseId, 'Letter Count', 0 FROM INSERTED

    INSERT INTO dbo.Sequence
            ( PhaseId ,
              Name ,
              CurrentSequenceNumber
            )
    SELECT PhaseId, 'Concern Response Count', 0 FROM INSERTED
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIPhaseDashboard]
   ON  dbo.Phase
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @phaseId INT
	SELECT @phaseId = PhaseId FROM INSERTED

    INSERT INTO dbo.PhaseDashboard (PhaseId)
    VALUES  (@phaseId)

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIUPhaseAudit] ON [dbo].[Phase]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO PhaseAudit    
        INSERT  INTO dbo.PhaseAudit
                ( PhaseId ,
                  ProjectId ,
                  PhaseTypeId ,
                  Name ,
                  Locked ,
                  Private ,
                  CommentStart ,
                  CommentEnd ,
                  Deleted ,
                  ReadingRoomActive ,
                  PublicCommentActive ,
                  LastUpdated ,
                  LastUpdatedBy,
                  AlwaysOpen,
                  ObjectionResponseDue,
                  ObjectionResponseExtendedBy,
                  ConcernTemplate,
                  ResponseTemplate,
                  PublicFormText,
                  ObjectionResponseExtendedJustification
                )
                SELECT  PhaseId ,
                        ProjectId ,
                        PhaseTypeId ,
                        Name ,
                        Locked ,
                        Private ,
                        CommentStart ,
                        CommentEnd ,
                        Deleted ,
                        ReadingRoomActive ,
                        PublicCommentActive ,
                        LastUpdated ,
                        LastUpdatedBy,
                        AlwaysOpen,
	                    ObjectionResponseDue,
			            ObjectionResponseExtendedBy,
						ConcernTemplate,
						ResponseTemplate,
						PublicFormText,
						ObjectionResponseExtendedJustification
                FROM    INSERTED
                
    END
GO
ALTER TABLE [dbo].[Phase] ADD CONSTRAINT [PKPhase] PRIMARY KEY CLUSTERED  ([PhaseId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Phase] ADD CONSTRAINT [FKPhaseTypePhase] FOREIGN KEY ([PhaseTypeId]) REFERENCES [dbo].[PhaseType] ([PhaseTypeId])
GO
ALTER TABLE [dbo].[Phase] ADD CONSTRAINT [FKProjectPhase] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]) ON DELETE CASCADE
GO
