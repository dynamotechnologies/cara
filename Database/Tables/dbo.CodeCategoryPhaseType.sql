CREATE TABLE [dbo].[CodeCategoryPhaseType]
(
[CodeCategoryId] [int] NOT NULL,
[PhaseTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodeCategoryPhaseType] ADD CONSTRAINT [PKCodeCategoryPhaseType] PRIMARY KEY CLUSTERED  ([CodeCategoryId], [PhaseTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodeCategoryPhaseType] ADD CONSTRAINT [FKCodeCategoryCCPT] FOREIGN KEY ([CodeCategoryId]) REFERENCES [dbo].[CodeCategory] ([CodeCategoryId])
GO
ALTER TABLE [dbo].[CodeCategoryPhaseType] ADD CONSTRAINT [FKPhaseTypeCCPT] FOREIGN KEY ([PhaseTypeId]) REFERENCES [dbo].[PhaseType] ([PhaseTypeId])
GO
