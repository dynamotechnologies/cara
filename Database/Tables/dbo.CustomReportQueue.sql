CREATE TABLE [dbo].[CustomReportQueue]
(
[CustomReportQueueId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[ReportDefinition] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomReportQueue] ADD CONSTRAINT [PK_CustomReportQueue] PRIMARY KEY CLUSTERED  ([CustomReportQueueId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomReportQueue] ADD CONSTRAINT [FK_CustomReportQueue_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
