CREATE TABLE [dbo].[UserSystemRole]
(
[UserId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[UnitId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_UserSystemRole_UnitId] DEFAULT ((1100))
) ON [PRIMARY]
ALTER TABLE [dbo].[UserSystemRole] ADD 
CONSTRAINT [PKUserSystemRole] PRIMARY KEY CLUSTERED  ([UserId], [RoleId], [UnitId]) ON [PRIMARY]
ALTER TABLE [dbo].[UserSystemRole] ADD
CONSTRAINT [FK_UserSystemRole_Unit] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([UnitId])

ALTER TABLE [dbo].[UserSystemRole] ADD
CONSTRAINT [FKRoleUserSystemRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([RoleId])
ALTER TABLE [dbo].[UserSystemRole] ADD
CONSTRAINT [FKUserUserSystemRole] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
