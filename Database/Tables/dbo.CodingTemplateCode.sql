CREATE TABLE [dbo].[CodingTemplateCode]
(
[CodeId] [int] NOT NULL,
[CodingTemplateId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[CodingTemplateCode] ADD 
CONSTRAINT [PKCodingTemplateCode] PRIMARY KEY CLUSTERED  ([CodeId], [CodingTemplateId]) ON [PRIMARY]
ALTER TABLE [dbo].[CodingTemplateCode] ADD
CONSTRAINT [FKCodeCodingTemplateCode] FOREIGN KEY ([CodeId]) REFERENCES [dbo].[Code] ([CodeId])
ALTER TABLE [dbo].[CodingTemplateCode] ADD
CONSTRAINT [FKCodingTemplateCodingTemplat] FOREIGN KEY ([CodingTemplateId]) REFERENCES [dbo].[CodingTemplate] ([CodingTemplateId])
GO
