CREATE TABLE [dbo].[CommentText]
(
[CommentTextId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Text] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[CommentText] ADD
CONSTRAINT [FKCommentCommentText] FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId]) ON DELETE CASCADE

GO
ALTER TABLE [dbo].[CommentText] ADD CONSTRAINT [PKCommentText] PRIMARY KEY CLUSTERED  ([CommentTextId]) ON [PRIMARY]
GO
