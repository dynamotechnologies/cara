CREATE TABLE [dbo].[NoResponseReason]
(
[NoResponseReasonId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NoResponseReason] ADD CONSTRAINT [PKNoResponseReason] PRIMARY KEY CLUSTERED  ([NoResponseReasonId]) ON [PRIMARY]
GO
