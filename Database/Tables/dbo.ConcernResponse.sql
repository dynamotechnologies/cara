CREATE TABLE [dbo].[ConcernResponse]
(
[ConcernResponseId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ConcernResponseStatusId] [int] NOT NULL,
[ConcernText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NULL,
[ResponseText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCreated] [datetime2] NULL,
[ResponseCreatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConcernCreated] [datetime2] NOT NULL,
[ConcernCreatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConcernResponseNumber] [int] NULL,
[CommentCount] [int] NULL,
[PhaseCodeId] [int] NULL,
[UserId] [int] NULL,
[DateAssigned] [datetime2] NULL,
[Label] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[ConcernResponse] ADD
CONSTRAINT [FK_ConcernResponse_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])CREATE TRIGGER [dbo].[TRDConcernResponseCodeCanRemove]
  ON [dbo].[ConcernResponse]
  AFTER DELETE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @DeletePhaseCodes TABLE ( PhaseCodeId INT, Active BIT NULL )
    
    INSERT INTO @DeletePhaseCodes (PhaseCodeId, Active)
    SELECT PhaseCodeId, 1
    FROM DELETED
    
    UPDATE @DeletePhaseCodes
    SET Active = 0
    WHERE PhaseCodeId NOT IN (SELECT PhaseCodeId FROM dbo.ConcernResponse)
		AND PhaseCodeId NOT IN (SELECT PhaseCodeId FROM dbo.CommentCode)
    
	UPDATE dbo.PhaseCode
	SET CanRemove = 'True'
	WHERE PhaseCodeId IN (SELECT PhaseCodeId FROM @DeletePhaseCodes WHERE Active = 0)
END
GO
CREATE TRIGGER [dbo].[TRIUConcernResponseCodeCanRemove]
  ON [dbo].[ConcernResponse]
  AFTER INSERT, UPDATE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE dbo.PhaseCode
    SET CanRemove = 'False'
    WHERE PhaseCodeId IN
    (SELECT PhaseCodeId FROM INSERTED)
END
GO

ALTER TABLE [dbo].[ConcernResponse] ADD
CONSTRAINT [FK_ConcernResponse_PhaseCode] FOREIGN KEY ([PhaseCodeId]) REFERENCES [dbo].[PhaseCode] ([PhaseCodeId])SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[TRDConcernResponsePhaseDashboard]
   ON  [dbo].[ConcernResponse]
   INSTEAD OF DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 1)
	BEGIN
		WITH CRStatus1 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
				(
					SELECT DISTINCT l.PhaseId, cr.ConcernResponseId, cr.ConcernResponseStatusId
					FROM DELETED cr
					INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
					INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
						WHERE cr.ConcernResponseStatusId = 1
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_1 = ConcernResponseStatus_1 - a.Total
		FROM CRStatus1 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 2)
	BEGIN
		WITH CRStatus2 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
				(
					SELECT DISTINCT l.PhaseId, cr.ConcernResponseId, cr.ConcernResponseStatusId
					FROM DELETED cr
					INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
					INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
						WHERE cr.ConcernResponseStatusId = 2
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_2 = ConcernResponseStatus_2 - a.Total
		FROM CRStatus2 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 3)
	BEGIN
		WITH CRStatus3 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
				(
					SELECT DISTINCT l.PhaseId, cr.ConcernResponseId, cr.ConcernResponseStatusId
					FROM DELETED cr
					INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
					INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
						WHERE cr.ConcernResponseStatusId = 3
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_3 = ConcernResponseStatus_3 - a.Total
		FROM CRStatus3 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 4)
	BEGIN
		WITH CRStatus4 (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM
				(
					SELECT DISTINCT l.PhaseId, cr.ConcernResponseId, cr.ConcernResponseStatusId
					FROM DELETED cr
					INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
					INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
						WHERE cr.ConcernResponseStatusId = 4
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_4 = ConcernResponseStatus_4 - a.Total
		FROM CRStatus4 AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	DELETE FROM dbo.ConcernResponse WHERE ConcernResponseId IN (SELECT ConcernResponseId FROM DELETED)
END


GO



CREATE TRIGGER [dbo].[TRUConcernResponsePhaseDashboard]
   ON  [dbo].[ConcernResponse]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 1) OR EXISTS (SELECT 1 FROM INSERTED WHERE ConcernResponseStatusId = 1)
	BEGIN
		WITH CRStatus1_Add (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, i.ConcernResponseId, i.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId = 1 AND d.ConcernResponseStatusId != 1
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_1 = ConcernResponseStatus_1 + a.Total
		FROM CRStatus1_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH CRStatus1_Sub (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, d.ConcernResponseId, d.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId != 1 AND d.ConcernResponseStatusId = 1
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_1 = ConcernResponseStatus_1 - a.Total
		FROM CRStatus1_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 2) OR EXISTS (SELECT 1 FROM INSERTED WHERE ConcernResponseStatusId = 2)
	BEGIN
		WITH CRStatus2_Add (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, i.ConcernResponseId, i.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId = 2 AND d.ConcernResponseStatusId != 2
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_2 = ConcernResponseStatus_2 + a.Total
		FROM CRStatus2_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH CRStatus2_Sub (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, d.ConcernResponseId, d.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId != 2 AND d.ConcernResponseStatusId = 2
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_2 = ConcernResponseStatus_2 - a.Total
		FROM CRStatus2_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 3) OR EXISTS (SELECT 1 FROM INSERTED WHERE ConcernResponseStatusId = 3)
	BEGIN
		WITH CRStatus3_Add (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, i.ConcernResponseId, i.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId = 3 AND d.ConcernResponseStatusId != 3
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_3 = ConcernResponseStatus_3 + a.Total
		FROM CRStatus3_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH CRStatus3_Sub (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, d.ConcernResponseId, d.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId != 3 AND d.ConcernResponseStatusId = 3
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_3 = ConcernResponseStatus_3 - a.Total
		FROM CRStatus3_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END
	
	IF EXISTS (SELECT 1 FROM DELETED WHERE ConcernResponseStatusId = 4) OR EXISTS (SELECT 1 FROM INSERTED WHERE ConcernResponseStatusId = 4)
	BEGIN
		WITH CRStatus4_Add (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, i.ConcernResponseId, i.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId = 4 AND d.ConcernResponseStatusId != 4
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_4 = ConcernResponseStatus_4 + a.Total
		FROM CRStatus4_Add AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;

		WITH CRStatus4_Sub (PhaseId, Total)
		AS
		(
			SELECT PhaseId, COUNT(*)
			FROM 				
				(
					SELECT DISTINCT l.PhaseId, d.ConcernResponseId, d.ConcernResponseStatusId
					FROM DELETED d
						INNER JOIN INSERTED i ON i.ConcernResponseId = d.ConcernResponseId
						INNER JOIN dbo.Comment c ON i.ConcernResponseId = c.ConcernResponseId
						INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
					WHERE i.ConcernResponseStatusId != 4 AND d.ConcernResponseStatusId = 4
				) t
			GROUP BY PhaseId
		)
		UPDATE dbo.PhaseDashboard SET ConcernResponseStatus_4 = ConcernResponseStatus_4 - a.Total
		FROM CRStatus4_Sub AS a
			 INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId;
	END

END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRIUConcernResponseAudit] ON [dbo].[ConcernResponse]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO ConcernResponseAudit      
        INSERT  INTO dbo.ConcernResponseAudit
                ( ConcernResponseId ,
                  ConcernResponseStatusId ,
                  ConcernText ,
                  Sequence ,
                  ResponseText ,
                  ResponseCreated ,
                  ResponseCreatedBy ,
                  ConcernCreated ,
                  ConcernCreatedBy ,
                  LastUpdated ,
                  LastUpdatedBy ,
                  ConcernResponseNumber,
                  PhaseCodeId ,
                  UserId ,
                  DateAssigned,
                  Label
                )
                SELECT  ConcernResponseId ,
                        ConcernResponseStatusId ,
                        ConcernText ,
                        Sequence ,
                        ResponseText ,
                        ResponseCreated ,
                        ResponseCreatedBy ,
                        ConcernCreated ,
                        ConcernCreatedBy ,
                        LastUpdated ,
                        LastUpdatedBy ,
                        ConcernResponseNumber,
                        PhaseCodeId ,
						UserId ,
						DateAssigned,
						Label
                FROM    INSERTED
    END
GO




CREATE FULLTEXT INDEX ON [dbo].[ConcernResponse] KEY INDEX [PKConcernResponse] ON [CARAProject]
GO

ALTER FULLTEXT INDEX ON [dbo].[ConcernResponse] ADD ([ConcernText] LANGUAGE 1033)
GO

ALTER FULLTEXT INDEX ON [dbo].[ConcernResponse] ADD ([ResponseText] LANGUAGE 1033)
GO

ALTER FULLTEXT INDEX ON [dbo].[ConcernResponse] ENABLE
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Create triggers for table ConcernResponse

CREATE TRIGGER [dbo].[TRIUConcernResponseCanRemove]
  ON [dbo].[ConcernResponse]
  AFTER INSERT, UPDATE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE pmr
    SET pmr.CanRemove = 'False'
    FROM dbo.PhaseMemberRole pmr, dbo.[User] u
    WHERE pmr.UserId = u.UserId
    and u.UserName IN (SELECT LastUpdatedBy FROM INSERTED)
       
END
GO


ALTER TABLE [dbo].[ConcernResponse] ADD
CONSTRAINT [FKConcernResponseStatusConcer] FOREIGN KEY ([ConcernResponseStatusId]) REFERENCES [dbo].[ConcernResponseStatus] ([ConcernResponseStatusId])
GO
ALTER TABLE [dbo].[ConcernResponse] ADD CONSTRAINT [PKConcernResponse] PRIMARY KEY CLUSTERED  ([ConcernResponseId]) ON [PRIMARY]
GO
