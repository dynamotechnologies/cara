CREATE TABLE [dbo].[ConcernResponseAudit]
(
[ConcernResponseAuditId] [int] NOT NULL IDENTITY(1, 1),
[ConcernResponseId] [int] NULL,
[ConcernResponseStatusId] [int] NULL,
[ConcernText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sequence] [int] NULL,
[ResponseText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCreated] [datetime2] NULL,
[ResponseCreatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConcernCreated] [datetime2] NULL,
[ConcernCreatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConcernResponseNumber] [int] NULL,
[PhaseCodeId] [int] NULL,
[UserId] [int] NULL,
[DateAssigned] [datetime2] NULL,
[Label] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConcernResponseAudit] ADD CONSTRAINT [PKConcernResponseAudit] PRIMARY KEY CLUSTERED  ([ConcernResponseAuditId]) ON [PRIMARY]
GO
