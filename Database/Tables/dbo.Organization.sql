CREATE TABLE [dbo].[Organization]
(
[OrganizationId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrganizationTypeId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Organization] ADD
CONSTRAINT [FKOrganizationTypeOrganization] FOREIGN KEY ([OrganizationTypeId]) REFERENCES [dbo].[OrganizationType] ([OrganizationTypeId])

GO
ALTER TABLE [dbo].[Organization] ADD CONSTRAINT [PKOrganization] PRIMARY KEY CLUSTERED  ([OrganizationId]) ON [PRIMARY]
GO
