CREATE TABLE [dbo].[AuthorAudit]
(
[AuthorAuditId] [int] NOT NULL IDENTITY(1, 1),
[AuthorId] [int] NULL,
[Sender] [bit] NULL,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prefix] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipPostal] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [int] NULL,
[ProvinceRegion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactMethod] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LetterId] [int] NULL,
[OrganizationId] [int] NULL,
[OfficialRepresentativeTypeId] [int] NULL,
[Deleted] [bit] NULL,
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuthorAudit] ADD CONSTRAINT [PKAuthorAudit] PRIMARY KEY CLUSTERED  ([AuthorAuditId]) ON [PRIMARY]
GO
