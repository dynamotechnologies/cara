CREATE TABLE [dbo].[PhaseDashboard]
(
[PhaseId] [int] NOT NULL,
[LetterStatus_1] [int] NOT NULL CONSTRAINT [DF_PhaseDashboard_LetterStatus_1] DEFAULT ((0)),
[LetterStatus_2] [int] NOT NULL CONSTRAINT [DF_PhaseDashboard_LetterStatus_2] DEFAULT ((0)),
[LetterStatus_3] [int] NOT NULL CONSTRAINT [DF_PhaseDashboard_LetterStatus_3] DEFAULT ((0)),
[LetterType_1] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Lette__2CB38214] DEFAULT ((0)),
[LetterType_2] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Lette__2DA7A64D] DEFAULT ((0)),
[LetterType_3] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Lette__2E9BCA86] DEFAULT ((0)),
[LetterType_4] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Lette__2F8FEEBF] DEFAULT ((0)),
[LetterType_5] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Lette__308412F8] DEFAULT ((0)),
[LetterType_9] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Lette__31783731] DEFAULT ((0)),
[EarlyActionStatus_1] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Early__326C5B6A] DEFAULT ((0)),
[EarlyActionStatus_2] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Early__33607FA3] DEFAULT ((0)),
[EarlyActionStatus_3] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Early__3454A3DC] DEFAULT ((0)),
[EarlyActionStatus_4] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Early__3548C815] DEFAULT ((0)),
[ConcernResponseStatus_1] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Conce__37311087] DEFAULT ((0)),
[ConcernResponseStatus_2] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Conce__382534C0] DEFAULT ((0)),
[ConcernResponseStatus_3] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Conce__391958F9] DEFAULT ((0)),
[ConcernResponseStatus_4] [int] NOT NULL CONSTRAINT [DF__PhaseDash__Conce__3A0D7D32] DEFAULT ((0)),
[CodeCategory_1] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__4396E76C] DEFAULT ((0)),
[CodeCategory_2] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__448B0BA5] DEFAULT ((0)),
[CodeCategory_3] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__457F2FDE] DEFAULT ((0)),
[CodeCategory_4] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__46735417] DEFAULT ((0)),
[CodeCategory_5] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__47677850] DEFAULT ((0)),
[CodeCategory_6] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__485B9C89] DEFAULT ((0)),
[CodeCategory_7] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__494FC0C2] DEFAULT ((0)),
[CodeCategory_8] [int] NOT NULL CONSTRAINT [DF__PhaseDash__CodeC__4A43E4FB] DEFAULT ((0)),
[CodeCategory_9] [int] NOT NULL CONSTRAINT [DF_PhaseDashboard_CodeCategory_9] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDashboard] ADD CONSTRAINT [PK_PhaseDashboard] PRIMARY KEY CLUSTERED  ([PhaseId])
GO
ALTER TABLE [dbo].[PhaseDashboard] ADD CONSTRAINT [FK_PhaseDashboard_Phase] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId]) ON DELETE CASCADE
GO
