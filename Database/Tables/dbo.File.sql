CREATE TABLE [dbo].[File]
(
[FileId] [int] NOT NULL IDENTITY(1, 1),
[StoragePath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Uploaded] [datetime2] NOT NULL,
[UploadedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContentType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Size] [int] NOT NULL,
[HashCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[File] ADD CONSTRAINT [PKFile] PRIMARY KEY CLUSTERED  ([FileId]) ON [PRIMARY]
GO
