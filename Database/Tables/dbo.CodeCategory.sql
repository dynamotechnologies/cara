CREATE TABLE [dbo].[CodeCategory]
(
[CodeCategoryId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Prefix] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodeCategory] ADD CONSTRAINT [PKCodeCategory] PRIMARY KEY CLUSTERED  ([CodeCategoryId]) ON [PRIMARY]
GO
