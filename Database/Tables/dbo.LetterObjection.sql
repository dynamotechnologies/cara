CREATE TABLE [dbo].[LetterObjection]
(
[LetterObjectionId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FiscalYear] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Region] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Forest] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObjectionNumber] [int] NOT NULL,
[RegulationNumber] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReviewStatusId] [int] NOT NULL,
[OutcomeId] [int] NULL,
[MeetingInfo] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignerFirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignerLastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectionDecisionMakerId] [int] NULL,
[ResponseDate] [datetime2] NULL,
[DmdId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdated] [datetime2] NOT NULL CONSTRAINT [DF_LetterObjection_LastUpdated] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TRULetterObjection]
   ON  [dbo].[LetterObjection]
   AFTER UPDATE
AS 
BEGIN
	
	SET NOCOUNT ON;
	UPDATE dbo.LetterObjection SET LastUpdated = GETUTCDATE()
	WHERE LetterObjectionId IN (SELECT LetterObjectionId FROM INSERTED)
    
END
GO

ALTER TABLE [dbo].[LetterObjection] ADD
CONSTRAINT [FK_LetterObjection_ObjectionDecisionMaker] FOREIGN KEY ([ObjectionDecisionMakerId]) REFERENCES [dbo].[ObjectionDecisionMaker] ([ObjectionDecisionMakerId])
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRILetterObjectionSequence]
   ON  [dbo].[LetterObjection]
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @nextNumber INT
	DECLARE @forest CHAR(2)
	DECLARE @region CHAR(2)
	DECLARE @fiscalYear CHAR(2)
	DECLARE @loId INT
	
	SELECT TOP 1 
		@loId = LetterObjectionId,
		@fiscalYear = FiscalYear,
		@region = Region,
		@forest = Forest
	FROM INSERTED
	
	-----------------------------------
	-- Select the highest objection number from phases that have the same region and forest
	-----------------------------------
	SELECT @nextNumber = MAX(ObjectionNumber) + 1
	FROM dbo.LetterObjection
	WHERE FiscalYear = @fiscalYear
		AND Forest = @forest
		AND Region = @region
		AND LetterObjectionId != @loId

	IF @nextNumber IS NULL
	BEGIN
		SET @nextNumber = 1
	END
	
	-----------------------------------
	-- Update LetterObjection to new sequence number
	-----------------------------------
    UPDATE dbo.LetterObjection
    SET ObjectionNumber = @nextNumber
    WHERE LetterObjectionId = @loId
    
END
GO
ALTER TABLE [dbo].[LetterObjection] ADD CONSTRAINT [PK_LetterObjection_1] PRIMARY KEY CLUSTERED  ([LetterObjectionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LetterObjection] ADD CONSTRAINT [FK_LetterObjection_ObjectionOutcome] FOREIGN KEY ([OutcomeId]) REFERENCES [dbo].[ObjectionOutcome] ([ObjectionOutcomeId])
GO
ALTER TABLE [dbo].[LetterObjection] ADD CONSTRAINT [FK_LetterObjection_ObjectionReviewStatus] FOREIGN KEY ([ReviewStatusId]) REFERENCES [dbo].[ObjectionReviewStatus] ([ObjectionReviewStatusId])
GO
