CREATE TABLE [dbo].[LetterTermConfirmationStatus]
(
[ConfirmationStatusId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sequence] [int] NOT NULL CONSTRAINT [DF_LetterTermConfirmationStatus_Order] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LetterTermConfirmationStatus] ADD CONSTRAINT [PK_LetterTermConfirmationStatus] PRIMARY KEY CLUSTERED  ([ConfirmationStatusId])
GO
