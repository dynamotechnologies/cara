CREATE TABLE [dbo].[CommonInterestClass]
(
[CommonInterestClassId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommonInterestClass] ADD CONSTRAINT [PKCommonInterestClass] PRIMARY KEY CLUSTERED  ([CommonInterestClassId]) ON [PRIMARY]
GO
