CREATE TABLE [dbo].[Comment]
(
[CommentId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[LetterId] [int] NOT NULL,
[ConcernResponseId] [int] NULL,
[IdentifiedOn] [datetime2] NOT NULL,
[IdentifiedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NoResponseReasonId] [int] NULL,
[NoResponseReasonOther] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Annotation] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SampleStatement] [bit] NOT NULL CONSTRAINT [DFCommentSampleStatement] DEFAULT ('False'),
[LastUpdated] [datetime2] NOT NULL,
[LastUpdatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentNumber] [int] NULL,
[UserId] [int] NULL,
[DateAssigned] [datetime2] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRDCommentConcernResponsePhaseDashboard] ON [dbo].[Comment]
    AFTER DELETE
AS
BEGIN
    SET NOCOUNT ON ;
	
    DECLARE @phases TABLE
(
 phaseId INT,
 concernResponseStatusId INT
)
    DECLARE @phaseCount INT

	-- Determines which phases where the deletion causes the
	-- associated concern/response to be deleted			
    INSERT  INTO @phases (phaseId, concernResponseStatusId)
            SELECT DISTINCT
                    l.phaseId, cr.ConcernResponseStatusId
            FROM    dbo.Letter l
                    INNER JOIN DELETED d ON l.LetterId = d.LetterId
                    INNER JOIN dbo.ConcernResponse cr ON d.ConcernResponseId = cr.ConcernResponseId
            WHERE   d.ConcernResponseId NOT IN (
                    SELECT  ConcernResponseId
                    FROM    dbo.Comment
                    WHERE   ConcernResponseId IS NOT NULL)
                    
    SELECT  @phaseCount = COUNT(*)
    FROM    @phases
    
    IF (@phaseCount > 0) 
        BEGIN
			-- Delete orphaned concern response
            DELETE  FROM dbo.ConcernResponse
            FROM    dbo.ConcernResponse cr
                    INNER JOIN DELETED d ON cr.ConcernResponseId = d.ConcernResponseId
            WHERE   d.ConcernResponseId NOT IN (
                    SELECT  ConcernResponseId
                    FROM    dbo.Comment
                    WHERE   ConcernResponseId IS NOT NULL) ;
                    
                    
            WITH    CRStatus_1(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(concernResponseStatusId)
                          FROM      @phases
                          WHERE     ConcernResponseStatusId = 1
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_1 = ConcernResponseStatus_1
                        - a.Total
                FROM    CRStatus_1 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

            WITH    CRStatus_2(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(concernResponseStatusId)
                          FROM      @phases
                          WHERE     ConcernResponseStatusId = 2
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_2 = ConcernResponseStatus_2
                        - a.Total
                FROM    CRStatus_2 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

            WITH    CRStatus_3(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(concernResponseStatusId)
                          FROM      @phases
                          WHERE     ConcernResponseStatusId = 3
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_3 = ConcernResponseStatus_3
                        - a.Total
                FROM    CRStatus_3 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

            WITH    CRStatus_4(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(concernResponseStatusId)
                          FROM      @phases
                          WHERE     ConcernResponseStatusId = 4
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_4 = ConcernResponseStatus_4
                        - a.Total
                FROM    CRStatus_4 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;
        END
END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRICommentSequence]
   ON  [dbo].[Comment]
   AFTER INSERT
AS 
BEGIN
	
	DECLARE @LS INT;
	
	SET NOCOUNT ON;
	
	-----------------------------------
	-- Grab current sequence plus one
	-----------------------------------
	
	SELECT @LS = ISNULL(CommentSequence, 0) + 1 
				FROM dbo.Letter 
				WHERE LetterId = (select MAX(INSERTED.LetterId) FROM INSERTED);

	-----------------------------------
	-- Update sequence table
	-----------------------------------
					
	UPDATE dbo.Letter
    SET CommentSequence = @LS
    WHERE LetterId = (select MAX(INSERTED.LetterId) FROM INSERTED);

	-----------------------------------
	-- Update Comment to new sequence number
	-----------------------------------

    UPDATE dbo.Comment
    SET CommentNumber = @LS
    WHERE CommentId = (select MAX(INSERTED.CommentId) FROM INSERTED);
    
   
    
    
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRIConcernResponseNumber]
   ON  [dbo].[Comment]
   FOR UPDATE
AS 
BEGIN
	
	DECLARE @LS INT;
	
	SET NOCOUNT ON;
	

	IF UPDATE(ConcernResponseId)
	BEGIN
		DECLARE @newConcernResponseId INT
		
		SELECT @newConcernResponseId = MAX(ConcernResponseId) FROM INSERTED
		
		-- update the sequence number if the concern response does not have the number set
		IF @newConcernResponseId IS NOT NULL AND EXISTS (SELECT 1 FROM dbo.ConcernResponse WHERE ConcernResponseId = @newConcernResponseId
			AND ConcernResponseNumber IS NULL)
		BEGIN
			-----------------------------------
			-- Grab current Number plus one
			-----------------------------------

			SELECT @LS = CurrentSequenceNumber + 1 
						FROM dbo.Sequence
						WHERE PhaseId = (SELECT MAX(l.PhaseId) FROM INSERTED
						INNER JOIN dbo.Letter l ON INSERTED.LetterId = l.LetterId)
						AND Name = 'Concern Response Count';	

			-----------------------------------
			-- Update Sequence table
			-----------------------------------
			
			UPDATE dbo.Sequence
			SET CurrentSequenceNumber = @LS
			WHERE PhaseId = (SELECT MAX(l.PhaseId) FROM INSERTED
						INNER JOIN dbo.Letter l ON INSERTED.LetterId = l.LetterId)
				AND Name = 'Concern Response Count';

			-----------------------------------
			-- Update ConcernResponse to new Sequence number
			-----------------------------------

			UPDATE dbo.ConcernResponse
			SET ConcernResponseNumber = @LS
			WHERE ConcernResponseId = (SELECT MAX(INSERTED.ConcernResponseId) FROM INSERTED)
		END
	END
END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRIUCommentAudit] ON [dbo].[Comment]
    AFTER INSERT, UPDATE
AS
    BEGIN      -- SET NOCOUNT ON added to prevent extra result sets from      -- interfering with SELECT statements.      SET NOCOUNT ON;            --INSERT INTO CommentAudit      
        INSERT  INTO dbo.CommentAudit
                ( CommentId ,
                  LetterId ,
                  ConcernResponseId ,
                  IdentifiedOn ,
                  IdentifiedBy ,
                  NoResponseReasonId ,
                  NoResponseReasonOther ,
                  Annotation ,
                  SampleStatement ,
                  CommentNumber ,
                  LastUpdated ,
                  LastUpdatedBy ,
                  UserId ,
                  DateAssigned
                )
                SELECT  CommentId ,
                        LetterId ,
                        ConcernResponseId ,
                        IdentifiedOn ,
                        IdentifiedBy ,
                        NoResponseReasonId ,
                        NoResponseReasonOther ,
                        Annotation ,
                        SampleStatement ,
                        CommentNumber ,
                        LastUpdated ,
                        LastUpdatedBy ,
                        UserId ,
                        DateAssigned
                FROM    INSERTED
    END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Create triggers for table Comment

CREATE TRIGGER [dbo].[TRIUCommentCanRemove]
  ON [dbo].[Comment]
  AFTER INSERT, UPDATE
  AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE pmr
    SET pmr.CanRemove = 'False'
    FROM dbo.PhaseMemberRole pmr, dbo.[User] u
    WHERE pmr.UserId = u.UserId
    and u.UserName IN (SELECT LastUpdatedBy FROM INSERTED)
       
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRUCommentConcernResponsePhaseDashboard] ON [dbo].[Comment]
    AFTER UPDATE
AS
BEGIN
    SET NOCOUNT ON ;
	

    IF EXISTS ( SELECT  1
                FROM    INSERTED i
                        INNER JOIN DELETED d ON i.CommentId = d.CommentId
                WHERE   d.ConcernResponseId IS NULL
                        AND i.ConcernResponseId IS NOT NULL ) 
        BEGIN
            DECLARE @phases TABLE (phaseId INT)
			
            INSERT  INTO @phases (phaseId)
                    SELECT DISTINCT
                            l.phaseId
                    FROM    dbo.Letter l
                            INNER JOIN INSERTED i ON l.letterId = i.LetterId
                            INNER JOIN DELETED d ON i.CommentId = d.CommentId
                    WHERE   d.ConcernResponseId IS NULL
                            AND i.ConcernResponseId IS NOT NULL ;
        
            WITH    CRStatus_1(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(*)
                          FROM      (
                                     SELECT DISTINCT
                                            l.PhaseId, cr.ConcernResponseId,
                                            cr.ConcernResponseStatusId
                                     FROM   dbo.ConcernResponse cr
                                            INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                            INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                                            INNER JOIN @phases p ON l.PhaseId = p.phaseId
                                     WHERE  cr.ConcernResponseStatusId = 1
                                    ) t
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_1 = a.Total
                FROM    CRStatus_1 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

            WITH    CRStatus_2(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(*)
                          FROM      (
                                     SELECT DISTINCT
                                            l.PhaseId, cr.ConcernResponseId,
                                            cr.ConcernResponseStatusId
                                     FROM   dbo.ConcernResponse cr
                                            INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                            INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                                            INNER JOIN @phases p ON l.PhaseId = p.phaseId
                                     WHERE  cr.ConcernResponseStatusId = 2
                                    ) t
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_2 = a.Total
                FROM    CRStatus_2 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

            WITH    CRStatus_3(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(*)
                          FROM      (
                                     SELECT DISTINCT
                                            l.PhaseId, cr.ConcernResponseId,
                                            cr.ConcernResponseStatusId
                                     FROM   dbo.ConcernResponse cr
                                            INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                            INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                                            INNER JOIN @phases p ON l.PhaseId = p.phaseId
                                     WHERE  cr.ConcernResponseStatusId = 3
                                    ) t
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_3 = a.Total
                FROM    CRStatus_3 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

            WITH    CRStatus_4(PhaseId, Total)
                      AS (
                          SELECT    PhaseId, COUNT(*)
                          FROM      (
                                     SELECT DISTINCT
                                            l.PhaseId, cr.ConcernResponseId,
                                            cr.ConcernResponseStatusId
                                     FROM   dbo.ConcernResponse cr
                                            INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                            INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                                            INNER JOIN @phases p ON l.PhaseId = p.phaseId
                                     WHERE  cr.ConcernResponseStatusId = 4
                                    ) t
                          GROUP BY  PhaseId
                         )
                UPDATE  dbo.PhaseDashboard
                SET     ConcernResponseStatus_4 = a.Total
                FROM    CRStatus_4 AS a
                        INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;
			
        
        END
END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Create triggers for table ConcernResponse

CREATE TRIGGER [dbo].[TRUCommentCRSequence]
  ON [dbo].[Comment]
  AFTER UPDATE
  AS
BEGIN
DECLARE @CRID INT;

SELECT @CRID = MAX(INSERTED.concernresponseid) FROM INSERTED;

IF @CRID IS NOT NULL 
BEGIN

	DECLARE @MaxSequence INT;
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT @MaxSequence = ISNULL(MAX(cr.Sequence) + 1,1)
    FROM dbo.ConcernResponse cr, dbo.Comment c, dbo.Letter l
    WHERE cr.ConcernResponseId = c.ConcernResponseId
    AND c.LetterId = l.LetterId
    AND l.PhaseId IN (SELECT l1.PhaseId 
						FROM dbo.Letter l1, dbo.Comment c1, INSERTED i
						WHERE l1.letterid = c1.letterid
						AND c1.concernresponseid = i.concernresponseid)
        
    UPDATE cr
    SET cr.Sequence = @MaxSequence
    FROM dbo.ConcernResponse cr
    WHERE cr.ConcernResponseId IN (SELECT ConcernResponseId FROM INSERTED)
    AND cr.sequence IS NULL
    
END    
       
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRUDCommentConcernResponseCount] ON [dbo].[Comment]
    AFTER UPDATE, DELETE
AS
BEGIN
    WITH    CommentCount(ConcernResponseId, Total)
              AS (
                  SELECT    ConcernResponseId, COUNT(*)
                  FROM      dbo.Comment
                  WHERE     ConcernResponseId IS NOT NULL
                            AND ConcernResponseId IN (
                            SELECT  ConcernResponseId
                            FROM    INSERTED
                            UNION
                            SELECT  ConcernResponseId
                            FROM    DELETED)
                  GROUP BY  ConcernResponseId
                 )

        UPDATE  dbo.ConcernResponse
        SET     CommentCount = a.Total
        FROM    CommentCount AS a
                INNER JOIN dbo.ConcernResponse cr ON a.ConcernResponseId = cr.ConcernResponseId	
	
END
GO
ALTER TABLE [dbo].[Comment] ADD CONSTRAINT [PKComment] PRIMARY KEY CLUSTERED  ([CommentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comment] ADD CONSTRAINT [FKConcernResponseComment] FOREIGN KEY ([ConcernResponseId]) REFERENCES [dbo].[ConcernResponse] ([ConcernResponseId]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Comment] ADD CONSTRAINT [FKLetterComment] FOREIGN KEY ([LetterId]) REFERENCES [dbo].[Letter] ([LetterId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Comment] ADD CONSTRAINT [FKNoResponseReasonComment] FOREIGN KEY ([NoResponseReasonId]) REFERENCES [dbo].[NoResponseReason] ([NoResponseReasonId])
GO
ALTER TABLE [dbo].[Comment] ADD CONSTRAINT [FK_Comment_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
