CREATE TABLE [dbo].[ProjectStatus]
(
[ProjectStatusId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectStatus] ADD CONSTRAINT [PKProjectStatus] PRIMARY KEY CLUSTERED  ([ProjectStatusId]) ON [PRIMARY]
GO
