CREATE TABLE [dbo].[CountrySp]
(
[CountryId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CountrySp] ADD CONSTRAINT [PKCountrySp] PRIMARY KEY CLUSTERED  ([CountryId])
GO
ALTER TABLE [dbo].[CountrySp] ADD CONSTRAINT [FK_CountrySp_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryId])
GO
