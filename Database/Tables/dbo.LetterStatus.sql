CREATE TABLE [dbo].[LetterStatus]
(
[LetterStatusId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[LetterStatus] ADD 
CONSTRAINT [PKLetterStatus] PRIMARY KEY CLUSTERED  ([LetterStatusId]) ON [PRIMARY]
GO
