CREATE TABLE [dbo].[GeneralAudit]
(
[GeneralAuditId] [int] NOT NULL IDENTITY(1, 1),
[ActionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDate] [datetime2] NULL,
[ActionBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralAudit] ADD CONSTRAINT [PKGeneralAudit] PRIMARY KEY CLUSTERED  ([GeneralAuditId]) ON [PRIMARY]
GO
