CREATE TABLE [dbo].[PhaseCodeAudit]
(
[PhaseCodeAuditId] [int] NOT NULL IDENTITY(1, 1),
[PhaseCodeId] [int] NULL,
[PhaseId] [int] NULL,
[CodeCategoryId] [int] NULL,
[CodeId] [int] NULL,
[ParentCodeId] [int] NULL,
[CodeName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CanRemove] [bit] NULL,
[LastUpdated] [datetime2] NULL CONSTRAINT [DFPhaseCodeAuditLastUpdated] DEFAULT (getutcdate()),
[UserId] [int] NULL
)
GO
ALTER TABLE [dbo].[PhaseCodeAudit] ADD CONSTRAINT [PKPhaseCodeAudit] PRIMARY KEY CLUSTERED  ([PhaseCodeAuditId]) ON [PRIMARY]
GO
