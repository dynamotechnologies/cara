CREATE TABLE [dbo].[Unit]
(
[UnitId] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]


GO
ALTER TABLE [dbo].[Unit] ADD CONSTRAINT [PKUnit] PRIMARY KEY CLUSTERED  ([UnitId]) ON [PRIMARY]
GO
