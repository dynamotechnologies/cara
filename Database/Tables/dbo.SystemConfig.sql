CREATE TABLE [dbo].[SystemConfig]
(
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ALTER TABLE [dbo].[SystemConfig] ADD 
CONSTRAINT [PKSystemConfig] PRIMARY KEY CLUSTERED  ([Name]) ON [PRIMARY]
GO
