CREATE TABLE [dbo].[Sequence]
(
[SequenceId] [int] NOT NULL IDENTITY(1, 1),
[PhaseId] [int] NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentSequenceNumber] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[Sequence] ADD
CONSTRAINT [FKPhaseSequence] FOREIGN KEY ([PhaseId]) REFERENCES [dbo].[Phase] ([PhaseId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sequence] ADD CONSTRAINT [PKSequence] PRIMARY KEY CLUSTERED  ([SequenceId]) ON [PRIMARY]
GO
