CREATE TABLE [dbo].[OfficialRepresentativeType]
(
[OfficialRepresentativeTypeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OfficialRepresentativeType] ADD CONSTRAINT [PKOfficialRepresentativeType] PRIMARY KEY CLUSTERED  ([OfficialRepresentativeTypeId]) ON [PRIMARY]
GO
