CREATE TABLE [dbo].[Activity]
(
[ActivityId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Activity__Code__18AE2015] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Activity] ADD CONSTRAINT [PKActivity] PRIMARY KEY CLUSTERED  ([ActivityId]) ON [PRIMARY]
GO
