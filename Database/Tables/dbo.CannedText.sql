CREATE TABLE [dbo].[CannedText]
(
[CannedTextId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TokenStart] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DFCannedTextTokenStart] DEFAULT ('[['),
[TokenEnd] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DFCannedTextTokenEnd] DEFAULT (']]'),
[Substituter] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CannedText] ADD CONSTRAINT [PKCannedText] PRIMARY KEY CLUSTERED  ([CannedTextId]) ON [PRIMARY]
GO
