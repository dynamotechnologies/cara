CREATE TABLE [dbo].[LetterObjectionDocument]
(
[LetterObjectionId] [int] NOT NULL,
[DocumentId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LetterObjectionDocument] ADD CONSTRAINT [PK_LetterObjectionDocument] PRIMARY KEY CLUSTERED  ([LetterObjectionId], [DocumentId])
GO
ALTER TABLE [dbo].[LetterObjectionDocument] ADD CONSTRAINT [FK_LetterObjectionDocument_Document] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[Document] ([DocumentId])
GO
ALTER TABLE [dbo].[LetterObjectionDocument] ADD CONSTRAINT [FK_LetterObjectionDocument_LetterObjection] FOREIGN KEY ([LetterObjectionId]) REFERENCES [dbo].[LetterObjection] ([LetterObjectionId])
GO
