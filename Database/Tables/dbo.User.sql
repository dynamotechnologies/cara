CREATE TABLE [dbo].[User]
(
[UserId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FirstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotifyAddedProject] [bit] NOT NULL,
[NotifySubmissionsReceived] [bit] NOT NULL,
[NotifyCommentPeriodOpen] [bit] NOT NULL,
[SubmissionsRecv] [int] NOT NULL,
[NotifyCommentPeriodEnd] [bit] NOT NULL,
[TimeZone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SearchResultsPerPage] [int] NOT NULL CONSTRAINT [CCUserSearchResultsPerPage] DEFAULT ((20)),
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LetterFontSize] [int] NOT NULL CONSTRAINT [DFUserLetterFontSize] DEFAULT ((10))
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IDXUserUserName] ON [dbo].[User] ([UserName]) ON [PRIMARY]


GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [PKUser] PRIMARY KEY CLUSTERED  ([UserId]) ON [PRIMARY]
GO
