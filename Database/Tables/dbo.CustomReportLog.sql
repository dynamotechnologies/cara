CREATE TABLE [dbo].[CustomReportLog]
(
[CustomReportLogId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[ExecutionDate] [datetime] NOT NULL CONSTRAINT [DF_CustomReportLog_ExecutionDate] DEFAULT (getutcdate()),
[Parameters] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomReportLog] ADD CONSTRAINT [PK_CustomReportLog] PRIMARY KEY CLUSTERED  ([CustomReportLogId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomReportLog] ADD CONSTRAINT [FK_CustomReportLog_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
GO
