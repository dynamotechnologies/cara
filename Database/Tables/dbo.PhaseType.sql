CREATE TABLE [dbo].[PhaseType]
(
[PhaseTypeId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_PhaseType_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseType] ADD CONSTRAINT [PKPhaseType] PRIMARY KEY CLUSTERED  ([PhaseTypeId]) ON [PRIMARY]
GO
