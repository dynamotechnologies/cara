CREATE TABLE [dbo].[ObjectionDecisionMaker]
(
[ObjectionDecisionMakerId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentId] [int] NULL,
[SortOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ObjectionDecisionMaker] ADD CONSTRAINT [PK_ObjectionDecisionMaker] PRIMARY KEY CLUSTERED  ([ObjectionDecisionMakerId])
GO
ALTER TABLE [dbo].[ObjectionDecisionMaker] ADD CONSTRAINT [FK_ObjectionDecisionMaker_ObjectionDecisionMaker] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[ObjectionDecisionMaker] ([ObjectionDecisionMakerId])
GO
