CREATE TABLE [dbo].[State]
(
[StateId] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[State] ADD CONSTRAINT [PKState] PRIMARY KEY CLUSTERED  ([StateId]) ON [PRIMARY]
GO
