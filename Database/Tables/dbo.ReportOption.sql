CREATE TABLE [dbo].[ReportOption]
(
[ReportId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsArtifact] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportOption] ADD CONSTRAINT [PKReportOption] PRIMARY KEY CLUSTERED  ([ReportId], [Name]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportOption] ADD CONSTRAINT [FKReportReportOption] FOREIGN KEY ([ReportId]) REFERENCES [dbo].[Report] ([ReportId])
GO
