SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[spsCommentWithCRInfoReport] 
( 
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
  )
AS
    SET NOCOUNT ON
    
    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

    BEGIN
   
		   Set @sql = 'SELECT 
			CASE WHEN ConcernResponseNumber IS NULL THEN '''' ELSE CONVERT(VARCHAR(10), ConcernResponseNumber) END AS [CRNumber],
			CASE WHEN dbo.PhaseCode.PhaseCodeId IS NULL THEN '''' ELSE dbo.fnGetCodeNumberDisplay(PhaseCode.CodeNumber) + '' '' + PhaseCode.CodeName END AS [CRCodes],
			dbo.Letter.LetterSequence AS ls,
			dbo.Comment.CommentNumber AS cn,
			CONVERT(VARCHAR(10), dbo.Letter.LetterSequence) + ''-'' + CONVERT(VARCHAR(10),dbo.Comment.CommentNumber) AS [CommentNumber],
			REPLACE(dbo.fnGetCommentCodeText(dbo.Comment.CommentId),''|'','' '') AS [CommentCodes],
			dbo.fnGetCommentText(dbo.Comment.CommentId) AS [CommentText],
			CASE WHEN dbo.Comment.SampleStatement = 1 THEN ''Yes'' ELSE ''No'' END AS [SampleStatement],
			CASE WHEN dbo.Comment.UserId IS NULL THEN '''' ELSE dbo.[User].LastName + '', '' + dbo.[User].FirstName END AS [AssignedTo]


			FROM dbo.Comment
			INNER JOIN dbo.Letter ON dbo.Comment.LetterId = dbo.Letter.LetterId
			LEFT OUTER JOIN dbo.ConcernResponse ON dbo.Comment.ConcernResponseId = dbo.ConcernResponse.ConcernResponseId
			LEFT OUTER JOIN dbo.PhaseCode ON dbo.ConcernResponse.PhaseCodeId = dbo.PhaseCode.PhaseCodeId
			LEFT OUTER JOIN dbo.[User] ON dbo.Comment.UserId = dbo.[User].UserId
			WHERE dbo.Letter.PhaseId = ' + CAST(@phaseId as varchar(20))
	
	 END		
	 
	  IF @sortBy IS NULL
		SET @sortBy = 'CommentCodes, ls, cn'           
    
    --ORDER BY r.Name, u.LastName, u.FirstName, u.Email
    
     /* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN
GO
GRANT EXECUTE ON  [dbo].[spsCommentWithCRInfoReport] TO [carauser]
GO
