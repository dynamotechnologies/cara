
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/* This stored procedure is for Letter Inbox page */

CREATE PROCEDURE [dbo].[spsLetters]
(
      @phaseId INT = -1,
      @filterLetterSequence INT = -1,
      @filterLetterTypeId INT = -1,
      @filterDateReceivedFrom DATETIME = NULL,
      @filterDateReceivedTo DATETIME = NULL,
      @filterLetterStatusId INT = -1,
      @filterKeyword varchar(100) = NULL,
      --@filterEarlyActionRequired BIT = NULL, -- Commented on 10/14/11 by LD
      @filterEarlyActionStatusId INT = -1,       -- Added new filter value on 10/14/11 by LD
      @filterViewAll bit = NULL,
      @filterWithinCommentPeriod bit = NULL,
      @filterFirstName VARCHAR(100)=NULL,
      @filterLastName VARCHAR(100)=NULL,
      @filterStartingLetterId INT = -1,
      @pageNum int = 1,
      @numRows int = -1,
      @sortKey varchar(50) = NULL,
      @totalRows int OUT
)
AS
      -- Note: All paging sps procedures should have SET NOCOUNT ON
      SET NOCOUNT ON

      DECLARE @sql varchar(8000), @sortBy varchar(255), @sqlWhere varchar(8000), @sqlCount varchar(8000), @sqlFrom VARCHAR(8000)
      DECLARE @Asterisk varchar(20)= '*', @SingleQuote VARCHAR(1) = ''''
      
      -- -------------------------------
      -- Construct the select query
      -- -------------------------------
      SET @sqlWhere = ' WHERE l.Deleted = 0 '
      SET @sql = ' SELECT DISTINCT l.PhaseId, l.LetterId, l.LetterStatusId, lst.NAME AS LetterStatusName, l.LetterSequence, l.DateSubmitted, l.LetterTypeId,
                  lt.Name as LetterTypeName, au.FirstName, au.LastName, org.OrganizationId, org.OrganizationTypeId, org.Name AS OrganizationName,
                  orgt.Name AS OrganizationTypeName, l.Size, l.EarlyActionStatusId, eas.Name AS EarlyActionStatus, l.WithinCommentPeriod ,
                  CASE WHEN l.Size > ISNULL(LEN(l.Text), 0) THEN 1 ELSE 0 END AS HasAttachment '
                  
      SET @sqlCount = ' SELECT 1 '
    SET @sqlFrom = 'FROM dbo.Letter l
                  INNER JOIN dbo.LetterStatus lst ON lst.LetterStatusId = l.LetterStatusId
                  INNER JOIN dbo.LetterType lt ON lt.LetterTypeId = l.LetterTypeId
                  INNER JOIN dbo.Author au ON au.LetterId = l.LetterId AND au.Sender = 1
                  LEFT OUTER JOIN dbo.EarlyActionStatus eas ON eas.StatusId = l.EarlyActionStatusId
                  LEFT OUTER JOIN dbo.Organization org ON org.OrganizationId = au.OrganizationId
                  LEFT OUTER JOIN dbo.OrganizationType orgt ON orgt.OrganizationTypeId = org.OrganizationTypeId '


      -- --------------------------
      -- Construct filters
      -- --------------------------
      IF (@phaseId <> -1)
      BEGIN
            SET @sqlWhere = @sqlWhere + 'AND l.PhaseId = ' + CAST(@phaseId as varchar(20))
      END

      IF (@filterKeyword IS NOT NULL)
      BEGIN
            SET @sqlWhere = @sqlWhere + 'AND CONTAINS((l.Text, l.CodedText),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ')'
      END
      
      IF (@filterLetterTypeId <> -1)
      BEGIN
            IF (@filterLetterTypeId = 6)
            BEGIN
                  -- Not Pending
                  SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (1,2,3,4,5) '
            END
            ELSE IF (@filterLetterTypeId = 7)
            BEGIN
                  -- All Forms
                  SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (3,4,5) '
            END
            ELSE IF (@filterLetterTypeId = 8)
            BEGIN
                  -- Master Form or Unique
                  SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (2,3) '
            END
            ELSE IF (@filterLetterTypeId = 10)
            BEGIN
                  -- Master Form or Unique
                  SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (2,3,4,5) '
            END
            ELSE
            BEGIN
                  SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId = ' + CAST(@filterLetterTypeId as varchar(20))
            END
      END

      IF (@filterDateReceivedFrom IS NOT NULL)
      BEGIN       
            SET @sqlWhere = @sqlWhere + ' AND l.DateSubmitted >= ''' + CAST(@filterDateReceivedFrom as varchar(20)) + ''''
      END
      
      IF (@filterDateReceivedTo IS NOT NULL)
      BEGIN       
            SET @sqlWhere = @sqlWhere + ' AND l.DateSubmitted <= ''' + CAST(@filterDateReceivedTo as varchar(20)) + ''''
      END
      
      IF (@filterLetterStatusId <> -1)
      BEGIN       
            SET @sqlWhere = @sqlWhere + ' AND l.LetterStatusId = ' + CAST(@filterLetterStatusId as varchar(20))
      END

      --IF (@filterEarlyActionRequired IS NOT NULL)
      --BEGIN           
      --    SET @sqlWhere = @sqlWhere + ' AND l.EarlyActionRequired = ' + CAST(@filterEarlyActionRequired as varchar(20))
      --END
      
      IF (@filterEarlyActionStatusId IS NOT NULL)
      IF (@filterEarlyActionStatusId = 5)
            BEGIN
                  -- Unique or Forms
                  SET @sqlWhere = @sqlWhere + ' AND l.EarlyActionStatusId IN (2,3,4) '
            END
      ELSE
      BEGIN       
            SET @sqlWhere = @sqlWhere + ' AND l.EarlyActionStatusId = ' + CAST(@filterEarlyActionStatusId as varchar(20))
      END
      
      IF (@filterViewAll IS NOT NULL AND @filterViewAll = 1)
      BEGIN       
            SET @sqlWhere = @sqlWhere + ' AND lt.Name IN (''Unique'', ''Master Form'', ''Form Plus'') '
      END
      
      IF (@filterWithinCommentPeriod IS NOT NULL)
      BEGIN       
            SET @sqlWhere = @sqlWhere + ' AND l.WithinCommentPeriod = ' + CAST(@filterWithinCommentPeriod as varchar(20))
      END
      
      IF (@filterLetterSequence <> -1)
      BEGIN
            SET @sqlWhere = @sqlWhere + ' AND l.LetterSequence = ' + CAST(@filterLetterSequence as varchar(20))
      END
      
      IF (@filterFirstName IS NOT NULL)
      BEGIN
            SET @sqlWhere = @sqlWhere + ' AND au.FirstName  like ''%' + REPLACE(REPLACE(CAST(@filterFirstName as varchar(50)), '"', '""'), '''', '''''') + '%'''
      END
      
      IF (@filterLastName IS NOT NULL)
		IF (@filterFirstName IS NULL)
		  BEGIN
				SET @sqlWhere = @sqlWhere + ' AND (au.LastName like ''%' + REPLACE(REPLACE(CAST(@filterLastName as varchar(50)), '"', '""'), '''', '''''') + '%'''
				 + 'OR au.FirstName like ''%' + REPLACE(REPLACE(CAST(@filterLastName as varchar(50)), '"', '""'), '''', '''''') + '%'')'
		  END
      ELSE
          BEGIN
            SET @sqlWhere = @sqlWhere + ' AND au.LastName like ''%' + REPLACE(REPLACE(CAST(@filterLastName as varchar(50)), '"', '""'), '''', '''''') + '%'''
		  END
      
      IF (@filterStartingLetterId <> -1)
      BEGIN
            SET @sqlWhere = @sqlWhere + ' AND l.LetterId > ' + CAST(@filterStartingLetterId as varchar(20))
      END
      
      SET @sql = @sql + @sqlFrom + @sqlWhere
      
      -- -------------------------------------------
      -- Build Sort By clause
      -- -------------------------------------------
	  DECLARE @sortFirstToo VARCHAR(50)
      IF @sortKey IS NOT NULL
      BEGIN 
			SET @sortBy = @sortKey
			
			IF (CHARINDEX('AuthorName', @sortBy) > 0)
			BEGIN
				SET @sortFirstToo = REPLACE(@sortBy, 'AuthorName', 'AuthorFirstName')
				SET @sortBy = @sortBy + ', ' + @sortFirstToo
			END

            -- translate the sort key into the corresponding column name or clause
            SET @sortBy = REPLACE(@sortBy, 'LetterId', 'l.LetterId')
            SET @sortBy = REPLACE(@sortBy, 'AuthorName', 'au.LastName')
            SET @sortBy = REPLACE(@sortBy, 'AuthorFirstName', 'au.FirstName')
            SET @sortBy = REPLACE(@sortBy, 'LetterStatusName', 'lst.Name')
            SET @sortBy = REPLACE(@sortBy, 'OrganizationTypeName', 'orgt.Name')
            SET @sortBy = REPLACE(@sortBy, 'OrganizationName', 'org.Name')
            SET @sortBy = REPLACE(@sortBy, 'DateSubmitted', 'l.DateSubmitted')
            SET @sortBy = REPLACE(@sortBy, 'LetterTypeName', 'lt.Name')
            SET @sortBy = REPLACE(@sortBy, 'EarlyActionStatus', 'eas.Name')
            SET @sortBy = REPLACE(@sortBy, 'Size', 'l.Size')
      END

      -- Default Sort By clause
      

      /* -------------------------------------------------------------------------------------------*/
      /* If @numRows = -1, then we want to return all records       */
      /* Otherwise, we want to page to @pageNum                       */
      /* -------------------------------------------------------------------------------------------*/
      IF @numRows <> -1
	  BEGIN
			IF @sortBy IS NULL
			SET @sortBy = 'l.LetterId asc'
			
			EXECUTE dbo.spsRowPager
				  @SQL = @sql,
				  @RecordsPerPage = @numRows,
				  @Page = @pageNum,
				  @ID = 'LetterId',
				  @SortBy = @sortBy
	              
			EXEC (@sqlCount + @sqlFrom + @sqlWhere)
			SET @totalRows = @@ROWCOUNT    
      END

      /* -------------------------------------------------------------------------------------------*/
      /* return all rows (no paging)                                                   */
      /* -------------------------------------------------------------------------------------------*/

      ELSE
			BEGIN
				IF @sortBy IS NULL
				SET @sortBy = 'LetterId asc'
            SET @sql = @sql + ' ORDER BY ' + @sortBy
            EXEC (@sql)       
            SET @totalRows = @@ROWCOUNT
      
			END

      RETURN






GO
GRANT EXECUTE ON  [dbo].[spsLetters] TO [carauser]
GO
