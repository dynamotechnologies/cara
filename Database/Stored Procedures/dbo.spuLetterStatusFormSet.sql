
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is to ensure all of the letters in a form set have the same publishing status as the master */

CREATE PROCEDURE [dbo].[spuLetterStatusFormSet]
(
	@formSetId INT,
	@letterStatusId INT
)
AS
	SET NOCOUNT OFF
	
	UPDATE TOP (200) dbo.Letter
	SET letterStatusId = @letterStatusId
	WHERE FormSetId = @formSetId
		AND LetterTypeId != 5 -- = Form Plus
		AND LetterStatusId != @letterStatusId

	DECLARE @rowsUpdated INT
	SELECT @rowsUpdated = @@ROWCOUNT
	RETURN @rowsUpdated

GO

GRANT EXECUTE ON  [dbo].[spuLetterStatusFormSet] TO [carauser]
GO
