
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO





/* This stored procedure is for Codes per phase */

CREATE PROCEDURE [dbo].[spsCodingStructureReport] ( @PhaseId INT = -1 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;

    DECLARE @phaseTypeId INT
    DECLARE @ReportTemp AS TABLE
        (
          PhaseCodeId INT ,
          CodeId INT ,
          ParentCodeId INT ,
          CodeName VARCHAR(200) ,
          CodeNumber VARCHAR(20) ,
          [LEVEL] INT ,
          CodeCategoryID INT ,
          CodeCategoryName VARCHAR(200) ,
          CommentCount INT
        )
	
    SELECT  @phaseTypeId = PhaseTypeId
    FROM    dbo.Phase
    WHERE   PhaseId = @phaseId ;
	
    WITH    TopLevelCodes ( PhaseCodeId, CodeId, ParentCodeId, CodeName, CodeNumber, [Level], CodeCategoryId, CodeCategoryName )
              AS ( SELECT   PhaseCodeId ,
                            CodeId ,
                            ParentCodeId ,
                            CodeName ,
                            CodeNumber ,
                            (dbo.fnGetLevelNumber(CodeNumber)) - 1 AS [Level],   
                            v.CodeCategoryId,
                            cc.Name
                   FROM     PhaseCode v
							INNER JOIN dbo.CodeCategory cc ON v.CodeCategoryId = cc.CodeCategoryId
                   WHERE    (ParentCodeId IS NULL
                             OR ParentCodeId NOT IN (Select CodeId FROM PhaseCode WHERE PhaseId = @PhaseId )
                             )
                            AND ( PhaseId = @PhaseId
                                  OR ( PhaseId IS NULL
                                       AND CodeId NOT IN (
                                       SELECT   codeid
                                       FROM     phasecode
                                       WHERE    PhaseId = @PhaseId
                                                AND ParentCodeId IS NULL )
                                     )
                                )
                   UNION ALL
                   SELECT   cpc.PhaseCodeId ,
                            cpc.CodeId ,
                            cpc.ParentCodeId ,
                            cpc.CodeName ,
                            cpc.CodeNumber ,
                            [Level] + 1 ,
                            cpc.CodeCategoryId,
                            cc.Name
                   FROM     PhaseCode cpc
							INNER JOIN dbo.CodeCategory cc ON cpc.CodeCategoryId = cc.CodeCategoryId
                            INNER JOIN TopLevelCodes AS t ON cpc.ParentCodeId = t.CodeId
                   WHERE    ( cpc.PhaseId = @PhaseId
                              OR ( cpc.PhaseId IS NULL
                                   AND cpc.CodeId NOT IN (
                                   SELECT   codeid
                                   FROM     phasecode
                                   WHERE    PhaseId = @PhaseId )
                                 )
                            )
                 )
        -- only insert the codes with applicable code categories
			INSERT    INTO @ReportTemp
                            ( PhaseCodeId ,
                              CodeId ,
                              ParentCodeId ,
                              CodeName ,
                              CodeNumber ,
                              LEVEL ,
                              CodeCategoryID ,
                              CodeCategoryName,
                              CommentCount
	                      )
                            SELECT  PhaseCodeId ,
                                    CodeId ,
                                    ParentCodeId ,
                                    CodeName ,
                                    CodeNumber ,
                                    [Level] ,
                                    CodeCategoryId ,
                                    CodeCategoryName,
                                    0 AS CommentCount
                            FROM    TopLevelCodes
                            WHERE   TopLevelCodes.CodeCategoryId IN (
                                    SELECT  CodeCategoryId
                                    FROM    dbo.CodeCategoryPhaseType ccpt
                                    WHERE   ccpt.PhaseTypeId = @phaseTypeId )
                            ORDER BY CodeNumber --codeid ASC, ParentCodeId asc
                                    
						
		-- update temp table with comment counts
    UPDATE  rt
    SET     rt.CommentCount = ( SELECT  COUNT(*)
                                FROM    dbo.CommentCode cc
                                WHERE   cc.PhaseCodeId = rt.PhaseCodeId
                              )
    FROM    @ReportTemp rt
		
		-- return report contents
    SELECT  CodeId ,
            ParentCodeId ,
            CodeName ,
            CodeNumber ,
            [Level] ,
            CodeCategoryId ,
            CodeCategoryName,
            CommentCount
    FROM    @ReportTemp 
						
    RETURN
GO



GRANT EXECUTE ON  [dbo].[spsCodingStructureReport] TO [carauser]
GO
