
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists signature and letter counts by organization type */

CREATE PROCEDURE [dbo].[spsLetterTypeReport] ( @PhaseId INT = -1 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;

    SELECT  ltl.name AS LetterTypeName, lta.scount AS SignatureCount, ltl.lcount AS LetterCount
    FROM    ( SELECT    lt.Name, lt.LetterTypeId, COUNT(a.AuthorId) AS scount
              FROM      dbo.Letter l ,
                        dbo.Author a ,
                        dbo.LetterType lt
              WHERE     l.LetterId = a.LetterId
                        AND l.LetterTypeId = lt.LetterTypeId
                        AND l.PhaseId = @PhaseId
              GROUP BY  lt.Name, lt.LetterTypeId
            ) lta ,
            ( SELECT    lt.Name, lt.LetterTypeId,
                        COUNT(DISTINCT l.LetterId) AS lcount
              FROM      dbo.Letter l ,
                        dbo.Author a ,
                        dbo.LetterType lt
              WHERE     l.LetterId = a.LetterId
                        AND l.LetterTypeId = lt.LetterTypeId
                        AND l.PhaseId = @PhaseId
              GROUP BY  lt.Name, lt.LetterTypeId
            ) ltl
    WHERE   lta.LetterTypeId = ltl.LetterTypeId
    ORDER BY ltl.Name
							
    RETURN

GO
GRANT EXECUTE ON  [dbo].[spsLetterTypeReport] TO [carauser]
GO
