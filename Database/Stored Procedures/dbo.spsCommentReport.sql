
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists comments by code category/code  */

CREATE PROCEDURE [dbo].[spsCommentReport]
(
	@PhaseId INT = -1,
	@pageNum INT = 1,
	@numRows INT = -1,
	@totalRows INT OUT
)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON ;
    
	DECLARE @sql VARCHAR(MAX)
	DECLARE @sqlCount VARCHAR(MAX)
	DECLARE @sortBy VARCHAR(255)	

    SET @sql = 'SELECT  cc.Name AS CodeCategory, cc.SortOrder, pc.CodeName, pc.CodeNumber,
				l.LetterSequence, c.CommentId as CommentId, c.LetterId as LetterId, c.CommentNumber,
				CommentText = LEFT(o.list, LEN(o.list) - 5)
		FROM    dbo.Letter l
				INNER JOIN dbo.Comment c ON l.LetterId = c.LetterId
				INNER JOIN dbo.CommentCode cmc ON c.CommentId = cmc.CommentId
				INNER JOIN dbo.PhaseCode pc ON cmc.PhaseCodeId = pc.PhaseCodeId
				INNER JOIN dbo.CodeCategory cc ON pc.CodeCategoryId = cc.CodeCategoryId
				CROSS APPLY ( SELECT    LTRIM([Text]) + ''[...]''
							  FROM      dbo.CommentText ctL
							  WHERE     ctL.CommentId = c.CommentId
							  ORDER BY  ctl.CommentTextId 
							FOR
							  XML PATH('''')
							) o ( list )
		WHERE   pc.PhaseId = ' + CAST(@PhaseId AS VARCHAR(20)) + ' '
		
	SET @sqlCount = 'SELECT c.CommentId
		FROM    dbo.Comment c
			INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
			INNER JOIN dbo.CommentCode cc ON c.CommentId = cc.CommentId
		WHERE   l.PhaseId = ' + CAST(@PhaseId AS VARCHAR(20)) + ' '
            
 
	IF @sortBy IS NULL 
		SET @sortBy = 'SortOrder, CodeNumber, LetterId, CommentId'            
		--ORDER BY cc.Name, pc.CodeNumber, c.LetterId, c.CommentId
    
	-- ----------------------------------------------------
	-- If @numRows = -1, then we want to return all records
	-- Otherwise, we want to page to @pageNum
	-- ----------------------------------------------------
	IF @numRows <> -1 AND @numRows > 0 
    BEGIN
        EXECUTE dbo.spsRowPagerDistinct 
            @SQL = @sql,
            @RecordsPerPage = @numRows,
            @Page = @pageNum,
            @ID = '',
            @SortBy = @sortBy
			
        EXEC (@sqlCount)
        SET @totalRows = @@ROWCOUNT
    END

	-- ---------------------------
	-- return all rows (no paging)
	-- ---------------------------
	ELSE 
    BEGIN
        SET @sql = @sql + ' ORDER BY ' + @sortBy
        EXEC (@sql)		
        SET @totalRows = @@ROWCOUNT
    END
							
RETURN
GO









GRANT EXECUTE ON  [dbo].[spsCommentReport] TO [carauser]
GO
