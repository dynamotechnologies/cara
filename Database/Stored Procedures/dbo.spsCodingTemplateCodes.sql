
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure returns a list of the template 0codes */

CREATE PROCEDURE [dbo].[spsCodingTemplateCodes]
(
	@codingTemplateId INT,
	@templateName VARCHAR(100) OUTPUT
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON;
	
	SELECT @templateName = [NAME]
	FROM dbo.CodingTemplate
	WHERE CodingTemplateId = @codingTemplateId

	SELECT @codingTemplateId AS CodingTemplateId, cd.CodeId, cd.ParentCodeId, cd.Name, cd.CodeNumber, cd.CodeCategoryId, cty.Name AS CodeCategoryName
	FROM dbo.Code cd
		INNER JOIN dbo.CodingTemplateCode ctc ON ctc.CodeId = cd.CodeId
		INNER JOIN dbo.CodingTemplate ct ON ct.CodingTemplateId = ctc.CodingTemplateId
		INNER JOIN dbo.CodeCategory cty ON cty.CodeCategoryId = cd.CodeCategoryId
	WHERE ct.CodingTemplateId = @codingTemplateId
	UNION ALL
	SELECT NULL AS CodingTemplateId, cd1.CodeId, cd1.ParentCodeId, cd1.Name, cd1.CodeNumber, cd1.CodeCategoryId, cty1.Name AS CodeCategoryName
	FROM dbo.Code cd1
		INNER JOIN dbo.CodeCategory cty1 ON cty1.CodeCategoryId = cd1.CodeCategoryId
	WHERE cd1.CodeId NOT IN (SELECT CodeId FROM dbo.CodingTemplateCode WHERE CodingTemplateId = @codingTemplateId)
	ORDER BY CodeNumber
	
	RETURN
GO


GRANT EXECUTE ON  [dbo].[spsCodingTemplateCodes] TO [carauser]
GO
