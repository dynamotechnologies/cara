
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Jeffrey,,Rhoadhouse>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spsCustomReport]
	(-- Add the parameters for the stored procedure here
	@myDoc XML,
	@pageNum INT = 1 ,
	@numRows INT = -1 ,
	@userId INT,
	@isCount BIT = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @select VARCHAR(MAX)

	--Booleans for table requirements
	--Filter tables
	DECLARE @authorFilter BIT
	SET @authorFilter = 0
	DECLARE @commentFilter BIT
	SET @commentFilter = 0
	DECLARE @commentCodeFilter BIT
	SET @commentCodeFilter = 0
	DECLARE @commentUserFilter BIT
	SET @commentUserFilter = 0
	DECLARE @concernResponseFilter BIT
	SET @concernResponseFilter = 0
	DECLARE @concernPhaseCodeFilter BIT
	SET @concernPhaseCodeFilter = 0
	DECLARE @concernUserFilter BIT
	SET @concernUserFilter = 0
	DECLARE @letterFilter BIT
	SET @letterFilter = 0
	DECLARE @letterObjectionFilter BIT
	SET @letterObjectionFilter = 0
	DECLARE @organizationFilter BIT
	SET @organizationFilter = 0
	DECLARE @phaseFilter BIT
	SET @phaseFilter = 1
	DECLARE @phaseCodeFilter BIT
	SET @phaseCodeFilter = 0
	DECLARE @projectFilter BIT
	SET @projectFilter = 0
	DECLARE @projectActivityFilter BIT
	SET @projectActivityFilter = 0
	DECLARE @projectPurposeFilter BIT
	SET @projectPurposeFilter = 0


	--column tables
	DECLARE @activityColumn BIT
	SET @activityColumn = 0
	DECLARE @analysisTypeColumn BIT
	SET @analysisTypeColumn = 0
	DECLARE @authorColumn BIT
	SET @authorColumn = 0
	DECLARE @codeCategoryColumn BIT
	SET @codeCategoryColumn = 0
	DECLARE @commentColumn BIT
	SET @commentColumn = 0
	DECLARE @commentRuleColumn BIT
	SET @commentRuleColumn = 0
	DECLARE @commentTextColumn BIT
	SET @commentTextColumn = 0
	DECLARE @commentUserColumn BIT
	SET @commentUserColumn = 0
	DECLARE @commonInterestClassColumn BIT
	SET @commonInterestClassColumn = 0
	DECLARE @concernCodeCategory BIT
	SET @concernCodeCategory = 0
	DECLARE @concernPhaseCodeColumn BIT
	SET @concernPhaseCodeColumn = 0
	DECLARE @concernResponseColumn BIT
	SET @concernResponseColumn = 0
	DECLARE @concernResponseStatusColumn BIT
	SET @concernResponseStatusColumn = 0
	DECLARE @concernUserColumn BIT
	SET @concernUserColumn = 0
	DECLARE @countryColumn BIT
	SET @countryColumn = 0
	DECLARE @deliveryTypeColumn BIT
	SET @deliveryTypeColumn = 0
	DECLARE @earlyActionStatusColumn BIT
	SET @earlyActionStatusColumn = 0
	DECLARE @formSetColumn BIT
	SET	@formSetColumn = 0
	DECLARE @letterColumn BIT
	SET @letterColumn = 0
	DECLARE @letterObjectionColumn BIT
	SET @letterObjectionColumn = 0
	DECLARE @letterStatusColumn BIT
	SET @letterStatusColumn = 0
	DECLARE @letterTermConfirmationColumn BIT
	SET @letterTermConfirmationColumn = 0
	DECLARE @letterTypeColumn BIT
	SET @letterTypeColumn = 0
	DECLARE @noResponseReasonColumn BIT
	SET @noResponseReasonColumn = 0
	DECLARE @objectionDecisionMakerColumn BIT
	SET @objectionDecisionMakerColumn = 0
	DECLARE @objectionOutcomeColumn BIT
	SET @objectionOutcomeColumn = 0
	DECLARE @objectionReviewStatusColumn BIT
	SET @objectionReviewStatusColumn = 0
	DECLARE @organizationColumn BIT
	SET @organizationColumn = 0
	DECLARE @organizationTypeColumn BIT
	SET @organizationTypeColumn = 0
	DECLARE @officialRepresentativeTypeColumn BIT
	SET @officialRepresentativeTypeColumn = 0
	DECLARE @phaseColumn BIT
	SET @phaseColumn = 0
	DECLARE @phaseCodeColumn BIT
	SET @phaseCodeColumn = 0
	DECLARE @phaseTypeColumn BIT
	SET @phaseTypeColumn = 0
	DECLARE @projectColumn BIT
	SET @projectColumn = 0
	DECLARE @projectTypeColumn BIT
	SET @projectTypeColumn = 0
	DECLARE @projectStatusColumn BIT
	SET @projectStatusColumn = 0
	DECLARE @purposeColumn BIT
	SET @purposeColumn = 0
	DECLARE @termColumn BIT
	SET @termColumn = 0
	DECLARE @unitColumn BIT
	SET @unitColumn = 0
	DECLARE @unpublishedReasonColumn BIT
	SET @unpublishedReasonColumn = 0

	DECLARE @columnName VARCHAR(MAX)
	SET @columnName = ''
	DECLARE @sortBy VARCHAR(MAX)
	SET @sortBy = ''

	--Create Where string
	DECLARE @where VARCHAR(MAX)
	SET @where = 'WHERE '
	DECLARE @filters XML
	SET @filters = @myDoc.query('/CustomReportData/fieldType/field')
	DECLARE @firstFilter BIT
	SET @firstFilter = 1
	WHILE (@filters.exist('./field') = 1)
	BEGIN
		DECLARE @filter XML
		SET @filter = @filters.query('./field[1]')
		DECLARE @fieldId INT
		SET @fieldId = @filter.value('(./field/@fieldId)[1]', 'int')
		DECLARE @values XML
		SET @values = @filter.query('./field/value')
		DECLARE @firstIncludeValue BIT
		SET @firstIncludeValue = 1
		DECLARE @firstExcludeValue BIT
		SET @firstExcludeValue = 1
		DECLARE @includeClause VARCHAR(MAX)
		SET @includeClause = ''
		DECLARE @excludeClause VARCHAR(MAX)
		SET @excludeClause = ''
		DECLARE @patternIndex BIGINT
		SET @patternIndex = -1 
		WHILE (@values.exist('./value') = 1)
		BEGIN
			DECLARE @value XML
			SET @value = @values.query('./value[1]')
			DECLARE @valueData VARCHAR(MAX)
			DECLARE @not VARCHAR(6)
			DECLARE @clause VARCHAR(MAX)
			
			IF @fieldId = 1 --Analysis Type
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Project.AnalysisTypeId = ' + @valueData
				SET @projectFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 2 --Project Activities
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'ProjectActivity.ActivityId = ' + @valueData
				SET @projectActivityFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 3 --Project Purpose
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'ProjectPurpose.PurposeId = ' + @valueData
				SET @projectPurposeFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 4 -- Author Last Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.LastName = ''' + @valueData + ''''
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 5 -- Author First Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.FirstName = ''' + @valueData + ''''
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 6 -- Author City
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.City = ''' + @valueData + ''''
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 7 --Author State
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.StateId = ''' + @valueData + ''''
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 8 -- Author zip
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.ZipPostal = ''' + @valueData + ''''
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END

			IF @fieldId = 9 --Country
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.CountryId = ' + @valueData
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 10 --Organization
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.OrganizationId = ' + @valueData
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 11 --Organization Type
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Organization.OrganizationTypeId = ' + @valueData
				SET @OrganizationFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 12 --Official Representative Type
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.OfficialRepresentativeTypeId = ' + @valueData
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 13 --Delivery Type
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(6)')
				SET @clause = 'Letter.DeliveryTypeId = ' + @valueData
				SET @letterFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 14 --Submitted date
			BEGIN
				IF @value.exist('./value/@from') = 1
				BEGIN
					SET @valueData = @value.value('(./value/@from)[1]', 'VARCHAR(MAX)')
					SET @clause = 'Letter.DateSubmitted >= ''' + @valueData + ''''
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					IF @value.exist('./value/@to') = 1
						SET @includeClause = @includeClause + '('
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
					SET @letterFilter = 1
				END
				IF @value.exist('./value/@to') = 1
				BEGIN
					SET @valueData = @value.value('(./value/@to)[1]', 'VARCHAR(MAX)')
					SET @clause = 'Letter.DateSubmitted <= ''' + @valueData + ''''
					IF @value.exist('./value/@from') = 1
						SET @includeClause = @includeClause + ' AND ' + @clause + ')'
					ELSE
					BEGIN
						IF @firstIncludeValue = 0
							SET @includeClause = @includeClause + ' OR '
						SET @includeClause = @includeClause + @clause
					END
					SET @firstIncludeValue = 0
					SET @letterFilter = 1
				END
			END
			
			IF @fieldId = 15 --Letter Number
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @patternIndex = CHARINDEX('-', @valueData)
				IF @patternIndex > 0
					SET @clause = 'Letter.LetterSequence BETWEEN ' + LEFT(@valueData, @patternIndex - 1) + ' AND ' 
						+ SUBSTRING(@valueData, @patternIndex + 1, LEN(@valueData))
				ELSE
					SET @clause = 'Letter.LetterSequence = ' + @valueData
				SET @letterFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 16 -- Letter Text
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @columnName = 'Letter.Text'
				SET @patternIndex = PATINDEX('% AND %', @valueData)
				IF @patternIndex > 0
				BEGIN
					SET @clause = '('
					WHILE @patternIndex > 0
					BEGIN
						SET @clause = @clause + 'CONTAINS(' + @columnName + ', ''' + LEFT(@valueData, @patternIndex - 1) + ''') AND '
						SET @valueData = SUBSTRING(@valueData, @patternIndex + 5, LEN(@valueData))
						SET @patternIndex = PATINDEX('% AND %', @valueData)
					END
					SET @clause = @clause + 'CONTAINS(' + @columnName + ', ''' + @valueData + '''))'
				END
				ELSE
					SET @clause = 'CONTAINS(' + @columnName + ', ''' + @valueData + ''')'
				SET @letterFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 17 --Comment Code
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				--check for range of codes
				IF (CHARINDEX('-',@valueData) = 0)
					SET @clause = 'PhaseCode.CodeNumber LIKE ''%' + @valueData + '%'''
				ELSE
					SET @clause = 'PhaseCode.CodeNumber BETWEEN ''' + REPLACE(@valueData, '-', ''' AND ''') + ''''
				SET @phaseCodeFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 18 -- Concern Text
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @columnName = 'ConcernResponse.ConcernText'
				SET @patternIndex = PATINDEX('% AND %', @valueData)
				IF @patternIndex > 0
				BEGIN
					SET @clause = '('
					WHILE @patternIndex > 0
					BEGIN
						SET @clause = @clause + 'CONTAINS(' + @columnName + ', ''' + LEFT(@valueData, @patternIndex - 1) + ''') AND '
						SET @valueData = SUBSTRING(@valueData, @patternIndex + 5, LEN(@valueData))
						SET @patternIndex = PATINDEX('% AND %', @valueData)
					END
					SET @clause = @clause + 'CONTAINS(' + @columnName + ', ''' + @valueData + '''))'
				END
				ELSE
					SET @clause = 'CONTAINS(' + @columnName + ', ''' + @valueData + ''')'
				SET @concernResponseFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 19 -- Response Text
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @columnName = 'ConcernResponse.ResponseText'
				SET @patternIndex = PATINDEX('% AND %', @valueData)
				IF @patternIndex > 0
				BEGIN
					SET @clause = '('
					WHILE @patternIndex > 0
					BEGIN
						SET @clause = @clause + 'CONTAINS(' + @columnName + ', ''' + LEFT(@valueData, @patternIndex - 1) + ''') AND '
						SET @valueData = SUBSTRING(@valueData, @patternIndex + 5, LEN(@valueData))
						SET @patternIndex = PATINDEX('% AND %', @valueData)
					END
					SET @clause = @clause + 'CONTAINS(' + @columnName + ', ''' + @valueData + '''))'
				END
				ELSE
					SET @clause = 'CONTAINS(' + @columnName + ', ''' + @valueData + ''')'
				SET @concernResponseFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 20 -- Unit
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @clause = 'LEFT(Project.UnitId, ' + CONVERT(VARCHAR(2),LEN(@valueData)) + ') = ''' + @valueData + ''''
				SET @projectFilter = 1
				IF @firstIncludeValue = 0
					SET @includeClause = @includeClause + ' OR '
				SET @includeClause = @includeClause + @clause
				SET @firstIncludeValue = 0
			END
			
			IF @fieldId = 21 --Comment Period Description
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @valueData = REPLACE(@valueData, '\', '\\')
				SET @valueData = REPLACE(@valueData, '%', '\%')
				SET @valueData = REPLACE(@valueData, '[', '\[')
				SET @valueData = REPLACE(@valueData, ']', '\]')
				SET @valueData = REPLACE(@valueData, '_', '\_')
				SET @valueData = REPLACE(@valueData, '^', '\^')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Phase.Name like ''%' + @valueData + '%'' escape ''\'''
				SET @phaseFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 22 --ProjectType
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Project.ProjectTypeId = ' + @valueData
				SET @projectFilter  = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 23 --Letter Type
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				IF @valueData = 6
				BEGIN
					SET @clause = 'Letter.LetterTypeId IN (1, 2, 3, 4, 5) '
				END
				ELSE IF @valueData = 7
				BEGIN
					SET @clause = 'Letter.LetterTypeId IN (3, 4, 5) '
				END
				ELSE IF @valueData = 8
				BEGIN
					SET @clause = 'Letter.LetterTypeId IN (2, 3) '
				END
				ELSE IF @valueData = 10
				BEGIN
					SET @clause = 'Letter.LetterTypeId IN (2, 3, 4, 5) '
				END
				ELSE
				BEGIN
					SET @clause = 'Letter.LetterTypeId = ' + @valueData
				END
				SET @letterFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 24 --Comment Period Type
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Phase.PhaseTypeId = ' + @valueData
				SET @phaseFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 25 --Project ID
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Project.ProjectNumber = ''' + @valueData + ''''
				SET @projectFilter  = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 26 --Project Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @valueData = REPLACE(@valueData, '\', '\\')
				SET @valueData = REPLACE(@valueData, '%', '\%')
				SET @valueData = REPLACE(@valueData, '[', '\[')
				SET @valueData = REPLACE(@valueData, ']', '\]')
				SET @valueData = REPLACE(@valueData, '_', '\_')
				SET @valueData = REPLACE(@valueData, '^', '\^')
				SET @valueData = 'like ''%' + @valueData + '%'' escape ''\'''
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = '(Project.Name ' + @valueData + ' OR Project.Description ' + @valueData + ')'
				SET @projectFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 27 --CR Code
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				--check for range of codes
				IF (CHARINDEX('-',@valueData) = 0)
				Begin
					SET @valueData = REPLACE(@valueData, '''', '''''')
					SET @valueData = REPLACE(@valueData, '\', '\\')
					SET @valueData = REPLACE(@valueData, '%', '\%')
					SET @valueData = REPLACE(@valueData, '[', '\[')
					SET @valueData = REPLACE(@valueData, ']', '\]')
					SET @valueData = REPLACE(@valueData, '_', '\_')
					SET @valueData = REPLACE(@valueData, '^', '\^')
					--  Andrei Barchai 12.02.2016 changes for CR 334
					--SET @clause = 'ConcernPhaseCode.CodeNumber LIKE ''%' + @valueData + '%'' escape ''\'''
					SET @clause = 'ConcernPhaseCode.CodeNumber LIKE ''' + @valueData + '%'' escape ''\'''
				END
				ELSE
					SET @clause = 'ConcernPhaseCode.CodeNumber BETWEEN ''' + REPLACE(@valueData, '-', ''' AND ''') + ''''
				SET @concernPhaseCodeFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
				
			END
			
			IF @fieldId = 28 --Preferred Contact Method
			BEGIN
				SET @valueData = @value.value('(./value/@value)[1]', 'VARCHAR(MAX)')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'Author.ContactMethod = ''' + @valueData + ''''
				IF @valueData = 'NULL'
					SET @clause = 'Author.ContactMethod is NULL'
				SET @authorFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 29 -- CR Assigned User First Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'ConcernUser.FirstName = ''' + @valueData + ''''
				SET @concernUserFilter = 1
				SET @concernResponseFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 30 -- CR Assigned User Last Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'ConcernUser.LastName = ''' + @valueData + ''''
				SET @concernUserFilter = 1
				SET @concernResponseFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 31 -- Comment Assigned User First Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'CommentUser.FirstName = ''' + @valueData + ''''
				SET @commentUserFilter = 1
				SET @commentFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			IF @fieldId = 32 -- Comment Assigned User Last Name
			BEGIN
				SET @valueData = @value.value('(./value/text())[1]', 'VARCHAR(MAX)')
				SET @valueData = REPLACE(@valueData, '''', '''''')
				SET @not = @value.value('(./value/@not)[1]', 'VARCHAR(MAX)')
				SET @clause = 'CommentUser.LastName = ''' + @valueData + ''''
				SET @commentUserFilter = 1
				SET @commentFilter = 1
				IF @not = 'true'
				BEGIN
					IF @firstExcludeValue = 0
						SET @excludeClause = @excludeClause + ' OR '
					SET @excludeClause = @excludeClause + @clause
					SET @firstExcludeValue = 0
				END
				ELSE
				BEGIN
					IF @firstIncludeValue = 0
						SET @includeClause = @includeClause + ' OR '
					SET @includeClause = @includeClause + @clause
					SET @firstIncludeValue = 0
				END
			END
			
			
			SET @values = @values.query('./value[position()>1]')
		END
		
		IF (@firstIncludeValue & @firstExcludeValue) = 0
		BEGIN
			IF @firstFilter = 0
				SET @where = @where + ' AND '
			SET @firstFilter = 0
			IF @firstIncludeValue = 0
			BEGIN
				SET @where = @where + '(' + @includeClause + ')'
				IF @firstExcludeValue = 0
					SET @where = @where + ' AND '
			END
			IF @firstExcludeValue = 0
				SET @where = @where + 'NOT(' + @excludeClause + ')'
		END
		SET @filters = @filters.query('./field[position()>1]')
	END
	IF @firstFilter = 0
		SET @where = @where + 'AND('
	SET @where = @where + 
		'Phase.Private = 0 OR Phase.PhaseId IN (SELECT  pmrl.PhaseId
		FROM dbo.PhaseMemberRole pmrl
			WHERE pmrl.UserId = '
        + CAST(@userId AS VARCHAR) + ' AND pmrl.Active = 1) OR '
        + CAST(@userId AS VARCHAR) + ' IN (SELECT usr.UserId
        FROM dbo.UserSystemRole usr
			WHERE usr.RoleId = 1 OR usr.RoleId = 2) '
    IF @firstFilter = 0
		SET @where = @where + ') '
    

	--Construct select clause
	SET @select = 'SELECT DISTINCT'
	DECLARE @columns XML
	SET @columns = @myDoc.query('/CustomReportData/columns/column')
	DECLARE @firstColumn BIT
	SET @firstColumn = 1
	WHILE (@columns.exist('./column') = 1)
	BEGIN
		IF @firstColumn = 0
			SET @select = @select + ', '
		SET @firstColumn = 0
		
		DECLARE @column XML
		SET @column = @columns.query('./column[1]')
		DECLARE @columnId INT
		SET @columnId = @column.value('(./column/@columnId)[1]', 'int')
		DECLARE @title VARCHAR(MAX)
		SET @title = ''
		
		IF @columnId = 7 --Unit
		BEGIN
			SET @select = @select + ' Unit.NAME AS ''Unit'''
			SET @unitColumn = 1
			SET @projectColumn = 1
		END
		
		IF @columnId = 8 --Project Status
		BEGIN
			SET @select = @select + ' ProjectStatus.Name AS ''Project Status'''
			SET @projectStatusColumn = 1
			SET @projectColumn = 1
		END
		
		IF @columnId = 9 --Project Name
		BEGIN
			SET @select = @select + ' Project.Name AS ''Project'''
			SET @projectColumn = 1 
		END
		
		IF @columnId = 10 --Project Description
		BEGIN
			SET @select = @select + ' Project.Description AS ''Project Description'''
			SET @projectColumn = 1
		END
		
		IF @columnId = 11 --Project Type
		BEGIN
			SET @select = @select + ' ProjectType.Name AS ''Project Type'''
			SET @projectColumn = 1
			SET @projectTypeColumn = 1
		END
		
		IF @columnId = 12 --Project Number
		BEGIN
			SET @select = @select + ' Project.ProjectNumber AS ''Project Number'''
			SET @projectColumn = 1
		END
		
		IF @columnId = 13 --Analysis Type
		BEGIN
			SET @select = @select + ' AnalysisType.Name AS ''Analysis Type'''
			SET @analysisTypeColumn = 1
			SET @projectColumn = 1
		END
		
		IF @columnId = 14 --Comment Rule
		BEGIN
			SET @select = @select + ' CommentRule.Name AS ''Comment Rule'''
			SET @commentRuleColumn = 1
			SET @projectColumn = 1
		END
		
		IF @columnId = 15 --project purpose
		BEGIN
			SET @select = @select + ' Purpose.Name AS ''Project Purpose'''
			SET @purposeColumn = 1
			SET @projectColumn = 1
		END
		
		IF @columnId = 16 --project activity
		BEGIN
			SET @select = @select + ' Activity.Name AS ''Project Activity'''
			SET @activityColumn = 1
			SET @projectColumn = 1
		END
		
		IF @columnId = 17 --Comment Period
		BEGIN
			SET @select = @select + ' Phase.Name AS ''Comment Period'''
			SET @phaseColumn = 1
		END
		
		IF @columnId = 18 --Archived
		BEGIN
			SET @select = @select + ' Phase.Locked AS ''Archived'''
			SET @phaseColumn = 1
		END
		
		IF @columnId = 19 --Comment Start date
		BEGIN
			SET @select = @select + ' Phase.CommentStart AS ''Comment Start Date'''
			SET @phaseColumn = 1
		END
		
		IF @columnId = 20 --Comment End date
		BEGIN
			SET @select = @select + ' Phase.CommentEnd AS ''Comment End Date'''
			SET @phaseColumn = 1
		END
		
		IF @columnId = 21 --Comment Period Type
		BEGIN
			SET @select = @select + ' PhaseType.Name AS ''Comment Period Type'''
			SET @phaseTypeColumn = 1
			SET @phaseColumn = 1
		END
		
		IF @columnId = 22 --Letter Status
		BEGIN
			SET @select = @select + ' LetterStatus.Name AS ''Letter Status'''
			SET @letterStatusColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 23 --Letter Text
		BEGIN
			SET @select = @select + ' Letter.Text AS ''Letter Text'''
			SET @letterColumn = 1
		END
		
		IF @columnId = 24 --Within Comment Period
		BEGIN
			SET @select = @select + ' Letter.WithinCommentPeriod AS ''Within Comment Period'''
			SET @letterColumn = 1
		END
		
		IF @columnId = 25 --Letter Type
		BEGIN
			SET @select = @select + ' LetterType.Name AS ''Letter Type'''
			SET @letterTypeColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 26 --Date Submitted
		BEGIN
			SET @select = @select + ' Letter.DateSubmitted AS ''Date Submitted'''
			SET @letterColumn = 1
		END
		
		IF @columnId = 27 --Date Entered
		BEGIN
			SET @select = @select + ' Letter.DateEntered AS ''Date Entered'''
			SET @letterColumn = 1
		END
		
		IF @columnId = 28 --Delivery Type
		BEGIN
			SET @select = @select + ' DeliveryType.Name AS ''Delivery Type'''
			SET @deliveryTypeColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 29 --Common Interest Class
		BEGIN
			SET @select = @select + ' CommonInterestClass.Name AS ''Common Interest Class'''
			SET @commonInterestClassColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 30 --Letter Number
		BEGIN
			SET @select = @select + ' Letter.LetterSequence AS ''Letter Number'''
			SET @letterColumn = 1
		END
		
		IF @columnId = 31 --Form Set
		BEGIN
			SET @select = @select + ' FormSet.Name AS ''Form Set'''
			SET @formSetColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 32 --Published To Reading Room
		BEGIN
			SET @select = @select + ' Letter.PublishToReadingRoom AS ''Published To Reading Room'''
			SET @letterColumn = 1
		END
		
		IF @columnId = 33 --Unpublished Reason
		BEGIN
			SET @select = @select + ' UnpublishedReason.Name AS ''Unpublished Reason'''
			SET @unpublishedReasonColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 34 --Early Action Status
		BEGIN
			SET @select = @select + ' EarlyActionStatus.Name AS ''Early Action Status'''
			SET @earlyActionStatusColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 35 --Sender
		BEGIN
			SET @select = @select + ' Author.Sender AS ''Sender'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 36 --First Name
		BEGIN
			SET @select = @select + ' Author.FirstName AS ''First Name'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 37 --Last Name
		BEGIN
			SET @select = @select + ' Author.LastName AS ''Last Name'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 38 --Middle Name
		BEGIN
			SET @select = @select + ' Author.MiddleName AS ''Middle Name'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 39 --Title
		BEGIN
			SET @select = @select + ' Author.Title AS ''Title'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 40 --Address
		BEGIN
			SET @select = @select + ' ISNULL(Author.Address1,'''') + '' '' + ISNULL(Author.Address2,'''') AS ''Address'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 41 --City
		BEGIN
			SET @select = @select + ' Author.City AS ''City'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 42 --State
		BEGIN
			SET @select = @select + ' Author.StateId AS ''State'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 43 --ZIP Code
		BEGIN
			SET @select = @select + ' Author.ZipPostal AS ''ZIP Code'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 44 --Province
		BEGIN
			SET @select = @select + ' Author.ProvinceRegion AS ''Province'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 45 --Country
		BEGIN
			SET @select = @select + ' Country.Name AS ''Country'''
			SET @countryColumn = 1
			SET @authorColumn = 1
		END
		
		IF @columnId = 46 --Email
		BEGIN
			SET @select = @select + ' Author.Email AS ''E-Mail'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 47 --Phone
		BEGIN
			SET @select = @select + ' Author.Phone AS ''Phone'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 48 --Official Representative Type
		BEGIN
			SET @select = @select + ' OfficialRepresentativeType.Name AS ''Official Representative Type'''
			SET @officialRepresentativeTypeColumn = 1
			SET @authorColumn = 1
		END
		
		IF @columnId = 49 --Organization
		BEGIN
			SET @select = @select + ' Organization.Name AS ''Organization'''
			SET @organizationColumn = 1
			SET @authorColumn = 1
		END
		
		IF @columnId = 50 --Organization Type
		BEGIN
			SET @select = @select + ' OrganizationType.Name AS ''Organization Type'''
			SET @organizationColumn = 1
			SET @organizationTypeColumn = 1
			SET @authorColumn = 1
		END
		
		IF @columnId = 51 --Annotation
		BEGIN
			SET @select = @select + ' Comment.Annotation AS ''Annotation'''
			SET @commentColumn = 1
		END
		
		--52 'Concern Response' is not mapped
		
		IF @columnId = 53 --No Response reason
		BEGIN
			SET @select = @select + ' NoResponseReason.Name AS ''No Response reason'''
			SET @noResponseReasonColumn = 1
			SET @commentColumn = 1
		END
		
		IF @columnId = 54 --Sample Statement
		BEGIN
			SET @select = @select + ' Comment.SampleStatement AS ''Sample Statement'''
			SET @commentColumn = 1
		END
		
		IF @columnId = 55 --Comment Number
		BEGIN
			SET @select = @select + ' Comment.CommentNumber AS ''Comment Number'''
			SET @commentColumn = 1
		END
		
		IF @columnId = 56 --Text
		BEGIN
			SET @select = @select + ' ''Comment Text'' = LEFT(CompleteCommentText.list, LEN(CompleteCommentText.list) - 5)'
			SET @commentTextColumn = 1
			SET @commentColumn = 1
		END
		
		IF @columnId = 57 --Code Name
		BEGIN
			SET @select = @select + ' PhaseCode.CodeName AS ''Comment Code Name'''
			SET @phaseCodeColumn = 1
			SET @commentColumn = 1
		END
		
		IF @columnId = 58 --Code Number
		BEGIN
			SET @select = @select + ' PhaseCode.CodeNumber AS ''Comment Code Number'''
			SET @phaseCodeColumn = 1
			SET @commentColumn = 1
		END
		
		IF @columnId = 59 --Code Category
		BEGIN
			SET @select = @select + ' CodeCategory.Name AS ''Comment Code Category'''
			SET @codeCategoryColumn = 1
			SET @phaseCodeColumn = 1
		END
		
		IF @columnId = 60 --Sequence
		BEGIN
			SET @select = @select + ' ConcernResponse.Sequence AS ''Concern Response Sequence'''
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 61 --CR Number
		BEGIN
			SET @select = @select + ' ConcernResponse.ConcernResponseNumber AS ''CR Number'''
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 62 --Concern Text
		BEGIN
			SET @select = @select + ' ConcernResponse.ConcernText AS ''Concern Text'''
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 63 --Response Text
		BEGIN
			SET @select = @select + ' ConcernResponse.ResponseText AS ''Response Text'''
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 64 --Comment Count
		BEGIN
			SET @select = @select + ' ConcernResponse.CommentCount AS ''Comment Count'''
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 65 --Concern Response Code Name
		BEGIN
			SET @select = @select + ' ConcernPhaseCode.CodeName AS ''Concern Response Code Name'''
			SET @concernPhaseCodeColumn = 1
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 66 --Concern Response Code Number
		BEGIN
			SET @select = @select + ' ConcernPhaseCode.CodeNumber AS ''Concern Response Code Number'''
			SET @concernPhaseCodeColumn = 1
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 67 --Concern Response Code Number
		BEGIN
			SET @select = @select + ' ConcernCodeCategory.Name AS ''Concern Response Code Category'''
			SET @concernCodeCategory = 1
			SET @concernPhaseCodeColumn = 1
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 68 -- CR Status
		BEGIN
			SET @select = @select + ' ConcernResponseStatus.Name AS ''CR Status'''
			SET @concernResponseStatusColumn = 1
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 69 --CR Assigned To
		BEGIN
			SET @select = @select + ' ConcernUser.LastName + '', '' + ConcernUser.FirstName AS ''CR Assigned To'''
			SET @concernUserColumn = 1
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 70 --Label
		BEGIN
			SET @select = @select + ' ConcernResponse.Label AS ''Label'''
			SET @concernResponseColumn = 1
		END
		
		IF @columnId = 71 --Preferred Contact Method
		BEGIN
			SET @select = @select + ' Author.ContactMethod AS ''Preferred Contact Method'''
			SET @authorColumn = 1
		END
		
		IF @columnId = 72 --Letter Objection Number
		BEGIN
			SET @select = @select + ' (LetterObjection.FiscalYear + '' '' + LetterObjection.Region + '' '' + LetterObjection.Forest + '' '' + ' +
				' REPLICATE(''0'',7-LEN(CONVERT(VARCHAR(7),LetterObjection.ObjectionNumber))) +' +
				' CONVERT(VARCHAR(7),LetterObjection.ObjectionNumber) + '' '' + RegulationNumber) AS ''Objection Number'''
			SET @letterObjectionColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 73 --Comment Assigned To
		BEGIN
			SET @select = @select + ' CommentUser.LastName + '', '' + CommentUser.FirstName AS ''Comment Assigned To'''
			SET @commentUserColumn = 1
			SET @commentColumn = 1
		END
		
		IF @columnId = 74 --Reviewing Officer
		BEGIN
			SET @select = @select + ' LetterObjection.SignerLastName + '', '' + LetterObjection.SignerFirstName AS ''Reviewing Officer'''
			SET @letterObjectionColumn = 1
			SET @letterColumn = 1
		END
		
		IF @columnId = 75 --Reviewing Officer Title
		BEGIN
			SET @select = @select + ' ObjectionDecisionMaker.Name AS ''Reviewing Officer Title'''
			SET @letterColumn = 1
			SET @letterObjectionColumn = 1
			SET @objectionDecisionMakerColumn = 1
		END
			
		IF @columnId = 76 --Objection Response Date
		BEGIN
			SET @select = @select + ' LetterObjection.ResponseDate AS ''Objection Response Date'''
			SET @letterColumn = 1
			SET @letterObjectionColumn = 1
		END
		
		IF @columnId = 77 --Review Status
		BEGIN
			SET @select = @select + ' ObjectionReviewStatus.Name AS ''Review Status'''
			SET @letterColumn = 1
			SET @letterObjectionColumn = 1
			SET @objectionReviewStatusColumn = 1
		END
		
		IF @columnId = 78 --Overall Objection Outcome
		BEGIN
			SET @select = @select + ' ObjectionOutcome.Name As ''Overall Objection Outcome'''
			SET @letterColumn = 1
			SET @letterObjectionColumn = 1
			SET @objectionOutcomeColumn = 1
		END
			
		IF @columnId = 79 --Objector Meeting Information
		BEGIN
			SET @select = @select + ' LetterObjection.MeetingInfo AS ''Objector Meeting Information'''
			SET @letterColumn = 1
			SET @letterObjectionColumn = 1
		END
			
		IF @columnId = 80 --Auto Markup
		BEGIN
			SET @select = @select + ' Term.Name AS ''Auto Markup'''
			SET @letterColumn = 1
			SET @letterTermConfirmationColumn = 1
			SET @termColumn = 1
		END
		
		SET @columns = @columns.query('./column[position()>1]')
	END



	--Construct order by clause
	DECLARE @sortOrder VARCHAR(MAX)
	SET @sortOrder = ''
	DECLARE @sortColumns XML
	SET @sortColumns = @myDoc.query('/CustomReportData/columns/column[@sortOrder>0]')
	DECLARE @firstSort BIT
	SET @firstSort = 1
	DECLARE @finished BIT
	SET @finished = 0
	DECLARE @sortSearch INT
	SET @sortSearch = 1
	WHILE (@finished = 0)
	BEGIN
		SET @columnName = ''
		DECLARE @remainingColumns XML
		SET @remainingColumns = @sortColumns
		
		WHILE (@remainingColumns.exist('./column') = 1)
		BEGIN
			SET @column = @remainingColumns.query('./column[1]')
			DECLARE @sortNum INT
			SET @sortNum = @column.value('(./column/@sortOrder)[1]', 'int')
			IF NOT (@sortNum = @sortSearch)
			BEGIN
				SET @remainingColumns = @remainingColumns.query('./column[position()>1]')
				CONTINUE
			END
			
			SET @columnId = @column.value('(./column/@columnId)[1]', 'int')
			DECLARE @sortDirection VARCHAR(4)
			SET @sortDirection = 'ASC'
			IF @column.value('(./column/@ascending)[1]', 'VARCHAR(6)') = 'false'
				SET @sortDirection = 'DESC'
			IF @columnId = 7 --Unit
				SET @columnName = '[Unit]'
			
			IF @columnId = 8 --Project Status
				SET @columnName = '[Project Status]'
			
			IF @columnId = 9 --Project Name
				SET @columnName = '[Project]'
			
			IF @columnId = 10 --Project Description
				SET @columnName = '[Project Description]'
			
			IF @columnId = 11 --Project Type
				SET @columnName = '[Project Type]'
			
			IF @columnId = 12 --Project Number
				SET @columnName = '[Project Number]'
			
			IF @columnId = 13 --Analysis Type
				SET @columnName = '[Analysis Type]'
			
			IF @columnId = 14 --Comment Rule
				SET @columnName = '[Comment Rule]'
			
			IF @columnId = 15 --project purpose
				SET @columnName = '[Project Purpose]'
			
			IF @columnId = 16 --project activity
				SET @columnName = '[Project Activity]'
			
			IF @columnId = 17 --Comment Period
				SET @columnName = '[Comment Period]'
			
			IF @columnId = 18 --Archived
				SET @columnName = '[Archived]'
			
			IF @columnId = 19 --Comment Start date
				SET @columnName = '[Comment Start Date]'
			
			IF @columnId = 20 --Comment End date
				SET @columnName = '[Comment End Date]'
			
			IF @columnId = 21 --Comment Period Type
				SET @columnName = '[Comment Period Type]'
			
			IF @columnId = 22 --Letter Status
				SET @columnName = '[Letter Status]'
			
			IF @columnId = 23 --Letter Text
				SET @columnName = '[Letter Text]'
			
			IF @columnId = 24 --Within Comment Period
				SET @columnName = '[Within Comment Period]'
			
			IF @columnId = 25 --Letter Type
				SET @columnName = '[Letter Type]'
			
			IF @columnId = 26 --Date Submitted
				SET @columnName = '[Date Submitted]'
			
			IF @columnId = 27 --Date Entered
				SET @columnName = '[Date Entered]'
			
			IF @columnId = 28 --Delivery Type
				SET @columnName = '[Delivery Type]'
			
			IF @columnId = 29 --Common Interest Class
				SET @columnName = '[Common Interest Class]'
			
			IF @columnId = 30 --Letter Number
				SET @columnName = '[Letter Number]'
			
			IF @columnId = 31 --Form Set
				SET @columnName = '[Form Set]'
			
			IF @columnId = 32 --Published To Reading Room
				SET @columnName = '[Published To Reading Room]'
			
			IF @columnId = 33 --Unpublished Reason
				SET @columnName = '[Unpublished Reason]'
			
			IF @columnId = 34 --Early Action Status
				SET @columnName = '[Early Action Status]'
			
			IF @columnId = 35 --Sender
				SET @columnName = '[Sender]'
			
			IF @columnId = 36 --First Name
				SET @columnName = '[First Name]'
			
			IF @columnId = 37 --Last Name
				SET @columnName = '[Last Name]'
			
			IF @columnId = 38 --Middle Name
				SET @columnName = '[Middle Name]'
			
			IF @columnId = 39 --Title
				SET @columnName = '[Title]'
			
			IF @columnId = 40 --Address
				SET @columnName = '[Address]'
			
			IF @columnId = 41 --City
				SET @columnName = '[City]'
			
			IF @columnId = 42 --State
				SET @columnName = '[State]'
			
			IF @columnId = 43 --ZIP Code
				SET @columnName = '[ZIP Code]'
			
			IF @columnId = 44 --Province
				SET @columnName = '[Province]'
			
			IF @columnId = 45 --Country
				SET @columnName = '[Country]'
			
			IF @columnId = 46 --Email
				SET @columnName = '[E-Mail]'
			
			IF @columnId = 47 --Phone
				SET @columnName = '[Phone]'
			
			IF @columnId = 48 --Official Representative Type
				SET @columnName = '[Official Representative Type]'
			
			IF @columnId = 49 --Organization
				SET @columnName = '[Organization]'
			
			IF @columnId = 50 --Organization Type
				SET @columnName = '[Organization Type]'
			
			IF @columnId = 51 --Annotation
				SET @columnName = '[Annotation]'
			
			--52 'Concern Response' is not mapped
			
			IF @columnId = 53 --No Response reason
				SET @columnName = '[No Response reason]'
			
			IF @columnId = 54 --Sample Statement
				SET @columnName = '[Sample Statement]'
			
			IF @columnId = 55 --Comment Number
				SET @columnName = '[Comment Number]'
			
			IF @columnId = 56 --Text
				SET @columnName = '[Comment Text]'
			
			IF @columnId = 57 --Code Name
				SET @columnName = '[Comment Code Name]'
			
			IF @columnId = 58 --Code Number
				SET @columnName = '[Comment Code Number]'
			
			IF @columnId = 59 --Code Category
				SET @columnName = '[Comment Code Category]'
			
			IF @columnId = 60 --Sequence
				SET @columnName = '[Concern Response Sequence]'
			
			IF @columnId = 61 --CR Number
				SET @columnName = '[CR Number]'
			
			IF @columnId = 62 --Concern Text
				SET @columnName = '[Concern Text]'
			
			IF @columnId = 63 --Response Text
				SET @columnName = '[Response Text]'
			
			IF @columnId = 64 --Comment Count
				SET @columnName = '[Comment Count]'
			
			IF @columnId = 65 --Concern Response Code Name
				SET @columnName = '[Concern Response Code Name]'
			
			IF @columnId = 66 --Concern Response Code Number
				SET @columnName = '[Concern Response Code Number]'
			
			IF @columnId = 67 --Concern Response Code Category
				SET @columnName = '[Concern Response Code Category]'
			
			IF @columnId = 68 -- CR Status
				SET @columnName = '[CR Status]'
			
			IF @columnId = 69 -- CR Assigned To
				SET @columnName = '[CR Assigned To]'
			
			IF @columnId = 70 -- Label
				SET @columnName = '[Label]'
				
			IF @columnId = 72 --Objection Number
				SET @columnName = '[Objection Number]'
				
			IF @columnId = 73 --Comment Assigned To
				SET @columnName = '[Comment Assigned To]'
				
			IF @columnId = 74 --Reviewing Officer
				SET @columnName = '[Reviewing Officer]'
				
			IF @columnId = 75 --Reviewing Officer Title
				SET @columnName = '[Reviewing Officer Title]'
				
			IF @columnId = 76 --Objection Response Date
				SET @columnName = '[Objection Response Date]'
				
			IF @columnId = 77 --Review Status
				SET @columnName = '[Review Status]'
				
			IF @columnId = 78 --Overall Objection Outcome
				SET @columnName = '[Overall Objection Outcome]'
				
			IF @columnId = 79 --Objector Meeting Information
				SET @columnName = '[Objector Meeting Information]'
				
			IF @columnId = 80 --Auto Markup
				SET @columnName = '[Auto Markup]'
				
			IF NOT (@columnName  = '')
			BEGIN
				IF @firstSort = 0
					SET @sortOrder = @sortOrder + ', '
				SET @firstSort = 0
				SET @sortOrder = @sortOrder + @columnName + ' ' + @sortDirection
				BREAK
			END
		END --finished looking through columns
		IF @columnName = ''
			BREAK
		SET @sortSearch = @sortSearch + 1
	END





	--construct from clause
	DECLARE @from VARCHAR(MAX)
	SET @from = 'FROM'
	DECLARE @firstTable BIT
	SET @firstTable = 1

	--Check to see if project table or related tables are required
	SET @projectFilter = @projectFilter | @projectActivityFilter | @projectPurposeFilter
	IF (@projectFilter | @projectColumn) = 1
	BEGIN
		SET @from = @from + ' dbo.Project'
		SET @firstTable = 0
	END
	IF @projectActivityFilter = 1
		SET @from = @from + ' INNER JOIN dbo.ProjectActivity ON dbo.ProjectActivity.ProjectId = dbo.Project.ProjectId'
	IF @projectPurposeFilter = 1
		SET @from = @from + ' INNER JOIN dbo.ProjectPurpose ON dbo.ProjectPurpose.ProjectId = dbo.Project.ProjectId'
	IF @unitColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.Unit ON Unit.UnitId = Project.UnitId'
	IF @projectStatusColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ProjectStatus ON ProjectStatus.ProjectStatusId = Project.ProjectId'
	IF @projectTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ProjectType ON ProjectType.ProjectTypeId = Project.ProjectTypeId'
	IF @analysisTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.AnalysisType ON AnalysisType.AnalysisTypeId = Project.AnalysisTypeId'
	IF @commentRuleColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.CommentRule ON CommentRule.CommentRuleId = Project.CommentRuleId'
	IF @purposeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ProjectPurpose ON ProjectPurpose.ProjectId = Project.ProjectId'
		+ ' LEFT OUTER JOIN dbo.Purpose ON Purpose.PurposeId = ProjectPurpose.PurposeId'
	IF @activityColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ProjectActivity ON ProjectActivity.ProjectId = Project.ProjectId'
		+ ' LEFT OUTER JOIN dbo.Activity ON Activity.ActivityId = ProjectActivity.ActivityId'


	--Check if Phase table or related tables are required
	DECLARE @lowerFilters BIT
	SET @lowerFilters = @letterFilter | @authorFilter | @organizationFilter | @commentFilter | @commentCodeFilter | 
		@phaseCodeFilter | @concernResponseFilter
	SET @phaseFilter = @phaseFilter | (@projectFilter & @lowerFilters)
	DECLARE @lowerColumns BIT
	SET @lowerColumns = @letterColumn | @authorColumn | @commentColumn | @concernResponseColumn
	SET @phaseColumn = @phaseColumn | ((@projectColumn | @projectFilter) & (@lowerFilters | @lowerColumns)) 
	IF (@phaseFilter | @phaseColumn) = 1
	BEGIN
		IF @firstTable = 0
		BEGIN
			IF (@phaseFilter & @projectFilter) = 1
				SET @from = @from + ' INNER JOIN'
			ELSE IF @phaseFilter = 1
				SET @from = @from + ' RIGHT OUTER JOIN'
			ELSE
				SET @from = @from + ' LEFT OUTER JOIN'
		END
		SET @from = @from + ' dbo.Phase'
		IF @firstTable = 0
			SET @from = @from + ' ON dbo.Phase.ProjectId = dbo.Project.ProjectId'
		SET @firstTable = 0
	END
	IF @phaseTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.PhaseType ON PhaseType.PhaseTypeId = Phase.PhaseTypeId'

	--Check if Letter Table or related tables are required
	--Letter table is required if it is directly required or 
	--if either the Author group or Comment group is required (as Phase table is always required for security)
	--Letter column is required if any two groups are filters or columns

		
	IF (@authorFilter | @organizationFilter) = 1
		SET @letterFilter = 1
	ELSE IF @authorColumn = 1
		SET @letterColumn = 1
		
	IF (@commentFilter | @commentCodeFilter | @phaseCodeFilter | @concernResponseFilter) = 1
		SET @letterFilter = 1
	ELSE IF (@commentColumn | @phaseCodeColumn | @concernResponseColumn) = 1
		SET @letterColumn = 1

	IF (@letterFilter | @letterColumn) = 1
	BEGIN
		IF @firstTable = 0
			IF (@letterFilter & @phaseFilter) = 1
				SET @from = @from + ' INNER JOIN'
			ELSE IF @letterFilter = 1
				SET @from = @from + ' RIGHT OUTER JOIN'
			ELSE
				SET @from = @from + ' LEFT OUTER JOIN'
		SET @from = @from + ' dbo.Letter'
		IF @firstTable = 0
			SET @from = @from + ' ON dbo.Letter.PhaseId = dbo.Phase.PhaseId'
		SET @firstTable = 0
	END

	IF @letterStatusColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.LetterStatus ON LetterStatus.LetterStatusId = Letter.LetterStatusId'
	IF @letterTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.LetterType ON LetterType.LetterTypeId = Letter.LetterTypeId'
	IF @deliveryTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.DeliveryType ON DeliveryType.DeliveryTypeId = Letter.DeliveryTypeId'
	IF @commonInterestClassColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.CommonInterestClass ON CommonInterestClass.CommonInterestClassId = Letter.CommonInterestClassId'
	IF @formSetColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.FormSet ON FormSet.FormSetId = Letter.FormSetId'
	IF @unpublishedReasonColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.UnpublishedReason ON UnpublishedReason.UnpublishedReasonId = Letter.UnpublishedReasonId'
	IF @earlyActionStatusColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.EarlyActionStatus ON EarlyActionStatus.StatusId = Letter.EarlyActionStatusId'
	IF @letterObjectionFilter = 1
		SET @from = @from + ' INNER JOIN dbo.LetterObjection ON LetterObjection.LetterObjectionId = Letter.LetterObjectionId'
	ELSE IF @letterObjectionColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.LetterObjection ON LetterObjection.LetterObjectionId = Letter.LetterObjectionId'
	IF @objectionDecisionMakerColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ObjectionDecisionMaker ON LetterObjection.ObjectionDecisionMakerId = ObjectionDecisionMaker.ObjectionDecisionMakerId'
	IF @objectionOutcomeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ObjectionOutcome ON LetterObjection.OutcomeId = ObjectionOutcome.ObjectionOutcomeId'
	IF @objectionReviewStatusColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ObjectionReviewStatus ON LetterObjection.ReviewStatusId = ObjectionReviewStatus.ObjectionReviewStatusId'
	IF @letterTermConfirmationColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.LetterTermConfirmation ON LetterTermConfirmation.LetterId = Letter.LetterId'
	IF @termColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.Term ON Term.TermId = LetterTermConfirmation.TermId'

	--Check if Author table or related tables are required
	SET @authorFilter = @authorFilter | (@organizationFilter & @letterFilter)
	IF (@authorFilter | @authorColumn) = 1
	BEGIN
		IF @firstTable = 0
			IF (@authorFilter & @letterFilter) = 1
				SET @from = @from + ' INNER JOIN'
			ELSE IF @authorFilter = 1
				SET @from = @from + ' RIGHT OUTER JOIN'
			ELSE
				SET @from = @from + ' LEFT OUTER JOIN'
		SET @from = @from + ' dbo.Author'
		IF @firstTable = 0
			SET @from = @from + ' ON dbo.Author.LetterId = dbo.Letter.LetterId'
		SET @firstTable = 0
	END

	IF @countryColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.Country ON Author.CountryId = Country.CountryId'
	IF @officialRepresentativeTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.OfficialRepresentativeType'
			+ ' ON OfficialRepresentativeType.OfficialRepresentativeTypeId = Author.OfficialRepresentativeTypeId'

	--Check if Organization table and related tables are required
	IF (@organizationFilter | @organizationColumn) = 1
	BEGIN
		IF @firstTable = 0
			IF (@organizationFilter & @authorFilter) = 1
				SET @from = @from + ' INNER JOIN'
			ELSE IF @organizationFilter = 1
				SET @from = @from + ' RIGHT OUTER JOIN'
			ELSE
				SET @from = @from + ' LEFT OUTER JOIN'
		SET @from = @from + ' dbo.Organization'
		IF @firstTable = 0
			SET @from = @from + ' ON dbo.Organization.OrganizationId = dbo.Author.OrganizationId'
		SET @firstTable = 0
	END

	IF @organizationTypeColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.OrganizationType'
			+ ' ON OrganizationType.OrganizationTypeId = Organization.OrganizationTypeId'

	--Check if Comment table is required
	--Comment table is required if it is needed directly 
	--or if Comment group or ConcernResponse table is required (as phas table is always required for security)
	IF @concernResponseFilter = 1
		SET @commentFilter = 1
	ELSE IF @concernResponseColumn = 1
		SET @commentColumn = 1
	IF @phaseCodeFilter = 1
		SET @commentFilter = 1
	ELSE IF @phaseCodeColumn = 1
		SET @commentColumn = 1
		
	IF (@commentFilter | @commentColumn) = 1
	BEGIN
		IF @firstTable = 0
			IF (@commentFilter & @letterFilter) = 1
				SET @from = @from + ' INNER JOIN'
			ELSE IF @commentFilter = 1
				SET @from = @from + ' RIGHT OUTER JOIN'
			ELSE
				SET @from = @from + ' LEFT OUTER JOIN'
		SET @from = @from + ' dbo.Comment'
		IF @firstTable = 0
			SET @from = @from + ' ON Comment.LetterId = Letter.LetterId'
		SET @firstTable = 0
		IF @commentTextColumn = 1
			SET @from = @from + 
			' CROSS APPLY ( SELECT    [Text] + ''[...]''
							  FROM      dbo.CommentText ctL
							  WHERE     ctL.CommentId = Comment.CommentId
							  ORDER BY  ctl.CommentTextId 
							FOR
							  XML PATH('''')
							) CompleteCommentText ( list )'
		IF @noResponseReasonColumn = 1
			SET @from = @from + ' LEFT OUTER JOIN dbo.NoResponseReason ON Comment.NoResponseReasonId = NoResponseReason.NoResponseReasonId'
		IF (@commentUserFilter | @commentUserColumn) = 1
		BEGIN
			IF @commentUserFilter = 1
				SET @from = @from + ' INNER'
			ELSE
				SET @from = @from + ' LEFT OUTER'
			SET @from = @from + ' JOIN dbo.[User] as CommentUser on Comment.UserId = CommentUser.UserId'
		END
	END

	--Check if CommentCode table is required
	IF (@phaseCodeFilter | @phaseCodeColumn) = 1
	BEGIN
		DECLARE @join VARCHAR(20)
		SET @join = ''
		IF @firstTable = 0
		BEGIN
			IF (@phaseCodeFilter & @commentFilter) = 1
				SET @join = ' INNER JOIN'
			ELSE IF @phaseCodeFilter = 1
				SET @join = ' RIGHT OUTER JOIN'
			ELSE
				SET @join = ' LEFT OUTER JOIN'
			SET @from = @from + @join + ' dbo.CommentCode ON CommentCode.CommentId = Comment.CommentId' + @join
		END
		SET @from = @from + ' dbo.PhaseCode'
		IF @firstTable = 0
			SET @from = @from + ' ON PhaseCode.PhaseCodeId = CommentCode.PhaseCodeId'
		SET @firstTable = 0
	END

	IF @codeCategoryColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.CodeCategory ON CodeCategory.CodeCategoryId = PhaseCode.CodeCategoryId'

	--Check if ConcernResponse table is required
	IF (@concernResponseFilter | @concernResponseColumn) = 1
	BEGIN
		IF @firstTable = 0
			IF (@concernResponseFilter & @commentFilter) = 1
				SET @from = @from + ' INNER JOIN'
			ELSE IF @concernResponseFilter = 1
				SET @from = @from + ' RIGHT OUTER JOIN'
			ELSE
				SET @from = @from + ' LEFT OUTER JOIN'
		SET @from = @from + ' dbo.ConcernResponse'
		IF @firstTable = 0
			SET @from = @from + ' ON ConcernResponse.ConcernResponseId = Comment.ConcernResponseId'
		SET @firstTable = 0
	END

	IF @concernResponseStatusColumn = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.ConcernResponseStatus'
			+ ' ON ConcernResponseStatus.ConcernResponseStatusId = ConcernResponse.ConcernResponseStatusId'

	IF (@concernPhaseCodeFilter | @concernPhaseCodeColumn) = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.PhaseCode AS ConcernPhaseCode '
			+ 'ON ConcernPhaseCode.PhaseCodeId = ConcernResponse.PhaseCodeId'

	IF @concernCodeCategory = 1
		SET @from = @from + ' LEFT OUTER JOIN dbo.CodeCategory AS ConcernCodeCategory '
			+ 'ON ConcernCodeCategory.CodeCategoryId = ConcernPhaseCode.CodeCategoryId'
	
	IF (@concernUserColumn | @concernUserFilter) = 1
	BEGIN
		IF @ConcernUserFilter = 1
			SET @from = @from + ' INNER'
		ELSE
			SET @from = @from + ' LEFT OUTER'
		SET @from = @from + ' JOIN dbo.[User] AS ConcernUser ON ConcernUser.UserId = ConcernResponse.UserId'
	END
		

	DECLARE @sql VARCHAR(MAX)
	SET @sql = @select + ' ' + @from + ' ' + @where
	--SELECT @sql AS 'SQL'
	IF @isCount = 1
		EXEC ('SELECT COUNT(*) FROM (' + @sql + ') as t')
	ELSE IF @numRows > 0
		EXECUTE dbo.spsRowPagerDistinct @SQL = @sql, @RecordsPerPage = @numRows, @Page = @pageNum, @ID = '', @sortBy = @sortOrder
	ELSE
		EXEC (@sql + ' ORDER BY ' + @sortOrder)
END
GO


























GRANT EXECUTE ON  [dbo].[spsCustomReport] TO [carauser]
GO
