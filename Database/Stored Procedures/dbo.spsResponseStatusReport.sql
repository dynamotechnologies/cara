
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists comments and codes that do not have responses associated with them */

CREATE PROCEDURE [dbo].[spsResponseStatusReport]
( 
    @phaseId INT = -1,
    @pageNum INT = 1 ,
    @numRows INT = -1 ,
    @totalRows INT OUT
)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;
    
    DECLARE @startRow INT
    DECLARE @endRow INT
    
    SELECT @totalRows = COUNT(*)
    FROM dbo.Comment c
              LEFT OUTER JOIN dbo.ConcernResponse cr ON c.ConcernResponseId = cr.ConcernResponseId
              INNER JOIN dbo.CommentCode cc ON c.CommentId = cc.CommentId
              INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
       WHERE (cr.ConcernResponseStatusId <> 4 OR cr.ConcernResponseStatusId IS NULL) AND l.PhaseId = @phaseId
       
       IF @numRows <= 0
       BEGIN
              SET @startRow = 1
              SET @endRow = @totalRows
       END
       ELSE
       BEGIN
              SET @startRow = ((@pageNum - 1) * @numRows) + 1
              SET @endRow = @pageNum * @numRows
       END
              
    ;

    WITH    ReportResults
          AS (
              SELECT    Associated, ConcernResponseId, StatusName, CommentId,
                        CommentNumber, CommentText, CodeNumber, CodeName,
                        LetterSequence, ConcernResponseNumber,
                        ROW_NUMBER() OVER (ORDER BY Associated DESC, ConcernResponseId, StatusName, LetterSequence,CommentNumber, CodeNumber)
                        AS RowNumber
              FROM      (
                         SELECT CAST(1 AS BIT) AS Associated,
                                cr.ConcernResponseId, crs.Name AS StatusName,
                                c.CommentId, c.CommentNumber,
                                CommentText = LEFT(ol.list, LEN(ol.list) - 5),
                                pc.CodeNumber, pc.CodeName, l.LetterSequence,
                                cr.ConcernResponseNumber
                         FROM   dbo.Comment c
                                INNER JOIN dbo.ConcernResponse cr ON c.ConcernResponseId = cr.ConcernResponseId
                                INNER JOIN dbo.ConcernResponseStatus crs ON cr.ConcernResponseStatusId = crs.ConcernResponseStatusId
                                INNER JOIN dbo.CommentText ct ON c.CommentId = ct.CommentId
                                INNER JOIN dbo.CommentCode cc ON ct.CommentId = cc.CommentId
                                INNER JOIN dbo.PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                                INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                                CROSS APPLY (
                                             SELECT [Text] + '[...]'
                                             FROM   dbo.CommentText ctL
                                             WHERE  ctL.CommentId = c.CommentId
                                             ORDER BY ctl.CommentTextId 
                                            FOR
                                             XML PATH('')
                                            ) ol (list)
                         WHERE  cr.ConcernResponseStatusId <> 4
                                AND pc.PhaseId = @phaseId
                         UNION
                         SELECT CAST(0 AS BIT) AS Associated,
                                NULL AS ConcernResponseId, NULL AS StatusName,
                                c.CommentId, c.CommentNumber,
                                CommentText = LEFT(ol.list, LEN(ol.list) - 5),
                                pc.CodeNumber, pc.CodeName, l.LetterSequence,
                                NULL AS ConcernResponseNumber
                         FROM   dbo.Comment c
                                INNER JOIN dbo.CommentText ct ON c.CommentId = ct.CommentId
                                INNER JOIN dbo.CommentCode cc ON ct.CommentId = cc.CommentId
                                INNER JOIN dbo.PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                                INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                                CROSS APPLY (
                                             SELECT [Text] + '[...]'
                                             FROM   dbo.CommentText ctL
                                             WHERE  ctL.CommentId = c.CommentId
                                             ORDER BY ctl.CommentTextId 
                                            FOR
                                             XML PATH('')
                                            ) ol (list)
                         WHERE  pc.PhaseId = @phaseId
                                AND c.ConcernResponseId IS NULL
                        ) t
             )
    SELECT  Associated, ConcernResponseId, StatusName, CommentId,
            CommentNumber, CommentText, CodeNumber, CodeName, LetterSequence,
            ConcernResponseNumber
    FROM    ReportResults
    WHERE   RowNumber BETWEEN @startRow AND @endRow
                     
    RETURN
GO





GRANT EXECUTE ON  [dbo].[spsResponseStatusReport] TO [carauser]
GO
