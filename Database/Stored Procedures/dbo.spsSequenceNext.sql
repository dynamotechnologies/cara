SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for returning the next available sequence for a phase and a key name */

CREATE PROCEDURE [dbo].[spsSequenceNext]
(
	@Name VARCHAR(100) = NULL,
	@PhaseId INT = NULL
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON
	
	DECLARE @NextSequenceNumber INT
	
	-- get the next available sequence number
	SELECT @NextSequenceNumber = ISNULL(CurrentSequenceNumber, 0) + 1
	FROM dbo.Sequence seq
	WHERE ((@PhaseId IS NULL AND PhaseId IS NULL) OR (@PhaseId IS NOT NULL AND seq.PhaseId = @PhaseId))
		AND ((@Name IS NULL AND Name IS NULL) OR (@Name IS NOT NULL AND seq.Name = @Name))
	
	-- update the current sequence number with the new sequence
	IF @NextSequenceNumber IS NOT NULL
	BEGIN
		UPDATE dbo.Sequence
		SET CurrentSequenceNumber = @NextSequenceNumber
		WHERE ((@PhaseId IS NULL AND PhaseId IS NULL) OR (@PhaseId IS NOT NULL AND PhaseId = @PhaseId))
			AND ((@Name IS NULL AND Name IS NULL) OR (@Name IS NOT NULL AND Name = @Name))
	END

	SELECT @NextSequenceNumber AS 'SequenceNumber'
GO
GRANT EXECUTE ON  [dbo].[spsSequenceNext] TO [carauser]
GO
