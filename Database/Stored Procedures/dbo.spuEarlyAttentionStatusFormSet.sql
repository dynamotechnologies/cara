
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is to ensure all of the letters in a form set have the same early action status as the master */

CREATE PROCEDURE [dbo].[spuEarlyAttentionStatusFormSet]
(
	@formSetId INT,
	@earlyAttentionStatusId INT
)
AS
	SET NOCOUNT OFF
	
	UPDATE TOP (200) dbo.Letter
	SET EarlyActionStatusId = @earlyAttentionStatusId
	WHERE FormSetId = @formSetId
		AND LetterTypeId != 5 -- = Form Plus
		AND EarlyActionStatusId != @earlyAttentionStatusId

	DECLARE @rowsUpdated INT
	SELECT @rowsUpdated = @@ROWCOUNT
	RETURN @rowsUpdated


GO

GRANT EXECUTE ON  [dbo].[spuEarlyAttentionStatusFormSet] TO [carauser]
GO
