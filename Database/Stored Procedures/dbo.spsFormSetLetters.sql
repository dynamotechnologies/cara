SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for FormManagement page */

CREATE PROCEDURE [dbo].[spsFormSetLetters]
(
 @phaseId INT = -1,
 @filterLetterTypeId INT = NULL,
 @filterLetterStatusId INT = NULL,
 @filterFormSetId INT = NULL,
 @pageNum INT = 1,
 @numRows INT = -1,
 @sortKey VARCHAR(50) = NULL,
 @totalRows INT OUT
)
AS -- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
SET NOCOUNT ON ;

	DECLARE @sql VARCHAR(MAX),
		@sqlSelect VARCHAR(MAX),
		@sqlFrom VARCHAR(MAX),
		@sortBy VARCHAR(255),
		@sqlWhere VARCHAR(MAX)

	SET @sqlWhere = ''

	IF @filterFormSetId IS NOT NULL AND @filterFormSetId >= 0
	BEGIN
		SET @sqlSelect = 'SELECT fs.FormSetId, fs.Name AS FormSetName, fs.MasterFormId,
					ltr.LetterId, ltr.LetterSequence, ltr.Size, ltr.Confidence, ls.Name AS StatusName, 
					lt.Name AS LetterType, a.LastName, a.FirstName,
					org.Name AS OrganizationName '
	                
		SET @sqlFrom = 'FROM   dbo.FormSet fs
					INNER JOIN dbo.Letter ltr ON fs.FormSetId = ltr.FormSetId
					INNER JOIN dbo.Author a ON ltr.LetterId = a.LetterId
					INNER JOIN dbo.LetterStatus ls ON ltr.LetterStatusId = ls.LetterStatusId
					INNER JOIN dbo.LetterType lt ON ltr.LetterTypeId = lt.LetterTypeId
					LEFT OUTER JOIN dbo.Organization org ON a.OrganizationId = org.OrganizationId
			 WHERE  a.Sender = 1 
				AND ltr.PhaseId = ' + CAST(@phaseId AS VARCHAR(20))
	END
	ELSE
	BEGIN
		IF @filterFormSetId = -1
		BEGIN
			SET @sqlSelect = 'SELECT -1 AS FormSetId, ''Unique'' AS FormSetName, -1 AS MasterFormId,
					ltr.LetterId, ltr.LetterSequence, ltr.Size, NULL AS Confidence, ls.Name AS StatusName, 
					lt.Name AS LetterType, a.LastName, a.FirstName,
					org.Name AS OrganizationName '
		END
		ELSE IF @filterFormSetId = -2
		BEGIN
			SET @sqlSelect = 'SELECT -2 AS FormSetId, ''Duplicate'' AS FormSetName, ISNULL(ltr.MasterDuplicateId, -1) AS MasterFormId,
					ltr.LetterId, ltr.LetterSequence, ltr.Size, NULL AS Confidence, ls.Name AS StatusName, 
					lt.Name AS LetterType, a.LastName, a.FirstName,
					org.Name AS OrganizationName '
		END
	                
		SET @sqlFrom = 'FROM   dbo.Letter ltr
					INNER JOIN dbo.Author a ON ltr.LetterId = a.LetterId
					INNER JOIN dbo.LetterStatus ls ON ltr.LetterStatusId = ls.LetterStatusId
					INNER JOIN dbo.LetterType lt ON ltr.LetterTypeId = lt.LetterTypeId
					LEFT OUTER JOIN dbo.Organization org ON a.OrganizationId = org.OrganizationId
			 WHERE  a.Sender = 1 
				AND ltr.PhaseId = ' + CAST(@phaseId AS VARCHAR(20))
	END

	IF @filterLetterTypeId IS NOT NULL 
    BEGIN
        IF (@filterLetterTypeId = 6) 
        BEGIN
			-- Not Pending
            SET @sqlWhere = @sqlWhere + ' AND ltr.LetterTypeId IN (1,2,3,4,5) '
            END
        ELSE 
        IF (@filterLetterTypeId = 7) 
        BEGIN
			-- All Forms
            SET @sqlWhere = @sqlWhere + ' AND ltr.LetterTypeId IN (3,4,5) '
        END
        ELSE 
        IF (@filterLetterTypeId = 8) 
        BEGIN
			-- Master Form or Unique
            SET @sqlWhere = @sqlWhere + ' AND ltr.LetterTypeId IN (2,3) '
        END
        ELSE 
        IF (@filterLetterTypeId = 10) 
        BEGIN
			-- Unique or Forms
            SET @sqlWhere = @sqlWhere + ' AND ltr.LetterTypeId IN (2,3,4,5) '
        END
        ELSE 
        BEGIN
			SET @sqlWhere = @sqlWhere
				+ ' AND (ltr.LetterTypeId = 3 OR ltr.LetterTypeId = '
                + CAST(@filterLetterTypeId AS VARCHAR(20)) + ')'
        END
    END
	
	IF @filterLetterStatusId IS NOT NULL 
    BEGIN
        SET @sqlWhere = @sqlWhere
            + ' AND ltr.LetterStatusId = '
            + CAST(@filterLetterStatusId AS VARCHAR(20)) + ' '
    END

	IF @filterFormSetId IS NULL OR @filterFormSetId < 0
    BEGIN
		IF @filterFormSetId = -1
		BEGIN
	        SET @sqlWhere = @sqlWhere + ' AND ltr.FormSetId IS NULL AND ltr.LetterTypeId = 2'
	    END
	    ELSE IF @filterFormSetId = -2
	    BEGIN
	        SET @sqlWhere = @sqlWhere + ' AND ltr.FormSetId IS NULL AND ltr.LetterTypeId = 1'
	    END
    END
	ELSE 
    IF @filterFormSetId IS NOT NULL 
    BEGIN
		SET @sqlWhere = @sqlWhere + ' AND ltr.FormSetId = '
            + CAST(@filterFormSetId AS VARCHAR(20))
    END
			
	SET @sql = @sqlSelect + ' ' + @sqlFrom + ' ' + @sqlWhere + ' '

	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL 
    BEGIN
        SET @sortBy = @sortKey
		
		-- translate the sort key into the corresponding column name or clause
        SET @sortBy = REPLACE(@sortBy, 'LetterSequence', 'ltr.LetterSequence')
        SET @sortBy = REPLACE(@sortBy, 'LetterType', 'lt.Name')
        SET @sortBy = REPLACE(@sortBy, 'AuthorName ASC', 'a.LastName ASC, a.FirstName ASC')
        SET @sortBy = REPLACE(@sortBy, 'AuthorName DESC', 'a.LastName DESC, a.FirstName DESC')
        SET @sortBy = REPLACE(@sortBy, 'OrganizationName', 'org.Name')
        SET @sortBy = REPLACE(@sortBy, 'Size', 'ltr.Size')
        SET @sortBy = REPLACE(@sortBy, 'Confidence', 'ltr.Confidence')
        SET @sortBy = REPLACE(@sortBy, 'StatusName', 'ls.Name')
    END
        
	-- Default Sort By clause
	IF @sortBy IS NULL 
    BEGIN
        SET @sortBy = 'LetterSequence'
    END
PRINT @sql
	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 
    BEGIN
        EXECUTE dbo.spsRowPager 
            @SQL = @sql,
            @RecordsPerPage = @numRows,
            @Page = @pageNum,
            @ID = 'LetterId',
            @SortBy = @sortBy
			
        EXEC ('SELECT 1 ' + @sqlFrom + ' ' + @sqlWhere + ' ')
        SET @totalRows = @@ROWCOUNT
			
    END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE 
    BEGIN
        SET @sql = @sql + ' ORDER BY ' + @sortBy		
        EXEC (@sql)
        SET @totalRows = @@ROWCOUNT		
    END

	RETURN

GO
GRANT EXECUTE ON  [dbo].[spsFormSetLetters] TO [carauser]
GO
