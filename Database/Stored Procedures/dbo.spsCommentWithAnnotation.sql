
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spsCommentWithAnnotation] 
	-- Add the parameters for the stored procedure here
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
    
    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

    BEGIN
   
		   Set @sql = 
			'SELECT 
				dbo.Letter.LetterSequence AS [LetterNumber], 
				dbo.Comment.CommentNumber AS [CommentNumber], 
				dbo.CommentText.Text AS [CommentText],
				dbo.Comment.Annotation AS [Annotation]
			FROM
				dbo.Phase
				INNER JOIN dbo.Letter ON Letter.PhaseId = dbo.Phase.PhaseId
				INNER JOIN dbo.Comment ON dbo.Comment.LetterId = dbo.Letter.LetterId
				INNER JOIN dbo.CommentText ON dbo.CommentText.CommentId = dbo.Comment.CommentId
			WHERE
				NOT dbo.Comment.Annotation IS NULL AND LTRIM(RTRIM(dbo.Comment.Annotation)) <> '''' AND dbo.Phase.PhaseId = ' + CAST(@phaseId as varchar(20))
	
	 END
	 
	  IF @sortBy IS NULL
		SET @sortBy = 'LetterNumber, CommentNumber'   	
	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN
END

GO

GRANT EXECUTE ON  [dbo].[spsCommentWithAnnotation] TO [carauser]
GO
