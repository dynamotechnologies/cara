SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Divya,,Name>
-- Create date: <1/30/17,,>
-- Description:	<Standard report for Concern response text with assigned to,,>
-- =============================================
CREATE PROCEDURE [dbo].[spsConcernResponseWithAssignedTo] 
	-- Add the parameters for the stored procedure here
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
    
    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

    BEGIN
   
		   			
			Set @sql = 
			'SELECT 
				
				cr.ConcernResponseNumber,
				crr.ConcernText,
				crr.ResponseText,
				u.LastName + '', '' + u.FirstName AS CRAssignedTo
			
			FROM
				dbo.vConcernResponsePhase cr			
				INNER JOIN dbo.ConcernResponse crr ON cr.ConcernResponseId = crr.ConcernResponseId
				LEFT OUTER JOIN dbo.[User] u ON u.UserId = crr.UserId
				
			WHERE
				cr.phaseid  = ' + CAST(@phaseId as varchar(20))
	
	 END
	 
	  IF @sortBy IS NULL
		SET @sortBy = 'ConcernResponseNumber'   	
	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN
END

GO
GRANT EXECUTE ON  [dbo].[spsConcernResponseWithAssignedTo] TO [carauser]
GO
