
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists responding organizations and key contact information */

CREATE PROCEDURE [dbo].[spsRespondingOrgReport] ( @PhaseId INT = -1 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;
    
    SELECT  o.Name AS OrgName, ot.Name AS OrgType, l.LetterSequence,
            a.LastName, a.FirstName, a.Address1, a.City, a.StateId,
            a.ZipPostal
    FROM    dbo.Letter l
            INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
            INNER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
            LEFT OUTER JOIN dbo.OrganizationType ot ON o.OrganizationTypeId = ot.OrganizationTypeId
    WHERE   l.PhaseId = @PhaseId
            -- AND a.OfficialRepresentativeTypeId <> 2 -- Member
    ORDER BY o.Name, l.LetterSequence
							
    RETURN
GO


GRANT EXECUTE ON  [dbo].[spsRespondingOrgReport] TO [carauser]
GO
