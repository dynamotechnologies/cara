
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spsRowPagerDistinct]
    (
      @SQL VARCHAR(MAX) ,
      @RecordsPerPage INT ,
      @Page INT ,
      @ID VARCHAR(255) ,
      @SortBy VARCHAR(255)
    )
AS 
    SET NOCOUNT OFF

    DECLARE @part1 VARCHAR(MAX)
    DECLARE @rowNumberClause VARCHAR(MAX)

    /*
	    The idea here is to build a query that looks like this

	    select *
	    from
			    (select [distinct] row_number() over (order by x, y) as [RowNumber], column1...columnN
				    from tableName
				    where whereClause
			    )
	    where RowNumber between pageIndex and pageIndex

    */
   
    SET @rowNumberClause = 'row_number()  over(order by ' + @SortBy
        + ')  as [RowNumber]'    
    
    SET @sql = RTRIM(LTRIM(@sql)) -- remove spaces just in case.
    
    SET @part1 = '
     SELECT *
     FROM (
     SELECT ' + @rowNumberClause + ', * FROM (' + @SQL + '
      ) as t ) as t1
     WHERE RowNumber
      BETWEEN '
        + CAST(( ( @RecordsPerPage * ( @Page - 1 ) ) + 1 ) AS VARCHAR(20))
        + ' AND ' + CAST(( @RecordsPerPage * @Page ) AS VARCHAR(20)) + ' ORDER BY RowNumber'

    EXEC(@part1)

GO
GRANT EXECUTE ON  [dbo].[spsRowPagerDistinct] TO [carauser]
GO
