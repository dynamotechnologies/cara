SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for generating report headers */

CREATE PROCEDURE [dbo].[spsReportHeader] ( @phaseId INT = -1 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON

    SELECT  p.Name AS ProjectName, p.ProjectNumber AS ProjectNumber,
            pt.Name AS PhaseType, ISNULL(ph.Name, ' ') AS PhaseName, ph.CommentStart,
            ph.CommentEnd
    FROM    dbo.Project p,
            dbo.Phase ph,
            dbo.PhaseType pt
    WHERE   p.ProjectId = ph.ProjectId
            AND ph.PhaseTypeId = pt.PhaseTypeId
            AND ph.phaseid = @phaseId

    RETURN
GO
GRANT EXECUTE ON  [dbo].[spsReportHeader] TO [carauser]
GO
