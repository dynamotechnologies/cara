
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for admin users page */

CREATE PROCEDURE [dbo].[spsUsers]
(
	@userId INT = NULL,
	@filterUsername varchar(100) = NULL,
	@filterLastName varchar(100) = NULL,
	@filterFirstName varchar(100) = NULL,
	@filterUnitId varchar(8) = NULL,
	@filterShowOnlyNonProjectRoles BIT = 1,
	@pageNum int = 1,
	@numRows int = -1,
	@sortKey varchar(50) = NULL,
	@totalRows int OUT
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON

	DECLARE @sql varchar(8000), @sortBy varchar(255), @sqlWhere varchar(8000), @countSql varchar(8000)
	DECLARE @Asterisk varchar(20)= '*', @SingleQuote VARCHAR(1) = ''''
	DECLARE @hasMsuPriv BIT = 0, @hasMucPriv BIT = 0
	
	-- check whether the request user has privileges to add/remove superuser and coordinator
	IF EXISTS(SELECT 1 FROM dbo.UserSystemRole sr
		INNER JOIN dbo.Role r on r.RoleId = sr.RoleId
		INNER JOIN dbo.RolePrivilege rp on rp.RoleId = r.RoleId
		INNER JOIN dbo.Privilege p on p.PrivilegeId = rp.PrivilegeId
			WHERE sr.UserId = @userId AND p.Code = 'MSU')
	BEGIN
		SET @hasMsuPriv = 1
	END

	IF EXISTS(SELECT 1 FROM dbo.UserSystemRole sr
		INNER JOIN dbo.Role r on r.RoleId = sr.RoleId
		INNER JOIN dbo.RolePrivilege rp on rp.RoleId = r.RoleId
		INNER JOIN dbo.Privilege p on p.PrivilegeId = rp.PrivilegeId
			WHERE sr.UserId = @userId AND p.Code = 'MUC')
	BEGIN
		SET @hasMucPriv = 1
	END
	
	---- -------------------------------
	---- Construct the select query
	---- -------------------------------
	SET @sqlWhere = ' WHERE 1 = 1 '
	SET @sql = ' SELECT usr.UserId, usr.UserName, usr.FirstName, usr.LastName, usr.Email, dbo.fnGetUnitNameText(usr.UserId) AS UnitNameText,
		ISNULL((SELECT 1 FROM dbo.UserSystemRole sr INNER JOIN dbo.Role r on r.RoleId = sr.RoleId
			WHERE sr.UserId = usr.UserId AND r.RoleId = 1 AND r.ProjectRole = 0), 0) AS IsSuperUser,
		ISNULL((SELECT top 1 1 FROM dbo.UserSystemRole sr INNER JOIN dbo.Role r on r.RoleId = sr.RoleId
			WHERE sr.UserId = usr.UserId AND r.RoleId = 2 AND r.ProjectRole = 0), 0) AS IsCoordinator, ' + CAST(@hasMsuPriv as varchar(20)) +
			' AS CanUpdateSuperUser, ' + + CAST(@hasMucPriv as varchar(20))+ + ' AS CanUpdateCoordinator
		FROM dbo.[User] usr '
	
	---- --------------------------
	---- Construct filters
	---- --------------------------
	IF (@filterUsername IS NOT NULL)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND LOWER(usr.UserName) LIKE ''%' + LOWER(REPLACE(@filterUsername, '''', '''''')) + '%'' '
	END
	
	IF (@filterLastName IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND LOWER(usr.LastName) LIKE ''%' + LOWER(REPLACE(@filterLastName, '''', '''''')) + '%'' '
	END

	IF (@filterFirstName IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND LOWER(usr.FirstName) LIKE ''%' + LOWER(REPLACE(@filterFirstName, '''', '''''')) + '%'' '
	END
	
	IF (@filterUnitId IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND EXISTS (SELECT 1 FROM dbo.UserSystemRole uu WHERE uu.UserId = usr.UserId AND uu.UnitId LIKE ''' +
			@filterUnitId + '%'') '
	END
	
	IF (@filterShowOnlyNonProjectRoles = 1)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND EXISTS (SELECT 1 FROM dbo.UserSystemRole usrr WHERE usrr.UserId = usr.UserId AND (usrr.RoleId = 1 OR usrr.RoleId = 2)) '
	END
	
	SET @sql = @sql + @sqlWhere
	
	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL
	BEGIN
		SET @sortBy = @sortKey
		
		-- translate the sort key into the corresponding column name or clause
		SET @sortBy = REPLACE(@sortBy, 'UserName', 'usr.UserName')
		SET @sortBy = REPLACE(@sortBy, 'LastName', 'usr.LastName')
		SET @sortBy = REPLACE(@sortBy, 'FirstName', 'usr.FirstName')
		SET @sortBy = REPLACE(@sortBy, 'Email', 'usr.Email')
	END

	-- Default Sort By clause
	IF @sortKey IS NULL
		SET @sortBy = 'usr.LastName'

	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1
	BEGIN
		EXECUTE dbo.spsRowPager
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = 'UserId',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
			
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
	END

	RETURN
GO





GRANT EXECUTE ON  [dbo].[spsUsers] TO [carauser]
GO
