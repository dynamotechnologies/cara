SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/* This stored procedure is for updating a coding template */

CREATE PROCEDURE [dbo].[spuCodingTemplate]
(
	@codingTemplateId INT,
	@templateName VARCHAR(100),
	@codeIds XML = NULL
)
AS
	SET NOCOUNT OFF
	
	IF @codingTemplateId IS NOT NULL AND @templateName IS NOT NULL
	BEGIN
		-- update the coding template
		UPDATE dbo.CodingTemplate
		SET Name = @templateName
		WHERE CodingTemplateId = @codingTemplateId
	
		-- insert the codes
		IF @codeIds IS NOT NULL
		BEGIN
			BEGIN TRAN
			
			DECLARE @ids TABLE (id INT)
			
			-- temp table for the code ids
			INSERT INTO @ids(id)
			SELECT list.id.value('.','VARCHAR(20)')
				FROM @codeIds.nodes('idList/id') AS list(id)
			
			-- delete phase codes that are deselected
			DELETE FROM dbo.CodingTemplateCode
			WHERE CodingTemplateId = @codingTemplateId AND CodeId NOT IN (SELECT id FROM @ids)

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
			END
			
			-- insert the new associations for non-custom codes
			INSERT INTO dbo.CodingTemplateCode ( CodeId, CodingTemplateId )
			SELECT cd.CodeId, @codingTemplateId
			FROM dbo.Code cd
			WHERE cd.CodeId IN (SELECT id FROM @ids)
				AND NOT EXISTS (SELECT 1 FROM dbo.CodingTemplateCode ctc WHERE ctc.CodingTemplateId = @codingTemplateId AND ctc.CodeId = cd.CodeId)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
			END
			
			COMMIT TRAN
		END
	END

	RETURN
GO
GRANT EXECUTE ON  [dbo].[spuCodingTemplate] TO [carauser]
GO
