
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for Codes per phase */

CREATE PROCEDURE [dbo].[spsPhaseCodesMostUsed] (@PhaseId INT = -1)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
SET NOCOUNT ON ;

	SELECT TOP 5
        pc.PhaseCodeId, pc.CodeNumber, pc.CodeName,
        COUNT(cc.PhaseCodeId) AS 'TimesUsed'
	FROM    dbo.Letter l
        INNER JOIN dbo.Comment c ON l.LetterId = c.LetterId
        INNER JOIN dbo.CommentCode cc ON c.CommentId = cc.CommentId
        INNER JOIN dbo.PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
	WHERE   l.PhaseId = @phaseId
	GROUP BY pc.PhaseCodeId, pc.CodeNumber, pc.CodeName
	ORDER BY  CodeNumber, COUNT(cc.PhaseCodeId) DESC

RETURN
GO

GRANT EXECUTE ON  [dbo].[spsPhaseCodesMostUsed] TO [carauser]
GO
