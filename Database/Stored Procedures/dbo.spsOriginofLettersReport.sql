
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists signature and letter counts by state */

CREATE PROCEDURE [dbo].[spsOriginofLettersReport] (@PhaseId INT = -1)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
SET NOCOUNT ON ;

SELECT  stl.name AS StateName, sta.scount AS SignatureCount,
        stl.lcount AS LetterCount
FROM    (
         SELECT ISNULL(s.Name, 'Unknown') AS Name, ISNULL(s.StateId, 'N0') AS StateId, COUNT(a.AuthorId) AS scount
         FROM   dbo.Letter l
                INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
                LEFT OUTER JOIN dbo.State s ON a.StateId = s.StateId
         WHERE  l.PhaseId = @PhaseId
         GROUP BY s.Name, s.StateId
        ) sta,
        (
         SELECT ISNULL(s.Name, 'Unknown') AS Name, ISNULL(s.StateId, 'N0') AS StateId, COUNT(DISTINCT l.LetterId) AS lcount
         FROM   dbo.Letter l
                INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
                LEFT OUTER JOIN dbo.State s ON a.StateId = s.StateId
         WHERE  l.PhaseId = @PhaseId
         GROUP BY s.Name, s.StateId
        ) stl
WHERE   sta.StateId = stl.StateId
ORDER BY stl.name
							
RETURN
GO

GRANT EXECUTE ON  [dbo].[spsOriginofLettersReport] TO [carauser]
GO
