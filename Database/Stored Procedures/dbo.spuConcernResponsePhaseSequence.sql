SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is for updating phase sequence for the concerns responses */

CREATE PROCEDURE [dbo].[spuConcernResponsePhaseSequence]
(
	@phaseId INT,
	@sequence INT
)
AS
	SET NOCOUNT OFF
	
	IF @phaseId IS NOT NULL AND @sequence IS NOT NULL
	BEGIN
		BEGIN TRAN
		
		-- decrement the concern response sequences greater than the input sequence
		UPDATE [dbo].[ConcernResponse]
		SET Sequence = Sequence - 1
		WHERE ConcernResponseId IN (SELECT ConcernResponseId FROM dbo.vConcernResponsePhase 
								WHERE PhaseId = @phaseId AND Sequence > @sequence)
		
		IF @@ERROR = 0
		BEGIN
			-- decrement the phase sequence number
			UPDATE [dbo].Sequence
			SET CurrentSequenceNumber = CurrentSequenceNumber - 1
			WHERE PhaseId = @phaseId AND Name = 'Concern Response Count'
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
			END
		END
		
		COMMIT TRAN
	END

	RETURN
GO
GRANT EXECUTE ON  [dbo].[spuConcernResponsePhaseSequence] TO [carauser]
GO
