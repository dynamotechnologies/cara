
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spsRowPager]
(
	@SQL varchar(8000),
	@RecordsPerPage int,
	@Page int,
	@ID varchar(255),
	@SortBy varchar(255)
)
AS
	SET NOCOUNT OFF

    DECLARE @part1 varchar(max)
    DECLARE @rowNumberClause varchar(max)

    /*
	    The idea here is to build a query that looks like this

	    select *
	    from
			    (select [distinct] row_number() over (order by x, y) as [RowNumber], column1...columnN
				    from tableName
				    where whereClause
			    )
	    where RowNumber between pageIndex and pageIndex

    */

    -- Moved 'distinct' to the front
    if (charindex('distinct', @sql) = 0)
        SET @rowNumberClause = 'row_number()  over(order by ' + @SortBy + ')  as [RowNumber]'
    else
    begin
        SET @rowNumberClause = 'distinct row_number()  over(order by ' + @SortBy + ')  as [RowNumber]'
        set @sql = replace(@sql, 'distinct', '')
    end

    set @sql = rtrim(ltrim(@sql)) -- remove spaces just in case.
    set @sql = 'select ' + @rowNumberClause + ', ' + right(@SQL, len(@SQL) - len('SELECT'))

    SET @part1 = '
     SELECT *
     FROM (' + @SQL + '
      ) as t
     WHERE RowNumber
      BETWEEN ' + cast(((@RecordsPerPage * (@Page - 1)) + 1) as varchar(20))
      + ' AND ' + cast((@RecordsPerPage * @Page) as varchar(20)) + ' ORDER BY RowNumber'

    exec(@part1)


GO
GRANT EXECUTE ON  [dbo].[spsRowPager] TO [carauser]
GO
