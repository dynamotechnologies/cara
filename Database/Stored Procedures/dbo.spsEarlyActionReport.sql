
SET ANSI_NULLS ON
GO

/* This stored procedure lists early attention letters by type, author, and comment */

CREATE PROCEDURE [dbo].[spsEarlyActionReport] (
 @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;


    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

BEGIN

SET @sql = 'SELECT DISTINCT


            tl.Name AS TermListName,
            ''Letters with Early Attention Auto-Markup'' AS EaType, l.DateSubmitted,
            l.LetterSequence, c.CommentId, c.CommentNumber, 
            CommentText = LEFT(o.list, LEN(o.list) - 5), a.LastName,
            a.FirstName, a.Address1, a.Address2, a.City, a.StateId,
            a.ZipPostal
    FROM    Phase p
            INNER JOIN Letter l ON p.PhaseId = l.PhaseId
            INNER JOIN LetterTermConfirmation ltc ON l.LetterId = ltc.LetterId
            INNER JOIN Term t ON ltc.TermId = t.TermId
            INNER JOIN TermList tl ON t.TermListId = tl.TermListId
            INNER JOIN Author a ON a.LetterId = l.LetterId
            LEFT OUTER JOIN Comment c ON c.LetterId = l.LetterId
            LEFT OUTER JOIN CommentText ct ON ct.CommentId = c.CommentId
            CROSS APPLY ( SELECT    [Text] + ''[...]''
                          FROM      dbo.CommentText ctL
                          WHERE     ctL.CommentId = c.CommentId 
                          FOR
                          XML PATH('''')
                        ) o ( list )
    WHERE   p.PhaseId =  ' + CAST(@phaseId as varchar(20)) + '
            AND tl.EarlyAction = ''True''
            AND ltc.StatusId in (2,3) 
            AND a.Sender = ''True''
            AND ( c.CommentId IS NULL
                  OR c.CommentId IN (
                  SELECT    cS.CommentId
                  FROM      dbo.Comment cS,
                            commenttext ctS
                  WHERE     cS.CommentId = ctS.CommentId
                            AND cS.LetterId = l.LetterId
                            AND ctS.text LIKE ''%'' + t.Name + ''%'' ) )
    UNION
    SELECT  pc.CodeNumber + '' - '' + pc.CodeName AS TermListName,
            ''Letters with Early Attention Coded Comments'' AS EaType,
            l.DateSubmitted, l.LetterSequence, c.CommentId, c.CommentNumber,
            CommentText = LEFT(o.list, LEN(o.list) - 5), a.LastName,
            a.FirstName, a.Address1, a.Address2, a.City, a.StateId,
            a.ZipPostal
    FROM    Phase p
            INNER JOIN Letter l ON p.PhaseId = l.PhaseId
            INNER JOIN Author a ON a.LetterId = l.LetterId
            INNER JOIN PhaseCode pc ON l.PhaseId = pc.PhaseId
            INNER JOIN Comment c ON c.LetterId = l.LetterId
            INNER JOIN dbo.CommentCode cc ON c.CommentId = cc.CommentId
                                             AND pc.PhaseCodeId = cc.PhaseCodeId
            INNER JOIN CommentText ct ON ct.CommentId = c.CommentId
            CROSS APPLY ( SELECT    [Text] + ''[...]''
                          FROM      dbo.CommentText ctL
                          WHERE     ctL.CommentId = c.CommentId 
                          FOR
                          XML PATH('''')
                        ) o ( list )
    WHERE   p.PhaseId = ' + CAST(@phaseId as varchar(20)) + '
            AND SUBSTRING(pc.CodeNumber, 1, 1) = 5
            AND a.Sender = ''True'''

   END

    IF @sortBy IS NULL
		SET @sortBy = 'TermlistName'

   /* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy

		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT

	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END

 RETURN    

GO








GRANT EXECUTE ON  [dbo].[spsEarlyActionReport] TO [carauser]
GO
