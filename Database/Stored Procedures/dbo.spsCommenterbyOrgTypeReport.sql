
SET ANSI_NULLS ON
GO

/* This stored procedure lists commenters by organization type and organization */

CREATE PROCEDURE [dbo].[spsCommenterbyOrgTypeReport] 
( 
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
  )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;
    
    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

    BEGIN
   
		Set @sql = 'SELECT  ISNULL(ot.name, ''Not Specified'') AS OrgTypeName ,
                    ISNULL(o.name, ''N/A'') AS OrgName , 
                    a.LastName ,
                    a.FirstName ,
                    l.LetterSequence
            FROM    dbo.Author a
                    JOIN dbo.Letter l ON a.LetterId = l.LetterId
                    LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
                    LEFT OUTER JOIN dbo.OrganizationType ot ON o.OrganizationTypeId = ot.OrganizationTypeId
            WHERE   l.PhaseId = ' + CAST(@phaseId as varchar(20))
            
     END
            
    IF @sortBy IS NULL
		SET @sortBy = 'OrgTypeName, OrgName ASC, LastName, FirstName, LetterSequence'
							
     /* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN


GO



GRANT EXECUTE ON  [dbo].[spsCommenterbyOrgTypeReport] TO [carauser]
GO
