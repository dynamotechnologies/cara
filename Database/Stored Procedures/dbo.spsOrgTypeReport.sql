
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists signature and letter counts by organization type */

CREATE PROCEDURE [dbo].[spsOrgTypeReport] ( @PhaseId INT = -1 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;

    SELECT  otl.Name AS OrganizationTypeName, ota.scount AS SignatureCount,
            otl.lcount AS LetterCount
    FROM    ( SELECT    ISNULL(ot.Name, 'Unknown') AS Name,
                        ot.OrganizationTypeId, COUNT(a.AuthorId) AS scount
              FROM      dbo.Letter l
                        INNER JOIN Author a ON l.LetterId = a.LetterId
                        LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
                        LEFT OUTER JOIN dbo.OrganizationType ot ON o.OrganizationTypeId = ot.OrganizationTypeId
              WHERE     l.PhaseId = @PhaseId
              GROUP BY  ot.Name, ot.OrganizationTypeId
            ) ota ,
            ( SELECT    ISNULL(ot.Name, 'Unknown') AS Name,
                        ot.OrganizationTypeId, COUNT(l.LetterId) AS lcount
              FROM      dbo.Letter l
                        INNER JOIN Author a ON l.LetterId = a.LetterId
                        LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
                        LEFT OUTER JOIN dbo.OrganizationType ot ON o.OrganizationTypeId = ot.OrganizationTypeId
              WHERE     l.PhaseId = @PhaseId
                        AND a.Sender = 1
              GROUP BY  ot.Name, ot.OrganizationTypeId
            ) otl
    WHERE   ota.Name = otl.Name
    ORDER BY otl.name

							
    RETURN
GO


GRANT EXECUTE ON  [dbo].[spsOrgTypeReport] TO [carauser]
GO
