
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/* This stored procedure is for inserting phase codes to the target phase
   based on coding template or phase */

CREATE PROCEDURE [dbo].[spiPhaseCodes]
(
	@codingTemplateId INT = NULL,
	@phaseId int = NULL,
	@codeIds XML = NULL,
	@targetPhaseId int
)
AS
	SET NOCOUNT OFF
	
	IF @targetPhaseId IS NOT NULL
	BEGIN
		-- insert codes based on coding template id
		IF @codingTemplateId IS NOT NULL
		BEGIN
			-- delete the existing phase codes
			DELETE FROM dbo.PhaseCode
			WHERE PhaseId = @targetPhaseId AND CanRemove = 1

			INSERT INTO dbo.PhaseCode(PhaseId, CodeCategoryId, CodeId, ParentCodeId, CodeName, CodeNumber)
			SELECT @targetPhaseId, CodeCategoryId, cd.CodeId, ParentCodeId, cd.Name, CodeNumber
			FROM dbo.Code cd
				INNER JOIN dbo.CodingTemplateCode ctc ON ctc.CodeId = cd.CodeId
				INNER JOIN dbo.CodingTemplate ct ON ct.CodingTemplateId = ctc.CodingTemplateId
			WHERE ct.CodingTemplateId = @codingTemplateId
		END
		-- insert codes based on input phase id
		ELSE IF @phaseId IS NOT NULL
		BEGIN
			-- delete the existing phase codes
			DELETE FROM dbo.PhaseCode
			WHERE PhaseId = @targetPhaseId AND CanRemove = 1
			
			INSERT INTO dbo.PhaseCode(PhaseId, CodeCategoryId, CodeId, ParentCodeId, CodeName, CodeNumber)
			SELECT @targetPhaseId, CodeCategoryId, pc.CodeId, ParentCodeId, pc.CodeName, pc.CodeNumber
			FROM dbo.PhaseCode pc
			WHERE pc.PhaseId = @phaseId
		END
		ELSE IF @codeIds IS NOT NULL
		BEGIN
			BEGIN TRAN
			
			DECLARE @ids TABLE (id INT)
			
			-- temp table for the code ids
			INSERT INTO @ids(id)
			SELECT list.id.value('.','VARCHAR(20)')
				FROM @codeIds.nodes('idList/id') AS list(id)
			
			-- delete phase codes that are deselected
			DELETE FROM dbo.PhaseCode
			WHERE PhaseId = @targetPhaseId AND CanRemove = 1 AND CodeId NOT IN (SELECT id FROM @ids)

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
			END
			
			-- insert the new associations for non-custom codes
			INSERT INTO dbo.PhaseCode(PhaseId, CodeCategoryId, CodeId, ParentCodeId, CodeName, CodeNumber)
			SELECT @targetPhaseId, CodeCategoryId, cd.CodeId, ParentCodeId, cd.Name, CodeNumber
			FROM dbo.Code cd
			WHERE cd.CodeId IN (SELECT id FROM @ids)
				AND NOT EXISTS (SELECT 1 FROM dbo.PhaseCode pc WHERE pc.PhaseId = @targetPhaseId AND pc.CodeId = cd.CodeId)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
			END
			
			COMMIT TRAN
		END
	END

	RETURN
GO



GRANT EXECUTE ON  [dbo].[spiPhaseCodes] TO [carauser]
GO
