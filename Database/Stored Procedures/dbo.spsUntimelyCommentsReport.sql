
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure returns letters that have been flagged as outside the comment period  */

CREATE PROCEDURE [dbo].[spsUntimelyCommentsReport] 
( 
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
  )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON

    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

    BEGIN
   
		Set @sql = 'SELECT  a.FirstName, a.LastName, a.Address1, a.Address2, a.StateId, a.City,
                    a.ZipPostal, a.Phone, a.Email, o.Name AS OrgName, l.LetterSequence,
                    l.DateSubmitted, ph.TimeZone
            FROM    dbo.Author a
                    INNER JOIN dbo.Letter l ON a.LetterId = l.LetterId
					INNER JOIN dbo.Phase ph on l.PhaseId = ph.PhaseId
                    LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
            WHERE   l.PhaseId = ' + CAST(@phaseId as varchar(20)) + ' AND l.WithinCommentPeriod = 0'
   
   END
   
     IF @sortBy IS NULL
		SET @sortBy = 'DateSubmitted'
    
   
        
     /* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN

GO



GRANT EXECUTE ON  [dbo].[spsUntimelyCommentsReport] TO [carauser]
GO
