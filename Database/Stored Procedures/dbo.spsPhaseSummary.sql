
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/* This stored procedure is for Phase details summary page */

CREATE PROCEDURE [dbo].[spsPhaseSummary]
(
	@phaseId int
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON;
	
	DECLARE @phaseTypeId INT
	
	SELECT @phaseTypeId = PhaseTypeId
	FROM dbo.Phase
	WHERE PhaseId = @phaseId;
	
	SELECT
		pd.LetterStatus_1 AS NewLetters,
		pd.LetterStatus_2 AS CodingInProgressLetters,
		pd.LetterStatus_3 AS CodingCompleteLetters,
        pd.EarlyActionStatus_1 AS Action_NotRequired,
        pd.EarlyActionStatus_2 AS Action_NeedsReview,
        pd.EarlyActionStatus_3 AS Action_Completed,
        pd.EarlyActionStatus_4 AS Action_Coded,
		pd.LetterType_2 AS UniqueLetters,
		pd.LetterType_4 AS FormLetters,
		pd.LetterType_1 AS DuplicateLetters,
		pd.LetterType_3 AS MasterFormLetters,
		pd.LetterType_5 AS FormPlusLetters,
		pd.LetterType_9 AS PendingLetters,
		pd.LetterType_2 + pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 + pd.LetterType_1 AS TotalLetters,
		pd.CodeCategory_1 AS IssueActionComments,
		pd.CodeCategory_2 AS RationaleComments,
		pd.CodeCategory_3 AS LocationComments,
		pd.CodeCategory_4 AS DocumentComments,
		pd.CodeCategory_5 AS LegalAppealsComments,
		pd.CodeCategory_6 AS ResourceAppealsComments,
		pd.CodeCategory_7 AS EarlyAttentionComments,
		pd.CodeCategory_8 AS OtherComments,
		pd.CodeCategory_9 AS ObjectionComments,
		pd.ConcernResponseStatus_1 AS InProgressConcerns,
		pd.ConcernResponseStatus_2 AS ReadyResponses,
		pd.ConcernResponseStatus_3 AS InProgressResponses,
		pd.ConcernResponseStatus_4 AS CompleteResponses,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 1), 0)) AS HasIssueAction,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 2), 0)) AS HasRationale,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 3), 0)) AS HasLocation,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 4), 0)) AS HasDocument,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 5), 0)) AS HasLegalAppeals,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 6), 0)) AS HasResourceAppeals,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 7), 0)) AS HasEarlyAttention,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 8), 0)) AS HasOther,
		CONVERT(BIT, ISNULL((SELECT 1 FROM dbo.CodeCategoryPhaseType WHERE PhaseTypeId = @phaseTypeId AND CodeCategoryId = 9), 0)) AS HasObjection
	FROM dbo.PhaseDashboard pd
	WHERE pd.PhaseId = @phaseId	
		
GO










GRANT EXECUTE ON  [dbo].[spsPhaseSummary] TO [carauser]
GO
