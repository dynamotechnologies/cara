
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists signature, letter, form, and form plus counts for each master letter*/

CREATE PROCEDURE [dbo].[spsOrganizedLetterCampaignReport] (@PhaseId INT = -1)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
SET NOCOUNT ON ;

	DECLARE @PhaseFormSets TABLE
		(
		 FormSetId INT,
		 MasterFormId INT,
		 Name VARCHAR(50)
		)

	DECLARE @LetterCount TABLE
		(
		 FormSetId INT,
		 LetterCount INT
		)

	DECLARE @FormCount TABLE
		(
		 FormSetId INT,
		 LetterCount INT
		)

	DECLARE @FormPlusCount TABLE
		(
		 FormSetId INT,
		 LetterCount INT
		)

	DECLARE @AuthorCount TABLE
		(
		 FormSetId INT,
		 AuthorCount INT
		)

	INSERT  INTO @PhaseFormSets (FormSetId, MasterFormId, Name)
			SELECT  DISTINCT
					fs.FormSetId, fs.MasterFormId, fs.Name
			FROM    dbo.FormSet fs
					INNER JOIN dbo.Letter l ON fs.FormSetId = l.FormSetId
			WHERE   fs.PhaseId = @PhaseId


	-- Letter Count
	INSERT  INTO @LetterCount (FormSetId, LetterCount)
			SELECT  FormSetId, COUNT(FormSetId)
			FROM    dbo.Letter
			WHERE   phaseId = @PhaseId
					AND FormSetId IS NOT NULL
			GROUP BY FormSetId 

	-- Form Count
	INSERT  INTO @FormCount (FormSetId, LetterCount)
			SELECT  FormSetId, COUNT(FormSetId)
			FROM    dbo.Letter
			WHERE   phaseId = @PhaseId
					AND FormSetId IS NOT NULL
					AND LetterTypeId = 4
			GROUP BY FormSetId 

	-- Form Plus Count
	INSERT  INTO @FormPlusCount (FormSetId, LetterCount)
			SELECT  FormSetId, COUNT(FormSetId)
			FROM    dbo.Letter
			WHERE   phaseId = @PhaseId
					AND FormSetId IS NOT NULL
					AND LetterTypeId = 5
			GROUP BY FormSetId

	-- Author Count
	INSERT  INTO @AuthorCount (FormSetId, AuthorCount)
			SELECT  FormSetId, COUNT(authorId)
			FROM    dbo.Letter l
					INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
			WHERE   phaseId = @PhaseId
					AND FormSetId IS NOT NULL
			GROUP BY FormSetId

	SELECT  pfs.FormSetId, l.LetterSequence, pfs.Name AS FormSetName, lc.LetterCount,
			ISNULL(fc.LetterCount, 0) AS FormCount,
			ISNULL(fpc.LetterCount, 0) AS FormPlusCount,
			ac.AuthorCount AS SignatureCount
	FROM    @PhaseFormSets pfs
			INNER JOIN dbo.Letter l ON pfs.FormSetId = l.FormSetId AND l.LetterTypeId = 3
			INNER JOIN @LetterCount lc ON pfs.FormSetId = lc.FormSetId
			INNER JOIN @AuthorCount ac ON pfs.FormSetId = ac.FormSetId
			LEFT OUTER JOIN @FormCount fc ON pfs.FormSetId = fc.FormSetId
			LEFT OUTER JOIN @FormPlusCount fpc ON pfs.FormSetId = fpc.FormSetId
RETURN
    
    
GO

GRANT EXECUTE ON  [dbo].[spsOrganizedLetterCampaignReport] TO [carauser]
GO
