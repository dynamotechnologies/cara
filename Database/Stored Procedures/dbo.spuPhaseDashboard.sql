
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is for updated the PhaseDashboard table */

CREATE PROCEDURE [dbo].[spuPhaseDashboard]
AS 
SET NOCOUNT OFF;
	
-- Populate the values for the existing records
WITH    LtrStatus1(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterStatusId = 1
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterStatus_1 = a.Total
    FROM    LtrStatus1 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrStatus2(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterStatusId = 2
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterStatus_2 = a.Total
    FROM    LtrStatus2 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrStatus3(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterStatusId = 3
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterStatus_3 = a.Total
    FROM    LtrStatus3 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrType1(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterTypeId = 1
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterType_1 = a.Total
    FROM    LtrType1 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrType2(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterTypeId = 2
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterType_2 = a.Total
    FROM    LtrType2 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrType3(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterTypeId = 3
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterType_3 = a.Total
    FROM    LtrType3 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrType4(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterTypeId = 4
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterType_4 = a.Total
    FROM    LtrType4 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrType5(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterTypeId = 5
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterType_5 = a.Total
    FROM    LtrType5 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    LtrType9(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     LetterTypeId = 9
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     LetterType_9 = a.Total
    FROM    LtrType9 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    ErlyActn1(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     EarlyActionStatusId = 1
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     EarlyActionStatus_1 = a.Total
    FROM    ErlyActn1 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    ErlyActn2(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     EarlyActionStatusId = 2
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     EarlyActionStatus_2 = a.Total
    FROM    ErlyActn2 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    ErlyActn3(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     EarlyActionStatusId = 3
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     EarlyActionStatus_3 = a.Total
    FROM    ErlyActn3 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;
	 
WITH    ErlyActn4(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      dbo.Letter
              WHERE     EarlyActionStatusId = 4
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     EarlyActionStatus_4 = a.Total
    FROM    ErlyActn4 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CRStatus_1(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                l.PhaseId, cr.ConcernResponseId,
                                cr.ConcernResponseStatusId
                         FROM   dbo.ConcernResponse cr
                                INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                         WHERE  cr.ConcernResponseStatusId = 1
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     ConcernResponseStatus_1 = a.Total
    FROM    CRStatus_1 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CRStatus_2(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                l.PhaseId, cr.ConcernResponseId,
                                cr.ConcernResponseStatusId
                         FROM   dbo.ConcernResponse cr
                                INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                         WHERE  cr.ConcernResponseStatusId = 2
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     ConcernResponseStatus_2 = a.Total
    FROM    CRStatus_2 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CRStatus_3(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                l.PhaseId, cr.ConcernResponseId,
                                cr.ConcernResponseStatusId
                         FROM   dbo.ConcernResponse cr
                                INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                         WHERE  cr.ConcernResponseStatusId = 3
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     ConcernResponseStatus_3 = a.Total
    FROM    CRStatus_3 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CRStatus_4(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                l.PhaseId, cr.ConcernResponseId,
                                cr.ConcernResponseStatusId
                         FROM   dbo.ConcernResponse cr
                                INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
                                INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
                         WHERE  cr.ConcernResponseStatusId = 4
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     ConcernResponseStatus_4 = a.Total
    FROM    CRStatus_4 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_1(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 1
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_1 = a.Total
    FROM    CodeCat_1 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_2(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 2
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_2 = a.Total
    FROM    CodeCat_2 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_3(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 3
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_3 = a.Total
    FROM    CodeCat_3 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_4(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 4
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_4 = a.Total
    FROM    CodeCat_4 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_5(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 5
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_5 = a.Total
    FROM    CodeCat_5 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_6(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 6
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_6 = a.Total
    FROM    CodeCat_6 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_7(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 7
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_7 = a.Total
    FROM    CodeCat_7 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_8(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 8
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_8 = a.Total
    FROM    CodeCat_8 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;

WITH    CodeCat_9(PhaseId, Total)
          AS (
              SELECT    PhaseId, COUNT(*)
              FROM      (
                         SELECT DISTINCT
                                pc.PhaseId, cc.CommentId, pc.CodeCategoryId
                         FROM   dbo.CommentCode cc
                                INNER JOIN PhaseCode pc ON cc.PhaseCodeId = pc.PhaseCodeId
                         WHERE  pc.CodeCategoryId = 9
                        ) t
              GROUP BY  PhaseId
             )
    UPDATE  dbo.PhaseDashboard
    SET     CodeCategory_9 = a.Total
    FROM    CodeCat_9 AS a
            INNER JOIN dbo.PhaseDashboard pd ON a.PhaseId = pd.PhaseId ;


RETURN


GO

GRANT EXECUTE ON  [dbo].[spuPhaseDashboard] TO [carauser]
GO
