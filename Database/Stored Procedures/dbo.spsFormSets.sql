SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for FormManagement page */

CREATE PROCEDURE [dbo].[spsFormSets]
(
 @phaseId INT = -1,
 @pageNum INT = 1,
 @numRows INT = -1,
 @sortKey VARCHAR(50) = NULL,
 @totalRows INT OUT
)
AS -- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
SET NOCOUNT ON ;

	DECLARE @sql VARCHAR(8000),
		@sqlSelect VARCHAR(8000),
		@sqlUnion1 VARCHAR(8000),
		@sqlUnion2 VARCHAR(8000),
		@sqlUnion3 VARCHAR(8000),
		@sortBy VARCHAR(255),
		@sqlWhere VARCHAR(8000)

	SET @sqlWhere = ''
	
	SET @sqlSelect = 'SELECT  FormSetId, FormSetName, MasterFormId, LetterCount,
        LetterId, LetterSequence, LastName, FirstName, OrganizationName, Size, Confidence '

	SET @sqlUnion1 = 'SELECT fs.FormSetId, fs.Name AS FormSetName, fs.MasterFormId,
				fs.LetterCount, ltr.LetterId, ltr.PhaseId,
                ltr.LetterSequence, ltr.Size, ltr.Confidence, a.LastName, a.FirstName,
                org.Name AS OrganizationName, 0 as FormSetSort
         FROM   FormSet fs
                INNER JOIN dbo.Letter ltr ON fs.FormSetId = ltr.FormSetId
                INNER JOIN dbo.Author a ON ltr.LetterId = a.LetterId
                LEFT OUTER JOIN dbo.Organization org ON a.OrganizationId = org.OrganizationId
         WHERE  a.Sender = 1 
			AND fs.MasterFormId = ltr.LetterId
			AND ltr.PhaseId = ' + CAST(@phaseId AS VARCHAR(20))

	-- Display Unique letters as a form set (always display at the bottom)
	SET @sqlUnion2 = 'SELECT NULL AS FormSetId, ''Unique'' AS FormSetName, NULL AS MasterFormId, (
                           SELECT   LetterType_2
                           FROM     dbo.PhaseDashboard
                           WHERE    PhaseId = ' + CAST(@phaseId AS VARCHAR(20)) + '
                           ) AS LetterCount, NULL AS LetterId, ' + CAST(@phaseId AS VARCHAR(20)) + ' AS PhaseId,
                NULL AS LetterSequence, NULL AS Size, NULL AS LastName, NULL As FirstName, 
                NULL AS OrganizationName, NULL AS Confidence, 1 AS FormSetSort'			

	-- Display Unique letters as a form set (always display at the bottom)
	SET @sqlUnion3 = 'SELECT NULL AS FormSetId, ''Duplicate'' AS FormSetName, NULL AS MasterFormId, (
                           SELECT   LetterType_1
                           FROM     dbo.PhaseDashboard
                           WHERE    PhaseId = ' + CAST(@phaseId AS VARCHAR(20)) + '
                           ) AS LetterCount, NULL AS LetterId, ' + CAST(@phaseId AS VARCHAR(20)) + ' AS PhaseId,
                NULL AS LetterSequence, NULL AS Size, NULL AS LastName, NULL As FirstName, 
                NULL AS OrganizationName, NULL AS Confidence, 1 AS FormSetSort'			

	SET @sql = @sqlSelect + 'FROM (' + @sqlUnion1 + @sqlWhere + ' UNION ' + @sqlUnion2 + @sqlWhere + ' UNION ' + @sqlUnion3 + @sqlWhere + ') fs '

	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL 
    BEGIN
        SET @sortBy = @sortKey
		
			-- translate the sort key into the corresponding column name or clause
        SET @sortBy = REPLACE(@sortBy, 'AuthorName ASC',
                              'LastName ASC, FirstName ASC')
        SET @sortBy = REPLACE(@sortBy, 'AuthorName DESC',
                              'LastName DESC, FirstName DESC')
        IF (PATINDEX('%FormSetName%', @sortBy) <= 0)
			SET @sortBy = @sortBy + ', FormSetName'
    END
        
	-- Default Sort By clause
	IF @sortBy IS NULL 
    BEGIN
        SET @sortBy = 'FormSetSort, FormSetName'
    END

	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 
    BEGIN
        EXECUTE dbo.spsRowPager 
            @SQL = @sql,
            @RecordsPerPage = @numRows,
            @Page = @pageNum,
            @ID = 'FormSetId',
            @SortBy = @sortBy
			
        EXEC ('SELECT 1 FROM (' + @sqlUnion1 + @sqlWhere + ' UNION ' + @sqlUnion2 + @sqlWhere + ') fs ')
        SET @totalRows = @@ROWCOUNT
			
    END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE 
    BEGIN
        SET @sql = @sql + ' ORDER BY ' + @sortBy		
        EXEC (@sql)
        SET @totalRows = @@ROWCOUNT		
    END

	RETURN

GO
GRANT EXECUTE ON  [dbo].[spsFormSets] TO [carauser]
GO
