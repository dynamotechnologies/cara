
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/* This stored procedure is for Projects page */

CREATE PROCEDURE [dbo].[spsProjects]
(
	@filterKeyword varchar(100) = NULL,
	@filterUnitId VARCHAR(8) = NULL,
	@filterAnalysisTypeId INT = -1,
	@filterProjectStatusId INT = -1,
	@filterProjectActivityId INT = -1,
	@filterStateId VARCHAR(20) = -1,
	@filterMyProjects bit = NULL,
	@filterArchived BIT = 0,
	@UserId int = NULL,
	@pageNum int = 1,
	@numRows int = -1,
	@sortKey varchar(50) = NULL,
	@filterPhaseTypeId INT = -1,
	@totalRows int OUT
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON

	DECLARE @sql varchar(MAX), @sortBy varchar(255), @sqlWhere varchar(MAX), @countSql varchar(MAX), @sqlSelect VARCHAR(MAX)
	DECLARE @Asterisk varchar(20)= '*', @SingleQuote VARCHAR(1) = ''''
	DECLARE @sqlProject VARCHAR(MAX), @sqlWhereJoin VARCHAR(50)

	IF @UserId IS NULL
	BEGIN
		SET @UserId = 0
	END

	-- If my project filter is used, select all available phases for the accessible projects
	IF (@filterMyProjects = 1)
	BEGIN
		SET @sqlProject = ' dbo.Project p '
		SET @sqlWhereJoin = ''
	END
	ELSE
	-- ----------------------------------------------------------------------------------------------------------------------
	-- If my project filter is not used, select based on the following project phase result set based on the union of:
	-- 1. All projects with most current phase
	-- 2. All projects with most current locked phase where user is team member
	-- 3. All proects with most current locked phase where user has create project privilege and is not a team member
	-- ----------------------------------------------------------------------------------------------------------------------
	BEGIN
		IF (@filterKeyword IS NOT NULL)
		BEGIN
		SET @sqlProject = '
			(SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, null as PhaseId
			 FROM Project p
			 WHERE CONTAINS((p.Description, p.ProjectNumber, p.Name),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ') 
				  AND p.ProjectId NOT IN (Select Projectid from Phase)
			UNION
			SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, vcpu.phaseid
			FROM Project p
				  INNER JOIN dbo.vCurrentPhasePublic vcpu ON p.projectid = vcpu.projectid
			      INNER JOIN dbo.PhaseMemberRole pmr ON vcpu.phaseid = pmr.PhaseId
				  INNER JOIN dbo.[User] u ON pmr.UserId = u.UserId
			WHERE CONTAINS((p.Description, p.ProjectNumber, p.Name),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ') 
	              OR (
						(
							u.FirstName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
								OR u.LastName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
						)
						AND pmr.ACTIVE = 1
					 )
			UNION			
			SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, vcp.phaseid
			FROM Project p
				  INNER JOIN dbo.vCurrentPhase vcp ON p.projectid = vcp.projectid
				  INNER JOIN dbo.PhaseMemberRole pmr ON vcp.phaseid = pmr.PhaseId
		          INNER JOIN dbo.PhaseMemberRole pmr2 ON vcp.phaseid = pmr2.PhaseId
				  INNER JOIN dbo.[User] u ON pmr2.UserId = u.UserId
			WHERE u.userId = ' + CAST(@UserId as varchar(20)) + ' AND pmr.Active = 1 AND NOT EXISTS (SELECT 1 FROM vCurrentPhasePublic WHERE phaseId = vcp.phaseid)
				AND (CONTAINS((p.Description, p.ProjectNumber, p.Name),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ') 
	              OR (
						(
							u.FirstName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
								OR u.LastName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%''
						)
						AND pmr2.ACTIVE = 1
					 ))
			UNION			
			SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, vcp.phaseid
			FROM Project p, dbo.vCurrentPhase vcp, dbo.UserSystemRole usr, dbo.RolePrivilege rp, dbo.Privilege pr, dbo.PhaseMemberRole pmr, dbo.[User] u
			WHERE p.projectid = vcp.projectid AND usr.userId = ' + CAST(@UserId as varchar(20)) + ' AND usr.roleId = rp.RoleId 
				AND rp.PrivilegeId = pr.PrivilegeId AND vcp.phaseid = pmr.PhaseId AND pmr.UserId = u.UserId AND pr.code = ''MPRJ''
				AND (CONTAINS((p.Description, p.ProjectNumber, p.Name),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ')
						 OR (
							 (
							  u.FirstName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
							  OR u.LastName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
							 )
							 AND pmr.ACTIVE = 1
							)
					)
				AND NOT EXISTS (SELECT 1 FROM dbo.vCurrentPhasePublic WHERE phaseId = vcp.phaseid)
				AND NOT EXISTS (SELECT 1 FROM dbo.PhaseMemberRole WHERE userId = ' + CAST(@UserId as varchar(20)) + ' AND phaseId = vcp.phaseid)
			) AS p '

		END
		ELSE
		BEGIN
			
		SET @sqlProject = '
			(SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, null as PhaseId
			 FROM Project p
				  WHERE p.ProjectId NOT IN (Select Projectid from Phase)
			UNION			
			SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, vcpu.phaseid
			FROM Project p
				  INNER JOIN dbo.vCurrentPhasePublic vcpu ON p.projectid = vcpu.projectid
			UNION			
			SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, vcp.phaseid
			FROM Project p
				  INNER JOIN dbo.vCurrentPhase vcp ON p.projectid = vcp.projectid
				  INNER JOIN dbo.PhaseMemberRole pmr ON vcp.phaseid = pmr.PhaseId
			WHERE pmr.userId = ' + CAST(@UserId as varchar(20)) + ' AND pmr.Active = 1 AND NOT EXISTS (SELECT 1 FROM vCurrentPhasePublic WHERE phaseId = vcp.phaseid)
			UNION			
			SELECT p.ProjectId, p.NAME, p.ProjectNumber, p.Description, p.UnitId, p.AnalysisTypeId, p.ProjectStatusId, vcp.phaseid
			FROM Project p, dbo.vCurrentPhase vcp, dbo.UserSystemRole usr, dbo.RolePrivilege rp, dbo.Privilege pr
			WHERE p.projectid = vcp.projectid AND usr.userId = ' + CAST(@UserId as varchar(20)) + ' AND usr.roleId = rp.RoleId AND rp.PrivilegeId = pr.PrivilegeId AND pr.code = ''MPRJ''
				AND NOT EXISTS (SELECT 1 FROM dbo.vCurrentPhasePublic WHERE phaseId = vcp.phaseid)
				AND NOT EXISTS (SELECT 1 FROM dbo.PhaseMemberRole WHERE userId = ' + CAST(@UserId as varchar(20)) + ' AND phaseId = vcp.phaseid)
			) AS p '
		END
			-- this where clause is added for joining the phaseId from the union set and the phase table
			SET @sqlWhereJoin = 'AND p.phaseId = ph.phaseId'
	END

	-- -------------------------------
	-- Construct the select query
	-- -------------------------------
	SET @sqlWhere = ''
	
	SET @countSql = 'SELECT p.ProjectId '
	SET @sqlSelect = 'SELECT p.ProjectId, p.NAME AS ProjectName, p.ProjectNumber, p.UnitId, u.Name UnitName, p.AnalysisTypeId, at.Name as AnalysisTypeName,
		p.ProjectStatusId, pst.Name ProjectStatusName, ph.PhaseId, ph.PhaseTypeId, pt.Name PhaseTypeName, ISNULL(ph.NAME, '' '') AS PhaseName, ph.CommentStart,
		pd.LetterStatus_1 AS PhaseNewLetterCount,
		ph.CommentEnd, pd.EarlyActionStatus_2 + pd.EarlyActionStatus_3 + pd.EarlyActionStatus_4 AS PhaseEarlyActionLetterCount,
		pd.LetterStatus_2 AS PhaseCodingInProgressCount,
		pd.LetterStatus_3 AS PhaseCodingCompleteCount,
		pd.LetterType_2 AS PhaseUniqueLettersCount,
		pd.LetterType_1 AS PhaseDuplicateLettersCount,
		pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 AS PhaseFormLettersCount,
		pd.LetterType_2 + pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 + pd.LetterType_1 AS PhaseTotalLettersCount '
	
	SET @sql = 'FROM ' + @sqlProject + ', dbo.Phase ph, dbo.Unit u, dbo.AnalysisType at, dbo.ProjectStatus pst, dbo.PhaseType pt, dbo.PhaseDashboard pd
		 WHERE p.ProjectId = ph.ProjectId AND p.UnitId = u.UnitId AND p.AnalysisTypeId = at.AnalysisTypeId AND p.ProjectStatusId = pst.ProjectStatusId
			AND ph.PhaseTypeId = pt.PhaseTypeId AND ph.PhaseId = pd.PhaseId ' + @sqlWhereJoin + ' and ph.Deleted = 0 '
	
	-- --------------------------
	-- Construct filters
	-- --------------------------
	IF (@filterArchived = 0)
	BEGIN
		SET @sqlWhere = @sqlWhere + 'and ph.Locked = 0 '
	END
	
	IF (@filterKeyword IS NOT NULL AND @filterMyProjects = 1)	
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND (CONTAINS((p.Description, p.ProjectNumber, p.Name),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ')
						 OR (
							 (
							  u.FirstName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
							  OR u.LastName LIKE ''' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + '%'' 
							 )
							 AND pmr.ACTIVE = 1
							)
					) '
	END
		
	IF (@filterUnitId IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND p.UnitId LIKE ''' + @filterUnitId + '%'''
	END
	
	IF (@filterAnalysisTypeId <> -1)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND p.AnalysisTypeId = ' + CAST(@filterAnalysisTypeId as varchar(20))
	END
	
	IF (@filterProjectStatusId <> -1)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND p.ProjectStatusId = ' + CAST(@filterProjectStatusId as varchar(20))
	END

	IF (@filterProjectActivityId <> -1)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND p.ProjectId in (select ProjectId from ProjectActivity where ActivityId = ' + CAST(@filterProjectActivityId as varchar(20)) + ')'
	END
	
	IF (@filterPhaseTypeId <> -1)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND p.ProjectId in (select ProjectId from Phase where PhaseTypeId = ' + CAST(@filterPhaseTypeId as varchar(20)) + ')'
	END

	IF (@filterStateId IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND EXISTS (SELECT 1 FROM dbo.Location WHERE ProjectId = p.ProjectId AND AreaType = ''State'' AND AreaId = ''' + @filterStateId + ''')'
	END	
	
	IF (@filterMyProjects = 1)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND ph.PhaseId in (SELECT ph.phaseid 
		FROM phase ph, phasememberrole pmr 
		WHERE ph.phaseid = pmr.phaseid
		AND pmr.ACTIVE = 1 AND pmr.userid = ' + CAST(@UserId as varchar(20)) + ')'
	END
	
	SET @sqlSelect = @sqlSelect + @sql + @sqlWhere
		
	print @sqlSelect

	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL
	BEGIN
		SET @sortBy = @sortKey
		
		-- translate the sort key into the corresponding column name or clause
		SET @sortBy = REPLACE(@sortBy, 'ProjectName', 'p.Name')
		SET @sortBy = REPLACE(@sortBy, 'AnalysisTypeName', 'at.Name')
		SET @sortBy = REPLACE(@sortBy, 'PhaseTypeName', 'pt.Name')
		SET @sortBy = REPLACE(@sortBy, 'PhaseId', 'ph.PhaseId')
		SET @sortBy = REPLACE(@sortBy, 'ProjectStatusName', 'pst.Name')
		SET @sortBy = REPLACE(@sortBy, 'UnitName', 'u.Name')
		SET @sortBy = REPLACE(@sortBy, 'CommentStart', 'ph.CommentStart')
		SET @sortBy = REPLACE(@sortBy, 'PhaseNewLetterCount', 'pd.LetterStatus_1')
		SET @sortBy = REPLACE(@sortBy, 'PhaseEarlyActionLetterCount', 'pd.EarlyActionStatus_2 + pd.EarlyActionStatus_3 + pd.EarlyActionStatus_4')
	   	SET @sortBy = REPLACE(@sortBy, 'PhaseCodingInProgressCount', 'pd.LetterStatus_2')
		SET @sortBy = REPLACE(@sortBy, 'PhaseCodingCompleteCount', 'pd.LetterStatus_3')
		SET @sortBy = REPLACE(@sortBy, 'PhaseUniqueLettersCount', 'pd.LetterType_2')
		SET @sortBy = REPLACE(@sortBy, 'PhaseDuplicateLettersCount', 'pd.LetterType_1')
		SET @sortBy = REPLACE(@sortBy, 'PhaseFormLettersCount', 'pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5')
		SET @sortBy = REPLACE(@sortBy, 'PhaseTotalLettersCount', 'pd.LetterType_2 + pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 + pd.LetterType_1')		
	END

	-- Default Sort By clause
	IF @sortBy IS NULL
	BEGIN
		SET @sortBy = 'ph.CommentStart desc'
	END
	
	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1
	BEGIN
		EXECUTE dbo.spsRowPager
			@SQL = @sqlSelect,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = 'projectId',
			@SortBy = @sortBy
		
		SET @countSql = @countSql + @sql + @sqlWhere
		
		EXEC (@countSql)
		SET @totalRows = @@ROWCOUNT
			
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy		
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT		
	END

	RETURN

GO

GRANT EXECUTE ON  [dbo].[spsProjects] TO [carauser]
GO
