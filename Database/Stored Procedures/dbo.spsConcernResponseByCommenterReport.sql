
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure provides a crosswalk from a commenter's name to the Concern/Response Number */

CREATE PROCEDURE [dbo].[spsConcernResponseByCommenterReport]
(
 @phaseId INT = -1,
 @option INT = 1,
 @pageNum INT = 1,
 @numRows INT = -1,
 @totalRows INT OUT
)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
SET NOCOUNT ON ;
    
	DECLARE @startRow INT
	DECLARE @endRow INT
	    
	SELECT  @totalRows = COUNT(*)
	FROM    dbo.Letter
	WHERE   PhaseId = @phaseId
	       
	IF @numRows <= 0 
		BEGIN
			SET @startRow = 1
			SET @endRow = @totalRows
		END
	ELSE 
		BEGIN
			SET @startRow = ((@pageNum - 1) * @numRows) + 1
			SET @endRow = @pageNum * @numRows
		END ;
	    
	IF @option = 1 
	BEGIN
		WITH    ConcernResponseNumbers
				  AS (
					  SELECT DISTINCT
								cr.ConcernResponseId,
								cr.Sequence AS ConcernResponseNumber,
								l.LetterId
					  FROM      dbo.ConcernResponse cr
								INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
								INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
														   AND l.PhaseId = @phaseId
					 ),
				ReportResults
				  AS (
					  SELECT    a.LastName, a.FirstName,
								ISNULL(o.Name, '') AS OrganizationName,
								l.LetterSequence,
								ISNULL(LEFT(list.crn, LEN(list.crn) - 1), '')
								AS ConcernResponseNumber,
								ROW_NUMBER() OVER (ORDER BY a.LastName, a.FirstName, l.LetterSequence) AS RowNumber,
								ROW_NUMBER() OVER (ORDER BY a.LastName DESC, a.FirstName DESC, l.LetterSequence DESC) AS TotalRows
					  FROM      dbo.Letter l
								INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
								LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
								CROSS APPLY (
											 SELECT CAST(cr.ConcernResponseNumber AS VARCHAR(20))
													+ ', '
											 FROM   ConcernResponseNumbers cr
											 WHERE  cr.LetterId = l.LetterId
											 ORDER BY cr.ConcernResponseNumber 
											FOR
											 XML PATH('')
											) list (crn)
					  WHERE     l.PhaseId = @phaseId
								AND a.Sender = 1
					 )

			SELECT  LastName, FirstName, OrganizationName, LetterSequence,
					ConcernResponseNumber, CAST(ReportResults.TotalRows + ReportResults.RowNumber - 1 AS INT) AS TotalRows
			FROM    ReportResults
			WHERE   RowNumber BETWEEN @startRow AND @endRow
	END
	
	IF @option = 2 
	BEGIN
		WITH    ConcernResponseNumbers
				  AS (
					  SELECT DISTINCT
								cr.ConcernResponseId,
								cr.Sequence AS ConcernResponseNumber,
								l.LetterId
					  FROM      dbo.ConcernResponse cr
								INNER JOIN dbo.Comment c ON cr.ConcernResponseId = c.ConcernResponseId
								INNER JOIN dbo.Letter l ON c.LetterId = l.LetterId
														   AND l.PhaseId = @phaseId
					 ),
				ReportResults
				  AS (
					  SELECT    a.LastName, a.FirstName,
								ISNULL(o.Name, '') AS OrganizationName,
								l.LetterSequence,
								ISNULL(LEFT(list.crn, LEN(list.crn) - 1), '')
								AS ConcernResponseNumber,
								ROW_NUMBER() OVER (ORDER BY a.LastName, a.FirstName, l.LetterSequence) AS RowNumber,
								ROW_NUMBER() OVER (ORDER BY a.LastName DESC, a.FirstName DESC, l.LetterSequence DESC) AS TotalRows
					  FROM      dbo.Letter l
								INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
								LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
								CROSS APPLY (
											 SELECT CAST(cr.ConcernResponseNumber AS VARCHAR(20))
													+ ', '
											 FROM   ConcernResponseNumbers cr
											 WHERE  cr.LetterId = l.LetterId
											 ORDER BY cr.ConcernResponseNumber 
											FOR
											 XML PATH('')
											) list (crn)
					  WHERE     l.PhaseId = @phaseId
								AND a.Sender = 1
								AND (l.LetterTypeId = 2 OR l.LetterTypeId = 3 OR l.LetterTypeId = 5)
					 )

			SELECT  LastName, FirstName, OrganizationName, LetterSequence,
					ConcernResponseNumber, CAST(ReportResults.TotalRows + ReportResults.RowNumber - 1 AS INT) AS TotalRows
			FROM    ReportResults
			WHERE   RowNumber BETWEEN @startRow AND @endRow
	END
	   
RETURN
GO




GRANT EXECUTE ON  [dbo].[spsConcernResponseByCommenterReport] TO [carauser]
GO
