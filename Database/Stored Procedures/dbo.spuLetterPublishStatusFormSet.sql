
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is to ensure all of the letters in a form set have the same publishing status as the master */

CREATE PROCEDURE [dbo].[spuLetterPublishStatusFormSet]
(
	@formSetId INT,
	@publishToReadingRoom BIT,
	@unpublishedReasonId INT,
	@unpublishedReasonOther VARCHAR(50)
)
AS
	SET NOCOUNT OFF
	
	UPDATE TOP (200) dbo.Letter
	SET PublishToReadingRoom = @publishToReadingRoom,
		UnpublishedReasonId = @unpublishedReasonId,
		UnpublishedReasonOther = @unpublishedReasonOther
	WHERE FormSetId = @formSetId
		AND LetterTypeId != 5 -- = Form Plus
		AND PublishToReadingRoom != @publishToReadingRoom

	DECLARE @rowsUpdated INT
	SELECT @rowsUpdated = @@ROWCOUNT
	RETURN @rowsUpdated

GO

GRANT EXECUTE ON  [dbo].[spuLetterPublishStatusFormSet] TO [carauser]
GO
