
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO




/* This stored procedure is for comment page */

CREATE PROCEDURE [dbo].[spsComments]
    (
      @phaseId INT = -1 ,
      @filterLetterSequence VARCHAR(100) = NULL ,
      @filterCommentNumber VARCHAR(100) = NULL,
      @filterCodeName VARCHAR(50) = NULL ,
      @filterCodeNumber VARCHAR(50) = NULL ,
      @filterCodeNumberList VARCHAR(500) = NULL ,
      @filterCodeCategoryId INT = -1 ,
      @filterKeyword VARCHAR(100) = NULL ,
      @filterAssociation BIT = NULL ,
      @filterSampleStatement BIT = NULL ,
      @filterNoResponseRequired BIT = NULL ,
      @filterNoResponseReasonId INT = -1 ,
      @filterAnnotation BIT = NULL ,
      @filterUserId INT = NULL,
      @pageNum INT = 1 ,
      @numRows INT = -1 ,
      @sortKey VARCHAR(50) = NULL ,
      @totalRows INT OUT
    )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON

    DECLARE @sql VARCHAR(8000),
		@sqlFrom VARCHAR(8000),
		@sqlCount VARCHAR(8000),
		@sqlSelect VARCHAR(8000),
        @sortBy VARCHAR(255) ,
        @sqlWhere VARCHAR(8000) ,
        @countSql VARCHAR(8000) ,
        @sqlCodeSearch VARCHAR(1000),
        @sqlPager VARCHAR(8000),
        @sqlGroupBy VARCHAR(1000)
        
    DECLARE @Asterisk VARCHAR(20)= '*' ,
        @singleQuote VARCHAR(1) = ''''	
    DECLARE @CodeRangeTemp TABLE
        (
          fromcode VARCHAR(20) ,
          tocode VARCHAR(20)
        )    
    DECLARE @Handle INT
	
	-- -------------------------------
	-- Construct the select query
	-- -------------------------------
    SET @sqlWhere = ' WHERE 1 = 1 '
    SET @sqlCodeSearch = ''
    
    
    SET @sqlGroupBy = 'GROUP BY lt.PhaseId, cm.CommentId, cm.LetterId, lt.LetterSequence, 
        cm.ConcernResponseId, cr.ConcernResponseNumber, cm.SampleStatement, 
        cm.CommentNumber, pc2.CodeNumber, pc2.CodeName, cm.UserId '
     
    SET @sqlSelect = 'SELECT DISTINCT lt.PhaseId, cm.CommentId, cm.LetterId, lt.LetterSequence, cm.ConcernResponseId, cr.ConcernResponseNumber, 
                      cm.SampleStatement, null as CommentCodeList, cm.CommentNumber, MIN(pc.CodeNumber)  AS CodeNumber,
                      pc2.CodeNumber AS ConcernResponseCodeNumber, pc2.CodeName AS ConcernResponseCodeName, cm.UserId '
    
    SET @sqlCount = 'SELECT distinct cm.commentId '
    
    SET @sqlFrom = 'FROM dbo.Comment cm
			INNER JOIN dbo.CommentCode cc ON cc.CommentId = cm.CommentId
			INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
           	LEFT OUTER JOIN dbo.ConcernResponse cr ON cr.ConcernResponseId = cm.ConcernResponseId
           	LEFT OUTER JOIN dbo.PhaseCode pc2 ON cr.PhaseCodeId = pc2.PhaseCodeId 
			INNER JOIN dbo.Letter lt ON lt.LetterId = cm.LetterId '
	IF ( @sortKey LIKE '%UserId%')
	BEGIN
		SET @sqlFrom = @sqlFrom + 'LEFT OUTER JOIN 
			(SELECT LastName + '', '' + FirstName AS UserName, UserId FROM dbo.[User]) u ON u.UserId = cm.UserId '
		SET @sqlGroupBy = @sqlGroupBy + ', u.UserName '
		SET @sqlSelect = @sqlSelect + ', u.UserName '
	END			
	
	-- --------------------------
	-- Construct filters
	-- --------------------------
    IF ( @phaseId <> -1 ) 
        BEGIN
            SET @sqlWhere = @sqlWhere + ' AND lt.PhaseId = '
                + CAST(@phaseId AS VARCHAR(20))
        END

    IF ( @filterKeyword IS NOT NULL ) 
        BEGIN
            IF LEN(@sqlCodeSearch) > 0 
                SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
			set @sqlFrom = @sqlFrom + ' INNER JOIN dbo.CommentText ct on ct.CommentId = cm.CommentId '
            set @sqlCodeSearch = @sqlCodeSearch + 
				'ct.[Text] LIKE ''%' + REPLACE(@filterKeyword, '''', '''''') + '%'''
        END

    IF ( @filterCodeName IS NOT NULL ) 
        BEGIN
            IF LEN(@sqlCodeSearch) > 0 
                SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
				
            SET @sqlCodeSearch = @sqlCodeSearch
                + ' EXISTS (SELECT 1 FROM dbo.CommentCode cc
			INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
			WHERE cc.CommentId = cm.CommentId AND pc.CodeName LIKE ''%'
                + REPLACE(@filterCodeName, '''', '''''') + '%'')'
        END
	
    IF ( @filterCodeNumber IS NOT NULL ) 
        BEGIN
            IF LEN(@sqlCodeSearch) > 0 
                SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
				
            SET @sqlCodeSearch = @sqlCodeSearch
                + ' EXISTS (SELECT 1 FROM dbo.CommentCode cc
			INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
			WHERE cc.CommentId = cm.CommentId AND pc.CodeNumber LIKE '''
                + REPLACE(@filterCodeNumber, '''', '''''') + '%'')'
        END
     
    IF @filterCodeNumberList IS NOT NULL
        AND @filterCodeNumberList != '' 
        BEGIN
    
            EXEC sp_xml_preparedocument @handle OUTPUT, @filterCodeNumberList
    
            INSERT  INTO @CodeRangeTemp
                    ( fromcode ,
                      tocode 
                    )
                    SELECT  *
                    FROM    OPENXML (@handle, '/codeRanges/codeRange', 2) WITH   ([from] VARCHAR(20),   [to] VARCHAR(20) )
            
            EXEC sp_xml_removedocument @handle
    
            IF LEN(@sqlCodeSearch) > 0 
                SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
				
            SET @sqlCodeSearch = @sqlCodeSearch
                + ' EXISTS (SELECT 1 FROM dbo.CommentCode cc
			INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
			WHERE cc.CommentId = cm.CommentId AND ('            
    
            SELECT  @sqlCodeSearch = @sqlCodeSearch + ' (pc.CodeNumber >= '
                    + @singleQuote + fromcode + @singleQuote
                    + ' AND pc.CodeNumber <= ' + @singleQuote + tocode
                    + @singleQuote + ') OR '
            FROM    @CodeRangeTemp
    
            SELECT  @sqlCodeSearch = LEFT(@sqlCodeSearch,
                                          LEN(@sqlCodeSearch) - 3) + '))'
        
        END    
    	
    -- add the code search filter after building the where clause from the logic above
    IF LEN(@sqlCodeSearch) > 0 
        BEGIN
            SET @sqlWhere = @sqlWhere + ' AND (' + @sqlCodeSearch + ') '
        END
    
	--IF (@filterCodeFrom IS NOT NULL AND @filterCodeTo IS NOT NULL)
	--BEGIN
	--	SET @sqlWhere = @sqlWhere + ' AND EXISTS (SELECT 1 FROM dbo.CommentCode cc
	--		INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
	--		WHERE cc.CommentId = cm.CommentId AND pc.CodeNumber >= ''' + @filterCodeFrom + ''' AND pc.CodeNumber <= ''' + @filterCodeTo + ''')'
	--END
	
    IF ( @filterCodeCategoryId <> -1 ) 
        BEGIN
            SET @sqlWhere = @sqlWhere
                + ' AND EXISTS (SELECT 1 FROM dbo.CommentCode cc
			INNER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
			WHERE cc.CommentId = cm.CommentId AND pc.CodeCategoryId = '
                + CAST(@filterCodeCategoryId AS VARCHAR(20)) + ')'
        END
    
    IF ( @filterAssociation IS NOT NULL ) 
        BEGIN
            IF ( @filterAssociation = 1 ) 
                SET @sqlWhere = @sqlWhere
                    + ' AND cm.ConcernResponseId IS NOT NULL '
            ELSE 
                SET @sqlWhere = @sqlWhere
                    + ' AND cm.ConcernResponseId IS NULL '
        END
	
    IF ( @filterSampleStatement IS NOT NULL ) 
        BEGIN		
            SET @sqlWhere = @sqlWhere + ' AND cm.SampleStatement = '
                + CAST(@filterSampleStatement AS VARCHAR(20))
        END
	
    IF ( @filterNoResponseRequired IS NOT NULL ) 
        BEGIN
            IF ( @filterNoResponseRequired = 1 ) 
                SET @sqlWhere = @sqlWhere
                    + ' AND cm.NoResponseReasonId IS NOT NULL '
            ELSE 
                SET @sqlWhere = @sqlWhere
                    + ' AND cm.NoResponseReasonId IS NULL '
        END

    IF ( @filterNoResponseReasonId <> -1 ) 
        BEGIN
            SET @sqlWhere = @sqlWhere
                + ' AND cm.NoResponseReasonId = '
                + CAST(@filterNoResponseReasonId AS VARCHAR(20)) + ' '
        END
    	
    IF ( @filterAnnotation IS NOT NULL ) 
        BEGIN		
            IF ( @filterAnnotation = 1 ) 
                SET @sqlWhere = @sqlWhere
                    + ' AND (cm.Annotation IS NOT NULL AND cm.Annotation <> '''') '
            ELSE 
                SET @sqlWhere = @sqlWhere
                    + ' AND (cm.Annotation IS NULL OR cm.Annotation = '''') '
        END
        
	IF (@filterLetterSequence IS NOT NULL)
		BEGIN
			SET @sqlWhere = @sqlWhere
				+ ' AND lt.LetterSequence = '''
				+ CAST(@filterLetterSequence AS VARCHAR(20)) + ' '''
		END     

	IF (@filterCommentNumber IS NOT NULL)
		BEGIN
			SET @sqlWhere = @sqlWhere
				+ ' AND cm.CommentNumber = ''' + CAST(@filterCommentNumber AS VARCHAR(20)) + ' '''
		END
	IF (@filterUserId IS NOT NULL AND NOT @filterUserId = 0)
		BEGIN
			SET @sqlWhere = @sqlWhere
				+ ' AND cm.UserId = ''' +  CAST(@filterUserId AS VARCHAR(20)) + ' '''
		END
	
    SET @sql = @sqlFrom + @sqlWhere + @sqlGroupBy

	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
    IF @sortKey IS NOT NULL 
        BEGIN
            SET @sortBy = @sortKey
		
			-- translate the sort key into the corresponding column name or clause
			IF @sortBy LIKE 'LetterCommentNumber%'
			BEGIN
				DECLARE @sortBy1 VARCHAR(50)
				
				-- build second sort key
				SET @sortBy1 = REPLACE(@sortBy, 'LetterCommentNumber', 'cm.CommentNumber')
	            SET @sortBy = REPLACE(@sortBy, 'LetterCommentNumber', 'lt.LetterSequence') + ', ' + @sortBy1
	        END
	        
            SET @sortBy = REPLACE(@sortBy, 'LetterId', 'cm.LetterId')
            SET @sortBy = REPLACE(@sortBy, 'ConcernResponseNumber', 'cm.ConcernResponseNumber')
            SET @sortBy = REPLACE(@sortBy, 'SampleStatementDisplay', 'cm.SampleStatement')
            SET @sortBy = REPLACE(@sortBy, 'FormattedConcernResponseCode', 'ConcernResponseCodeNumber')
            SET @sortBy = REPLACE(@sortBy, 'UserId', 'u.UserName')
        END

	-- Default Sort By clause
    IF @sortBy IS NULL 
        SET @sortBy = 'CommentCodeText' --CodeNumber asc, LetterSequence asc, CommentNumber asc '
   
    /*CARA-956 If the sort order is by comment code text additional sorting is required*/
    /*to sort comments with multiple codes properly*/
    IF @sortBy LIKE 'CommentCodeText%'
		BEGIN
			DECLARE @ResultQuery VARCHAR(max) =
				'SELECT cm.CommentID AS CommentID, pc.CodeNumber AS CodeNumber, lt.LetterSequence AS LetterSequence, cm.CommentNumber AS CommentNumber '
				 + @sqlFrom + @sqlWhere +
				'ORDER BY commentID, cc.sequence'
			DECLARE @ResultTable TABLE (CommentID INT, CodeNumber VARCHAR(20), LetterSequence INT, CommentNumber INT)
			INSERT @ResultTable (CommentID, CodeNumber, LetterSequence, CommentNumber) EXEC (@ResultQuery)
			--Create a cursor to iterate of the result table
			DECLARE @LastCommentId INT
			DECLARE @CommentId INT
			DECLARE @CodeNumber VARCHAR(20)
			DECLARE @CodeNumberList VARCHAR(Max) = ''
			DECLARE @LastLetterSequence INT
			DECLARE @LetterSequence INT
			DECLARE @LastCommentNumber INT
			DECLARE @CommentNumber INT
			DECLARE @ReturnTable TABLE (CommentID INT, CodeNumbers VARCHAR(MAX), LetterSequence INT, CommentNumber INT)
			
			DECLARE cursor1 CURSOR FOR SELECT * FROM @ResultTable
			OPEN cursor1
			FETCH NEXT FROM cursor1 INTO @CommentId, @CodeNumber, @LetterSequence, @CommentNumber
			SET @LastCommentId = @CommentId
			SET @CodeNumberList = ''
			SET @LastLetterSequence = @LetterSequence
			SET @LastCommentNumber = @CommentNumber
			While (@@Fetch_Status =0)
			BEGIN 
				IF (@LastCommentId <> @CommentId)
				BEGIN
					INSERT INTO  @ReturnTable (CommentID, CodeNumbers, LetterSequence, CommentNumber) VALUES
						(@LastCommentId, @CodeNumberList, @LastLetterSequence, @LastCommentNumber)
					SET @LastCommentId = @CommentId
					SET @CodeNumberList = ''
					SET @LastLetterSequence = @LetterSequence
					SET @LastCommentNumber = @CommentNumber
				END
				SET @CodeNumberList = @CodeNumberList + '_' + @CodeNumber
				FETCH NEXT FROM cursor1 INTO @CommentId, @CodeNumber, @LetterSequence, @CommentNumber
			END
			IF NOT (@CommentId IS NULL)
				INSERT INTO  @ReturnTable (CommentID, CodeNumbers, LetterSequence, CommentNumber) VALUES
					(@LastCommentId, @CodeNumberList, @LastLetterSequence, @LastCommentNumber)
			CLOSE cursor1
			DEALLOCATE cursor1
			
			--Resort and return with proper paging
			/* -------------------------------------------------------------------------------------------*/
			/* If @numRows = -1, then we want to return all records       */
			/* Otherwise, we want to page to @pageNum                       */
			/* -------------------------------------------------------------------------------------------*/
			IF (@numRows <> -1 AND @numRows > 0)
				BEGIN 
					IF @sortBy LIKE '%desc%'
						SELECT RowNumber, CommentId
						FROM (SELECT row_number() over(ORDER BY CodeNumbers DESC, LetterSequence DESC, CommentNumber DESC)
						 AS [RowNumber], * FROM @ReturnTable) AS t
						WHERE RowNumber BETWEEN CAST(((@numRows * ( @pageNum - 1 ) ) + 1 ) AS VARCHAR(20))
							AND CAST(( @numRows * @pageNum ) AS VARCHAR(20))
						ORDER BY RowNumber
					ELSE
					--default behavior should be sort by CodeNumber asc, LetterSequence asc, CommentNumber asc '
						SELECT RowNumber, CommentId
						FROM (
							SELECT row_number() over(ORDER BY CodeNumbers ASC, LetterSequence ASC, CommentNumber ASC)
								AS [RowNumber], CodeNumbers, CommentID
							FROM @ReturnTable) AS t
						WHERE RowNumber BETWEEN CAST(( ( @numRows * ( @pageNum - 1 ) ) + 1 ) AS VARCHAR(20))
							AND CAST(( @numRows * @pageNum ) AS VARCHAR(20))
						ORDER BY RowNumber
					
					SELECT CommentID FROM @ReturnTable
					SET @totalRows = @@ROWCOUNT
				END

			/* -------------------------------------------------------------------------------------------*/
			/* return all rows (no paging)                                                   */
			/* -------------------------------------------------------------------------------------------*/

			ELSE 
				BEGIN
					IF @sortBy LIKE '%desc%'
						SELECT * FROM @ReturnTable ORDER BY CodeNumbers desc, LetterSequence desc, CommentNumber desc
					ELSE
						SELECT * FROM @ReturnTable ORDER BY CodeNumbers ASC, LetterSequence ASC, CommentNumber ASC
					SET @totalRows = @@ROWCOUNT
				END
		END 
    ELSE
		BEGIN 
			/* -------------------------------------------------------------------------------------------*/
			/* If @numRows = -1, then we want to return all records       */
			/* Otherwise, we want to page to @pageNum                       */
			/* -------------------------------------------------------------------------------------------*/
			IF (@numRows <> -1 AND @numRows > 0)
				BEGIN
					-- -------------------------------------------
					-- Get rowcount for all possible rows
					-- -------------------------------------------
					DECLARE @pagerSortBy VARCHAR(50)
					SET @sqlSelect = @sqlSelect + @sql
					
					SET @pagerSortBy = REPLACE(@sortBy, 'cm.', '')
					SET @pagerSortBy = REPLACE(@pagerSortBy, 'lt.', '')
					SET @pagerSortBy = REPLACE(@pagerSortBy, 'pc.', '')
					SET @pagerSortBy = REPLACE(@pagerSortBy, 'pc2.', '')
					SET @pagerSortBy = REPLACE(@pagerSortBy, 'u.', '')
			
					--Code to set the Pager
				   SET @sqlPager = 'SELECT RowNumber, CommentId
									FROM (
									   SELECT row_number()  over(ORDER BY '+ @pagerSortBy + ') AS [RowNumber], * FROM (' + @sqlSelect + '
											 ) as t ) as t1
									   WHERE RowNumber
									   BETWEEN '
											+ CAST(( ( @numRows * ( @pageNum - 1 ) ) + 1 ) AS VARCHAR(20))
											+ ' AND ' + CAST(( @numRows * @pageNum ) AS VARCHAR(20)) + ' ORDER BY RowNumber'
		                    
					EXEC (@sqlPager)
					EXEC (@sqlCount + @sql)
					SET @totalRows = @@ROWCOUNT
				END

			/* -------------------------------------------------------------------------------------------*/
			/* return all rows (no paging)                                                   */
			/* -------------------------------------------------------------------------------------------*/

			ELSE 
				BEGIN
					SET @sql = @sqlSelect + @sql + ' ORDER BY ' + @sortBy     
					EXEC (@sql)
					SET @totalRows = @@ROWCOUNT
				END
		END 

    RETURN



GO






GRANT EXECUTE ON  [dbo].[spsComments] TO [carauser]
GO
