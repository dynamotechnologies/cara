
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for the team member report */

CREATE PROCEDURE [dbo].[spsTeamMemberReport] 
( 
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
  )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON
    
    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)

    BEGIN
   
		   Set @sql = 'SELECT  u.FirstName AS TeamMemberFirstName,
			           u.LastName AS TeamMemberLastName, r.Name AS RoleName,
			           NULL AS UnitName, u.Phone AS TeamMemberPhone,
			           u.Email AS TeamMemberEmail, pmr.Active AS TeamMemberStatus
		       FROM    dbo.PhaseMemberRole pmr,
			           dbo.[User] u,
			           dbo.Role r
		       WHERE   pmr.UserId = u.UserId
			           AND pmr.RoleId = r.RoleId
	
			           AND phaseid = ' + CAST(@phaseId as varchar(20))
	
	 END		
	 
	  IF @sortBy IS NULL
		SET @sortBy = 'RoleName, TeamMemberLastName, TeamMemberFirstName, TeamMemberEmail'           
    
    --ORDER BY r.Name, u.LastName, u.FirstName, u.Email
    
     /* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN


GO







GRANT EXECUTE ON  [dbo].[spsTeamMemberReport] TO [carauser]
GO
