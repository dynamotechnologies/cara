
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for getting user privileges */

CREATE PROCEDURE [dbo].[spsUserPrivileges]
(
	@UserId INT
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON

	SELECT DISTINCT usr.UserId, p.Code as PrivilegeCode, usr.UnitId, 0 as PhaseId
	FROM UserSystemRole usr
		INNER JOIN RolePrivilege rp on usr.RoleId = rp.RoleId
		INNER JOIN Privilege p on rp.PrivilegeId = p.PrivilegeId
	WHERE usr.UserId = @UserId
	UNION
	SELECT pmr.UserId, p.Code as PrivilegeCode, NULL AS UnitId, pmr.PhaseId
	FROM PhaseMemberRole pmr
		INNER JOIN RolePrivilege rp on pmr.RoleId = rp.RoleId
		INNER JOIN Privilege p on rp.PrivilegeId = p.PrivilegeId
	WHERE pmr.Active = 1 AND pmr.UserId = @UserId
GO

GRANT EXECUTE ON  [dbo].[spsUserPrivileges] TO [carauser]
GO
