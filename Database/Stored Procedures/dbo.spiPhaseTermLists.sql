
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/* This stored procedure is for inserting term lists to the phase */

CREATE PROCEDURE [dbo].[spiPhaseTermLists]
(
	@phaseId int,
	@termListIds XML = NULL
)
AS
	SET NOCOUNT OFF
	
	IF @phaseId IS NOT NULL
	BEGIN
		BEGIN TRAN
		
		-- delete current associations of phase and term lists
		DELETE FROM dbo.PhaseTermList
		WHERE PhaseId = @phaseId
				
		IF @termListIds IS NOT NULL
		BEGIN
			-- parse the id and insert the new associations
			INSERT INTO dbo.PhaseTermList( PhaseId, TermListId )
			SELECT @phaseId, list.id.value('.','VARCHAR(20)')
			FROM @termListIds.nodes('idList/id') AS list(id)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
			END
		END
		
		COMMIT TRAN
	END

	RETURN
GO

GRANT EXECUTE ON  [dbo].[spiPhaseTermLists] TO [carauser]
GO
