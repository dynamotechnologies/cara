
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for updating a concern response sequence */

CREATE PROCEDURE [dbo].[spuConcernResponseSequence]
(
	@concernResponseId INT,
	@sequence INT = NULL,
	@position VARCHAR(10) = NULL
)
AS
	SET NOCOUNT OFF
	
	IF @concernResponseId IS NOT NULL
	BEGIN
		DECLARE @maxSeq INT
		DECLARE @oldSeq INT
		DECLARE @phaseId INT
		
		-- get the phase id
		SELECT @phaseId = PhaseId
		FROM dbo.vConcernResponsePhase WHERE ConcernResponseId = @concernResponseId
		
		-- get the maximum sequence for the phase
		SELECT @maxSeq = ISNULL(MAX(Sequence), 0)
		FROM dbo.vConcernResponsePhase
		WHERE PhaseId = @phaseId
		
		-- if sequence is greater than max sequence, set sequence to max sequence
		IF @sequence > @maxSeq
		BEGIN
			SET @sequence = @maxSeq
		END

		-- If sequence is less than zero, set sequence to 1
		IF @sequence <= 0
		BEGIN
			SET @sequence = 1
		END
		
		-- get the current sequence
		SELECT @oldSeq = Sequence
		FROM dbo.ConcernResponse
		WHERE ConcernResponseId = @concernResponseId
		
		-- if position param is specified, convert it to sequence
		IF @position IS NOT NULL
		BEGIN
			SET @sequence = @oldSeq
			
			IF @position = 'First'
				SET @sequence = 1
			ELSE IF @position = 'Last'
				SET @sequence = @maxSeq
			ELSE IF @position = 'Prev' AND @oldSeq > 1
				SET @sequence = @oldSeq - 1
			ELSE IF @position = 'Next' AND @oldSeq < @maxSeq
				SET @sequence = @oldSeq + 1
		END
		
		-- move the preceding concern responses down if the new sequence is less than the current sequence
		IF @sequence < @oldSeq
		BEGIN
			UPDATE dbo.ConcernResponse
			SET Sequence = Sequence + 1
			WHERE ConcernResponseId IN (
				SELECT DISTINCT Comment.ConcernResponseId
				FROM dbo.Comment
				INNER JOIN dbo.Letter ON dbo.Comment.LetterId = dbo.Letter.LetterId
				WHERE Letter.PhaseId = @phaseId AND dbo.Comment.ConcernResponseId IS NOT NULL)
				AND Sequence >= @sequence AND Sequence < @oldSeq
		END
		ELSE IF @sequence > @oldSeq
		-- move the following concern response up if the new sequence is greater than the current sequence
		BEGIN
			UPDATE dbo.ConcernResponse
			SET Sequence = Sequence - 1
			WHERE ConcernResponseId IN (
				SELECT DISTINCT Comment.ConcernResponseId
				FROM dbo.Comment
				INNER JOIN dbo.Letter ON dbo.Comment.LetterId = dbo.Letter.LetterId
				WHERE Letter.PhaseId = @phaseId AND dbo.Comment.ConcernResponseId IS NOT NULL)
				AND Sequence > @oldSeq AND Sequence <= @sequence
		END
		
		-- update the concern response with new sequence
		UPDATE dbo.ConcernResponse
		SET Sequence = @sequence
		WHERE ConcernResponseId = @concernResponseId
	END

	RETURN
GO



GRANT EXECUTE ON  [dbo].[spuConcernResponseSequence] TO [carauser]
GO
