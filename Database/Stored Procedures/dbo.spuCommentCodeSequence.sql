SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is for updating comment code sequence */

CREATE PROCEDURE [dbo].[spuCommentCodeSequence]
(
	@commentId INT,
	@phaseCodeId INT,
	@sequence INT
	
)
AS
	SET NOCOUNT OFF
	
	
	BEGIN
		
		-- update the comment code with new sequence
		UPDATE [dbo].[CommentCode]
		SET Sequence = @sequence
		WHERE CommentId = @CommentId
		AND PhaseCodeId = @phaseCodeId 
	END

IF @@ERROR <> 0
BEGIN
RETURN 1
END

ELSE
BEGIN
	RETURN 0
END
	



GO
GRANT EXECUTE ON  [dbo].[spuCommentCodeSequence] TO [carauser]
GO
