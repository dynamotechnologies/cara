SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure returns authors for a given project  */

CREATE PROCEDURE [dbo].[spsMailingListReport] 
( 
   @phaseId INT = -1,
   @pageNum INT = 1,
   @numRows INT = -1,
   @totalRows INT OUT
  )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON


    DECLARE @ProjectId INT
    DECLARE @sql VARCHAR(MAX)
    DECLARE @sortBy varchar(255)
 
   SELECT @ProjectId= p.ProjectId
                 FROM    dbo.Phase p
                 WHERE   p.ProjectId IN ( SELECT ProjectId
                         FROM   dbo.Phase p1
                         WHERE  p1.PhaseId = @phaseId)
                             
   
  
   IF @ProjectId > 0
   BEGIN
   
	   Set @sql = 'SELECT DISTINCT 
				a.LastName, a.FirstName, o.Name AS OrgName, a.Address1, a.Address2,
				a.City, a.StateId, a.ZipPostal, a.Email, a.ContactMethod
		FROM    dbo.Author a
				INNER JOIN dbo.Letter l ON a.LetterId = l.LetterId
				LEFT OUTER JOIN dbo.Organization o ON a.OrganizationId = o.OrganizationId
		WHERE   l.PhaseId IN ( SELECT   p.PhaseId
							   FROM     dbo.Phase p
							   WHERE    p.projectid = ' + CAST(@ProjectId as varchar(20)) + ')
				AND a.ContactMethod IS NOT NULL '
				
	
   END
   	  
   IF @sortBy IS NULL
		SET @sortBy = 'Lastname, Firstname'
  
   /* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1 AND @numRows > 0
	BEGIN
		EXECUTE dbo.spsRowPagerDistinct
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = '',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END
   
RETURN
GO
GRANT EXECUTE ON  [dbo].[spsMailingListReport] TO [carauser]
GO
