SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spsReviewableLetterCount] ( @userId INT = -1 )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON

	SELECT p.ProjectId, p.Name AS ProjectName, p.ProjectNumber, ph.PhaseId, ph.Name AS PhaseName,
		pd.EarlyActionStatus_2 AS EarlyActionCount, p.UnitId,
		COUNT(*) AS UnpublishedLetterCount
	FROM dbo.Project p
		INNER JOIN dbo.Phase ph ON p.ProjectId = ph.ProjectId
		INNER JOIN dbo.PhaseMemberRole pmr ON ph.PhaseId = pmr.PhaseId
		INNER JOIN dbo.PhaseDashboard pd ON ph.PhaseId = pd.PhaseId
		INNER JOIN dbo.Letter l ON ph.PhaseId = l.PhaseId
	WHERE pmr.UserId = @userId
		AND l.PublishToReadingRoom = 0
	GROUP BY p.ProjectId, p.Name, p.ProjectNumber, ph.PhaseId, ph.Name,
		pd.EarlyActionStatus_2, UnitId
	ORDER BY p.Name

    RETURN
GO
GRANT EXECUTE ON  [dbo].[spsReviewableLetterCount] TO [carauser]
GO
