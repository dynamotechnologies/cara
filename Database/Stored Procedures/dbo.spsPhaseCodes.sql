
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for Codes per phase */

CREATE PROCEDURE [dbo].[spsPhaseCodes]
    (
      @PhaseId INT = -1 ,
      @filterCodeName VARCHAR(50) = NULL ,
      @filterCodeNumber VARCHAR(50) = NULL ,
      @filterCodeNumberList VARCHAR(500) = NULL	
    )
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
    SET NOCOUNT ON ;

    DECLARE @phaseTypeId INT
    
    DECLARE @sql VARCHAR(8000)
    DECLARE @where VARCHAR(8000)
    DECLARE @orderBy VARCHAR(8000)
    
    DECLARE @singleQuote VARCHAR(1) = ''''
        
    DECLARE @CodeRangeTemp TABLE
        (
          fromcode VARCHAR(20) ,
          tocode VARCHAR(20)
        )    
    DECLARE @Handle INT
    	
    SELECT  @phaseTypeId = PhaseTypeId
    FROM    dbo.Phase
    WHERE   PhaseId = @phaseId ;
	
    SET @sql = 'WITH    TopLevelCodes ( PhaseCodeId, PhaseId, CodeId, ParentCodeId, CodeCategoryId, Name, CodeNumber, [Level], CanRemove, UserId )
              AS ( SELECT   PhaseCodeId ,
                            PhaseId ,
                            CodeId ,
                            ParentCodeId ,
                            CodeCategoryId ,
                            CodeName ,
                            CodeNumber ,
                            0 AS [Level] ,
                            CanRemove ,
                            UserId
                   FROM     vCodePhaseCode
                   WHERE    ParentCodeId IS NULL
                            AND ( PhaseId = ' + STR(@PhaseId) + '
                                  OR ( PhaseId IS NULL
                                       AND CodeId NOT IN (
                                       SELECT   codeid
                                       FROM     phasecode
                                       WHERE    PhaseId = ' + STR(@PhaseId)
        + ' 
                                                AND ParentCodeId IS NULL )
                                     )
                                )
                   UNION ALL
                   SELECT   cpc.PhaseCodeId ,
                            cpc.PhaseId ,
                            cpc.CodeId ,
                            cpc.ParentCodeId ,
                            cpc.CodeCategoryId ,
                            cpc.CodeName ,
                            cpc.CodeNumber ,
                            [Level] + 1 ,
                            cpc.CanRemove ,
                            cpc.UserId
                   FROM     vCodePhaseCode cpc
                            INNER JOIN TopLevelCodes AS t ON cpc.ParentCodeId = t.CodeId
                   WHERE    ( cpc.PhaseId = ' + STR(@PhaseId) + '
                              OR ( cpc.PhaseId IS NULL
                                   AND cpc.CodeId NOT IN (
                                   SELECT   codeid
                                   FROM     phasecode
                                   WHERE    PhaseId = ' + STR(@PhaseId) + ')
                                 )
                            )
                 ) 
                 '                
                 
        -- only select the codes with applicable code categories
        
    SET @sql = @sql + 'SELECT    PhaseCodeId ,
                            PhaseId ,
                            CodeId ,
                            ParentCodeId ,
                            CodeCategoryId ,
                            Name AS CodeName ,
                            CodeNumber ,
                            [Level] ,
                            CanRemove ,
                            UserId
                  FROM      TopLevelCodes
                  WHERE     TopLevelCodes.CodeCategoryId IN (
                            SELECT  CodeCategoryId
                            FROM    dbo.CodeCategoryPhaseType ccpt
                            WHERE   ccpt.PhaseTypeId = ' + STR(@phaseTypeId)
                            
                            
    SET @where = ''
	
    IF @filterCodeName IS NOT NULL 
    BEGIN
    	IF LEN(@where) > 0
			SET @where = @where + ' OR '
		
        SET @where = @where + ' [Name] LIKE ' + @singlequote + '%'
            + @filterCodeName + '%' + @singlequote
	END
	
    IF @filterCodeNumber IS NOT NULL 
    BEGIN
    	IF LEN(@where) > 0
			SET @where = @where + ' OR '
			
        SET @where = @where + ' CodeNumber LIKE ' + @singlequote
            + @filterCodeNumber + '%' + @singlequote
    END
    
    IF @filterCodeNumberList IS NOT NULL AND @filterCodeNumberList != ''
        BEGIN
			DECLARE @hasOrStatement BIT = 0
			
            EXEC sp_xml_preparedocument @handle OUTPUT, @filterCodeNumberList
    
            INSERT  INTO @CodeRangeTemp
                    ( fromcode ,
                      tocode 
                    )
                    SELECT  *
                    FROM    OPENXML (@handle, '/codeRanges/codeRange', 2) WITH   ([from] VARCHAR(20),   [to] VARCHAR(20) )
            
            EXEC sp_xml_removedocument @handle
    
    		IF LEN(@where) > 0
    		BEGIN
				SET @where = @where + ' OR ('
				SET @hasOrStatement = 1
			END
    
            SELECT  @where = @where + ' (CodeNumber >= ' + @singleQuote
                    + fromcode + @singleQuote + ' AND CodeNumber <= '
                    + @singleQuote + tocode + @singleQuote + ') OR '
            FROM    @CodeRangeTemp
    
            SELECT  @where = LEFT(@where, LEN(@where) - 3) 
			
			IF @hasOrStatement = 1
			BEGIN
				SET @where = @where + ')'
			END
        END
                            
    SET @OrderBy = ')
                  ORDER BY  codeid ASC ,
                            ParentCodeId ASC'

	-- add the code search filter after building the where clause from the logic above
	IF LEN(@where) > 0
    BEGIN
		SET @where = ' AND (' + @where + ') '
    END

    SET @sql = @sql + @where + @orderBy

    EXEC (@sql)
						
    RETURN
GO








GRANT EXECUTE ON  [dbo].[spsPhaseCodes] TO [carauser]
GO
