
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for ConcernResponse listing page */

CREATE PROCEDURE [dbo].[spsConcernResponses]
(
	@phaseId INT = -1,
	  @filterCodeName VARCHAR(50) = NULL ,
	  @filterCodeNumber VARCHAR(50) = NULL ,
	  @filterCodeNumberList VARCHAR(500) = NULL ,
	@filterDateUpdatedFrom DATE = NULL,
	@filterDateUpdatedTo DATE = NULL,
	@filterConcernResponseStatusId INT = -1,
	@filterConcernResponseNumber INT = -1,
	@filterKeyword varchar(100) = NULL,
	@filterCodeCategoryId INT = -1,
	@filterUserId INT = NULL,
	@pageNum int = 1,
	@numRows int = -1,
	@sortKey varchar(50) = NULL,
	@totalRows int OUT
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON

	DECLARE @sql varchar(8000), @sortBy varchar(255), @sqlWhere varchar(8000), @countSql varchar(8000)
	DECLARE @Asterisk varchar(20)= '*', @SingleQuote VARCHAR(1) = '''', @sqlCodeSearch VARCHAR(1000)
	
    DECLARE @CodeRangeTemp TABLE
        (
          fromcode VARCHAR(20) ,
          tocode VARCHAR(20)
        ) 
    DECLARE @Handle INT
	
	-- -------------------------------
	-- Construct the select query
	-- -------------------------------
	SET @sqlWhere = ' WHERE 1 = 1 '
    SET @sqlCodeSearch = ''
	SET @sql = ' SELECT cr.PhaseId, cr.ConcernResponseId, cr.ConcernResponseNumber, cr.ConcernResponseStatusId, crst.NAME AS ConcernResponseStatusName, crr.ConcernText, crr.ResponseText,
			cr.Sequence, cr.LastUpdated, cr.LastUpdatedBy, CAST(cr.CommentCount AS INT) AS CommentCount, cr.CodeName, cr.CodeNumber,
			crr.UserId, u.UserName, crr.Label
		FROM dbo.vConcernResponsePhase cr
			INNER JOIN dbo.ConcernResponse crr ON cr.ConcernResponseId = crr.ConcernResponseId
			INNER JOIN dbo.ConcernResponseStatus crst ON crst.ConcernResponseStatusId = cr.ConcernResponseStatusId
			LEFT OUTER JOIN 
			(SELECT LastName + '', '' + FirstName AS UserName, UserId FROM dbo.[User]) u ON u.UserId = crr.UserId'
	
	-- --------------------------
	-- Construct filters
	-- --------------------------
	IF (@phaseId <> -1)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND cr.PhaseId = ' + CAST(@phaseId as varchar(20))
	END

	IF (@filterKeyword IS NOT NULL AND LEN(@filterKeyword)>0)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND CONTAINS((crr.ResponseText, crr.ConcernText),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ')'
	END

	IF (@filterDateUpdatedFrom IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND cr.LastUpdated >= ''' + CAST(@filterDateUpdatedFrom as varchar(20)) + ''''
	END
	
	IF (@filterDateUpdatedTo IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND cr.LastUpdated <= ''' + CAST(@filterDateUpdatedTo as varchar(20)) + ''''
	END
	
	IF (@filterConcernResponseStatusId > 0)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND cr.ConcernResponseStatusId = ' + CAST(@filterConcernResponseStatusId as varchar(20))
	END
	
	IF (@filterConcernResponseNumber > 0)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND cr.ConcernResponseNumber = ' + CAST(@filterConcernResponseNumber as varchar(20))
	END
	
	IF (@filterCodeCategoryId > 0 OR 
		@filterCodeName IS NOT NULL OR 
		@filterCodeNumber IS NOT NULL OR 
		@filterCodeNumberList IS NOT NULL)
	BEGIN
		SET @sql = @sql + ' LEFT OUTER JOIN dbo.PhaseCode pc ON crr.PhaseCodeId = pc.PhaseCodeId'
	END
	
	IF (@filterCodeCategoryId > 0)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND pc.CodeCategoryId = ' + CAST(@filterCodeCategoryId as varchar(20))
	END

    IF ( @filterCodeName IS NOT NULL ) 
    BEGIN
        IF LEN(@sqlCodeSearch) > 0 
            SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
			
        SET @sqlCodeSearch = @sqlCodeSearch
            + ' EXISTS (SELECT 1 FROM dbo.PhaseCode pc 
				WHERE pc.PhaseCodeId = crr.PhaseCodeId AND pc.CodeName LIKE ''%'
            + REPLACE(@filterCodeName, '''', '''''') + '%'')'
    END
	
    IF ( @filterCodeNumber IS NOT NULL ) 
    BEGIN
        IF LEN(@sqlCodeSearch) > 0 
            SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
			
        SET @sqlCodeSearch = @sqlCodeSearch
            + ' EXISTS (SELECT 1 FROM dbo.PhaseCode pc
				WHERE pc.PhaseCodeId = crr.PhaseCodeId AND pc.CodeNumber LIKE '''
            + REPLACE(@filterCodeNumber, '''', '''''') + '%'')'
    END
    
    IF @filterCodeNumberList IS NOT NULL
        AND @filterCodeNumberList != '' 
    BEGIN

        EXEC sp_xml_preparedocument @handle OUTPUT, @filterCodeNumberList

        INSERT  INTO @CodeRangeTemp
                ( fromcode ,
                  tocode 
                )
                SELECT  *
                FROM    OPENXML (@handle, '/codeRanges/codeRange', 2) WITH   ([from] VARCHAR(20),   [to] VARCHAR(20) )
        
        EXEC sp_xml_removedocument @handle

        IF LEN(@sqlCodeSearch) > 0 
            SET @sqlCodeSearch = @sqlCodeSearch + ' OR '
			
        SET @sqlCodeSearch = @sqlCodeSearch
            + ' EXISTS (SELECT 1 FROM dbo.PhaseCode pc
				WHERE pc.PhaseCodeId = crr.PhaseCodeId AND ('            

        SELECT  @sqlCodeSearch = @sqlCodeSearch + ' (pc.CodeNumber >= '
                + @SingleQuote + fromcode + @SingleQuote
                + ' AND pc.CodeNumber <= ' + @SingleQuote + tocode
                + @SingleQuote + ') OR '
        FROM    @CodeRangeTemp

        SELECT  @sqlCodeSearch = LEFT(@sqlCodeSearch,
                                      LEN(@sqlCodeSearch) - 3) + '))'
    
    END    
    	
    -- add the code search filter after building the where clause from the logic above
    IF LEN(@sqlCodeSearch) > 0 
    BEGIN
        SET @sqlWhere = @sqlWhere + ' AND (' + @sqlCodeSearch + ') '
    END
    
	
	IF @filterUserId IS NOT NULL AND @filterUserId > 0
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND crr.UserId = ' + CAST(@filterUserId as varchar(20))
	END
	
	SET @sql = @sql + @sqlWhere
	
	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL
	BEGIN
		SET @sortBy = @sortKey
		
		-- translate the sort key into the corresponding column name or clause
		SET @sortBy = REPLACE(@sortBy, 'Sequence', 'cr.Sequence')
		SET @sortBy = REPLACE(@sortBy, 'ConcernResponseNumber', 'cr.ConcernResponseNumber')
		SET @sortBy = REPLACE(@sortBy, 'ConcernResponseId', 'cr.ConcernResponseId')
		SET @sortBy = REPLACE(@sortBy, 'ConcernResponseStatusName', 'crst.Name')
		SET @sortBy = REPLACE(@sortBy, 'LastUpdated', 'cr.LastUpdated')
		SET @sortBy = REPLACE(@sortBy, 'CommentCount', 'cr.CommentCount')
		SET @sortBy = REPLACE(@sortBy, 'FormattedCode', 'cr.CodeNumber')
        SET @sortBy = REPLACE(@sortBy, 'UserId', 'u.UserName')
        SET @sortBy = REPLACE(@sortBy, 'Label', 'crr.Label')
	END

	-- Default Sort By clause
	IF @sortBy IS NULL
		SET @sortBy = 'cr.ConcernResponseNumber asc'

	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1
	BEGIN
		EXECUTE dbo.spsRowPager
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = 'ConcernResponseId',
			@SortBy = @sortBy
			
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT	
		
			
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)
		SET @totalRows = @@ROWCOUNT		
	END

	RETURN

GO













GRANT EXECUTE ON  [dbo].[spsConcernResponses] TO [carauser]
GO
