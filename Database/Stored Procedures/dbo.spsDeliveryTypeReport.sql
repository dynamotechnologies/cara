
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure lists signature and letter counts by delivery type */

CREATE PROCEDURE [dbo].[spsDeliveryTypeReport] (@PhaseId INT = -1)
AS -- Note: All paging sps procedures should have SET NOCOUNT ON
SET NOCOUNT ON ;

SELECT  dtl.Name AS DeliveryTypeName, dta.SignatureCount, dtl.LetterCount
FROM    (
         SELECT ISNULL(dt.DeliveryTypeId, '-1') AS DeliveryTypeId,
                ISNULL(dt.Name, 'Unknown') AS Name, COUNT(AuthorId) AS SignatureCount
         FROM   dbo.Letter l
                INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
                LEFT OUTER JOIN dbo.DeliveryType dt ON l.DeliveryTypeId = dt.DeliveryTypeId
         WHERE  l.PhaseId = @PhaseId
         GROUP BY dt.Name, dt.DeliveryTypeId
        ) dta,
        (
         SELECT ISNULL(dt.DeliveryTypeId, '-1') AS DeliveryTypeId,
                ISNULL(dt.Name, 'Unknown') AS Name, COUNT(l.LetterId) AS LetterCount
         FROM   dbo.Letter l
                INNER JOIN dbo.Author a ON l.LetterId = a.LetterId
                LEFT OUTER JOIN dbo.DeliveryType dt ON l.DeliveryTypeId = dt.DeliveryTypeId
         WHERE  l.PhaseId = @PhaseId
			AND a.Sender = 1
         GROUP BY dt.Name, dt.DeliveryTypeId
        ) dtl
WHERE   dta.DeliveryTypeId = dtl.DeliveryTypeId
ORDER BY dtl.Name
							
RETURN
GO



GRANT EXECUTE ON  [dbo].[spsDeliveryTypeReport] TO [carauser]
GO
