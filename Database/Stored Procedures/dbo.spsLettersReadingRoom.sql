
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/* This stored procedure is for Letter Reading Room pages */

CREATE PROCEDURE [dbo].[spsLettersReadingRoom]
(
	@projectId INT = -1,
	@phaseId INT = -1,
	@filterLetterTypeId INT = -1,
	@filterFirstName VARCHAR(100) = NULL,
	@filterLastName VARCHAR(100) = NULL,
	@filterOrganizationId VARCHAR(100) = NULL,
	@filterKeyword varchar(100) = NULL,
	@filterPublishToReadingRoom bit = NULL,
	@filterShowForms BIT = NULL,
	@pageNum int = 1,
	@numRows int = -1,
	@sortKey varchar(50) = NULL,
	@totalRows int OUT
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON

	DECLARE @sql varchar(8000), @sortBy varchar(255), @sqlWhere varchar(8000), @sqlCount varchar(8000), @sqlFrom VARCHAR(8000)
	DECLARE @Asterisk varchar(20)= '*', @SingleQuote VARCHAR(1) = ''''
	DECLARE @attachmmentCount varchar(100)
	SET @attachmmentCount = 'ISNULL((SELECT COUNT(1) FROM dbo.LetterDocument WHERE LetterId = l.LetterId), 0)'
	DECLARE @currentPhase VARCHAR(200)
	SET @currentPhase = 'CAST(ISNULL((SELECT 1 FROM dbo.vCurrentPhase vcp WHERE vcp.ProjectId = ph.ProjectId AND vcp.PhaseId = ph.PhaseId), 0) AS bit)'
	
	
	-- -------------------------------
	-- Construct the select query
	-- -------------------------------
	SET @sqlWhere = ' WHERE l.Deleted = 0 AND l.LetterTypeId <> 1 ' -- don't show duplicates
	SET @sqlCount = ' SELECT 1 '
	SET @sql = ' SELECT l.PhaseId, ph.Name PhaseName, ph.PhaseTypeId, ph.TimeZone, l.LetterId, l.LetterSequence, l.DateSubmitted, l.LetterTypeId,
			au.FirstName, au.LastName, org.OrganizationId, org.OrganizationTypeId,org.Name AS OrganizationName,
			l.Size, l.PublishToReadingRoom, l.DmdId, l.UnpublishedReasonId, l.UnpublishedReasonOther, ' +
			@attachmmentCount + ' as AttachmentCount, ISNULL(( SELECT    COUNT(LetterId)
                  FROM      dbo.Letter
                  WHERE     PhaseId = l.PhaseId
                ), 0) AS LetterCount, ' +
            @currentPhase + ' AS [CurrentPhase] '
    SET @sqlFrom = ' FROM dbo.Letter l
			INNER JOIN dbo.Author au ON au.LetterId = l.LetterId AND au.Sender = 1
			INNER JOIN dbo.Phase ph ON ph.PhaseId = l.PhaseId 
			LEFT OUTER JOIN dbo.Organization org ON org.OrganizationId = au.OrganizationId '
	-- --------------------------
	-- Construct filters
	-- --------------------------
	IF (@projectId <> -1)
	BEGIN
		-- only display the letters from the phase with PRR activated if project ID is used
		SET @sqlWhere = @sqlWhere + 'AND ph.ReadingRoomActive = 1 AND ph.ProjectId = ' + CAST(@projectId as varchar(20))
	END
	
	IF (@phaseId <> -1)
	BEGIN
		SET @sqlWhere = @sqlWhere + 'AND l.PhaseId = ' + CAST(@phaseId as varchar(20))
	END

	IF (@filterKeyword IS NOT NULL)
	BEGIN
		SET @sqlWhere = @sqlWhere + 'AND CONTAINS((l.Text, l.CodedText),' + @SingleQuote + '"' + REPLACE(REPLACE(@filterKeyword, '"', '""'), '''', '''''') + @Asterisk + '"' + @SingleQuote + ')'
	END
	
	IF (@filterLetterTypeId <> -1)
	BEGIN
		IF (@filterLetterTypeId = 6)
		BEGIN
			-- Not Pending
			SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (1,2,3,4,5) '
		END
		ELSE IF (@filterLetterTypeId = 7)
		BEGIN
			-- All Forms
			SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (3,4,5) '
		END
		ELSE IF (@filterLetterTypeId = 8)
		BEGIN
			-- Master Form or Unique
			SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (2,3) '
		END
		ELSE IF (@filterLetterTypeId = 10)
		BEGIN
			-- Unique or Form
			SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId IN (2,3,4,5) '
		END
		ELSE
		BEGIN
			SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId = ' + CAST(@filterLetterTypeId as varchar(20))
		END
	END

	IF (@filterFirstName IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND au.FirstName = ''' + REPLACE(REPLACE(@filterFirstName, '"', '""'), '''', '''''') + ''''
	END
	
	IF (@filterLastName IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND au.LastName = ''' + REPLACE(REPLACE(@filterLastName, '"', '""'), '''', '''''') + ''''
	END
	
	IF (@filterOrganizationId IS NOT NULL)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND org.Name = ''' + REPLACE(REPLACE(@filterOrganizationId, '"', '""'), '''', '''''') + ''''
	END
	
	--IF (@filterOrganizationId <> -1)
	--BEGIN
	--	SET @sqlWhere = @sqlWhere + ' AND au.OrganizationId = ' + CAST(@filterOrganizationId as varchar(20))
	--END

	IF (@filterPublishToReadingRoom IS NOT NULL)
	BEGIN		
		SET @sqlWhere = @sqlWhere + ' AND l.PublishToReadingRoom = ' + CAST(@filterPublishToReadingRoom as varchar(20))
	END
	
	IF (@filterShowForms IS NOT NULL AND @filterShowForms = 0)
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND l.LetterTypeId <> 4 '
	END
	
	-- -------------------
	-- Apply display delay
	-- -------------------
	DECLARE @delay VARCHAR(20)
	SELECT @delay = Value
	FROM dbo.SystemConfig
	WHERE Name = 'cara.readingroom.displaydelay'
	
	SET @sqlWhere = @sqlWhere + ' AND DATEADD(minute, ' + @delay + ', l.DateEntered) < GETUTCDATE() '
	
	SET @sql = @sql + @sqlFrom + @sqlWhere
	
	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL
	BEGIN
		SET @sortBy = @sortKey
		
		IF (CHARINDEX('FullName', @sortBy) > 0)
		BEGIN
			DECLARE @sortFirstToo VARCHAR(50)
			SET @sortFirstToo = REPLACE(@sortBy, 'FullNameLastFirst', 'FullFirstName')
			SET @sortBy = @sortBy + ', ' + @sortFirstToo
		END
		
		-- translate the sort key into the corresponding column name or clause
		SET @sortBy = REPLACE(@sortBy, 'LetterId', 'l.LetterId')
		SET @sortBy = REPLACE(@sortBy, 'FullNameLastFirst', 'au.LastName')
		SET @sortBy = REPLACE(@sortBy, 'FullFirstName', 'au.FirstName')
		SET @sortBy = REPLACE(@sortBy, 'OrganizationName', 'org.Name')
		SET @sortBy = REPLACE(@sortBy, 'DateSubmitted', 'l.DateSubmitted')
		SET @sortBy = REPLACE(@sortBy, 'LetterTypeName', 'lt.Name')
		SET @sortBy = REPLACE(@sortBy, 'PublishToReadingRoom', 'l.PublishToReadingRoom')
		SET @sortBy = REPLACE(@sortBy, 'Size', 'l.Size')
		SET @sortBy = REPLACE(@sortBy, 'AttachmentCount', @attachmmentCount)
	END

	-- Default Sort By clause
	IF @sortBy IS NULL
		SET @sortBy = @currentPhase + ' desc, l.LetterId asc'

	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1
	BEGIN
		EXECUTE dbo.spsRowPager
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = 'LetterId',
			@SortBy = @sortBy
			
		EXEC (@sqlCount + @sqlFrom + @sqlWhere)
		SET @totalRows = @@ROWCOUNT
		
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
		SET @totalRows = @@ROWCOUNT
	END

	RETURN
GO













GRANT EXECUTE ON  [dbo].[spsLettersReadingRoom] TO [carauser]
GO
