
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spsCommentDetails] 
	-- Add the parameters for the stored procedure here
    @xmlComments XML
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
    SET NOCOUNT ON ;
	
    -- -------------------------------
	-- Construct the select query
	-- -------------------------------
	
    DECLARE @xTable TABLE
	(
		rowNumber INT,
		commentId INT
	)    
	
    INSERT  INTO @xTable (rowNumber, commentId)
            SELECT  list.comment.value('@rowNumber', 'int') AS 'rowNumber',
                    list.comment.value('@commentId', 'int') AS 'commentId'
            FROM    @xmlComments.nodes('commentsList/comment') AS list (comment) 
	
     
  	
    SELECT  lt.PhaseId, cm.CommentId, cm.LetterId, lt.LetterSequence,
            cm.ConcernResponseId, cr.ConcernResponseNumber, cm.SampleStatement,
            NULL AS CommentCodeList, pc.CodeNumber AS CommentCodeNumber,
            pc.CodeName AS CommentCodeText,
            dbo.[fnGetCommentText](cm.CommentId) AS CommentText,
            cm.CommentNumber, pc2.CodeNumber AS ConcernResponseCodeNumber,
            pc2.CodeName AS ConcernResponseCodeName,
            cm.UserId
    FROM    dbo.Comment cm
            INNER JOIN dbo.CommentCode cc ON cc.CommentId = cm.CommentId
            LEFT OUTER JOIN dbo.PhaseCode pc ON pc.PhaseCodeId = cc.PhaseCodeId
            LEFT OUTER JOIN dbo.ConcernResponse cr ON cr.ConcernResponseId = cm.ConcernResponseId
            LEFT OUTER JOIN dbo.PhaseCode pc2 ON cr.PhaseCodeId = pc2.PhaseCodeId
            INNER JOIN dbo.Letter lt ON lt.LetterId = cm.LetterId
            INNER JOIN @xTable xt ON xt.CommentId = cm.CommentId
    ORDER BY xt.rowNumber, cc.sequence           
                
                 
                
END


GO




GRANT EXECUTE ON  [dbo].[spsCommentDetails] TO [carauser]
GO
