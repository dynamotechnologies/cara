
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



/* This stored procedure is for Phases listing page */

CREATE PROCEDURE [dbo].[spsPhases]
(
	@userId INT = NULL,
	@projectId int = NULL,
	@pageNum int = 1,
	@numRows int = -1,
	@sortKey varchar(50) = NULL
)
AS
	-- Note: All paging sps procedures should have SET NOCOUNT ON
	SET NOCOUNT ON

	DECLARE @sql varchar(8000), @sortBy varchar(255), @sqlWhere varchar(8000), @countSql varchar(8000)
	DECLARE @Asterisk varchar(20)= '*', @SingleQuote VARCHAR(1) = ''''

	-- -------------------------------
	-- Construct the select query
	-- -------------------------------
	SET @sqlWhere = ''
	SET @sql = 'SELECT ph.PhaseId, ph.ProjectId, ph.[Name], ph.Locked, ph.Private, ph.PhaseTypeId, pt.Name PhaseTypeName,
		ph.CommentStart, ph.CommentEnd, ISNULL((SELECT 1 FROM dbo.vCurrentPhase vcp WHERE vcp.ProjectId = ph.ProjectId AND vcp.PhaseId = ph.PhaseId), 0) AS [Current],
		ph.AlwaysOpen,
		pd.LetterStatus_1 AS NewLetterCount,
		pd.EarlyActionStatus_2 + pd.EarlyActionStatus_3 + pd.EarlyActionStatus_4 AS EarlyActionLetterCount,
		pd.LetterStatus_2 AS CodingInProgressCount,
		pd.LetterStatus_3 AS CodingCompleteCount,
		pd.LetterType_2 AS UniqueLettersCount,
		pd.LetterType_1 AS DuplicateLettersCount,
		pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 AS FormLettersCount,
		pd.LetterType_2 + pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 + pd.LetterType_1 AS TotalLettersCount 
		 FROM dbo.Phase ph, dbo.PhaseType pt, dbo.PhaseDashboard pd
		 WHERE ph.PhaseTypeId = pt.PhaseTypeId AND ph.PhaseId = pd.PhaseId AND ph.Deleted = 0 '

	
	-- --------------------------
	-- Construct filters
	-- --------------------------
	IF (@projectId IS NOT NULL)	
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND ph.ProjectId = ' + CAST(@projectId as varchar(20))
	END
	
	-- Note: if the phase is private then only team members or system super user/coordinator can see it
	IF (@userId IS NOT NULL)	
	BEGIN
		SET @sqlWhere = @sqlWhere + ' AND ((ph.Private = 1 AND (EXISTS (SELECT 1 FROM dbo.PhaseMemberRole pmr WHERE pmr.PhaseId = ph.PhaseId AND pmr.UserId = ' +  CAST(@userId as varchar(20)) +
		 ') OR EXISTS (SELECT 1 FROM dbo.[User] usr INNER JOIN dbo.UserSystemRole sr on sr.UserId = usr.UserId 
			INNER JOIN dbo.Role r on r.RoleId = sr.RoleId AND r.[Name] IN (''SuperUser'',''Coordinator'') AND usr.UserId = ' + CAST(@userId as varchar(20)) + ')))
			OR ph.Private = 0) '
	END

	SET @sql = @sql + @sqlWhere
	
	-- -------------------------------------------
	-- Build Sort By clause
	-- -------------------------------------------
	IF @sortKey IS NOT NULL
	BEGIN
		SET @sortBy = @sortKey
		
		-- translate the sort key into the corresponding column name or clause
		SET @sortBy = REPLACE(@sortBy, 'PhaseTypeName', 'pt.Name')
		SET @sortBy = REPLACE(@sortBy, 'Name', 'ph.Name')
		SET @sortBy = REPLACE(@sortBy, 'CommentStart', 'ph.CommentStart')
		SET @sortBy = REPLACE(@sortBy, 'CommentEnd', 'ph.CommentEnd')
		SET @sortBy = REPLACE(@sortBy, 'NewLetterCount', 'pd.LetterStatus_1')
		SET @sortBy = REPLACE(@sortBy, 'EarlyActionLetterCount', 'pd.EarlyActionStatus_2 + pd.EarlyActionStatus_3 + pd.EarlyActionStatus_4')
	    SET @sortBy = REPLACE(@sortBy, 'CodingInProgressCount', 'pd.LetterStatus_2')
		SET @sortBy = REPLACE(@sortBy, 'CodingCompleteCount', 'pd.LetterStatus_3')
		SET @sortBy = REPLACE(@sortBy, 'UniqueLettersCount', 'pd.LetterType_2')
		SET @sortBy = REPLACE(@sortBy, 'DuplicateLettersCount', 'pd.LetterType_1')
		SET @sortBy = REPLACE(@sortBy, 'FormLettersCount', 'pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5')
		SET @sortBy = REPLACE(@sortBy, 'TotalLettersCount', 'pd.LetterType_2 + pd.LetterType_3 + pd.LetterType_4 + pd.LetterType_5 + pd.LetterType_1')		
	END

	-- Default Sort By clause (current phase first)
	IF @sortBy IS NULL
		SET @sortBy = '[Current] desc, ph.CommentStart desc'

	/* -------------------------------------------------------------------------------------------*/
	/* If @numRows = -1, then we want to return all records       */
	/* Otherwise, we want to page to @pageNum                       */
	/* -------------------------------------------------------------------------------------------*/
	IF @numRows <> -1
	BEGIN
		EXECUTE dbo.spsRowPager
			@SQL = @sql,
			@RecordsPerPage = @numRows,
			@Page = @pageNum,
			@ID = 'phaseId',
			@SortBy = @sortBy
	END

	/* -------------------------------------------------------------------------------------------*/
	/* return all rows (no paging)                                                   */
	/* -------------------------------------------------------------------------------------------*/

	ELSE
	BEGIN
		SET @sql = @sql + ' ORDER BY ' + @sortBy
		EXEC (@sql)		
	END

	RETURN


GO








GRANT EXECUTE ON  [dbo].[spsPhases] TO [carauser]
GO
