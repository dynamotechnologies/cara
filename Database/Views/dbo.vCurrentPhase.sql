
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Create views section -------------------------------------------------

CREATE VIEW [dbo].[vCurrentPhase]
  WITH SCHEMABINDING
 AS
  select projectid, phaseid
from dbo.phase p1
where getutcdate() >= commentstart
and getutcdate() <= commentend
union
select projectid, phaseid
from dbo.phase p2
where getutcdate() < (select min(commentstart) from dbo.phase where projectid = p2.projectid)
and commentstart = (select min(commentstart) from dbo.phase where projectid = p2.projectid)
union
select projectid, phaseid
from dbo.phase p3
where getutcdate() > (select max(commentend) from dbo.phase where projectid = p3.projectid)
and commentend = (select max(commentend) from dbo.phase where projectid = p3.projectid)
UNION
-- get the previous phase if the current date does not fall within any phase period in the project
select projectid, phaseid
from dbo.phase p4
WHERE NOT (EXISTS (SELECT 1 FROM dbo.Phase ph
		WHERE ProjectId = p4.ProjectId
		AND getutcdate() >= ph.CommentStart AND getutcdate() <= ph.CommentEnd))
	AND p4.phaseId = (SELECT TOP 1 phaseId FROM dbo.Phase ph WHERE ph.ProjectId = p4.ProjectId AND
	GETUTCDATE() > ph.CommentEnd ORDER BY ph.CommentEnd DESC)
GO
