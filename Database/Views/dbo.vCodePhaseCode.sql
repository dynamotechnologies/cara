
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbo.vCodePhaseCode
AS
SELECT     NULL AS PhaseCodeId, NULL AS PhaseId, c.CodeId, c.ParentCodeId, c.CodeCategoryId, c.Name AS CodeName, c.CodeNumber, 'True' AS CanRemove, NULL AS UserId
FROM         code c
UNION
SELECT     pc.PhaseCodeId, pc.PhaseId, pc.CodeId, pc.ParentCodeId, pc.CodeCategoryId, pc.CodeName, pc.CodeNumber, pc.CanRemove, UserId
FROM         phasecode pc
GO

