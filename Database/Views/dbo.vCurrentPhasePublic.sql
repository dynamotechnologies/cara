
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vCurrentPhasePublic]
  WITH SCHEMABINDING
 AS
  select projectid, phaseid
from dbo.phase p1
where getutcdate() >= commentstart
and getutcdate() <= commentend
AND [Private] = 0
union
select projectid, phaseid
from dbo.phase p2
where getutcdate() < (select min(commentstart) from dbo.phase where projectid = p2.projectid AND [Private] = 0)
and commentstart = (select min(commentstart) from dbo.phase where projectid = p2.projectid AND [Private] = 0)
union
select projectid, phaseid
from dbo.phase p3
where getutcdate() > (select max(commentend) from dbo.phase where projectid = p3.projectid AND [Private] = 0)
and commentend = (select max(commentend) from dbo.phase where projectid = p3.projectid AND [Private] = 0)
union
-- get the previous phase if the current date does not fall within any phase period in the project
select projectid, phaseid
from dbo.phase p4
WHERE p4.[Private] = 0 AND NOT (EXISTS (SELECT 1 FROM dbo.Phase ph
		WHERE ProjectId = p4.ProjectId AND ph.[Private] = 0
		AND getutcdate() >= ph.CommentStart AND getutcdate() <= ph.CommentEnd))
	AND p4.phaseId = (SELECT TOP 1 phaseId FROM dbo.Phase ph WHERE ph.[Private] = 0 AND ph.ProjectId = p4.ProjectId AND
	GETUTCDATE() > ph.CommentEnd ORDER BY ph.CommentEnd DESC)
GO
