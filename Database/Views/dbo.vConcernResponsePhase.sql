
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vConcernResponsePhase]
 
AS
SELECT  l.PhaseId, cr.ConcernResponseId, cr.ConcernResponseNumber,
        cr.ConcernResponseStatusId, crs.Name AS ConcernResponseStatusName,
        cr.ConcernText, cr.Sequence, cr.ResponseText, cr.ResponseCreated,
        cr.ResponseCreatedBy, cr.ConcernCreated, cr.ConcernCreatedBy,
        cr.LastUpdated, cr.LastUpdatedBy, pc.CodeName, pc.CodeNumber,
        COUNT_BIG(*) AS CommentCount
FROM    dbo.Letter l
        INNER JOIN dbo.Comment c ON l.LetterId = c.LetterId
        LEFT OUTER JOIN dbo.ConcernResponse cr ON c.ConcernResponseId = cr.ConcernResponseId
        LEFT OUTER JOIN dbo.PhaseCode pc ON cr.PhaseCodeId = pc.PhaseCodeId
        LEFT OUTER JOIN dbo.ConcernResponseStatus crs ON cr.ConcernResponseStatusId = crs.ConcernResponseStatusId
WHERE   l.letterid = c.letterid
        AND c.ConcernResponseId = cr.ConcernResponseId
        AND cr.ConcernResponseStatusId = crs.ConcernResponseStatusId
GROUP BY l.PhaseId, cr.ConcernResponseId, cr.ConcernResponseNumber,
        cr.ConcernResponseStatusId, crs.Name, cr.ConcernText, cr.Sequence,
        cr.ResponseText, cr.ResponseCreated, cr.ResponseCreatedBy,
        cr.ConcernCreated, cr.ConcernCreatedBy, cr.LastUpdated,
        cr.LastUpdatedBy, pc.CodeName, pc.CodeNumber
GO
