SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDmdSendQueue]
  WITH SCHEMABINDING
 AS

	SELECT ld.DocumentId, ld.LetterId
	FROM dbo.Document d
		INNER JOIN dbo.LetterDocument ld ON d.DocumentId = ld.DocumentId
	WHERE d.DmdId IS NULL
  
GO
