SET IDENTITY_INSERT [dbo].[PhaseType] ON
INSERT INTO [dbo].[PhaseType] ([PhaseTypeId], [Name], [Active]) VALUES (1, 'Informal', 0)
INSERT INTO [dbo].[PhaseType] ([PhaseTypeId], [Name], [Active]) VALUES (2, 'Formal', 0)
INSERT INTO [dbo].[PhaseType] ([PhaseTypeId], [Name], [Active]) VALUES (3, 'Objection', 1)
INSERT INTO [dbo].[PhaseType] ([PhaseTypeId], [Name], [Active]) VALUES (4, 'Scoping', 1)
INSERT INTO [dbo].[PhaseType] ([PhaseTypeId], [Name], [Active]) VALUES (5, 'Notice of Availability', 1)
INSERT INTO [dbo].[PhaseType] ([PhaseTypeId], [Name], [Active]) VALUES (6, 'Other', 1)
SET IDENTITY_INSERT [dbo].[PhaseType] OFF
