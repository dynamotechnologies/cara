SET IDENTITY_INSERT [dbo].[ArtifactType] ON
INSERT INTO [dbo].[ArtifactType] ([ArtifactTypeId], [Name], [KeyFlag]) VALUES (1, 'Response to Comment Report', 1)
INSERT INTO [dbo].[ArtifactType] ([ArtifactTypeId], [Name], [KeyFlag]) VALUES (2, 'Mailing List Report', 0)
INSERT INTO [dbo].[ArtifactType] ([ArtifactTypeId], [Name], [KeyFlag]) VALUES (3, 'Team Member Report', 0)
INSERT INTO [dbo].[ArtifactType] ([ArtifactTypeId], [Name], [KeyFlag]) VALUES (4, 'Coding Structure Report', 1)
INSERT INTO [dbo].[ArtifactType] ([ArtifactTypeId], [Name], [KeyFlag]) VALUES (5, 'Demographic Report', 0)
SET IDENTITY_INSERT [dbo].[ArtifactType] OFF
