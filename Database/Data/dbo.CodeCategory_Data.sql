SET IDENTITY_INSERT [dbo].[CodeCategory] ON
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (9, 'Objection', '6', 9)
SET IDENTITY_INSERT [dbo].[CodeCategory] OFF
SET IDENTITY_INSERT [dbo].[CodeCategory] ON
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (1, 'Issue/Action', '1', 2)
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (2, 'Resource/Rationale', '2', 3)
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (3, 'Location', '3', 4)
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (4, 'Document', '4', 5)
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (7, 'Early Attention', '5', 1)
INSERT INTO [dbo].[CodeCategory] ([CodeCategoryId], [Name], [Prefix], [SortOrder]) VALUES (8, 'Other', '8', 6)
SET IDENTITY_INSERT [dbo].[CodeCategory] OFF
