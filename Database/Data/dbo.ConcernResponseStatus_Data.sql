SET IDENTITY_INSERT [dbo].[ConcernResponseStatus] ON
INSERT INTO [dbo].[ConcernResponseStatus] ([Name], [ConcernResponseStatusId], [SortOrder]) VALUES ('Ready for Review', 5, 4)
INSERT INTO [dbo].[ConcernResponseStatus] ([Name], [ConcernResponseStatusId], [SortOrder]) VALUES ('Response Complete', 4, 5)
INSERT INTO [dbo].[ConcernResponseStatus] ([Name], [ConcernResponseStatusId], [SortOrder]) VALUES ('Response In Progress', 3, 3)
SET IDENTITY_INSERT [dbo].[ConcernResponseStatus] OFF
SET IDENTITY_INSERT [dbo].[ConcernResponseStatus] ON
INSERT INTO [dbo].[ConcernResponseStatus] ([Name], [ConcernResponseStatusId], [SortOrder]) VALUES ('Concern In Progress', 1, 1)
INSERT INTO [dbo].[ConcernResponseStatus] ([Name], [ConcernResponseStatusId], [SortOrder]) VALUES ('Ready for Response', 2, 2)
SET IDENTITY_INSERT [dbo].[ConcernResponseStatus] OFF
