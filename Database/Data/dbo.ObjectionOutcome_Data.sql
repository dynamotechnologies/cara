SET IDENTITY_INSERT [dbo].[ObjectionOutcome] ON
INSERT INTO [dbo].[ObjectionOutcome] ([ObjectionOutcomeId], [Name]) VALUES (1, 'Resolved during resolution meeting                ')
INSERT INTO [dbo].[ObjectionOutcome] ([ObjectionOutcomeId], [Name]) VALUES (2, 'Responsible official provided further instructions')
INSERT INTO [dbo].[ObjectionOutcome] ([ObjectionOutcomeId], [Name]) VALUES (3, 'Responsible official may sign decision            ')
SET IDENTITY_INSERT [dbo].[ObjectionOutcome] OFF
