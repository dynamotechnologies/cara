SET IDENTITY_INSERT [dbo].[DeliveryType] ON
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (2, 'Carrier: USPS, UPS, FedEx, etc.')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (3, 'Email')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (4, 'FAX')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (5, 'Meeting Comment Card')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (6, 'Personally delivered, oral, including telephone')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (7, 'Transcript or notes of oral meeting comments')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (9, 'Other or Unknown')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (10, 'Personally delivered, written')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (11, 'CARA Web-portal')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (12, 'Regulations.gov')
INSERT INTO [dbo].[DeliveryType] ([DeliveryTypeId], [Name]) VALUES (13, 'Social media, blog submissions, (not email or web-portal)')
SET IDENTITY_INSERT [dbo].[DeliveryType] OFF
