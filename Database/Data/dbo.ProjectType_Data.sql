SET IDENTITY_INSERT [dbo].[ProjectType] ON
INSERT INTO [dbo].[ProjectType] ([ProjectTypeId], [Name]) VALUES (1, 'PALS')
INSERT INTO [dbo].[ProjectType] ([ProjectTypeId], [Name]) VALUES (2, 'Non-PALS')
INSERT INTO [dbo].[ProjectType] ([ProjectTypeId], [Name]) VALUES (3, 'ORMS')
SET IDENTITY_INSERT [dbo].[ProjectType] OFF
