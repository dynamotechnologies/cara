SET IDENTITY_INSERT [dbo].[AnalysisType] ON
INSERT INTO [dbo].[AnalysisType] ([AnalysisTypeId], [Name]) VALUES (5, 'N/A')
SET IDENTITY_INSERT [dbo].[AnalysisType] OFF
SET IDENTITY_INSERT [dbo].[AnalysisType] ON
INSERT INTO [dbo].[AnalysisType] ([AnalysisTypeId], [Name]) VALUES (1, 'EIS')
INSERT INTO [dbo].[AnalysisType] ([AnalysisTypeId], [Name]) VALUES (2, 'EA')
INSERT INTO [dbo].[AnalysisType] ([AnalysisTypeId], [Name]) VALUES (3, 'CE')
INSERT INTO [dbo].[AnalysisType] ([AnalysisTypeId], [Name]) VALUES (4, 'RULE')
SET IDENTITY_INSERT [dbo].[AnalysisType] OFF
