SET IDENTITY_INSERT [dbo].[OfficialRepresentativeType] ON
INSERT INTO [dbo].[OfficialRepresentativeType] ([OfficialRepresentativeTypeId], [Name]) VALUES (1, 'Official Representative')
INSERT INTO [dbo].[OfficialRepresentativeType] ([OfficialRepresentativeTypeId], [Name]) VALUES (2, 'Member')
INSERT INTO [dbo].[OfficialRepresentativeType] ([OfficialRepresentativeTypeId], [Name]) VALUES (3, 'Elected Official')
SET IDENTITY_INSERT [dbo].[OfficialRepresentativeType] OFF
