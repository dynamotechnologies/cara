INSERT INTO [dbo].[LetterTermConfirmationStatus] ([ConfirmationStatusId], [Name], [Sequence]) VALUES (1, 'Not Required', 3)
INSERT INTO [dbo].[LetterTermConfirmationStatus] ([ConfirmationStatusId], [Name], [Sequence]) VALUES (2, 'Needs Attention', 1)
INSERT INTO [dbo].[LetterTermConfirmationStatus] ([ConfirmationStatusId], [Name], [Sequence]) VALUES (3, 'Completed', 2)
