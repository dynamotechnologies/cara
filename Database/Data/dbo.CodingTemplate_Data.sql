SET IDENTITY_INSERT [dbo].[CodingTemplate] ON
INSERT INTO [dbo].[CodingTemplate] ([CodingTemplateId], [Name]) VALUES (5, 'Objection')
SET IDENTITY_INSERT [dbo].[CodingTemplate] OFF
SET IDENTITY_INSERT [dbo].[CodingTemplate] ON
INSERT INTO [dbo].[CodingTemplate] ([CodingTemplateId], [Name]) VALUES (1, 'Basic')
INSERT INTO [dbo].[CodingTemplate] ([CodingTemplateId], [Name]) VALUES (2, 'Moderate')
INSERT INTO [dbo].[CodingTemplate] ([CodingTemplateId], [Name]) VALUES (3, 'Detailed')
SET IDENTITY_INSERT [dbo].[CodingTemplate] OFF
