SET IDENTITY_INSERT [dbo].[NoResponseReason] ON
INSERT INTO [dbo].[NoResponseReason] ([NoResponseReasonId], [Name]) VALUES (1, 'Beyond the Scope of the Proposal')
INSERT INTO [dbo].[NoResponseReason] ([NoResponseReasonId], [Name]) VALUES (2, 'Unrelated to the Decision Being Made')
INSERT INTO [dbo].[NoResponseReason] ([NoResponseReasonId], [Name]) VALUES (3, 'Already Decided by Law, Regulation, or Policy')
INSERT INTO [dbo].[NoResponseReason] ([NoResponseReasonId], [Name]) VALUES (4, 'Conjectural in Nature or Not Supported by Scientific Evidence')
INSERT INTO [dbo].[NoResponseReason] ([NoResponseReasonId], [Name]) VALUES (5, 'General in Nature or Position Statements')
INSERT INTO [dbo].[NoResponseReason] ([NoResponseReasonId], [Name]) VALUES (6, 'Other (If Other, please specify)')
SET IDENTITY_INSERT [dbo].[NoResponseReason] OFF
