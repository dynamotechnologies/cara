SET IDENTITY_INSERT [dbo].[Purpose] ON
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (1, 'Regulations, Directives, Orders', 'RO')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (2, 'Land management planning', 'PN')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (3, 'Recreation management', 'RW')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (4, 'Heritage resource management', 'HR')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (5, 'Special area management', 'RU')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (6, 'Wildlife, Fish, Rare plants', 'WF')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (7, 'Grazing management', 'RG')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (8, 'Forest products', 'TM')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (9, 'Vegetation management (other than forest products)', 'VM')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (10, 'Fuels management', 'HF')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (11, 'Watershed management', 'WM')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (12, 'Minerals and Geology', 'MG')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (13, 'Land ownership management', 'LM')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (14, 'Land acquisition', 'LW')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (15, 'Special use management', 'SU')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (16, 'Facility management', 'FC')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (17, 'Road management', 'RD')
INSERT INTO [dbo].[Purpose] ([PurposeId], [Name], [Code]) VALUES (18, 'Research', 'FR')
SET IDENTITY_INSERT [dbo].[Purpose] OFF
