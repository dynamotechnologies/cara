SET IDENTITY_INSERT [dbo].[CommonInterestClass] ON
INSERT INTO [dbo].[CommonInterestClass] ([CommonInterestClassId], [Name]) VALUES (1, 'In Favor')
INSERT INTO [dbo].[CommonInterestClass] ([CommonInterestClassId], [Name]) VALUES (2, 'In Favor, somewhat or with qualifications')
INSERT INTO [dbo].[CommonInterestClass] ([CommonInterestClassId], [Name]) VALUES (3, 'Neutral or not stated')
INSERT INTO [dbo].[CommonInterestClass] ([CommonInterestClassId], [Name]) VALUES (4, 'Opposed, somewhat or with qualifications')
INSERT INTO [dbo].[CommonInterestClass] ([CommonInterestClassId], [Name]) VALUES (5, 'Opposed')
SET IDENTITY_INSERT [dbo].[CommonInterestClass] OFF
