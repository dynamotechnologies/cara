SET IDENTITY_INSERT [dbo].[LetterStatus] ON
INSERT INTO [dbo].[LetterStatus] ([LetterStatusId], [Name]) VALUES (1, 'New')
INSERT INTO [dbo].[LetterStatus] ([LetterStatusId], [Name]) VALUES (2, 'In Progress')
INSERT INTO [dbo].[LetterStatus] ([LetterStatusId], [Name]) VALUES (3, 'Complete')
INSERT INTO [dbo].[LetterStatus] ([LetterStatusId], [Name]) VALUES (4, 'Not Needed')
SET IDENTITY_INSERT [dbo].[LetterStatus] OFF
