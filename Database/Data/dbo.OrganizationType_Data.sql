SET IDENTITY_INSERT [dbo].[OrganizationType] ON
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (1, 'County Government Agency /Elected Official', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (2, 'Federal Agency/Elected Official', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (3, 'Municipal/City Government Agency /Elected Official', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (4, 'International Government Agency or Official ', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (5, 'State Government Agency /Elected Official', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (6, 'American Indian Govt. Agency /Elected Official', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (7, 'Other', 21)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (8, 'Agriculture Industry or Associations (Farm Bureau)', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (9, 'Grazing, Domestic Livestock (incl. permittees)', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (10, 'Logging, Timber, Wood Products', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (11, 'Mining (locatable)', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (12, 'Oil, Natural Gas, Coal, Pipeline (leasable)', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (13, 'Recreational (incl. permitees)', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (14, 'Utility Group (water, electrical, gas)', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (15, 'Other', 22)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (16, 'Academic/Professional Society', 23)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (17, 'Civic or Place-Based Groups (Kiwanis, Elks, Community Council, Planning Cooperative)', 23)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (18, 'Environmental Conservation/ Preservation', 23)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (19, 'Multiple Use or Land Rights Organization', 23)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (20, 'Other', 23)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (21, 'Government', NULL)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (22, 'Business/Industry', NULL)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [Name], [ParentOrganizationTypeId]) VALUES (23, 'Not-For-Profit', NULL)
SET IDENTITY_INSERT [dbo].[OrganizationType] OFF
