SET IDENTITY_INSERT [dbo].[ObjectionReviewStatus] ON
INSERT INTO [dbo].[ObjectionReviewStatus] ([ObjectionReviewStatusId], [Name]) VALUES (1, 'New')
INSERT INTO [dbo].[ObjectionReviewStatus] ([ObjectionReviewStatusId], [Name]) VALUES (2, 'Under Review')
INSERT INTO [dbo].[ObjectionReviewStatus] ([ObjectionReviewStatusId], [Name]) VALUES (3, 'Set Aside From Review')
INSERT INTO [dbo].[ObjectionReviewStatus] ([ObjectionReviewStatusId], [Name]) VALUES (4, 'Closed')
SET IDENTITY_INSERT [dbo].[ObjectionReviewStatus] OFF
