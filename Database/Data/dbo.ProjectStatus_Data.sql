SET IDENTITY_INSERT [dbo].[ProjectStatus] ON
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (0, 'Unknown')
SET IDENTITY_INSERT [dbo].[ProjectStatus] OFF
SET IDENTITY_INSERT [dbo].[ProjectStatus] ON
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (1, 'Developing Proposal')
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (2, 'In Progress')
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (3, 'On Hold')
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (4, 'Cancelled')
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (5, 'Completed')
INSERT INTO [dbo].[ProjectStatus] ([ProjectStatusId], [Name]) VALUES (6, 'N/A')
SET IDENTITY_INSERT [dbo].[ProjectStatus] OFF
