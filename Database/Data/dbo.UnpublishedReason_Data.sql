SET IDENTITY_INSERT [dbo].[UnpublishedReason] ON
INSERT INTO [dbo].[UnpublishedReason] ([UnpublishedReasonId], [Name], [TermListId]) VALUES (1, 'Sensitive Tribal Information', 6)
INSERT INTO [dbo].[UnpublishedReason] ([UnpublishedReasonId], [Name], [TermListId]) VALUES (2, 'Sensitive Resource Information', 7)
INSERT INTO [dbo].[UnpublishedReason] ([UnpublishedReasonId], [Name], [TermListId]) VALUES (3, 'Proprietary Information', NULL)
INSERT INTO [dbo].[UnpublishedReason] ([UnpublishedReasonId], [Name], [TermListId]) VALUES (4, 'Offensive Content', 5)
INSERT INTO [dbo].[UnpublishedReason] ([UnpublishedReasonId], [Name], [TermListId]) VALUES (5, 'Other', NULL)
SET IDENTITY_INSERT [dbo].[UnpublishedReason] OFF
