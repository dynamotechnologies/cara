SET IDENTITY_INSERT [dbo].[Role] ON
INSERT INTO [dbo].[Role] ([RoleId], [Name], [ProjectRole]) VALUES (4, 'Team Member', 1)
INSERT INTO [dbo].[Role] ([RoleId], [Name], [ProjectRole]) VALUES (5, 'Reader', 0)
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[Role] ON
INSERT INTO [dbo].[Role] ([RoleId], [Name], [ProjectRole]) VALUES (1, 'System Admin', 0)
INSERT INTO [dbo].[Role] ([RoleId], [Name], [ProjectRole]) VALUES (2, 'Planning Contributor', 0)
INSERT INTO [dbo].[Role] ([RoleId], [Name], [ProjectRole]) VALUES (3, 'Comment Period Manager (deprecated)', 1)
SET IDENTITY_INSERT [dbo].[Role] OFF
