SET IDENTITY_INSERT [dbo].[CommentRule] ON
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (1, 'Not Subject to Regulations')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (2, '215')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (3, '218')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (4, '219 (2005)')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (5, '219 (Converting)')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (6, '219 (1982)')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (7, 'EA to EIS')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (8, 'EA to DM')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (9, 'EIS to EA')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (10, 'EIS to DM')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (11, 'DM to EIS')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (12, 'DM to EA')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (13, '219 (2012)')
SET IDENTITY_INSERT [dbo].[CommentRule] OFF
SET IDENTITY_INSERT [dbo].[CommentRule] ON
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (14, '218 (2013) HFRA')
INSERT INTO [dbo].[CommentRule] ([CommentRuleId], [Name]) VALUES (15, '218 (2013) Non-HFRA')
SET IDENTITY_INSERT [dbo].[CommentRule] OFF
