SET IDENTITY_INSERT [dbo].[LetterType] ON
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (1, 'Duplicate')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (2, 'Unique')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (3, 'Master Form')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (4, 'Form')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (5, 'Form Plus')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (6, 'Not Pending')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (7, 'All Forms')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (8, 'Master Form or Unique')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (9, 'Pending')
INSERT INTO [dbo].[LetterType] ([LetterTypeId], [Name]) VALUES (10, 'Unique or Forms')
SET IDENTITY_INSERT [dbo].[LetterType] OFF
