﻿using System;
using System.Linq;
using Aquilent.Cara.Reporting;
using Telerik.Reporting.Processing;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Report.Personal;
using System.Configuration;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using System.IO;
using Amazon.S3;
using Amazon.S3.IO;
using System.Text;

namespace Cara.ReportsConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting AWS SES testing...");
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Starting the CARA Custom Reports Console App...");
        //    Console.WriteLine("");

        //    var customReportId = Int32.Parse(args[0]);

        //    using (var crqManager = ManagerFactory.CreateInstance<CustomReportQueueManager>())
        //    {
        //        var customReport = crqManager.Get(customReportId);

        //        if (customReport == null) return;

        //        Console.WriteLine("Report Definition...");
        //        Console.WriteLine(customReport.ReportDefinition);
        //        Console.WriteLine("");

        //        string provider = ConfigurationManager.ConnectionStrings["CaraCustomReportConnection"].ProviderName;
        //        string constring = ConfigurationManager.ConnectionStrings["CaraCustomReportConnection"].ConnectionString;

        //        int userId = customReport.UserId; 

        //        List<CustomReportData> retVal = null;

        //        using (CustomReportManager reportManager = new Aquilent.Cara.Domain.Report.CustomReportManager())
        //        {
        //            reportManager.Open(provider, constring);
        //            int total;
        //            retVal = reportManager.RunReport(customReport.ReportDefinition, out total, userId, 0, 0, 0, true);
        //            reportManager.Close();
        //        }

        //        StreamWriter writer = null;
        //        String filename = "customreports_kt_" + Guid.NewGuid() + ".csv";

        //        using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
        //        {
        //            S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, "caradev-john-customreports");
        //            var csvFile = rootDirectory.GetFile(filename);

        //            using (writer = new StreamWriter(csvFile.OpenWrite(), Encoding.UTF8))
        //            {
        //                foreach (CustomReportData result in retVal)
        //                {
        //                    writer.WriteLine(result.Csv);
        //                }
        //            }
        //        }

        //        writer.Close();

        //        //FileStream fs = new FileStream(@"C:\Users\ktran\ReportExports\CustomReport_" + Guid.NewGuid() + ".csv", FileMode.CreateNew);

        //        //using (StreamWriter writer = new StreamWriter(fs))
        //        //{
        //        //    foreach (CustomReportData result in retVal)
        //        //    {
        //        //        writer.WriteLine(result.Csv); 
        //        //    }
        //        //}



        //        Console.WriteLine("CARA Custom Report Generation complete."); 
        //    }
        //}

        /*Parameters/arguments in this order: phaseId, id, exportType, option, and storage directory.*/

        // deviation from change from git to dev
        // ****This line was in original, but replaced by static void Main..., don't know what previous person had planned
        // or what is going on but I'm guessing that was a typo. Included original private void ProcessSta... for completeness
        //private void ProcessStandardReport(string[] args)
        //***********************

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Starting the CARA Standard Reports Console App...");
        //    Console.WriteLine("");

        //    /*Process arguments here...*/

        //    var phaseId = Int32.Parse(args[0]);
        //    var id = Int32.Parse(args[1]);
        //    var exportType = args.Length < 3 ? "PDF" : args[2];
        //    var option = args.Length < 4 ? "1" : args[3];
        //    var storagePath = args.Length < 5 ? @"C:\Temp\ReportExports\" : args[4];

        //    var document = ReportFactory.GetReport(id, phaseId, option);
        //    RenderingResult result = ReportUtilities.ExportReport(document, exportType);

        //    System.IO.Directory.CreateDirectory(storagePath);

        //    string fileName = result.DocumentName + "_" + System.Guid.NewGuid() + "." + result.Extension;
        //    string filePath = System.IO.Path.Combine(storagePath, fileName);

        //    using (System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
        //    {
        //        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        //    }

        //    Console.WriteLine("Report generation complete.");
        //    Console.WriteLine("File located at: " + filePath);
        //}
    }
}
