﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Aquilent.Cara.Configuration;

namespace Aquilent.Cara.Configuration.LookupManager
{
	public class LookupsCollection : ConfigurationElementCollection
	{
		public LookupsCollection()
		{
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}

		protected override ConfigurationElement CreateNewElement(string elementName)
		{
			return base.CreateNewElement(elementName);
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new LookupClassElement();
		}

		protected override Object GetElementKey(ConfigurationElement element)
		{
			return ((LookupClassElement) element).ClassName;
		}

		public FullyQualifiedClassElement this[int index]
		{
			get
			{
				return (LookupClassElement) BaseGet(index);
			}
			set
			{
				if (BaseGet(index) != null)
				{
					BaseRemoveAt(index);
				}
				BaseAdd(index, value);
			}
		}

		public int IndexOf(LookupClassElement fqce)
		{
			return BaseIndexOf(fqce);
		}

		public void Add(LookupClassElement fqce)
		{
			BaseAdd(fqce);
			// Add custom code here.
		}

		public void Remove(LookupClassElement fqce)
		{
			if (BaseIndexOf(fqce) >= 0)
			{
				BaseRemove(fqce.ClassName);
			}
		}

		public void RemoveAt(int index)
		{
			BaseRemoveAt(index);
		}

		public void Remove(string classname)
		{
			BaseRemove(classname);
		}

		public void Clear()
		{
			BaseClear();
			// Add custom code here.
		}
	}
}
