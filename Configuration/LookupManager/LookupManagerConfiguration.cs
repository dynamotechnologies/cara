﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Aquilent.Cara.Configuration.LookupManager
{
	public class LookupManagerConfiguration : ConfigurationSection
	{
		[ConfigurationProperty("lookups", IsDefaultCollection = false)]
		public LookupsCollection Lookups
		{
			get
			{
				return (LookupsCollection) base["lookups"];
			}
		}

	}
}
