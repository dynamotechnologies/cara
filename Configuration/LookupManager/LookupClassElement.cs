﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Runtime.Remoting;

namespace Aquilent.Cara.Configuration.LookupManager
{
	/// <summary>
	/// This represents a configuration element that contains two parts of a fully qualified class name:
	///   class name and assembly name.
	/// </summary>
	public class LookupClassElement : FullyQualifiedClassElement
	{
		/// <summary>
		/// The name of the class.
		/// 
		/// Example:
		///   Name.Space.Class.Name
		/// </summary>
		[ConfigurationProperty("lookupLoaderType", IsRequired=false)]
		public string LookupLoaderType
		{
			get
			{
				return (string) this["lookupLoaderType"];
			}
			set
			{
				this["lookupLoaderType"] = value;
			}
		}

		#region IObjectHandle Members

		/// <summary>
		/// Returns an instance of the type represented by this configuration element.
		/// </summary>
		/// <returns>The unwrapped object.</returns>
		public object UnwrapLookupLoaderType()
		{
			int indexOf = LookupLoaderType.IndexOf(",");
			string assemblyName = LookupLoaderType.Substring(indexOf + 1).Trim();
			string className = LookupLoaderType.Substring(0, indexOf).Trim();

			return Activator.CreateInstance(assemblyName, className).Unwrap();
		}

		#endregion

		public override string ToString()
		{
			return string.Format("{0}, LookupType: {1}", base.ToString(), LookupLoaderType);
		}
	}
}
