﻿using System.Xml;
using System.Xml.Serialization;
using System;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Configuration.FormDetection
{
    [XmlType]
    public class ConfidenceGrade
    {
        #region Member Variables
        private string _Name = "";
        private double _minConfidence = 0;
        private double _maxConfidence = 0;
        private string _autoTrust = "False";
        private bool? _isValid;
        #endregion Member Variables

        #region Properties

        [XmlAttribute(AttributeName = "gradeName")]
        public string GradeName
        {
            get
            {
                IsValid();
                return _Name;
            }

            set
            {
                _isValid = new bool?();
                _Name = value;
            }
        }

        [XmlAttribute(AttributeName = "autoTrust", DataType = "string")]
        public string AutoTrust
        {
            get
            {
                IsValid();
                return _autoTrust;
            }

            set
            {
                _isValid = new bool?();
                _autoTrust = value;
            }
        }

        /// <summary>
        /// A value greater than or equal 0.  Setting a value &lt;&eq;0 will deafult MinConfidence to 0
        /// </summary>
        [XmlAttribute(AttributeName = "minConfidence", DataType = "double")]
        public double MinConfidence
        {
            get
            {
                IsValid();
                return _minConfidence;
            }

            set
            {
                _isValid = new bool?();
                _minConfidence = Math.Max(0, value);
            }
        }

        /// <summary>
        /// A value greater than or equal 0.  Setting a value &lt;&eq;0 will deafult MinConfidence to 0
        /// </summary>
        [XmlAttribute(AttributeName = "maxConfidence", DataType = "double")]
        public double MaxConfidence
        {
            get
            {
                IsValid();
                return _maxConfidence;
            }

            set
            {
                _isValid = new bool?();
                _maxConfidence = Math.Max(0, value);
            }
        }
        #endregion

        #region Methods

        private bool IsValid()
        {
            if (!_isValid.HasValue)
            {
                _isValid = true;

                // Validate min/max fragments
                if (_minConfidence > _maxConfidence)
                {
                    _isValid = false;
                }

                if (_isValid == false)
                {
                    throw new System.Configuration.ConfigurationErrorsException("The Confidence Grade is not valid");
                }
            }

            return _isValid.Value;
        }
        #endregion Methods
    }
}
