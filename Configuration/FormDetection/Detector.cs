﻿using System.Xml.Serialization;

namespace Aquilent.Cara.Configuration.FormDetection
{
	[XmlType]
	public class Detector
	{
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		[XmlElement(ElementName = "threshold", IsNullable = false)]
		public Threshold[] Thresholds { get; set; }
	}
}
