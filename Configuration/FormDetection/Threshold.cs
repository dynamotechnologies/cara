﻿using System.Xml.Serialization;

namespace Aquilent.Cara.Configuration.FormDetection
{
	[XmlType]
	public class Threshold
	{
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		[XmlAttribute(AttributeName = "value", DataType = "double")]
		public double Value { get; set; }
	}
}
