﻿using System.Xml.Serialization;

namespace Aquilent.Cara.Configuration.FormDetection
{
    [XmlType]
    public class WordRadius
    {
        [XmlAttribute(AttributeName = "value", DataType = "int")]
        public int Value { get; set; }
    }
}
