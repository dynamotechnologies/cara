﻿using System.Configuration;
using System.Xml.Serialization;

namespace Aquilent.Cara.Configuration.FormDetection
{
	[XmlRoot(ElementName = "formDetectionConfiguration", Namespace = "")]
	public class FormDetectionConfiguration
    {
        [XmlElement(ElementName = "wordRadius")]
        public WordRadius WordRadius { get; set; }

        [XmlElement(ElementName = "letterSize")]
        public LetterSize[] LetterSizes { get; set; }

        [XmlElement(ElementName = "confidenceGrade")]
        public ConfidenceGrade[] ConfidenceGrades { get; set; }
	}
}
