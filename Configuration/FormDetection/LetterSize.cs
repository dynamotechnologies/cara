﻿using System.Xml;
using System.Xml.Serialization;
using System;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Configuration.FormDetection
{
	[XmlType]
	public class LetterSize
	{
		#region Member Variables
		private int _minFragments = 0;
		private int _maxFragments = int.MaxValue;
		private string _ignoreTop = "0";
		private string _ignoreBottom = "0";
		private bool? _isValid;
		#endregion Member Variables

		#region Properties
		/// <summary>
		/// A value greater than or equal 0.  Setting a value &lt;&eq;0 will deafult MinFragrments to 0
		/// </summary>
		[XmlAttribute(AttributeName = "minFragments", DataType = "int")]
		public int MinFragments
		{
			get
			{
				IsValid();
				return _minFragments;
			}

			set
			{
				_isValid = new bool?();
				_minFragments = Math.Max(0, value);
			}
		}

		/// <summary>
		/// A value greater than 0.  Setting a value &lt;&eq;0 will deafult MaxFragments to int.MaxValue
		/// </summary>
		[XmlAttribute(AttributeName = "maxFragments", DataType = "int")]
		public int MaxFragments
		{
			get
			{
				IsValid();
				return _maxFragments;
			}

			set
			{
				_isValid = new bool?();
				_maxFragments = value <= 0 ? int.MaxValue : value;
			}
		}

		[XmlAttribute(AttributeName = "ignoreTop")]
		public string IgnoreTop
		{
			get
			{
				IsValid();
				return _ignoreTop;
			}

			set
			{
				_isValid = new bool?();
				_ignoreTop = value;
			}
		}

		[XmlAttribute(AttributeName = "ignoreBottom")]
		public string IgnoreBottom
		{
			get
			{
				IsValid();
				return _ignoreBottom;
			}

			set
			{
				_isValid = new bool?();
				_ignoreBottom = value;
			}
		}

		[XmlAttribute(AttributeName = "minConfidence")]
        public double MinConfidence { get; set; }
        
		[XmlElement(ElementName = "detector", IsNullable = false)]
		public Detector[] Detectors { get; set; }
		#endregion Properties

		#region Methods
		public int IgnoreTopFramentCount(int totalFragments)
		{
			return GetIgnoreFragmentCount(IgnoreTop, totalFragments);
		}

		public int IgnoreBottomFramentCount(int totalFragments)
		{
			return GetIgnoreFragmentCount(IgnoreBottom, totalFragments);
		}

		private int GetIgnoreFragmentCount(string ignore, int totalFragments)
		{
			int val = 0;
			IsValid();

			if (ignore.EndsWith("%"))
			{
				double pct = Convert.ToInt32(ignore.Substring(0, ignore.Length - 1)) / 100.0;

				val = Convert.ToInt32(Math.Round(totalFragments * pct));
			}
			else
			{
				val = Convert.ToInt32(ignore);
			}

			return val;
		}

		private bool IsValid()
		{
            if (!_isValid.HasValue)
            {
                _isValid = false;

                // Validate min/max fragments
                bool minLessThanOrEqualToMax = _minFragments <= _maxFragments;

                // Validate ignore values
                // The value must be a percent between "0%" and "100%" or an integer >= 0
                var pattern = new Regex(@"^(100|\d{1,2})%$|^\d*$");
                var isValidIgnoreTop = pattern.IsMatch(_ignoreTop);
                var isValidIgnoreBottom = pattern.IsMatch(_ignoreBottom);

                _isValid = minLessThanOrEqualToMax && isValidIgnoreBottom && isValidIgnoreTop;

                if (_isValid == false)
                {
                    string errorMsg = string.Format("The LetterSize is not valid:{0}{1}{2}",
                        minLessThanOrEqualToMax ? string.Empty : "  MinFragments greater than MaxFragments.",
                        isValidIgnoreTop ? string.Empty : "  IgnoreTop value must be a positive integer or a percent (e.g., 15%).",
                        isValidIgnoreBottom ? string.Empty : "  IgnoreBottom value must be a positive integer or a percent (e.g., 15%).");

                    throw new System.Configuration.ConfigurationErrorsException(errorMsg);
                }
            }

			return _isValid.Value;
		}
		#endregion Methods
    }
}
