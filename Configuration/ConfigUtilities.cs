﻿using log4net;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System;
using System.Configuration;

namespace Aquilent.Cara.Configuration
{
	public static class ConfigUtilities
	{
		private static ILog _logger = LogManager.GetLogger(typeof(ConfigUtilities));

		/// <summary>
		/// Looks for the specified section name in two places:
		/// First it looks to see if there is a section titled "sectionName" in the web.config file (or app.config as appropriate)
		/// If no section is found, it looks for an appSetting with the key, "sectionName".  That value should point to a
		/// separate config file with a section titled "sectionName"
		/// </summary>
		/// <typeparam name="T">Must be a class</typeparam>
		/// <param name="sectionName">The name of the section to get</param>
		/// <returns>The requested section or null</returns>
		public static T GetSection<T>(string sectionName) where T : class //ConfigurationSection
		{
			T configSection = null;
			if (ConfigurationManager.GetSection(sectionName) != null)
			{
				configSection = ConfigurationManager.GetSection(sectionName) as T;
			}
			else
			{
				string configSectionPath = ResolveRelativeConfigPath(sectionName);
                if (configSectionPath != null)
                {
                    using (FileConfigurationSource configFile = new FileConfigurationSource(configSectionPath))
                    {
                        configSection = configFile.GetSection(sectionName) as T;
                    }
                }
			}

			if (configSection == null)
			{
				string message = String.Format("No configuration section or appSetting exists named {0}", sectionName);
				_logger.FatalFormat(message);
				throw new ConfigurationErrorsException(message);
			}

			return configSection;
		}

		/// <summary>
		/// This is used when the configuration value is assumed to be a file or directory path
		/// </summary>
		/// <param name="appSettingsKey"></param>
		/// <returns>the resolved path</returns>
		public static string ResolveRelativeConfigPath(string appSettingsKey)
		{
            _logger.DebugFormat("ResolveRelativeConfigPath(\"{0}\")", appSettingsKey);
            string configSectionPath = null;
            
			var sectionNameValue = ConfigurationManager.AppSettings[appSettingsKey];
            if (sectionNameValue != null)
            {
                var fileInfo = new System.IO.FileInfo(sectionNameValue);
                configSectionPath = sectionNameValue;

                // Check to see if the specificed config file exists
                if (fileInfo.Exists)
                {
                    configSectionPath = fileInfo.FullName;
                }
                else
                {
                    // Assume the specified path is relative to the BaseDirectory
                    configSectionPath = AppDomain.CurrentDomain.BaseDirectory + sectionNameValue;
                }
            }

			return configSectionPath;
		}
	}
}
