﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Configuration
{
	public class DirectoryElement : ConfigurationElement
	{
		private DirectoryInfo _dirInfo;

		[ConfigurationProperty("dir", IsRequired=true)]
		public string DirectoryPath
		{
			get
			{
				return (string)this["dir"];
			}
			set
			{
				this["dir"] = value;
			}
		}

		public DirectoryInfo DirInfo
		{
			get
			{
				if (_dirInfo == null)
				{
					_dirInfo = new DirectoryInfo(DirectoryPath);
				}

				return _dirInfo;
			}
		}
	}
}
