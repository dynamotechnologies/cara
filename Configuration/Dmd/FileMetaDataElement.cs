﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;

namespace Aquilent.Cara.Configuration.Dmd
{
	public class FileMetaDataElement : ConfigurationElement
	{
		[ConfigurationProperty("appdoctype", IsRequired = true)]
		public string AppDocType
		{
			get
			{
				return (string) this["appdoctype"];
			}

			set
			{
				this["appdoctype"] = value;
			}
		}

		[ConfigurationProperty("datelabel")]
		public string DateLabel
		{
			get
			{
				return (string) this["datelabel"];
			}

			set
			{
				this["datelabel"] = value;
			}
		}

		[ConfigurationProperty("level2name", IsRequired = true)]
		public string Level2Name
		{
			get
			{
				return (string) this["level2name"];
			}

			set
			{
				this["level2name"] = value;
			}
		}

		[ConfigurationProperty("level3name", IsRequired = true)]
		public string Level3Name
		{
			get
			{
				return (string) this["level3name"];
			}

			set
			{
				this["level3name"] = value;
			}
		}

		[ConfigurationProperty("level2id", IsRequired = true)]
		public string Level2Id
		{
			get
			{
				return (string) this["level2id"];
			}

			set
			{
				this["level2id"] = value;
			}
		}

		[ConfigurationProperty("level3id", IsRequired = true)]
		public string Level3Id
		{
			get
			{
				return (string) this["level3id"];
			}

			set
			{
				this["level3id"] = value;
			}
		}

		[ConfigurationProperty("filename")]
		public string Filename
		{
			get
			{
				return (string) this["filename"];
			}

			set
			{
				this["filename"] = value;
			}
		}
	}
}
