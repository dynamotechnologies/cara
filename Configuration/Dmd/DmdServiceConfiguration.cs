﻿using System.Configuration;
using System.Net;

namespace Aquilent.Cara.Configuration.Dmd
{
	public class DmdServiceConfiguration : ConfigurationSection
	{
		/// <summary>
		/// DMD Username
		/// </summary>
		[ConfigurationProperty("username", IsRequired = true)]
		public string Username
		{
			get
			{
				return (string) this["username"];
			}

			set
			{
				this["username"] = value;
			}
		}

		/// <summary>
		/// DMD Password
		/// </summary>
		[ConfigurationProperty("password", IsRequired = true)]
		public string Password
		{
			get
			{
				return (string) this["password"];
			}

			set
			{
				this["password"] = value;
			}
		}

		/// <summary>
		/// Returns the username and password as an ICredential
		/// </summary>
		public ICredentials Credentials
		{
			get
			{
				return new NetworkCredential(Username, Password);
			}
		}

		/// <summary>
		/// The URL of the DMD XML-RPC service
		/// </summary>
		[ConfigurationProperty("url", IsRequired = true)]
		public string Url
		{
			get
			{
				return (string) this["url"];
			}

			set
			{
				this["url"] = value;
			}
		}

		/// <summary>
		/// The AppSysId assigned to CARA
		/// </summary>
		[ConfigurationProperty("appsysid", IsRequired = true)]
		public string AppSysId
		{
			get
			{
				return (string) this["appsysid"];
			}

			set
			{
				this["appsysid"] = value;
			}
		}

		[ConfigurationProperty("letterdefaults", IsRequired = true)]
		public FileMetaDataElement LetterDefaults
		{
			get
			{
				return (FileMetaDataElement) this["letterdefaults"];
			}

			set
			{
				this["letterdefaults"] = value;
			}
		}

        [ConfigurationProperty("reportdefaults", IsRequired = true)]
        public FileMetaDataElement ReportDefaults
        {
            get
            {
                return (FileMetaDataElement)this["reportdefaults"];
            }

            set
            {
                this["reportdefaults"] = value;
            }
        }

        [ConfigurationProperty("commentperioddefaults", IsRequired = true)]
        public FileMetaDataElement CommentPeriodDefaults
        {
            get
            {
                return (FileMetaDataElement)this["commentperioddefaults"];
            }

            set
            {
                this["commentperioddefaults"] = value;
            }
        }

        [ConfigurationProperty("objectiondefaults", IsRequired = true)]
        public FileMetaDataElement ObjectionDefaults
        {
            get
            {
                return (FileMetaDataElement)this["objectiondefaults"];
            }

            set
            {
                this["objectiondefaults"] = value;
            }
        }

        [ConfigurationProperty("objectionletterdefaults", IsRequired = true)]
        public FileMetaDataElement ObjectionLetterDefaults
        {
            get
            {
                return (FileMetaDataElement)this["objectionletterdefaults"];
            }

            set
            {
                this["objectionletterdefaults"] = value;
            }
        }
    }
}
