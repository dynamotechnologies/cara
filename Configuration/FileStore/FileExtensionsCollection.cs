﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Configuration.FileStore
{
	public class FileExtensionsCollection : ConfigurationElementCollection
	{
		public FileExtensionsCollection()
		{
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}

		protected override ConfigurationElement CreateNewElement(string elementName)
		{
			return base.CreateNewElement(elementName);
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new FileExtensionConfigElement();
		}

		protected override Object GetElementKey(ConfigurationElement element)
		{
			return ((FileExtensionConfigElement)element).Extension;
		}

		public FileExtensionConfigElement this[int index]
		{
			get
			{
				return (FileExtensionConfigElement)BaseGet(index);
			}
			set
			{
				if (BaseGet(index) != null)
				{
					BaseRemoveAt(index);
				}
				BaseAdd(index, value);
			}
		}

		public int IndexOf(FileExtensionConfigElement fileExt)
		{
			return BaseIndexOf(fileExt);
		}

		public void Add(FileExtensionConfigElement fileExt)
		{
			BaseAdd(fileExt);
			// Add custom code here.
		}

		public void Remove(FileExtensionConfigElement fileExt)
		{
			if (BaseIndexOf(fileExt) >= 0)
			{
				BaseRemove(fileExt.Name);
			}
		}

		public void RemoveAt(int index)
		{
			BaseRemoveAt(index);
		}

		public void Remove(string extension)
		{
			BaseRemove(extension);
		}

		public void Clear()
		{
			BaseClear();
			// Add custom code here.
		}
	}
}
