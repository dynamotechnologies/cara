﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.IO;

namespace Aquilent.Cara.Configuration.FileStore
{
	public class FileStoreConfiguration : ConfigurationSection
	{
		[ConfigurationProperty("filestore", IsRequired = true)]
		public FullyQualifiedClassElement FileStore 
		{
			get
			{
				return (FullyQualifiedClassElement)base["filestore"];
			}
			set
			{
				base["filestore"] = value;
			}
		}

		[ConfigurationProperty("validFileExtensions", IsDefaultCollection=false)]
		public FileExtensionsCollection FileExtensions
		{
			get
			{
				return (FileExtensionsCollection)base["validFileExtensions"];
			}
		}

		[ConfigurationProperty("invalidCharacters", IsDefaultCollection = false)]
		public KeyValueConfigurationCollection InvalidCharacters
		{
			get
			{
				return (KeyValueConfigurationCollection) base["invalidCharacters"];
			}
		}

		[ConfigurationProperty("uploadFolder")]
		public DirectoryElement UploadFolder
		{
			get
			{
				return (DirectoryElement)base["uploadFolder"];
			}
		}

		[ConfigurationProperty("tempZipFolder")]
		public DirectoryElement TempZipFolder
		{
			get
			{
				return (DirectoryElement)base["tempZipFolder"];
			}
		}

		//protected override void DeserializeSection(
		//    System.Xml.XmlReader reader)
		//{
		//    base.DeserializeSection(reader);
		//    // You can add custom processing code here.
		//}

		//protected override string SerializeSection(
		//    ConfigurationElement parentElement,
		//    string name, ConfigurationSaveMode saveMode)
		//{
		//    string s =
		//        base.SerializeSection(parentElement,
		//        name, saveMode);
		//    // You can add custom processing code here.
		//    return s;
		//}
	}
}
