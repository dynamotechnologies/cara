﻿using System;
using System.Configuration;

namespace Aquilent.Cara.Configuration.FileStore
{

	public class FileExtensionConfigElement : ConfigurationElement
	{
		#region Properties
		[ConfigurationProperty("name", IsRequired=true)]
        public string Name
        {
			get
			{
				return (string)this["name"];
			}
			set
			{
				this["name"] = value;
			}
		}

		[ConfigurationProperty("extension", IsRequired=true, IsKey=true)]
		public string Extension
		{
			get
			{
				return (string)this["extension"];
			}
			set
			{
				this["extension"] = value;
			}
		}

		[ConfigurationProperty("mimetype", IsRequired=true, DefaultValue="application/octet-stream")]
		[RegexStringValidator(@"[\w\.-]+\/[\w\.-]+")]
		public string MimeType
		{
			get
			{
				return (string)this["mimetype"];
			}
			set
			{
				this["mimetype"] = value;
			}
		}
		#endregion Properties

		#region Constructors
		public FileExtensionConfigElement()
		{
		}

		public FileExtensionConfigElement(string elementName)
		{
			Extension = elementName;
		}

		public FileExtensionConfigElement(string newName, string newExtension, string mimetype)
		{
			Name = newName;
			Extension = newExtension;
			MimeType = mimetype;
		}
		#endregion Constructors
	}

}
