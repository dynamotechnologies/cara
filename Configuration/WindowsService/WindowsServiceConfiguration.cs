﻿using System.Configuration;

namespace Aquilent.Cara.Configuration.WindowsService
{
	public class WindowsServiceConfiguration : ConfigurationSection
	{
		[ConfigurationProperty("schedule", IsRequired = true)]
		public ScheduleElement Schedule
		{
			get
			{
				return (ScheduleElement) this["schedule"];
			}

			set
			{
				this["schedule"] = value;
			}
		}
	}
}
