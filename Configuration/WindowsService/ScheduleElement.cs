﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Aquilent.Cara.Configuration.WindowsService
{
	public class ScheduleElement : ConfigurationElement
	{
		/// <summary>
		/// Time span to wait after server reboot
		/// before the service starts to run.
		/// </summary>
		[ConfigurationProperty("rebootDelay", IsRequired = true, DefaultValue="00:05:00")]
		[TimeSpanValidator(MinValueString = "00:04:00", MaxValueString = "01:00:00")]
		public TimeSpan RebootDelay
		{
			get
			{
				return (TimeSpan) this["rebootDelay"];
			}

			set
			{
				this["rebootDelay"] = value;
			}
		}

		/// <summary>
		/// true, to run the service on startup, then resume schedule
		/// false, to wait for next scheduled time
		/// </summary>
		[ConfigurationProperty("runOnStart", IsRequired = true)]
		public bool RunOnStart
		{
			get
			{
				return (bool) this["runOnStart"];
			}

			set
			{
				this["runOnStart"] = value;
			}
		}

		/// <summary>
		/// Display the next run time in the log when the current run completes
		/// </summary>
		[ConfigurationProperty("showNextRunTime", IsRequired = true)]
		public bool ShowNextRunTime
		{
			get
			{
				return (bool) this["showNextRunTime"];
			}

			set
			{
				this["showNextRunTime"] = value;
			}
		}

		/// <summary>
		/// Indicates the time to start the service the first time
		/// 
		/// Format:  hh:mm:ss, where you can use ## as a wildcard,
		/// so ##:05:00, will always start the service at the next 5 minutes
		/// past the hour after the service has started.
		/// </summary>
		[ConfigurationProperty("startTime", IsRequired = false)]
		public string StartTime
		{
			get
			{
				return (string) this["startTime"];
			}

			set
			{
				this["startTime"] = value;
			}
		}

		/// <summary>
		/// Minutes between runs
		/// 
		/// Note: Must be used with &lt;startTime&gt;
		/// </summary>
		[ConfigurationProperty("interval", IsRequired = false, DefaultValue="0:1:0")]
		[TimeSpanValidator(MinValueString = "0:1:0")]
		public TimeSpan Interval
		{
			get
			{
				return (TimeSpan) this["interval"];
			}

			set
			{
				this["interval"] = value;
			}
		}

		/// <summary>
		/// Comma delimited list of explicit times to run the service
		/// 
		/// Note: Takes precedence over &lt;startTime&gt; and  &lt;interval&gt; and &lt;frequencey&gt;
		/// </summary>
		[ConfigurationProperty("scheduledTimes", IsRequired = false)]
		//[RegexStringValidator(@"(\d{1,2}:\d{2}:\d{2},){0,1}(\d{1,2}:\d{2}:\d{2})")] Commented out because this doesn't work without DefaultValue in ConfigurationProperty
		public string ScheduledTimes
		{
			get
			{
				return (string) this["scheduledTimes"];
			}

			set
			{
				this["scheduledTimes"] = value;
			}
		}

		public override string ToString()
		{
			return string.Format("Interval: {0}, RebootDelay: {1}, RunOnStart: {2}, ScheduledTimes: {3}, ShowNextRunTime: {4}, StartTime: {5}",
				Interval, RebootDelay, RunOnStart, ScheduledTimes, ShowNextRunTime, StartTime);
		}
	}
}
