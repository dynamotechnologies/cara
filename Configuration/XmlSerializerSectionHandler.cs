﻿using System;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.IO;

namespace Aquilent.Cara.Configuration
{
    public class XmlSerializerSectionHandler : IConfigurationSectionHandler
    {
        public static string QualifyPath(XmlNode sectionNode, string file)
        {
            if (Path.IsPathRooted(file))
            {
                return file;
            }
            string configPath = ConfigurationException.GetXmlNodeFilename(sectionNode);
            string path = Path.GetDirectoryName(configPath);
            return Path.Combine(path, file);
        }

        public XmlNode LoadFile(string fileName)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.Load(fileName);
                return document.DocumentElement;
            }
            catch (Exception e)
            {
                throw new Exception("Unable to load configuration from file " + fileName, e);
            }
        }

        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            if (section == null)
                throw new Exception("Could not configure section because the node was null");

            XmlNode sectionNode = section;
            if (section.Attributes["file"] != null)
            {
                sectionNode = LoadFile(QualifyPath(section, section.Attributes["file"].Value));
            }
            return CreateFromSection(sectionNode);
        }

        public object CreateFromSection(XmlNode section)
        {
            XPathNavigator nav = section.CreateNavigator();
            string typename = (string)nav.Evaluate("string(@type)");
            Type t = Type.GetType(typename);

            XmlSerializer ser = new XmlSerializer(t);

            return ser.Deserialize(new XmlNodeReader(section));
        }
    }
}
