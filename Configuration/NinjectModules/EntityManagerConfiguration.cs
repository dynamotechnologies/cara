﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Aquilent.Cara.Configuration.EntityManager
{
	public class EntityManagerConfiguration : ConfigurationSection
	{
		[ConfigurationProperty("managers", IsDefaultCollection = false)]
		public ManagersCollection Modules
		{
			get
			{
				return (ManagersCollection) base["managers"];
			}
		}

	}
}
