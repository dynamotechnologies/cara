﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Runtime.Remoting;
using Ninject.Modules;

namespace Aquilent.Cara.Configuration.EntityManager
{
	/// <summary>
	/// This represents a configuration element that contains two parts of a fully qualified class name:
	///   class name and assembly name.
	/// </summary>
	public class ManagerClassElement : FullyQualifiedClassElement
	{
		/// <summary>
		/// The name of the class.
		/// 
		/// Example:
		///   Name.Space.Class.Name
		/// </summary>
		[ConfigurationProperty("moduleType", IsRequired=false)]
		public string ModuleType
		{
			get
			{
				return (string) this["moduleType"];
			}
			set
			{
				this["moduleType"] = value;
			}
		}

		#region IObjectHandle Members

		/// <summary>
		/// Returns an instance of the type represented by this configuration element.
		/// </summary>
		/// <returns>The unwrapped object.</returns>
		public NinjectModule UnwrapNinjectModule()
		{
			int indexOf = ModuleType.IndexOf(",");
			string assemblyName = ModuleType.Substring(indexOf + 1).Trim();
			string className = ModuleType.Substring(0, indexOf).Trim();

			return (NinjectModule) Activator.CreateInstance(assemblyName, className).Unwrap();
		}

		#endregion

		public override string ToString()
		{
			return string.Format("{0}, LookupType: {1}", base.ToString(), ModuleType);
		}
	}
}
