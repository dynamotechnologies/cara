﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class MessageLoggerBehavior : IEndpointBehavior
	{
		private bool LoggingEnabled { get; set; }

		public MessageLoggerBehavior(bool loggingEnabled)
		{
			LoggingEnabled = loggingEnabled;
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
		{
			
		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
		{
			if (LoggingEnabled)
			{
				clientRuntime.MessageInspectors.Add(new ClientMessageLogger());
			}
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
		{
			
		}

		public void Validate(ServiceEndpoint endpoint)
		{
			
		}
	}
}
