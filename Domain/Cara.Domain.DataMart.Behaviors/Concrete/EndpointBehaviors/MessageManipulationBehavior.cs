﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class MessageManipulationBehavior : IEndpointBehavior
	{
		private string ReplacementEmail { get; set; }

		public MessageManipulationBehavior(string replacementEmail)
			: base()
		{
			ReplacementEmail = replacementEmail;
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
		{

		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
		{
			clientRuntime.MessageInspectors.Add(new DataMartUserEmailReplacer(ReplacementEmail));
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
		{

		}

		public void Validate(ServiceEndpoint endpoint)
		{

		}
	}
}
