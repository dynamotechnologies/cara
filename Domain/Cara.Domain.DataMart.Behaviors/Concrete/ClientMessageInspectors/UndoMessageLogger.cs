﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using log4net;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.ServiceModel.Channels;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class UndoMessageLogger : IClientMessageInspector
	{
		private ILog _logger = LogManager.GetLogger(typeof(UndoMessageLogger));

		public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
		{
		}

		public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
		{
			_logger.Info(request.ToString());
			var bufferedCopy = request.CreateBufferedCopy(int.MaxValue);
			var messageCopy = bufferedCopy.CreateMessage();
			var method = (request.Properties.Values.First() as HttpRequestMessageProperty).Method;

			string body;
			XElement bodyContent = null;
			if (method != "GET")
			{
				if (!messageCopy.IsEmpty)
				{
					using (var xdr = messageCopy.GetReaderAtBodyContents())
					{
						bodyContent = XElement.Load(xdr, LoadOptions.None);
						//body = bodyContent.ToString();

						//_logger.InfoFormat("{0}::{1}::{2}", (messageCopy.Properties.Values.First() as HttpRequestMessageProperty).Method, messageCopy.Headers.To.OriginalString, body);
					}
				}

				if (method == "POST")
				{
					if (request.Headers.To.AbsolutePath.Contains("/linkProject"))
					{
						LogUndoLinkProject(bodyContent);
					}
					else if (request.Headers.To.AbsolutePath.Contains("/unlinkProject"))
					{
						LogUndoUnlinkProject(bodyContent);
					}
					else if (request.Headers.To.AbsolutePath.Contains("/linkDoc"))
					{
						LogUndoLinkDoc(bodyContent);
					}
					else if (request.Headers.To.AbsolutePath.Contains("/unlinkDoc"))
					{
						LogUndoUnlinkDoc(bodyContent);
					}
					else if (request.Headers.To.AbsolutePath.Contains("/comment/phases"))
					{
						LogUndoAddCommentPhase(bodyContent, request.Headers.To.Segments);
					}
					else if (request.Headers.To.AbsolutePath.Contains("/mailinglist/subscribers"))
					{
						LogUndoAddSubscriber(bodyContent, request.Headers.To.Segments);
					}
				}
				else if (method == "DELETE")
				{
					if (request.Headers.To.AbsolutePath.Contains("/comment/phases"))
					{
						LogUndoDeleteCommentPhase(request.Headers.To.Segments);
					}
					else if (request.Headers.To.AbsolutePath.Contains("/mailinglist/subscribers"))
					{
						LogUndoDeleteSubscriber(request.Headers.To.Segments);
					}
				}
			}

			request = bufferedCopy.CreateMessage();

			return Guid.NewGuid();
		}

		private void LogUndoLinkProject(XElement bodyContent)
		{
			string caraProjectId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "caraid").First().Value;
			string palsProjectId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "palsid").First().Value;
			_logger.InfoFormat("UnlinkProject::{0}::{1}", caraProjectId, palsProjectId);
		}

		private void LogUndoUnlinkProject(XElement bodyContent)
		{
			string caraProjectId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "caraid").First().Value;
			string palsProjectId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "palsid").First().Value;
			_logger.InfoFormat("LinkProject::{0}::{1}", caraProjectId, palsProjectId);
		}

		private void LogUndoLinkDoc(XElement bodyContent)
		{
			string caraProjectId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "caraid").First().Value;
			string phaseId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "phaseid").First().Value;
			string docId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "docid").First().Value;
			_logger.InfoFormat("UnlinkDoc::{0}::{1}::{2}", caraProjectId, phaseId, docId);
		}

		private void LogUndoUnlinkDoc(XElement bodyContent)
		{
			string caraProjectId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "caraid").First().Value;
			string phaseId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "phaseid").First().Value;
			string docId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "docid").First().Value;
			_logger.InfoFormat("LinkDoc::{0}::{1}::{2}", caraProjectId, phaseId, docId);
		}

		private void LogUndoAddCommentPhase(XElement bodyContent, string[] uriSegments)
		{
			var pattern = "\\d+/?";
			var regex = new System.Text.RegularExpressions.Regex(pattern);
			var intSegments = uriSegments.Where(x => regex.IsMatch(x)).ToList();

			var caraProjectId = intSegments[0].EndsWith("/") ? intSegments[0].Substring(0, intSegments[0].Length - 1) : intSegments[0];
			string phaseId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "phaseid").First().Value;

			_logger.InfoFormat("DeletePhase::{0}::{1}", caraProjectId, phaseId);
		}

		private void LogUndoDeleteCommentPhase(string[] uriSegments)
		{
			var pattern = "\\d+/?";
			var regex = new System.Text.RegularExpressions.Regex(pattern);
			var intSegments = uriSegments.Where(x => regex.IsMatch(x)).ToList();

			var caraProjectId = intSegments[0].EndsWith("/") ? intSegments[0].Substring(0, intSegments[0].Length - 1) : intSegments[0];
			var phaseId = intSegments[1].EndsWith("/") ? intSegments[1].Substring(0, intSegments[1].Length - 1) : intSegments[1];

			_logger.InfoFormat("AddPhase::{0}::{1}", caraProjectId, phaseId);
		}

		private void LogUndoAddSubscriber(XElement bodyContent, string[] uriSegments)
		{
			var pattern = "\\d+/?";
			var regex = new System.Text.RegularExpressions.Regex(pattern);
			var intSegments = uriSegments.Where(x => regex.IsMatch(x)).ToList();

			var caraProjectId = intSegments[0].EndsWith("/") ? intSegments[0].Substring(0, intSegments[0].Length - 1) : intSegments[0];
			string subscriberId = bodyContent.Elements(bodyContent.GetDefaultNamespace() + "subscriberid").First().Value;

			_logger.InfoFormat("DeleteSubscriber::{0}::{1}", caraProjectId, subscriberId);
		}

		private void LogUndoDeleteSubscriber(string[] uriSegments)
		{
			var pattern = "\\d+/?";
			var regex = new System.Text.RegularExpressions.Regex(pattern);
			var intSegments = uriSegments.Where(x => regex.IsMatch(x)).ToList();

			var caraProjectId = intSegments[0].EndsWith("/") ? intSegments[0].Substring(0, intSegments[0].Length - 1) : intSegments[0];
			var subscriberId = intSegments[1].EndsWith("/") ? intSegments[1].Substring(0, intSegments[1].Length - 1) : intSegments[1];

			_logger.InfoFormat("AddSubscriber::{0}::{1}", caraProjectId, subscriberId);
		}
	}
}
