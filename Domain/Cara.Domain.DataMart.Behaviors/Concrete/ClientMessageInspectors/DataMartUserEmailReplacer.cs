﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using System.Xml.Linq;
using System.ServiceModel.Channels;
using System.IO;
using System.Xml;
using System.Configuration;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class DataMartUserEmailReplacer : IClientMessageInspector
	{
		private List<Guid> _stateCorrelators = new List<Guid>();
		private string ReplacementEmail { get; set; }

		public DataMartUserEmailReplacer(string replacementEmail)
		{
			ReplacementEmail = replacementEmail;
		}

		public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
		{
			bool processMessage = false;

			if (correlationState is Guid)
			{
				Guid correlationStateGuid = (Guid) correlationState;
				if (_stateCorrelators.Contains(correlationStateGuid))
				{
					processMessage = true;
					_stateCorrelators.Remove(correlationStateGuid);
				}
			}

			if (processMessage)
			{
				reply = ReplaceEmailsInMessage(reply);
			}
		}

		public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
		{
			// Remember calls to /users/{username} and /utils/findUser
			Guid correlationState = Guid.NewGuid();
			if (request.Headers.To.AbsolutePath.ToLower().Contains("user"))
			{
				_stateCorrelators.Add(correlationState);
			}

			return correlationState;
		}

		/// <summary>
		/// Replaces all e-mail addresses in the message
		/// </summary>
		/// <param name="oldMessage">the reply</param>
		/// <returns>a message to replace the reply</returns>
		private Message ReplaceEmailsInMessage(Message oldMessage)
		{
			Message newMessage = null;

			var bufferedCopy = oldMessage.CreateBufferedCopy(int.MaxValue);
			var messageCopy = bufferedCopy.CreateMessage();

			XElement xdoc;
			using (var xdr = messageCopy.GetReaderAtBodyContents())
			{
				// Replace all of the e-mail addresses
				xdoc = XElement.Load(xdr, LoadOptions.None);
			}

			// Change all of the e-mail addresses
			foreach (var emailElement in xdoc.Descendants(xdoc.GetDefaultNamespace() + "email"))
			{
				emailElement.Value = ReplacementEmail;
			}

			// Write the XML to memory so we can use it in the new message
			MemoryStream ms = new MemoryStream();
			XmlWriter xw = XmlWriter.Create(ms);
			xdoc.Save(xw);
			xw.Flush();
			xw.Close();

			ms.Position = 0;
			XmlReader xr = XmlReader.Create(ms);

			// Create the new message
			newMessage = Message.CreateMessage(oldMessage.Version, null, xr);
			newMessage.Headers.CopyHeadersFrom(oldMessage);
			newMessage.Properties.CopyProperties(oldMessage.Properties);

			return newMessage;
		}
	}
}
