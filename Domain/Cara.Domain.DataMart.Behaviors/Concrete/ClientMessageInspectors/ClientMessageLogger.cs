﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using log4net;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.ServiceModel.Channels;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class ClientMessageLogger : IClientMessageInspector
	{
		private ILog _logger = LogManager.GetLogger(typeof(ClientMessageLogger));

		public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
		{
			if (correlationState is Guid)
			{
				var bufferedCopy = reply.CreateBufferedCopy(int.MaxValue);
				var messageCopy = bufferedCopy.CreateMessage();

				_logger.InfoFormat("REPLY   ({0})", correlationState);

				if (!messageCopy.IsEmpty)
				{
					string body;
					using (var xdr = messageCopy.GetReaderAtBodyContents())
					{
						var xe = XElement.Load(xdr, LoadOptions.None);
						body = xe.ToString();

						_logger.InfoFormat("REPLY  ({0})\n{1}", correlationState, body);
					}
				}

				reply = bufferedCopy.CreateMessage();
			}
		}

		public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
		{
			Guid correlationState = Guid.NewGuid();

			var bufferedCopy = request.CreateBufferedCopy(int.MaxValue);
			var messageCopy = bufferedCopy.CreateMessage();

			_logger.InfoFormat("REQUEST ({0}) {1} {2}", correlationState, (messageCopy.Properties.Values.First() as HttpRequestMessageProperty).Method, messageCopy.Headers.To.OriginalString);

			if (!messageCopy.IsEmpty)
			{
				string body;
				using (var xdr = messageCopy.GetReaderAtBodyContents())
				{
					var xe = XElement.Load(xdr, LoadOptions.None);
					body = xe.ToString();

					_logger.InfoFormat("REQUEST ({0})\n{1}", correlationState, body);
				}
			}

			request = bufferedCopy.CreateMessage();

			return correlationState;
		}
	}
}
