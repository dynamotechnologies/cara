﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class UndoLoggerBehaviorExtension : BehaviorExtensionElement
	{

		public override Type BehaviorType
		{
			get { return typeof(UndoMessageLogger); }
		}

		protected override object CreateBehavior()
		{
			return new UndoMessageLogger();
		}
	}
}
