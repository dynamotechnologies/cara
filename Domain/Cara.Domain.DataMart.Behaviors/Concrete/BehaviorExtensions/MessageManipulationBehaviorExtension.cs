﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Configuration;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class MessageManipulationBehaviorExtension : BehaviorExtensionElement
	{
		[ConfigurationProperty("replaceAllUserEmailWith", IsRequired = true)]
		public string ReplacementEmail
		{
			get
			{
				return (string) this["replaceAllUserEmailWith"];
			}
			set
			{
				this["replaceAllUserEmailWith"] = value;
			}
		}

		public override Type BehaviorType
		{
			get { return typeof(MessageManipulationBehavior); }
		}

		protected override object CreateBehavior()
		{
			return new MessageManipulationBehavior(ReplacementEmail);
		}
	}
}
