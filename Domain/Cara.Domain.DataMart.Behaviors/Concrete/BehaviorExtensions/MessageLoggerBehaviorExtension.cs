﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Configuration;

namespace Aquilent.Cara.Domain.DataMart.Behaviors
{
	public class MessageLoggerBehaviorExtension : BehaviorExtensionElement
	{
		[ConfigurationProperty("enabled", IsRequired = true)]
		public bool LoggingEnabled
		{
			get
			{
				return (bool) this["enabled"];
			}
			set
			{
				this["enabled"] = value;
			}
		}

		public override Type BehaviorType
		{
			get { return typeof(MessageLoggerBehavior); }
		}

		protected override object CreateBehavior()
		{
			return new MessageLoggerBehavior(LoggingEnabled);
		}
	}
}
