﻿using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.Dmd;
using Aquilent.Cara.Domain.Dmd;
using System;
using System.Collections.Generic;
using System.IO;

namespace Aquilent.Cara.Domain.DataMartReplaceFileMetaData
{
	class Program
	{
		private static DmdServiceConfiguration _config = ConfigUtilities.GetSection<DmdServiceConfiguration>("dmdServiceConfiguration");

		/// <summary>
		/// 
		/// </summary>
		/// <param name="args">Should contain a file of DMD IDs with one id per line</param>
		static void Main(string[] args)
		{
			FileInfo inputDmdIds = new FileInfo(args[0]);
			if (!inputDmdIds.Exists)
			{
				throw new ApplicationException(string.Format("The specified file, {0}, does not exist", inputDmdIds.FullName));
			}

			var dmdIds = ReadInputFile(inputDmdIds);

			DmdManager dmdManager = new DmdManager();

			var newMetadata = new NewFileMetaData();
			newMetadata.AppDocType = "Supporting";
			newMetadata.MoreMetaData.Level2Name = "Public involvement";
			
			var options = new ReplaceFileMetaDataOptions();
			options.mergeMoreMetadataValues = true;

			string level3name;
			foreach (string dmdId in dmdIds)
			{
				FileMetaData metadata;
				try
				{
					metadata = dmdManager.GetFileMetadata(dmdId);
				}
				catch
				{
					Console.WriteLine(string.Format("An error occurred getting the metadata for {0}", dmdId));
					continue;
				}

				if (metadata.AppDocType != "Supporting")
				{
					level3name = null;
					if (metadata.MoreMetaData.Level3Name == "Comments")
					{
						level3name = "Original Letters and Submissions";
					}

					newMetadata.MoreMetaData.Level3Name = level3name;

					try
					{
						if (dmdManager.ReplaceFileMetatdata(dmdId, newMetadata, options))
						{
							Console.WriteLine(string.Format("{0} was updated", dmdId));
						}
					}
					catch
					{
						Console.WriteLine(string.Format("An error occurred replacing the metadata for {0}", dmdId));
						continue;
					}
				}
				else
				{
					Console.WriteLine(string.Format("Metadata appears to have already been updated for {0}", dmdId));
				}
			}

			Console.WriteLine("Done");
		}

		private static IList<string> ReadInputFile(FileInfo inputDmdIds)
		{
			var reader = inputDmdIds.OpenText();
			var dmdIds = new List<string>();
			using (reader)
			{
				while (!reader.EndOfStream)
				{
					dmdIds.Add(reader.ReadLine());
				}
			}

			return dmdIds;
		}
	}
}
