﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class UnitLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.Units.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.Units.Where(x => x.UnitId != "0000").ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
