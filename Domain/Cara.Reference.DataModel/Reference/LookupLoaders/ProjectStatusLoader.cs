﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class ProjectStatusLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.ProjectStatuses.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.ProjectStatuses.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
