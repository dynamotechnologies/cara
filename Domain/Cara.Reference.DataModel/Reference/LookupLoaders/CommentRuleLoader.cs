﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class CommentRuleLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.CommentRules.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.CommentRules.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
