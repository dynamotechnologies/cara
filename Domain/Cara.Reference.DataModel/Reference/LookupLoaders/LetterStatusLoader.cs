﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class LetterStatusLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.LetterStatuses.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.LetterStatuses.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
