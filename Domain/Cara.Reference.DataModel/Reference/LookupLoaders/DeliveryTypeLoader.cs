﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class DeliveryTypeLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.DeliveryTypes.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.DeliveryTypes.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
