﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class TermRefLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.Units.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.TermRefs.Include("TermList").ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
