﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class PurposeLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.Purposes.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.Purposes.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
