﻿using System;
using System.Linq;

using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Reference
{
	public class UnpublishedReasonLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.UnpublishedReasons.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.UnpublishedReasons.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
