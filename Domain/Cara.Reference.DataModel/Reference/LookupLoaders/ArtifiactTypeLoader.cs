﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Cara.Reference
{
	public class ArtifactTypeLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReferenceEntities())
			{
				context.ArtifactTypes.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.ArtifactTypes.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
