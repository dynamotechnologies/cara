﻿using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;

namespace Aquilent.Cara.Domain.DataMart
{
	public class DataMartManager : IDisposable
	{
		#region Member Variables
        private static ChannelFactory<IDataMart> ChannelFactory { get; set; }
		private IDataMart DataMart { get; set; }
		public UserManager UserManager { get; set; }
		public ProjectManager ProjectManager { get; set; }
		public WebCommentPageInfoManager WebInfoManager { get; set; }
		private ILog logger = LogManager.GetLogger(typeof(DataMartManager));

        public static readonly string DocumentBaseUrl = ConfigurationManager.AppSettings["DataMartDocumentBaseUrl"];
		#endregion

        #region Contructors
        /// <summary>
        /// Static constructor used to initialize the ChannelFactory
        /// 
        /// Requires the following AppSettings:
        ///   - DataMartUseCredentials:  true/false
        ///   - DataMartBasicUsername
        ///   - DataMartBasicPassword
        /// </summary>
        static DataMartManager()
        {
            try
            {
                if (ChannelFactory == null)
                {
                    bool useCredentials;
                    try
                    {
                        useCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["DataMartUseCredentials"]);
                    }
                    catch
                    {
                        useCredentials = false;
                    }

                    string endpoint = ConfigurationManager.AppSettings["DataMartEndpoint"];
                    ChannelFactory = new ChannelFactory<IDataMart>(endpoint);

                    if (useCredentials)
                    {
                        ChannelFactory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["DataMartBasicUsername"];
                        ChannelFactory.Credentials.UserName.Password = ConfigurationManager.AppSettings["DataMartBasicPassword"];
                    }
                }
            }
            catch (ConfigurationErrorsException cee)
            {
                LogManager.GetLogger(typeof(DataMartManager)).Fatal("An error occurred retrieving the DataMart configuration: " + cee.BareMessage);
                throw new DataMartException(cee.BareMessage, cee);
            }
        }

        /// <summary>
        /// Standard Constructor
        /// </summary>
        public DataMartManager()
        {
            try
            {
                DataMart = ChannelFactory.CreateChannel();
                ((IClientChannel)DataMart).Open();
            }
            catch (CommunicationException ce)
            {
                logger.Fatal("An CommunicationException occurred opening the channel.", ce);
                Dispose();
            }
            catch (TimeoutException te)
            {
                logger.Fatal("An TimeoutException occurred opening the channel.", te);
                Dispose();
            }
        }
        #endregion Contructors

        public IDataMart GetDataMart()
        {
            return this.DataMart;
        }

        #region User Methods
        /// <summary>
		/// Given a username:
		///		1. retrieves the user from the DataMart
		///		2. looks for the corresponding user in CARA
		///			a. If not found, creates a new user in CARA
		///			b. If found, syncs the user
		///		3. Returns the CARA user object
		/// </summary>
		/// <param name="username">The username of the user to get</param>
		/// <returns>The user object</returns>
		public User GetUser(string username)
		{
			if (UserManager == null)
			{
				throw new MemberAccessException("The UserManager property must be assigned before calling this method");
			}

			User user = null;
			DataMartUser dmUser;
			try
			{
				logger.Info(string.Format("DataMart > GetUser > {0}", username));
				dmUser = DataMart.GetUser(username);
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving a user (username = {0}) from the Datamart:: {1}", username, e.Message);
				logger.Error(message);
				throw new DataMartException(message, e);
			}

			bool needToSave = false;

			if (dmUser != null)
			{
				user = UserManager.Find(x => x.Username.ToLower() == username.ToLower()).FirstOrDefault();

				// This user cannot be found in CARA, so create a new CARA user.
				if (user == null)
				{
					user = dmUser.CopyValuesTo(user);

					user.TimeZone = LookupManager.GetLookup<LookupTimeZone>().GetDefaultTimeZone().Id;
					try
					{
						user.SearchResultsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["userDefaultSearchResultsPerPage"]);
					}
					catch
					{
						user.SearchResultsPerPage = 25;
					}

					user.NotifyAddedProject = true;
					user.NotifyCommentPeriodEnd = true;
					user.NotifyCommentPeriodOpen = true;
					user.NotifySubmissionsReceived = true;
					user.SubmissionsRecv = 100;
					user.LetterFontSize = 10;

					// Assign the user the reader role, by default
					var readerRole = LookupManager.GetLookup<LookupRoleByName>()["Reader"];  // TODO: Remove hard coded string

                    UserSystemRole usr = new UserSystemRole { RoleId = readerRole.RoleId, UnitId = "0000" };

                    user.UserSystemRoles.Add(usr);
					UserManager.Add(user);

					needToSave = true;
				}

				// Sync the user information
				else
				{
					needToSave = SyncUser(user, dmUser, false, UserManager);
				}

				// Save the changes back to the repository
				if (needToSave)
				{
					UserManager.UnitOfWork.Save();
				}
			}

			return user;
		}

		/// <summary>
		/// Returns the queryable list of users from the DataMart and from CARA.
		/// Duplicate users are filtered out.
		/// </summary>
		/// <param name="filter">DataMart User Filter</param>
        /// <param name="allowNullParameters">Boolean indicates whether search parameters can be null</param>
		/// <returns>Queryable list of users</returns>
		public DataMartUserSearchResults FindUsers(DataMartUserFilter filter, bool allowNullParameters = false)
		{
			if (UserManager == null)
			{
				throw new MemberAccessException("The UserManager property must be assigned before calling this method");
			}

			if (!allowNullParameters && string.IsNullOrWhiteSpace(filter.FirstName) && string.IsNullOrWhiteSpace(filter.LastName))
			{
				throw new ArgumentException("firstName and lastName cannot both be null or empty at the same time");
			}

            // Final list of users
			IList<User> userList;

			// First, search the datamart with filter and paging
			DataMartUserSearchResults dmUsers;
			try
			{
				logger.Info(string.Format("DataMart > FindUsers > {0}, {1}, {2}, {3}", filter.FirstName, filter.LastName, filter.NumRows, filter.StartRow));
				dmUsers = DataMart.FindUsers(filter.FirstName, filter.LastName, filter.NumRows.ToString(), filter.StartRow.ToString());
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred searching for users (first name = {0}, last name = {1}) from the Datamart:: {2}", filter.FirstName, filter.LastName, e.Message);
				logger.Error(message);
				throw new DataMartException(message, e);
			}

            // Second, search Cara users using UserManager included in the datamart search results

            // get usernames from the datamart search results
            List<string> dmUsernames = dmUsers.DataMartUsers.Select(x => x.Username).ToList();
            // get the cara users from the datamart username list
            IQueryable<User> users = UserManager.All.Where(x => dmUsernames.Contains(x.Username));

			// Filter out users that are in both result sets
			List<DataMartUser> filteredDmUsers = dmUsers.DataMartUsers;

            userList = new List<User>();
            if (users != null && users.Count() > 0)
			{
				var usernames = (from u in users
								select u.Username).ToList();
                
                // take out users from datamart who exists in Cara
				filteredDmUsers = dmUsers.DataMartUsers.Where(x => !usernames.Contains(x.Username)).ToList();

                // add each user to avoid circular reference error in JSON
                foreach(User u in users)
                {
                    userList.Add(new User
                    {
                        UserId = u.UserId,
                        Username = u.Username,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Phone = u.Phone,
                        Email = u.Email
                    });
                }
			}

			// Add the dmUsers to the list
			foreach (DataMartUser dmu in filteredDmUsers)
			{
				userList.Add(dmu.Convert());
			}

			// Return the list sorted by last
			dmUsers.Users = userList.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ThenBy(x => x.Email).AsQueryable();

			return dmUsers;
		}

		/// <summary>
		/// Syncs all user information in CARA with the corresponding records in the DataMart
		/// </summary>
		/// <param name="batchAmount">number of users to sync at a time [defaults to 25]</param>
		public void SyncUsers(int batchAmount = 25)
		{
			if (UserManager == null)
			{
				throw new MemberAccessException("The UserManager property must be assigned before calling this method");
			}

			logger.Info("Begin: DataMartManager.SyncUsers");

			int userCount = UserManager.All.Count();
			int batches = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(userCount) / batchAmount));
			int atLeastOneIsDifferent = 0;

			for (int i=0; i < batches; i++)
			{
				logger.DebugFormat("Getting next {2} CARA users (batch {0} of {1})", i, batches, batchAmount);

				// Get the next batch of users
				var caraUsers = UserManager.All.OrderBy(x => x.UserId).Skip(i * batchAmount).Take(batchAmount).ToList();

				// Sync each user in the batch
				foreach (User u in caraUsers)
				{
					try
					{
						logger.Info(string.Format("DataMart > GetUser (in SyncUsers) > {0}", u.Username));
						var dmu = DataMart.GetUser(u.Username);
						var changesMade = SyncUser(u, dmu);
						if (changesMade)
						{
							atLeastOneIsDifferent++;
						}
					}
					catch (Exception e)
					{
						// Log the error and continue processing
						logger.Error(string.Format("An error occurred trying to sync the user with username = {0}:: {1}", u.Username, e.Message));
					}
				}
			}

			if (atLeastOneIsDifferent > 0)
			{
				logger.InfoFormat("{0} out of {1} users were updated. Persisting updates", atLeastOneIsDifferent, userCount);
				UserManager.UnitOfWork.Save();
			}

			logger.Info("End: DataMartManager.SyncUsers");
		}

		/// <summary>
		/// Syncs the user informatin from the DataMart to CARA
		/// </summary>
		/// <param name="user">CARA user</param>
		/// <param name="dataMartUser">DataMart User</param>
		/// <param name="commit">true, to persist the changes in this method</param>
		/// <param name="userManager">the UserManager to use to persist the changes</param>
		/// <returns>true, if changes were detected</returns>
		public bool SyncUser(User user, DataMartUser dataMartUser, bool commit = false, UserManager userManager = null)
		{
			if (commit == true && userManager == null)
			{
				throw new ArgumentException("When commit is true, userManager cannot be null");
			}

			logger.DebugFormat("Syncing user {0}", user.Username);

			var differencesDetected = dataMartUser.IsDifferent(user);
			if (differencesDetected)
			{
				logger.DebugFormat("{0} needs to be updated", user.Username);
				dataMartUser.CopyValuesTo(user);

				if (commit && userManager != null)
				{
					userManager.UnitOfWork.Save();
				}
			}

			return differencesDetected;
		}
		#endregion User Methods

		#region Project Methods
		/// <summary>
		/// Retrieves project information from the DataMart
		/// </summary>
		/// <param name="palsProjectId">The PALS ID of the project</param>
		/// <returns></returns>
		public Project GetProject(int palsProjectId)
		{
			// Retrieve the base project information
			DataMartProject dmProject;
			try
			{
				logger.Info(string.Format("DataMart > GetProject > {0}", palsProjectId));
				dmProject = DataMart.GetProject(palsProjectId.ToString());
                
                // Make sure the list of purposes is distinct.
                PurposeList purposeList = new PurposeList();
                foreach (var purpose in dmProject.NepaInfo.Purposes)
                {
                    if (!purposeList.Contains(purpose))
                    {
                        purposeList.Add(purpose);
                    }
                }
                dmProject.NepaInfo.Purposes = purposeList;
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving a project (PALS Project ID = {0}) from the Datamart:: {1}", palsProjectId, e.Message);
				logger.Error(message);
				throw new DataMartException(message, e);
			}

			// Retrieve the project activities
			ProjectActivityList activities;
			try
			{
				logger.Info(string.Format("DataMart > GetProjectActivities > {0}", palsProjectId));
				activities = DataMart.GetProjectActivities(palsProjectId.ToString());
				if (activities != null)
				{
					dmProject.ActivityCodes = activities.Select(x => x.ActivityId).ToList();
				}
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving a project activities (PALS Project ID = {0}) from the Datamart:: {1}", palsProjectId, e.Message);
				logger.Info(message);
				dmProject.ActivityCodes = new List<string>();
			}

			// Retrieve the project location
			try
			{
				logger.Info(string.Format("DataMart > GetProjectLocations > {0}", palsProjectId));
				dmProject.Location = DataMart.GetProjectLocations(palsProjectId.ToString());
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving a project location (PALS Project ID = {0}) from the Datamart:: {1}", palsProjectId, e.Message);
				logger.Info(message);
				dmProject.Location = new Location();
			}

			// Retrieve the project documents
			try
			{
				logger.Info(string.Format("DataMart > GetProjectDocuments > {0}", palsProjectId));
				var dmdDocumentList = GetProjectDocuments(palsProjectId);
				if (dmdDocumentList != null)
				{
					dmProject.Documents = dmdDocumentList.Select(x =>
						new ProjectDocument
						{
							DmdId = x.DmdDocumentId, PrimarySequence = int.Parse(x.PrimarySequence), Sequence = int.Parse(x.Sequence)
						}
					).ToList();
				}
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving a project documents (PALS Project ID = {0}) from the Datamart:: {1}", palsProjectId, e.Message);
				logger.Error(message);
				throw new DataMartException(message, e);
			}

			return dmProject.ToProject();
		}

		/// <summary>
		/// Returns a list of projects from the DataMart that match
		/// the parameters defined in the DataMartProjectFilter
		/// </summary>
		/// <param name="filter"></param>
		/// <returns></returns>
		public DataMartProjectSearchResults FindProjects(DataMartProjectFilter filter)
		{
			if (ProjectManager == null)
			{
				throw new MemberAccessException("The ProjectManager property must be assigned before calling this method");
			}

			string projectId = filter.ProjectId;
			string partialName = filter.PartialName;
			string statusIds = filter.StatusIdsAsCommaDelimitedString;
			string unitcodes = filter.UnitCodesAsCommaDelimitedString;
			string numRows = filter.NumRows.ToString();
			string startRow = filter.StartRow.ToString();

			DataMartProjectSearchResults dmProjectsResults;
			try
			{
				logger.Info(string.Format("DataMart > FindProjects > {0}, {1}, {2}, {3}, {4}, {5}", projectId, partialName, statusIds, unitcodes, numRows, startRow));
				dmProjectsResults = DataMart.FindProjects(projectId, partialName, statusIds, unitcodes, numRows, startRow);
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred searching for projects (projectId = {0}, name = {1}, statusIds = {2}, units = {3}) from the Datamart:: {4}",
					filter.ProjectId, filter.PartialName, filter.StatusIdsAsCommaDelimitedString, filter.UnitCodesAsCommaDelimitedString, e.Message);
				logger.Error(message);
				throw new DataMartException(message, e);
			}

			var listOfProjectIds = dmProjectsResults.Projects.Select(p => p.ProjectId).ToList();

			// Find existing projects in CARA
			var existingProjects = ProjectManager.All.Where(x => listOfProjectIds.Contains(x.ProjectNumber));

			foreach (var p in existingProjects)
			{
			    dmProjectsResults.Projects.Where(dmp => dmp.ProjectId == p.ProjectNumber).First().ExistsInCara = true;
			}

			foreach (var p in dmProjectsResults.Projects)
			{
				MilestoneList milestones;
				try
				{
					logger.Info(string.Format("DataMart > GetProjectMilestones > {0}", p.ProjectId));
					milestones = DataMart.GetProjectMilestones(p.ProjectId.ToString());
					var estDecisionDate = milestones.FirstOrDefault(x => x.MilestoneType == Milestone.TYPE_DECISION_DATE && x.Status == Milestone.STATUS_ESTIMATED);
					if (estDecisionDate != null)
					{
						p.EstimatedDecisionDate = estDecisionDate.DateString;
					}
				}
				catch (Exception e)
				{
					string message = string.Format("An error occurred getting project milestones (projectId = {0}) from the Datamart:: {1}", filter.ProjectId, e.Message);
					logger.Info(message);
					p.EstimatedDecisionDate = "Not Specified";
				}

				ProjectActivityList activities;
				try
				{
					logger.Info(string.Format("DataMart > GetProjectActivities > {0}", p.ProjectId));
					activities = DataMart.GetProjectActivities(p.ProjectId.ToString());
					p.ActivityCodes = activities.Select(x => x.ActivityId).ToList();
				}
				catch (Exception e)
				{
					string message = string.Format("An error occurred retrieving a project activities (PALS Project ID = {0}) from the Datamart:: {1}", filter.ProjectId, e.Message);
					logger.Info(message);
					p.ActivityCodes = new List<string>();
				}
			}

			return dmProjectsResults;
		}

		/// <summary>
		/// Associates a CARA project to a PALS project in the Datamart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="palsProjectId"></param>
		public bool LinkProject(int caraProjectId, int palsProjectId)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > LinkProject > caraProjectId: {0}, palsId: {1}", caraProjectId, palsProjectId));
				DataMart.LinkProject(new CaraLinkProject(caraProjectId, palsProjectId));
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with LinkProject: CARA Project ID: {0}, PALS ProjectId: {1}:: {2}", caraProjectId, palsProjectId, e.Message));
			}

			return success;
		}

		/// <summary>
		/// Disassociates a CARA project from a PALS project in the Datamart
		/// </summary>
		/// <param name="caraProjectId"></param>
		public bool UnlinkProject(int caraProjectId, int palsProjectId)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > UnlinkProject > caraProjectId: {0}", caraProjectId));
				DataMart.UnlinkProject(new CaraUnlinkProject(caraProjectId));
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with UnlinkProject: CARA Project ID: {0}, PALS ProjectId: {1}:: {2}", caraProjectId, palsProjectId, e.Message));
			}

			return success;
		}


		public void SyncProjects(int batchAmount = 25)
		{
			if (ProjectManager == null)
			{
				throw new MemberAccessException("The ProjectManager property must be assigned before calling this method");
			}

			logger.Info("Begin: DataMartManager.SyncProjects");

			int projectCount = ProjectManager.All.Count();
			int batches = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(projectCount) / batchAmount));
			int atLeastOneIsDifferent = 0;

            for (int i = 0; i < batches; i++)
            {
                logger.DebugFormat("Getting next {2} CARA projects (batch {0} of {1})", i, batches, batchAmount);

                // Get the next batch of projects
                var caraProjects = ProjectManager.All
                    .Where(x => x.ProjectTypeId == 1) // 1 == PALS/NEPA project
                    .OrderBy(x => x.ProjectId)
                    .Skip(i * batchAmount)
                    .Take(batchAmount);

                // Sync each projects in the batch
                foreach (Project p in caraProjects)
                {
                    try
                    {
                        var dmp = GetProject(Convert.ToInt32(p.ProjectNumber));
                        var changesMade = SyncProject(p, dmp);
                        if (changesMade)
                        {
                            atLeastOneIsDifferent++;
                        }
                    }
                    catch (Exception e)
                    {
                        // Log the error and continue processing
                        logger.Error(string.Format("An error occurred trying to sync the project with PALS ID = {0}:: {1}", p.ProjectNumber, e.Message));
                    }
                }

                if (atLeastOneIsDifferent > 0)
                {
                    logger.InfoFormat("{0} out of {1} projects in batch {2} of {3} were updated. Persisting updates", 
                        atLeastOneIsDifferent, 
                        caraProjects.Count(), 
                        i + 1, 
                        batches);

                    try
                    {
                        ProjectManager.UnitOfWork.Save();
                    }
                    catch (Exception ex)
                    {
                        // Ignore.  Error is logged deeper.
                    }
                    atLeastOneIsDifferent = 0;  // reset for the next batch
                }
            }

			logger.Info("End: DataMartManager.SyncProjects");
		}

		public bool SyncProject(Project project, Project dataMartProject, bool commit = false, ProjectManager projectManager = null)
		{
			if (commit == true && projectManager == null)
			{
				throw new ArgumentException("When commit is true, projectManager cannot be null");
			}

			logger.DebugFormat("Syncing project {0} with Project Number {1}", project.ProjectId, project.ProjectNumber);

			var differencesDetected = dataMartProject.IsDifferent(project);
			if (differencesDetected)
			{
				logger.InfoFormat("Project {0} needs to be updated", project.ProjectId);
				dataMartProject.CopyValuesTo(project);

				if (commit && projectManager != null)
				{
					projectManager.UnitOfWork.Save();
				}
			}

			return differencesDetected;
		}

		public IList<DmdDocument> GetProjectDocuments(int palsProjectId)
		{
			if (ProjectManager == null)
			{
				throw new MemberAccessException("The ProjectManager property must be assigned before calling this method");
			}

			// Get list of documents from the Datamart
			DmdDocumentList documents = null;
			try
			{
				logger.Info(string.Format("DataMart > GetProjectDocuments > palsId: {0}", palsProjectId));
				documents = DataMart.GetProjectDocuments(palsProjectId.ToString());
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving project documents: PALS ProjectId: {0}:: {1}", palsProjectId, e.Message);
				logger.Info(message);
			}

			return documents;
		}

        public MilestoneList GetProjectMilestones(int palsProjectId)
        {
            MilestoneList milestones = null;
			try
			{
				logger.Info(string.Format("DataMart > GetProjectMilestones > palsId: {0}", palsProjectId));
				milestones = DataMart.GetProjectMilestones(palsProjectId.ToString());
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving project milestones: PALS ProjectId: {0}:: {1}", palsProjectId, e.Message);
				logger.Info(message);
			}

			return milestones;
        }
		#endregion Project Methods

		#region Phase Methods
		/// <summary>
		/// Associates a CARA Phase with a CARA project in the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="phase"></param>
		public bool LinkPhase(int caraProjectId, Phase phase)
		{
			var success = true;
			var dmli = new DataMartLetterInfo
			{
				PhaseId = phase.PhaseId,
				Name = phase.PhaseTypeDesc,
				StartDate = phase.CommentStart.ToString("yyyy-MM-dd"),
				FinishDate = phase.CommentEnd.ToString("yyyy-MM-dd"),
			};

			try
			{
				// The entry with the letter counts is how the DataMart associates a phase with a project.
				// This should be done when the phase is created, so for this call, the DataMartLetterInfo class
				// will be used with all of the letter counts defaulted to zero.
				logger.Info(string.Format("DataMart > LinkPhase > caraProjectId: {0}, phaseId: {1}", caraProjectId, phase.PhaseId));
				DataMart.AddPhaseLetterInfo(caraProjectId.ToString(), dmli);
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with LinkPhase: CARA Project ID: {0}, Phase ID: {1}:: {2}", caraProjectId, phase.PhaseId, e.Message));
			}

			return success;
		}

		/// <summary>
		/// Breaks the association between a CARA Phase and a CARA Project in the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="phaseId"></param>
        /// <param name="documents"></param>
		public bool UnlinkPhase(int caraProjectId, int phaseId, ICollection<ProjectDocument> documents)
		{
			var success = true;
			try
			{
				// unlink the phase documents
				if (documents != null)
				{
					foreach (ProjectDocument doc in documents.Where(x => x.PhaseId == phaseId))
					{
						UnlinkPhaseDocuments(caraProjectId, phaseId, doc.DmdId);
					}
				}

				// delete the phase letter info
				logger.Info(string.Format("DataMart > UnlinkPhase > caraProjectId: {0}, phaseId: {1}", caraProjectId, phaseId));
				DataMart.DeletePhaseLetterInfo(caraProjectId.ToString(), phaseId.ToString());
			}
			catch (Exception e)
			{
				success = false;
				string dmdIdList = "No Documents";
				if (documents != null)
				{
					var dmdIds = documents.Select(x => x.DmdId);
					dmdIdList = string.Join(", ", dmdIds);
				}
				logger.Error(string.Format("An error occurred with UnlinkPhase: CARA Project ID: {0}, Phase ID: {1}, DMD IDs: {2}:: {3}", caraProjectId, phaseId, dmdIdList, e.Message));
			}

			return success;
		}

		/// <summary>
		/// Sends the letter count information to the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="letterInfo"></param>
		public bool SendPhaseLetterInfo(int caraProjectId, DataMartLetterInfo letterInfo)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > SendPhaseLetterInfo > caraProjectId: {0}, phaseId: {1}", caraProjectId, letterInfo.PhaseId));
				DataMart.UpdatePhaseLetterInfo(caraProjectId.ToString(), letterInfo.PhaseId.ToString(), letterInfo);
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with SendPhaseLetterInfo: CARA Project ID: {0}:: {1}", caraProjectId, e.Message));
			}

			return success;
		}
		#endregion Phase Methods

		#region Phase Document Methods
		/// <summary>
		/// Associates a DMD Document with a CARA comment phase/period
		/// </summary>
		/// <param name="caraPhaseId"></param>
		/// <param name="palsProjectId"></param>
		/// <param name="documentIds"></param>
		/// <returns></returns>
		public bool LinkPhaseDocuments(int caraProjectId, int phaseId, string dmdDocumentId)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > LinkPhaseDocument > caraProjectId: {0}, phaseId: {1}, dmdId: {2}", caraProjectId, phaseId, dmdDocumentId));
				DataMart.LinkPhaseDocument(new CaraLinkDocument(caraProjectId, phaseId, dmdDocumentId));
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with LinkPhaseDocuments: CARA Project ID: {0}, Phase ID: {1}, DMD ID: {2}:: {3}", caraProjectId, phaseId, dmdDocumentId, e.Message));
			}

			return success;
		}

		/// <summary>
		/// Diassociates a DMD Document with a CARA comment phase/period
		/// </summary>
		/// <param name="caraPhaseId"></param>
		/// <param name="palsProjectId"></param>
		/// <param name="documentIds"></param>
		public bool UnlinkPhaseDocuments(int caraProjectId, int phaseId, string dmdDocumentId)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > UnlinkPhaseDocument > caraProjectId: {0}, phaseId: {1}, dmdId: {2}", caraProjectId, phaseId, dmdDocumentId));
				DataMart.UnlinkPhaseDocument(new CaraUnlinkDocument(caraProjectId, phaseId, dmdDocumentId));
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with UnlinkPhaseDocuments: CARA Project ID: {0}, Phase ID: {1}, DMD ID: {2}:: {3}", caraProjectId, phaseId, dmdDocumentId, e.Message));
			}

			return success;
		}
		#endregion Phase-Document Methods

        #region Objection Methods
		public Objection GetObjection(int objectionId)
		{
			Objection objection;
			try
			{
				logger.Info(string.Format("DataMart > GetObjection > {0}", objectionId));
				objection = DataMart.GetObjection(objectionId.ToString());
			}
			catch (Exception e)
			{
				string message = string.Format("An error occurred retrieving an objection (Objection ID = {0}) from the Datamart:: {1}", objectionId, e.Message);
				logger.Warn(message);
                objection = null;
			}

            return objection;
		}

		public bool AddObjection(Objection objection)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > AddObjection > objectionId: {0}", objection.ObjectionId));
				DataMart.AddObjection(objection);
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with AddObjection: Objection ID: {0}:: {1}", objection.ObjectionId, e.Message));
			}

			return success;
		}

		public bool UpdateObjection(Objection objection)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > UpdateObjection > objectionId: {0}", objection.ObjectionId));
				DataMart.UpdateObjection(objection.ObjectionId.ToString(), objection);
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with UpdateObjection: Objection ID: {0}:: {1}", objection.ObjectionId, e.Message));
			}

			return success;
		}

		public bool DeleteObjection(int objectionId)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > DeleteObjection > objectionId: {0}", objectionId));
				DataMart.DeleteObjection(objectionId.ToString());
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with DeleteObjection: objectionId: {0}:: {1}", objectionId, e.Message));
			}

			return success;
		}
		#endregion

		#region Mailing List Methods
		public bool AddSubscriber(int caraProjectId, Author author)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > AddSubscriber > caraProjectId: {0}, authorId: {1}", caraProjectId, author.AuthorId));
				DataMart.AddSubscriber(caraProjectId.ToString(), new Subscriber(author));
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with AddSubscriber: CARA Project ID: {0}, Author ID: {1}:: {2}", caraProjectId, author.AuthorId, e.Message));
			}

			return success;
		}

		public bool UpdateSubscriber(int caraProjectId, Author author)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > UpdateSubscriber > caraProjectId: {0}, authorId: {1}", caraProjectId, author.AuthorId));
				DataMart.UpdateSubscriber(caraProjectId.ToString(), author.AuthorId.ToString(), new Subscriber(author));
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with UpdateSubscriber: CARA Project ID: {0}, Author ID: {1}:: {2}", caraProjectId, author.AuthorId, e.Message));
			}

			return success;
		}

		public bool DeleteSubscriber(int caraProjectId, int authorId)
		{
			var success = true;
			try
			{
				logger.Info(string.Format("DataMart > DeleteSubscriber > caraProjectId: {0}, authorId: {1}", caraProjectId, authorId));
				DataMart.DeleteSubscriber(caraProjectId.ToString(), authorId.ToString());
			}
			catch (Exception e)
			{
				success = false;
				logger.Error(string.Format("An error occurred with DeleteSubscriber: CARA Project ID: {0}, Author ID: {1}:: {2}", caraProjectId, authorId, e.Message));
			}

			return success;
		}
		#endregion

		#region Public Comment Page Methods
		public WebCommentPageInfo GetProjectCommentPage(string palsProjectId)
		{
			if (WebInfoManager == null)
			{
				throw new MemberAccessException("The WebInfoManager property must be assigned before calling this method");
			}

            if (ProjectManager == null)
            {
                throw new MemberAccessException("The ProjectManager property must be assigned before calling this method");
            }

			// First see if the information is stored locally
			var project = ProjectManager.Find(x => x.ProjectNumber == palsProjectId).FirstOrDefault();
			var wcpi = WebInfoManager.GetByPalsId(Convert.ToInt32(palsProjectId));

			// Public Comment page existed before project existed in CARA
			if (project != null && wcpi != null && wcpi.ProjectId == null)
			{
				wcpi.ProjectId = project.ProjectId;
				WebInfoManager.UnitOfWork.Save();
			}

			// If Public Comment does not exist yet,
			// get it from the Datamart and save it in the database
			if (wcpi == null)
			{
                //dglaubitz: This is broken for at least one project id
                try
				{
					wcpi = GetProjectCommentPageFromDataMart(palsProjectId);

					if (project != null)
					{
						wcpi.ProjectId = project.ProjectId;
						wcpi.CreatedDate = DateTime.UtcNow;
						wcpi.LastUpdatedDate = (DateTime)wcpi.CreatedDate;
					}

					WebInfoManager.Add(wcpi);
					WebInfoManager.UnitOfWork.Save();
				}
				catch (Exception e)
				{
					string message = string.Format("An error occurred getting data for the public comment page (Project ID: {0}) from the Datamart:: {1}", palsProjectId, e.Message);
					logger.Error(message);
					throw new DataMartException(message, e);
				}
			}

			return wcpi;
		}

		/// <summary>
		/// Retrieves the Project and Unit information from the DataMart and combines
		/// them into a WebCommentPageInfo entity
		/// </summary>
		/// <param name="palsProjectId"></param>
		/// <returns></returns>
		private WebCommentPageInfo GetProjectCommentPageFromDataMart(string palsProjectId)
		{
			WebCommentPageInfo wcpi = null;

			logger.Info(string.Format("DataMart > GetProjectCommentPage > GetProject > palsId: {0}", palsProjectId));
			var dmProject = DataMart.GetProject(palsProjectId);

			logger.Info(string.Format("DataMart > GetProjectCommentPage > GetUnitInfo > unitcode: {0}", dmProject.LeadManagementUnit));
			var unitInfo = DataMart.GetUnitInfo(dmProject.LeadManagementUnit);

			wcpi = dmProject.ToWebCommentPageInfo(unitInfo);

			return wcpi;
		}

		public void SyncWebCommentPageInfos(int batchAmount = 25)
		{
			if (ProjectManager == null)
			{
				throw new MemberAccessException("The ProjectManager property must be assigned before calling this method");
			}

			logger.Info("Begin: DataMartManager.SyncWebCommentPageInfos");

			int projectCount = WebInfoManager.All.Count();
			int batches = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(projectCount) / batchAmount));
			int atLeastOneIsDifferent = 0;

			for (int i = 0; i < batches; i++)
			{
				logger.DebugFormat("Getting next {2} CARA project comment page (batch {0} of {1})", i+1, batches, batchAmount);

				// Get the next batch of users
				var caraProjects = WebInfoManager.All.OrderBy(x => x.PalsId).Skip(i * batchAmount).Take(batchAmount);

				// Sync each user in the batch
				foreach (WebCommentPageInfo p in caraProjects)
				{
					try
					{
						var dmp = GetProjectCommentPageFromDataMart(p.PalsId.ToString());

						// See if the project has been added to CARA,
						// after "p" has been created.  If so,
						// assign the project Id to "dmp"
						if (!p.ProjectId.HasValue)
						{
							string palsIdString = p.PalsId.ToString();
							var caraProject = ProjectManager.All.SingleOrDefault(x => x.ProjectNumber == palsIdString);
							if (caraProject != null)
							{
								dmp.ProjectId = caraProject.ProjectId;
							}
						}
						else
						{
							dmp.ProjectId = p.ProjectId;
						}

						var changesMade = SyncWebCommentPageInfo(p, dmp);
						if (changesMade)
						{
							p.LastUpdatedDate = DateTime.UtcNow;
							atLeastOneIsDifferent++;
						}
					}
					catch (Exception e)
					{
						// Log the error and continue processing
						logger.Error(string.Format("An error occurred trying to sync the public comment page info with PALS ID = {0}:: {1}", p.PalsId, e.Message));
					}
				}

			    if (atLeastOneIsDifferent > 0)
			    {
    				logger.InfoFormat("Found {0} projects in batch {1} of {2} that need updating. Persisting updates.", atLeastOneIsDifferent, i+1, batches);
				    WebInfoManager.UnitOfWork.Save();

                    // Reset the atLeastOneIsDifferentCount for the next batch
                    atLeastOneIsDifferent = 0;
			    }
			}

			logger.Info("End: DataMartManager.SyncWebCommentPageInfos");
		}

		public bool SyncWebCommentPageInfo(WebCommentPageInfo project, WebCommentPageInfo dataMartProject, bool commit = false, WebCommentPageInfoManager wcpiManager = null)
		{
			if (commit == true && wcpiManager == null)
			{
				throw new ArgumentException("When commit is true, wcpiManager cannot be null");
			}

			logger.DebugFormat("Syncing Web Comment Page with Project Number {0}", project.PalsId);

			var differencesDetected = dataMartProject.IsDifferent(project);
			if (differencesDetected)
			{
				logger.InfoFormat("Project {0} needs to be updated", project.PalsId);
				dataMartProject.CopyValuesTo(project);

				if (commit && wcpiManager != null)
				{
					project.LastUpdatedDate = DateTime.UtcNow;
					wcpiManager.UnitOfWork.Save();
				}
			}

			return differencesDetected;
		}
		#endregion

		#region Public Reading Room Methods
		/// <summary>
		/// Sets the reading room status in the datamart for a project.  This needs to be called after the
		/// reading room status of a phase is updated.
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="isActive"></param>
		/// <returns></returns>
		public bool SetReadingRoomStatus(int caraProjectId, bool isActive)
		{
			var success = true;
			var project = ProjectManager.Get(caraProjectId);

            if (project.IsPalsProject)
            {
                var palsProjectId = project.ProjectNumber;

                try
                {
                    if (isActive)
                    {
                        logger.Info(string.Format("DataMart > ActivateReadingRoom > palsId: {0}", palsProjectId));
                        DataMart.ActivateReadingRoom(palsProjectId);
                    }
                    else
                    {
                        logger.Info(string.Format("DataMart > DeactivateReadingRoom > palsId: {0}", palsProjectId));
                        DataMart.DeactivateReadingRoom(palsProjectId);
                    }
                }
                catch (Exception e)
                {
                    string message = string.Format("An error occurred updating project reading room status: PALS ProjectId: {0}:: {1}", palsProjectId, e.Message);
                    logger.Info(message);
                    success = false;
                }
            }

			return success;
		}
		#endregion

        #region Implement IDispoable interface
        public void Dispose()
        {
            try
            {
                if (DataMart != null)
                {
                    if (((IClientChannel)DataMart).State != CommunicationState.Faulted)
                    {
                        ((IClientChannel)DataMart).Close();
                    }
                    else
                    {
                        ((IClientChannel)DataMart).Abort();
                    }
                }
            }
            catch (CommunicationException)
            {
                ((IClientChannel)DataMart).Abort();
            }
            catch (TimeoutException)
            {
                ((IClientChannel)DataMart).Abort();
            }
            catch (Exception)
            {
                ((IClientChannel)DataMart).Abort();
                throw;
            }
            finally
            {
                DataMart = null;
            }
        }
        #endregion Implement IDispoable interface
    }
}
