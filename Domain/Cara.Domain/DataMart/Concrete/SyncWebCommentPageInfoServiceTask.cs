﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.DataMart
{
	public class SyncWebCommentPageInfoServiceTask : IServiceTask
	{
		public string Name
		{
			get { return "DataMart Web Comment Page Info Sync"; }
		}

		public void RunServiceTask()
		{
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.ProjectManager = projectManager;
                dmManager.WebInfoManager = ManagerFactory.CreateInstance<WebCommentPageInfoManager>(); // Don't need to dispose because it shares the unitofwork
                dmManager.WebInfoManager.UnitOfWork = dmManager.ProjectManager.UnitOfWork;

                dmManager.SyncWebCommentPageInfos(100);
                dmManager.Dispose();
            }
		}
	}
}
