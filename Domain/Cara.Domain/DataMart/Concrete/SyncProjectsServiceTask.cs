﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.DataMart
{
	public class SyncProjectsServiceTask : IServiceTask
	{
		public string Name
		{
			get { return "DataMart Project Sync"; }
		}

		public void RunServiceTask()
		{
            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.ProjectManager = projectManager;

                dmManager.SyncProjects();
                dmManager.Dispose();
            }
		}
	}
}
