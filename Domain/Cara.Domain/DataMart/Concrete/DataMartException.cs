﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.DataMart
{
	public class DataMartException : Exception
	{
		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public DataMartException()
			: base()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public DataMartException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public DataMartException(string message, Exception innerException) :
			base(message, innerException)
		{
		}
		#endregion        
	}
}
