﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.DataMart
{
	public static class DataMartExtensions
	{
		#region Project Methods
		public static void CopyValuesTo(this Project fromProject, Project toProject)
		{
			// FYI: 'this' was a DataMartProject

			toProject.UnitId = fromProject.UnitId;	// this.LeadManagementUnit;
			toProject.ProjectStatusId = fromProject.ProjectStatusId;  // this.NepaInfo.ProjectStatus;
			toProject.Name = fromProject.Name; //this.Name;
			//project.CreateDate = this.CreationDate;
			toProject.ProjectTypeId = fromProject.ProjectTypeId; //LookupManager.GetLookup<LookupProjectType>().First(x => x.Name.ToUpper() == "NEPA").ProjectTypeId;
			toProject.Description = fromProject.Description; //this.Description;
			toProject.ProjectNumber = fromProject.ProjectNumber;  //this.ProjectId;
			toProject.AnalysisTypeId = fromProject.AnalysisTypeId; //this.NepaInfo.AnalysisType;
			toProject.SOPAUser = fromProject.SOPAUser; //this.NepaInfo.SopaContactName;
			toProject.LastUpdated = fromProject.LastUpdated; //this.LastUpdateDate;
			//project.PalsProjectManager1 = this.ProjectManagers.PrimaryManager.FullName;
			toProject.ProjectDocumentId = fromProject.ProjectDocumentId;
			toProject.CommentRuleId = fromProject.CommentRuleId; //Convert.ToInt32(this.CommentReg);

			if (AreArraysDifferent<int>(
				fromProject.ProjectActivities.Select(x => x.ActivityId).ToList(),
				toProject.ProjectActivities.Select(x => x.ActivityId).ToList()))
			{
				toProject.ProjectActivities.Clear();
				foreach (var item in fromProject.ProjectActivities)
				{
					toProject.ProjectActivities.Add(item);
				}
			}

			if (AreArraysDifferent<int>(
				fromProject.ProjectPurposes.Select(x => x.PurposeId).ToList(),
				toProject.ProjectPurposes.Select(x => x.PurposeId).ToList()))
			{
				toProject.ProjectPurposes.Clear();
				foreach (var item in fromProject.ProjectPurposes)
				{
					toProject.ProjectPurposes.Add(item);
				}
			}

			if (AreArraysDifferent<string>(
				fromProject.Locations.Select(x => x.AreaId).ToList(),
				toProject.Locations.Select(x => x.AreaId).ToList()))
			{
				toProject.Locations.Clear();
				foreach (var item in fromProject.Locations)
				{
					// copying the item to a new object is a bit of a hack.
					// for some reason, when I do:  toProject.Locations.Add(item)
					// it gets removed from fromProject.Locations, which causes
					// an exception when the loop tries to go to the next item.
					var location = new Domain.Location
					{
						ProjectId = toProject.ProjectId,
						AreaId = item.AreaId,
						AreaType = item.AreaType
					};
					toProject.Locations.Add(location);
				}
			}

			if (AreArraysDifferent<string>(
				fromProject.ProjectDocuments.Select(x => x.DmdId).ToList(),
				toProject.ProjectDocuments.Select(x => x.DmdId).ToList()) || DidDocumentOrderChange(fromProject.ProjectDocuments, toProject.ProjectDocuments))
			{
				// Maintain previous projectdocument phase associations
				var phaseDocuments = toProject.ProjectDocuments.Select(x => new 
                    { 
                        DmdId = x.DmdId, 
                        PhaseId = x.PhaseId, 
                        PrimarySequence = x.PrimarySequence,
                        Sequence = x.Sequence
                    }).ToList();

				toProject.ProjectDocuments.Clear();
				foreach (var item in fromProject.ProjectDocuments)
				{
					if (phaseDocuments.Count > 0)
					{
						var phaseDocument = phaseDocuments.FirstOrDefault(x => x.DmdId == item.DmdId && x.PhaseId.HasValue);
						if (phaseDocument != null)
						{
							item.PhaseId = phaseDocument.PhaseId;
						}
					}

					toProject.ProjectDocuments.Add(item);
				}
			}
		}

		public static bool IsDifferent(this Project origProject, Project project)
		{
			var different = false;

			if (project.UnitId != origProject.UnitId)
			{
				different = true;
			}
			else if (project.ProjectStatusId != project.ProjectStatusId)
			{
				different = true;
			}
			else if (project.Name != origProject.Name)
			{
				different = true;
			}
			//else if (project.CreateDate != dmProject.CreateDate)
			//{
			//    different = true;
			//}
			else if (project.Description != origProject.Description)
			{
				different = true;
			}
			else if (project.ProjectNumber != origProject.ProjectNumber)
			{
				different = true;
			}
			else if (project.AnalysisTypeId != origProject.AnalysisTypeId)
			{
				different = true;
			}
			else if (project.SOPAUser != origProject.SOPAUser)
			{
				different = true;
			}
			else if (project.LastUpdated != origProject.LastUpdated)
			{
				different = true;
			}
			else if (project.CommentRuleId != origProject.CommentRuleId)
			{
				different = true;
			}
			else if (project.ProjectDocumentId != origProject.ProjectDocumentId)
			{
				different = true;
			}
			//else if (project.PalsProjectManager1 != dmProject.PalsProjectManager1)
			//{
			//    different = true;
			//}
			else if (AreArraysDifferent<string>(
				project.Locations.Select(x => x.AreaId).ToList(),
				origProject.Locations.Select(x => x.AreaId).ToList()))
			{
				different = true;
			}
			else if (AreArraysDifferent<int>(
				project.ProjectActivities.Select(x => x.ActivityId).ToList(),
				origProject.ProjectActivities.Select(x => x.ActivityId).ToList()))
			{
				different = true;
			}
			else if (AreArraysDifferent<int>(
				project.ProjectPurposes.Select(x => x.PurposeId).ToList(),
				origProject.ProjectPurposes.Select(x => x.PurposeId).ToList()))
			{
				different = true;
			}
			else if (AreArraysDifferent<string>(
				project.ProjectDocuments.Select(x => x.DmdId).ToList(),
				origProject.ProjectDocuments.Select(x => x.DmdId).ToList()))
			{
				different = true;
			}
            else if (DidDocumentOrderChange(project.ProjectDocuments, origProject.ProjectDocuments))
            {
                different = true;
            }
            

			return different;
		}
		#endregion Project Methods
		
		#region WebCommentPageInfo Methods
		public static void CopyValuesTo(this WebCommentPageInfo fromProject, WebCommentPageInfo toProject)
		{
			toProject.CommentEmail = fromProject.CommentEmail;
			toProject.CommentRuleId = fromProject.CommentRuleId;
			toProject.ContactName = fromProject.ContactName;
			toProject.MailAddressCity = fromProject.MailAddressCity;
			toProject.MailAddressState = fromProject.MailAddressState;
			toProject.MailAddressStreet1 = fromProject.MailAddressStreet1;
			toProject.MailAddressStreet2 = fromProject.MailAddressStreet2;
			toProject.MailAddressZip = fromProject.MailAddressZip;
			toProject.Name = fromProject.Name;
			toProject.NewspaperOfRecord = fromProject.NewspaperOfRecord;
			toProject.NewspaperOfRecordUrl = fromProject.NewspaperOfRecordUrl;
			toProject.ProjectId = fromProject.ProjectId;
		}

        //CARA 721:- Added new check for column ProjectUrl
		public static bool IsDifferent(this WebCommentPageInfo origPageInfo, WebCommentPageInfo pageInfo)
		{
			var different = false;

			if (pageInfo.CommentEmail != origPageInfo.CommentEmail)
			{
				different = true;
			}
			else if (pageInfo.CommentRuleId != origPageInfo.CommentRuleId)
			{
				different = true;
			}
			else if (pageInfo.ContactName != origPageInfo.ContactName)
			{
				different = true;
			}
			else if (pageInfo.FullAddress != origPageInfo.FullAddress)
			{
				different = true;
			}
			else if (pageInfo.Name != origPageInfo.Name)
			{
				different = true;
			}
			else if (pageInfo.NewspaperOfRecord != origPageInfo.NewspaperOfRecord)
			{
				different = true;
			}
			else if (pageInfo.NewspaperOfRecordUrl != origPageInfo.NewspaperOfRecordUrl)
			{
				different = true;
			}
			else if (pageInfo.ProjectId.HasValue != origPageInfo.ProjectId.HasValue)
			{
				different = true;
			}
            else if (pageInfo.ProjectUrl != origPageInfo.ProjectUrl)
            {
                different = true;
            }
            else if (pageInfo.UnitId != origPageInfo.UnitId)
            {
                different = true;
            }

			return different;
		}
		#endregion WebCommentPageInfo Methods


		/// <summary>
		/// Compares the contents of two arrays to make sure they contain the same values
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="a1"></param>
		/// <param name="a2"></param>
		/// <returns></returns>
		private static bool AreArraysDifferent<T>(List<T> a1, List<T> a2) where T : IComparable
		{
			a1.Sort();
			a2.Sort();

			var different = false;
			if (a1.Count != a2.Count)
			{
				different = true;
			}
			else
			{
				for (int i = 0; i < a1.Count; i++)
				{
					if (!a1[i].Equals(a2[i]))
					{
						different = true;
						break;
					}
				}
			}

			return different;
		}

        private static bool DidDocumentOrderChange(ICollection<ProjectDocument> pd1, ICollection<ProjectDocument> pd2)
        {
            var different = false;

            foreach (var doc1 in pd1)
            {
                var doc2 = pd2.FirstOrDefault(x => x.DmdId == doc1.DmdId);
                if (doc2 != null && (doc2.PrimarySequence != doc1.PrimarySequence || doc2.Sequence != doc1.Sequence))
                {
                    different = true;
                    break;
                }
            }

            return different;
        }
	}
}
