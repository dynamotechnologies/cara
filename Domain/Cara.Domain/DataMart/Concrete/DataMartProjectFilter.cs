﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain.DataMart
{
	public class DataMartProjectFilter : IFilter
	{
		private IList<int> _statusIds;
		private IList<string> _unitCodes;
		private int _numRows = 100;
		private int _startRow = 0;

		public string PartialName { get; set; }
		public string ProjectId { get; set; }
		public int NumRows 
		{
			get { return _numRows; }
			set { _numRows = value; }
		}

		public int StartRow 
		{
			get { return _startRow; }
			set { _startRow = value; }
		}

		public IList<int> StatusIds
		{
			get
			{
				if (_statusIds == null)
				{
					_statusIds = new List<int>();
				}

				return _statusIds;
			}
		}

		public IList<string> UnitCodes
		{
			get
			{
				if (_unitCodes == null)
				{
					_unitCodes = new List<string>();
				}

				return _unitCodes;
			}

			set
			{
				_unitCodes = value;
			}
		}

		public string StatusIdsAsCommaDelimitedString
		{
			get
			{
				StringBuilder sb = new StringBuilder();

				foreach (int id in StatusIds)
				{
					sb.Append(id);
					sb.Append(",");
				}

				if (StatusIds.Count > 0)
				{
					// Remove the last comma
					sb = sb.Remove(sb.Length - 1, 1);
				}

				return sb.ToString();
			}
		}

		public string UnitCodesAsCommaDelimitedString
		{
			get
			{
				StringBuilder sb = new StringBuilder();

                string strippedCode;
				foreach (string code in UnitCodes)
				{
                    // Remove trailing 0s
                    strippedCode = code;
                    while (strippedCode.EndsWith("00"))
                    {
                        strippedCode = Unit.GetParentUnitId(strippedCode);
                    }

					sb.Append(strippedCode);
					sb.Append(",");
				}

				if (UnitCodes.Count > 0)
				{
					// Remove the last comma
					sb = sb.Remove(sb.Length - 1, 1);
				}

				return sb.ToString();
			}
		}

        public bool IsObjection { get; set; }
	}
}
