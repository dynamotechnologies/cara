﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;

namespace Aquilent.Cara.Domain.DataMart
{
	public class DataMartUserFilter : IFilter
	{
		private int _numRows = 100;
		private int _startRow = 0;

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Shortname { get; set; }

		public int NumRows
		{
			get { return _numRows; }
			set { _numRows = value; }
		}

		public int StartRow
		{
			get { return _startRow; }
			set { _startRow = value; }
		}
	}
}
