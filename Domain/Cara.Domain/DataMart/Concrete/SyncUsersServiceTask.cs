﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.DataMart
{
	public class SyncUsersServiceTask : IServiceTask
	{
		public string Name
		{
			get { return "DataMart User Sync"; }
		}

		public void RunServiceTask()
		{
            using (var userManager = ManagerFactory.CreateInstance<UserManager>())
            {
                var dmManager = new DataMartManager();
                dmManager.UserManager = userManager;

                dmManager.SyncUsers();
                dmManager.Dispose();
            }
		}
	}
}
