﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace Aquilent.Cara.Domain.DataMart
{
	[ServiceContract(Namespace="")]
	public interface IDataMart
	{
		#region User Retrieval Methods
		/// <summary>
		/// Searches the DataMart for a user whose username matches the input parameter
		/// </summary>
		/// <param name="username"></param>
		/// <returns>The matching user or null, if none found</returns>
		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			RequestFormat = WebMessageFormat.Xml,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "users/{username}")]
		DataMartUser GetUser(string username);

		/// <summary>
		/// Retrieves users from the DataMart for each username in the list
		/// </summary>
		/// <param name="usernames"></param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(
		    BodyStyle = WebMessageBodyStyle.Bare,
		    ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/findUser?firstname={firstName}&lastname={lastName}&rows={numRows}&start={startRow}")]
		DataMartUserSearchResults FindUsers(string firstName, string lastName, string numRows, string startRow);
		#endregion User Retrieval Methods

		#region Project Retrieval Methods
		/// <summary>
		/// Retrieves a list of projects that matches the information
		/// defined in the filter.
		/// </summary>
		/// <param name="filter"></param>
		/// <returns>A list of matching projects or null, if none found.</returns>
		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/findNEPAProject?id={palsid}&name={partialName}&status={statusIds}&unit={unitCodes}&rows={numRows}&start={startRow}")]
		DataMartProjectSearchResults FindProjects(string palsid, string partialName, string statusIds, string unitCodes, string numRows, string startRow);

		/// <summary>
		/// Retrieves a project with the matching ID
		/// </summary>
		/// <param name="projectId"></param>
		/// <returns>The matching project or null, if none found.</returns>
		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "projects/nepa/{palsProjectId}")]
		DataMartProject GetProject(string palsProjectId);

		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "projects/nepa/{palsProjectId}/activities")]
		ProjectActivityList GetProjectActivities(string palsProjectId);

		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "projects/nepa/{palsProjectId}/locations")]
		Location GetProjectLocations(string palsProjectId);

		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "projects/nepa/{palsProjectId}/milestones")]
		MilestoneList GetProjectMilestones(string palsProjectId);

		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "ref/units/{unitcode}")]
		UnitInfo GetUnitInfo(string unitcode);
		#endregion Project Retrieval Methods

        #region Objection Methods
        /// <summary>
		/// Get Objection data from the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			RequestFormat = WebMessageFormat.Xml,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "objections/{objectionId}")]
		Objection GetObjection(string objectionId);

		/// <summary>
		/// Sends Objection data to the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "objections")]
		void AddObjection(Objection objection);

        /// <summary>
		/// Updates Objection data in the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebInvoke(
			Method = "PUT",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "objections/{objectionId}")]
		void UpdateObjection(string objectionId, Objection objection);

        /// <summary>
		/// Deletes Objection data from the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebInvoke(
			Method = "DELETE",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "objections/{objectionId}")]
		void DeleteObjection(string objectionId);
        #endregion Objection Methods

        #region CARA-to-PALS Project Linking Methods
        /// <summary>
		/// Associates a CARA project to a PALS project in the Datamart
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/linkProject")]
		void LinkProject(CaraLinkProject caraLinkProject);

		/// <summary>
		/// Disassociates a CARA project to a PALS project in the Datamart
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/unlinkProject")]
		void UnlinkProject(CaraUnlinkProject caraUnlinkProject);
		#endregion CARA-to-PALS Project Linking Methods

		#region CARA-to-PALS Document Linking Methods
		/// <summary>
		/// Associates a PALS Document with a CARA comment phase/period
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/linkDoc")]
		void LinkPhaseDocument(CaraLinkDocument caraLinkDocument);

		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/unlinkDoc")]
		void UnlinkPhaseDocument(CaraUnlinkDocument caraUnlinkDocument);

		/// <summary>
		/// Retrieves all of the information from the Datamart about the
		/// documents stored in the DMD associated with a PALS project
		/// that has been published to the projects page (pubFlag = 03)
		/// </summary>
		/// <param name="palsProjectId"></param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "projects/nepa/{palsProjectId}/docs?pubflag=1")]
		DmdDocumentList GetProjectDocuments(string palsProjectId);
		#endregion CARA-to-PALS Document Linking Methods

		#region CARA Phase Letter Counts Methods
		/// <summary>
		/// Sends information about the count of different classes of letters
		/// </summary>
		/// <param name="letterInfo"></param>
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/comment/phases")]
		void AddPhaseLetterInfo(string caraProjectId, DataMartLetterInfo letterInfo);

		[OperationContract]
		[WebInvoke(
			Method = "PUT",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/comment/phases/{phaseId}")]
		void UpdatePhaseLetterInfo(string caraProjectId, string phaseId, DataMartLetterInfo letterInfo);

		[OperationContract]
		[WebInvoke(
			Method = "DELETE",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/comment/phases/{phaseId}")]
		void DeletePhaseLetterInfo(string caraProjectId, string phaseId);
		#endregion CARA Phase Letter Counts Methods

		#region Mailing List Methods
		/// <summary>
		/// Sends Mailing List data about a project to the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/mailinglist/subscribers")]
		void AddSubscriber(string caraProjectId, Subscriber subscriber);

		/// <summary>
		/// Updates Mailing List data about a project to the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebInvoke(
			Method = "PUT",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/mailinglist/subscribers/{subscriberId}")]
		void UpdateSubscriber(string caraProjectId, string subscriberId, Subscriber subscriber);

		/// <summary>
		/// Deletes a project's mailing list from the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebInvoke(
			Method = "DELETE",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/mailinglist/subscribers/{subscriberId}")]
		void DeleteSubscriber(string caraProjectId, string subscriberId);

		/// <summary>
		/// Retrieves a project's mailing list from the DataMart
		/// </summary>
		/// <param name="caraProjectId"></param>
		/// <param name="mailingList"></param>
		[OperationContract]
		[WebGet(
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "caraprojects/{caraProjectId}/mailinglist/subscribers")]
		SubscriberList GetSubscribers(string caraProjectId);
		#endregion Mailing List Methods

		#region Public Reading Room Methods
		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/projectbypalsid/{palsProjectId}/readingroom/setactive")]
		void ActivateReadingRoom(string palsProjectId);

		[OperationContract]
		[WebInvoke(
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			ResponseFormat = WebMessageFormat.Xml,
			UriTemplate = "utils/cara/projectbypalsid/{palsProjectId}/readingroom/setinactive")]
		void DeactivateReadingRoom(string palsProjectId);
		#endregion
	}
}
