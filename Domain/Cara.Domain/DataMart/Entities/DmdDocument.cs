﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "projectdocument")]
	public class DmdDocument
	{
		[DataMember(Name = "docid", Order = 1)]
		public string DmdDocumentId { get; set; }

		[DataMember(Name = "projectid", Order = 2)]
		public int PalsProjectId { get; set; }

		[DataMember(Name = "projecttype", Order = 3)]
		public string ProjectType { get; set; }

		[DataMember(Name = "pubflag", Order = 4)]
		public string PubFlag { get; set; }

		[DataMember(Name = "wwwlink", Order = 5)]
		public string WwwLink { get; set; }

		[DataMember(Name = "docname", Order = 6)]
		public string Name { get; set; }

		[DataMember(Name = "description", Order = 7)]
		public string Description { get; set; }

		[DataMember(Name = "pdffilesize", Order = 8)]
		public string Size { get; set; }

        [DataMember(Name = "ordertag", Order = 9)]
		public string Sequence { get; set; }

        [DataMember(Name = "contid", Order = 10)]
		public string ContainerId { get; set; }

        [DataMember(Name = "contorder", Order = 11)]
		public string PrimarySequence { get; set; }
}
}
