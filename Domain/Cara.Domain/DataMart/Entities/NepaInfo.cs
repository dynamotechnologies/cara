﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "nepainfo")]
	public class NepaInfo
    {
        #region Properties
		[DataMember(Name = "projectdocumentid", Order = 1)]
		public string ProjectDocumentId { get; set; }

		[DataMember(Name = "analysistypeid", Order = 2)]
		public string AnalysisType { get; set; }

		[DataMember(Name = "statusid", Order = 3)]
		public int ProjectStatus { get; set; }

		[DataMember(Name = "contactname", Order = 4)]
		public string SopaContactName { get; set; }

		[DataMember(Name = "contactphone", Order = 5)]
		public string SopaContactPhone { get; set; }

		[DataMember(Name = "contactemail", Order = 6)]
		public string SopaContactEmail { get; set; }

		[DataMember(Name = "primaryprojectmanager", Order = 7, IsRequired = false)]
		public string PrimaryProjectManager { get; set; }

		[DataMember(Name = "secondaryprojectmanager", Order = 8, IsRequired = false)]
		public string SecondaryProjectManager { get; set; }

        [DataMember(Name = "dataentryperson", Order = 9, IsRequired = false)]
		public string DataEntryPerson { get; set; }

		[DataMember(Name = "purposeids", Order = 10)]
		public PurposeList Purposes { get; set; }

        public string AnalysisTypeName
        {
            get
            {
				return AnalysisType;
            }
        }

        public string ProjectStatusName
        {
            get
            {
                return LookupManager.GetLookup<LookupProjectStatus>()[ProjectStatus].Name;
            }
        }
        #endregion
    }
}
