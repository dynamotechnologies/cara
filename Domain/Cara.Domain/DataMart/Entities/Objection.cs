﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "objection")]
	public class Objection
	{
		[DataMember(Name = "id", Order = 1)]
		public int ObjectionId { get; set; }

		[DataMember(Name = "projecttype", Order = 2)]
		public string ProjectType { get; set; }

		[DataMember(Name = "projectid", Order = 3)]
		public int PalsId { get; set; }

		[DataMember(Name = "objector", Order = 4)]
		public string Objector { get; set; }

		[DataMember(Name = "docid", Order = 5)]
		public string DmdId { get; set; }

   		[DataMember(Name = "objrule", Order = 6)]
		public string CommentRule { get; set; }

		[DataMember(Name = "responsedate", Order = 7)]
		public string ResponseDate { get; set; }

		[DataMember(Name = "statusdetails", Order = 8)]
        public string Status { get; set; }

		[DataMember(Name = "outcomedetails", Order = 9)]
        public string Outcome { get; set; }

		[DataMember(Name = "periodstartdate", Order = 10)]
        public string StartDate { get; set; }

		[DataMember(Name = "periodenddate", Order = 11)]
        public string EndDate { get; set; }

		[DataMember(Name = "responseduedate", Order = 12)]
        public string DueDate { get; set; }

		[DataMember(Name = "responseduedateext", Order = 13)]
        public string ExtendedDueDate { get; set; }

		[DataMember(Name = "title", Order = 14)]
        public string Title { get; set; }
	}
}
