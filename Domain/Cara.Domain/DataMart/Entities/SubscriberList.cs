﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[CollectionDataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "subscribers", ItemName = "subscriber")]
	public class SubscriberList : List<Subscriber>
	{
	}
}
