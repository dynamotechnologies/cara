﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "projectsearchresults")]
	public class DataMartProjectSearchResults
	{
		[DataMember(Name = "resultmetadata", Order = 1, IsRequired = true)]
		public ResultMetaData ResultMetaData { get; set; }

		[DataMember(Name = "projects", Order = 2)]
		public DataMartProjectList Projects { get; set; }
	}
}
