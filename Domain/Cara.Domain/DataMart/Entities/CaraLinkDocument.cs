﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "caralinkdoc")]
	public class CaraLinkDocument
	{
		[DataMember(Name = "caraid", Order = 1)]
		public int CaraProjectId { get; set; }

		[DataMember(Name = "phaseid", Order = 2)]
		public int PhaseId { get; set; }

		[DataMember(Name = "docid", Order = 3)]
		public string DmdDocumentId { get; set; }

		public CaraLinkDocument(int caraProjectId, int phaseId, string dmdDocumentId)
		{
			CaraProjectId = caraProjectId;
			PhaseId = phaseId;
			DmdDocumentId = dmdDocumentId;
		}
	}
}
