﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "mailaddress")]
	public class UnitMailAddress
	{
		[DataMember(Name="street1", Order = 1)]
		public string Address1 { get; set; }

		[DataMember(Name = "street2", Order = 2)]
		public string Address2 { get; set; }

		[DataMember(Name = "city", Order = 3)]
		public string City { get; set; }

		[DataMember(Name = "state", Order = 4)]
		public string State { get; set; }

		[DataMember(Name = "zip", Order = 5)]
		public string ZipCode { get; set; }
	}
}
