﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "projectmanagement")]
	public class ProjectManagement
	{
		[DataMember(Name = "primary", Order = 1)]
		public ProjectUser PrimaryManager { get; set; }

		[DataMember(Name = "secondary", Order = 2)]
		public ProjectUser SecondaryManager { get; set; }
	}
}
