﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "unitrole")]
	public class UnitRole
	{
		[DataMember(Name = "unit", Order = 1)]
		public string Unit { get; set; }

		[DataMember(Name = "role", Order = 2)]
		public string Role { get; set; }
	}
}
