﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "projectmilestone")]
	public class Milestone
	{
		public static readonly int TYPE_DECISION_DATE = 5;
		public static readonly int STATUS_ESTIMATED = 0;
		public static readonly int OBJECTION_START_DATE = 4;

		[DataMember(Name = "projectid", Order = 1)]
		public int PalsProjectId { get; set; }

		[DataMember(Name = "seqnum", Order = 2)]
		public int SequenceNumber { get; set; }

		/// <summary>
		/// ID		Name
		/// 0		Expected Analysis Type Change
		/// 1		Notice of Initiation
		/// 2		Comment Period
		/// 3		Final Analysis Document Availability
		/// 4		Objection Period
		/// 5		NEPA or Forest Plan Amendment Decision Document Available
		/// 6		Implementation
		/// </summary>
		[DataMember(Name = "milestonetype", Order = 3)]
		public int MilestoneType { get; set; }

		[DataMember(Name = "datestring", Order = 4)]
		public string DateString { get; set; }

		/// <summary>
		/// Status		Meaning
		/// 0			Datestring is estimated and has format MM/YYYY
		/// 1			Datestring is actual and has format MM/DD/YYYY
		/// </summary>
		[DataMember(Name = "status", Order = 5)]
		public int Status { get; set; }

		/// <summary>
		/// History		Meaning
		/// 0			History Milestone
		/// 1			Current Milestone
		/// </summary>
		[DataMember(Name = "history", Order = 6)]
		public int History { get; set; }
	}
}
