﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[CollectionDataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "purposeids", ItemName = "purposeid")]
	public class PurposeList : List<string>
	{

	}
}
