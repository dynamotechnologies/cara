﻿using Aquilent.Framework.Security;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "user")]
	public class DataMartUser
	{
		#region Properties
		[DataMember(Name="shortname", Order=1)]
		public string Username { get; set; }

		[DataMember(Name = "firstname", Order=2)]
		public string FirstName { get; set; }

		[DataMember(Name = "lastname", Order=3)]
		public string LastName { get; set; }

		[DataMember(Name = "email", Order=4)]
		public string Email { get; set; }

		[DataMember(Name = "phone", Order=5)]
		public string Phone { get; set; }

		[DataMember(Name = "title", Order=6)]
		public string Title { get; set; }

		[DataMember(Name = "unitroles", Order = 7)]
		public UnitRoleList UnitRoles { get; set; }
		#endregion Properties

		#region Methods
		/// <summary>
		/// Uses the DataMartUser properties to create a new User object.
		/// </summary>
		/// <returns>The new User object.</returns>
		public User Convert()
		{
			var user = new User();

			user.Username = Username;
			user.FirstName = FirstName;
			user.LastName = LastName;
			user.Email = Email;
			user.Phone = Phone;
			user.Title = Title;

			// UnitRoles will no longer be sync'd to CARA (Jira issue:  CARA-238)
			// user.UserUnits = UnitRoles.Select(x => x.Unit).Distinct().Select(unitcode =>  new UserUnit { UnitId = unitcode }).ToList();

			return user;
		}

		/// <summary>
		/// Copies the values from the DataMartUser to the User
		/// </summary>
		/// <param name="user"></param>
		public User CopyValuesTo(User user)
		{
			if (user == null)
			{
				user = new User();
			}

			user.Username = Username.ToLower();
			user.FirstName = FirstName;
			user.LastName = LastName;
			user.Email = Email;
			user.Phone = Phone;
			user.Title = Title;

			// UnitRoles will no longer be sync'd to CARA (Jira issue:  CARA-238)
			// user.UserUnits.Clear();
			// user.UserUnits = UnitRoles.Select(x => x.Unit).Distinct().Select(unitcode =>  new UserUnit { UnitId = unitcode }).ToList();

			return user;
		}

		/// <summary>
		/// Compares the properties of this DataMartUser to a User
		/// to see if they are equal.
		/// </summary>
		/// <param name="user">The user to compare</param>
		/// <returns>true, if the properties don't match</returns>
		public bool IsDifferent(User user)
		{
			var different = false;
			if (user.FirstName != FirstName
					|| user.LastName != LastName
					|| user.Email != Email
					|| user.Phone != Phone
					|| user.Title != Title)
			{
				different = true;
			}
            // CARA-1296 Units are managed via CARA for now
            //else
            //{
            //    List<string> uUnitCodes = user.Units;
            //    List<string> dmUnitCodes = UnitRoles.Select(x => x.Unit).Distinct().ToList();

            //    if (uUnitCodes.Count != dmUnitCodes.Count)
            //    {
            //        different = true;
            //    }
            //    else
            //    {
            //        uUnitCodes.Sort();
            //        dmUnitCodes.Sort();

            //        for (int i = 0; i < uUnitCodes.Count; i++)
            //        {
            //            if (uUnitCodes[i] != dmUnitCodes[i])
            //            {
            //                different = true;
            //                break;
            //            }
            //        }
            //    }
            //}

			return different;
		}
		#endregion
	}
}
