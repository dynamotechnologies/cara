﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "usersearchresults")]
	public class DataMartUserSearchResults
	{
		[DataMember(Name = "resultmetadata", Order = 1, IsRequired = true)]
		public ResultMetaData ResultMetaData { get; set; }

		[DataMember(Name = "users", Order = 2)]
		public DataMartUserList DataMartUsers { get; set; }

		public IQueryable<User> Users { get; set; }
	}
}
