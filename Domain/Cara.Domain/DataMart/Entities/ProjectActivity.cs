﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "projectactivity")]
	public class ProjectActivity
	{
		[DataMember(Name = "projectid", Order = 1)]
		public int PalsProjectId { get; set; }

		[DataMember(Name = "activityid", Order = 2)]
		public string ActivityId { get; set; }
	}
}
