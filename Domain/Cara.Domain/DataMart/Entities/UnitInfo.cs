﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "unit")]
	public class UnitInfo
	{
		[DataMember(Name = "code", Order = 1)]
		public string Code { get; set; }

		[DataMember(Name = "name", Order = 2)]
		public string Name { get; set; }

		[DataMember(Name = "extendeddetails", Order = 3)]
		public string ExtendedDetails { get; set; }

		[DataMember(Name = "projectmap", Order = 4)]
		public bool ProjectMap { get; set; }

		[DataMember(Name = "address1", Order = 5)]
		public string Address1 { get; set; }

		[DataMember(Name = "address2", Order = 6)]
		public string Address2 { get; set; }

		[DataMember(Name = "city", Order = 7)]
		public string City { get; set; }

		[DataMember(Name = "state", Order = 8)]
		public string State { get; set; }

		[DataMember(Name = "zip", Order = 9)]
		public string Zip { get; set; }

		[DataMember(Name = "phonenumber", Order = 10)]
		public string Phone { get; set; }

		[DataMember(Name = "wwwlink", Order = 11)]
		public string WwwLink { get; set; }

		[DataMember(Name = "commentemail", Order = 12)]
		public string CommentEmail { get; set; }

		[DataMember(Name = "newspaper", Order = 13)]
		public string Newspaper { get; set; }

		[DataMember(Name = "newspaperurl", Order = 14)]
		public string NewspaperLink { get; set; }

		[DataMember(Name = "unitboundary", Order = 15)]
		public string Boundary { get; set; }
	}
}
