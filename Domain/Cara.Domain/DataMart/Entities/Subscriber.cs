﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "subscriber")]
	public class Subscriber
	{
		private Author Author { get; set; }

		public Subscriber(Author author)
		{
			Author = author;
		}

		#region Subscriber Data Members
		[DataMember(Name = "subscriberid", Order = 1)]
		public int SubscriberId
		{
			get { return Author.AuthorId; }
			set { Author.AuthorId = value; }
		}

		[DataMember(Name = "email", Order = 2, EmitDefaultValue = false)]
		public string Email
		{
			get { return GetValue(Author.Email); }
			set { Author.Email = value; }
		}

		[DataMember(Name = "lastname", Order = 3)]
		public string LastName
		{
			get { return GetValue(Author.LastName); }
			set { Author.LastName = value; }
		}

		[DataMember(Name = "firstname", Order = 4)]
		public string FirstName
		{
			get { return GetValue(Author.FirstName); }
			set { Author.FirstName = value; }
		}

		[DataMember(Name = "title", Order = 5, EmitDefaultValue = false)]
		public string Title
		{
			get { return GetValue(Author.Title); }
			set { Author.Title = value; }
		}

		[DataMember(Name = "org", Order = 6, EmitDefaultValue = false)]
		public string Organization
		{
			get 
			{
				string orgname = string.Empty;
				if (Author.Organization != null)
				{
					orgname = Author.Organization.Name;
				}
				return orgname; 
			}

			set
			{
				if (Author.Organization == null)
				{
					Author.Organization = new Organization();
				}

				Author.Organization.Name = value;
			}
		}

		[DataMember(Name = "orgtype", Order = 7, EmitDefaultValue = false)]
		public string OrganizationType
		{
			get
			{
				string orgtype = string.Empty;
				if (Author.Organization != null && !string.IsNullOrEmpty(Author.Organization.OrganizationTypeName))
				{
					orgtype = Author.Organization.OrganizationTypeName;
				}
				return orgtype;
			}

			set
			{
				if (Author.Organization == null)
				{
					Author.Organization = new Organization();
				}

				Author.Organization.OrganizationTypeId = LookupManager.GetLookup<LookupOrganizationType>().FirstOrDefault(x => x.Name == value).OrganizationTypeId;
			}
		}

		[DataMember(Name = "subscribertype", Order = 8, EmitDefaultValue = false)]
		public string SubscriberType
		{
			get { return GetValue(Author.OfficialRepresentativeTypeName); }
			set { Author.OfficialRepresentativeTypeId = LookupManager.GetLookup<LookupOfficialRepresentativeType>().FirstOrDefault(x => x.Name == value).OfficialRepresentativeTypeId; }
		}

		[DataMember(Name = "addstreet1", Order = 9, EmitDefaultValue = false)]
		public string Address1
		{
			get { return GetValue(Author.Address1); }
			set { Author.Address1 = value; }
		}

		[DataMember(Name = "addstreet2", Order = 10, EmitDefaultValue = false)]
		public string Address2
		{
			get { return GetValue(Author.Address2); }
			set { Author.Address2 = value; }
		}

		[DataMember(Name = "city", Order = 11, EmitDefaultValue = false)]
		public string City
		{
			get { return GetValue(Author.City); }
			set { Author.City = value; }
		}

		[DataMember(Name = "state", Order = 12, EmitDefaultValue = false)]
		public string State
		{
			get { return GetValue(Author.StateId); }
			set { Author.StateId = value; }
		}

		[DataMember(Name = "zip", Order = 13, EmitDefaultValue = false)]
		public string ZipPostal
		{
			get { return GetValue(Author.ZipPostal); }
			set { Author.ZipPostal = value; }
		}

		[DataMember(Name = "prov", Order = 14, EmitDefaultValue=false)]
		public string Province
		{
			get { return GetValue(Author.ProvinceRegion); }
			set { Author.ProvinceRegion = value; }
		}

		[DataMember(Name = "country", Order = 15, EmitDefaultValue = false)]
		public string Country
		{
			get { return GetValue(Author.CountryName); }
			set { Author.CountryId = LookupManager.GetLookup<LookupCountry>().FirstOrDefault(x => x.Name == value).CountryId; }
		}

		[DataMember(Name = "phone", Order = 14, EmitDefaultValue = false)]
		public string Phone
		{
			get { return GetValue(Author.Phone); }
			set { Author.Phone = value; }
		}

		[DataMember(Name = "contactmethod", Order = 15)]
		public string ContactMethod
		{
			get { return GetValue(Author.ContactMethod); }
			set { Author.ContactMethod = value; }
		}
		#endregion Subscriber Data Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="?"></param>
		/// <returns>the value if not null, otherwise string.Empty</returns>
		private string GetValue(string value)
		{
			return string.IsNullOrEmpty(value) ? string.Empty : value;
		}
	}
}
