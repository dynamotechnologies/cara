﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[CollectionDataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "projectdocuments", ItemName = "projectdocument")]
	public class DmdDocumentList : List<DmdDocument>
	{
	}
}
