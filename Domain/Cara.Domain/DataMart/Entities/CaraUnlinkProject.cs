﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "caraunlinkproject")]
	public class CaraUnlinkProject
	{
		[DataMember(Name = "caraid", Order = 1)]
		public int CaraProjectId { get; set; }

		public CaraUnlinkProject(int caraProjectId)
		{
			CaraProjectId = caraProjectId;
		}
	}
}
