﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema")]
	public class ProjectUser
	{
		[DataMember(Name = "shortname", Order = 1)]
		public string Username { get; set; }

		[DataMember(Name = "lastname", Order = 2)]
		public string LastName { get; set; }

		[DataMember(Name = "firstname", Order = 3)]
		public string FirstName { get; set; }

		public string FullName
		{
			get
			{
				return FirstName + " " + LastName;
			}
		}
	}
}
