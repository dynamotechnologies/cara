﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "caralinkproject")]
	public class CaraLinkProject
	{
		[DataMember(Name = "caraid", Order = 1)]
		public int CaraProjectId { get; set; }

		[DataMember(Name = "palsid", Order = 2)]
		public int PalsProjectId { get; set; }

		public CaraLinkProject(int caraProjectId, int palsProjectId)
		{
			CaraProjectId = caraProjectId;
			PalsProjectId = palsProjectId;
		}
	}
}
