﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "locations")]
	public class Location
	{
		[DataMember(Name = "projectid", Order = 1)]
		public string ProjectId { get; set; }

		[DataMember(Name = "locationdesc", Order = 2)]
		public string Description { get; set; }

		[DataMember(Name = "locationlegaldesc", Order = 3)]
		public string LegalDescription { get; set; }

		[DataMember(Name = "latitude", Order = 4)]
		public double Latitude { get; set; }

		[DataMember(Name = "longitude", Order = 5)]
		public double Longitude { get; set; }

		[DataMember(Name = "regions", Order = 6)]
		public RegionList Regions { get; set; }

		[DataMember(Name = "forests", Order = 7)]
		public ForestList Forests { get; set; }

		[DataMember(Name = "districts", Order = 8)]
		public DistrictList Districts { get; set; }

		[DataMember(Name = "states", Order = 9)]
		public StateList States { get; set; }

		[DataMember(Name = "counties", Order = 10)]
		public CountyList Counties { get; set; }

		public Location()
		{
			Regions = new RegionList();
			Forests = new ForestList();
			Districts = new DistrictList();
			States = new StateList();
			Counties = new CountyList();
		}
	}
}
