﻿using Aquilent.Cara.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "project")]
	public class DataMartProject
	{
		#region Properties

		#region Properties returned from the DataMart
		[DataMember(Name = "type", Order = 1)]
		public string Type { get; set; }

		[DataMember(Name = "id", Order = 2)]
		public string ProjectId { get; set; }

		[DataMember(Name = "name", Order = 3)]
		public string Name { get; set; }

		[DataMember(Name = "unitcode", Order = 4)]
		public string LeadManagementUnit { get; set; }

		[DataMember(Name = "adminapp", Order = 5)]
		public string AdminApp { get; set; }

		[DataMember(Name = "description", Order = 6)]
		public string Description { get; set; }

		//[DataMember(Name = "createdate", Order = 8)]
		//public DateTime CreationDate { get; set; }

		[DataMember(Name = "lastupdate", Order = 9)]
		public DateTime LastUpdateDate { get; set; }

		//[DataMember(Name = "projectmanagement", Order = 10)]
		//public ProjectManagement ProjectManagers { get; set; }

		[DataMember(Name = "commentreg", Order = 11)]
		public string CommentReg { get; set; }

		[DataMember(Name = "wwwlink", Order = 12)]
		public string WwwLink { get; set; }

		[DataMember(Name = "wwwsummary", Order = 13)]
		public string WwwSummary { get; set; }

		[DataMember(Name = "wwwpub", Order = 14)]
		public int WwwPub { get; set; }

		[DataMember(Name = "nepainfo", Order = 15)]
		public NepaInfo NepaInfo { get; set; }
		#endregion Properties returned from the DataMart

		public bool ExistsInCara { get; set; }

        public bool IsObjection { get; set; }

        public string LeadManagementUnitName
        {
            get
            {
                return LookupManager.GetLookup<LookupUnit>()[LeadManagementUnit].Name;
            }
        }

		public IList<string> ActivityCodes { get; set; }
		public string ActivityNameList
		{
			get
			{
				var lookup = LookupManager.GetLookup<LookupActivity>();
				var activityNameList = new StringBuilder();
				foreach (string code in ActivityCodes)
				{
					if (activityNameList.Length > 0)
					{
						activityNameList.Append(", ");
					}

					activityNameList.Append(lookup.Where(x => x.Code == code).First().Name);
				}

				return activityNameList.ToString();
			}
		}

		public string EstimatedDecisionDate { get; set; }
		public Location Location { get; set; }
		public IList<ProjectDocument> Documents { get; set; }
		#endregion Properties

		#region Constructor
		public DataMartProject()
		{
			ExistsInCara = false;
		}
		#endregion Constructor

		#region Transformation Methods
		public Project ToProject()
		{
			Project toProject = new Project();

			toProject.UnitId = this.LeadManagementUnit;
			toProject.ProjectStatusId = this.NepaInfo.ProjectStatus;
			toProject.Name = this.Name;
			//toProject.CreateDate = this.CreationDate;
			toProject.ProjectTypeId = LookupManager.GetLookup<LookupProjectType>().First(x => x.Name.ToUpper() == "PALS").ProjectTypeId;
			toProject.Description = this.Description;
			toProject.ProjectNumber = this.ProjectId;
			toProject.AnalysisTypeId = LookupManager.GetLookup<LookupAnalysisType>().First(x => x.Name.ToUpper() == this.NepaInfo.AnalysisType).AnalysisTypeId;
			toProject.SOPAUser = this.NepaInfo.SopaContactName;
			toProject.LastUpdated = this.LastUpdateDate;
			//toProject.PalsProjectManager1 = this.ProjectManagers.PrimaryManager.FullName;
			toProject.CommentRuleId = Convert.ToInt32(this.CommentReg);
			toProject.ProjectDocumentId = Convert.ToInt32(this.NepaInfo.ProjectDocumentId);

			if (toProject.ProjectActivities == null)
			{
				toProject.ProjectActivities = new List<Domain.ProjectActivity>();
			}
			
			var activityLookup = LookupManager.GetLookup<LookupActivity>();
			toProject.ProjectActivities = this.ActivityCodes.Select(x =>
				new Domain.ProjectActivity
				{
					ProjectId = toProject.ProjectId,
					ActivityId = activityLookup.First(y => y.Code == x).ActivityId
				}
			).ToList();

			if (toProject.ProjectPurposes == null)
			{
				toProject.ProjectPurposes = new List<ProjectPurpose>();
			}

			var purposeLookup = LookupManager.GetLookup<LookupPurpose>();
			toProject.ProjectPurposes = this.NepaInfo.Purposes.Select(x =>
				new ProjectPurpose
				{
					ProjectId = toProject.ProjectId,
					PurposeId = purposeLookup.First(y => y.Code == x).PurposeId
				}
			).ToList();

			if (toProject.ProjectDocuments == null)
			{
				toProject.ProjectDocuments = new List<ProjectDocument>();
			}

			toProject.ProjectDocuments = this.Documents;


			if (toProject.Locations == null)
			{
				toProject.Locations = new List<Domain.Location>();
			}

			foreach (string regionCode in this.Location.Regions)
			{
				toProject.Locations.Add(
					new Domain.Location
					{
						AreaId = regionCode,
						AreaType = Domain.Location.AreaTypeEnum.Unit.ToString(),
						ProjectId = toProject.ProjectId
					}
				);
			}

			foreach (string forestCode in this.Location.Forests)
			{
				toProject.Locations.Add(
					new Domain.Location
					{
						AreaId = forestCode,
						AreaType = Domain.Location.AreaTypeEnum.Unit.ToString(),
						ProjectId = toProject.ProjectId
					}
				);
			}

			foreach (string districtCode in this.Location.Districts)
			{
				toProject.Locations.Add(
					new Domain.Location
					{
						AreaId = districtCode,
						AreaType = Domain.Location.AreaTypeEnum.Unit.ToString(),
						ProjectId = toProject.ProjectId
					}
				);
			}

			foreach (string stateId in this.Location.States)
			{
				toProject.Locations.Add(
					new Domain.Location
					{
						AreaId = stateId,
						AreaType = Domain.Location.AreaTypeEnum.State.ToString(),
						ProjectId = toProject.ProjectId
					}
				);
			}

			foreach (string countyId in this.Location.Counties)
			{
				toProject.Locations.Add(
					new Domain.Location
					{
						AreaId = countyId,
						AreaType = Domain.Location.AreaTypeEnum.County.ToString(),
						ProjectId = toProject.ProjectId
					}
				);
			}

			return toProject;
		}

        //CARA 721:- Added new column ProjectUrl
		public WebCommentPageInfo ToWebCommentPageInfo(UnitInfo unit)
		{
			WebCommentPageInfo wcpi = new WebCommentPageInfo
			{
				CommentRuleId = Convert.ToInt32(this.CommentReg),
				CommentEmail = !string.IsNullOrEmpty(unit.CommentEmail) ? unit.CommentEmail : this.NepaInfo.SopaContactEmail,
				ContactName = this.NepaInfo.SopaContactName,
				//CreatedDate = this.CreationDate,
				LastUpdatedDate = this.LastUpdateDate,
				MailAddressCity = unit.City,
				MailAddressState = unit.State,
				MailAddressStreet1 = unit.Address1,
				MailAddressStreet2 = unit.Address2,
				MailAddressZip = unit.Zip,
				Name = this.Name,
				NewspaperOfRecord = unit.Newspaper,
				NewspaperOfRecordUrl = unit.NewspaperLink,
				PalsId = Convert.ToInt32(this.ProjectId),
                ProjectUrl = WwwLink,
                UnitId = unit.Code
			};

			return wcpi;
		}
		#endregion Transformation Methods
	}
}
