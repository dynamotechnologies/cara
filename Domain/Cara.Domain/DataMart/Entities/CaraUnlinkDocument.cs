﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "caraunlinkdoc")]
	public class CaraUnlinkDocument : CaraLinkDocument
	{
		public CaraUnlinkDocument(int caraProjectId, int phaseId, string dmdDocumentId)
			: base(caraProjectId, phaseId, dmdDocumentId)
		{
		}
	}
}
