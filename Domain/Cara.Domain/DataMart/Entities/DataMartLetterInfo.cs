﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "commentphase")]
	public class DataMartLetterInfo
	{
		#region Properties
		[DataMember(Name = "phaseid", Order = 1)]
		public int PhaseId { get; set; }

		[DataMember(Name = "name", Order = 2)]
		public string Name { get; set; }

		[DataMember(Name = "startdate", Order = 3)]
		public string StartDate { get; set; }

		[DataMember(Name = "finishdate", Order = 4)]
		public string FinishDate { get; set; }

		[DataMember(Name = "lettno", Order = 5)]
		public int TotalLetterCount { get; set; }

		[DataMember(Name = "lettuniq", Order = 6)]
		public int UniqueLetterCount { get; set; }

		[DataMember(Name = "lettmst", Order = 7)]
		public int MasterFormLetterCount { get; set; }

		[DataMember(Name = "lettform", Order = 8)]
		public int FormLetterCount { get; set; }

		[DataMember(Name = "lettformplus", Order = 9)]
		public int FormPlusLetterCount { get; set; }

		[DataMember(Name = "lettformdupe", Order = 10)]
		public int DuplicateLetterCount { get; set; }
		#endregion Properties
	}
}
