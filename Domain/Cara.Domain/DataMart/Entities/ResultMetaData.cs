﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aquilent.Cara.Domain.DataMart
{
	[DataContract(Namespace = "http://www.fs.fed.us/nepa/schema", Name = "resultmetadata")]
	public class ResultMetaData
	{
		[DataMember(Name = "request", Order = 1)]
		public string RequestUrl { get; set; }

		[DataMember(Name = "rowcount", Order = 2)]
		public int RowCount { get; set; }

		[DataMember(Name = "totalcount", Order = 3)]
		public int TotalCount { get; set; }
	}
}
