﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the phase code entity
    /// </summary>
    public class PhaseCodeMetadata
    {
        #region Metadata Fields
        [Required, StringLength(20), DisplayName("Number")]
        public string CodeNumber { get; set; }

        [Required, StringLength(200), DisplayName("Description")]
        public string CodeName { get; set; }

        [Required, DisplayName("Type")]
        public int CodeCategoryId { get; set; }
        #endregion
    }
}
