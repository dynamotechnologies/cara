﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    public partial class ProjectActivity
    {
        public string ActivityName
        {
            get
            {
				return LookupManager.GetLookup<LookupActivity>()[this.ActivityId].Name;
            }
        }
    }
}
