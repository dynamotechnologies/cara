﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain.Report
{
    /// <summary>
    /// Metadata for the custom report data entity
    /// </summary>
    public class CustomReportDataMetadata
    {
        #region Metadata Fields
        [DisplayName("State")]
        public string StateId { get; set; }

        [DisplayName("Country")]
        public string CountryName { get; set; }

        [DisplayName("Organization")]
        public string OrganizationName { get; set; }

        [DisplayName("Codes")]
        public string CommentCodeList { get; set; }

        [DisplayName("Concerns")]
        public string ConcernTextDisplay { get; set; }

        [DisplayName("Responses")]
        public string ResponseTextDisplay { get; set; }

        [DisplayName("Concerns")]
        public string ConcernText { get; set; }

        [DisplayName("Responses")]
        public string ResponseText { get; set; }

        [DisplayName("Activities")]
        public string ProjectActivityNameList { get; set; }

        [DisplayName("Regions")]
        public string ProjectRegionNameList { get; set; }

        [DisplayName("Forests")]
        public string ProjectForestNameList { get; set; }

        [DisplayName("Districts")]
        public string ProjectDistrictNameList { get; set; }
        
        [DisplayName("States")]
        public string ProjectStateList { get; set; }

        [DisplayName("Counties")]
        public string ProjectCountyNameList { get; set; }        
        #endregion
    }
}
