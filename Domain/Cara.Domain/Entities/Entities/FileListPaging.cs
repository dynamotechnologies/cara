﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a file metadata list paging response
    /// </summary>
    public class FileListPaging
    {
		[XmlRpcMember("rowcount")]
		public int RowCount { get; set; }

		[XmlRpcMember("msize")]
		public int MSize { get; set; }
    }
}
