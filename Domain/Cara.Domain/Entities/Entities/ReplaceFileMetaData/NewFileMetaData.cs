﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a file request
    /// </summary>
	public class NewFileMetaData
    {
		[XmlRpcMember("appsysid")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string AppSysId { get; set; } 

		[XmlRpcMember("appdoctype")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string AppDocType { get; set; }

		[XmlRpcMember("title")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string Title { get; set; }

		[XmlRpcMember("author")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string Author { get; set; }

		[XmlRpcMember("moreMetadata")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public NewMoreFileMetaData MoreMetaData { get; set; }

		public NewFileMetaData()
		{
			MoreMetaData = new NewMoreFileMetaData();
		}
    }
}
