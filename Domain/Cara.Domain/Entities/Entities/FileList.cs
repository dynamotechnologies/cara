﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a list of file metadata response
    /// </summary>
    public class FileList
    {
		[XmlRpcMember("resultmetadata")]
		public FileListPaging ResultMetaData { get; set;}

		[XmlRpcMember("hitlist")]
		public FileListMetaData[] Hitlist;
    }
}
