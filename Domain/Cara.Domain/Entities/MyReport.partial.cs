﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.Report.Personal
{
    [MetadataType(typeof(MyReportMetadata))]
    public partial class MyReport
    {
        #region Public Properties
        public string Type
        {
            get
            {
                return (Private.HasValue ? (Private.Value ? "Private" : "Shared") : string.Empty);
            }
        }

        public string ParameterDisplay
        {
            get
            {
                string result = string.Empty;

                if (!string.IsNullOrEmpty(Parameters))
                {
                }

                return result;
            }
        }

        public string Username { get; set; }
        #endregion
    }
}
