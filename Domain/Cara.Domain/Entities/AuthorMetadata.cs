﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the author entity
    /// </summary>
    public class AuthorMetadata
    {
        #region Metadata Fields
        [StringLength(100), DisplayName("First Name")]
        public string FirstName { get; set; }

        [StringLength(100), DisplayName("Last Name")]
        public string LastName { get; set; }

        [StringLength(100), DisplayName("Middle Name")]
        public string MiddleName { get; set; }

        [DisplayName("Author Name")]
        public string FullName { get; set; }

        [StringLength(50), DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("Official Representative Type")]
        public string OfficialRepresentativeTypeName { get; set; }

        [StringLength(200), DisplayName("Address 1")]
        public string Address1 { get; set; }

        [StringLength(200), DisplayName("Address 2")]
        public string Address2 { get; set; }

        [DisplayName("State")]
        public int StateId { get; set; }

        [DisplayName("Province/Region")]
        public string ProvinceRegion { get; set; }
        
        [DisplayName("Country")]
        public int CountryId { get; set; }

        [StringLength(20), DisplayName("ZIP/Postal Code")]
        public string ZipPostal { get; set; }

        [StringLength(20), DisplayName("Phone Number")]
        public string Phone { get; set; }

        [StringLength(100), DisplayName("Email")]
        [RegularExpression("^([\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4})?$", ErrorMessage = "The Email field must contain a valid email address")]
        public string Email { get; set; }

        [Required, DisplayName("How would you like to be contacted about this or similar projects in the future?")]
        public string ContactMethod { get; set; }

        [DisplayName("Organization")]
        public int OrganizationId { get; set; }

        [DisplayName("Official Representative Type")]
        public int OfficialRepresentativeTypeId { get; set; }

        [DisplayName("Author Address")]
        public string FullAddress { get; set; }
        #endregion
    }
}
