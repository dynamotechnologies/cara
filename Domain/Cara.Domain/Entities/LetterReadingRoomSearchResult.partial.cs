﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(LetterReadingRoomSearchResultMetadata))]
    public partial class LetterReadingRoomSearchResult
    {
        #region Public Properties
        /// <summary>
        /// Retrieves the name of the user using "First Last" Format (e.g., Bill Gates)
        /// </summary>
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }
        
        /// <summary>
        /// Retrieves the name of the user using "Last, First" Format (e.g., Gates, Bill)
        /// </summary>
        public string FullNameLastFirst
        {
            get
            {
                return string.Format("{0}, {1}", LastName, FirstName);
            }
        }

        /// <summary>
        /// Returns the name of the letter type
        /// </summary>
        public string LetterTypeName
        {
            get
            {
                return (LetterTypeId > 0 ? LookupManager.GetLookup<LookupLetterType>()[LetterTypeId].Name : string.Empty);
            }

        }

        /// <summary>
        /// Returns the name of the letter organization
        /// </summary>
        public string OrganizationTypeName
        {
            get
            {
                return OrganizationTypeId.HasValue ? LookupManager.GetLookup<LookupOrganizationType>()[OrganizationTypeId.Value].Name : string.Empty;
            }
        }

        /// <summary>
        /// Returns the name of the letter publish status
        /// </summary>
        public string PublishToReadingRoomName
        {
            get
            {
                return PublishToReadingRoom ? "Published" : "Un-published";
            }
        }

        /// <summary>
        /// Returns a string indicating whether the select checkbox should be checked based on the publish flag
        /// </summary>
        public string Checked
        {
            get
            {
                return (PublishToReadingRoom ? "checked" : string.Empty);
            }
        }

        public string UnpublishedReasonOtherDisplay
        {
            get
            {
                return (!string.IsNullOrEmpty(UnpublishedReasonOther) ? string.Empty : "display:none;");
            }
        }

        /// <summary>
        /// Returns the name of the letter unpublished reason
        /// </summary>
        public string UnpublishedReasonName
        {
            get
            {
                return UnpublishedReasonId.HasValue ? LookupManager.GetLookup<LookupUnpublishedReason>()[UnpublishedReasonId.Value].Name : string.Empty;
            }
        }

        /// <summary>
        /// Returns true if the letter contains attachment(s)
        /// </summary>
        public bool HasAttachments
        {
            get
            {
                return (AttachmentCount > 0);
            }
        }

        /// <summary>
        /// Returns the key string containing the information to group a collection of reading room
        /// </summary>
        public string ReadingRoomKeyCount
        {
            get
            {
                return string.Format("[id:{0}]{1} (Total Letters: {2})", PhaseId.ToString(),
                    PhaseTypeDesc, (LetterCount.HasValue ? LetterCount.Value : 0).ToString());
            }
        }

        /// <summary>
        /// Returns the name of the letter phase type name and description
        /// </summary>
        public string PhaseTypeDesc
        {
            get
            {
                return string.Format("{0} ({1})", LookupManager.GetLookup<LookupPhaseType>()[this.PhaseTypeId].Name, PhaseName);
            }
        }

        #endregion
    }
}
