﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    public partial class TeamMemberReportData
    {
        /// <summary>
        /// Returns the TeamMember Name for display
        /// </summary>
        public string TeamMemberName
        {
            get
            {
                return (string.Format("{0}, {1}", TeamMemberLastName, TeamMemberFirstName));
            }
        }
    }
}
