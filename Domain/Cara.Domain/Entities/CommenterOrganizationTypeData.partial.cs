﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    public partial class CommenterOrganizationTypeData
    {
        /// <summary>
        /// Returns the Author Name for display
        /// </summary>
        public string AuthorName
        {
            get
            {
                return (string.Format("{0}, {1}", LastName, FirstName));
            }
        }
    }
}
