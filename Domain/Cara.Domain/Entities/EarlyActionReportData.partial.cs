﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    public partial class EarlyActionReportData
    {

        /// <summary>
        /// Returns the Author Name for display
        /// </summary>
        public string Name
        {
            get
            {
                return (string.Format("{0}, {1}", LastName, FirstName));
            }
        }
        
        /// <summary>
        /// Returns the Letter sequence comment number display
        /// </summary>
        public string LetterCommentNumber
        {
            get
            {
                return (string.Format("{0}-{1}", LetterSequence, CommentNumber));
            }
        }

      
        /// <summary>
        /// Returns the TermListName display
        /// </summary>
        public string TermListNameDisplay
        {
            get
            {
                StringBuilder termListName = new StringBuilder(TermListName);

                termListName.Replace(".0000.00", string.Empty);
                termListName.Replace("00.00", string.Empty);
                termListName.Replace(".00", string.Empty);
                return (string.Format("{0}", termListName));
            }
        }
    }
}
