//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain
{
    public partial class LetterDocument
    {
        #region Primitive Properties
    
        public virtual int DocumentId
        {
            get;
            set;
        }
    
        public virtual int LetterId
        {
            get { return _letterId; }
            set
            {
                if (_letterId != value)
                {
                    if (Letter != null && Letter.LetterId != value)
                    {
                        Letter = null;
                    }
                    _letterId = value;
                }
            }
        }
        private int _letterId;
    
        public virtual Nullable<bool> Protected
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual Letter Letter
        {
            get { return _letter; }
            set
            {
                if (!ReferenceEquals(_letter, value))
                {
                    var previousValue = _letter;
                    _letter = value;
                    FixupLetter(previousValue);
                }
            }
        }
        private Letter _letter;
    
        public virtual DmdSendQueue DmdSendQueue
        {
            get { return _dmdSendQueue; }
            set
            {
                if (!ReferenceEquals(_dmdSendQueue, value))
                {
                    var previousValue = _dmdSendQueue;
                    _dmdSendQueue = value;
                    FixupDmdSendQueue(previousValue);
                }
            }
        }
        private DmdSendQueue _dmdSendQueue;

        #endregion

        #region Association Fixup
    
        private void FixupLetter(Letter previousValue)
        {
            if (previousValue != null && previousValue.LetterDocuments.Contains(this))
            {
                previousValue.LetterDocuments.Remove(this);
            }
    
            if (Letter != null)
            {
                if (!Letter.LetterDocuments.Contains(this))
                {
                    Letter.LetterDocuments.Add(this);
                }
                if (LetterId != Letter.LetterId)
                {
                    LetterId = Letter.LetterId;
                }
            }
        }
    
        private void FixupDmdSendQueue(DmdSendQueue previousValue)
        {
            if (previousValue != null && ReferenceEquals(previousValue.LetterDocument, this))
            {
                previousValue.LetterDocument = null;
            }
    
            if (DmdSendQueue != null)
            {
                DmdSendQueue.LetterDocument = this;
            }
        }

        #endregion

    }
}
