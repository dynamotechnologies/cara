﻿using System;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Represents a code range entity
    /// </summary>
    public class CodeRange
    {
        #region Properties
        public string CodeRangeFrom { get; set; }
        public string CodeRangeTo { get; set; }
        #endregion
    }
}
