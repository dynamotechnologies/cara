﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(OrganizationMetadata))]
    public partial class Organization
    {
		public string OrganizationTypeName
		{
			get
			{
				return OrganizationTypeId.HasValue ? LookupManager.GetLookup<LookupOrganizationType>()[OrganizationTypeId.Value].Name : string.Empty;
			}
		}
	}
}
