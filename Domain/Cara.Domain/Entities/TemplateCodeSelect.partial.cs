﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    public partial class TemplateCodeSelect
    {
        /// <summary>
        /// Returns the first character of the code number
        /// </summary>
        public string Prefix
        {
            get
            {
                return (!string.IsNullOrEmpty(CodeNumber) ? CodeNumber.Substring(0, 1) : string.Empty);
            }
        }

        /// <summary>
        /// Returns the code number in display format (without trailing zeros)
        /// </summary>
        public string CodeNumberDisplay
        {
            get
            {
                string number = CodeNumber;

                if (!string.IsNullOrEmpty(number))
                {
                    number = number.Replace(".0000.00", string.Empty).Replace("00.00", string.Empty).Replace(".00", string.Empty);
                }

                return number;
            }
        }

        /// <summary>
        /// Returns the direct children codes of the code
        /// </summary>
        public ICollection<TemplateCodeSelect> ChildCodes { get; set; }
    }
}
