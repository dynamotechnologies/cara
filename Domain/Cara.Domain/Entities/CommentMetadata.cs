﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the comment entity
    /// </summary>
    public class CommentMetadata
    {
        #region Metadata Fields
        [Required, DisplayName("Comment #")]
        public int CommentId { get; set; }

        [Required, DisplayName("Letter ID")]
        public int LetterId { get; set; }

        [DisplayName("Comment #")]
        public int LetterCommentNumber { get; set; }

        [DisplayName("Concern/Response #")]
        public int ConcernResponseId { get; set; }

        [DisplayName("Sample Comment")]
        public bool SampleStatement { get; set; }

        [DisplayName("Sample Comment")]
        public string SampleStatementText { get; set; }

        [DisplayName("Codes")]
        public string CommentCodeText { get; set; }

        [DisplayName("No Further Response Reason")]
        public string NoResponseReasonName { get; set; }

        [DisplayName("If Other")]
        public string NoResponseReasonOther { get; set; }

        [DisplayName("Text")]
        public string CommentText { get; set; }

        [DisplayName("Annotations")]
        public string Annotation { get; set; }

        [DisplayName("No Further Response Required")]
        public bool NoResponseRequired { get; set; }
        #endregion
    }
}
