﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    public partial class PhaseCodeSelect
    {
        /// <summary>
        /// Returns the Code Category object assocated with the ID
        /// </summary>
        public CodeCategory CodeCategory
        {
            get
            {
                return (CodeCategoryId.HasValue ? LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.CodeCategoryId == CodeCategoryId.Value).FirstOrDefault() : null);
            }
        }

        /// <summary>
        /// Returns the code number in display format (without trailing zeros)
        /// </summary>
        public string CodeNumberDisplay
        {
            get
            {
                string number = CodeNumber;

                if (!string.IsNullOrEmpty(number))
                {
                    number = number.Replace(".0000.00", string.Empty).Replace("00.00", string.Empty).Replace(".00", string.Empty);
                }

                return number;
            }
        }

        /// <summary>
        /// Returns the casted code type object from the phase code select
        /// </summary>
        public Code Code
        {
            get
            {
                return new Code
                {
                    CodeId = (this.CodeId.HasValue ? this.CodeId.Value : 0),
                    CodeCategoryId = (this.CodeCategoryId.HasValue ? this.CodeCategoryId.Value : 0),
                    CodeNumber = this.CodeNumber,
                    Name = this.CodeName,
                    ParentCodeId = this.ParentCodeId
                };
            }
        }

        /// <summary>
        /// Returns the casted phase code type object from the phase code select
        /// </summary>
        public PhaseCode PhaseCode
        {
            get
            {
                return new PhaseCode
                {
                    PhaseId = (this.PhaseId.HasValue ? this.PhaseId.Value : 0),
                    PhaseCodeId = (this.PhaseCodeId.HasValue ? this.PhaseCodeId.Value : 0),
                    CodeId = (this.CodeId.HasValue ? this.CodeId.Value : 0),
                    CodeCategoryId = (this.CodeCategoryId.HasValue ? this.CodeCategoryId.Value : 0),
                    CodeNumber = this.CodeNumber,
                    CodeName = this.CodeName,
                    ParentCodeId = this.ParentCodeId,
                    CanRemove = this.CanRemove,
                    UserId = this.UserId
                };
            }
        }

        /// <summary>
        /// Returns the direct children codes of the code
        /// </summary>
        public ICollection<PhaseCodeSelect> ChildCodes { get; set; }
    }
}
