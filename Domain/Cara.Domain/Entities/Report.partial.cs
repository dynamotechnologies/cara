﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Reporting;

namespace Aquilent.Cara.Domain.Report
{
    public partial class Report
    {
        #region Public Properties
        /// <summary>
        /// Returns true if report options exist
        /// </summary>
        public bool HasReportOptions
        {
            get
            {
                return (ReportOptions != null && ReportOptions.Count > 0);
            }
        }

		/// <summary>
		/// The report document represented by the report entity
		/// </summary>
		public IReportDocument Document { get; set; }
        #endregion
    }
}
