﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Documents;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(LetterObjectionMetadata))]
    public partial class LetterObjection
    {
        #region Public Properties
        /// <summary>
        /// Retrieves the name of the user using "First Last" Format (e.g., Bill Gates)
        /// </summary>
        public string ObjectionId
        {
            get
            {
                return string.Format("{0} {1} {2} {3} {4}", FiscalYear, Region, Forest, ObjectionNumber.ToString("D7"), RegulationNumber);
            }
        }

        public string ReviewStatusName
        {
            get
            {
                return LookupManager.GetLookup<LookupObjectionReviewStatus>()[ReviewStatusId].Name;
            }
        }

        public string OutcomeName
        {
            get
            {
                string name = string.Empty;
                if (OutcomeId.HasValue)
                {
                    name = LookupManager.GetLookup<LookupObjectionOutcome>()[OutcomeId.Value].Name;
                }

                return name;
            }
        }

        public string ObjectionDecisionMakerName
        {
            get
            {
                string name = string.Empty;
                if (ObjectionDecisionMakerId.HasValue)
                {
                    name = LookupManager.GetLookup<LookupObjectionDecisionMaker>()[ObjectionDecisionMakerId.Value].Name;
                }

                return name;
            }
        }

        /// <summary>
        /// Returns a list of letter documents
        /// </summary>
        private ICollection<Document> documents = null;
        public ICollection<Document> Documents
        {
            get
            {
                if (documents == null)
                {
                    IList<int> ids = new List<int>();
                    documents = new List<Document>();

                    if (LetterObjectionDocuments != null)
                    {
                        foreach (LetterObjectionDocument ld in LetterObjectionDocuments)
                        {
                            ids.Add(ld.DocumentId);
                        }

                        using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                        {
                            documents = docManager.Get(ids, "File");
                        }
                    }
                }

                return documents;
            }
        }
        #endregion
    }
}
