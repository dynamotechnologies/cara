﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the organization entity
    /// </summary>
    public class OrganizationMetadata
    {
        #region Metadata Fields
        [DisplayName("Organization Name")]
        public string Name { get; set; }
        
        [DisplayName("Organization Type")]
        public string OrganizationTypeName { get; set; }
        #endregion
    }
}
