﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the author entity
    /// </summary>
    public class LetterObjectionMetadata
    {
        #region Metadata Fields
        [DisplayName("Objection ID")]
        public string ObjectionId { get; set; }

        [DisplayName("Review Status")]
        public string ReviewStatusId { get; set; }

        [DisplayName("Overall Objection Outcome")]
        public int OutcomeId { get; set; }

        [StringLength(4000, ErrorMessage = "Objector Meeting Information must be no more than 4000 characters"), DisplayName("Objector Meeting Information")]
        public string MeetingInfo { get; set; }

        [Required]
        [DisplayName("Objection Number")]
        [Range(0, 9999999, ErrorMessage = "The Objection Number component of the Objection ID must be between 0 and 9999999")]
        public int ObjectionNumber { get; set; }

        [Required, DisplayName("Forest"), RegularExpression(@"\d{2}", ErrorMessage = "The Forest component of the Objection ID must be two digits")]
        public string Forest { get; set; }

        [StringLength(100, ErrorMessage = "The Signer First Name must be no more than 100 characters"), DisplayName("Reviewing Officer First Name")]
        public string SignerFirstName { get; set; }

        [StringLength(100, ErrorMessage = "The Signer Last Name must be no more than 100 characters"), DisplayName("Reviewing Officer Last Name")]
        public string SignerLastName { get; set; }

        [DisplayName("Reviewing Officer Title")]
        public int ObjectionDecisionMakerId { get; set; }

        [DisplayName("Response Date"), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ResponseDate { get; set; }
        #endregion
    }
}
