﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    // may rename to FormSetLetterSearchResultMetaData
    /// <summary>
    /// Metadata for the formset entity
    /// </summary>
    public class FormSetSearchResultMetadata
    {
        #region Metadata Fields
        [Required, DisplayName("Form Set ID")]
        public int FormSetId { get; set; }

        [DisplayName("Form Set Name")]
        public string FormSetName { get; set; }

        [DisplayName("Letter #")]
        public int LetterSequence { get; set; }

        [DisplayName("Letter Count")]
        public int LetterCount { get; set; }

        [DisplayName("Letter ID")]
        public int LetterId { get; set; }

        /*[DisplayName("Letter Type")]
        public string LetterTypeName { get; set; }*/

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }
        
        [DisplayName("Organization Name")]
        public string OrganizationName { get; set; }

        [DisplayName("Size (bytes)")]
        public int Size { get; set; }

        [DisplayName("Author Name")]
        public string AuthorName { get; set; }
        #endregion
    }
}
