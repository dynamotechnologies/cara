﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(ProjectMetadata))]
    public partial class Project
    {
        #region Class Members
        private User _projectManager;
        #endregion

        #region Public Properties
        public string ProjectNameNumber
        {
            get
            {
                return string.Format("{0} #{1}", Name, ProjectNumber);
            }
        }

        public string ProjectName
        {
            get
            {
                return string.Format("{0}", Name);
            }
        }


        public string UnitName
        {
            get
            {
				return LookupManager.GetLookup<LookupUnit>()[UnitId].Name;
            }
        }

        public string ProjectStatusName
        {
            get
            {
				return LookupManager.GetLookup<LookupProjectStatus>()[ProjectStatusId].Name;
            }
        }

        public string ProjectTypeName
        {
            get
            {
				return LookupManager.GetLookup<LookupProjectType>()[ProjectTypeId].Name;
            }
        }

        public string AnalysisTypeName
        {
            get
            {
				return LookupManager.GetLookup<LookupAnalysisType>()[AnalysisTypeId].Name;
            }
        }

        public string ProjectActivityNameList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                foreach (ProjectActivity pa in ProjectActivities)
                {
                    sb.Append((!string.IsNullOrEmpty(sb.ToString()) ? ", " : string.Empty) + pa.ActivityName);
                }

                return sb.ToString();
            }
        }

        public string ProjectPurposeNameList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                foreach (ProjectPurpose pp in ProjectPurposes)
                {
                    sb.Append((!string.IsNullOrEmpty(sb.ToString()) ? ", " : string.Empty) + pp.PurposeName);
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns a comma separated list of unit names (regions, forests, districts)
        /// </summary>
        /// <returns></returns>
        public string UnitNameList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(LocationNameList(Location.AreaTypeEnum.District));
                sb.Append((!string.IsNullOrEmpty(sb.ToString()) ? ", " : string.Empty) + LocationNameList(Location.AreaTypeEnum.Forest));
                sb.Append((!string.IsNullOrEmpty(sb.ToString()) ? ", " : string.Empty) + LocationNameList(Location.AreaTypeEnum.Region));

                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns a comma separated list of location names by area type
        /// </summary>
        /// <param name="areaType"></param>
        /// <returns></returns>
        public string LocationNameList(Location.AreaTypeEnum areaType)
        {
            StringBuilder sb = new StringBuilder();

            foreach (Location loc in Locations.Where(l => l.AreaTypeSpecific == areaType).OrderBy(x => x.AreaId))
            {
                sb.Append((!string.IsNullOrEmpty(sb.ToString()) ? ", " : string.Empty) + loc.Area);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns a list of project documents
        /// </summary>
        public IList<DmdDocument> Documents
        {
            get
            {
				IList<DmdDocument> documentList = null;

                using (var projManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    var dmManager = new DataMartManager();
                    dmManager.ProjectManager = projManager;

                    try
                    {
                        int palsProjectNumber = Convert.ToInt32(ProjectNumber);
                        documentList = dmManager.GetProjectDocuments(palsProjectNumber)
                            .OrderBy(x => int.Parse(x.PrimarySequence))
                            .ThenBy(x => int.Parse(x.Sequence))
                            .ToList();
                    }
                    catch (FormatException)
                    {
                        documentList = null;
                    }

                    dmManager.Dispose();
                }

				return documentList;
            }
        }

        /// <summary>
        /// Returns a list of project documents that are not associated with any phases
        /// </summary>
        public IList<DmdDocument> ProjectOnlyDocuments
        {
            get
            {
				IList<DmdDocument> documentList = null;

                using (var projManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    var dmManager = new DataMartManager();
                    dmManager.ProjectManager = projManager;

                    IList<DmdDocument> dmProjectDocs;

                    try
                    {
                        // get project documents from data mart
                        int palsProjectNumber = Convert.ToInt32(ProjectNumber);
                        dmProjectDocs = dmManager.GetProjectDocuments(palsProjectNumber);
                    }
                    catch (FormatException)
                    {
                        dmProjectDocs = null;
                    }

                    if (dmProjectDocs != null)
                    {
                        // select only the unassociated documents
                        var dmdIds = ProjectDocuments.Where(x => !x.PhaseId.HasValue).Select(x => x.DmdId);

                        if (ProjectDocuments != null)
                        {
                            documentList = dmProjectDocs.Where(x => dmdIds.Contains(x.DmdDocumentId)).ToList();
                        }
                    }

                    dmManager.Dispose();
                }

				return documentList;
            }
        }

		public bool AllowFormal
		{
			get
			{
				return PhaseMinLength.AllowFormal;
			}
		}

		public bool AllowObjection
		{
			get
			{
				return PhaseMinLength.AllowObjection;
			}
		}

		public PhaseMinimumLength PhaseMinLength
		{
			get
			{
                PhaseMinimumLength pml;

                if (CommentRuleId.HasValue)
                {
                    pml = LookupManager.GetLookup<LookupPhaseMinimumLength>().GetValue(AnalysisTypeId, CommentRuleId.Value);
                }
                else
                {
                    pml = new PhaseMinimumLength();
                    pml.PhaseMinimumDays = 0;
                    pml.ObjectionPeriodMinimumDays = 0;
                }
                return pml;
			}
		}

        public bool IsPalsProject
        {
            get
            {
                return ProjectTypeId == 1;
            }
        }
		#endregion
    }
}
