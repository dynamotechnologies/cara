﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;

namespace Aquilent.Cara.Domain
{
    public partial class LetterDocument
    {
        #region Class Members
        private Document _document;
        #endregion

        #region Public Properties
        public Document Document
        {
            get
            {
                // get document from db if it doesn't exist
                if (_document == null)
                {
                    using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        _document = docManager.Get(DocumentId);
                    }
                }

                return _document;
            }

            set
            {
                _document = value;
            }
        }
        #endregion
    }
}
