﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    public partial class ProjectPurpose
    {
		public string PurposeName
		{
			get
			{
				return LookupManager.GetLookup<LookupPurpose>()[PurposeId].Name;
			}
		}

	}
}
