﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(ConcernResponseMetadata))]
    public partial class ConcernResponse
    {
        #region Public Properties
        public string ConcernResponseStatusName
        {
            get
            {
				return LookupManager.GetLookup<LookupConcernResponseStatus>()[this.ConcernResponseStatusId].Name;
            }
        }
        #endregion
    }
}
