﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Domain.Report
{
    public class ResponseToCommentReportData
    {
        #region Public Properties
        public IEnumerable<Comment> Comments { get; set; }
        public string ConcernText { get; set; }
        public string ResponseText { get; set; }
        public int ResponseSequence { get; set; }
        public int ResponseNumber { get; set; }
        public int ResponseId { get; set; }
        public bool HasSampleStatement { get; set; }

        public string ResponseTextNonHtml
        {
            get
            {
                return Dentitize(ResponseText);
            }
        }

        public string ConcernTextNonHtml
        {
            get
            {
                return Dentitize(ConcernText);
            }
        }
        #endregion

        private string Dentitize(string value)
        {
            return value != null ? HtmlAgilityPack.HtmlEntity.DeEntitize(Regex.Replace(Regex.Replace(value, "(<[^>]*(>|$))+", " "), @"[\s\r\n]+", " ").Trim()) : string.Empty;
        }

        #region Not sure what to do here yet
        public class Comment
        {
            public ICollection<CommentText> CommentText { private get; set; }
            public int CommentNumber { get; set; }
            public int LetterNumber { get; set; }
            public bool SampleStatement { get; set; }

            public string UniqueNumber
            {
                get
                {
                    return string.Format("{0}-{1}", LetterNumber, CommentNumber);
                }
            }

            public string ConcatenatedCommentText
            {
                get
                {
                    StringBuilder ct = new StringBuilder();
                    for (int i = 0; i < CommentText.Count; i++)
                    {
                        ct.Append(CommentText.ElementAt(i).Text);

                        if (i + 1 < CommentText.Count)
                        {
                            ct.Append("...");
                        }
                    }

                    return ct.ToString();
                }
            }
        }
        #endregion Not sure what to do here yet
    }
}
