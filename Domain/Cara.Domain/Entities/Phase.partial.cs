﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Domain.DataMart;
using System.Configuration;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(PhaseMetadata))]
    public partial class Phase
    {
        #region Public Properties
        public string PhaseTypeName
        {
            get
            {
				return LookupManager.GetLookup<LookupPhaseType>()[this.PhaseTypeId].Name;
            }
        }

        public string PhaseTypeDesc
        {
            get
            {
                var phaseTypeLookupName = LookupManager.GetLookup<LookupPhaseType>()[this.PhaseTypeId].Name;
                string description;
                if (string.IsNullOrEmpty(Name))
                {
                    description = phaseTypeLookupName;
                }
                else
                {
                    description = string.Format("{0} ({1})", phaseTypeLookupName, Name);
                }

				return description;
            }
        }

        /// <summary>
        /// Returns a list of DmdDocuments of the ProjectDocuments
        /// </summary>
        public IList<DmdDocument> Documents
        {
            get
            {
                IList<DmdDocument> documentList = null;

                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    if (Project != null)
                    {
                        var dmdIds = ProjectDocuments.Select(x => x.DmdId).ToList();

                        if (Project.IsPalsProject)
                        {
                            var documents = Project.Documents.Where(x => dmdIds.Contains(x.DmdDocumentId))
                                .OrderBy(x => x.PrimarySequence)
                                .ThenBy(x => x.Sequence);
                            documentList = new List<DmdDocument>(documents.Count());
                            foreach (var doc in documents)
                            {
                                var dmdDocument = new DmdDocument
                                {
                                    DmdDocumentId = doc.DmdDocumentId,
                                    Name = doc.Name,
                                    Size = doc.Size,
                                    WwwLink = string.Format("{0}/{1}_{2}.pdf",
                                        DataMartManager.DocumentBaseUrl,
                                        Project.ProjectDocumentId,
                                        doc.DmdDocumentId)
                                };

                                documentList.Add(dmdDocument);
                            }
                        }
                        else
                        {
                            // For non-pals projects, the documents are not as easy to retrieve from the DMD,
                            // so we get them from the CARA database.
                            IList<Document> phaseDocumentList = null;
                            using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                            {
                                var phaseDocuments = documentManager.All.Where(x => dmdIds.Contains(x.DmdId)).Select(x => new { Document = x, File = x.File }).ToList();
                                if (phaseDocuments.Count() > 0)
                                {
                                    phaseDocumentList = phaseDocuments.Select(pd => pd.Document).ToList();
                                }
                            }

                            if (phaseDocumentList != null)
                            {
                                DmdDocument dmdDocument;
                                documentList = new List<DmdDocument>(phaseDocumentList.Count);
                                foreach (Document doc in phaseDocumentList)
                                {
                                    dmdDocument = new DmdDocument
                                    {
                                        DmdDocumentId = doc.DmdId,
                                        Name = doc.Name,
                                        Size = doc.File.Size.ToString(),
                                        WwwLink = string.Format("/Public/DownloadCommentPeriodDocument/{0}?dmdId={1}",
                                            PhaseId,
                                            doc.DmdId)
                                    };

                                    documentList.Add(dmdDocument);
                                }
                            }
                        }
                    }
                }

                return documentList;
            }
        }

        /// <summary>
        /// Returns the days of duration between comment start date and end date
        /// </summary>
        public int Duration
        {
            get
            {
                // set the duration based on comment period end date
                var d1 = CommentStart;
                var d2 = CommentEnd;
                TimeSpan span = d2 - d1;

                return (int) span.TotalDays;
            }
        }
        
        /// <summary>
        /// Returns the Accessibility Status display name
        /// </summary>
        public string AccessStatusName
        {
            get
            {
                return (Private ? "Team Only" : "All CARA Users");
            }
        }

        /// <summary>
        /// Returns the Comment Analysis Status display name
        /// </summary>
        public string StatusName
        {
            get
            {
                return (Locked ? "Archived" : (DateTime.UtcNow >= CommentStart ? "Active" : "Not Yet Active"));
            }
        }

        /// <summary>
        /// Returns the Reading Room Active display name
        /// </summary>
        public string ReadingRoomActiveName
        {
            get
            {
                return (ReadingRoomActive ? "Active" : "Inactive");
            }
        }

        /// <summary>
        /// Returns the Reading Room Active display name
        /// </summary>
        public string PublicCommentActiveName
        {
            get
            {
                return (PublicCommentActive ? "Active" : "Inactive");
            }
        }

        /// <summary>
        /// Returns true if the commend period is active
        /// </summary>
        public bool IsActive
        {
            get
            {
                return ("Active".Equals(StatusName));
            }
        }

        public bool IsFormal
        {
            get
            {
                return PhaseTypeName == PhaseType.Formal.Name || PhaseTypeName == PhaseType.NoticeOfAvailability.Name;
            }
        }

        public bool IsInformal
        {
            get
            {
                return !IsFormal && PhaseTypeName != PhaseType.Objection.Name;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Determines whether the given UTC date is within the comment period.
        /// </summary>
        /// <param name="utcDate"></param>
        /// <returns></returns>
        public bool IsWithinCommentPeriod(DateTime utcDate, string phaseTimeZone)
        {
            bool withinCurrentPhase = false;
            var commentStart = CommentStart;
            DateTime commentEnd;
            string configTimeZone = string.Empty;
            TimeZoneInfo phaseTimeZoneInfo;

            try
            {
                commentEnd = CommentEnd.AddDays(1); // the comment period includes this day
            }
            catch
            {
                commentEnd = CommentEnd;
            }

            if (utcDate.Kind == DateTimeKind.Local)
            {
                utcDate = TimeZoneInfo.ConvertTimeToUtc(utcDate);
            }

            //retrieve default time zone from config if necessary
            if (String.IsNullOrEmpty(phaseTimeZone))
            {
                try
                {
                    configTimeZone = ConfigurationManager.AppSettings["CommentPeriodTimeZoneEnd"];
                }
                catch (ConfigurationErrorsException cee)
                {
                    configTimeZone = "UTC-11"; //key for the default time zone in case config can't be read
                }
                phaseTimeZone = configTimeZone;
            }

            phaseTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(phaseTimeZone);

            var convertedUtcDate = TimeZoneInfo.ConvertTimeFromUtc(utcDate, phaseTimeZoneInfo);

            if (convertedUtcDate >= commentStart && convertedUtcDate <= commentEnd)
            {
                withinCurrentPhase = true;
            }

            return withinCurrentPhase;
        }
        #endregion Methods
    }
}
