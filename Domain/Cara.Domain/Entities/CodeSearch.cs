﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Represents a code search entity
    /// </summary>
    [Serializable]
    public class CodeSearch
    {
        #region Properties
        public string CodeSearchText { get; set; }
        public string CodeName { get; set; }
        public string CodeNumber { get; set; }
        public List<CodeRange> CodeRanges { get; set; }

        /// <summary>
        /// Returns the xml string of the code ranges
        /// </summary>
        public string CodeRangeListXml
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (CodeRanges != null && CodeRanges.Count > 0)
                {
                    sb.Append(@"<codeRanges>");
                    foreach (CodeRange cr in CodeRanges)
                    {
                        sb.Append(string.Format("<codeRange><from>{0}</from><to>{1}</to></codeRange>", cr.CodeRangeFrom, cr.CodeRangeTo));
                    }
                    sb.Append(@"</codeRanges>");
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns the code number to a full code number (e.g. 110.0000.00 for 110 or 110* for 110*)
        /// </summary>
        public string FullCodeNumber
        {
            get
            {
                string codeNum = CodeNumber;

                if (!string.IsNullOrEmpty(codeNum) && !codeNum.EndsWith("%"))
                {
					// Remove the dots first to make things easier.
					codeNum = codeNum.Replace(".", string.Empty);

                    // pad the number with digit
                    codeNum = codeNum.PadRight(9, '0');

                    // add the dots
                    codeNum = codeNum.Insert(3, ".").Insert(8, ".");
                }

                return codeNum;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Empty constructor
        /// </summary>
        public CodeSearch()
        {            
        }

        /// <summary>
        /// Special constructor with code search text as argument
        /// </summary>
        /// <param name="codeSearchText"></param>
        public CodeSearch(string codeSearchText)
        {
            this.CodeSearchText = codeSearchText;

            Init();
        }
        #endregion

        #region Init Methods
        /// <summary>
        /// Initialize the object by parsing the code search text
        /// </summary>
        private void Init()
        {
            if (!string.IsNullOrEmpty(CodeSearchText))
            {
                string[] elements = CodeSearchText.Split(',');

                if (elements != null && elements.Length > 0)
                {
                    CodeRanges = new List<CodeRange>();
                    // regular expression
                    var regex = new Regex(@"(^\d{3}\*{0,1}$)|(^\d{3}\.\d{1,4}\*{0,1}$)|(^\d{3}\.\d{4}\.\d{1,2}\*{0,1}$)", RegexOptions.IgnoreCase);

                    foreach (string s in elements)
                    {
                        string sTrim = s.Trim();
                        // it's a range if it contains a dash
                        if (s.Contains('-'))
                        {
                            string[] r = s.Split('-');

                            if (r != null && r.Length > 1)
                            {
                                CodeRanges.Add(new CodeRange
                                {
                                    // code range from is always a non-wildcard search since it's using greater than for comparison
                                    CodeRangeFrom = ToFullRangedCodeNumber(r[0].Trim().TrimEnd('*')),
                                    // code range to can be a wildcard or non-wildcard search
                                    CodeRangeTo = ToFullRangedCodeNumber(r[1].Trim())
                                });
                            }
                        }
                        // it's a code number single search if match the reg ex
                        else if (regex.IsMatch(sTrim))
                        {
                            //CARA-1238 multiple single searches maybe separated by commas, if so add them to code ranges.
                            if (CodeNumber == null)
                            {
                                CodeNumber = ReplaceWildCardChar(sTrim);
                            }
                            else
                            {
                                CodeRange originalCode = new CodeRange
                                {
                                    CodeRangeFrom = ToFullRangedCodeNumber(CodeNumber.TrimEnd('*')),
                                    CodeRangeTo = ToFullRangedCodeNumber(CodeNumber)
                                };
                                if (!CodeRanges.Contains(originalCode))
                                {
                                    CodeRanges.Add(originalCode);
                                }
                                CodeRanges.Add(new CodeRange
                                {
                                    CodeRangeFrom = ToFullRangedCodeNumber(sTrim.TrimEnd('*')),
                                    CodeRangeTo = ToFullRangedCodeNumber(sTrim)
                                });
                            }
                        }
                        else
                        {
                            CodeName = ReplaceWildCardChar(s.Trim());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Replace application defined wild card character (*) with a SQL wild card character (%)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string ReplaceWildCardChar(string text)
        {
            return (!string.IsNullOrEmpty(text) ? text.Replace('*', '%') : string.Empty);
        }

        /// <summary>
        /// Converts the input range code number to a full code number (e.g. 110.0000.00 for 110 or 110.9999.99 for 110*)
        /// </summary>
        /// <param name="codeNumber"></param>
        /// <returns></returns>
        private string ToFullRangedCodeNumber(string codeNumber)
        {
            string codeNum = codeNumber.Replace(".", string.Empty);

            if (!string.IsNullOrEmpty(codeNum))
            {
                // for wildcard search, padded character is the max number beginning with the code number phrase
                char padChar = codeNum.EndsWith("*") ? '9' : '0';

                // remove any wild card character
                codeNum = codeNum.TrimEnd('*');

                // pad the number with digit
                codeNum = codeNum.PadRight(9, padChar);

                // add the dots
                codeNum = codeNum.Insert(3, ".").Insert(8, ".");
            }

            return codeNum;
        }
        #endregion
    }
}
