﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the concern/response search result entity
    /// </summary>
    public class ConcernResponseSearchResultMetadata
    {
        #region Metadata Fields
        [Required, DisplayName("Concern/Response #")]
        public int ConcernResponseId { get; set; }

        [DisplayName("Concern/Response #")]
        public int ConcernResponseNumber { get; set; }

        [StringLength(8000), DisplayName("Concern")]
        public string ConcernText { get; set; }

        [StringLength(8000), DisplayName("Response")]
        public string ResponseText{ get; set; }
        
        [DisplayName("Status")]
        public string ConcernResponseStatusName { get; set; }

        [DisplayName("Code")]
        public string FormattedCode { get; set; }
        #endregion
    }
}
