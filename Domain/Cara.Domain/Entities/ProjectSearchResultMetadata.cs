﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the project search result entity
    /// </summary>
    public class ProjectSearchResultMetadata
    {
        #region Metadata Fields
        [Required, StringLength(100)]
        public string ProjectName { get; set; }

        [Required, StringLength(50), DisplayName("Project Number")]
        public string ProjectNumber { get; set; }

        [DisplayName("Project Name (ID)")]
        public string ProjectNameNumber { get; set; }

        [Required]
        public string UnitId { get; set; }

        [DisplayName("Lead Mgmt. Unit")]
        public string UnitName { get; set; }

        [Required]
        public int AnalysisTypeId { get; set; }

        [DisplayName("Analysis Type")]
        public string AnalysisTypeName { get; set; }

        [Required]
        public int ProjectStatusId { get; set; }

        [DisplayName("Status")]
        public string ProjectStatusName { get; set; }

        [DisplayName("Comment Period")]
        public string PhaseTypeName { get; set; }
        
        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("Comment Period Start Date")]
        public DateTime CommentStart { get; set; }

        [DisplayName("New Letters")]
        public Nullable<int> PhaseNewLetterCount { get; set; }

        [DisplayName("Early Attention")]
        public Nullable<int> PhaseEarlyActionLetterCount { get; set; }

        [DisplayName("Coding In Progress")]
        public Nullable<int> PhaseCodingInProgressCount { get; set; }

        [DisplayName("Coding Complete")]
        public Nullable<int> PhaseCodingCompleteCount { get; set; }

        [DisplayName("Unique Letters")]
        public Nullable<int> PhaseUniqueLettersCount { get; set; }

        [DisplayName("Duplicate Letters")]
        public Nullable<int> PhaseDuplicateLettersCount { get; set; }

        [DisplayName("Form Letters")]
        public Nullable<int> PhaseFormLettersCount { get; set; }

        [DisplayName("Total Letters")]
        public Nullable<int> PhaseTotalLettersCount { get; set; }
        #endregion
    }
}
