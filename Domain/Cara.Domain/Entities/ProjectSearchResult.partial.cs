﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(ProjectSearchResultMetadata))]
    public partial class ProjectSearchResult
    {
        #region Public Properties
        public string ProjectNameNumber
        {
            get
            {
                return string.Format("{0} #{1}", ProjectName, ProjectNumber);
            }
        }

        public string CommentStartShortDate
        {
            get
            {
                return (CommentStart.HasValue ? CommentStart.Value.ToShortDateString() : string.Empty);
            }
        }

        public string CommentEndShortDate
        {
            get
            {
                return (CommentEnd.HasValue ? CommentEnd.Value.ToShortDateString() : string.Empty);
            }
        }
        #endregion
    }
}
