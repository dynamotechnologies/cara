﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(LetterSearchResultMetadata))]
    public partial class LetterSearchResult
    {
        #region Public Properties
        /// <summary>
        /// Retrieves the name of the user using "Last, First" Format (e.g. Gates, Bill)
        /// </summary>
        public string AuthorName
        {
            get
            {
                return string.Format("{0}{1} {2}", LastName, (!string.IsNullOrEmpty(LastName) ? "," : string.Empty), FirstName).Trim();
            }
        }

        public string WithinCommentPeriodName
        {
            get
            {
                return (WithinCommentPeriod ? "Yes" : "No");
            }
        }

        public string DateSubmittedStr
        {
            get
            {
                return string.Format("{0:MM/dd/yyyy}", DateSubmitted);
            }
        }
        #endregion
    }
}
