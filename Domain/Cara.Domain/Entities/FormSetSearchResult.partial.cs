﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(FormSetSearchResultMetadata))]
    public partial class FormSetSearchResult
    {
        #region Public Properties
        /// <summary>
        /// Retrieves the name of the user using "Last, First" Format (e.g., Gates, Bill)
        /// </summary>
        public string AuthorName
        {
            get
            {
                return string.Format("{0}, {1}", LastName, FirstName);
            }
        }

        /// <summary>
        /// Returns true if the letter is the master form of the form set it belongs to
        /// </summary>
		public bool IsMasterForm
		{
			get
			{
                return LetterId == MasterFormId;
			}
		}

        /// <summary>
        /// Returns the key string containing the information to group a collection of form sets
        /// </summary>
        public string FormSetKeyCount
        {
            get
            {
                return string.Format("[id:{0}]{1} (Total Forms: {2})", (FormSetId.HasValue ? FormSetId.Value : 0).ToString(),
                    FormSetName, (LetterCount.HasValue ? LetterCount.Value : 0).ToString());
            }
        }
        #endregion
    }
}
