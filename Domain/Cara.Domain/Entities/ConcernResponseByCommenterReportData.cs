//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain.Report
{
    public partial class ConcernResponseByCommenterReportData
    {
        #region Primitive Properties
    
        public string LastName
        {
            get;
            set;
        }
    
        public string FirstName
        {
            get;
            set;
        }
    
        public string OrganizationName
        {
            get;
            set;
        }
    
        public Nullable<int> LetterSequence
        {
            get;
            set;
        }
    
        public string ConcernResponseNumber
        {
            get;
            set;
        }
    
        public int TotalRows
        {
            get;
            set;
        }

        #endregion
    }
}
