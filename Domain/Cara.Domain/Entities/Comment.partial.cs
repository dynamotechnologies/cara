﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(CommentMetadata))]
    public partial class Comment
    {
        #region Properties
        private bool noResponseRequired = false;

        /// <summary>
        /// Returns the Letter sequence comment number display
        /// </summary>
        public string LetterCommentNumber
        {
            get
            {
                return (string.Format("{0}-{1}", Letter.LetterSequence, CommentNumber));
            }
        }

        /// <summary>
        /// Returns a period separated list of comment text in a string
        /// </summary>
        public string CommentText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                foreach (CommentText ct in CommentTexts)
                {
                    text.Append((text.Length > 0 ? "[...]" : string.Empty) + ct.Text);
                }

                return text.ToString();
            }
        }

        /// <summary>
        /// Returns a piped separated list of comment codes
        /// </summary>
        public string CommentCodeText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                foreach (CommentCode cc in CommentCodes)
                {
                    text.Append((text.Length > 0 ? "|" : string.Empty) + string.Format("{0} {1}", cc.PhaseCode.CodeNumberDisplay, cc.PhaseCode.CodeName));
                }

                return text.ToString();
            }
        }

        public string NoResponseReasonName
        {
            get
            {
				return (NoResponseReasonId.HasValue ? LookupManager.GetLookup<LookupNoResponseReason>()[NoResponseReasonId.Value].Name : string.Empty);
            }
        }

        public string SampleStatementText
        {
            get
            {
                return (SampleStatement ? "Yes" : "No");
            }
        }

        public bool NoResponseRequired
        {
            get
            {
                return (noResponseRequired ? noResponseRequired : NoResponseReasonId.HasValue);
            }
            
            set
            {
                noResponseRequired = value;
            }
        }
        #endregion
    }
}
