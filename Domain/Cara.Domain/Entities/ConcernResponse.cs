//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain
{
    public partial class ConcernResponse
    {
        #region Primitive Properties
    
        public virtual int ConcernResponseId
        {
            get;
            set;
        }
    
        public virtual int ConcernResponseStatusId
        {
            get;
            set;
        }
    
        public virtual string ConcernText
        {
            get;
            set;
        }
    
        public virtual Nullable<int> Sequence
        {
            get;
            set;
        }
    
        public virtual string ResponseText
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> ResponseCreated
        {
            get;
            set;
        }
    
        public virtual string ResponseCreatedBy
        {
            get;
            set;
        }
    
        public virtual System.DateTime ConcernCreated
        {
            get;
            set;
        }
    
        public virtual string ConcernCreatedBy
        {
            get;
            set;
        }
    
        public virtual System.DateTime LastUpdated
        {
            get;
            set;
        }
    
        public virtual string LastUpdatedBy
        {
            get;
            set;
        }
    
        public virtual Nullable<int> ConcernResponseNumber
        {
            get;
            set;
        }
    
        public virtual Nullable<int> CommentCount
        {
            get;
            set;
        }
    
        public virtual Nullable<int> PhaseCodeId
        {
            get { return _phaseCodeId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_phaseCodeId != value)
                    {
                        if (PhaseCode != null && PhaseCode.PhaseCodeId != value)
                        {
                            PhaseCode = null;
                        }
                        _phaseCodeId = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _phaseCodeId;
    
        public virtual Nullable<int> UserId
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DateAssigned
        {
            get;
            set;
        }
    
        public virtual string Label
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<Comment> Comments
        {
            get
            {
                if (_comments == null)
                {
                    var newCollection = new FixupCollection<Comment>();
                    newCollection.CollectionChanged += FixupComments;
                    _comments = newCollection;
                }
                return _comments;
            }
            set
            {
                if (!ReferenceEquals(_comments, value))
                {
                    var previousValue = _comments as FixupCollection<Comment>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupComments;
                    }
                    _comments = value;
                    var newValue = value as FixupCollection<Comment>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupComments;
                    }
                }
            }
        }
        private ICollection<Comment> _comments;
    
        public virtual PhaseCode PhaseCode
        {
            get { return _phaseCode; }
            set
            {
                if (!ReferenceEquals(_phaseCode, value))
                {
                    var previousValue = _phaseCode;
                    _phaseCode = value;
                    FixupPhaseCode(previousValue);
                }
            }
        }
        private PhaseCode _phaseCode;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupPhaseCode(PhaseCode previousValue)
        {
            if (previousValue != null && previousValue.ConcernResponses.Contains(this))
            {
                previousValue.ConcernResponses.Remove(this);
            }
    
            if (PhaseCode != null)
            {
                if (!PhaseCode.ConcernResponses.Contains(this))
                {
                    PhaseCode.ConcernResponses.Add(this);
                }
                if (PhaseCodeId != PhaseCode.PhaseCodeId)
                {
                    PhaseCodeId = PhaseCode.PhaseCodeId;
                }
            }
            else if (!_settingFK)
            {
                PhaseCodeId = null;
            }
        }
    
        private void FixupComments(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Comment item in e.NewItems)
                {
                    item.ConcernResponse = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Comment item in e.OldItems)
                {
                    if (ReferenceEquals(item.ConcernResponse, this))
                    {
                        item.ConcernResponse = null;
                    }
                }
            }
        }

        #endregion

    }
}
