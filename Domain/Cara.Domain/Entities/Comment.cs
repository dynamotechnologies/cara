//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain
{
    public partial class Comment
    {
        #region Primitive Properties
    
        public virtual int CommentId
        {
            get;
            set;
        }
    
        public virtual int LetterId
        {
            get { return _letterId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_letterId != value)
                    {
                        if (Letter != null && Letter.LetterId != value)
                        {
                            Letter = null;
                        }
                        _letterId = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _letterId;
    
        public virtual System.DateTime IdentifiedOn
        {
            get;
            set;
        }
    
        public virtual string IdentifiedBy
        {
            get;
            set;
        }
    
        public virtual string Annotation
        {
            get;
            set;
        }
    
        public virtual System.DateTime LastUpdated
        {
            get;
            set;
        }
    
        public virtual string LastUpdatedBy
        {
            get;
            set;
        }
    
        public virtual Nullable<int> ConcernResponseId
        {
            get { return _concernResponseId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_concernResponseId != value)
                    {
                        if (ConcernResponse != null && ConcernResponse.ConcernResponseId != value)
                        {
                            ConcernResponse = null;
                        }
                        _concernResponseId = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _concernResponseId;
    
        public virtual Nullable<int> NoResponseReasonId
        {
            get;
            set;
        }
    
        public virtual string NoResponseReasonOther
        {
            get;
            set;
        }
    
        public virtual bool SampleStatement
        {
            get;
            set;
        }
    
        public virtual Nullable<int> CommentNumber
        {
            get;
            set;
        }
    
        public virtual Nullable<int> UserId
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DateAssigned
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<CommentCode> CommentCodes
        {
            get
            {
                if (_commentCodes == null)
                {
                    var newCollection = new FixupCollection<CommentCode>();
                    newCollection.CollectionChanged += FixupCommentCodes;
                    _commentCodes = newCollection;
                }
                return _commentCodes;
            }
            set
            {
                if (!ReferenceEquals(_commentCodes, value))
                {
                    var previousValue = _commentCodes as FixupCollection<CommentCode>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCommentCodes;
                    }
                    _commentCodes = value;
                    var newValue = value as FixupCollection<CommentCode>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCommentCodes;
                    }
                }
            }
        }
        private ICollection<CommentCode> _commentCodes;
    
        public virtual ICollection<CommentText> CommentTexts
        {
            get
            {
                if (_commentTexts == null)
                {
                    var newCollection = new FixupCollection<CommentText>();
                    newCollection.CollectionChanged += FixupCommentTexts;
                    _commentTexts = newCollection;
                }
                return _commentTexts;
            }
            set
            {
                if (!ReferenceEquals(_commentTexts, value))
                {
                    var previousValue = _commentTexts as FixupCollection<CommentText>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCommentTexts;
                    }
                    _commentTexts = value;
                    var newValue = value as FixupCollection<CommentText>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCommentTexts;
                    }
                }
            }
        }
        private ICollection<CommentText> _commentTexts;
    
        public virtual ConcernResponse ConcernResponse
        {
            get { return _concernResponse; }
            set
            {
                if (!ReferenceEquals(_concernResponse, value))
                {
                    var previousValue = _concernResponse;
                    _concernResponse = value;
                    FixupConcernResponse(previousValue);
                }
            }
        }
        private ConcernResponse _concernResponse;
    
        public virtual Letter Letter
        {
            get { return _letter; }
            set
            {
                if (!ReferenceEquals(_letter, value))
                {
                    var previousValue = _letter;
                    _letter = value;
                    FixupLetter(previousValue);
                }
            }
        }
        private Letter _letter;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupConcernResponse(ConcernResponse previousValue)
        {
            if (previousValue != null && previousValue.Comments.Contains(this))
            {
                previousValue.Comments.Remove(this);
            }
    
            if (ConcernResponse != null)
            {
                if (!ConcernResponse.Comments.Contains(this))
                {
                    ConcernResponse.Comments.Add(this);
                }
                if (ConcernResponseId != ConcernResponse.ConcernResponseId)
                {
                    ConcernResponseId = ConcernResponse.ConcernResponseId;
                }
            }
            else if (!_settingFK)
            {
                ConcernResponseId = null;
            }
        }
    
        private void FixupLetter(Letter previousValue)
        {
            if (previousValue != null && previousValue.Comments.Contains(this))
            {
                previousValue.Comments.Remove(this);
            }
    
            if (Letter != null)
            {
                if (!Letter.Comments.Contains(this))
                {
                    Letter.Comments.Add(this);
                }
                if (LetterId != Letter.LetterId)
                {
                    LetterId = Letter.LetterId;
                }
            }
        }
    
        private void FixupCommentCodes(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CommentCode item in e.NewItems)
                {
                    item.Comment = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CommentCode item in e.OldItems)
                {
                    if (ReferenceEquals(item.Comment, this))
                    {
                        item.Comment = null;
                    }
                }
            }
        }
    
        private void FixupCommentTexts(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CommentText item in e.NewItems)
                {
                    item.Comment = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CommentText item in e.OldItems)
                {
                    if (ReferenceEquals(item.Comment, this))
                    {
                        item.Comment = null;
                    }
                }
            }
        }

        #endregion

    }
}
