﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(LocationMetadata))]
    public partial class Location
    {
		public enum AreaTypeEnum { Unit, State, County, Region, Forest, District, Unknown };

		public AreaTypeEnum AreaTypeSpecific
        {
			get
			{
				AreaTypeEnum ate = AreaTypeEnum.Unknown;

				if (AreaType == AreaTypeEnum.Unit.ToString())
				{
                    if (AreaId.Length == 4)
					{
						ate = AreaTypeEnum.Region;
					}
                    else if (AreaId.Length == 6)
					{
						ate = AreaTypeEnum.Forest;
					}
                    else if (AreaId.Length == 8)
					{
						ate = AreaTypeEnum.District;
					}
				}
				else if (AreaType == AreaTypeEnum.State.ToString())
				{
					ate = AreaTypeEnum.State;
				}
				else if (AreaType == AreaTypeEnum.County.ToString())
				{
                    ate = AreaTypeEnum.County;
				}

				return ate;
			}
        }

		public string Area
		{
			get
			{
				string name = string.Empty;
				if (AreaType == AreaTypeEnum.Unit.ToString())
				{
					name = LookupManager.GetLookup<LookupUnit>()[AreaId].Name;
				}
				else if (AreaType == AreaTypeEnum.State.ToString())
				{
					name = LookupManager.GetLookup<LookupState>()[AreaId].Name;
				}
				else if (AreaType == AreaTypeEnum.County.ToString())
				{
					name = LookupManager.GetLookup<LookupCounty>()[AreaId].Name;
				}

				return name;
			}
		}
    }
}
