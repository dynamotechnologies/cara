﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    public partial class CommentDetails
    {
        /// <summary>
        /// Returns the Letter sequence comment number display
        /// </summary>
        public string LetterCommentNumber
        {
            get
            {
                return (string.Format("{0}-{1}", LetterSequence, CommentNumber));
            }
        }

        /// <summary>
        /// Returns a string indicating whether the select checkbox should be disabled based on the concern/response
        /// </summary>
        public string Disabled
        {
            get
            {
                return (ConcernResponseId.HasValue ? "disabled" : string.Empty);
            }
        }

        public string SampleStatementDisplay
        {
            get
            {
                return (SampleStatement ? "Yes" : "No");
            }
        }

        /// <summary>
        /// Returns a html formatted comment code text as a string
        /// </summary>
        public string CommentCodeTextHtml
        {
            get
            {
				// "|" delimiter comes from dbo.fnGetCommentCodeText
                return (!string.IsNullOrEmpty(CommentCodeText) ? CommentCodeText.Replace("|", "<br />") : string.Empty);
            }
        }

        /// <summary>
        /// Returns a html formatted comment code text as a display string
        /// </summary>
        public string CommentCodeTextHtmlDisplay
        {
            get
            {
                return (!string.IsNullOrWhiteSpace(CommentCodeTextHtml) ? CommentCodeTextHtml : string.Empty);
            }
        }
     
        public string FormattedConcernResponseCode
        {
            get
            {
                string value = string.Empty;

                if (!string.IsNullOrEmpty(ConcernResponseCodeNumber))
                {
                    value = string.Format("{0} {1}", Code.FormatCodeNumber(ConcernResponseCodeNumber), ConcernResponseCodeName);
                }

                return value;
            }
        }
    }
}
