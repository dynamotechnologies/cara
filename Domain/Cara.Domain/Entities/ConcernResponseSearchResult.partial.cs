﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(ConcernResponseSearchResultMetadata))]
    public partial class ConcernResponseSearchResult
    {
        #region Public Properties
        public string LastUpdateText
        {
            get
            {
                return LastUpdated.ToShortDateString();
            }
        }

        public string FormattedCode
        {
            get
            {
                return Code.FormatCodeNumber(CodeNumber) + " " + CodeName;
            }
        }
        #endregion
    }
}
