﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the letter entity
    /// </summary>
    public class LetterMetadata
    {
        #region Metadata Fields
        [Required, DisplayName("Letter ID")]
        public int LetterId { get; set; }

        [DisplayName("Letter #")]
        public int LetterSequence { get; set; }

        [DisplayName("Letter Text")]
        public string Text { get; set; }

        [DisplayName("Letter Text")]
        public string HtmlText { get; set; }

        [Required, DisplayName("Letter Type")]
        public int LetterTypeId { get; set; }

        [DisplayName("Delivery Type")]
        public int DeliveryTypeId { get; set; }

        [DisplayName("Common Interest Class")]
        public int CommonInterestClassId { get; set; }

        [DisplayName("Letter Type")]
        public string LetterTypeName { get; set; }

        [DisplayName("Coding Status")]
        public string LetterStatusName { get; set; }

        [DisplayName("Delivery Type")]
        public string DeliveryTypeName { get; set; }

        [DisplayName("Common Interest Class")]
        public string CommonInterestClassName { get; set; }

        [DisplayName("Early Attention")]
        public int EarlyActionStatusId { get; set; }

        [DisplayName("Within Comment Period")]
        public bool WithinCommentPeriod { get; set; }

        [DisplayName("Size (bytes)")]
        public int Size { get; set; }

        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("Date Submitted")]
        public DateTime DateSubmitted { get; set; }

        [DisplayName("Last Updated")]
        public DateTime LastUpdated { get; set; }

        [DisplayName("Last Updated By")]
        public string LastUpdatedBy { get; set; }

        [DisplayName("Authors")]
        public string AuthorNameList { get; set; }

        [DisplayName("Publish Status")]
        public string PublishToReadingRoomName { get; set; }
        #endregion
    }
}
