﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(AuthorMetadata))]
    public partial class Author
    {
        public const string ANON = "Anon";

        #region Public Properties
        /// <summary>
        /// Retrieves the name of the user using "First Last" Format (e.g., Bill Gates)
        /// </summary>
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }
        
        /// <summary>
        /// Retrieves the name of the user using "Last, First" Format (e.g., Gates, Bill)
        /// </summary>
        public string FullNameLastFirst
        {
            get
            {
                return string.Format("{0}, {1}", LastName, FirstName);
            }
        }

        public string StateName
        {
            get
            {
				return (!string.IsNullOrEmpty(StateId) ? LookupManager.GetLookup<LookupState>()[StateId].Name : string.Empty);
            }
        }

        public string CountryName
        {
            get
            {
				return (CountryId.HasValue ? LookupManager.GetLookup<LookupCountry>()[CountryId.Value].Name : string.Empty);
            }
        }

        public string OfficialRepresentativeTypeName
        {
            get
            {
                return (OfficialRepresentativeTypeId.HasValue ?
					LookupManager.GetLookup<LookupOfficialRepresentativeType>()[OfficialRepresentativeTypeId.Value].Name : string.Empty);
            }
        }

        public string FullAddress
        {
            get
            {
                return (string.Format("{0} {1} {2} {3} {4}",
                        Address1, Address2 + (!string.IsNullOrEmpty(Address1 + Address2) ? "," : string.Empty),
                        City + (!string.IsNullOrEmpty(City) ? "," : string.Empty),
                        StateName + (!string.IsNullOrEmpty(StateName) ? "," : string.Empty),
                        ZipPostal).Trim());
            }
        }

        /// <summary>
        /// Set last and first name to anon if anonymous is true
        /// </summary>
        /// <param name="author">The Author</param>
        /// <param name="anonymous">Anonymous Flag</param>
        public static void FixAuthorName(Author author, bool anonymous = false)
        {
            if (author != null)
            {
                // if anonymous, set full name to anon's
                if (anonymous)
                {
                    author.LastName = author.FirstName = ANON;
                }
                // if not anonymous, set the name to anon if it's empty
                else
                {
                    if (string.IsNullOrWhiteSpace(author.FirstName))
                    {
                        author.FirstName = ANON;
                    }

                    if (string.IsNullOrWhiteSpace(author.LastName))
                    {
                        author.LastName = ANON;
                    }
                }
            }
        }
        #endregion
    }
}
