﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the phase entity
    /// </summary>
    public class PhaseMetadata
    {
        #region Metadata Fields
        [StringLength(100), DisplayName("Description")]
        public string Name { get; set; }

        [Required, DisplayName("Comment Period")]
        public int PhaseTypeId { get; set; }

        [DisplayName("Comment Period")]
        public string PhaseTypeName { get; set; }

        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy (ddd)}"), DisplayName("Start Date")]
        public DateTime CommentStart { get; set; }

        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy (ddd)}"), DisplayName("End Date")]
        public DateTime CommentEnd { get; set; }

        [DisplayName("Viewing Status")]
        public string AccessStatusName { get; set; }

        [DisplayName("Comment Analysis Status")]
        public string StatusName { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy (ddd)}"), DisplayName("Response Due")]
        public DateTime? ObjectionResponseDue { get; set; }

        [DisplayName("Response Extended By")]
        public string ObjectionResponseExtendedBy { get; set; }

        [DisplayName("Reason for Extension")]
        public string ObjectionResponseExtendedJustification { get; set; }
        #endregion
    }
}
