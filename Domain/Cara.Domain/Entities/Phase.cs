//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain
{
    public partial class Phase
    {
        #region Primitive Properties
    
        public virtual int PhaseId
        {
            get;
            set;
        }
    
        public virtual int ProjectId
        {
            get { return _projectId; }
            set
            {
                if (_projectId != value)
                {
                    if (Project != null && Project.ProjectId != value)
                    {
                        Project = null;
                    }
                    _projectId = value;
                }
            }
        }
        private int _projectId;
    
        public virtual string Name
        {
            get;
            set;
        }
    
        public virtual bool Locked
        {
            get;
            set;
        }
    
        public virtual bool Private
        {
            get;
            set;
        }
    
        public virtual System.DateTime CommentStart
        {
            get;
            set;
        }
    
        public virtual System.DateTime CommentEnd
        {
            get;
            set;
        }
    
        public virtual Nullable<bool> Deleted
        {
            get;
            set;
        }
    
        public virtual System.DateTime LastUpdated
        {
            get;
            set;
        }
    
        public virtual string LastUpdatedBy
        {
            get;
            set;
        }
    
        public virtual int PhaseTypeId
        {
            get;
            set;
        }
    
        public virtual bool ReadingRoomActive
        {
            get;
            set;
        }
    
        public virtual bool PublicCommentActive
        {
            get;
            set;
        }
    
        public virtual bool AlwaysOpen
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> ObjectionResponseDue
        {
            get;
            set;
        }
    
        public virtual Nullable<int> ObjectionResponseExtendedBy
        {
            get;
            set;
        }
    
        public virtual string ConcernTemplate
        {
            get;
            set;
        }
    
        public virtual string ResponseTemplate
        {
            get;
            set;
        }
    
        public virtual string PublicFormText
        {
            get;
            set;
        }
    
        public virtual string ObjectionResponseExtendedJustification
        {
            get;
            set;
        }
    
        public virtual string TimeZone
        {
            get;
            set;
        }
    
        public virtual string ContactName
        {
            get;
            set;
        }
    
        public virtual string MailAddressStreet1
        {
            get;
            set;
        }
    
        public virtual string MailAddressStreet2
        {
            get;
            set;
        }
    
        public virtual string MailAddressCity
        {
            get;
            set;
        }
    
        public virtual string MailAddressState
        {
            get;
            set;
        }
    
        public virtual string MailAddressZip
        {
            get;
            set;
        }
    
        public virtual string CommentEmail
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<Letter> Letters
        {
            get
            {
                if (_letters == null)
                {
                    var newCollection = new FixupCollection<Letter>();
                    newCollection.CollectionChanged += FixupLetters;
                    _letters = newCollection;
                }
                return _letters;
            }
            set
            {
                if (!ReferenceEquals(_letters, value))
                {
                    var previousValue = _letters as FixupCollection<Letter>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupLetters;
                    }
                    _letters = value;
                    var newValue = value as FixupCollection<Letter>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupLetters;
                    }
                }
            }
        }
        private ICollection<Letter> _letters;
    
        public virtual Project Project
        {
            get { return _project; }
            set
            {
                if (!ReferenceEquals(_project, value))
                {
                    var previousValue = _project;
                    _project = value;
                    FixupProject(previousValue);
                }
            }
        }
        private Project _project;
    
        public virtual ICollection<TermList> TermLists
        {
            get
            {
                if (_termLists == null)
                {
                    var newCollection = new FixupCollection<TermList>();
                    newCollection.CollectionChanged += FixupTermLists;
                    _termLists = newCollection;
                }
                return _termLists;
            }
            set
            {
                if (!ReferenceEquals(_termLists, value))
                {
                    var previousValue = _termLists as FixupCollection<TermList>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupTermLists;
                    }
                    _termLists = value;
                    var newValue = value as FixupCollection<TermList>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupTermLists;
                    }
                }
            }
        }
        private ICollection<TermList> _termLists;
    
        public virtual ICollection<PhaseMemberRole> PhaseMemberRoles
        {
            get
            {
                if (_phaseMemberRoles == null)
                {
                    var newCollection = new FixupCollection<PhaseMemberRole>();
                    newCollection.CollectionChanged += FixupPhaseMemberRoles;
                    _phaseMemberRoles = newCollection;
                }
                return _phaseMemberRoles;
            }
            set
            {
                if (!ReferenceEquals(_phaseMemberRoles, value))
                {
                    var previousValue = _phaseMemberRoles as FixupCollection<PhaseMemberRole>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupPhaseMemberRoles;
                    }
                    _phaseMemberRoles = value;
                    var newValue = value as FixupCollection<PhaseMemberRole>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupPhaseMemberRoles;
                    }
                }
            }
        }
        private ICollection<PhaseMemberRole> _phaseMemberRoles;
    
        public virtual ICollection<ProjectDocument> ProjectDocuments
        {
            get
            {
                if (_projectDocuments == null)
                {
                    var newCollection = new FixupCollection<ProjectDocument>();
                    newCollection.CollectionChanged += FixupProjectDocuments;
                    _projectDocuments = newCollection;
                }
                return _projectDocuments;
            }
            set
            {
                if (!ReferenceEquals(_projectDocuments, value))
                {
                    var previousValue = _projectDocuments as FixupCollection<ProjectDocument>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupProjectDocuments;
                    }
                    _projectDocuments = value;
                    var newValue = value as FixupCollection<ProjectDocument>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupProjectDocuments;
                    }
                }
            }
        }
        private ICollection<ProjectDocument> _projectDocuments;
    
        public virtual ICollection<PhaseCode> PhaseCodes
        {
            get
            {
                if (_phaseCodes == null)
                {
                    var newCollection = new FixupCollection<PhaseCode>();
                    newCollection.CollectionChanged += FixupPhaseCodes;
                    _phaseCodes = newCollection;
                }
                return _phaseCodes;
            }
            set
            {
                if (!ReferenceEquals(_phaseCodes, value))
                {
                    var previousValue = _phaseCodes as FixupCollection<PhaseCode>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupPhaseCodes;
                    }
                    _phaseCodes = value;
                    var newValue = value as FixupCollection<PhaseCode>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupPhaseCodes;
                    }
                }
            }
        }
        private ICollection<PhaseCode> _phaseCodes;
    
        public virtual ICollection<PhaseArtifact> PhaseArtifacts
        {
            get
            {
                if (_phaseArtifacts == null)
                {
                    var newCollection = new FixupCollection<PhaseArtifact>();
                    newCollection.CollectionChanged += FixupPhaseArtifacts;
                    _phaseArtifacts = newCollection;
                }
                return _phaseArtifacts;
            }
            set
            {
                if (!ReferenceEquals(_phaseArtifacts, value))
                {
                    var previousValue = _phaseArtifacts as FixupCollection<PhaseArtifact>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupPhaseArtifacts;
                    }
                    _phaseArtifacts = value;
                    var newValue = value as FixupCollection<PhaseArtifact>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupPhaseArtifacts;
                    }
                }
            }
        }
        private ICollection<PhaseArtifact> _phaseArtifacts;
    
        public virtual ICollection<FormManagementChangeLog> FormManagementChangeLogs
        {
            get
            {
                if (_formManagementChangeLogs == null)
                {
                    var newCollection = new FixupCollection<FormManagementChangeLog>();
                    newCollection.CollectionChanged += FixupFormManagementChangeLogs;
                    _formManagementChangeLogs = newCollection;
                }
                return _formManagementChangeLogs;
            }
            set
            {
                if (!ReferenceEquals(_formManagementChangeLogs, value))
                {
                    var previousValue = _formManagementChangeLogs as FixupCollection<FormManagementChangeLog>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupFormManagementChangeLogs;
                    }
                    _formManagementChangeLogs = value;
                    var newValue = value as FixupCollection<FormManagementChangeLog>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupFormManagementChangeLogs;
                    }
                }
            }
        }
        private ICollection<FormManagementChangeLog> _formManagementChangeLogs;
    
        public virtual ICollection<FormSet> FormSets
        {
            get
            {
                if (_formSets == null)
                {
                    var newCollection = new FixupCollection<FormSet>();
                    newCollection.CollectionChanged += FixupFormSets;
                    _formSets = newCollection;
                }
                return _formSets;
            }
            set
            {
                if (!ReferenceEquals(_formSets, value))
                {
                    var previousValue = _formSets as FixupCollection<FormSet>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupFormSets;
                    }
                    _formSets = value;
                    var newValue = value as FixupCollection<FormSet>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupFormSets;
                    }
                }
            }
        }
        private ICollection<FormSet> _formSets;
    
        public virtual ICollection<LetterUploadQueue> LetterUploadQueues
        {
            get
            {
                if (_letterUploadQueues == null)
                {
                    var newCollection = new FixupCollection<LetterUploadQueue>();
                    newCollection.CollectionChanged += FixupLetterUploadQueues;
                    _letterUploadQueues = newCollection;
                }
                return _letterUploadQueues;
            }
            set
            {
                if (!ReferenceEquals(_letterUploadQueues, value))
                {
                    var previousValue = _letterUploadQueues as FixupCollection<LetterUploadQueue>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupLetterUploadQueues;
                    }
                    _letterUploadQueues = value;
                    var newValue = value as FixupCollection<LetterUploadQueue>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupLetterUploadQueues;
                    }
                }
            }
        }
        private ICollection<LetterUploadQueue> _letterUploadQueues;

        #endregion

        #region Association Fixup
    
        private void FixupProject(Project previousValue)
        {
            if (previousValue != null && previousValue.Phases.Contains(this))
            {
                previousValue.Phases.Remove(this);
            }
    
            if (Project != null)
            {
                if (!Project.Phases.Contains(this))
                {
                    Project.Phases.Add(this);
                }
                if (ProjectId != Project.ProjectId)
                {
                    ProjectId = Project.ProjectId;
                }
            }
        }
    
        private void FixupLetters(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Letter item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Letter item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupTermLists(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (TermList item in e.NewItems)
                {
                    if (!item.Phases.Contains(this))
                    {
                        item.Phases.Add(this);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (TermList item in e.OldItems)
                {
                    if (item.Phases.Contains(this))
                    {
                        item.Phases.Remove(this);
                    }
                }
            }
        }
    
        private void FixupPhaseMemberRoles(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (PhaseMemberRole item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (PhaseMemberRole item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupProjectDocuments(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ProjectDocument item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ProjectDocument item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupPhaseCodes(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (PhaseCode item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (PhaseCode item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupPhaseArtifacts(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (PhaseArtifact item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (PhaseArtifact item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupFormManagementChangeLogs(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (FormManagementChangeLog item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (FormManagementChangeLog item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupFormSets(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (FormSet item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (FormSet item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }
    
        private void FixupLetterUploadQueues(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (LetterUploadQueue item in e.NewItems)
                {
                    item.Phase = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (LetterUploadQueue item in e.OldItems)
                {
                    if (ReferenceEquals(item.Phase, this))
                    {
                        item.Phase = null;
                    }
                }
            }
        }

        #endregion

    }
}
