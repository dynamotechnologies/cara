﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the LetterReadingRoomSearchResult entity
    /// </summary>
    public class LetterReadingRoomSearchResultMetadata
    {
        #region Metadata Fields
        [DisplayName("Letter #")]
        public int LetterSequence { get; set; }

        [DisplayName("Letter Type")]
        public string LetterTypeName { get; set; }

        [StringLength(100), DisplayName("First Name")]
        public string FirstName { get; set; }

        [StringLength(100), DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Author Name")]
        public string FullName { get; set; }

        [DisplayName("Size (bytes)")]
        public int Size { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("Date Submitted")]
        public DateTime DateSubmitted { get; set; }

        [DisplayName("Attachments")]
        public bool AttachmentCount { get; set; }
        #endregion
    }
}
