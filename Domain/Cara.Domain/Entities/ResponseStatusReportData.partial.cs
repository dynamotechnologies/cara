﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    public partial class ResponseStatusReportData
    {
        /// <summary>
        /// Returns the Letter sequence comment number display
        /// </summary>
        public string LetterCommentNumber
        {
            get
            {
                return (string.Format("{0}-{1}", LetterSequence, CommentNumber));
            }
        }

        public string GroupBy1
        {
            get
            {
                string output;
                if (Associated)
                {
                    output = string.Format("Concern Response #: {0}, Status: {1}", ConcernResponseNumber, StatusName);
                }
                else
                {
                    output = "Unassociated Comments";
                }
                return (output);
            }
        }

        //public string GroupByOrder2
        //{
        //    get
        //    {
        //        return (string.Format("{0},{1}", LetterCommentNumber, CommentCodeText));
        //    }
        //}

        public string CommentCodeText
        {
            get
            {
                StringBuilder cCText = new StringBuilder(CodeNumber);

                cCText.Replace(".0000.00", string.Empty);
                cCText.Replace("00.00", string.Empty);
                cCText.Replace(".00", string.Empty);
                cCText.Append(" " + CodeName);

                return (string.Format("{0}", cCText));

            }
        }

    }
}
