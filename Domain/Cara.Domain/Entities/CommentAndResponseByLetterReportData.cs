﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    public class CommentsAndResponsesByLetterReportData
    {
        private IList<string> _authors = new List<string>();
        public IList<string> Authors
        {
            get
            {
                return _authors;
            }
        }

        public string Sender
        {
            get
            {
                return string.Format("{0}, {1}", SenderLast, SenderFirst);
            }
        }

        public string SenderLast { get; set; }
        public string SenderFirst { get; set; }
        public string CommentText { get; set; }
        public string ResponseText { get; set; }
        public int? ResponseNumber { get; set; }

        public string AuthorList
        {
            get
            {
                var list = new StringBuilder();
                foreach (var author in Authors)
                {
                    list.Append(author);
                    list.Append("\n");
                }

                return list.ToString();
            }
        }
    }
}
