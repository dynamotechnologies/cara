﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(LetterMetadata))]
    public partial class Letter
    {
        #region Public Properties
        private Author _sender;
        public Author Sender
        {
            get
            {
                if (_sender == null)
                {
                    _sender = Authors.SingleOrDefault(author => (author.Sender == true));
                }

                return _sender;
            }
        }

        public string LetterTypeName
        {
            get
            {
                return (LetterTypeId > 0 ? LookupManager.GetLookup<LookupLetterType>()[LetterTypeId].Name: string.Empty);
            }
        }

        public string LetterStatusName
        {
            get
            {
				return LookupManager.GetLookup<LookupLetterStatus>()[LetterStatusId].Name;
            }
        }

        public string EarlyActionStatus
        {
            get
            {
                return (EarlyActionStatusId.HasValue ? LookupManager.GetLookup<LookupEarlyActionStatus>()[EarlyActionStatusId.Value].Name : string.Empty);
            }
        }

        public string DeliveryTypeName
        {
            get
            {
                return (DeliveryTypeId.HasValue ? LookupManager.GetLookup<LookupDeliveryType>()[DeliveryTypeId.Value].Name : string.Empty);
            }
        }

        public string CommonInterestClassName
        {
            get
            {
				return (CommonInterestClassId.HasValue ? LookupManager.GetLookup<LookupCommonInterestClass>()[CommonInterestClassId.Value].Name : string.Empty);
            }
        }

		public bool IsMasterForm
		{
			get
			{
                bool isMasterForm = false;
                FormSet formset = null;
                if (FormSetId.HasValue && FormSet == null)
                {
                    using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                    {
                        formset = formSetManager.Get(FormSetId.Value);
                    }
                }
                else if (FormSetId.HasValue && FormSet != null)
                {
                    formset = FormSet;
                }

                if (formset != null && formset.MasterFormId == LetterId)
                {
                    isMasterForm = true;
                }

                return isMasterForm;
			}
		}

        /// <summary>
        /// Returns the name of the letter publish status
        /// </summary>
        public string PublishToReadingRoomName
        {
            get
            {
                return PublishToReadingRoom && LetterTypeId != 1 /* Duplicate */ ? "Published" : "Un-published";
            }
        }

        /// <summary>
        /// Returns the name of the letter unpublished reason
        /// </summary>
        public string UnpublishedReasonName
        {
            get
            {
                string urn = UnpublishedReasonId.HasValue ? LookupManager.GetLookup<LookupUnpublishedReason>()[UnpublishedReasonId.Value].Name : string.Empty;

                return LetterTypeId != 1 ? urn : "Duplicate";
            }
        }

        /// <summary>
        /// Returns a list of letter documents
        /// </summary>
        public ICollection<Document> Documents
        {
            get
            {
                IList<int> ids = new List<int>();
                IList<Document> list = new List<Document>();

                if (LetterDocuments != null)
                {
                    foreach (LetterDocument ld in LetterDocuments)
                    {
                        ids.Add(ld.DocumentId);
                    }

                    using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        list = docManager.Get(ids, "File");
                    }
                }

                return list;
            }
        }

        /// <summary>
        /// Returns a comma separated list of all author names
        /// </summary>
        public string AuthorNameList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (Authors != null)
                {
                    foreach (Author author in Authors.OrderBy(x => x.Sender).ThenBy(x => x.FullName))
                    {
                        sb.Append((!string.IsNullOrWhiteSpace(sb.ToString()) ? ", " : string.Empty) + author.FullName + (author.Sender ? " (Sender)" : string.Empty));
                    }
                }

                return sb.ToString();
            }
        }
        #endregion
    }
}
