﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the phase search result entity
    /// </summary>
    public class PhaseSearchResultMetadata
    {
        #region Metadata Fields
        [DisplayName("Description")]
        public string Name { get; set; }

        [DisplayName("Comment Period")]
        public string PhaseTypeName { get; set; }

        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("Start Date")]
        public DateTime CommentStart { get; set; }

        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("End Date")]
        public DateTime CommentEnd { get; set; }

        [DisplayName("Viewing Status")]
        public string AccessStatusName { get; set; }

        [DisplayName("Comment Analysis Status")]
        public string StatusName { get; set; }
        #endregion
    }
}
