﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the project entity
    /// </summary>
    public class ProjectMetadata
    {
        #region Metadata Fields
        [Required, StringLength(100)]
        public string Name { get; set; }

        [Required, StringLength(50), DisplayName("Project Number")]
        public string ProjectNumber { get; set; }

        [DisplayName("Project Name (ID)")]
        public string ProjectNameNumber { get; set; }

        [Required]
        public string UnitId { get; set; }

        [DisplayName("Lead Mgmt. Unit")]
        public string UnitName { get; set; }

        [Required]
        public int AnalysisTypeId { get; set; }

        [DisplayName("Analysis Type")]
        public string AnalysisTypeName { get; set; }

        [DisplayName("Project Type")]
        public string ProjectTypeName { get; set; }

        [Required]
        public int ProjectStatusId { get; set; }

        [DisplayName("PALS Project Status")]
        public string ProjectStatusName { get; set; }

        [Required, StringLength(1000)]
        public string Description { get; set; }

        [DisplayName("Activities")]
        public string ProjectActivityNameList { get; set; }

        [DisplayName("Purposes")]
        public string ProjectPurposeNameList { get; set; }

        [DisplayName("SOPA Contact Name")]
        public string SOPAUser { get; set; }

        [DisplayName("Last Updated By")]
        public string LastUpdatedBy { get; set; }

        [DisplayName("Last Updated")]
        public DateTime LastUpdated { get; set; }

        [DisplayName("Date Created")]
        public DateTime CreateDate { get; set; }

        [DisplayName("Units")]
        public string UnitNameList { get; set; }
        #endregion
    }
}
