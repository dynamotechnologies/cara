﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Domain.Report
{
    public class CommentResponseNoConcernReportData
    {
        #region Public Properties
        public ICollection<CommentText> CommentText { private get; set; }
        public int CommentNumber { get; set; }
        public int LetterNumber { get; set; }
        public string ResponseText { get; set; }
        public int ResponseNumber { get; set; }

        public string UniqueNumber
        {
            get
            {
                return string.Format("{0}-{1}", LetterNumber, CommentNumber);
            }
        }

        public string ConcatenatedCommentText
        {
            get
            {
                StringBuilder ct = new StringBuilder();
                for (int i=0; i < CommentText.Count; i++)
                {
                    ct.Append(CommentText.ElementAt(i).Text);

                    if (i + 1 < CommentText.Count)
                    {
                        ct.Append("...");
                    }
                }

                return ct.ToString();
            }
        }

        public string ResponseTextNonHtml
        {
            get
            {
                return HtmlAgilityPack.HtmlEntity.DeEntitize(Regex.Replace(Regex.Replace(ResponseText, "(<[^>]*(>|$))+", " "), @"[\s\r\n]+", " ").Trim());
            }
        }
        #endregion
    }
}
