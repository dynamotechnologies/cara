﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    public partial class PhaseMemberRole
    {
        #region Class Members
        private User _user = null;
        #endregion

        #region Public Properties
        public string RoleName
        {
            get
            {
				return LookupManager.GetLookup<LookupRole>()[this.RoleId].Name.Replace("Project", "Comment Period");
            }
        }

        public User User
        {
            set { _user = value; }

            get
            {
                if (_user == null)
                {
                    // get the user from db if it doesn't exist
                    using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                    {
                        _user = userManager.Get(UserId);
                    }
                }

                return _user;
            }
        }

        public string Status
        {
            get
            {
                return (Active ? "Active" : "Inactive");
            }
        }
        #endregion
    }
}
