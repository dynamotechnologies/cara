//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain
{
    public partial class PhaseMemberRole
    {
        #region Primitive Properties
    
        public virtual int PhaseId
        {
            get { return _phaseId; }
            set
            {
                if (_phaseId != value)
                {
                    if (Phase != null && Phase.PhaseId != value)
                    {
                        Phase = null;
                    }
                    _phaseId = value;
                }
            }
        }
        private int _phaseId;
    
        public virtual int UserId
        {
            get;
            set;
        }
    
        public virtual int RoleId
        {
            get;
            set;
        }
    
        public virtual bool Active
        {
            get;
            set;
        }
    
        public virtual System.DateTime LastUpdated
        {
            get;
            set;
        }
    
        public virtual string LastUpdatedBy
        {
            get;
            set;
        }
    
        public virtual bool CanRemove
        {
            get;
            set;
        }
    
        public virtual bool IsPointOfContact
        {
            get;
            set;
        }
    
        public virtual bool IsEarlyAttentionNewsletterSubscriber
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual Phase Phase
        {
            get { return _phase; }
            set
            {
                if (!ReferenceEquals(_phase, value))
                {
                    var previousValue = _phase;
                    _phase = value;
                    FixupPhase(previousValue);
                }
            }
        }
        private Phase _phase;

        #endregion

        #region Association Fixup
    
        private void FixupPhase(Phase previousValue)
        {
            if (previousValue != null && previousValue.PhaseMemberRoles.Contains(this))
            {
                previousValue.PhaseMemberRoles.Remove(this);
            }
    
            if (Phase != null)
            {
                if (!Phase.PhaseMemberRoles.Contains(this))
                {
                    Phase.PhaseMemberRoles.Add(this);
                }
                if (PhaseId != Phase.PhaseId)
                {
                    PhaseId = Phase.PhaseId;
                }
            }
        }

        #endregion

    }
}
