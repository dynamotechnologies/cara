﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(PhaseSearchResultMetadata))]
    public partial class PhaseSearchResult
    {
        #region Public Properties

        /// <summary>
        /// Returns Viewing Status display name
        /// </summary>
        public string AccessStatusName
        {
            get
            {
                return (Private ? "Team Only" : "All CARA Users");
            }
        }

        /// <summary>
        /// Returns Comment Analysis Status display name
        /// </summary>
        public string StatusName
        {
            get
            {
                return (Locked ? "Archived" : (DateTime.UtcNow >= CommentStart ? "Active" : "Not Yet Active"));
            }
        }

        /// <summary>
        /// Returns true if the commend period is active
        /// </summary>
        public bool IsActive
        {
            get
            {
                return ("Active".Equals(StatusName));
            }
        }
        #endregion
    }
}
