﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the location entity
    /// </summary>
    public class LocationMetadata
    {
        #region Metadata Fields
        [DisplayName("Region")]
        public string RegionName { get; set; }

        [DisplayName("Forest")]
        public string ForestName { get; set; }

        [DisplayName("District")]
        public string DistrictName { get; set; }

        [DisplayName("State")]
        public string StateName { get; set; }

        [DisplayName("County")]
        public string CountyName { get; set; }
        #endregion
    }
}
