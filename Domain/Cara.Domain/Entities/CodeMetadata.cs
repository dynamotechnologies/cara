﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the code entity
    /// </summary>
    public class CodeMetadata
    {
        #region Metadata Fields
        [Required, StringLength(20), DisplayName("Code")]
        public string CodeNumber { get; set; }

        [Required, StringLength(100), DisplayName("Title")]
        public string Name { get; set; }

        [DisplayName("Code Number")]
        public string CodeNumberDisplay { get; set; }
        #endregion
    }
}
