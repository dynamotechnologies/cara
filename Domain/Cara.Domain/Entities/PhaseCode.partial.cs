﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(PhaseCodeMetadata))]
    public partial class PhaseCode
    {
        /// <summary>
        /// Returns the Code Category object assocated with the ID
        /// </summary>
        public CodeCategory CodeCategory
        {
            get
            {
                return (CodeCategoryId > 0 ? LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.CodeCategoryId == CodeCategoryId).FirstOrDefault() : null);
            }
        }

        /// <summary>
        /// Returns the code number in display format (without trailing zeros)
        /// </summary>
        public string CodeNumberDisplay
        {
            get
            {
                return (!string.IsNullOrEmpty(CodeNumber) ? CodeNumber.Replace(".0000.00", string.Empty).Replace("00.00", string.Empty).Replace(".00", string.Empty) :
                    string.Empty);
            }
        }

        /// <summary>
        /// Returns the direct children codes of the code
        /// </summary>
        public ICollection<PhaseCode> ChildCodes { get; set; }
    }
}
