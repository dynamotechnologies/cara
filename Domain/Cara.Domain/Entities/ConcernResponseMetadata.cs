﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the concern/response entity
    /// </summary>
    public class ConcernResponseMetadata
    {
        #region Metadata Fields
        [Required, DisplayName("Concern/Response #")]
        public int ConcernResponseId { get; set; }

        [DisplayName("Concern/Response #")]
        public int ConcernResponseNumber { get; set; }
        
        [DisplayName("Concern Statement")]
        public string ConcernText { get; set; }

        [DisplayName("Response")]
        public string ResponseText { get; set; }
        
        [DisplayName("Status")]
        public string ConcernResponseStatusName { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("Last Updated on")]
        public DateTime LastUpdated { get; set; }

        [DisplayName("Last Updated by")]
        public string LastUpdatedBy { get; set; }

        [DisplayName("Code")]
        public int PhaseCodeId { get; set; }

        [DisplayName("Label"), StringLength(100)]
        public string Label { get; set; }
        #endregion
    }
}
