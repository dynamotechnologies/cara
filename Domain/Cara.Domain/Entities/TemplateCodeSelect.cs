//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Domain
{
    public partial class TemplateCodeSelect
    {
        #region Primitive Properties
    
        public Nullable<int> CodingTemplateId
        {
            get;
            set;
        }
    
        public int CodeId
        {
            get;
            set;
        }
    
        public Nullable<int> ParentCodeId
        {
            get;
            set;
        }
    
        public string Name
        {
            get;
            set;
        }
    
        public string CodeNumber
        {
            get;
            set;
        }
    
        public int CodeCategoryId
        {
            get;
            set;
        }
    
        public string CodeCategoryName
        {
            get;
            set;
        }

        #endregion

    }
}
