﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the letter entity
    /// </summary>
    public class LetterSearchResultMetadata
    {
        #region Metadata Fields
        [Required, DisplayName("Letter ID")]
        public int LetterId { get; set; }

        [DisplayName("Letter #")]
        public int LetterSequence { get; set; }

        [DisplayName("Author Name")]
        public string AuthorName { get; set; }

        [DisplayName("Letter Type")]
        public string LetterTypeName { get; set; }

        [DisplayName("Status")]
        public string LetterStatusName { get; set; }

        [Required, DisplayName("Date Submitted")]
        public DateTime DateSubmitted { get; set; }

        [DisplayName("Organization Type")]
        public string OrganizationTypeName { get; set; }

        [DisplayName("Organization Name")]
        public string OrganizationName { get; set; }

        [DisplayName("Early Attention")]
        public string EarlyActionStatus { get; set; }

        [DisplayName("Within Comment Period")]
        public string WithinCommentPeriodName { get; set; }

        [DisplayName("Size (bytes)")]
        public int Size { get; set; }
        #endregion
    }
}
