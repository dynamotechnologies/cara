﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    public partial class WebCommentPageInfo
    {
        #region Public Properties
        public string FullAddress
        {
            get
            {
                return (string.Format("{0} {1} {2} {3} {4}",
                        MailAddressStreet1, MailAddressStreet2 + (!string.IsNullOrEmpty(MailAddressStreet1 + MailAddressStreet2) ? "," : string.Empty),
                        MailAddressCity + (!string.IsNullOrEmpty(MailAddressCity) ? "," : string.Empty),
                        MailAddressState + (!string.IsNullOrEmpty(MailAddressState) ? "," : string.Empty),
                        MailAddressZip).Trim());
            }
        }

        public string UnitName
        {
            get
            {
                string name = string.Empty;
                var lookup = LookupManager.GetLookup<LookupUnit>();
                if (lookup.Any(x => x.UnitId == UnitId))
                {
                    name = lookup[UnitId].Name;
                }

                return name;
            }
        }
        #endregion
    }
}
