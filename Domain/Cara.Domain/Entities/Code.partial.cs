﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    [MetadataType(typeof(CodeMetadata))]
    public partial class Code
    {
        public string CodeCategoryName
        {
            get
            {
                return (CodeCategoryId > 0 ? LookupManager.GetLookup<LookupCodeCategory>()[CodeCategoryId].Name : string.Empty);
            }
        }

        public bool HasChildCodes
        {
            get
            {
                return (ChildCodes == null ? false : (ChildCodes.Count > 0 ? true : false));
            }
        }

        /// <summary>
        /// Returns the code number in display format (without trailing zeros)
        /// </summary>
        public string CodeNumberDisplay
        {
            get
            {
                string number = CodeNumber;

                return FormatCodeNumber(number);
            }
        }

        public static string FormatCodeNumber(string codeNumber)
        {
            if (!string.IsNullOrEmpty(codeNumber))
            {
                codeNumber = codeNumber.Replace(".0000.00", string.Empty).Replace("00.00", string.Empty).Replace(".00", string.Empty);
            }

            return codeNumber;
        }
    }
}
