﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Metadata for the my report entity
    /// </summary>
    public class MyReportMetadata
    {
        #region Metadata Fields
        [StringLength(100), DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Type")]
        public bool? Private { get; set; }
        
        [DisplayName("Type")]
        public string Type { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}"), DisplayName("Date Created")]
        public DateTime DateCreated { get; set; }
        #endregion
    }
}
