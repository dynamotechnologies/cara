﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain
{
    public enum LetterUploadStatus { Pending, Processing, Completed, Error };
    public enum LetterUploadFileType { PST, FDMS };

    public partial class LetterUploadQueue
    {
        public LetterUploadStatus LetterUploadStatus
        {
            get
            {
                LetterUploadStatus status;
                if (!Enum.TryParse<LetterUploadStatus>(Status, true, out status))
                {
                    throw new ApplicationException("Status is not defined");
                }

                return status;
            }

            set
            {
                Status = value.ToString();
            }
        }


        public LetterUploadFileType LetterUploadFileType
        {
            get
            {
                LetterUploadFileType filetype;
                if (!Enum.TryParse<LetterUploadFileType>(FileType, true, out filetype))
                {
                    throw new ApplicationException("FileType is not defined");
                }

                return filetype;
            }

            set
            {
                FileType = value.ToString();
            }
        }
    }
}
