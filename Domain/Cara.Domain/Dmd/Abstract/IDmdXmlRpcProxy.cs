﻿using System;
using CookComputing.XmlRpc;


namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// RPC Proxy client to the DMD service
    /// </summary>
	public interface IDmdXmlRpcProxy : IXmlRpcProxy
    {
        #region Interface methods
        [XmlRpcMethod("putFile")]
        string PutFile(byte[] document, FileMetaData request);

        [XmlRpcMethod("getFileData")]
		byte[] GetFileData(string docId);

        [XmlRpcMethod("getFileData")]
		byte[] GetFileData(string docId, FileDataRequest request);

        [XmlRpcMethod("getFileMetadata")]
		FileMetaData GetFileMetadata(string docId);

        [XmlRpcMethod("getFileMetadata")]
		FileMetaData GetFileMetadata(string docId, FileDataRequest request);

		[XmlRpcMethod("deleteFile")]
		bool DeleteFile(string docId);

        [XmlRpcMethod("searchDocs")]
        FileList SearchDocs(string querystring, SearchDocOption options);

		[XmlRpcMethod("replaceFileMetadata")]
		bool ReplaceFileMetatdata(string docId, NewFileMetaData newFileMetaData, ReplaceFileMetaDataOptions options);

        [XmlRpcMethod("zipPhaseDocs")]
        bool ZipPhaseLetters(string projectId, string phaseId, string shortname);

        [XmlRpcMethod("zipFormSet")]
        bool ZipFormLetters(string projectId, string phaseId, string shortname, string formSetName, string[] dmdIDs);

        [XmlRpcMethod("publishFile")]
        bool PublishFile(string agent, string docId);

        [XmlRpcMethod("putMergedFile")]
        string PutMergedFile(string[] docIds, FileMetaData request);

        [XmlRpcMethod("echoMsg")]
        string EchoMsg(string msg);
        #endregion
    }
}
