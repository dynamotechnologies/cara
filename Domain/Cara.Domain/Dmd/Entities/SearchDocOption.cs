﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a group of search doc options
    /// </summary>
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class SearchDocOption
    {
        [XmlRpcMissingMapping(MappingAction.Error)]
        public string indexname;
        public int countitems;
        public int startitem;
        public int pagenum;
        public int collapse;
        public int[] projectids;
    }
}
