﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a file request
    /// </summary>
	public class FileMetaData
    {
		[XmlRpcMember("appsysid")]
		public string AppSysId { get; set; } 

		[XmlRpcMember("appdoctype")]
		public string AppDocType { get; set; }

		[XmlRpcMember("filename")]
		public string Filename { get; set; }

		[XmlRpcMember("title")]
		public string Title { get; set; }

		[XmlRpcMember("author")]
		public string Author { get; set; }

		[XmlRpcMember("pubdate")]
		public DateTime PublicationDate { get; set; }

		[XmlRpcMember("filesize")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public int? FileSize { get; set; }

		[XmlRpcMember("moreMetadata")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public MoreFileMetaData MoreMetaData { get; set; }

        [XmlRpcMember("dmdId")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string DmdId { get; set; }
    }
}
