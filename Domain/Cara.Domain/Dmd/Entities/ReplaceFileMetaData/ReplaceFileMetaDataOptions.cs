﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a group of search doc options
    /// </summary>
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class ReplaceFileMetaDataOptions
    {
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		[XmlRpcMember("deleteMM")]
		public string[] deletedMoreMetaDataFields;

		[XmlRpcMissingMapping(MappingAction.Ignore)]
		[XmlRpcMember("mergeMM")]
		public bool? mergeMoreMetadataValues;
    }
}
