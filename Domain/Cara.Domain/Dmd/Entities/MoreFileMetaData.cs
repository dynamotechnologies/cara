﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a extra file metadata list response
    /// </summary>
	public class MoreFileMetaData
    {
		[XmlRpcMember("projectid")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string ProjectId { get; set; }

		[XmlRpcMember("docdate")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string DocumentDate { get; set; }

		[XmlRpcMember("datelabel")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
		public string DateLabel { get; set; }

		[XmlRpcMember("keyflag")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
		public string KeyFlag { get; set; }

		[XmlRpcMember("level2name")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
		public string Level2Name { get; set; }

		[XmlRpcMember("level3name")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string Level3Name { get; set; }

		[XmlRpcMember("level2id")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
		public string Level2Id { get; set; }

		[XmlRpcMember("level3id")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string Level3Id { get; set; }

		/// <summary>
		/// A letter with 2 attachments, would be "1 of 3"
		/// </summary>
		[XmlRpcMember("attachment")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string IsAttachment { get; set; }

		[XmlRpcMember("caraletterid")]
		[XmlRpcMissingMapping(MappingAction.Ignore)]
		public string LetterId { get; set; }

        [XmlRpcMember("caraid")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string CaraProjectId { get; set; }

        [XmlRpcMember("caraphaseid")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string CaraPhaseId { get; set; }

        [XmlRpcMember("objrule")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string ObjectionRule { get; set; }

        [XmlRpcMember("responsesigner")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string ResponseSigner { get; set; }

        [XmlRpcMember("objector")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string Objector { get; set; }
    }
}
