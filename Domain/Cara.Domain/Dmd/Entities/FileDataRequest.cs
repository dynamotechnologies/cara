﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a file data request
    /// </summary>
    public class FileDataRequest
    {
		[XmlRpcMember("fileType")]
		public string FileType { get; set; }

        // other optional parameters
    }
}
