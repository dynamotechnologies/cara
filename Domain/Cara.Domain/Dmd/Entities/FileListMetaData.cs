﻿using System;
using CookComputing.XmlRpc;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Class represents a file metadata list response
    /// </summary>
    public class FileListMetaData
    {
		[XmlRpcMember("rownum")]
		public int RowNumber { get; set; }

		[XmlRpcMember("relevance")]
		public int Relevance { get; set; }

		[XmlRpcMember("collapsed")]
		public bool Collapsed { get; set; }

		[XmlRpcMember("docid")]
		public string DmdDocId { get; set; }

		[XmlRpcMember("htmlcontext")]
		public string HtmlContext { get; set; }
        //public struct metadata;   // metadata and moremetadata for hit
    }
}
