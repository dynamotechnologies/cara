﻿using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.Dmd;
using Aquilent.Cara.Reference;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using CookComputing.XmlRpc;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Telerik.Reporting.Processing;
using Aquilent.EntityAccess;
using System.Configuration;
//using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// RPC Proxy class providing operations to the DMD service
    /// </summary>
	public class DmdManager
	{
		#region Properties
		private ILog _logger = LogManager.GetLogger(typeof(DmdManager));
		private DmdServiceConfiguration _config = ConfigUtilities.GetSection<DmdServiceConfiguration>("dmdServiceConfiguration");

		IDmdXmlRpcProxy _xmlRpcProxy;
		private IDmdXmlRpcProxy DmdService
		{
			get
			{
				if (_xmlRpcProxy == null)
				{
					_xmlRpcProxy = XmlRpcProxyGen.Create<IDmdXmlRpcProxy>();
					_xmlRpcProxy.Credentials = _config.Credentials;
					_xmlRpcProxy.Url = _config.Url;

                    bool attachXmlRpcLogger = false;

                    if (bool.TryParse(ConfigurationManager.AppSettings["AttachXmlRpcLogger"], out attachXmlRpcLogger) && attachXmlRpcLogger)
                    {
                        DmdXmlRpcLogger xmlRpcLogger = new DmdXmlRpcLogger();
                        xmlRpcLogger.Attach(_xmlRpcProxy);
                    }
				}

				return _xmlRpcProxy;
			}
		}
		#endregion Properties

        #region Constants
        public readonly int FILE_NOT_FOUND_ERROR = 803;
        #endregion Constants

        #region PutFile Methods
        /// <summary>
        /// Instantiate and call the RPC proxy and return the dmd id of the input file.
		/// NOTE:  This is really just a test method as this has no practical use in CARA
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>doc id</returns>
		public string PutFile(FileInfo file, string title, string author)
		{
			string dmdId = string.Empty;

			if (file != null)
			{
				try
				{
					if (DmdService != null)
					{
						FileMetaData request = new FileMetaData();

						// set the request data
						request.AppSysId = _config.AppSysId;
						request.AppDocType = _config.LetterDefaults.AppDocType;
						request.Filename = file.Name;
						request.Title = title;
						request.Author = author;
						request.PublicationDate = DateTime.UtcNow;

						request.MoreMetaData = new MoreFileMetaData
						{
							ProjectId = "0001",
							DocumentDate = request.PublicationDate.ToIso8601Format(),
							DateLabel = _config.LetterDefaults.DateLabel,
							KeyFlag = "0",
							Level2Name = _config.LetterDefaults.Level2Name,
							Level3Name = _config.LetterDefaults.Level3Name,
							Level2Id = _config.LetterDefaults.Level2Id,
							Level3Id = _config.LetterDefaults.Level3Id
						};

						// submit the request and get the doc id
						using (var stream = file.OpenRead())
						{
							var fileContents = new byte[file.Length];
							stream.Read(fileContents, 0, Convert.ToInt32(file.Length));
							_logger.Info(string.Format("DMD > Put File > file: {0}, title: {1}, author: {2}", file.Name, title, author));
							dmdId = DmdService.PutFile(fileContents, request);
						}
					}
				}
				catch (XmlRpcFaultException ex)
				{
					string message = string.Format("An error occurred trying to send a file to the DMD: {0} - {1}:: {2}",
						ex.FaultCode, ex.FaultString, ex.Message);
					_logger.Error(message);
					throw new DomainException(message, ex);
				}
				catch (Exception ex)
				{
					throw new DomainException("An error occurred trying to send a file to the DMD", ex);
				}
			}

			return dmdId;
		}

		/// <summary>
		/// Used to send a letter to the DMD.
        /// 
        /// Note:  This does not send the attachments
		/// </summary>
		/// <param name="letter"></param>
		/// <returns></returns>
		public string PutLetter(Letter letter, string projectNumber, int projectId, bool isPalsProject, bool isObjection)
		{
            string dmdId = null;
            var letterMetaData = letter.GetMetaData(projectNumber, _config, projectId, isPalsProject, isObjection);

            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf16 = Encoding.Unicode;
            byte[] utfBytes = utf16.GetBytes(letter.ToDmdFormat());
            byte[] isoBytes = Encoding.Convert(utf16, iso, utfBytes);

            // Convert the letter to base64 string
            var letterTextBase64 = isoBytes;

            // Send the letter to the DMD
            try
            {
                dmdId = DmdService.PutFile(letterTextBase64, letterMetaData);
                if (string.IsNullOrEmpty(dmdId))
                {
                    _logger.ErrorFormat("The dmdId returned from letterId = {0} is NULL", letter.LetterId);
                }
                else
                {
                    _logger.InfoFormat("DMD > Put Letter > letter: {0}, {1}", letter.LetterId, dmdId);
                }
            }
            catch (XmlRpcFaultException ex)
            {
                string message = string.Format("An error occurred trying to send a letter (letterId = {0}) to the DMD: {1} - {2}:: {3}",
                    letter.LetterId, ex.FaultCode, ex.FaultString, ex.Message);
                throw new LetterException(message, ex, letter.LetterId);
            }
            catch (Exception ex)
            {
                throw new LetterException(string.Format("An error occurred trying to send a letter to the DMD (letterId = {0})", letter.LetterId), ex, letter.LetterId);
            }

            return dmdId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="projectNumber"></param>
        /// <param name="docManager"></param>
        public string PutLetterAttachment(Letter letter, string projectNumber, Document document, DocumentManager docManager, int projectId, bool isPalsProject, bool isObjection)
        {
            string dmdId = null;
            var letterMetaData = letter.GetMetaData(projectNumber, _config, projectId, isPalsProject, isObjection, document);

            #region Put the attachments
            byte[] buffer;
            MemoryStream stream;

            try
            {
                buffer = new byte[document.File.Size];
                using (stream = new MemoryStream(buffer, 0, buffer.Length, true, true))
                {
                    docManager.DownloadFile(document, stream);
                    stream.Position = 0;
                    dmdId = DmdService.PutFile(stream.GetBuffer(), letterMetaData);

                    if (string.IsNullOrEmpty(dmdId))
                    {
                        _logger.ErrorFormat("The dmdId returned from letterId {0}, documentId {1} is NULL",
                            letter.LetterId, document.DocumentId);
                    }
                    else
                    {
                        _logger.Info(string.Format("DMD > Put Letter > Attachment > letter: {0}, documentId: {1}: {2}",
                            letter.LetterId, document.DocumentId, dmdId));
                    }
                }
            }
            catch (XmlRpcFaultException ex)
            {
#if DEBUG
                if (ex.FaultCode != 1010)
                {
#endif
                    string message = string.Format("An error occurred trying to send attachment (documentId = {3}) for letter (ID = {0}) to the DMD: {1} - {2}:: {3}",
                        letter.LetterId, ex.FaultCode, ex.FaultString, document.DocumentId, ex.Message);
                    _logger.Error(message);
                    throw new LetterException(message, ex, letter.LetterId);
#if DEBUG
                }
#endif
            }
            catch (Exception ex)
            {
                throw new LetterException(string.Format("An error occurred trying to send a letter to the DMD (letterId = {0}, documentId = {1})", letter.LetterId, document.DocumentId), ex, letter.LetterId);
            }
            #endregion Put the attachments

            return dmdId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="projectNumber"></param>
        /// <param name="docManager"></param>
        public string PutObjectionAttachment(
            LetterObjection letterObjection,
            Letter letter,
            string objectorName,
            string projectNumber, 
            string objectRule,
            Document document,
            DocumentManager docManager)
        {
            string dmdId = null;
            var metadata = letterObjection.GetObjectionMetaData(letter, projectNumber, objectorName, objectRule, _config, document);

            #region Put the attachments
            byte[] buffer;
            MemoryStream stream;

            try
            {
                buffer = new byte[document.File.Size];
                using (stream = new MemoryStream(buffer, 0, buffer.Length, true, true))
                {
                    docManager.DownloadFile(document, stream);
                    stream.Position = 0;
                    dmdId = DmdService.PutFile(stream.GetBuffer(), metadata);

                    if (string.IsNullOrEmpty(dmdId))
                    {
                        _logger.ErrorFormat("The dmdId returned from letterId {0}, documentId {1} is NULL",
                            letter.LetterId, document.DocumentId);
                    }
                    else
                    {
                        _logger.Info(string.Format("DMD > Put Letter > Objection Attachment > letter: {0}, documentId: {1}: {2}",
                            letter.LetterId, document.DocumentId, dmdId));
                    }
                }
            }
            catch (XmlRpcFaultException ex)
            {
#if DEBUG
                if (ex.FaultCode != 1010)
                {
#endif
                    string message = string.Format("An error occurred trying to send attachment (documentId = {3}) for letter (ID = {0}) to the DMD: {1} - {2}:: {3}",
                        letter.LetterId, ex.FaultCode, ex.FaultString, document.DocumentId, ex.Message);
                    _logger.Error(message);
                    throw new LetterException(message, ex, letter.LetterId);
#if DEBUG
                }
#endif
            }
            catch (Exception ex)
            {
                throw new LetterException(string.Format("An error occurred trying to send a letter to the DMD (letterId = {0}, documentId = {1})", letter.LetterId, document.DocumentId), ex, letter.LetterId);
            }
            #endregion Put the attachments

            return dmdId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="projectNumber"></param>
        /// <param name="docManager"></param>
        public string MergeObjectionAttachments(
            LetterObjection letterObjection,
            Letter letter,
            string objectorName,
            string projectNumber, 
            string objectRule,
            IList<string> dmdIds)
        {
            string dmdId = null;

            // Ensure PDFs exist for each dmdId
            FileMetaData fileMetadata = null;
            foreach (var id in dmdIds)
            {
                fileMetadata = GetFileMetadata(id, "PDF");
                if (fileMetadata == null)
                {
                    throw new NoPdfExistsException(string.Format("Document with DMD ID {0} has no PDF", id));
                }
            }

            try
            {
                var metadata = letterObjection.GetObjectionMetaData(letter, projectNumber, objectorName, objectRule, _config);
                dmdId = DmdService.PutMergedFile(dmdIds.ToArray(), metadata);
            }
            catch (XmlRpcFaultException ex)
            {
#if DEBUG
                if (ex.FaultCode != 1010)
                {
#endif
                    string message = string.Format("An error occurred trying to merge objection attachments: {0} - {1}:: {2}",
                        ex.FaultCode, ex.FaultString, ex.Message);
                    _logger.Error(message);
                    throw new DmdException(message, ex);
#if DEBUG
                }
#endif
            }

            return dmdId;
        }

		/// <summary>
		/// Sends a report to the DMD.  Automatically deletes the existing report, if one already exists
		/// </summary>
		/// <param name="phase"></param>
		/// <param name="report"></param>
		public void PutReport(Phase phase, Report.Report report, User archiver)
		{
			if (!report.ArtifactTypeId.HasValue)
			{
				throw new ArgumentException("You can't send reports that don't have an artifact type to the DMD", "report");
			}

			if (report.Document == null)
			{
				throw new ArgumentException("You must populate report.Document before calling PutReport", "report");
			}

			User projectManager = archiver;
			var artifactType = LookupManager.GetLookup<LookupArtifactType>()[report.ArtifactTypeId.Value];

			// Check to see if an artifact exists in the DMD and delete it
			PhaseArtifact existingArtifact = phase.PhaseArtifacts.FirstOrDefault(x => x.ArtifactTypeId == report.ArtifactTypeId);
			if (existingArtifact != null)
			{
				var dmdId = existingArtifact.DmdId;
				try
				{
					_logger.InfoFormat("DMD > Put Report > Delete existing report ({0}, dmdId = {2}) for phase (phaseId = {1}).", report.Name, phase.PhaseId, dmdId);
					DmdService.DeleteFile(dmdId.ToString());
				}
				catch (XmlRpcFaultException ex)
				{
					string message = string.Format("An error occurred trying to delete an existing report ({0}, dmdId = {2}) for phase (phaseId = {1}) to the DMD to the DMD: {3} - {4}:: {5}",
						report.Name, phase.PhaseId, dmdId, ex.FaultCode, ex.FaultString, ex.Message);
					_logger.Error(message);
				}
				catch (Exception ex)
				{
					string message = string.Format("An error occurred trying to delete an existing report ({0}, dmdId = {2}) for phase (phaseId = {1}) to the DMD to the DMD",
						report.Name, phase.PhaseId, dmdId);
					_logger.Error(message, ex);
					throw new DomainException(message, ex);
				}
			}

			#region Generate the report PDF
			ReportProcessor reportProcessor = new ReportProcessor();
			RenderingResult result = reportProcessor.RenderReport("PDF", report.Document, null);
			#endregion Generate the report PDF

			var dateGenerated = DateTime.UtcNow;
			var authorName = archiver.FullNameLastFirst;

			// Populate the MetaData
			var reportMetaData = new FileMetaData
			{
				AppDocType = _config.ReportDefaults.AppDocType,
				AppSysId = _config.AppSysId,
				Author = authorName.Substring(0, Math.Min(80, authorName.Length)),
				Filename = _config.ReportDefaults.Filename,
				PublicationDate = DateTime.UtcNow,
				Title = artifactType.Name
			};

			reportMetaData.MoreMetaData = new MoreFileMetaData
			{
				ProjectId = phase.Project.ProjectNumber,
				DocumentDate = dateGenerated.ToIso8601Format(),
				DateLabel = _config.ReportDefaults.DateLabel,
				KeyFlag = artifactType.KeyFlag ? "1" : "0",
				Level2Name = _config.ReportDefaults.Level2Name,
				Level3Name = _config.ReportDefaults.Level3Name,
				Level2Id = _config.ReportDefaults.Level2Id,
				Level3Id = _config.ReportDefaults.Level3Id
			};

			// Send the report to the DMD
			try
			{
				_logger.InfoFormat("DMD > Put Report > Put File > phaseId: {0}, reportName: {1}.", phase.PhaseId, report.Name);
				var dmdId = DmdService.PutFile(result.DocumentBytes, reportMetaData);
				_logger.DebugFormat("Added report ({0}) for phase (phaseId = {1}) to the DMD (dmdId = {2})", report.Name, phase.PhaseId, dmdId);

				// Save the DMD ID in the PhaseArtifact
				if (existingArtifact != null)
				{
					existingArtifact.DmdId = dmdId;
				}
				else
				{
					phase.PhaseArtifacts.Add(
						new PhaseArtifact
						{
							PhaseId = phase.PhaseId,
							ArtifactTypeId = report.ArtifactTypeId.Value,
							DmdId = dmdId
						});
				}
			}
			catch (XmlRpcFaultException ex)
			{
				string message = string.Format("An error occurred trying to send a report ({0}) for phase (phaseId = {1}) to the DMD to the DMD: {2} - {3}:: {4}",
					report.Name, phase.PhaseId, ex.FaultCode, ex.FaultString, ex.Message);
				_logger.Error(message);
				throw new DomainException(message, ex);
			}
		}

        public string PutCommentPeriodDocument(User user, int projectId, int phaseId, Document document)
        {
            string dmdId = string.Empty;
            var authorName = string.Format("{0}, {1}", user.LastName, user.FirstName);
            var title = document.Name;
            var filename = document.File.OriginalName;

            // Populate the MetaData
            var fileMetaData = new FileMetaData
            {
                AppDocType = _config.CommentPeriodDefaults.AppDocType,
                AppSysId = _config.AppSysId,
                Author = authorName.Substring(0, Math.Min(80, authorName.Length)),
                Filename = filename,
                PublicationDate = DateTime.UtcNow,
                Title = title,
            };

            fileMetaData.MoreMetaData = new MoreFileMetaData
            {
                Level2Name = _config.CommentPeriodDefaults.Level2Name,
                Level3Name = _config.CommentPeriodDefaults.Level3Name,
                Level2Id = _config.CommentPeriodDefaults.Level2Id,
                Level3Id = _config.CommentPeriodDefaults.Level3Id,
                CaraProjectId = projectId.ToString(),
                CaraPhaseId = phaseId.ToString()
            };

            #region Put the attachments
            byte[] buffer;
            MemoryStream stream;

            try
            {
                buffer = new byte[document.File.Size];
                using (stream = new MemoryStream(buffer, 0, buffer.Length, true, true))
                {
                    using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        docManager.DownloadFile(document, stream);
                        stream.Position = 0;
                        dmdId = DmdService.PutFile(stream.GetBuffer(), fileMetaData);

                        if (string.IsNullOrEmpty(dmdId))
                        {
                            _logger.ErrorFormat("The dmdId returned from comment period document (documentId: {0}) is NULL",
                                document.DocumentId);
                        }
                        else
                        {
                            _logger.Info(string.Format("DMD > Put Comment Period Document > documentId: {0}: {1}",
                                document.DocumentId, dmdId));

                            //// Publish the file to the CDN
                            //bool publishResult = PublishFile(dmdId);
                            //_logger.Info(string.Format("DMD > Put Comment Period Document > Publish File > documentId: {0}: {1}",
                            //    dmdId, publishResult ? "Published" : "Not Published"));
                        }
                    }
                }
            }
            catch (XmlRpcFaultException ex)
            {
#if DEBUG
                if (ex.FaultCode != 1010)
                {
#endif
                    string message = string.Format("An error occurred trying to send comment period document (documentId = {0}) for letter (ID = {0}) to the DMD: {1} - {2}:: {3}",
                        document.DocumentId, ex.FaultCode, ex.FaultString, ex.Message);
                    _logger.Error(message);
                    throw new DomainException(message, ex);
#if DEBUG
                }
#endif
            }
            catch (Exception ex)
            {
                throw new DomainException(string.Format("An error occurred trying to send a comment period document to the DMD (documentId = {0})", document.DocumentId), ex);
            }
            #endregion Put the attachments

            return dmdId;
        }
        #endregion

        #region GetFileData Methods
        /// <summary>
        /// Instantiate and call the RPC proxy and return the base64 string of the input doc id
        /// </summary>
        /// <param name="docId"></param>
        /// <returns>base64 string</returns>
        public byte[] GetFileData(string docId)
        {
            return GetFileData(docId, null);
        }

        /// <summary>
        /// Instantiate and call the RPC proxy and return the base64 string of the input doc id
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="docType"></param>
        /// <returns>base64 string</returns>
        public byte[] GetFileData(string docId, string docType)
        {
            byte[] base64 = null;

            if (!string.IsNullOrEmpty(docId))
            {
                try
                {
                    if (DmdService != null)
                    {
                        // call the proxy method based on the optional doc type param
                        if (!string.IsNullOrEmpty(docType))
                        {
                            FileDataRequest request = new FileDataRequest();
                            request.FileType = docType;

							base64 = DmdService.GetFileData(docId, request);
                        }
                        else
                        {
							base64 = DmdService.GetFileData(docId);
                        }
                    }
                }
				catch (XmlRpcFaultException ex)
				{
                    if (ex.FaultCode == FILE_NOT_FOUND_ERROR)
                    {
                        _logger.InfoFormat("File Not Found (docId = {0}, type = {1})", docId, docType);
                    }
                    else
                    {
                        string message = string.Format("An error occurred trying to get the file (docId = {0}, type = {1}):: {2}", docId, docType, ex.Message);
                        _logger.Error(message);
                        throw new DomainException(message, ex);
                    }
				}
			}

            return base64;
        }
        #endregion

        #region GetFileMetaData Methods
        /// <summary>
        /// Instantiate and call the RPC proxy and return the get file metadata response of the input doc id
        /// </summary>
        /// <param name="docId">The DMD ID</param>
        /// <returns>FileResponse</returns>
        public FileMetaData GetFileMetadata(string docId)
        {
            return GetFileMetadata(docId, null);
        }

        /// <summary>
        /// Instantiate and call the RPC proxy and return the get file metadata response of the input doc id
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="docType"></param>
        /// <returns>base64 string</returns>
		public FileMetaData GetFileMetadata(string docId, string docType)
        {
			FileMetaData file = new FileMetaData();

            if (!string.IsNullOrEmpty(docId))
            {
                try
                {
                    if (DmdService != null)
                    {
                        // call the proxy method based on the optional doc type param
                        if (!string.IsNullOrEmpty(docType))
                        {
                            FileDataRequest request = new FileDataRequest();
                            request.FileType = docType;

							file = DmdService.GetFileMetadata(docId, request);
                        }
                        else
                        {
							file = DmdService.GetFileMetadata(docId);
                        }
                    }
                }
				catch (XmlRpcFaultException ex)
				{
                    if (ex.FaultCode == FILE_NOT_FOUND_ERROR)
                    {
                        string message = string.Format("File Not Found (docId = {0}, type = {1})", docId, docType);
                        _logger.Info(message);
                        file = null;
                    }
                    else
                    {
                        string message = string.Format("An error occurred trying to get the file metatdata (docId = {0}, type = {1}):: {2}", docId, docType, ex.Message);
                        _logger.Error(message);
                        throw new DomainException(message, ex);
                    }
				}
			}

            return file;
        }
        #endregion

        #region SearchDoc Method
        /// <summary>
        /// Instantiate and call the RPC proxy and return the get file list metadata response
        /// </summary>
        /// <param name="queryString"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public FileList SearchDocs(string queryString, SearchDocOption options)
        {
            FileList list = new FileList();

            try
            {
                if (DmdService != null)
                {
                    // submit the request and get the doc id
					list = DmdService.SearchDocs(queryString, options);
                }
            }
			catch (XmlRpcFaultException ex)
			{
				string message = string.Format("An error occurred trying to search for a document:: {0}", ex.Message);
				_logger.Error(message);
				throw new DomainException(message, ex);
			}

            return list;
        }

        /// <summary>
        /// Overloading method of SearchDocs with input of a project id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public FileList SearchDocs(int projectId)
        {
            SearchDocOption options = new SearchDocOption();
            options.projectids = new int[1];
            options.projectids[0] = projectId;

            return SearchDocs(null, options);
        }
        #endregion

		#region DeleteFile Method
		/// <summary>
		/// 
		/// </summary>
		/// <param name="dmdId"></param>
		/// <returns>true, if successful</returns>
		public bool DeleteFile(string dmdId)
		{
			bool success = false;
			try
			{
				_logger.InfoFormat("DMD > Delete File: {0}", dmdId);
				success = DmdService.DeleteFile(dmdId.ToString());
			}
			catch (XmlRpcFaultException ex)
			{
				string message = string.Format("An error occurred trying to delete a file ({0}) from the DMD: {1} - {2}:: {3}",
					dmdId, ex.FaultCode, ex.FaultString, ex.Message);
				_logger.Error(message);
			}
			catch (Exception ex)
			{
				string message = string.Format("An error occurred trying to delete an existing file ({0})", dmdId);
				_logger.Error(message, ex);
				throw new DomainException(message, ex);
			}

			return success;
		}
		#endregion DeleteFile Method

		#region ReplaceFileMetadata
		public bool ReplaceFileMetatdata(string dmdId, NewFileMetaData newFileMetaData, ReplaceFileMetaDataOptions options)
		{
			bool result;
			try
			{
				result = DmdService.ReplaceFileMetatdata(dmdId, newFileMetaData, options);
			}
			catch (XmlRpcFaultException ex)
			{
				string message = string.Format("An error occurred trying to replace the metadata for file ({0}): {1} - {2}:: {3}",
					dmdId, ex.FaultCode, ex.FaultString, ex.Message);
				_logger.Error(message);
				throw new DomainException(message, ex);
			}

			return result;
		}
		#endregion ReplaceFileMetadata

        #region ZipPhaseDocuments
        public bool RequestAllPhaseLetters(int projectId, int phaseId, string shortname)
        {
            bool result;
            try
            {
                result = DmdService.ZipPhaseLetters(projectId.ToString(), phaseId.ToString(), shortname);
            }
            catch (XmlRpcFaultException ex)
            {
                string message = string.Format("An error occurred trying to request phase letters: (projectId = {0}, phaseId = {4}, shortname = {5}) {1} - {2}:: {3}",
                    projectId, ex.FaultCode, ex.FaultString, ex.Message, phaseId, shortname);
                _logger.Error(message);
                throw new DomainException(message, ex);
            }

            return result;
        }
        #endregion


        #region ZipFormDocuments
        public bool RequestFormLetters(int projectId, int phaseId, string shortname, int formSetId)
        {
            int duplicateLetterId = -2;
            string formSetName = "";
            string[] dmdIds = null;
            bool result;
            try
            {
                if (formSetId >= 0)
                {
                    // retrieving all dmd ids in the formset id
                    using (var formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                    {
                        dmdIds = formSetManager.Get(formSetId).FormLetters.Select(x => x.DmdId).ToArray();

                        // limiting formset filename to A-z, 0-9, and '_' chars
                        try
                        {
                            string formTempName = formSetManager.Get(formSetId).Name.Replace(" ", "_");
                            StringBuilder toFileName = new StringBuilder();
                            for (int x = 0; x < formTempName.Length; x++)
                            {
                                if ((formTempName[x] >= 'A' && formTempName[x] <= 'z') ||
                                   (formTempName[x] >= '0' && formTempName[x] <= '9') ||
                                    formTempName[x] == '_')
                                {
                                    toFileName.Append(formTempName[x]);
                                }
                            }
                            formSetName = toFileName.ToString();
                        }
                        catch(Exception e)
                        {
                            formSetName = formSetId.ToString();

                            string message = string.Format("An error occurred trying to compile formset letter filename from fromset name: (projectId = {0}, phaseId = " +
                                                           "{2}, shortname = {3}): {1}", projectId, e.Message, phaseId, shortname);
                            _logger.Error(message);
                        }
                    }
                }
                else
                {
                    // user chose to download "unique" or "dupicate"
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        if (formSetId == duplicateLetterId)
                        {
                            // -2 indicates request for Duplicate letter downloads
                            dmdIds = phaseManager.Get(phaseId).Letters.Where(x => x.PhaseId == phaseId && x.LetterTypeId == 1).Select(x => x.DmdId).ToArray();
                            formSetName = "duplicate";
                        }
                        else
                        {
                            // -1 indicates request for Unique letter downloads
                            dmdIds = phaseManager.Get(phaseId).Letters.Where(x => x.PhaseId == phaseId && x.LetterTypeId == 2).Select(x => x.DmdId).ToArray();
                            formSetName = "unique";
                        }
                    }
                }
                if (dmdIds.Contains(null))
                    throw new DomainException("Formset array (dmdIds) contains null value(s)");
                result = true;
                result = DmdService.ZipFormLetters(projectId.ToString(), phaseId.ToString(), shortname, formSetName, dmdIds);
            }
            catch (XmlRpcFaultException ex)
            {
                string message = string.Format("An error occurred trying to request formset letters: (projectId = {0}, phaseId = {4}, shortname = {5}) {1} - {2}:: {3}",
                    projectId, ex.FaultCode, ex.FaultString, ex.Message, phaseId, shortname);
                _logger.Error(message);

                throw new DomainException(message, ex);
            }
            return result;
        }
        #endregion

        #region PublishFile
        public bool PublishFile(string docId)
        {
            bool result;
            try
            {
                result = DmdService.PublishFile("CARA_WWW_1", docId);
            }
            catch (XmlRpcFaultException ex)
            {
                string message = string.Format("An error occurred trying to publish the document with DMD ID: {0}", docId);
                _logger.Error(message);
                throw new DomainException(message, ex);
            }

            return result;
        }
        #endregion PublishFile

        public string EchoMsg(string msg)
        {
            var result = DmdService.EchoMsg(msg);

            return result; 
        }
    }
}
