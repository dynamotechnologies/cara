﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using log4net;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using System.Configuration;
using Amazon.SQS;
using Amazon;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.Aws.Sqs;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Domain.Dmd
{
    public class DmdSendFilesServiceTask : IServiceTask
    {
        private ILog _tempLogger = LogManager.GetLogger("DmdFailureLog");
        private ILog _logger = LogManager.GetLogger(typeof(DmdSendFilesServiceTask));
        private static readonly string _letterIdPrefix = "LetterId";
        private static readonly string _documentIdPrefix = "DocumentId";
        private static readonly string _objectionDocumentIdPrefix = "ObjectionDocumentId";
        private static readonly int _maxNumberOfMessages = 10;
        private static readonly string _queueNameKey = "cara.queue.dmdsend";

        public string Name
        {
            get { return "DMD Send Files"; }
        }

        public void RunServiceTask()
        {
            var dmdManager = new DmdManager();

            // Grab letters and documents from the queue
            try
            {
                // Create a reference to the queue
                string queueName;
                var sqs = SqsHelper.GetQueue(_queueNameKey, out queueName);

                var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                var createQueueResponse = sqs.CreateQueue(sqsRequest);
                string queueUrl = createQueueResponse.QueueUrl;

                int numberOfMessages = 0;
                do
                {
                    IEnumerable<Message> letterMessages;
                    IEnumerable<Message> documentMessages;
                    IEnumerable<Message> objectionDocumentMessages;

                    var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = _maxNumberOfMessages };
                    var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                    numberOfMessages = 0;

                    if (receiveMessageResponse.Messages != null && receiveMessageResponse.Messages.Count > 0)
                    {
                        numberOfMessages = receiveMessageResponse.Messages.Count;

                        letterMessages = receiveMessageResponse.Messages.Where(x => x.Body.StartsWith(_letterIdPrefix));
                        documentMessages  = receiveMessageResponse.Messages.Where(x => x.Body.StartsWith(_documentIdPrefix));
                        objectionDocumentMessages  = receiveMessageResponse.Messages.Where(x => x.Body.StartsWith(_objectionDocumentIdPrefix));

                        #region Send Letters to the DMD and remove queue entries
                        var dmbrLetters = SendLettersToDmd(dmdManager, letterMessages);
                        if (dmbrLetters != null && dmbrLetters.Count > 0)
                        {
                            var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrLetters };
                            sqs.DeleteMessageBatch(deleteRequest);
                        }
                        #endregion Send Letters to the DMD and remove queue entries

                        #region Send Attachments to the DMD and remove queue entries
                        var dmbrAttachments = SendLetterAttachmentsToDmd(dmdManager, documentMessages);
                        if (dmbrAttachments != null && dmbrAttachments.Count > 0)
                        {
                            var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrAttachments };
                            sqs.DeleteMessageBatch(deleteRequest);
                        }
                        #endregion Send Attachments to the DMD and remove queue entries

                        #region Send Attachments to the DMD and remove queue entries
                        var dmbrObjectionAttachments = SendObjectionAttachmentsToDmd(dmdManager, objectionDocumentMessages);
                        if (dmbrObjectionAttachments != null && dmbrObjectionAttachments.Count > 0)
                        {
                            var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrObjectionAttachments };
                            sqs.DeleteMessageBatch(deleteRequest);
                        }
                        #endregion Send Attachments to the DMD and remove queue entries
                    }
                } while (numberOfMessages > 0);
            }
            catch (AmazonSQSException ase)
            {
                _logger.Error("An error occurred processing the DMD send queue", ase);
            }
        }

        private List<DeleteMessageBatchRequestEntry> SendLetterAttachmentsToDmd(DmdManager dmdManager, IEnumerable<Message> attachmentMessages)
        {
            List<DeleteMessageBatchRequestEntry> dmbrEntries = null;
            IEnumerable<EntityToMessageMapper> messageToDocumentMapper;
            
            if (attachmentMessages.Count() > 0)
            {
                dmbrEntries = new List<DeleteMessageBatchRequestEntry>(attachmentMessages.Count());
                messageToDocumentMapper = attachmentMessages.Select(m => 
                    new EntityToMessageMapper
                    {
                        Id = int.Parse(m.Body.Substring(_documentIdPrefix.Length + 1)),
                        Message = m
                    });


                using (var letterDocumentManager = ManagerFactory.CreateInstance<LetterDocumentManager>())
                {
                    letterDocumentManager.UnitOfWork.Context.CommandTimeout = 180;

                    using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        List<int> idlist = messageToDocumentMapper.Select(e => e.Id).ToList();
                        var documentsToSend = documentManager
                            .All
                            .Where(doc => idlist.Contains(doc.DocumentId))
                            .ToList();

                        var attachmentsToSend = letterDocumentManager
                            .All
                            .Where(doc => idlist.Contains(doc.DocumentId))
                            .Select(doc => new
                            {
                                DocumentId = doc.DocumentId,
                                Letter = doc.Letter,
                                Author = doc.Letter.Authors.FirstOrDefault(a => a.Sender),
                                Organization = doc.Letter.Authors.FirstOrDefault(a => a.Sender).Organization,
                                ProjectNumber = doc.Letter.Phase.Project.ProjectNumber,
                                ProjectTypeId = doc.Letter.Phase.Project.ProjectTypeId,
                                ProjectId = doc.Letter.Phase.ProjectId,
                                PhaseTypeId = doc.Letter.Phase.PhaseTypeId
                            });

                        // Limit the number of files to send per execution
                        int maxAttachmentsToSend;
                        if (int.TryParse(ConfigurationManager.AppSettings["MaxAttachmentsToSendPerRun"], out maxAttachmentsToSend))
                        {
                            if (maxAttachmentsToSend >= 0)
                            {
                                attachmentsToSend = attachmentsToSend.Take(maxAttachmentsToSend);
                            }
                        }

                        // The query above will load the properties in the letter that we need,
                        // so we don't need to lazy load them
                        letterDocumentManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                        string dmdId;
                        Letter letter;
                        Document document;
                        int count = 0;
                        foreach (var entry in attachmentsToSend)
                        {
                            dmdId = null;
                            letter = entry.Letter;
                            document = documentsToSend.First(d => d.DocumentId == entry.DocumentId);

                            try
                            {
                                //CR#379 CARA would require a code change to resend the attachments to DMD 
                                // Delete an existing letter
                                if (!string.IsNullOrEmpty(document.DmdId))
                                {
                                    if (dmdManager.DeleteFile(document.DmdId))
                                    {
                                        document.DmdId = null;
                                    }
                                    else
                                    {
                                        _logger.ErrorFormat("Failed to delete letter ID {0}, {1} from the DMD before replacing updated letter.", letter.LetterId, document.DmdId);
                                    }
                                }
                                dmdId = dmdManager.PutLetterAttachment(letter, entry.ProjectNumber, document, documentManager, entry.ProjectId, (entry.ProjectTypeId == 1), (entry.PhaseTypeId == 3));  // 1 == PALS

                                if (!string.IsNullOrEmpty(dmdId))
                                {
                                    //documentManager.UnitOfWork.Context.AttachTo("Documents", document); // not sure this is necessary
                                    document.DmdId = dmdId;
                                    count++;

                                    // PutLetterAttachment was successful, so add entry to list to be removed from the queue
                                    var message = messageToDocumentMapper.First(x => x.Id == document.DocumentId).Message;
                                    dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                                }
                                else
                                {
                                    _logger.ErrorFormat("Failed to send letter ID {0} to the DMD", letter.LetterId);
                                }
                            }
                            catch (Exception ex)
                            {
                                // -- Start - TEMPORARY FIX for DMD issues --
                                // Parse DMD ID from the error message
                                string pattern = @"FSPLT\d_\d*";
                                var matches = Regex.Matches(ex.Message, pattern);
                                if (matches.Count > 0)
                                {
                                    var parsedDmdId = matches[0].Value;

                                    // Remove the letter from the queue and parse the error message
                                    var message = messageToDocumentMapper.First(x => x.Id == document.DocumentId).Message;
                                    dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });

                                    // Log the error
                                    _tempLogger.ErrorFormat("LetterAttachment,{0},{1}", document.DocumentId, parsedDmdId);
                                }
                                // -- End - TEMPORARY FIX for DMD issues --

                                _logger.Error(string.Format(
                                    "An error occurred while sending letter ID {0}, documentId {1} to the DMD: {2}",
                                    letter.LetterId,
                                    document.DocumentId,
                                    dmdId), ex);
                            }
                        }

                        documentManager.UnitOfWork.Save();

                        _logger.InfoFormat("Sent {0} attachments to the DMD", count);
                    }
                    letterDocumentManager.UnitOfWork.Save();
                }
            }

            return dmbrEntries;
        }

        private List<DeleteMessageBatchRequestEntry> SendLettersToDmd(DmdManager dmdManager, IEnumerable<Message> letterMessages)
        {
            List<DeleteMessageBatchRequestEntry> dmbrEntries = null;
            IEnumerable<EntityToMessageMapper> messageToLetterMapper;
            
            if (letterMessages.Count() > 0)
            {
                dmbrEntries = new List<DeleteMessageBatchRequestEntry>(letterMessages.Count());
                messageToLetterMapper = letterMessages.Select(m => 
                    new EntityToMessageMapper
                    {
                        Id = int.Parse(m.Body.Substring(_letterIdPrefix.Length + 1)),
                        Message = m
                    });

                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    letterManager.UnitOfWork.Context.CommandTimeout = 180;
                    // Find all letters that have
                    // null DMD ID
                    List<int> idlist = messageToLetterMapper.Select(e => e.Id).ToList();
                    var lettersToSend = letterManager.All
                        .Where(ltr => idlist.Contains(ltr.LetterId))
                        .Select(ltr => new
                        {
                            Letter = ltr,
                            Author = ltr.Authors.FirstOrDefault(a => a.Sender),
                            Organization = ltr.Authors.FirstOrDefault(a => a.Sender).Organization,
                            ProjectNumber = ltr.Phase.Project.ProjectNumber,
                            ProjectTypeId = ltr.Phase.Project.ProjectTypeId,
                            ProjectId = ltr.Phase.ProjectId,
                            PhaseTypeId = ltr.Phase.PhaseTypeId,
                            Phase = ltr.Phase
                        });

                    // The query above will load the properties in the letter that we need,
                    // so we don't need to lazy load them
                    letterManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                    // Limit the number of files to send per execution
                    int maxLettersToSend;
                    if (int.TryParse(ConfigurationManager.AppSettings["MaxLettersToSendPerRun"], out maxLettersToSend))
                    {
                        if (maxLettersToSend >= 0)
                        {
                            lettersToSend = lettersToSend.Take(maxLettersToSend);
                        }
                    }

                    string dmdId;
                    Letter letter;
                    int count = 0;
                    foreach (var entry in lettersToSend)
                    {
                        dmdId = null;
                        letter = entry.Letter;
                        try
                        {
                            // Delete an existing letter
                            if (!string.IsNullOrEmpty(letter.DmdId))
                            {
                                if (dmdManager.DeleteFile(letter.DmdId))
                                {
                                    letter.DmdId = null;
                                }
                                else
                                {
                                    _logger.ErrorFormat("Failed to delete letter ID {0}, {1} from the DMD before replacing updated letter.", letter.LetterId, letter.DmdId);
                                }
                            }

                            dmdId = dmdManager.PutLetter(letter, entry.ProjectNumber, entry.ProjectId, (entry.ProjectTypeId == 1), (entry.PhaseTypeId == 3)); // 1 = PALS Project

                            if (!string.IsNullOrEmpty(dmdId))
                            {
                                letter.DmdId = dmdId;
                                count++;

                                // PutAttachment was successful, so add entry to list to be removed from the queue
                                var message = messageToLetterMapper.First(x => x.Id == letter.LetterId).Message;
                                dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                            }
                            else
                            {
                                _logger.ErrorFormat("Failed to send letter ID {0} to the DMD", letter.LetterId);
                            }
                        }
                        catch (Exception ex)
                        {
                            // -- Start - TEMPORARY FIX for DMD issues --
                            // Parse DMD ID from the error message
                            string pattern = @"FSPLT\d_\d*";
                            var matches = Regex.Matches(ex.Message, pattern);
                            if (matches.Count > 0)
                            {
                                var parsedDmdId = matches[0].Value;

                                // Remove the letter from the queue and parse the error message
                                var message = messageToLetterMapper.First(x => x.Id == letter.LetterId).Message;
                                dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });

                                // Log the error
                                _tempLogger.ErrorFormat("Letter,{0},{1}", letter.LetterId, parsedDmdId);
                            }
                            // -- End - TEMPORARY FIX for DMD issues --

                            _logger.Error(string.Format(
                                "An error occurred while sending letter ID {0} to the DMD: {1}",
                                letter.LetterId,
                                dmdId), ex);
                        }
                    }

                    letterManager.UnitOfWork.Save();

                    _logger.InfoFormat("Sent {0} Letters to the DMD", count);
                }
            }

            return dmbrEntries;
        }

        private List<DeleteMessageBatchRequestEntry> SendObjectionAttachmentsToDmd(DmdManager dmdManager, IEnumerable<Message> attachmentMessages)
        {
            List<DeleteMessageBatchRequestEntry> dmbrEntries = null;
            IEnumerable<EntityToMessageMapper> messageToDocumentMapper;
            
            if (attachmentMessages.Count() > 0)
            {
                dmbrEntries = new List<DeleteMessageBatchRequestEntry>(attachmentMessages.Count());
                messageToDocumentMapper = attachmentMessages.Select(m => 
                    new EntityToMessageMapper
                    {
                        Id = int.Parse(m.Body.Substring(_objectionDocumentIdPrefix.Length + 1)),
                        Message = m
                    });


                using (var lodManager = ManagerFactory.CreateInstance<LetterObjectionDocumentManager>())
                {
                    lodManager.UnitOfWork.Context.CommandTimeout = 180;

                    using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
                    {
                        List<int> idlist = messageToDocumentMapper.Select(e => e.Id).ToList();
                        var documentsToSend = documentManager
                            .All
                            .Where(doc => idlist.Contains(doc.DocumentId))
                            .ToList();

                        var attachmentsToSend = lodManager
                            .All
                            .Where(doc => idlist.Contains(doc.DocumentId))
                            .Select(doc => new
                            {
                                DocumentId = doc.DocumentId,
                                LetterObjection = doc.LetterObjection,
                                Letter = doc.LetterObjection.Letters.FirstOrDefault(),
                                Author = doc.LetterObjection.Letters.FirstOrDefault().Authors.FirstOrDefault(x => x.Sender),
                                ProjectNumber = doc.LetterObjection.Letters.FirstOrDefault().Phase.Project.ProjectNumber,
                                CommentRuleId = doc.LetterObjection.Letters.FirstOrDefault().Phase.Project.CommentRuleId
                            });

                        // Limit the number of files to send per execution
                        int maxAttachmentsToSend;
                        if (int.TryParse(ConfigurationManager.AppSettings["MaxAttachmentsToSendPerRun"], out maxAttachmentsToSend))
                        {
                            if (maxAttachmentsToSend >= 0)
                            {
                                attachmentsToSend = attachmentsToSend.Take(maxAttachmentsToSend);
                            }
                        }

                        // The query above will load the properties in the letter that we need,
                        // so we don't need to lazy load them
                        lodManager.UnitOfWork.Context.ContextOptions.LazyLoadingEnabled = false;

                        string dmdId;
                        Letter letter;
                        Document document;
                        int count = 0;
                        foreach (var entry in attachmentsToSend)
                        {
                            dmdId = null;
                            letter = entry.Letter;
                            document = documentsToSend.First(d => d.DocumentId == entry.DocumentId);

                            try
                            {
                                //CR#379 CARA would require a code change to resend the attachments to DMD 
                                // Delete an existing letter
                                if (!string.IsNullOrEmpty(document.DmdId))
                                {
                                    if (dmdManager.DeleteFile(document.DmdId))
                                    {
                                        document.DmdId = null;
                                    }
                                    else
                                    {
                                        _logger.ErrorFormat("Failed to delete letter ID {0}, {1} from the DMD before replacing updated letter.", letter.LetterId, document.DmdId);
                                    }
                                }
                                string commentRuleName = LookupManager.GetLookup<LookupCommentRule>()[entry.CommentRuleId.Value].Name;
                                dmdId = dmdManager.PutObjectionAttachment(entry.LetterObjection, letter, entry.Author.FullNameLastFirst, entry.ProjectNumber, commentRuleName, document, documentManager);

                                if (!string.IsNullOrEmpty(dmdId))
                                {
                                    //documentManager.UnitOfWork.Context.AttachTo("Documents", document); // not sure this is necessary
                                    document.DmdId = dmdId;
                                    count++;

                                    // PutLetterAttachment was successful, so add entry to list to be removed from the queue
                                    var message = messageToDocumentMapper.First(x => x.Id == document.DocumentId).Message;
                                    dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                                }
                                else
                                {
                                    _logger.ErrorFormat("Failed to send letter ID {0} to the DMD", letter.LetterId);
                                }
                            }
                            catch (Exception ex)
                            {
                                // -- Start - TEMPORARY FIX for DMD issues --
                                // Parse DMD ID from the error message
                                string pattern = @"FSPLT\d_\d*";
                                var matches = Regex.Matches(ex.Message, pattern);
                                if (matches.Count > 0)
                                {
                                    var parsedDmdId = matches[0].Value;

                                    // Remove the letter from the queue and parse the error message
                                    var message = messageToDocumentMapper.First(x => x.Id == document.DocumentId).Message;
                                    dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });

                                    // Log the error
                                    _tempLogger.ErrorFormat("ObjectionAttachment,{0},{1}", document.DocumentId, parsedDmdId);
                                }
                                // -- End - TEMPORARY FIX for DMD issues --

                                _logger.Error(string.Format(
                                    "An error occurred while sending letter ID {0}, documentId {1} to the DMD: {2}",
                                    letter.LetterId,
                                    document.DocumentId,
                                    dmdId), ex);
                            }
                        }

                        documentManager.UnitOfWork.Save();

                        _logger.InfoFormat("Sent {0} attachments to the DMD", count);
                    }
                    lodManager.UnitOfWork.Save();
                }
            }

            return dmbrEntries;
        }

        private class EntityToMessageMapper
        {
            public int Id { get; set; }
            public Message Message { get; set; }
        }
    }
}
