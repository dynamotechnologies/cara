﻿using System;
using System.Text;
using Aquilent.Framework.Documents;
using Aquilent.Cara.Configuration.Dmd;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.Dmd
{
    /// <summary>
    /// Helper class which contains functions for XML RPC operations
    /// </summary>
	public static class DmdHelper
    {
		/// <summary>
		/// Returns the DateTime in the format:  YYYYMMDDTHH:MM:SS
		/// </summary>
		/// <param name="datetime"></param>
		/// <returns></returns>
		public static string ToIso8601Format(this DateTime datetime, bool isUtc = true)
		{
			var isodate = datetime.ToString("yyyy-MM-ddTHH:mm:ss");

			if (isUtc)
			{
				isodate += "Z";
			}

			return isodate;
		}

		public static string ToDmdFormat(this Letter letter)
		{
			var dmdFormat = new StringBuilder();

            var timezone = letter.Phase.TimeZone;

            dmdFormat.Append(string.Format("Date submitted ({1}): {0}\n", 
                TimeZoneInfo.ConvertTimeFromUtc(letter.DateSubmitted, TimeZoneInfo.FindSystemTimeZoneById(timezone)).ToString("G"), 
                timezone));
            dmdFormat.Append(string.Format("First name: {0}\n", letter.Sender.FirstName));
            dmdFormat.Append(string.Format("Last name: {0}\n", letter.Sender.LastName));
            dmdFormat.Append(string.Format("Organization: {0}\n", letter.Sender.Organization != null ? letter.Sender.Organization.Name : string.Empty));
            dmdFormat.Append(string.Format("Title: {0}\n", letter.Sender.Title));
            //dmdFormat.Append(string.Format("Official Representative/Member Indicator: {0}\n", letter.Sender.OfficialRepresentativeTypeName));
			dmdFormat.Append(string.Format("Comments:\n{0}\n", letter.Text));

			return dmdFormat.ToString();
		}

        public static FileMetaData GetMetaData(this Letter letter, string projectNumber, DmdServiceConfiguration config, int projectId, bool isPalsProject, bool isObjection, Document document = null)
        {
            var authorName = string.Format("{0}, {1}", letter.Sender.LastName, letter.Sender.FirstName);
            string title;
            FileMetaDataElement configDefaults;

            if (!isObjection)
            {
                title = string.Format("Public Comment - {0}, {1}", letter.Sender.LastName, letter.Sender.FirstName);
                configDefaults = config.LetterDefaults;
            }
            else
            {
                title = string.Format("Objection Letter - {0}, {1}", letter.Sender.LastName, letter.Sender.FirstName);
                configDefaults = config.ObjectionLetterDefaults;
            }

            var filename = config.LetterDefaults.Filename;
            var isAttachment = "no";

            if (document != null)
            {
                title = string.Format("{0} (Attachment)", title);
                filename = document.File.OriginalName;
                isAttachment = "yes";
            }

            // Populate the MetaData
            var letterMetaData = new FileMetaData
            {
                AppDocType = configDefaults.AppDocType,
                AppSysId = config.AppSysId,
                Author = authorName.Substring(0, Math.Min(80, authorName.Length)),
                Filename = filename,
                PublicationDate = DateTime.UtcNow,
                Title = title,
            };

            letterMetaData.MoreMetaData = new MoreFileMetaData
            {
                DocumentDate = letter.DateSubmitted.ToIso8601Format(),
                DateLabel = configDefaults.DateLabel,
                KeyFlag = "0",  // TODO: Remove hard coded value
                Level2Name = configDefaults.Level2Name,
                Level3Name = configDefaults.Level3Name,
                Level2Id = configDefaults.Level2Id,
                Level3Id = configDefaults.Level3Id,
                IsAttachment = isAttachment,
                LetterId = letter.LetterId.ToString(),
                CaraPhaseId = letter.PhaseId.ToString(),
                CaraProjectId = projectId.ToString()
            };

            if (isPalsProject)
            {
                letterMetaData.MoreMetaData.ProjectId = projectNumber;
            }

            return letterMetaData;
        }

        public static FileMetaData GetCommentPeriodMetaData(User user, int projectId, DmdServiceConfiguration config, Document document)
        {
            var authorName = string.Format("{0}, {1}", user.LastName, user.FirstName);
            var title = document.Name;
            var filename = document.File.OriginalName;

            // Populate the MetaData
            var fileMetaData = new FileMetaData
            {
                AppDocType = config.CommentPeriodDefaults.AppDocType,
                AppSysId = config.AppSysId,
                Author = authorName.Substring(0, Math.Min(80, authorName.Length)),
                Filename = filename,
                PublicationDate = DateTime.UtcNow,
                Title = title,
            };

            fileMetaData.MoreMetaData = new MoreFileMetaData
            {
                Level2Name = config.CommentPeriodDefaults.Level2Name,
                Level2Id = config.CommentPeriodDefaults.Level2Id,
                Level3Name = config.CommentPeriodDefaults.Level3Name,
                Level3Id = config.CommentPeriodDefaults.Level3Id,
                CaraProjectId = projectId.ToString()
            };

            return fileMetaData;
        }

        public static FileMetaData GetObjectionMetaData(this LetterObjection letterObjection, 
            Letter letter, 
            string projectNumber, 
            string objectorName, 
            string objectRule,
            DmdServiceConfiguration config,
            Document document = null)
        {
            string filename;
            string title;
            if (document == null)
            {
                filename = string.Format("Objection Response - {0}.pdf", objectorName);
                title = string.Format("Objection Response - {0}", objectorName);
            }
            else
            {
                filename = document.File.OriginalName;
                title = filename;
            }

            // Populate the MetaData
            var fileMetaData = new FileMetaData
            {
                Title = title,
                Author = objectorName.Substring(0, Math.Min(80, objectorName.Length)),
                PublicationDate = DateTime.UtcNow,
                AppSysId = config.AppSysId,
                AppDocType = config.LetterDefaults.AppDocType,
                Filename = filename,
            };

            string responseSigner = string.Format("{0}, {1}", letterObjection.SignerLastName, letterObjection.SignerFirstName);

            fileMetaData.MoreMetaData = new MoreFileMetaData
            {
                DateLabel = config.ObjectionDefaults.DateLabel,
                ProjectId = projectNumber,
                ObjectionRule = objectRule,
                Level3Name = config.ObjectionDefaults.Level3Name,
                Level2Name = config.ObjectionDefaults.Level2Name,
                Level3Id = config.ObjectionDefaults.Level3Id,
                Level2Id = config.ObjectionDefaults.Level2Id,
                ResponseSigner = responseSigner.Substring(0, Math.Min(80, responseSigner.Length)),
                DocumentDate = letter.DateSubmitted.ToIso8601Format(),
                Objector = objectorName.Substring(0, Math.Min(80, objectorName.Length))
            };

            return fileMetaData;
        }
	}
}
