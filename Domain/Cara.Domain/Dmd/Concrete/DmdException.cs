﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Dmd
{
    public class DmdException : ApplicationException
    {
        public DmdException() : base()
        {
        }

        public DmdException(string message) : base(message)
        {
        }

        public DmdException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
