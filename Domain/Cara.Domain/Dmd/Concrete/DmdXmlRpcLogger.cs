﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CookComputing.XmlRpc;
using log4net;

namespace Aquilent.Cara.Domain.Dmd
{
    public class DmdXmlRpcLogger : XmlRpcLogger
    {
        private ILog logger = LogManager.GetLogger(typeof(DmdXmlRpcLogger));

        protected override void OnRequest(object sender, XmlRpcRequestEventArgs e)
        {
            LogStream(e.RequestStream, e.RequestNum, true);
        }

        protected override void  OnResponse(object sender, XmlRpcResponseEventArgs e)
        {
            LogStream(e.ResponseStream, e.RequestNum, false);
        }

        /// <summary>
        /// This method logs DEBUG-level log4net messages containing the XML-RPC request and response.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="requestNumber"></param>
        /// <param name="isRequest"></param>
        private void LogStream(Stream stream, long requestNumber, bool isRequest)
        {
            if (logger.IsDebugEnabled)
            {
                stream.Position = 0;
                TextReader textReader = new StreamReader(stream);
                string package = textReader.ReadToEnd();
                stream.Position = 0;

                logger.Debug("=========================================");
                logger.DebugFormat("Start {0} for (Request #{1})", isRequest ? "Request" : "Response", requestNumber);
                logger.Debug(package);
                logger.DebugFormat("End {0} for (Request #{1})", isRequest ? "Request" : "Response", requestNumber);
                logger.Debug("-----------------------------------------");
            }
        }
    }
}
