﻿using System;
using System.Configuration;
using System.Linq;
using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Aquilent.EntityAccess;
using Aquilent.Framework.Services;
using log4net;
using System.Collections.Generic;
using Aquilent.Cara.Domain.Aws.Sqs;

namespace Aquilent.Cara.Domain.Dmd
{
    public class DmdDeleteQueueServiceTask : IServiceTask
    {
        private ILog _logger = LogManager.GetLogger(typeof(DmdDeleteQueueServiceTask));
        private static readonly int _maxNumberOfMessages = 10;
        private static readonly string _queueNameKey = "cara.queue.dmddelete";

        public string Name
        {
            get { return "DMD Delete Queue"; }
        }

        public void RunServiceTask()
        {
            var dmdManager = new DmdManager();

            // Grab letters and documents from the queue
            try
            {
                // Create a reference to the queue
                string queueName;
                var sqs = SqsHelper.GetQueue(_queueNameKey, out queueName);

                var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                var createQueueResponse = sqs.CreateQueue(sqsRequest);
                string queueUrl = createQueueResponse.QueueUrl;

                int numberOfMessages = 0;
                do
                {
                    List<DeleteMessageBatchRequestEntry> dmbrEntries = new List<DeleteMessageBatchRequestEntry>(_maxNumberOfMessages);
                    var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = _maxNumberOfMessages };
                    var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                    numberOfMessages = 0;

                    if (receiveMessageResponse.Messages != null)
                    {
                        bool success;
                        numberOfMessages = receiveMessageResponse.Messages.Count;
                        foreach (var message in receiveMessageResponse.Messages)
                        {
                            var entry = message.Body;
                            dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });

                            success = dmdManager.DeleteFile(entry);

                            if (!success)
                            {
                                // Check to see if the file exists
                                var fileMetaData = dmdManager.GetFileMetadata(entry);

                                if (fileMetaData != null)
                                {
                                    // The file exists, but couldn't be deleted
                                    _logger.ErrorFormat("Deleting {0} from the DMD was unsuccessful", entry);
                                }
                            }
                        }

                        try
                        {
                            _logger.Debug("Clearing the queue");
                            // Remove the entries from the queue
                            if (dmbrEntries.Count > 0)
                            {
                                var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrEntries };
                                sqs.DeleteMessageBatch(deleteRequest);
                            }
                        }
                        catch (AmazonSQSException ex)
                        {
                            _logger.Error("An error occurred clearing the 'DMD Delete Queue'");
                        }
                    }
                } while (numberOfMessages > 0);
            }
            catch (AmazonSQSException ase)
            {
                _logger.Error("An error occurred processing the DMD send queue", ase);
            }
        }
    }
}
