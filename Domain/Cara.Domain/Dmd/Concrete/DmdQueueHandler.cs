﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Cara.Domain.Aws.Sqs;

namespace Aquilent.Cara.Domain.Dmd
{
    public static class DmdQueueHandler
    {
        private static readonly string _dmdSendQueueNameKey = "cara.queue.dmdsend";
        private static readonly string _dmdDeleteQueueNameKey = "cara.queue.dmddelete";

        /// <summary>
        /// UnitOfWorkEventHandler to add letters to the DMD send queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void QueueDocumentsForDmdSend(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Added"))
            {
                string queueName;
                var sqs = SqsHelper.GetQueue(_dmdSendQueueNameKey, out queueName);

                IList<object> entries = (IList<object>) e["Added"];
                foreach (var entry in entries)
                {
                    Document document;
                    if (entry is Document)
                    {
                        document = (Document)entry;

                        // Queue the letter
                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                            {
                                QueueUrl = queueUrl,
                                MessageBody = string.Format("DocumentId,{0}", document.DocumentId)
                            };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
        }

        /// <summary>
        /// UnitOfWorkEventHandler to add letters to the DMD send queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void QueueObjectionDocumentsForDmdSend(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Added"))
            {
                string queueName;
                var sqs = SqsHelper.GetQueue(_dmdSendQueueNameKey, out queueName);

                IList<object> entries = (IList<object>) e["Added"];
                foreach (var entry in entries)
                {
                    Document document;
                    if (entry is Document)
                    {
                        document = (Document)entry;

                        // Queue the letter
                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                            {
                                QueueUrl = queueUrl,
                                MessageBody = string.Format("ObjectionDocumentId,{0}", document.DocumentId)
                            };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
        }

        /// <summary>
        /// UnitOfWorkEventHandler to add letters to the DMD send queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void QueueDocumentsForDmdDelete(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Deleted"))
            {
                string queueName;
                var sqs = SqsHelper.GetQueue(_dmdDeleteQueueNameKey, out queueName);

                IList<object> entries = (IList<object>) e["Deleted"];
                foreach (var entry in entries)
                {
                    Document document;
                    if (entry is Document)
                    {
                        document = (Document)entry;
                        if (!string.IsNullOrEmpty(document.DmdId))
                        {
                            // Queue the letter
                            var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                            var createQueueResponse = sqs.CreateQueue(sqsRequest);
                            string queueUrl = createQueueResponse.QueueUrl;

                            var sendMessageRequest = new SendMessageRequest
                                {
                                    QueueUrl = queueUrl,
                                    MessageBody = document.DmdId
                                };
                            sqs.SendMessage(sendMessageRequest);
                        }
                    }
                }
            }
        }
    }
}
