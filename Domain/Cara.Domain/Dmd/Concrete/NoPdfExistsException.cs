﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Dmd
{
    public class NoPdfExistsException : DmdException
    {
        public NoPdfExistsException() : base()
        {
        }

        public NoPdfExistsException(string message) : base(message)
        {
        }

        public NoPdfExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
