﻿using Aquilent.Cara.Reference;
using Aquilent.Cara.Domain.Aws.DynamoDB;
using System.Collections.Generic;
using Amazon.DynamoDBv2.Model;

namespace Aquilent.Cara.Domain.Ingress
{
	public interface IFormLetterDetector
	{
        LetterType Compare(LetterSenderComposite oldLetter, Letter newLetter, out double confidence, DynamoDBHelper dynamo = null, List<string> newLetterFragKeys = null);
        List<string> FragmentKeys(string text);
        List<string> FragmentKeys(int phaseId, int letterId);
        List<string> GetRelevantFragmentKeys(List<string> fragmentKeys);
	}
}
