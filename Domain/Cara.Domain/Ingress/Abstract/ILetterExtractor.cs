﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Ingress
{
	public interface ILetterExtractor
	{
		IList<Letter> Extract();
		bool IsValid { get; }
	}
}
