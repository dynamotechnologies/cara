﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using Aquilent.Framework.Services;
using ICSharpCode.SharpZipLib.Zip;
using Independentsoft.Pst;
using log4net;
using Aquilent.Cara.Domain.Aws.Sqs;

namespace Aquilent.Cara.Domain.Ingress
{
    public class ProcessLetterUploadQueueServiceTask : IServiceTask
    {
        #region Private Members
        private ILog _logger;
        private static string letterUploadFolder;
        private static int letterBulkBatchSize;
        private static readonly string _queueNameKey = "cara.queue.letterupload";
        #endregion Private Members

        #region Static Constructor
        static ProcessLetterUploadQueueServiceTask()
        {
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("ETFV-FT6C-6R7H-ACFA");
            using (var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>())
            {
                var setting = resourceManager.Get("resource.letterupload.folder");

                if (setting == null)
                {
                    throw new ApplicationException("Missing resource in the database:  resource.letterupload.folder");
                }

                letterUploadFolder = setting.Value;
            }

            if (!int.TryParse(ConfigurationManager.AppSettings["letterBulkLoadBatchSize"], out letterBulkBatchSize))
            {
                letterBulkBatchSize = 100;
            }
        }
        #endregion Static Constructor

        #region IServiceTask Methods
        public string Name
		{
			get { return "Letter Upload Queue"; }
		}

        public void RunServiceTask()
        {
            log4net.ThreadContext.Properties["ThreadContext"] = Name;
            _logger = LogManager.GetLogger(typeof(ProcessLetterUploadQueueServiceTask));

            using (var letterUploadQueueManager = ManagerFactory.CreateInstance<LetterUploadQueueManager>())
            {
                try
                {
                    string queueName;
                    var sqs = SqsHelper.GetQueue(_queueNameKey, out queueName);

                    var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                    var createQueueResponse = sqs.CreateQueue(sqsRequest);
                    string queueUrl = createQueueResponse.QueueUrl;

                    int numberOfMessages = 0;
                    do
                    {
                        #region Retrieve messages from the queue
                        IList<int> idList;
                        var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = 1 }; // get one at a time
                        var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                        numberOfMessages = 0;
                        #endregion Retrieve messages from the queue

                        if (receiveMessageResponse.Messages != null)
                        {
                            numberOfMessages = receiveMessageResponse.Messages.Count;
                            idList = new List<int>(numberOfMessages);

                            foreach (var message in receiveMessageResponse.Messages)
                            {
                                idList.Add(int.Parse(message.Body));
                            }

                            // Get all of the custom reports that are queued up
                            var entries = letterUploadQueueManager.All.Where(x => idList.Contains(x.LetterUploadQueueId)).ToList();

                            foreach (var entry in entries)
                            {
                                try
                                {
                                    // Update the status
                                    entry.LetterUploadStatus = LetterUploadStatus.Processing;
                                    letterUploadQueueManager.UnitOfWork.Save();

                                    var project = entry.Phase.Project;

                                    User user;
                                    using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                                    {
                                        user = userManager.Get(entry.UserId);
                                    }

                                    int letterCount = 0;
                                    using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                                    {
                                        using (var orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                                        {
                                            ILetterExtractor extractor = null;

                                            #region Get the letter extractor
                                            try
                                            {
                                                extractor = GetLetterExtractor(entry, user.Username, docManager, orgManager);
                                            }
                                            catch (LetterExtractionException lee)
                                            {
                                                entry.LetterUploadStatus = LetterUploadStatus.Error;
                                                letterUploadQueueManager.UnitOfWork.Save();

                                                _logger.Error(string.Format(
                                                    "Error occurred processing {0}: ID = {1}, PhaseId = {2}, Username = {3}",
                                                    entry.StoredFilename,
                                                    entry.LetterUploadQueueId,
                                                    entry.PhaseId,
                                                    user.Username), lee);

                                                // Send e-mail
                                                letterUploadQueueManager.SendQueueEntryErrorNotification(entry, user, lee.Message);
                                            }
                                            #endregion Get the letter extractor

                                            if (extractor != null)
                                            {
                                                try
                                                {
                                                    #region Extract the letters from the file
                                                    // Extract the letters from the file
                                                    _logger.InfoFormat("Starting extraction: {0}, {1} bytes", entry.StoredFilename, entry.FileSize);
                                                    var letters = extractor.Extract();
                                                    _logger.InfoFormat("Finished extraction: {0}, {1} bytes, {2} letters", entry.StoredFilename, entry.FileSize, letters.Count);
                                                    #endregion Extract the letters from the file

                                                    #region Save the letters to the database
                                                    // Save the letters to the database
                                                    // (includes sending notifications and adding subscribers to the datamart)
                                                    _logger.InfoFormat("Starting letter save: {0}, {1} bytes, {2} letters", entry.StoredFilename, entry.FileSize, letters.Count);
                                                    var sequences = SaveLetters(letters, entry.Phase, user.Username, orgManager);
                                                    letterCount = letters.Count;
                                                    _logger.InfoFormat("Finished letter save: {0}, {1} bytes, {2} letters", entry.StoredFilename, entry.FileSize, letterCount);
                                                    #endregion Save the letters to the database

                                                    #region Letter Post Processing
                                                    // Add Subscribers and Send Notifications
                                                    LetterPostProcessing(letters, entry.Phase, sequences);

                                                    entry.LetterCount = letterCount;
                                                    entry.LetterUploadStatus = LetterUploadStatus.Completed;
                                                    entry.ProcessDate = DateTime.UtcNow;

                                                    // Send e-mail
                                                    letterUploadQueueManager.SendQueueEntryProcessedNotification(entry, user);
                                                    #endregion Letter Post Processing
                                                }
                                                catch (Exception ex)
                                                {
                                                    entry.LetterUploadStatus = LetterUploadStatus.Error;
                                                    _logger.Error(string.Format(
                                                        "Error occurred processing {0}: ID = {1}, PhaseId = {2}, Username = {3}",
                                                        entry.StoredFilename,
                                                        entry.LetterUploadQueueId,
                                                        entry.PhaseId,
                                                        user.Username), ex);

                                                    // Send e-mail
                                                    letterUploadQueueManager.SendQueueEntryErrorNotification(entry, user, null);
                                                }
                                            }
                                            else
                                            {
                                                entry.LetterUploadStatus = LetterUploadStatus.Error;
                                            }
                                        }
                                    }

                                    #region Update the letter upload queue
                                    // Update the entry
                                    letterUploadQueueManager.UnitOfWork.Save();
                                    #endregion Update the letter upload queue

                                    #region Delete the uploaded file and remove the entry from the queue
                                    var message = receiveMessageResponse.Messages.First(x => x.Body == entry.LetterUploadQueueId.ToString());
                                    sqs.DeleteMessage(new DeleteMessageRequest { QueueUrl = queueUrl, ReceiptHandle = message.ReceiptHandle });

                                    // Remove the file from the filesystem
                                    if (entry.LetterUploadStatus != LetterUploadStatus.Error)
                                    {
                                        using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                                        {
                                            S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, letterUploadFolder);
                                            S3FileInfo fileInfo = rootDirectory.GetFile(entry.StoredFilename);

                                            try
                                            {
                                                _logger.InfoFormat("Deleting: {0}", entry.StoredFilename);
                                                fileInfo.Delete();
                                            }
                                            catch (Exception ex)
                                            {
                                                _logger.Error(string.Format("An error occurred trying to delete {0}", entry.StoredFilename), ex);
                                            }
                                        }
                                    }
                                    #endregion Delete the uploaded file and remove the entry from the queue
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(string.Format("An error occurred trying to process {0}", entry.Filename), ex);
                                }
                            }
                        }
                    } while (numberOfMessages > 0);
                }
                catch (Exception ex)
                {
                    _logger.Error(
                        "An error occurred attempting to process the letter upload queue.",
                        ex);
                }
            }
        }
        #endregion IServiceTask Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="letters"></param>
        /// <param name="phase"></param>
        /// <param name="username"></param>
        /// <param name="orgManager"></param>
        private IList<int?> SaveLetters(IList<Letter> letters, Phase phase, string username, OrganizationManager orgManager)
        {
            IList<int?> sequences = null;

            LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>();
            letterManager.UnitOfWork = orgManager.UnitOfWork;

            // save each letter from the extracted letters
            foreach (Letter letter in letters)
            {
                // set the required letter data
                letter.PhaseId = phase.PhaseId;
                letter.LastUpdated = letter.DateEntered = DateTime.UtcNow;
                letter.Deleted = false;
                letter.LastUpdatedBy = username;
                letter.LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().SelectionList.First(x => x.Value == "New").Key;
                letter.LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList.First(x => x.Value == "Pending").Key;
                letter.WithinCommentPeriod = phase.IsWithinCommentPeriod(letter.DateSubmitted, phase.TimeZone);
                letterManager.FixLetterText(letter);

                #region Create Objection Info
                if (phase.PhaseTypeId == 3) // Objection
                {
                    string unitId;
                    string regulationNumber;
                    unitId = phase.Project.UnitId;
                    if (phase.Project.CommentRuleId.HasValue)
                    {
                        int regulationId = phase.Project.CommentRuleId.Value;
                        regulationNumber = LookupManager.GetLookup<LookupCommentRule>()[regulationId].Name.Contains("218") ? "218" : "219";
                    }
                    else
                    {
                        regulationNumber = "N/A";
                    }

                    var now = DateTime.UtcNow;
                    var thisYear = now.Year.ToString().Substring(2, 2);
                    var nextYear = (now.Year + 1).ToString().Substring(2, 2);

                    letter.LetterObjection = new LetterObjection
                    {
                        FiscalYear = now.Month <= 9 ? thisYear : nextYear,
                        Region = unitId.Substring(2, 2),
                        Forest = unitId.Substring(4, 2),
                        ReviewStatusId = 1, // New
                        OutcomeId = null,
                        MeetingInfo = null,
                        RegulationNumber = regulationNumber,
                        SignerFirstName = null,
                        SignerLastName = null,
                        ObjectionDecisionMakerId = null,
                        ResponseDate = null
                    };
                }
                #endregion Create Objection Info

                // set the required author data
                foreach (Author author in letter.Authors)
                {
                    author.Deleted = false;
                    author.LastUpdated = DateTime.UtcNow;
                    author.LastUpdatedBy = username;

                    if (!string.IsNullOrEmpty(author.Email))
                    {
                        author.ContactMethod = "Email";
                    }

                    // set last and first name to anon if blank
                    Author.FixAuthorName(author);
                }
            }

            // Save the letters to the database
            letterManager.BulkLoad(letters, letterBulkBatchSize);

            // Retrieve the sequence numbers of the newly inserted letters
            var listOfIds = letters.Select(x => x.LetterId).ToList();
            sequences = letterManager.All
                .Where(x => listOfIds.Contains(x.LetterId) && x.LetterSequence % 10 == 0)
                .Select(x => x.LetterSequence)
                .ToList();

            return sequences;
        }
  
        private void LetterPostProcessing(IList<Letter> letters, Phase phase, IList<int?> sequences)
        {
            using (var dmManager = new DataMartManager())
            {
                foreach (var letter in letters)
                {
                    if (phase.Project.IsPalsProject)
                    {
                        foreach (var author in letter.Authors)
                        {
                            if (author.ContactMethod != null)
                            {
                                dmManager.AddSubscriber(phase.ProjectId, author);
                            }
                        }
                    }
                }

                if (sequences != null && sequences.Count > 0)
                {
#if !DEBUG
                    // Send the new letter notifications in a separate thread
                    Thread thread = new Thread(d => NotificationManager.SendNewLettersArrivedNotification(phase, sequences));
                    thread.Start();
#endif
                }
            }
        }

        private ILetterExtractor GetLetterExtractor(LetterUploadQueue entry, string username, DocumentManager docManager, OrganizationManager orgManager)
        {
            ILetterExtractor extractor = null;

            try
            {
                using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, letterUploadFolder);
                    Stream s3FileStream = rootDirectory.GetFile(entry.StoredFilename).OpenRead();

                    switch (entry.LetterUploadFileType)
                    {
                        case LetterUploadFileType.PST:
                            var pstFile = new PstFile(s3FileStream);
                            extractor = new OutlookPstLetterExtractor(username, pstFile, docManager);
                            break;
                        case LetterUploadFileType.FDMS:
                            var zipFile = new ZipFile(s3FileStream);
                            extractor = new FdmsLetterExtractor(username, zipFile, docManager, orgManager);
                            break;
                        default:
                            extractor = null;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new LetterExtractionException("The uploaded file is not in a recognized format.");
            }

            return extractor;
        }
        #endregion Private Methods
    }
}
