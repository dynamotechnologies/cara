﻿using Aquilent.Cara.Reference;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using GemBox.Spreadsheet;
using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Dmd;

namespace Aquilent.Cara.Domain.Ingress
{
	public class FdmsLetterExtractor : ILetterExtractor
	{
		#region Properties
		private ZipFile FdmsZipFile { get; set; }
		private IEnumerable<ZipEntry> ZipEntries { get; set; }
		private DocumentManager DocumentManager { get; set; }
		private OrganizationManager OrganizationManager { get; set; }
		private ZipEntry XlsxZipEntry { get; set; }
		private string Username { get; set; }
		private readonly string _xlsxExtension = ".xlsx";
		private readonly string _folderSuffix = "_docs";
		private ILog _logger;

		public bool IsValid
		{
			get
			{
				return Verify();
			}
		}
		#endregion Properties

		#region Constructors
		public FdmsLetterExtractor(string username, ZipFile fdmsZipFile, DocumentManager documentManager, OrganizationManager organizationManager)
		{
			_logger = LogManager.GetLogger(typeof(FdmsLetterExtractor));

			FdmsZipFile = fdmsZipFile;
			ZipEntries = FdmsZipFile.Cast<ZipEntry>();
			XlsxZipEntry = ZipEntries.FirstOrDefault(x => x.Name.EndsWith(_xlsxExtension) && !x.Name.Contains(@"/"));

			DocumentManager = documentManager;
            DocumentManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdSend);

			OrganizationManager = organizationManager;
			Username = username;

			if (!IsValid)
			{
				_logger.FatalFormat("{0} does not represent a valid FDMS export file", fdmsZipFile.Name);
				throw new LetterExtractionException("The zip file is not a valid FDMS file");
			}
		}
		#endregion Constructors

		#region ILetterExtractor methods
		public IList<Letter> Extract()
		{
			_logger.DebugFormat("Start:  Extract letters from {0}", FdmsZipFile.Name);

			var letterList = new List<Letter>();

			// Unzip the file in memory and load it.
			ExcelFile ef = new ExcelFile();
			using (var s = FdmsZipFile.GetInputStream(XlsxZipEntry))
			{
				byte[] b = new byte[XlsxZipEntry.Size];
				using (var ms = new MemoryStream(b, true))
				{
					DocumentManager.WriteStreamToStream(s, ms);
					ms.Position = 0;

					ef.LoadXlsx(ms, XlsxOptions.None);
				}
			}

			// Parse the xlsx file and start to build the letter entities
			var sheet = ef.Worksheets[0]; // there should only be one sheet that we care about.

			// Isolate the rows that pertain to public comments and order them so that
			// the corresponding attachment rows are adjacent to their letter.
            // CARA-971 We no longer varify the document type and subtype
			var rows = sheet.Rows.Cast<ExcelRow>().Skip(1).OrderBy(x => x.Cells["B"].Value.ToString()).ToList();

			// Loop through the rows in the excel
			Letter letter;
			Author author;
			string hyperlink;
			string hyperlinkText;

			int rowCount = rows.Count;
            //CARA-971
			for (int i = 0; i < rowCount; i++)
			{
				var cells = rows[i].Cells;

				letter = new Letter();
				letter.LastUpdated = DateTime.UtcNow;
				letter.LastUpdatedBy = Username;
				letter.DateEntered = DateTime.UtcNow;
				letter.Protected = true;
                letter.DeliveryTypeId = 12; // Regulations.gov

				//Column N:  Received Date
				letter.DateSubmitted = Convert.ToDateTime(cells["N"].Value);

				// Build the Author Entity
				author = GetAuthorFromCellRange(cells);

				// Add the author to the letter
				letter.Authors.Add(author);

				// Grab the letter content from the htm files
				//		Column B:  Link to comment document is in the cell formula
				//		Formulas are in the format:  =HYPERLINK("{URL}", "{DISPLAYED_TEXT}")
				var cellHyperlinkFormula = cells["B"].Formula;
				GetHyperlinkFromFormula(cellHyperlinkFormula, out hyperlink, out hyperlinkText);
				Document commentFile;
				letter.Text = ExtractLetterText(hyperlink);
                letter.OriginalText = letter.Text;

				// Associate any attachments
                //CARA-971 retrieve attachments from the next sheet
                IList<Document> attachments = new List<Document>();
                if (ef.Worksheets.Count > 1 && cells["D"].Formula != null)
                {
                    string searchKey = String.Format("!D{0})", i+2);
                    var attachmentSheet = ef.Worksheets[1];
                    var attachmentRows = 
                        attachmentSheet.Rows.Cast<ExcelRow>()
                        .Where(x => x.Cells["A"].Formula != null)
                        .Where(x => x.Cells["A"].Formula.Contains(searchKey))
                        .OrderBy(x => x.Cells["B"].Value)
                        .ToList();
                    foreach (ExcelRow attachmentRow in attachmentRows)
                    {
                        cellHyperlinkFormula = attachmentRow.Cells["C"].Formula;
                        if (cellHyperlinkFormula != null)
                        {
                            GetHyperlinkFromFormula(cellHyperlinkFormula, out hyperlink, out hyperlinkText);
                            Document attachment;
                            ZipEntry attachmentZipEntry = FdmsZipFile.GetEntry(hyperlink);
                            using (var zipEntryInputStream = FdmsZipFile.GetInputStream(attachmentZipEntry))
                            {
                                byte[] b = new byte[attachmentZipEntry.Size];
                                using (var ms = new MemoryStream(b, true))
                                {
                                    DocumentManager.WriteStreamToStream(zipEntryInputStream, ms);
                                    ms.Position = 0;
                                    try
                                    {
                                        attachment = IngressUtilities.PrepareDocument(DocumentManager, ms, attachmentZipEntry,
                                            FdmsZipFile.Name, Username, hyperlinkText);
                                        attachments.Add(attachment);
                                    }
                                    catch (InvalidFileTypeException ifte)
                                    {
                                        // Ignore and move on
                                    }
                                }
                            }
                        }
				    }
                }

				// Persist the attachments
				DocumentManager.UnitOfWork.Save();

				// Add the attachments to the letter
                //CARA-714:- Set LetterDocument.Protected = True, when letter attachments are uploaded via FDMS Ingress
				foreach (var d in attachments)
				{
					letter.LetterDocuments.Add(new LetterDocument { DocumentId = d.DocumentId, Protected = true });
				}

				// Add the letter to the list
				letterList.Add(letter);
			}

			_logger.DebugFormat("End:  Extracted {1} letters from {0}", FdmsZipFile.Name, letterList.Count);

			return letterList;
		}

		#endregion ILetterExtractor methods

		#region Methods
		/// <summary>
		/// Verifies that the zip file containing the letters is a valid
		/// zip file.  AFAIK, the zip file contains an xlsx file 
		/// </summary>
		/// <returns></returns>
		private bool Verify()
		{
			bool valid = false;

			try
			{
				// Get the count of entries
				var totalEntryCount = ZipEntries.Count();

				// Isolate the name of the file minus the extension
				//var nameMinusExtension = XlsxZipEntry.Name.Substring(0, XlsxZipEntry.Name.Length - _xlsxExtension.Length);
                var nameMinusExtension = XlsxZipEntry.Name.Substring(0, 32);

				// All of the other entries should be in a folder called {nameMinusExtension}_docs
				var otherFilesCount = ZipEntries.Where(x => x.Name.StartsWith(nameMinusExtension + _folderSuffix)).Count();

				if (totalEntryCount - otherFilesCount == 1)
				{
					valid = true;
				}
			}
			catch
			{
				valid = false;
			}

			return valid;
		}

		/// <summary>
		/// Extracts the author information from a row in the Excel file
		/// </summary>
		/// <param name="cells">Represntes the Row.AllocatedCells</param>
		/// <returns>Author</returns>
		private Author GetAuthorFromCellRange(CellRange cells)
		{
			var author = new Author();
			author.Sender = true;

            //author.FirstName = CellValue(cells["AA"]);
            // 07-17-2019 - NOW it's X
            author.FirstName = CellValue(cells["X"]);

            //author.LastName = CellValue(cells["AB"]);
            // 07-17-2019 - NOW it's Y
            author.LastName = CellValue(cells["Y"]);

            //author.Address1 = CellValue(cells["AC"]);
            // 07-17-2019 - NOW it's Z
            author.Address1 = CellValue(cells["Z"]);

			// Address2 is now AD (was previously Column Z:  Address2)
            //author.Address2 = CellValue(cells["AD"]);
            // 07-17-2019 - NOW it's AA
            author.Address2 = CellValue(cells["AA"]);

			// City is now column AE (was previously Column AA: City)
            //author.City = CellValue(cells["AE"]);
            // 07-17-2019 NOW it's AB
            author.City = CellValue(cells["AB"]);

            //Trim Author name fields.  CARA-1276
            author.FirstName = author.FirstName != null ? author.FirstName.Trim() : null;
            author.LastName = author.LastName != null ? author.LastName.Trim() : null;

			// Country is now AF (was previously Column AB: Country)
            //var country = LookupManager.GetLookup<LookupCountry>().FirstOrDefault(c => c.Name == CellValue(cells["AF"]));
            // 07-17-2019 NOW it's AC
            var country = LookupManager.GetLookup<LookupCountry>().FirstOrDefault(c => c.Name == CellValue(cells["AC"])); 
            if (country != null)
			{
				author.CountryId = country.CountryId;
			}

			// ZIP code is now Column AH (was previously Column AD:  ZIP Code)
            //author.ZipPostal = CellValue(cells["AH"]);
            // 07-17-2019 NOW it's AE
            author.ZipPostal = CellValue(cells["AE"]);

			// Email is now column AI (was previously Column AE:  Email Address)
            //author.Email = CellValue(cells["AI"]);
            // 07-17-2019 NOW it's AF
            author.Email = CellValue(cells["AF"]);

			// State is now column AG (was previously Column AC:  State or Province)
            //string cellValue = CellValue(cells["AG"]);
            // 07-17-2019 NOW it's AD
            string cellValue = CellValue(cells["AD"]); 
            if (cellValue != null)
            { 
                if (cellValue.Length >= 2)
                {
                    try
                    {
                        State state = null;
                        LookupState lkuState = LookupManager.GetLookup<LookupState>();
                        if (cellValue.Length == 2)
                        {
                            state = lkuState[cellValue.ToUpper()];
                        }
                        else
                        {
                            state = lkuState.FirstOrDefault(x => x.Name == cellValue);
                        }

                        if (state != null)
                        {
                            author.StateId = state.StateId;
                        }
                    }
                    catch (KeyNotFoundException)
                    {
                        author.ProvinceRegion = cellValue;
                    }
                }
                else if (cellValue.Length > 2)
                {
                    author.ProvinceRegion = cellValue;
                }
            }

			// Organization Name is now Column AL (was previously Column AG:  Organization Name)
            //string organizationName = CellValue(cells["AL"]);
            // 07-17-2019 NOW it's AI
            string organizationName = CellValue(cells["AI"]); 
            if (organizationName != null)
			{
				var organization = OrganizationManager.Get(organizationName);
                if (organization == null)
                {
                    organization = new Organization { Name = organizationName };
                    author.Organization = organization;
                }
                else
                {
                    author.OrganizationId = organization.OrganizationId;
                }
			}

			return author;
		}

		/// <summary>
		/// Gets the {URL} of the Formula:  =HYPERLINK("{URL}", "{DISPLAYED_TEXT}")
		/// </summary>
		/// <param name="cellHyperlinkFormula"></param>
		/// <returns></returns>
		private void GetHyperlinkFromFormula(string cellHyperlinkFormula, out string hyperlink, out string hyperlinkText)
		{
			var hyperlinkStartTokenIdx = cellHyperlinkFormula.IndexOf("\"");
			var hyperlinkEndTokenIdx = cellHyperlinkFormula.IndexOf("\"", hyperlinkStartTokenIdx + 1);
			hyperlink = cellHyperlinkFormula.Substring(hyperlinkStartTokenIdx + 1, hyperlinkEndTokenIdx - hyperlinkStartTokenIdx - 1).Replace(@"\", "/");

			var commaIdx = cellHyperlinkFormula.IndexOf(",");
			var hyperlinkTextStartTokenIdx = cellHyperlinkFormula.IndexOf("\"", commaIdx);
			var hyperlinkTextEndTokenIdx = cellHyperlinkFormula.IndexOf("\"", hyperlinkTextStartTokenIdx + 1);
			hyperlinkText = cellHyperlinkFormula.Substring(hyperlinkTextStartTokenIdx + 1, hyperlinkTextEndTokenIdx - hyperlinkTextStartTokenIdx - 1);
		}

		/// <summary>
		/// Retrieves the comment HTML file from the zip file and extracts
		/// the comment text from the inner text of the last &lt;p&gt;.
		/// </summary>
		/// <param name="commentHtmlFile"></param>
		/// <returns></returns>
		private string ExtractLetterText(string commentHtmlFile)
		{
			string letterText;
			ZipEntry commentFile = FdmsZipFile.GetEntry(commentHtmlFile);
			MemoryStream commentFileStream = new MemoryStream();
			using (commentFileStream)
			{
				using (var zipEntryInputStream = FdmsZipFile.GetInputStream(commentFile))
				{
					byte[] b = new byte[commentFile.Size];
					DocumentManager.WriteStreamToStream(zipEntryInputStream, commentFileStream);
					commentFileStream.Position = 0;
				}

				using (StreamReader reader = new StreamReader(commentFileStream))
				{
					//var contents = reader.ReadToEnd();
					// Parse the contents to return the comment
					HtmlDocument htmlDoc = new HtmlDocument();
					htmlDoc.Load(reader);

					// The comment should be in the last <p> tag

                    var paragraphCount = htmlDoc.DocumentNode.DescendantNodes().Where(x => x.Name.ToLower() == "p").Count();

                    if (paragraphCount >= 5)
                    {
                        letterText = htmlDoc.DocumentNode.DescendantNodes().Where(x => x.Name.ToLower() == "p").Skip(4).First().InnerText.Replace("<br>", "\n");
                    }

                    else
                    {
                        letterText = htmlDoc.DocumentNode.DescendantNodes().Where(x => x.Name.ToLower() == "p").Skip(3).First().InnerText.Replace("<br>", "\n");
                    }
				}
			}

			return letterText;
		}

		private IList<Document> ExtractLetterAttachments(List<ExcelRow> rows, ref int rowCounter, string hyperlinkText)
		{
			string hyperlink;
			string documentName;
			IList<Document> attachments = new List<Document>();
			while (rowCounter < rows.Count && rows[rowCounter].Cells["B"].Formula.IndexOf(hyperlinkText + ".") > -1)
			{
				string attachmentFormula = rows[rowCounter].Cells["B"].Formula;
				GetHyperlinkFromFormula(attachmentFormula, out hyperlink, out documentName);
				Document attachment;
				ZipEntry attachmentZipEntry = FdmsZipFile.GetEntry(hyperlink);
				using (var zipEntryInputStream = FdmsZipFile.GetInputStream(attachmentZipEntry))
				{
					byte[] b = new byte[attachmentZipEntry.Size];
					using (var ms = new MemoryStream(b, true))
					{
						DocumentManager.WriteStreamToStream(zipEntryInputStream, ms);
						ms.Position = 0;

						attachment = IngressUtilities.PrepareDocument(DocumentManager, ms, attachmentZipEntry, 
							FdmsZipFile.Name, Username, documentName);
						attachments.Add(attachment);
					}
				}

				rowCounter++;
			}

			return attachments;
		}

		private string CellValue(ExcelCell cell)
		{
			string cellValue = null;

			if (cell.Value != null && ! string.IsNullOrWhiteSpace(cell.Value.ToString())) 
			{
				cellValue = cell.Value.ToString();
			}

			return cellValue;
		}
		#endregion Methods
	}
}
