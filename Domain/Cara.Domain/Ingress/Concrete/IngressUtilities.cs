﻿using Aquilent.Framework.Documents;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;

namespace Aquilent.Cara.Domain.Ingress
{
	public static class IngressUtilities
	{
		public static ILog _logger = LogManager.GetLogger(typeof(IngressUtilities));

		/// <summary>
		/// 
		/// </summary>
		/// <param name="docManager"></param>
		/// <param name="stream"></param>
		/// <param name="zipEntry"></param>
		/// <param name="username"></param>
		/// <param name="originalName"></param>
		/// <returns></returns>
		public static Document PrepareDocument(DocumentManager docManager, Stream stream, ZipEntry zipEntry, string zipFileName, string username, string originalName)
		{
			var fileAttachment = new Aquilent.Framework.Documents.File(stream);
			fileAttachment.Size = Convert.ToInt32(zipEntry.Size);
			fileAttachment.OriginalName = zipEntry.Name;
			fileAttachment.Uploaded = DateTime.UtcNow;
			fileAttachment.UploadedBy = username;

			try
			{
				fileAttachment.ContentType = docManager.ContentType(fileAttachment.Extension);
			}
			catch (KeyNotFoundException knfe)
			{
				_logger.WarnFormat("Skipping attachment: {0} in {1} because it is not an accepted file type", zipEntry.Name, zipFileName);
				throw knfe;
			}

			var attachment = new Document
			{
				Name = originalName,
				File = fileAttachment
			};

			docManager.Add(attachment);

			return attachment;
		}

		public static void ParseAuthorName(this Author author, string name)
		{
			try
			{
				// Parse mailAddress.DisplayName
				var nameParts = name.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

				// Try to populate the author's name properties
				if (nameParts.Length == 1)
				{
					author.LastName = nameParts[0];
				}
				else if (nameParts.Length == 2)
				{
					if (name.Contains(','))
					{
						author.FirstName = nameParts[1];
						author.LastName = nameParts[0];
					}
					else
					{
						author.FirstName = nameParts[0];
						author.LastName = nameParts[1];
					}
				}
				else if (nameParts.Length >= 3)
				{
					if (name.Contains(','))
					{
						author.LastName = nameParts[0];
						author.FirstName = nameParts[1];
						author.MiddleName = String.Join(" ", nameParts.Skip(2));
					}
					else
					{
						author.FirstName = nameParts[0];
						author.MiddleName = nameParts[1];
						author.LastName = String.Join(" ", nameParts.Skip(2));
					}
                }
			}
			catch (Exception)
			{
                author.FirstName = null;
                author.MiddleName = null;
                author.LastName = name.Trim();
			}
		}
	}
}
