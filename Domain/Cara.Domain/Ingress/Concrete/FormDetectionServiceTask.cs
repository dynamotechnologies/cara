﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using log4net;
using System.Configuration;

namespace Aquilent.Cara.Domain.Ingress
{
	public class FormDetectionServiceTask : IServiceTask
	{
        private ILog _logger = LogManager.GetLogger(typeof(FormDetectionServiceTask));
        private int _defaultBatchSize = 50;

		public string Name
		{
			get { return "Form Detection"; }
		}

        public void RunServiceTask()
        {
            var serviceTaskStart = DateTime.UtcNow;
            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
            {
                try
                {
                    letterManager.ProcessPendingLetters(serviceTaskStart);
                }
                catch (Exception ex)
                {
                    _logger.Error(
                        "An error occurred attempting to process pending letters",
                        ex);
                }
                finally
                {
                    try
                    {
                        letterManager.UnitOfWork.Save();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(
                            string.Format("An error occurred attempting to persist the changes made by the {0}", Name),
                            ex);
                    }
                }
            }
        }

	}
}
