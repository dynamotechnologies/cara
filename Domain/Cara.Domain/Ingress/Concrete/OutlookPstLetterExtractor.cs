﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Independentsoft.Pst;
using log4net;

namespace Aquilent.Cara.Domain.Ingress
{
	public class OutlookPstLetterExtractor : ILetterExtractor
	{
		#region Properties
		private DocumentManager DocumentManager { get; set; }
		private string Username { get; set; }
		private PstFile PstFile { get; set; }
		private ILog _logger;
		private readonly string DELETED_FILES_FOLDER_NAME = "Deleted Files";

		public bool IsValid
		{
			get
			{
				return Verify(); 
			}
		}
		#endregion Properties

		#region Constructors
		public OutlookPstLetterExtractor(string username, PstFile pstFile, DocumentManager documentManager)
		{
			_logger = LogManager.GetLogger(typeof(OutlookPstLetterExtractor));

			PstFile = pstFile;
			DocumentManager = documentManager;
            DocumentManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdSend);

			Username = username;

			if (!IsValid)
			{
				_logger.FatalFormat("{0} does not represent a valid Outlook PST file", pstFile.ToString());
				throw new LetterExtractionException("The file is not a valid Outlook PST file");
			}

		}
		#endregion Constructors

		#region ILetterExtractor Methods

        // Modified the below method as part of the fix for ticket CARA-732
        //CARA-732:-.When uploading a PST file, user gets the error: An error occurred while extracting the file - Object reference not set to an instance of an object. 
        public IList<Letter> Extract()
        {

            _logger.DebugFormat("Start:  Extract letters from {0}", PstFile.ToString());

            IList<Letter> letterList = new List<Letter>();

            using (PstFile)
            {
                FolderCollection allFolders = PstFile.MailboxRoot.GetFolders(true);
                allFolders.Add(PstFile.MailboxRoot);  // Otherwise the root of the pst file will be ignored

                foreach (Folder folder in allFolders)
                {
                    if (folder.DisplayName != DELETED_FILES_FOLDER_NAME)
                    {
                        ItemCollection items = folder.GetItems();

                        Letter letter;
                        Aquilent.Framework.Documents.Document originalEmail;
                        foreach (Item item in items)
                        {
                            if (item is Message)
                            {
                                var message = item as Message;
                                bool iskickBackLtr = isKickBackLetter(message.Subject);

                                if (!iskickBackLtr)
                                {
                                    letter = ExtractLetterAndAuthor(message);

                                    //CARA-7skip attachment
                                    var attachments = ExtractLetterAttachments(message);

                                    if (attachments != null && attachments.Count > 0)
                                    {
                                        // Save the extracted attachments
                                        DocumentManager.UnitOfWork.Save();

                                        //CARA-714:- Set LetterDocument.Protected = True, when letter attachments are uploaded via Email Ingress
                                        // Add the attachments to the letter
                                        foreach (var d in attachments)
                                        {
                                            letter.LetterDocuments.Add(new LetterDocument { DocumentId = d.DocumentId, Protected = true });
                                        }
                                    }

                                    letterList.Add(letter);

                                } //skip Letter if it is a KickBackLetter(Undelivered email)
                            }
                        }
                    }
                }
            }

            _logger.DebugFormat("End:  Extracted {1} letters from {0}", PstFile.ToString(), letterList.Count);

            return letterList;
        }
		#endregion ILetterExtractor Methods

		#region Private Methods
		/// <summary>
		/// Verifies that the pst file containing the letters is a valid
		/// pstfile.
		/// </summary>
		/// <returns></returns>
		private bool Verify()
		{
			bool valid = false;

			FolderCollection allFolders = PstFile.MailboxRoot.GetFolders(true);
			allFolders.Add(PstFile.MailboxRoot);  // Otherwise the root of the pst file will be ignored

			foreach (Folder folder in allFolders)
			{
				if (folder.ItemCount > 0 && folder.DisplayName != DELETED_FILES_FOLDER_NAME)
				{
					valid = true;
					break;
				}
			}

			return valid;
		}

        // Modified the below method as part of the fix for ticket CARA-732
        //CARA-732:-.When uploading a PST file, user gets the error: An error occurred while extracting the file - Object reference not set to an instance of an object. 
		private Letter ExtractLetterAndAuthor(Message emailedLetter)
		{
			Letter letter = new Letter();
			letter.DateEntered = DateTime.UtcNow;
			letter.LastUpdated = DateTime.UtcNow;
			letter.LastUpdatedBy = Username;
			letter.DateSubmitted = TimeZoneInfo.ConvertTimeToUtc(emailedLetter.MessageDeliveryTime);
			letter.Protected = false;
            letter.DeliveryTypeId = 3; // Email

            var author = new Author();
            author.Sender = true;
            author.Email = emailedLetter.SentRepresentingEmailAddress;
            ParseAuthorName(emailedLetter, author);

            letter.Authors.Add(author);
    
            if (emailedLetter.Body != null)
            {
                letter.Text = emailedLetter.Subject + "\n\n" + emailedLetter.Body.Replace("\r\n", "\n");
            }
            else
            {
                letter.Text = emailedLetter.Subject + "\n\n";
            }

            letter.OriginalText = letter.Text;
           
			return letter; 
           
		}

        // Added the below method as part of the fix for ticket CARA-732
        //CARA-732:-.When uploading a PST file, user gets the error: An error occurred while extracting the file - Object reference not set to an instance of an object. 
        /// <summary>
        /// Returns true if the letter was an Undelievered email.
        /// </summary>
        /// <param>Email Subject</param>
        private bool isKickBackLetter(string emailSubject)
        {
            bool kickedBack = false;

            if (!string.IsNullOrEmpty(emailSubject))
            {
                if (emailSubject.Trim().Length > 14)
                {
                    var emailSub = emailSubject.Trim().Substring(2, 13);

                    if (emailSub == "Undeliverable")
                    {
                        kickedBack = true;
                    }
                }
            }

            return kickedBack;

        }


		/// <summary>
		/// Populates the author's email, lastname, firstname, and middlename properties
		/// </summary>
		/// <param name="fromContent">The string containing the information</param>
		/// <param name="author">The author object to accept the values</param>
		private void ParseAuthorName(Message emailedLetter, Author author)
		{
			author.ParseAuthorName(emailedLetter.SentRepresentingName);
		}


        // Modified the below method as part of the fix for ticket CARA-732
        //CARA-732:-.When uploading a PST file, user gets the error: An error occurred while extracting the file - Object reference not set to an instance of an object. 
		
		private IList<Aquilent.Framework.Documents.Document> ExtractLetterAttachments(Message emailedLetter)
		{
			IList<Aquilent.Framework.Documents.Document> attachments = new List<Aquilent.Framework.Documents.Document>();
			Aquilent.Framework.Documents.Document attachment;
			string documentName;
            foreach (Independentsoft.Pst.Attachment emailAttachment in emailedLetter.Attachments)
            {
                documentName = emailAttachment.LongFileName;
                if (!string.IsNullOrEmpty(documentName))
                {
                    byte[] sourceData = emailAttachment.Data;
                    if (sourceData == null)
                    {
                        sourceData = emailAttachment.DataObject;
                    }

                    // If sourceData is still null, then we need to skip the attachment
                    if (sourceData != null)
                    {
                        int sourceDataLength = sourceData.Length;

                        byte[] b = new byte[sourceDataLength];
                        using (var ms = new MemoryStream(b, true))
                        {
                            emailAttachment.Save(ms);
                            ms.Position = 0;

                            var fileAttachment = new Aquilent.Framework.Documents.File(ms);
                            fileAttachment.Size = Convert.ToInt32(sourceDataLength);
                            fileAttachment.OriginalName = documentName;
                            fileAttachment.Uploaded = DateTime.UtcNow;
                            fileAttachment.UploadedBy = Username;

                            var contentType = DocumentManager.ContentType(fileAttachment.Extension);

                            if (string.IsNullOrEmpty(contentType))
                            {
                                _logger.WarnFormat("Skipping attachment: {0} because it is not an accepted file type", emailAttachment.FileName);
                                continue;
                            }

                            fileAttachment.ContentType = contentType;

                            attachment = new Aquilent.Framework.Documents.Document
                            {
                                Name = fileAttachment.OriginalName,
                                File = fileAttachment
                            };

                            DocumentManager.Add(attachment);
                            attachments.Add(attachment);
                        }
                    }
                    else
                    {
                        _logger.WarnFormat("Skipping attachment: {0} because it cannot be retrieved from the e-mail", emailAttachment.FileName);
                    }
                }
            }

			return attachments;
		}
		#endregion
	}
}
