﻿using Aquilent.Cara.Configuration.FormDetection;
using DiffPlex;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;
using System.Linq;

namespace Aquilent.Cara.Domain.Ingress
{
	public class FormLetterDetectorStats
	{
		#region Properties
		private int _totalFragments = 0;
		public int TotalFragments
		{
			get
			{
				var tf = 0;
				if (LetterSizeConfig != null)
				{
					tf = _totalFragments - (LetterSizeConfig.IgnoreTopFramentCount(_totalFragments) + LetterSizeConfig.IgnoreBottomFramentCount(_totalFragments));
				}

				return tf;
			}

			set
			{
				_totalFragments = value;
			}
		}

		public int Inserted { get; set; }
		public int Deleted { get; set; }
		public int Modified { get; set; }
		public int Unchanged { get; set; }
		public int Imaginary { get; set; }
		public LetterSize LetterSizeConfig { get; set; }
		#endregion Properties

		#region Constructors
		public FormLetterDetectorStats(string existingLetter, string newLetter, FormDetectionConfiguration config)
		{
			if (!string.IsNullOrWhiteSpace(existingLetter) && !string.IsNullOrWhiteSpace(newLetter))
			{
				IDiffer differ = new Differ();
				ISideBySideDiffBuilder diffBuilder = new SideBySideSentenceFragmentDiffer(differ);

				var diffModel = diffBuilder.BuildDiffModel(existingLetter, newLetter);

				Init(diffModel, config);
			}
		}
		#endregion Constructors

		#region Methods
		private void Init(SideBySideDiffModel diffModel, FormDetectionConfiguration config)
		{
			var fragments = diffModel.NewText.Lines;
			LetterSizeConfig = config.LetterSizes.Where(lsc => fragments.Count >= lsc.MinFragments && fragments.Count <= lsc.MaxFragments).FirstOrDefault();

			if (LetterSizeConfig != null)
			{
				var startIndex = LetterSizeConfig.IgnoreTopFramentCount(fragments.Count);
				var endIndex = fragments.Count - LetterSizeConfig.IgnoreBottomFramentCount(fragments.Count);
				TotalFragments = fragments.Count;

				if (endIndex > startIndex)
				{
					Inserted = 0;
					Unchanged = 0;
					Deleted = 0;
					Modified = 0;
					Imaginary = 0;
					for (var i = startIndex; i < endIndex; i++)
					{
						var piece = fragments[i];
						switch (piece.Type)
						{
							case ChangeType.Inserted:
								Inserted++;
								break;
							case ChangeType.Unchanged:
								Unchanged++;
								break;
							case ChangeType.Deleted:
								Deleted++;
								break;
							case ChangeType.Modified:
								Modified++;
								break;
							case ChangeType.Imaginary:
								Imaginary++;
								break;
						}
					}
				}
			}
		}

		public override string ToString()
		{
			return string.Format("Total Fragments: {0}, Unchanged: {1}, Modified: {2}, Inserted: {3}, Deleted: {4}",
				TotalFragments, Unchanged, Modified, Inserted, Imaginary);
		}
		#endregion Methods
	}
}
