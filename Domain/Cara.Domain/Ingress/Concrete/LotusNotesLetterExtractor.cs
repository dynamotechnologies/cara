﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using ICSharpCode.SharpZipLib.Zip;
using log4net;

namespace Aquilent.Cara.Domain.Ingress
{
	public class LotusNotesLetterExtractor : ILetterExtractor
	{
		#region Properties
		private readonly string _emailSuffix = ".rtf";
		private readonly string _emailIdentifier = "_exprt";
		private readonly string _attachmentDelimiter = "_ATTCHMNT_";
		private ZipFile LotusNotesZipFile { get; set; }
		private IEnumerable<ZipEntry> ZipEntries { get; set; }
		private DocumentManager DocumentManager { get; set; }
		private string Username { get; set; }
		private ILog _logger;

		public bool IsValid
		{
			get
			{
				return Verify(); 
			}
		}
		#endregion Properties

		#region Constructors
		public LotusNotesLetterExtractor(string username, ZipFile lnZipFile, DocumentManager documentManager)
		{
			_logger = LogManager.GetLogger(typeof(LotusNotesLetterExtractor));

			LotusNotesZipFile = lnZipFile;
			ZipEntries = LotusNotesZipFile.Cast<ZipEntry>();

			DocumentManager = documentManager;
			Username = username;

			if (!IsValid)
			{
				_logger.FatalFormat("{0} does not represent a valid Lotus Notes Export file", lnZipFile.Name);
				throw new LetterExtractionException("The zip file is not a valid Lotus Notes Export file");
			}

		}
		#endregion Constructors

		#region ILetterExtractor Methods
		public IList<Letter> Extract()
		{
			_logger.InfoFormat("Start:  Extract letters from {0}", LotusNotesZipFile.Name);

			IList<Letter> letterList = new List<Letter>();

			// Isolate the letters
			var emailedLetters = ZipEntries.Where(x => x.Name.EndsWith(_emailSuffix) && x.Name.Contains(_emailIdentifier));

			Letter letter;
			foreach (var emailedLetter in emailedLetters)
			{
				letter = ExtractLetterAndAuthor(emailedLetter);
				var attachments = ExtractLetterAttachments(emailedLetter);

				// Save the extracted attachments
                DocumentManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdSend);
				DocumentManager.UnitOfWork.Save();

				// Add the attachments to the letter
                //CARA-714:- Set LetterDocument.Protected = True, when letter attachments are uploaded via Email Ingress
				foreach (var d in attachments)
				{
					letter.LetterDocuments.Add(new LetterDocument { DocumentId = d.DocumentId, Protected = true });
				}

				letterList.Add(letter);
			}

			_logger.InfoFormat("End:  Extracted {1} letters from {0}", LotusNotesZipFile.Name, letterList.Count);

			return letterList;
		}
		#endregion ILetterExtractor Methods

		#region Private Methods
		/// <summary>
		/// Verifies that the zip file containing the letters is a valid
		/// zip file.  AFAIK, the zip file contains files ending with _emailSuffix.
		/// </summary>
		/// <returns></returns>
		private bool Verify()
		{
			bool valid = false;

			var emailedLettersCount = ZipEntries.Where(x => x.Name.EndsWith(_emailSuffix) && x.Name.Contains(_emailIdentifier)).Count();

			if (emailedLettersCount > 0)
			{
				valid = true;
			}

			return valid;
		}

		private Letter ExtractLetterAndAuthor(ZipEntry emailedLetter)
		{
			Letter letter = new Letter();
			letter.DateEntered = DateTime.UtcNow;
			letter.LastUpdated = DateTime.UtcNow;
			letter.LastUpdatedBy = Username;
			letter.Protected = false;
            letter.DeliveryTypeId = 3; // Email

			using (var s = LotusNotesZipFile.GetInputStream(emailedLetter))
			{
				// Unzip the file
				byte[] b = new byte[emailedLetter.Size];
				using (var ms = new MemoryStream(b, true))
				{
					DocumentManager.WriteStreamToStream(s, ms);
					ms.Position = 0;

					// Read the letter and parse out the important pieces
					using (var sr = new StreamReader(ms, Encoding.UTF7))
					{
						// The first line should start with "From: "
						string fromPrefix = "From: ";
						string fromLine = sr.ReadLine();
						if (fromLine.IndexOf(fromPrefix) < 0)
						{
							throw new LetterExtractionException(string.Format("The first line of the letter should start with \"From: \".  Actual line: {0}", fromLine));
						}

						// Parse the From line
						string fromContent = fromLine.Substring(fromPrefix.Length).Replace("\r\n", string.Empty).Replace("\n", string.Empty).Trim();
						Author author = new Author();
						author.Sender = true;
						ParseAuthorNameAndEmail(fromContent, author, emailedLetter);

						// The next line should be empty
						sr.ReadLine();

						// The second line should start with "Sent: "
						string sentPrefix = "Sent: ";
						string sentLine = sr.ReadLine();
						if (sentLine.IndexOf(sentPrefix) < 0)
						{
							throw new LetterExtractionException(string.Format("The second line of the letter should start with \"Sent: \".  Actual line: {0}", sentLine));
						}

						// Parse the sent line
						string sentContent = sentLine.Substring(sentPrefix.Length).Replace("\r\n", string.Empty).Replace("\n", string.Empty).Trim();
						try
						{
							letter.DateSubmitted = Convert.ToDateTime(sentContent);
						}
						catch (FormatException fe)
						{
							throw new LetterExtractionException(string.Format("The sent date could not be parsed from the letter: \"{0}\"", sentContent), fe);
						}

						// Continue to read lines until you reach the Subject line, then
						// grab the rest of the content
						string letterContent;
						while (true)
						{
							letterContent = sr.ReadLine();

							if (letterContent.StartsWith("Subject:"))
							{
								letterContent = letterContent + sr.ReadToEnd();
								break;
							}
						}

						letter.Authors.Add(author);
						letter.Text = letterContent;
                        letter.OriginalText = letterContent;
					}
				}
			}

			return letter;
		}

		/// <summary>
		/// Populates the author's email, lastname, firstname, and middlename properties
		/// </summary>
		/// <param name="fromContent">The string containing the information</param>
		/// <param name="author">The author object to accept the values</param>
		/// <param name="emailedLetter">The ZipEntry containing the letter content</param>
		private void ParseAuthorNameAndEmail(string fromContent, Author author, ZipEntry emailedLetter)
		{
			try
			{
				MailAddress mailAddress;
				mailAddress = new MailAddress(fromContent.Replace("\"", string.Empty));
				author.Email = mailAddress.Address;
				author.ParseAuthorName(mailAddress.DisplayName);
			}
			catch (FormatException fe)
			{
				author.Email = fromContent;
			}
		}

		private IList<Document> ExtractLetterAttachments(ZipEntry emailedLetter)
		{
			// Find all of the attachments for this letter
			var firstUnderscore = emailedLetter.Name.IndexOf("_");
			var emailAttachments = ZipEntries.Where(x => x.Name.StartsWith(emailedLetter.Name.Substring(0, firstUnderscore + 1))
				&& x.Name.Contains(_attachmentDelimiter));

			IList<Document> attachments = new List<Document>();
			Document attachment;
			string documentName;
			foreach (var emailAttachment in emailAttachments)
			{
				// The attachment name is after the _ATTCHMNT_ delimiter
				//		Example:  test\6_Sean Fiorito _sfiorito@phaseonecg_com__ATTCHMNT_PALS Data Approval _ SOPA Management.ppt
				documentName = emailAttachment.Name.Substring(emailAttachment.Name.IndexOf(_attachmentDelimiter) + _attachmentDelimiter.Length);
				using (var zipEntryInputStream = LotusNotesZipFile.GetInputStream(emailAttachment))
				{
					byte[] b = new byte[emailAttachment.Size];
					using (var ms = new MemoryStream(b, true))
					{
						DocumentManager.WriteStreamToStream(zipEntryInputStream, ms);
						ms.Position = 0;

						try
						{
							attachment = IngressUtilities.PrepareDocument(DocumentManager, ms, emailAttachment, 
								LotusNotesZipFile.Name, Username, documentName);
						}
						catch
						{
							continue;
						}

						attachments.Add(attachment);
					}
				}
			}

			return attachments;
		}
		#endregion
	}
}
