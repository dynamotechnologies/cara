﻿using Aquilent.Cara.Reference;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Domain.Aws.DynamoDB;
using Aquilent.EntityAccess;

using DiffPlex;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;
using System.Linq;
using Aquilent.Cara.Configuration.FormDetection;
using System;
using System.Collections.Generic;
using System.Configuration;
using log4net;

using Amazon.DynamoDBv2.Model;
using System.Text;
using System.Text.RegularExpressions;


namespace Aquilent.Cara.Domain.Ingress
{
	public class FormLetterDetector : IFormLetterDetector
    {
        #region Properties
        private FormDetectionConfiguration Config { get; set; }
        private DynamoDBHelper Dynamo { get; set; }
        private string[] _newLetterWords;
        private string _newLetter;
        private int _wordSampleSize = Convert.ToInt32(ConfigurationManager.AppSettings["formDetectionWordSampleSize"]);
        private ILog _logger = LogManager.GetLogger(typeof(FormLetterDetector));

        #endregion Properties

        #region Constructor
        public FormLetterDetector(FormDetectionConfiguration config)
		{
			Config = config;
            Dynamo = new DynamoDBHelper();
		}
        #endregion Constructor

        #region IFormLetterDetector Methods
        public LetterType Compare(LetterSenderComposite oldLetter, Letter newLetter, out double confidence, DynamoDBHelper dynamo = null, List<string> newLetterFragKeys = null)
        {
            if (dynamo != null)
            {
                Dynamo = dynamo;
            }

            int oldLetterSize = oldLetter.Size;
            confidence = 0;
            LookupLetterType letterTypeLookup = LookupManager.GetLookup<LookupLetterType>();
            LetterType letterType = letterTypeLookup.Where(x => x.Name == "Unique").First();
            List<string> oldLetterFragKeys = oldLetter.FragmentKeys;
            
            // Get the Letter size
            int newLetterSize = newLetter.Size.HasValue ? newLetter.Size.Value : 0;

            // Get the letter fragment keys
            if (newLetterSize > 0 && newLetterFragKeys == null)
            {
                newLetterFragKeys = FragmentKeys(newLetter.PhaseId, newLetter.LetterId);
            }

            if (oldLetterSize > 0 && oldLetterFragKeys == null)
            {
                oldLetter.FragmentKeys = FragmentKeys(oldLetter.PhaseId, oldLetter.LetterId);
                oldLetterFragKeys = oldLetter.FragmentKeys;
            }

            if ((newLetterFragKeys != null && newLetterFragKeys.Any()) || (oldLetterFragKeys != null && oldLetterFragKeys.Any()))
            {
                int newLetterFragCount = newLetterFragKeys.Count();
                int oldLetterFragCount = oldLetterFragKeys.Count();

                bool newerIsShorter = newLetterFragCount <= oldLetterFragCount;
                List<string> shortLetterFragKeys = newerIsShorter ? newLetterFragKeys : oldLetterFragKeys;
                List<string> longLetterFragKeys = newerIsShorter ? oldLetterFragKeys : newLetterFragKeys;

                int shorterFragCount = newerIsShorter ? newLetterFragCount : oldLetterFragCount;

                shortLetterFragKeys = GetRelevantFragmentKeys(shortLetterFragKeys);
                longLetterFragKeys = GetRelevantFragmentKeys(longLetterFragKeys);

                if (shortLetterFragKeys.Any() && longLetterFragKeys.Any())
                {
                    int matches = shortLetterFragKeys.Count(x => longLetterFragKeys.Contains(x));

                    LetterSize letterSizeCategory = Config.LetterSizes
                        .FirstOrDefault(x => shorterFragCount >= x.MinFragments && shorterFragCount <= x.MaxFragments);

                    int relevantFragmentCount = shortLetterFragKeys.Count();

                    if (letterSizeCategory != null && relevantFragmentCount > 0)
                    {
                        confidence = (double)matches / relevantFragmentCount;
                        confidence = Math.Min(confidence, 1.0);

                        double autoTrustConfidence = 1.1;
                        ConfidenceGrade lowestAutoTrust = Config.ConfidenceGrades
                            .Where(x => x.AutoTrust.ToLower() == "true")
                            .OrderBy(x => x.MinConfidence)
                            .FirstOrDefault();

                        if (lowestAutoTrust != null)
                        {
                            autoTrustConfidence = lowestAutoTrust.MinConfidence;
                        }

                        if (confidence >= autoTrustConfidence && confidence >= letterSizeCategory.MinConfidence)
                        {
                            int formPlusSizeDiff = Convert.ToInt32(letterSizeCategory
                                .Detectors.FirstOrDefault(x => x.Name == "formplus")
                                .Thresholds.FirstOrDefault(x => x.Name == "size").Value);

                            if ((newLetterSize - oldLetterSize) >= formPlusSizeDiff)
                            {
                                letterType = letterTypeLookup.Where(x => x.Name == "Form Plus").First();
                            }
                            else if ((oldLetterSize - newLetterSize) >= formPlusSizeDiff)
                            {
                                letterType = letterTypeLookup.Where(x => x.Name == "Master Form").First();
                            }
                            else
                            {
                                letterType = letterTypeLookup.Where(x => x.Name == "Form").First();
                            }
                        }
                    }
                }
            }

            return letterType;
        }

        /// <summary>
        /// returns the fragments keys formatted for storage in Dynamo DB
        /// </summary>
        /// <param name="letterId"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<string> FragmentKeys(string text)
        {
            List<string> fragmentkeys = new List<string>();
            if (string.IsNullOrWhiteSpace(text))
            {
                return fragmentkeys;
            }

            // Break letter into sentences
            char[] sentenceSplitTokens = new char[] { '.', '!', '?' };
            string[] sentences = text.Split(sentenceSplitTokens, StringSplitOptions.RemoveEmptyEntries);

            // If only one sentence is found, try to
            // divide the text using secondary delimiters
            if (sentences.Length == 1)
            {
                sentenceSplitTokens = new char[] { '-', ':', '*' };
                sentences = text.Split(sentenceSplitTokens, StringSplitOptions.RemoveEmptyEntries);
            }

            // Only consider sentences with more than 4 words
            char[] wordSplitTokens = new char[] { ' ', '\r', '\n' };
            int fragmentCount = 0;
            foreach (var sentence in sentences)
            {
                // Remove non-alpha characters
                var simpleSentence = Regex.Replace(sentence, @"[^a-zA-Z\s]", string.Empty);

                string[] words = simpleSentence.Split(wordSplitTokens, StringSplitOptions.RemoveEmptyEntries);

                // Only consider sentences that have more than 4 words
                // Fragment Key = First 3 words + Last 3 words
                if (words.Length > 4)
                {
                    StringBuilder key = new StringBuilder();
                    key.AppendFormat("{0}:", fragmentCount.ToString("X4")); // Prepend fragment number, so we can maintain order
                    for (int i = 0; i < 3; i++)
                    {
                        key.AppendFormat("{0} ", words[i].ToLower());
                    }

                    for (int i = 3; i > 0; i--)
                    {
                        key.AppendFormat("{0} ", words[words.Length - i].ToLower());
                    }

                    fragmentkeys.Add(key.ToString());
                    fragmentCount++;
                }
            }

            return fragmentkeys;
        }

        /// <summary>
        /// Gets the letter fragment keys from dynamoDB
        /// </summary>
        /// <param name="letterId"></param>
        /// <returns></returns>
        public List<string> FragmentKeys(int phaseId, int letterId)
        {
            List<string> fragmentKeys = null;
            List<KeyValuePair<string, AttributeValue>> item = Dynamo.GetItem(new List<KeyValuePair<string, AttributeValue>>()
            {
                new KeyValuePair<string,AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(letterId)),
                new KeyValuePair<string,AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(phaseId))
            });

            //if the dynamo query worked then return the fragment keys otherwise get them from the database
            if (item != null)
            {
                fragmentKeys = DynamoDBHelper.GetStringSetAttribute(item, "FragmentKeys");
            }
            
            if (item == null || fragmentKeys.Count == 0)
            {
                using (LetterManager letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    fragmentKeys = FragmentKeys(letterManager
                        .All
                        .Where(x=>x.LetterId == letterId)
                        .Select(x=>x.Text)
                        .FirstOrDefault());
                }
            }

            return fragmentKeys;
        }

        /// <summary>
        /// returns the relavent fragment keys based on the letter size category
        /// </summary>
        /// <param name="fragmentKeys"></param>
        /// <returns></returns>
        public List<string> GetRelevantFragmentKeys(List<string> fragmentKeys)
        {
            List<string> rawFragmentKeys = new List<string>();
            int numberOfFragments = fragmentKeys.Count();
            LetterSize letterSizeCategory = Config.LetterSizes
                .Where(x => x.MinFragments <= numberOfFragments && x.MaxFragments >= numberOfFragments)
                .FirstOrDefault();

            if (letterSizeCategory != null)
            {
                // ignoreTop and ignoreBottom is either a percent (with the % sign on the end)
                // or an integer
                string ignoreTopConfig = letterSizeCategory.IgnoreTop;
                string ignoreBottomConfig = letterSizeCategory.IgnoreBottom;

                int ignoreTop;
                if (ignoreTopConfig.EndsWith("%"))
                {
                    double ignoreTopPct = int.Parse(ignoreTopConfig.TrimEnd('%')) / 100.0;
                    ignoreTop = Convert.ToInt32(Math.Ceiling(ignoreTopPct * fragmentKeys.Count()));
                }
                else
                {
                    ignoreTop = int.Parse(ignoreTopConfig);
                }

                int ignoreBottom;
                if (ignoreBottomConfig.EndsWith("%"))
                {
                    double ignoreBottomPct = int.Parse(ignoreBottomConfig.TrimEnd('%')) / 100.0;
                    ignoreBottom = Convert.ToInt32(Math.Ceiling(ignoreBottomPct * fragmentKeys.Count()));
                }
                else
                {
                    ignoreBottom = int.Parse(ignoreBottomConfig);
                }

                if (numberOfFragments - ignoreTop - ignoreBottom > 0)
                {
                    fragmentKeys.Sort();
                    fragmentKeys = fragmentKeys.Skip(ignoreTop).Take(numberOfFragments - ignoreTop - ignoreBottom).ToList();
                }
            }

            // strip out the order prefix
            foreach (var key in fragmentKeys)
            {
                rawFragmentKeys.Add(key.Substring(key.IndexOf(':') + 1));
            }

            return rawFragmentKeys;
        }
		#endregion IFormLetterDetector Methods

		#region Custom Methods

        private void InitDynamo(string tableNameKey)
        {
            if (Dynamo == null)
            {
                Dynamo = new DynamoDBHelper(_logger);
            }
        }

		/// <summary>
		/// Returns true if the DiffStats that the letter is a form letter
		/// </summary>
		/// <param name="stats"></param>
		/// <returns></returns>
		private bool IsForm(FormLetterDetectorStats stats)
		{
			var result = false;

			if (stats.LetterSizeConfig != null)
			{
				var detector = stats.LetterSizeConfig.Detectors.Where(x => x.Name == "form").FirstOrDefault();

				var threshold1 = Convert.ToDouble(detector.Thresholds.First(x => x.Name == "U/NL").Value);
				var calculation1 = (double) stats.Unchanged / stats.TotalFragments;

				var threshold2 = Convert.ToDouble(detector.Thresholds.First(x => x.Name == "M/NL").Value);
				var calculation2 = (double) stats.Modified / stats.TotalFragments;

				if (calculation1 >= threshold1 && calculation2 <= threshold2)
				{
					result = true;
				}
			}
			return result;
		}

		/// <summary>
		/// Returns true if the DiffStats that the letter is a form plus letter
		/// </summary>
		/// <param name="stats"></param>
		/// <returns></returns>
		private bool IsFormPlus(FormLetterDetectorStats stats)
		{
			var result = false;

			if (stats.LetterSizeConfig != null)
			{
				var detector = stats.LetterSizeConfig.Detectors.FirstOrDefault(x => x.Name == "formplus");

				var threshold1 = Convert.ToDouble(detector.Thresholds.Where(x => x.Name == "(U+I)/NL").First().Value);
				var calculation1 = (double) (stats.Unchanged + stats.Inserted) / stats.TotalFragments;

				var threshold2 = Convert.ToDouble(detector.Thresholds.Where(x => x.Name == "M/NL").First().Value);
				var calculation2 = (double) stats.Modified / stats.TotalFragments;

				var threshold3 = Convert.ToDouble(detector.Thresholds.Where(x => x.Name == "U/M").First().Value);
				double calculation3 = threshold3;
				if (stats.Modified > 0)
				{
					calculation3 = (double) stats.Unchanged / stats.Modified;
				}

				var threshold4_lower = Convert.ToDouble(detector.Thresholds.Where(x => x.Name == "lower (I/U)").First().Value);
				var threshold4_upper = Convert.ToDouble(detector.Thresholds.Where(x => x.Name == "upper (I/U)").First().Value);
				double calculation4 = 1.0;
				if (stats.Unchanged > 0)
				{
					calculation4 = (double) stats.Inserted / stats.Unchanged;
				}

				if (calculation1 >= threshold1
					&& calculation2 <= threshold2
					&& calculation3 >= threshold3
					&& calculation4 <= threshold4_upper
					&& calculation4 >= threshold4_lower)
				{
					result = true;
				}
			}

			return result;
		}

		/// <summary>
		/// Returns true if the DiffStats that the letter is a form minus letter
		/// </summary>
		/// <param name="stats"></param>
		/// <returns></returns>
		private bool IsFormMinus(FormLetterDetectorStats stats)
		{
			var result = false;

			if (stats.LetterSizeConfig != null)
			{
				var detector = stats.LetterSizeConfig.Detectors.FirstOrDefault(x => x.Name == "formminus");

				var threshold1 = Convert.ToDouble(detector.Thresholds.Where(x => x.Name == "U/(NL-D)").First().Value);
				double calculation1 = 0;
				if (stats.TotalFragments - stats.Imaginary > 0)
				{
					calculation1 = (double) stats.Unchanged / (stats.TotalFragments - stats.Imaginary);
				}

				if (calculation1 >= threshold1)
				{
					result = true;
				}
			}

			return result;
		}
		#endregion Custom Methods
	}
}
