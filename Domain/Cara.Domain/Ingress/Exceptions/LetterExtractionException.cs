﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Ingress
{
	public class LetterExtractionException : ApplicationException
	{
		/// <summary>Initializes a new instance of the <see cref="T:Aquilent.Cara.Domain.Ingress.LetterExtractionException"/>
		/// class.</summary>
		public LetterExtractionException()
			: base()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:Aquilent.Cara.Domain.Ingress.LetterExtractionException"/>
		/// class with a specified error message.</summary>
		/// <param name="message">A message that describes the error. </param>
		public LetterExtractionException(string message)
			: base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:Aquilent.Cara.Domain.Ingress.LetterExtractionException"/>
		/// class with a specified error message and a reference to the inner exception that
		/// is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.
		/// </param>
		/// <param name="innerException">The exception that is the cause of the current exception.
		/// If the <paramref name="innerException"/> parameter is not a null reference, the
		/// current exception is raised in a catch block that handles the inner exception.
		/// </param>
		public LetterExtractionException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}
}
