﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an letter repository
    /// </summary>
    public interface ILetterRepository : IRepository<Letter>
    {
        #region Letter Interfaces
        /// <summary>
        /// Returns a list of letter search results
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<LetterSearchResult> GetLetterSearchResults(LetterSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Returns a list of letter search results for reading room
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<LetterReadingRoomSearchResult> GetLetterReadingRoomSearchResults(LetterReadingRoomSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Sets the publishing status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="publish"></param>
        /// <param name="unpublishReasonId"></param>
        /// <param name="unpublishReason"></param>
        void UpdatePublishingStatus(int formSetId, bool publish, int? unpublishReasonId, string unpublishReason);

        /// <summary>
        /// Sets the letter status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="letterStatusId"></param>
        void UpdateLetterStatus(int formSetId, int letterStatusId);

        /// <summary>
        /// Sets the early attention status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="earlyAttentionStatusId"></param>
        void UpdateFormSetEarlyAttentionStatus(int formSetId, int earlyAttentionStatusId);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the letter context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion
    }
}
