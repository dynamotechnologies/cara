﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    /// <summary>
    /// Interface for the report object context
    /// </summary>
    public interface IReportContext
    {
        #region Report Methods
        ReportHeaderData GetReportHeader(int phaseId);
        IList<TeamMemberReportData> GetTeamMemberReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommentWithCRInfoReportData> GetCommentWithCRInfoReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<ConcernResponseWithAssignedToReportData> GetConcernResponseWithAssignedToReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommentWithAnnotationReportData> GetCommentWithAnnotationReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommentReportData> GetCommentReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommenterOrganizationTypeData> GetCommenterOrganizationTypeReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
		IList<CodingStructureReportData> GetCodingStructureReport(int phaseId);
        IList<EarlyActionReportData> GetEarlyActionReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<MailingListReportData> GetMailingListReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<UntimelyCommentsReportData> GetUntimelyCommentsReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<ResponseStatusReportData> GetResponseStatusReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<ConcernResponseByCommenterReportData> GetConcernResponseByCommenterReport(int phaseId, int? option, int? PageNum, int? NumRows, ObjectParameter total);

		#region Demographic Subreports
		IList<DeliveryTypeReportData> GetDeliveryTypeReport(int phaseId);
		IList<OrganizationTypeReportData> GetOrganizationTypeReport(int phaseId);
		IList<OriginOfLettersReportData> GetOriginOfLettersReport(int phaseId);
		IList<LetterTypeReportData> GetLetterTypeReport(int phaseId);
		IList<OrganizedLetterCampaignReportData> GetOrganizedLetterCampaignReport(int phaseId);
		IList<RespondingOrgReportData> GetRespondingOrgReport(int phaseId);
		#endregion Demographic Subreports

		#endregion Report Methods
	}
}
