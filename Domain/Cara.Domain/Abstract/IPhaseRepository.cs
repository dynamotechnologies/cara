﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an phase repository
    /// </summary>
    public interface IPhaseRepository : IRepository<Phase>
    {
        #region Phase Interfaces
        /// <summary>
        /// Returns a list of phase search results
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IList<PhaseSearchResult> GetPhases(PhaseSearchResultFilter filter);

        /// <summary>
        /// Returns a phase summary
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        PhaseSummary GetPhaseSummary(int phaseId);

        /// <summary>
        /// Returns the top X most used phase codes
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        IList<PhaseCodesMostUsed> GetMostUsedPhaseCodes(int phaseId);
        #endregion

        #region PhaseCode Interfaces
        /// <summary>
        /// Returns a list of phase code selects
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IList<PhaseCodeSelect> GetPhaseCodeSelects(PhaseCodeSelectFilter filter);

        /// <summary>
        /// Insert the phase codes from coding template or phase
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="phaseId"></param>
        /// <param name="codeIds"></param>
        /// <param name="targetPhaseId"></param>
        void InsertPhaseCodes(int? codingTemplateId, int? phaseId, string codeIds, int targetPhaseId);
        #endregion

        #region TermList Interfaces
        /// <summary>
        /// Insert the term lists specified in the id list
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="termListIdList"></param>
        void InsertPhaseTermLists(int phaseId, string termListIdList);
        #endregion

        #region Sequence Interfaces
        /// <summary>
        /// Returns the next available sequence number
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        int? GetNextSequence(string name, int? phaseId);
        #endregion

        #region Context Property
        /// <summary>
        /// Returns the phase context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion

        #region ReviewableLetters
        IList<ReviewableLetterCounts> GetReviewableLetterCounts(int userId);
        #endregion ReviewableLetters
    }
}
