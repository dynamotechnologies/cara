﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an formset repository
    /// </summary>
    public interface IFormSetRepository : IRepository<FormSet>
    {
        #region FormSet Interfaces
        /// <summary>
        /// Returns a list of formset search results
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        IList<FormSetSearchResult> GetFormSetSearchResults(FormSetSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Returns a list of formset letters search results
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        IList<FormSetLetterSearchResult> GetFormSetLetterSearchResults(FormSetSearchResultFilter filter, ObjectParameter total);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the letter context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion
    }
}
