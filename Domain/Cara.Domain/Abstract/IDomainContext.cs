﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Xml.Linq;
using System;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Interface for the domain object context
    /// </summary>
    public interface IDomainContext
    {
        #region Project Methods
        /// <summary>
        /// Returns a list of projects
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<ProjectSearchResult> GetProjectSearchResults(ProjectSearchResultFilter filter, ObjectParameter total);
        #endregion

        #region Phase Methods
        /// <summary>
        /// Returns a list of phases
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IList<PhaseSearchResult> GetPhaseSearchResults(PhaseSearchResultFilter filter);

        /// <summary>
        /// Returns a phase summary
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        PhaseSummary GetPhaseSummary(int phaseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        IList<PhaseCodesMostUsed> GetMostUsedPhaseCodes(int phaseId);
        #endregion

        #region PhaseCode Methods
        /// <summary>
        /// Returns a list of phase codes associated with a phase
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IList<PhaseCodeSelect> GetPhaseCodeSelects(PhaseCodeSelectFilter filter);

        /// <summary>
        /// Insert the phase codes from coding template or phase
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="phaseId"></param>
        /// <param name="codeIds"></param>
        /// <param name="targetPhaseId"></param>
        void InsertPhaseCodes(int? codingTemplateId, int? phaseId, string codeIds, int targetPhaseId);
        #endregion

        #region TermList Methods
        /// <summary>
        /// Insert the term lists specified in the id list
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="termListIdList"></param>
        void InsertPhaseTermLists(int phaseId, string termListIdList);
        #endregion

        #region Comment Methods
        /// <summary>
        /// Returns a list of comments
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<CommentSearchResult> GetCommentSearchResults(CommentSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Returns a list of comments
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<CommentDetails> CommentDetails(XDocument xdoc);

        //*Added for comment code sort
        /// <summary>
        /// Update the sequence ofcomment code
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="phaseCodeId"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        void UpdateCommentCodeSequence(int commentId, int phaseCodeId, int sequence);
        #endregion

        #region Letter Methods
        /// <summary>
        /// Returns a list of letters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<LetterSearchResult> GetLetterSearchResults(LetterSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Returns a list of letters for reading room
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<LetterReadingRoomSearchResult> GetLetterReadingRoomSearchResults(LetterReadingRoomSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Sets the publishing status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="publish"></param>
        /// <param name="unpublishReasonId"></param>
        /// <param name="unpublishReason"></param>
        int UpdateFormSetPublishingStatus(int? formSetId, bool? publish, int? unpublishReasonId, string unpublishReason);

        /// <summary>
        /// Sets the letter status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="letterStatusId"></param>
        int UpdateFormSetLetterStatus(int? formSetId, int? letterStatusId);

        int UpdateFormSetEarlyAttentionStatus(int? formSetId, int? earlyAttentionStatusId);
        #endregion

        #region FormSet Methods
        /// <summary>
        /// Returns a list of form sets
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        IList<FormSetSearchResult> GetFormSetSearchResults(FormSetSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Returns a list of letters in a form set
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        IList<FormSetLetterSearchResult> GetFormSetLetterSearchResults(FormSetSearchResultFilter filter, ObjectParameter total);
        #endregion

        #region ConcernResponse Methods
        /// <summary>
        /// Returns a list of concern/responses
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<ConcernResponseSearchResult> GetConcernResponseSearchResults(ConcernResponseSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="concernResponseId"></param>
        /// <param name="sequence"></param>
        /// <param name="position"></param>
        void UpdateConcernResponseSequence(int concernResponseId, int? sequence, string position);

        /// <summary>
        /// Update the phase sequence of concern/response
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="sequence"></param>
        void UpdateConcernResponsePhaseSequence(int phaseId, int? sequence);
        #endregion

        #region CodingTemplate Methods
        /// <summary>
        /// Returns a list of template codes
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        IList<TemplateCodeSelect> GetTemplateCodeSelects(int codingTemplateId, ObjectParameter templateName);

        /// <summary>
        /// Update an existing coding template
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <param name="codeIds"></param>
        void UpdateCodingTemplate(int codingTemplateId, string templateName, string codeIds);
        #endregion

        #region Sequence Methods
        /// <summary>
        /// Returns the next available sequence number
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        int? GetNextSequence(string name, int? phaseId);
        #endregion

        #region Reviewable Letter Count
        IList<ReviewableLetterCounts> GetReviewableLetterCounts(int userId);
        #endregion
    }
}
