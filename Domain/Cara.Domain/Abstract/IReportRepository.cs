﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain.Report
{
    /// <summary>
    /// An interface to define an report repository
    /// </summary>
    public interface IReportRepository : IRepository<Report>
    {
        #region Context Interfaces
        IReportContext ReportContext { get; }
        #endregion

        #region Standard Report Interfaces

        IList<MailingListReportData> GetMailingListReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<TeamMemberReportData> GetTeamMemberReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<UntimelyCommentsReportData> GetUntimelyCommentsReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommenterOrganizationTypeData> GetCommenterOrganizationTypeReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<EarlyActionReportData> GetEarlyActionReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommentReportData> GetCommentReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<ResponseStatusReportData> GetResponseStatusReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<ConcernResponseByCommenterReportData> GetConcernResponseByCommenterReport(int phaseId, int? option, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommentWithCRInfoReportData> GetCommentWithCRInfoReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<ConcernResponseWithAssignedToReportData> GetConcernResponseWithAssignedToReportData(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);
        IList<CommentWithAnnotationReportData> GetCommentWithAnnotationReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total);

        #endregion
    }
}
