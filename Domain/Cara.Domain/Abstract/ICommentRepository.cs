﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using System.Xml.Linq;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an comment repository
    /// </summary>
    public interface ICommentRepository : IRepository<Comment>
    {
        #region Comment Interfaces
        /// <summary>
        /// Returns a list of commentId search results
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<CommentSearchResult> GetCommentSearchResults(CommentSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Returns a list of comment details for a given CommentId
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<CommentDetails> CommentDetails(XDocument xdoc);

        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the comment context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion
    }
}
