﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an coding template repository
    /// </summary>
    public interface ICodingTemplateRepository : IRepository<CodingTemplate>
    {
        #region CodingTemplate Interfaces
        /// <summary>
        /// Returns a list of template codes
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        IList<TemplateCodeSelect> GetTemplateCodeSelects(int codingTemplateId, ObjectParameter templateName);

        /// <summary>
        /// Update an existing coding template
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <param name="codeIds"></param>
        void UpdateCodingTemplate(int codingTemplateId, string templateName, string codeIds);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the domain context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion
    }
}
