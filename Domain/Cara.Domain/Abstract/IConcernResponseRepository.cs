﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an concern/response repository
    /// </summary>
    public interface IConcernResponseRepository : IRepository<ConcernResponse>
    {
        #region ConcernResponse Interfaces
        /// <summary>
        /// Returns a list of concern/response search results
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<ConcernResponseSearchResult> GetConcernResponseSearchResults(ConcernResponseSearchResultFilter filter, ObjectParameter total);

        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="concernResponseId"></param>
        /// <param name="sequence"></param>
        /// <param name="position"></param>
        void UpdateConcernResponseSequence(int concernResponseId, int? sequence, string position);

        /// <summary>
        /// Update the phase sequence of concern/response
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="sequence"></param>
        void UpdateConcernResponsePhaseSequence(int phaseId, int? sequence);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the concern/response context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion
    }
}
