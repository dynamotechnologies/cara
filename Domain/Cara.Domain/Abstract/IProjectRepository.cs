﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// An interface to define an project repository
    /// </summary>
    public interface IProjectRepository : IRepository<Project>
    {
        #region Project Interfaces
        /// <summary>
        /// Returns a list of project search results
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<ProjectSearchResult> GetProjectSearchResults(ProjectSearchResultFilter filter, ObjectParameter total);
        #endregion

        #region Context Interfaces
        /// <summary>
        /// Returns the project context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion
    }
}
