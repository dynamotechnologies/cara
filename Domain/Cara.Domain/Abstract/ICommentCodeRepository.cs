﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    public interface ICommentCodeRepository : IRepository<CommentCode>
    {

        #region Context Interfaces
        /// <summary>
        /// Returns the comment context
        /// </summary>
        IDomainContext DomainContext { get; }
        #endregion

        //*Added for comment code sort
        /// <summary>
        /// Update the sequence of comment code
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="phaseCodeId"></param>
        /// <param name="sequence"></param>
        void UpdateCommentCodeSequence(int commentId, int phaseCodeId, int sequence);
    }
}
