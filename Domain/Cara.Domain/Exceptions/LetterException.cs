﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework;

namespace Aquilent.Cara.Domain
{
	public class LetterException : DomainException
	{
		#region Properties
		public int LetterId { get; set;}
		#endregion Properties

		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public LetterException(int letterId)
			: base()
		{
			LetterId = letterId;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public LetterException(string message, int letterId)
			: base(message)
		{
			LetterId = letterId;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public LetterException(string message, Exception innerException, int letterId)
			: base(message, innerException)
		{
			LetterId = letterId;
		}
		#endregion        
	}
}
