﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework;

namespace Aquilent.Cara.Domain
{
	public class DomainException : FrameworkException
	{
		#region    Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public DomainException()
			: base()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		public DomainException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public DomainException(string message, Exception innerException) :
			base(message, innerException)
		{
		}
		#endregion        
	}
}
