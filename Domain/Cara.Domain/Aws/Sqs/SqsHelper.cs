﻿using System;
using System.Linq;
using Amazon;
using Amazon.SQS;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain.Aws.Sqs
{
    public static class SqsHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="settingKey">The name of the settings key (used by the CaraSettingManager)</param>
        /// <param name="queueName"></param>
        /// <returns></returns>
        public static IAmazonSQS GetQueue(string settingKey, out string queueName)
        {
            using (var settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                var queueNameSetting = settingsManager.Get(settingKey);
                if (queueNameSetting != null)
                {
                    queueName = queueNameSetting.Value;
                }
                else
                {
                    throw new AmazonSQSException(string.Format("The configuration setting {0} could not be found", settingKey));
                }
            }

            return AWSClientFactory.CreateAmazonSQSClient();
        }
    }
}
