﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;

using log4net;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;

using Aquilent.EntityAccess;

// Add using statements to access AWS SDK for .NET services. 
// Both the Service and its Model namespace need to be added 
// in order to gain access to a service. For example, to access
// the EC2 service, add:
// using Amazon.EC2;
// using Amazon.EC2.Model;

namespace Aquilent.Cara.Domain.Aws.DynamoDB
{
    public class DynamoDBHelper
    {
        private int batchWriteLimit; // number of items to add to each Batch write or delete request
        private int batchReadLimit; //number of results to return from each query request
        private int retryLimit; //number of times to retry a query if it fails
        private int timeForRetry; //length of time (milliseconds) to wait between request retries
        private string tableName;
        //END TODO
        private ILog _logger;
        private AmazonDynamoDBClient client = null;

        public DynamoDBHelper(ILog log = null)
        {
            #region retrieve cara settings
            tableName = GetConfigSetting("cara.dynamo.lettermetadata");
            batchWriteLimit = Convert.ToInt32(GetConfigSetting("cara.dynamo.batchwritelimit"));
            batchReadLimit = Convert.ToInt32(GetConfigSetting("cara.dynamo.batchreadlimit"));
            retryLimit = Convert.ToInt32(GetConfigSetting("cara.dynamo.retrylimit"));
            timeForRetry = Convert.ToInt32(GetConfigSetting("cara.dynamo.timeforretry"));
            #endregion

            AmazonDynamoDBConfig config = new AmazonDynamoDBConfig();

            if (log != null)
            {
                _logger = log;
            }
            else
            {
                _logger = LogManager.GetLogger(typeof(DynamoDBHelper));
            }

            client = new AmazonDynamoDBClient(config);
        }

        /// <summary>
        /// Retrieves the specified item from the Dynamo table.  Returns null if dynamo returned a bad status
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, AttributeValue>> GetItem(List<KeyValuePair<string, AttributeValue>> keys)
        {
            List<KeyValuePair<string, AttributeValue>> returnValue = null;
            if (keys == null)
            {
                return returnValue;
            }

            try
            {
                GetItemRequest request = new GetItemRequest();
                request.TableName = tableName;
                request.Key = keys.ToDictionary(x => x.Key, x => x.Value);
                request.ReturnConsumedCapacity = "TOTAL";
                GetItemResponse response = null;

                for (int i = 0; i < retryLimit; i++)
                {
                    try
                    {
                        response = client.GetItem(request);
                    }
                    catch (Exception ex)
                    {
                        //log incident
                        _logger.Error(string.Format("Recieved exception for GetItem Request attempt number {0}", i), ex);
                        System.Threading.Thread.Sleep(timeForRetry);
                        continue;
                    }

                    if (response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        _logger.Error(string.Format("Recieved status code {0} for GetItem Request attempt number {1}",
                            response.HttpStatusCode, i));
                        continue;
                    }
                    
                    List<KeyValuePair<string, AttributeValue>> item = response.Item.ToList();
                    if (item.Any())
                    {
                        returnValue = item;
                    }

                    break;
                }
            }
            catch (Exception ex)
            {
                string keyString = "";
                if (keys != null)
                {
                    keys.Select(x => x.Key).ToList().ForEach(x => keyString = String.Format("{0} {1}", keyString, x));
                }

                _logger.Error(string.Format("Exception encountered with Dynamo get item on table {0} with keys {1}", string.IsNullOrEmpty(tableName) ? "NULL" : tableName, keyString), ex);

                returnValue = null;
            }

            return returnValue;
        }

        /// <summary>
        /// Puts the specified item in the Dynamo table
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns>true if success, False if bad status code was returned</returns>
        public bool Put(List<KeyValuePair<string, AttributeValue>> attributes)
        {
            bool returnValue = false;
            if (attributes == null || !attributes.Any())
            {
                return returnValue;
            }

            try
            {
                PutItemRequest request = new PutItemRequest();
                request.TableName = tableName;
                request.Item = attributes.ToDictionary(x => x.Key, x => x.Value);
                PutItemResponse response = null;

                for (int i = 0; i < retryLimit; i++)
                {
                    try
                    {
                        response = client.PutItem(request);
                    }
                    catch (Exception ex)
                    {
                        //log incident
                        _logger.Error(string.Format("Recieved exception for PutItem Request attempt number {0}", i), ex);
                        System.Threading.Thread.Sleep(timeForRetry);
                        continue;
                    }

                    if (response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        _logger.Error(string.Format("Recieved status code {0} for PutItem Request attempt number {1}",
                            response.HttpStatusCode, i));
                        continue;
                    }

                    returnValue = true;
                    break;
                }
            }
            catch (Exception ex)
            {
                string keyString = "";
                if (attributes != null)
                {
                    attributes.Select(x => x.Key).ToList().ForEach(x => keyString = String.Format("{0} {1}", keyString, x));
                }

                _logger.Error(string.Format("Exception encountered with Dynamo put item on table {0} with attributes{1}", 
                    string.IsNullOrEmpty(tableName) ? "NULL" : tableName, keyString), ex);

                returnValue = false;
            }

            return returnValue;
        }

        /// <summary>
        /// Adds the list of items to the Dynamo Table
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of items that were properly added</returns>
        public List<List<KeyValuePair<string, AttributeValue>>> Put(List<List<KeyValuePair<string,AttributeValue>>> items)
        {
            List<List<KeyValuePair<string, AttributeValue>>> returnValue = new List<List<KeyValuePair<string, AttributeValue>>>();
            try
            {
                while (items.Count() > 0)
                {
                    List<List<KeyValuePair<string, AttributeValue>>> itemBatch = items.Take(batchWriteLimit).ToList();
                    BatchWriteItemRequest request = new BatchWriteItemRequest();

                    request.RequestItems = new Dictionary<string, List<WriteRequest>>
                    {
                        {tableName, 
                        itemBatch.Select(item => new WriteRequest()
                            {
                                PutRequest = new PutRequest()
                                {
                                    Item = item.ToDictionary(x=> x.Key, x=>x.Value)
                                }
                            }).ToList()}
                    };

                    bool successfulWrite = false;
                    List<List<KeyValuePair<string, AttributeValue>>> unprocessedItems = itemBatch;
                    for (int i = 0; i < retryLimit; i++)
                    {
                        BatchWriteItemResponse response = null;
                        try
                        {
                            response = client.BatchWriteItem(request);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(string.Format("Recieved exception for PutItem(list) request attempt number {0}", i),ex);
                            System.Threading.Thread.Sleep(timeForRetry);
                            continue;
                        }

                        if (response.HttpStatusCode != HttpStatusCode.OK)
                        {
                            //log incident
                            _logger.Error(string.Format("Invalid status code '{0}' for PutItem(list) Request attempt number {1}",
                                    response.HttpStatusCode, i));
                            System.Threading.Thread.Sleep(timeForRetry);
                            continue;
                        }

                        if (response.UnprocessedItems.Any())
                        {
                            //log incident
                            unprocessedItems = response.UnprocessedItems
                                .FirstOrDefault(x => x.Key == tableName)
                                .Value.Select(writeReq => writeReq.PutRequest.Item.ToList())
                                .ToList();
                            _logger.Error(string.Format("{0} items not processed for PutItem(list) Request attempt number {1}",
                                    unprocessedItems.Count(), i));
                            request.RequestItems = response.UnprocessedItems;
                            System.Threading.Thread.Sleep(timeForRetry);
                            continue;
                        }

                        successfulWrite = true;
                        break;
                    }

                    //if not successful try adding the items individually
                    if (!successfulWrite)
                    {
                        unprocessedItems.RemoveAll(item => Put(item));
                        returnValue.AddRange(itemBatch.Where(item => !unprocessedItems.Contains(item)));
                    }
                    else
                    {
                        returnValue.AddRange(itemBatch);
                    }

                    items = items.Skip(batchWriteLimit).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Exception encountered with Dynamo Batch Write (put) on table {0}", 
                    string.IsNullOrEmpty(tableName) ? "NULL" : tableName), ex);

            }

            return returnValue;
        }

        /// <summary>
        /// Removes the specified items from the Dynamo table
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of items removed from the dynamo table</returns>
        public List<List<KeyValuePair<string, AttributeValue>>> Remove(List<List<KeyValuePair<string, AttributeValue>>> items)
        {
            List<List<KeyValuePair<string, AttributeValue>>> returnValue = new List<List<KeyValuePair<string, AttributeValue>>>();
            try
            {
                while (items.Count() > 0)
                {
                    List<List<KeyValuePair<string, AttributeValue>>> itemBatch = items.Take(batchWriteLimit).ToList();
                    List<List<KeyValuePair<string, AttributeValue>>> unprocessedItems = itemBatch;
                    BatchWriteItemRequest request = new BatchWriteItemRequest();

                    request.RequestItems = new Dictionary<string, List<WriteRequest>>()
                    {
                        {
                            tableName, 
                            itemBatch.Select(item => new WriteRequest()
                            {
                                DeleteRequest = new DeleteRequest()
                                {
                                    Key = item.ToDictionary(x=> x.Key, x=>x.Value)
                                }
                            }).ToList()
                        }
                    };

                    bool successfulWrite = false;
                   
                    for (int i = 0; i < retryLimit; i++)
                    {
                        BatchWriteItemResponse response = null;
                        try
                        {
                            response = client.BatchWriteItem(request);
                        }
                        catch (Exception ex)
                        {
                            //log incident
                            _logger.Error(string.Format("Recieved exception for Remove(list) Request attempt number {0}", i), ex);
                            System.Threading.Thread.Sleep(timeForRetry);
                            continue;
                        }

                        if (response.HttpStatusCode != HttpStatusCode.OK)
                        {
                            //log incident
                            _logger.Error(string.Format("Invalid status code '{0}' for Remove(list) Request attempt number {1}",
                                    response.HttpStatusCode, i));
                            System.Threading.Thread.Sleep(timeForRetry);
                            continue;
                        }

                        if (response.UnprocessedItems.Any())
                        {
                            //log incident
                            unprocessedItems = response.UnprocessedItems
                                .FirstOrDefault(x => x.Key == tableName)
                                .Value.Select(writeReq => writeReq.DeleteRequest.Key.ToList())
                                .ToList();
                            _logger.Error(string.Format("{0} items not processed for Remove(list) Request attempt number {1}",
                                    unprocessedItems.Count(), i));
                            request.RequestItems = response.UnprocessedItems;
                            System.Threading.Thread.Sleep(timeForRetry);
                            continue;
                        }

                        successfulWrite = true;
                        break;
                    }

                    //if not successful try adding the items individually
                    if (!successfulWrite)
                    {
                        unprocessedItems.RemoveAll(item => Remove(item));
                        returnValue.AddRange(itemBatch.Where(item => !unprocessedItems.Contains(item)));
                    }
                    else
                    {
                        returnValue.AddRange(itemBatch);
                    }

                    items = items.Skip(batchWriteLimit).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format("Exception encountered with Dynamo Batch Remove(list) on table {0}",
                    string.IsNullOrEmpty(tableName) ? "NULL" : tableName), ex);
            }

            return returnValue;
        }

        /// <summary>
        /// Querys the DynamoDB for a list of items
        /// </summary>
        /// <param name="indexName"></param>
        /// <param name="keys"></param>
        /// <param name="parameters"></param>
        /// <returns>The list of items or null if a bad status code was returned</returns>
        public IQueryable<List<KeyValuePair<string, AttributeValue>>> Query(
            string indexName,
            List<KeyValuePair<string,
            AttributeValue>> keys,
            List<KeyValuePair<string, AttributeValue>> parameters)
        {
            IQueryable<List<KeyValuePair<string, AttributeValue>>> returnValue = null;

            try
            {
                QueryRequest request = new QueryRequest();
                request.IndexName = indexName;
                request.TableName = tableName;
                request.Limit = batchReadLimit;

                #region populate parameters
                if (parameters != null)
                {
                    request.QueryFilter = new Dictionary<string, Condition>();
                    foreach (KeyValuePair<string, AttributeValue> parameter in parameters)
                    {
                        string key = parameter.Key;
                        if (request.QueryFilter.Any(x => x.Key == key))
                        {
                            Condition condition = request.QueryFilter
                                .FirstOrDefault(x => x.Key == key).Value;
                            condition.AttributeValueList
                                .Add(parameter.Value);
                            condition.ComparisonOperator = ComparisonOperator.IN;
                        }
                        else
                        {
                            request.QueryFilter.Add(key, new Condition()
                            {
                                AttributeValueList = new List<AttributeValue>() { parameter.Value },
                                ComparisonOperator = ComparisonOperator.EQ
                            });
                        }
                    }
                }
                #endregion

                #region populate keys
                request.KeyConditions = new Dictionary<string, Condition>();
                foreach (KeyValuePair<string, AttributeValue> keyCondition in keys)
                {
                    string key = keyCondition.Key;
                    if (request.KeyConditions.Any(x => x.Key == key))
                    {
                        Condition condition = request.KeyConditions
                            .FirstOrDefault(x => x.Key == key).Value;
                        condition.AttributeValueList
                            .Add(keyCondition.Value);
                        condition.ComparisonOperator = ComparisonOperator.IN;
                    }
                    else
                    {
                        request.KeyConditions.Add(key, new Condition()
                        {
                            AttributeValueList = new List<AttributeValue>() { keyCondition.Value },
                            ComparisonOperator = ComparisonOperator.EQ
                        });
                    }
                }
                #endregion

                #region execute request
                
                QueryResponse response = null;
                Dictionary<string, AttributeValue> lastEvaluatedKey = null;
                do
                {
                    for (int i = 0; i < retryLimit; i++)
                    {
                        try
                        {
                            if (lastEvaluatedKey != null)
                            {
                                request.ExclusiveStartKey = lastEvaluatedKey;
                            }

                            response = client.Query(request);
                        }
                        catch (Exception ex)
                        {
                            //log incident
                            _logger.Error(string.Format("Recieved exception for Query Request attempt number {0}", i), ex);
                            System.Threading.Thread.Sleep(timeForRetry);
                            lastEvaluatedKey = null;
                            continue;
                        }

                        if (response.HttpStatusCode != HttpStatusCode.OK)
                        {
                            //log incident
                            _logger.Error(string.Format("Invalid status code '{0}' for Query Request attempt number {1}",
                                    response.HttpStatusCode, i));
                            System.Threading.Thread.Sleep(timeForRetry);
                            lastEvaluatedKey = null;
                            continue;
                        }
                        
                        if (returnValue == null)
                        {
                            returnValue = new List<List<KeyValuePair<string, AttributeValue>>>().AsQueryable();
                        }

                        returnValue = returnValue.Concat(response.Items.Select(item => item.ToList()).AsQueryable());
                        lastEvaluatedKey = response.LastEvaluatedKey;
                        break;
                    }
                } while (lastEvaluatedKey != null && lastEvaluatedKey.Any());
                #endregion
            }
            catch (Exception ex)
            {
                string keystring = "";
                if (keys != null)
                {
                    keys.ForEach(x => keystring = string.Format("{0} {1}", keystring, x.Value));
                }

                string parameterstring = "";
                if (parameters != null)
                {
                    parameters.ForEach(x => parameterstring = string.Format("{0} {1}", parameterstring, x.Value));
                }
                _logger.Error(string.Format("Exception encountered with Dynamo query on table {0}, index {1}, keys{2}, paramerters{3}",
                    string.IsNullOrEmpty(tableName) ? "NULL" : tableName,
                    string.IsNullOrEmpty(indexName) ? "NULL" : indexName, 
                    keystring, 
                    parameterstring), ex);

                returnValue = null;
            }

            return returnValue;
        }

        public bool Update(List<KeyValuePair<string, AttributeValue>> keys, List<KeyValuePair<string, AttributeValue>> newValues)
        {
            bool returnValue = false;
            try
            {
                UpdateItemRequest request = new UpdateItemRequest();
                request.TableName = tableName;
                request.AttributeUpdates = newValues
                    .ToDictionary(x => x.Key, x => new AttributeValueUpdate() { Value = x.Value, Action = AttributeAction.PUT });
                request.Key = keys.ToDictionary(x => x.Key, x => x.Value);
                for (int i = 0; i < retryLimit; i++)
                {
                    UpdateItemResponse response = null;
                    try
                    {
                        response = client.UpdateItem(request);
                    }
                    catch (Exception ex)
                    {
                        //log incident
                        _logger.Error(string.Format("Recieved exception for Update Request attempt number {0}", i), ex);
                        System.Threading.Thread.Sleep(timeForRetry);
                        continue;
                    }
                    if (response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        //log incident
                        _logger.Error(string.Format("Invalid status code '{0}' for Update Request attempt number {1}",
                                response.HttpStatusCode, i));
                        System.Threading.Thread.Sleep(timeForRetry);
                        continue;
                    }
                    returnValue = true;
                    break;
                }
            }
            catch (Exception ex)
            {
                string keyString = "";
                if (keys != null)
                {
                    keys.Select(x => x.Key).ToList().ForEach(x => keyString = String.Format("{0} {1}", keyString, x));
                }
                _logger.Error(string.Format("Exception encountered with Dynamo update on table {0} with keys{1}",
                    string.IsNullOrEmpty(tableName) ? "NULL" : tableName, keyString), ex);
                returnValue = false;
            }

            return returnValue;
        }

        public bool Remove(List<KeyValuePair<string, AttributeValue>> keys)
        {
            bool returnValue = false;
            try
            {
                DeleteItemRequest request = new DeleteItemRequest();
                request.TableName = tableName;
                request.Key = new Dictionary<string, AttributeValue>();
                keys.ForEach(key => request.Key.Add(key.Key, key.Value));

                for (int i = 0; i < retryLimit; i++)
                {
                    DeleteItemResponse response = null;
                    try
                    {
                        response = client.DeleteItem(request);
                    }
                    catch (Exception ex)
                    {
                        //log incident
                        _logger.Error(string.Format("Recieved Exception for Delete Item Request attempt number {0}", i), ex);
                        System.Threading.Thread.Sleep(timeForRetry);
                        continue;
                    }

                    if (response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        //log incident
                        _logger.Error(string.Format("Invalid status code '{0}' for Delete Item Request attempt number {1}",
                                response.HttpStatusCode, i));
                        System.Threading.Thread.Sleep(timeForRetry);
                        continue;
                    }
                    returnValue = true;
                    break;
                }
            }
            catch (Exception ex)
            {
                string keyString = "";
                if (keys != null)
                {
                    keys.Select(x => x.Key).ToList().ForEach(x => keyString = String.Format("{0}{1} ", keyString, x));
                }

                _logger.Error(string.Format("Exception encountered with Dynamo remove on table {0} with keys{1}", 
                    string.IsNullOrEmpty(tableName) ? "NULL" : tableName, keyString), ex);

                returnValue = false;
            }

            return returnValue;
        }

        public static AttributeValue GetAttributeValue(string attributeValue)
        {
            return new AttributeValue() { S = attributeValue == null ? "" : attributeValue };
        }

        public static AttributeValue GetAttributeValue(int attributeValue)
        {
            return new AttributeValue() { N = attributeValue.ToString() };
        }

        public static AttributeValue GetAttributeValue(List<string> attributeValue)
        {
            return new AttributeValue() { SS = attributeValue == null ? new List<string>(): attributeValue };
        }

        public static int GetIntAttribute(List<KeyValuePair<string, AttributeValue>> item, string attributeName)
        {
            return item.Any(x => x.Key == attributeName) ? Convert.ToInt32(item.FirstOrDefault(x => x.Key == attributeName).Value.N) : 0;
        }

        public static List<string> GetStringSetAttribute(List<KeyValuePair<string, AttributeValue>> item, string attributeName)
        {
            return item.Any(x => x.Key == attributeName) ? item.FirstOrDefault(x => x.Key == attributeName).Value.SS : new List<string>();
        }

        public static string GetStringAttribute(List<KeyValuePair<string, AttributeValue>> item, string attributeName)
        {
            return item.Any(x => x.Key == attributeName) ? item.FirstOrDefault(x => x.Key == attributeName).Value.S : "";
        }

        private string GetConfigSetting(string key)
        {
            string returnValue = "";
            using (CaraSettingManager settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                CaraSetting setting = settingsManager.Get(key);
                if (setting != null)
                {
                    returnValue = setting.Value;
                }
                else
                {
                    throw new AmazonDynamoDBException(string.Format("The configuration setting {0} could not be found", key));
                }
            }
            return returnValue;
        }
    }
}