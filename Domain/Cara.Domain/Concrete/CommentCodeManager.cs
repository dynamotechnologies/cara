﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;


using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    public class CommentCodeManager : AbstractEntityManager<CommentCode>
    {
        public CommentCodeManager(ICommentCodeRepository repository)
			: base(repository)
		{
        }

        #region Override AbstractEntityService members
        /// <summary>
        /// Returns a phasecodeId with the given comment ID
        /// </summary>
        /// <param name="name">The ID of the comment</param>
        /// <returns>Comment or null</returns>
        public override CommentCode Get(int uniqueId)
        {
            return All.Where(x => x.CommentId == uniqueId).FirstOrDefault();
        }

        /// <summary>
        /// Returns a comment with the uniqueName
        /// </summary>
        /// <param name="uniqueName"></param>
        /// <returns></returns>
        public override CommentCode Get(string uniqueName)
        {
            throw new NotImplementedException();
        }
        #endregion Override AbstractEntityService members
        #region Custom Methods
        /// <summary>
        /// Returns PhaseCodeId for the given commentId
        /// </summary>
        /// <param name="name">The ID of the code</param>
        /// <returns>CodeCategoryId or 0</returns>
        public int GetPhaseCodeId(int commentId)
        {
            int phaseCodeId = 0;
            CommentCode commentCode = Get(commentId);

            if (commentCode != null)
            {
                phaseCodeId = commentCode.PhaseCodeId;
            }
            return phaseCodeId;
        }
        #endregion

        //*Added for comment code sort
        /// <summary>
        /// Update the sequence of comment code
        /// </summary>
        /// <param name="commentId">commentId</param>
        /// <param name="phaseCodeId">phaseCodeId</param>
        /// <param name="sequence">sequence</param>
        public void UpdateCommentCodeSequence(int commentId, int phaseCodeId, int sequence)
        {
            (Repository as ICommentCodeRepository).UpdateCommentCodeSequence(commentId, phaseCodeId, sequence);
        }
    }
}
