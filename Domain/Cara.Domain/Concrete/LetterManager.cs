﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using log4net;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Domain.Aws.Sqs;
using Aquilent.Cara.Domain.Aws.DynamoDB;
//using System.ComponentModel;

namespace Aquilent.Cara.Domain
{
	public class LetterManager : AbstractEntityManager<Letter>
	{
        #region Private Members
		private IRepository<LetterDocument> _repositoryDocument;
		private IFormLetterDetector _formLetterDetector;
		private ILog _logger = LogManager.GetLogger(typeof(LetterManager));
        private bool hasAddedLetters = false;
        private bool hasDeletedLetters = false;
        private DynamoDBHelper dynamo
        {
            get
            {
                if (_dynamoHelper == null)
                {
                    _dynamoHelper = new DynamoDBHelper(_logger);
                }
                return _dynamoHelper;
            }
        }
        private DynamoDBHelper _dynamoHelper;
        private int _wordSampleSize = Convert.ToInt32(ConfigurationManager.AppSettings["formDetectionWordSampleSize"]);

		private static LookupUnpublishedReason _lookupUnpublishedReason = LookupManager.GetLookup<LookupUnpublishedReason>();
        private static LookupLetterType _lookupLetterType = LookupManager.GetLookup<LookupLetterType>();
        private static LookupTermRef _lookupTermRef = LookupManager.GetLookup<LookupTermRef>();
        private static LookupLetterStatus _lookupLetterStatus = LookupManager.GetLookup<LookupLetterStatus>();
        private static readonly int _notRequiredEarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Not Required")).First().StatusId;
        private static readonly int _needsReviewEarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().Where(x => x.Name.Equals("Auto-Marked")).First().StatusId;
        private static readonly int _pendingLetterTypeId = _lookupLetterType.Where(x => x.Name == "Pending").First().LetterTypeId;
        private static readonly LetterType _duplicateLetterType = _lookupLetterType.Where(x => x.Name == "Duplicate").First();
        private static readonly LetterType _masterFormLetterType = _lookupLetterType.Where(x => x.Name == "Master Form").First();
        private static readonly LetterType _uniqueLetterType = _lookupLetterType.Where(x => x.Name == "Unique").First();
        private static readonly LetterType _formLetterType = _lookupLetterType.Where(x => x.Name == "Form").First();
        private static readonly LetterType _formPlusLetterType = _lookupLetterType.Where(x => x.Name == "Form Plus").First();
        private static readonly int _notNeededLetterStatusId = _lookupLetterStatus.Where(x => x.Name.Equals("Not Needed")).First().LetterStatusId;
        private static readonly int _newLetterStatusId = _lookupLetterStatus.Where(x => x.Name.Equals("New")).First().LetterStatusId;
        private static readonly int _inProgressLetterStatusId = _lookupLetterStatus.Where(x => x.Name.Equals("In Progress")).First().LetterStatusId;

        private static readonly int _lettersPerBatch = Convert.ToInt32(ConfigurationManager.AppSettings["formDetectionLetterBatchSize"]);
        private static readonly TextReplacementConfiguration txtReplacementConfig = ConfigUtilities.GetSection<TextReplacementConfiguration>("textReplacementConfiguration");
        private static readonly string _formDetectionQueueNameKey = "cara.queue.formdetection";
        private static readonly string _dmdSendQueueNameKey = "cara.queue.dmdsend";
        private static readonly string _dmdDeleteQueueNameKey = "cara.queue.dmddelete";
        private static MemoryCache _cache = MemoryCache.Default;
        #endregion

		public LetterManager(ILetterRepository repository, IRepository<LetterDocument> repositoryDocument, IFormLetterDetector formLetterDetector)
			: base(repository)
		{
            // assign the Repository.UnitOfWork to the other IRepository, so that they are sharing the same instance of the objectcontext
            this._repositoryDocument = repositoryDocument;
            this._repositoryDocument.UnitOfWork.Dispose();
            this._repositoryDocument.UnitOfWork = this.UnitOfWork;

			this._formLetterDetector = formLetterDetector;

            //this.UnitOfWork.PropertyChanged += new PropertyChangedEventHandler(ResetUnitOfWork); 
		}

        //internal void ResetUnitOfWork(object sender, PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName = "
        //    this.hasAddedLetters = false;
        //}

		#region Override AbstractEntityService members
		public override IQueryable<Letter> All
		{
			get
			{
				return Repository.ObjectQuery.Include("FormSet");
			}
		}

		/// <summary>
        /// Returns a letter with the given letter ID
		/// </summary>
        /// <param name="name">The ID of the letter</param>
		/// <returns>Letter or null</returns>
		public override Letter Get(int uniqueId)
		{
			return All.Where(x => x.LetterId == uniqueId).FirstOrDefault();
		}

		public override Letter Get(string uniqueName)
		{
			throw new NotImplementedException();
		}

        public override void Add(Letter entity)
        {
			try
			{
              
				// Replace extended ASCII with printable characters
				entity.Text = ReplaceInvalidCharacters(entity.Text);
                entity.HtmlText = ReplaceInvalidCharacters(entity.HtmlText);

				// Set letter to Pending:  Form Detection occurs in a service
                entity.LetterTypeId = _pendingLetterTypeId;

                //CARA-734:- 'Early Attention Status' not being set on all letters uploaded from the PST file using 'Upload Email Letters' feature in CARA.
                entity.EarlyActionStatusId = _notRequiredEarlyActionStatusId;

				// Mark up the coded text with auto-markup tags
                // CARA_5.7 - added if statements to prevent non-internal web form entries from losing their PublishToReadingRoom status
                // public web form doesn't assign html text for some reason, it loses sensitive content information, so it always sets PublishToReadingRoom = 1
                // even if PublishToReadingRoom == 0 prior
                if (entity.Text != null)
                {
                    entity.CodedText = TagAutoMarkupTerms(entity, entity.Text);
                }
                if (entity.HtmlText != null)
                {
                    entity.HtmlCodedText = TagAutoMarkupTerms(entity, entity.HtmlText);
                }
				// Ensure that Duplicate letters are not published
                if (entity.LetterTypeId == _duplicateLetterType.LetterTypeId)
				{
					entity.PublishToReadingRoom = false;
				}

				base.Add(entity);

                // Add the event listeners, which apply to the entire
                // UnitOfWork and not just one particular entity
                if (!hasAddedLetters)
                {
                    UnitOfWork.Saved += new UnitOfWorkEventHandler(QueueLettersForFormDetection);
                    UnitOfWork.Saved += new UnitOfWorkEventHandler(QueueLettersForDmd);                     
                    hasAddedLetters = true;
                }
			}
			catch (Exception e)
			{
				_logger.Debug("An error occurred adding a letter", e);
				throw new DomainException("An error occurred adding a letter", e);
			}
        }
  
        /// <summary>
        /// UnitOfWorkEventHandler to add letters to the form detection queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void QueueLettersForFormDetection(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Added"))
            {
                IList<object> entries = (IList<object>) e["Added"];
                foreach (var entry in entries)
                {
                    Letter letter;
                    if (entry is Letter)
                    {
                        letter = (Letter)entry;

                        // Queue the letter
                        string queueName;
                        var sqs = SqsHelper.GetQueue(_formDetectionQueueNameKey, out queueName);

                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                            {
                                QueueUrl = queueUrl,
                                MessageBody = letter.LetterId.ToString()
                            };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
        }

        /// <summary>
        /// UnitOfWorkEventHandler to repost (delete/send) a letter to the DMD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void QueueLettersForDmdRepost(object sender, UnitOfWorkEventArgs e)
        {
            //CR#379 CARA would require a code change to resend the attachments to DMD
            //QueueForDmdSend(e, "Modified");
            var state = "Modified";
            QueueForDmdSend(e, state);

            IList<object> entries = (IList<object>)e[state];
            foreach (var entry in entries)
            {
                Letter letter;
                if (entry is Letter)
                {
                    string queueName;
                    var sqs = SqsHelper.GetQueue(_dmdSendQueueNameKey, out queueName);

                    letter = (Letter)entry;
                    foreach (var document in letter.LetterDocuments)
                    {
                        // Queue the letter
                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                        {
                            QueueUrl = queueUrl,
                            MessageBody = string.Format("DocumentId,{0}", document.DocumentId)
                        };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
        }

        /// <summary>
        /// UnitOfWorkEventHandler to add letters to the DMD send queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void QueueLettersForDmd(object sender, UnitOfWorkEventArgs e)
        {
            QueueForDmdSend(e, "Added");
        }
  
        private void QueueForDmdSend(UnitOfWorkEventArgs e, string state)
        {
            if (e.ContainsKey(state))
            {
                string queueName;
                var sqs = SqsHelper.GetQueue(_dmdSendQueueNameKey, out queueName);

                IList<object> entries = (IList<object>)e[state];
                foreach (var entry in entries)
                {
                    Letter letter;
                    if (entry is Letter)
                    {
                        letter = (Letter)entry;

                        // Queue the letter
                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                        {
                            QueueUrl = queueUrl,
                            MessageBody = string.Format("LetterId,{0}", letter.LetterId)
                        };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
        }

        /// <summary>
        /// UnitOfWorkEventHandler to add letters to the DMD send queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void QueueLettersForDmdDelete(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Deleted"))
            {
                IList<object> entries = (IList<object>) e["Deleted"];
                foreach (var entry in entries)
                {
                    Letter letter;
                    if (entry is Letter)
                    {
                        letter = (Letter)entry;

                        if (!string.IsNullOrEmpty(letter.DmdId))
                        {
                            // Queue the letter
                            string queueName;
                            var sqs = SqsHelper.GetQueue(_dmdDeleteQueueNameKey, out queueName);

                            var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                            var createQueueResponse = sqs.CreateQueue(sqsRequest);
                            string queueUrl = createQueueResponse.QueueUrl;

                            var sendMessageRequest = new SendMessageRequest
                                {
                                    QueueUrl = queueUrl,
                                    MessageBody = letter.DmdId
                                };
                            sqs.SendMessage(sendMessageRequest);
                        }
                    }
                }
            }
        }

        public override void Delete(Letter entity)
		{
            // Delete the documents
            using (var documentManager = ManagerFactory.CreateInstance<DocumentManager>())
            {
                var documentIds = entity.LetterDocuments.Select(ld => ld.DocumentId).ToList();

                //Do this to avoid ConcurrencyOperationException 
                entity.LetterDocuments.Clear();
                UnitOfWork.Save();

                var documents = documentManager.Get(documentIds);
                foreach (var doc in documents)
                {
                    documentManager.Delete(doc);
                }

                // Have to save this here, since the docManager 
                // doesn't share the same unitofwork as the lettermanager
                documentManager.UnitOfWork.Saved += new UnitOfWorkEventHandler(DmdQueueHandler.QueueDocumentsForDmdDelete);
                documentManager.UnitOfWork.Save();
            }

			// If the letter is a master form letter, reassign the master
            if (entity.FormSet != null && entity.LetterId == entity.FormSet.MasterFormId)
            {
                // Don't need to wrap in a using because it is sharing the same UnitOfWork
                var formSetManager = ManagerFactory.CreateInstance<FormSetManager>();
                formSetManager.UnitOfWork = UnitOfWork;
                if (entity.FormSet.FormLetters.Count > 1)
                {
                    var newMaster = entity.FormSet.FormLetters.Where(ltr => ltr.LetterId != entity.LetterId).FirstOrDefault();
                    if (newMaster != null)
                    {
                        // Don't need to wrap in a using because it is sharing the same UnitOfWork
                        var fmChangeLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>();
                        fmChangeLogManager.UnitOfWork = UnitOfWork;
                        fmChangeLogManager.LogChangeLetterType(newMaster, newMaster.LetterTypeId, "CARA System");

                        entity.FormSet.MasterFormId = newMaster.LetterId;
                        newMaster.LetterTypeId = entity.LetterTypeId;
                    }
                }
                else
                {
                    formSetManager.Delete(entity.FormSet);

                    // Unfortunately, have to call save here because of the following error:
                    //		Unable to determine a valid ordering for dependent operations. Dependencies may exist due to foreign
                    //		key constraints, model requirements, or store-generated values. 
                    UnitOfWork.Save();
                }
            }
			
            //delete data from dynamo tables
            DeleteFromDynamo(entity);


			base.Delete(entity);

            if (!hasDeletedLetters)
            {
                hasDeletedLetters = true;
                UnitOfWork.Saved += new UnitOfWorkEventHandler(QueueLettersForDmdDelete);
            }
		}
		#endregion Override AbstractEntityService members

        #region LetterAttachment Methods
        
        public void DeleteAttachment(Letter letter, int docId, DocumentManager documentManager)
        {
			// Remove the attachment for the letter from CARA
			var ltrdoc = letter.LetterDocuments.FirstOrDefault(ld => ld.DocumentId == docId);
			if (ltrdoc != null)
			{
				letter.LetterDocuments.Remove(ltrdoc);
			}

            // Delete the documents
           var document = documentManager.Get(docId);
           if (document != null)
           {
               documentManager.Delete(document);
            }
        }
        #endregion

        #region LetterDocument Methods

        /// <summary>
        /// CARA-714:- Allow users to add/delete letter attachments.
        /// Returns TRUE if the letterdocument is protected
        /// </summary>
        /// <param name="pmr">letterdocument</param>

        public bool CheckProtected(LetterDocument letterdoc)
        {
            bool chkprotected=false;
            
            if (letterdoc !=null){
                chkprotected = letterdoc.Protected.Value;
            }
            return chkprotected;
        }
        /// <summary>
        /// Add a new letter document
        /// </summary>
        /// <param name="pmr"></param>
        public void AddLetterDocument(LetterDocument ld)
        {
            _repositoryDocument.Add(ld);
        }
        #endregion

        #region Custom Methods
        /// <summary>
        /// Returns the letters based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <returns>A list of matching letter search results</returns>
        public IList<LetterSearchResult> GetLetters(LetterSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<LetterSearchResult> list = (Repository as ILetterRepository).GetLetterSearchResults(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        /// <summary>
        /// This method reverts the state of a letter back to
        /// when it was originally retrieved
        /// </summary>
        /// <param name="letterId"></param>
        public void RevertLetter(int letterId)
        {
            var letterToRevert = Repository.ObjectQuery.Include("Comments").FirstOrDefault(x => x.LetterId == letterId);

            if (letterToRevert != null)
            {
                if (letterToRevert.Comments.Count > 0)
                {
                    var commentManager = ManagerFactory.CreateInstance<CommentManager>();
                    commentManager.UnitOfWork = UnitOfWork;
                    var comments = letterToRevert.Comments.ToList();

                    foreach (var comment in comments)
                    {
                        commentManager.Delete(comment);
                    }
                }

                // Reset the text
                letterToRevert.Text = letterToRevert.OriginalText;
                letterToRevert.CodedText = TagAutoMarkupTerms(letterToRevert, letterToRevert.Text);
                if (letterToRevert.IsLetterHtml)
                {
                    letterToRevert.HtmlText = letterToRevert.HtmlOriginalText;
                    letterToRevert.HtmlCodedText = TagAutoMarkupTerms(letterToRevert, letterToRevert.HtmlText);
                }
            }
        }

        /// <summary>
        /// Returns the letters for the reading room based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <returns>A list of matching letter search results</returns>
        public IList<LetterReadingRoomSearchResult> GetLettersReadingRoom(LetterReadingRoomSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<LetterReadingRoomSearchResult> list = (Repository as ILetterRepository).GetLetterReadingRoomSearchResults(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

		private static string ParseAttachments(Letter entity)
		{
			var fullLetterText = new StringBuilder(entity.Text);
			string letterText;
			string extractedText;
			foreach (var doc in entity.Documents)
			{
				if (doc.HasFile)
				{
					extractedText = doc.ExtractText();
					if (!string.IsNullOrWhiteSpace(extractedText))
					{
						fullLetterText.Append("\n\n*** START - INSERT TEXT FROM ATTACHMENT:  ");
						fullLetterText.Append(doc.Name);
						fullLetterText.Append(" ***\n");
						fullLetterText.Append(extractedText);
						fullLetterText.Append("\n*** END - INSERT TEXT FROM ATTACHMENT:  ");
						fullLetterText.Append(doc.Name);
						fullLetterText.Append(" ***\n\n");
					}
				}
			}

			letterText = fullLetterText.ToString();
			return letterText;
		}

		/// <summary>
		/// Searches the letter for terms found in the term lists associated with the letter's phase
		/// 
		/// If a term is found in a list that is marked for Early Action, the letter's
		/// EarlyActionRequired flag will be set.
		/// </summary>
		/// <param name="letter"></param>
		/// <returns>The tagged text</returns>
        public string TagAutoMarkupTerms(Letter letter, string textToTag)
        {
			letter.PublishToReadingRoom = true; // Default to publish the letter to the PRR

            // trick: to catch the start and end string terms by word boundary
            // reg exp, add a space before and after the text to tag
            var taggedText = string.Format(" {0} ", textToTag);

            // get the phase if it doesn't exist
            Phase phase = null;
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                phase = GetPhase(phaseManager, letter.PhaseId);

                // proceed if the input text is not empty
                if (phase != null && !string.IsNullOrWhiteSpace(textToTag))
                {

                    //var markup = "[auto-markup:{0}]{1}[auto-markup end]";  // TODO:  Move to the SystemConfig table
                    string markupStartLiteral = "[auto-markup:";
                    var markupStart = markupStartLiteral + "{0}]";
                    var markupEnd = "[auto-markup end]";

                    // get the phase term lists
                    var termLists = GetTermLists(phase);

                    // combine into a master term list sorted by the term name length
                    var masterTermList = new List<TermRef>();
                    foreach (var termList in termLists)
                    {
                        //CARA-966
                        masterTermList.AddRange(_lookupTermRef.Where(x => x.TermListId == termList.TermListId && x.Active));
                    }

                    foreach (var term in masterTermList.OrderByDescending(x => x.Name.Length))
                    {
                        var regex = new Regex(@"\b" + term.Name + @"\b", RegexOptions.IgnoreCase);

                        var matches = regex.Matches(taggedText);

                        // Used in conjunction with matchIndex to normalize the starting 
                        // index of the match after the auto-markup start and end tags are added
                        var lengthAdjustment = 0;

                        // The index where the match starts in the string (match.Index + lengthAdjustment)
                        var matchIndex = 0;

                        bool handledEarlyActionAlready = false;
                        var handledPublishToReadingRoom = false;
                        int startIndex = -1;
                        int endIndex = -1;
                        foreach (object m in matches)
                        {
                            var match = (Match)m;
                            matchIndex = match.Index + lengthAdjustment;

                            //CARA-1217 find the last occurrences of "[auto-markup:" and "[auto-markup end]" before the match
                            //Keep the last indexes of the start and end tags from previous run
                            //so it doesn't have to search the whole string again, but reset them if the match occures before them
                            if (matchIndex <= startIndex || matchIndex <= endIndex)
                            {
                                startIndex = -1;
                                endIndex = -1;
                            }
                            string textBeforeMatch = taggedText.Substring(0, matchIndex);
                            while (textBeforeMatch.IndexOf(markupStartLiteral, startIndex + 1) > -1)
                            {
                                startIndex = textBeforeMatch.IndexOf(markupStartLiteral, startIndex + 1);
                            }
                            while (textBeforeMatch.IndexOf(markupEnd, endIndex + 1) > -1)
                            {
                                endIndex = textBeforeMatch.IndexOf(markupEnd, endIndex + 1);
                            }
                            //CR372 special handling for mail like terms
                            string[] mailTerms = new string[] { "mail", "mailed", "mailing", "mailings", "mails" };
                            if (mailTerms.Contains(term.Name) && (textBeforeMatch.EndsWith("@") || taggedText[matchIndex + term.Name.Length] == '@'))
                            {
                                continue;
                            }

                            // We want to make sure that we arent' finding a word that's 
                            // already been tagged:  [auto-markup:Economic]financial[end auto-markup]
                            if (match.Success && (endIndex >= startIndex))
                            {
                                // We only want to save Early Action info for the first match
                                if (!handledEarlyActionAlready)
                                {
                                    // If the termList requires early action
                                    if (term.TermList.EarlyAction.HasValue && term.TermList.EarlyAction.Value && letter.LetterTypeId != 1 /* Duplicate */)
                                    {
                                        letter.EarlyActionStatusId = _needsReviewEarlyActionStatusId;
                                    }

                                    // Add to the list of terms found in the letter
                                    if (letter.LetterTermConfirmations != null)
                                    {
                                        // if the letter term confirmation entry already exists
                                        if (letter.LetterTermConfirmations.Where(x => x.TermId == term.TermId).Count() == 0)
                                        {
                                            // new term confirmation
                                            letter.LetterTermConfirmations.Add(new LetterTermConfirmation { TermId = term.TermId, StatusId = null });
                                        }
                                    }
                                }

                                handledEarlyActionAlready = true;

                                if (!handledPublishToReadingRoom && term.TermList.HarmfulContent)
                                {
                                    letter.PublishToReadingRoom = false;

                                    // Set the unpublished reason
                                    var unpublishedReason = _lookupUnpublishedReason.SingleOrDefault(x => x.TermListId == term.TermListId);
                                    if (unpublishedReason != null)
                                    {
                                        letter.UnpublishedReasonId = unpublishedReason.UnpublishedReasonId;
                                    }

                                    handledPublishToReadingRoom = true;
                                }

                                var markupStartForTermList = string.Format(markupStart, term.TermList.Name);
                                taggedText = taggedText.Insert(matchIndex + match.Length, markupEnd);
                                taggedText = taggedText.Insert(matchIndex, markupStartForTermList);

                                // match.Index for each match in matches was determined
                                // before any auto-markup tags were added.  So, we need to
                                lengthAdjustment += (markupEnd.Length + markupStartForTermList.Length);
                            }
                        }
                    }
                }
            }
            
            return taggedText.Trim();
        }
  
        /// <summary>
        /// TermLists per phase are cached.  Doing this until a more robust, application-wide caching strategy
        /// can be recommended and suggested.  This is called by the TagAutoMarkupTerms method.
        /// </summary>
        /// <param name="phase"></param>
        /// <returns></returns>
        private ICollection<TermList> GetTermLists(Phase phase)
        {
            ICollection<TermList> termLists;
            string key = string.Format("PhaseTermLists_{0}", phase.PhaseId);
            if (_cache.Contains(key))
            {
                termLists = _cache.Get(key) as ICollection<TermList>;
            }
            else
            {
                termLists = phase.TermLists;
                _cache.Add(key, termLists, new CacheItemPolicy { AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddDays(1)) });
            }

            return termLists;
        }

        /// <summary>
        /// TermLists per phase are cached.  Doing this until a more robust, application-wide caching strategy
        /// can be recommended and suggested.  This is called by the TagAutoMarkupTerms method.
        /// </summary>
        /// <param name="phase"></param>
        /// <returns></returns>
        private Phase GetPhase(PhaseManager phaseManager, int phaseId)
        {
            Phase phase = null;
            string key = string.Format("AutomarkupPhaseCache_{0}", phaseId);
            if (_cache.Contains(key))
            {
                phase = _cache.Get(key) as Phase;
            }
            else
            {
                phase = phaseManager.Get(phaseId);
                _cache.Add(key, phase, new CacheItemPolicy { AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddDays(1)) });

                // CARA-1221:  Pre-fetching term lists
                GetTermLists(phase);
            }

            return phase;
        }


		/// <summary>
		/// Compares a letter to all existing letters in the same
		/// phase to determine its letter type
		/// </summary>
		/// <param name="letter"></param>
		/// <returns></returns>
        public LetterType DetermineLetterType(Letter letter, List<string> letterFragKeys = null)
        {
            LetterType letterType = _uniqueLetterType;

            // make sure the letter has fragment keys
            if (letterFragKeys == null)
            {
                letterFragKeys = _formLetterDetector.FragmentKeys(letter.PhaseId, letter.LetterId);
                if (!_formLetterDetector.GetRelevantFragmentKeys(letterFragKeys).Any())
                {
                    return letterType;
                }
            }

            LetterSenderComposite letterMetaData = new LetterSenderComposite(letter);

            // Potential duplicates
            // TODO:  Don't compare duplicates
            IQueryable<LetterSenderComposite> potentialDuplicates = GetLetterSenderCompositeByAuthor(letterMetaData)
                .Where(x => x.LetterId != letter.LetterId && x.Size > 0 && x.LetterTypeId != _pendingLetterTypeId).AsQueryable();

            if (potentialDuplicates != null && potentialDuplicates.Any())
            {
                letterType = DetectDuplicate(letter, potentialDuplicates.ToList(), letterFragKeys);
            }

            // Duplicate not found, search for an existing master
            if (letterType.LetterTypeId == _uniqueLetterType.LetterTypeId)
            {
                // Potential masters from existing masters
                IQueryable<LetterSenderComposite> potentialMastersExisting = GetLetterSenderCompositeByLetterType(letter.PhaseId, _masterFormLetterType)
                    .Where(x => !(x.AuthorFirstName == letterMetaData.AuthorFirstName && x.AuthorLastName == letterMetaData.AuthorLastName)
                        && x.LetterId != letterMetaData.LetterId && x.Size > 0)
                    .AsQueryable();

                if (potentialMastersExisting != null && potentialMastersExisting.Any())
                {
                    letterType = BatchFormDetection(letter, potentialMastersExisting, letterFragKeys);
                }
            }

            // Existing master not found, search for a new master among existing uniques
            if (letterType.LetterTypeId == _uniqueLetterType.LetterTypeId)
            {
                // Potential masters from existing uniques
                IQueryable<LetterSenderComposite> potentialMasters = GetLetterSenderCompositeByLetterType(letter.PhaseId, _uniqueLetterType)
                    .Where(x => !(x.AuthorFirstName == letterMetaData.AuthorFirstName && x.AuthorLastName == letterMetaData.AuthorLastName)
                        && x.LetterId != letterMetaData.LetterId && x.Size > 0)
                    .AsQueryable();

                if (potentialMasters != null && potentialMasters.Any())
                {
                    letterType = BatchFormDetection(letter, potentialMasters, letterFragKeys);
                }
            }

            // If the letter is unique or duplicate, save it in Dynamo
            if (letterType.LetterTypeId == _uniqueLetterType.LetterTypeId || letterType.LetterTypeId == _duplicateLetterType.LetterTypeId)
            {
                bool success = dynamo.Update(
                    new List<KeyValuePair<string, AttributeValue>>()
                    {
                        //indexes
                        new KeyValuePair<string,AttributeValue>
                            ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                        new KeyValuePair<string,AttributeValue>
                            ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                    },
                    new List<KeyValuePair<string, AttributeValue>>()
                    {
                        //new values
                        new KeyValuePair<string,AttributeValue>
                            ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letterType.LetterTypeId)),
                        new KeyValuePair<string,AttributeValue>
                            ("FormSetId", DynamoDBHelper.GetAttributeValue(0)),
                    }
                );

                //check for successful result
                if (!success)
                {
                    _logger.Error(String.Format(
                        "Could not update letter ({0}, {1}, {2}) in Dynamo",
                        letter.PhaseId,
                        letter.LetterId,
                        _uniqueLetterType.Name));
                }
            }

            return letterType;
        }

        /// <summary>
        /// Stores the letter metadata in DynamoDB
        /// </summary>
        /// <param name="letter"></param>
        private void StoreLetterInfo(List<LetterSenderComposite> letters)
        {
            if (letters.Any())
            {
                
                List<List<KeyValuePair<string,AttributeValue>>> goodItems = 
                    dynamo.Remove(letters.Select(letter => letter.Item.Where(x => x.Key == "LetterId" || x.Key == "PhaseId")
                    .ToList()).ToList());
                if (goodItems.Count() < letters.Count())
                {
                    _logger.Error(string.Format("{0} letters could not be removed from the Dynamo DB", 
                        letters.Count() - goodItems.Count()));
                }

                goodItems = dynamo.Put(letters.Select(letter => letter.Item).ToList());
                if (goodItems.Count() < letters.Count())
                {
                    _logger.Error(string.Format("{0} letters could not be added to the Dynamo DB",
                        letters.Count() - goodItems.Count()));
                }
            }
        }

        private void DeleteFromDynamo(Letter letter)
        {
            bool success = dynamo.Remove(new List<KeyValuePair<string, AttributeValue>>()
            {
                new KeyValuePair<string,AttributeValue>("PhaseId",DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                new KeyValuePair<string,AttributeValue>("LetterId",DynamoDBHelper.GetAttributeValue(letter.LetterId))
            });
            if (!success)
            {
                _logger.Error(string.Format("Could not delete letter {0} from Dynamo", letter.LetterId));
            }
        }

        /// <summary>
        /// Gets information for all the letters with the specified phase and lettertype from dynamo
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="letterType"></param>
        /// <returns></returns>
        private IQueryable<LetterSenderComposite> GetLetterSenderCompositeByLetterType(int phaseId, LetterType letterType)
        {
            IQueryable<List<KeyValuePair<string, AttributeValue>>> queryResult = dynamo.Query("LetterTypeId-index",
                new List<KeyValuePair<string, AttributeValue>>()
                {
                    new KeyValuePair<string, AttributeValue>("PhaseId",DynamoDBHelper.GetAttributeValue(phaseId)),
                    new KeyValuePair<string,AttributeValue>("LetterTypeId",DynamoDBHelper.GetAttributeValue(letterType.LetterTypeId))
                }, null);
            IQueryable<LetterSenderComposite> returnValue = null;

            //if the query was successful generate list from response otherwise get the information from the database
            if (queryResult != null)
            {
                returnValue = queryResult.Select(item => new LetterSenderComposite(item)).AsQueryable();
            }
            else
            {
                returnValue = Find(letter => letter.PhaseId == phaseId && letter.LetterTypeId == letterType.LetterTypeId)
                    .Select(letter => new LetterSenderComposite(letter, 
                        _formLetterDetector.FragmentKeys(phaseId, letter.LetterId)))
                    .AsQueryable();
            }

            return returnValue;
        }

        private IQueryable<LetterSenderComposite> GetLetterSenderCompositeByAuthor(LetterSenderComposite letterData)
        {
            IQueryable<List<KeyValuePair<string, AttributeValue>>> queryResult = dynamo.Query("AuthorName-index",
                new List<KeyValuePair<string, AttributeValue>>() 
                {
                    new KeyValuePair<string, AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(letterData.PhaseId)),
                    new KeyValuePair<string, AttributeValue>("AuthorName", DynamoDBHelper.GetAttributeValue(letterData.AuthorName))
                }, null);
            IQueryable<LetterSenderComposite> returnValue = null;

            //if the query was successful, generate list from response otherwise get the information from the database
            if (queryResult != null)
            {
                returnValue = queryResult.Select(item => new LetterSenderComposite(item)).AsQueryable();
            }
            else
            {
                returnValue = Find(letter => letter.PhaseId == letterData.PhaseId
                        && letter.Sender.FirstName == letterData.AuthorFirstName
                        && letter.Sender.LastName == letterData.AuthorLastName)
                    .Select(letter => new LetterSenderComposite(letter,
                        _formLetterDetector.FragmentKeys(letter.PhaseId, letter.LetterId)))
                    .AsQueryable();
            }

            return returnValue;
        }


        /// <summary>
        /// Executes batch form detection based on a queryable that is passed into the method
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="potentialMasters"></param>
        /// <param name="letterTypeLookup"></param>
        /// <returns></returns>
        private LetterType BatchFormDetection(Letter letter, IQueryable<LetterSenderComposite> potentialMasters, List<string> letterFragKeys = null)
        {
            // Get the letters in batches to be more efficient
            var batchNumber = 0;
            var masterFound = false;
            int lettersReturnedInBatch;
            var uniqueLetterType = _uniqueLetterType;
            var letterType = uniqueLetterType;

            do
            {
                var potentialMastersBatch = potentialMasters
                    .OrderBy(x => x.LetterId)
                    .Skip(batchNumber++ * _lettersPerBatch)
                    .Take(_lettersPerBatch)
                    .ToList();

                letterType = DetectForm(letter, potentialMastersBatch, letterFragKeys);

                lettersReturnedInBatch = potentialMastersBatch.Count;

                if (letterType.LetterTypeId != uniqueLetterType.LetterTypeId)
                {
                    masterFound = true;
                }
            } while (!masterFound && lettersReturnedInBatch == _lettersPerBatch);

            return letterType;
        }

        public LetterType DetectDuplicate(Letter letter, ICollection<LetterSenderComposite> potentialDuplicates, List<string> letterFragKeys = null)
        {
            var letterSender = letter.Sender;
            LetterType letterType = _uniqueLetterType;

            foreach (LetterSenderComposite potentialDuplicate in potentialDuplicates)
            {
                if (letterSender.FirstName == potentialDuplicate.AuthorFirstName
                    && letterSender.LastName == potentialDuplicate.AuthorLastName
                    && potentialDuplicate.LetterTypeId != _duplicateLetterType.LetterTypeId)
                {
                    double confidence;
                    letterType = _formLetterDetector.Compare
                        (potentialDuplicate, letter, out confidence, dynamo, letterFragKeys);

                    if (letterType == _uniqueLetterType)
                    {
                        continue;
                    }

                    // If result was master form then the new letter is a "form minus" of the old letter which only matters if the old letter was unique
                    if (letterType == _masterFormLetterType && potentialDuplicate.LetterTypeId != _uniqueLetterType.LetterTypeId)
                    {
                        letterType = _duplicateLetterType;
                        letter.Confidence = confidence;
                        letter.MasterDuplicateId = potentialDuplicate.LetterId;

                        // Duplicate found, no need to continue processing other letters
                        break;
                    }

                    // If a form letter was detected, this means that the content and author is the same,
                    // so this is really a duplicate.
                    else if (letterType == _formLetterType)
                    {
                        letterType = _duplicateLetterType;
                        letter.Confidence = confidence;
                        letter.MasterDuplicateId = potentialDuplicate.LetterId;

                        // Duplicate found, no need to continue processing other letters
                        break;
                    }

                    // If this is a form plus or form minus, then this should be treated as such:
                    //		same author with additional content
                    #region FormPlus or master
                    else if (letterType == _formPlusLetterType || letterType == _masterFormLetterType)
                    {
                        // Create new form set, if necessary
                        if (potentialDuplicate.LetterTypeId == _uniqueLetterType.LetterTypeId)
                        {
                            Letter potential = Get(potentialDuplicate.LetterId);

                            //make the new formset
                            using (FormSetManager formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                            {
                                FormSet formSet = new FormSet
                                {
                                    PhaseId = letter.PhaseId,
                                    MasterFormId = letterType == _masterFormLetterType ? 
                                        letter.LetterId : potentialDuplicate.LetterId,
                                    Name = string.Format("Form Set: Master Letter #{0}", letterType == _masterFormLetterType ?
                                        letter.LetterSequence : potential.LetterSequence)
                                };

                                formSetManager.Add(formSet);
                                formSetManager.UnitOfWork.Save();

                                if (letterType == _masterFormLetterType)
                                {
                                    potential.LetterTypeId = _formPlusLetterType.LetterTypeId;
                                    potential.Confidence = confidence;
                                }
                                else
                                {
                                    potential.LetterTypeId = _masterFormLetterType.LetterTypeId;
                                }

                                potential.FormSetId = formSet.FormSetId;
                                letter.FormSetId = formSet.FormSetId;

                                bool success = dynamo.Update(
                                    new List<KeyValuePair<string, AttributeValue>>()
                                    {
                                        //indexes
                                        new KeyValuePair<string,AttributeValue>
                                            ("PhaseId", DynamoDBHelper.GetAttributeValue(potential.PhaseId)),
                                        new KeyValuePair<string,AttributeValue>
                                            ("LetterId", DynamoDBHelper.GetAttributeValue(potential.LetterId))
                                    },
                                    new List<KeyValuePair<string, AttributeValue>>()
                                    {
                                        //new values
                                        new KeyValuePair<string,AttributeValue>
                                            ("FormSetId", DynamoDBHelper.GetAttributeValue(formSet.FormSetId)),
                                        new KeyValuePair<string,AttributeValue>
                                            ("LetterTypeId", DynamoDBHelper.GetAttributeValue(potential.LetterTypeId))
                                    }
                                );
                                
                                // check for successful result
                                if (!success)
                                {
                                    _logger.Error(String.Format(
                                        "Could not update letterId {0} with form set {1} and letter type {2} in Dynamo",
                                        potential.LetterId,
                                        formSet.FormSetId,
                                        potential.LetterTypeId));
                                }
                            }
                        }
                        // Potential was already in a formset
                        else if (potentialDuplicate.FormSetId > 0)
                        {
                            letter.FormSetId = potentialDuplicate.FormSetId;
                            bool success = dynamo.Update(
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    //indexes
                                    new KeyValuePair<string,AttributeValue>
                                        ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                                    new KeyValuePair<string,AttributeValue>
                                        ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                                },
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    //new values
                                    new KeyValuePair<string,AttributeValue>
                                        ("FormSetId", DynamoDBHelper.GetAttributeValue(potentialDuplicate.FormSetId)),
                                    new KeyValuePair<string,AttributeValue>
                                        ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letterType.LetterTypeId)),
                                }
                            );

                            if (!success)
                            {
                                _logger.Error(string.Format("Letter {0} could not be updated with formset {1} in Dynamo",
                                    letter.LetterId, potentialDuplicate.FormSetId));
                            }
                        }
                    }
                    #endregion

                    // set the letter confidence if necessary
                    if (letterType != _masterFormLetterType)
                    {
                        letter.Confidence = confidence;
                    }

                    // Duplicate found, no need to continue processing other letters
                    break;
                }
            }

            return letterType;
        }

		public LetterType DetectForm(Letter letter, ICollection<LetterSenderComposite> potentialMasters, List<string> letterfragKeys = null)
		{
			LetterType letterType = _uniqueLetterType;

            // Ensure the letter has Fragment Keys
            if (letterfragKeys == null)
            {
                letterfragKeys = _formLetterDetector.FragmentKeys(letter.PhaseId, letter.LetterId);
                if (!_formLetterDetector.GetRelevantFragmentKeys(letterfragKeys).Any())
                {
                    return letterType;
                }
            }

			foreach (var potentialMaster in potentialMasters)
			{
                double confidence;
				letterType = _formLetterDetector.Compare
                    (potentialMaster, letter, out confidence, dynamo, letterfragKeys);

                // If result was master form then the new letter is a "form minus"
                // of the potentialMaster which only matters if the potentialMaster was unique
                if (letterType == _masterFormLetterType && potentialMaster.LetterTypeId != _uniqueLetterType.LetterTypeId)
                {
                    letterType = _formLetterType;
                }

                #region match found
                if (letterType != _uniqueLetterType)
                {
                    #region New master found
                    if (potentialMaster.LetterTypeId == _uniqueLetterType.LetterTypeId)
                    {
                        Letter potential = Get(potentialMaster.LetterId);

                        // Create new form set
                        using (FormSetManager formSetManager = ManagerFactory.CreateInstance<FormSetManager>())
                        {
                            FormSet formSet = new FormSet
                            {
                                PhaseId = letter.PhaseId,
                                MasterFormId = letterType == _masterFormLetterType ?
                                    letter.LetterId : potentialMaster.LetterId,
                                Name = string.Format("Form Set: Master Letter #{0}",
                                    letterType == _masterFormLetterType ?
                                        letter.LetterSequence : potential.LetterSequence)
                            };

                            formSetManager.Add(formSet);
                            formSetManager.UnitOfWork.Save();
                            if (letterType == _masterFormLetterType)
                            {
                                potential.LetterTypeId = _formPlusLetterType.LetterTypeId;
                                potential.Confidence = confidence;
                            }
                            else
                            {
                                potential.LetterTypeId = _masterFormLetterType.LetterTypeId;
                            }

                            potential.FormSetId = formSet.FormSetId;
                            letter.FormSetId = formSet.FormSetId;

                            // Update the potentialMaster
                            bool success = dynamo.Update(
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    //indexes
                                    new KeyValuePair<string,AttributeValue>
                                        ("PhaseId",DynamoDBHelper.GetAttributeValue(potential.PhaseId)),
                                    new KeyValuePair<string,AttributeValue>
                                        ("LetterId",DynamoDBHelper.GetAttributeValue(potential.LetterId))
                                },
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    //new values
                                    new KeyValuePair<string,AttributeValue>
                                        ("FormSetId",DynamoDBHelper.GetAttributeValue(formSet.FormSetId)),
                                    new KeyValuePair<string,AttributeValue>
                                        ("LetterTypeId",DynamoDBHelper.GetAttributeValue(potential.LetterTypeId))
                                }
                            );

                            //check for successful result
                            if (!success)
                            {
                                _logger.Error(String.Format(
                                    "Could not update letter ({0}, {1}) with form set {2} and letter type {3} in Dynamo",
                                    potential.PhaseId,
                                    potential.LetterId,
                                    formSet.FormSetId,
                                    potential.LetterTypeId));
                            }

                            // Update the letter
                            success = dynamo.Update(
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    //indexes
                                    new KeyValuePair<string,AttributeValue>
                                        ("PhaseId",DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                                    new KeyValuePair<string,AttributeValue>
                                        ("LetterId",DynamoDBHelper.GetAttributeValue(letter.LetterId))
                                },
                                new List<KeyValuePair<string, AttributeValue>>()
                                {
                                    //new values
                                    new KeyValuePair<string,AttributeValue>
                                        ("FormSetId",DynamoDBHelper.GetAttributeValue(formSet.FormSetId)),
                                    new KeyValuePair<string,AttributeValue>
                                        ("LetterTypeId",DynamoDBHelper.GetAttributeValue(letterType.LetterTypeId))
                                }
                            );

                            //check for successful result
                            if (!success)
                            {
                                _logger.Error(String.Format(
                                    "Could not update letter ({0}, {1}, {2}, {3}) in Dynamo",
                                    letter.PhaseId,
                                    letter.LetterId,
                                    letterType.Name,
                                    formSet.FormSetId));
                            }
                        }
                    }
                    #endregion New master found

                    #region matches existing formset
                    //if the potentialMaster is already part of a formset
                    else if (potentialMaster.FormSetId > 0)
                    {
                        letter.FormSetId = potentialMaster.FormSetId;

                        bool success = dynamo.Update(
                            new List<KeyValuePair<string, AttributeValue>>()
                            {
                                //indexes
                                new KeyValuePair<string,AttributeValue>
                                    ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                                new KeyValuePair<string,AttributeValue>
                                    ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                            },
                            new List<KeyValuePair<string, AttributeValue>>()
                            {
                                //new values
                                new KeyValuePair<string,AttributeValue>
                                    ("FormSetId", DynamoDBHelper.GetAttributeValue(potentialMaster.FormSetId)),
                                new KeyValuePair<string,AttributeValue>
                                    ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letterType.LetterTypeId)),
                            }
                        );

                        //check for successful result
                        if (!success)
                        {
                            _logger.Error(String.Format(
                                "Could not update letter ({0}, {1}, {2}) in Dynamo",
                                letter.PhaseId,
                                letter.LetterId,
                                letterType.Name));
                        }
                    }
                    #endregion matches existing formset

                    if (letterType != _masterFormLetterType)
                    {
                        letter.Confidence = confidence;
                    }

                    // Master found, no need to continue processing other letters
                    break;
                }
                #endregion
            }

			return letterType;
		}

        /// <summary>
        /// Determine the letter type for letters that have the Pending letter type
        /// </summary>
        /// <param name="maxDateEntered">Only consider letters entered before this DateTime</param>
        public void ProcessPendingLetters(DateTime maxDateEntered)
        {
            LetterType letterType;

            try
            {
                string queueName;
                var sqs = SqsHelper.GetQueue(_formDetectionQueueNameKey, out queueName);

                var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                var createQueueResponse = sqs.CreateQueue(sqsRequest);
                string queueUrl = createQueueResponse.QueueUrl;

                int numberOfMessages = 0;
                do
                {
                    IList<int> letterIds;
                    List<DeleteMessageBatchRequestEntry> dmbrEntries = new List<DeleteMessageBatchRequestEntry>(numberOfMessages);
                    var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = _lettersPerBatch };
                    var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                    numberOfMessages = 0;

                    if (receiveMessageResponse.Messages != null)
                    {
                        numberOfMessages = receiveMessageResponse.Messages.Count;
                        letterIds = new List<int>(numberOfMessages);

                        foreach (var message in receiveMessageResponse.Messages)
                        {
                            letterIds.Add(int.Parse(message.Body));
                            dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                        }

                        // Find all Pending LetterIds retrieved from the queue
                        var pendingLettersQuery = All.Where(ltr => letterIds.Contains(ltr.LetterId));

                        int pendingLettersCount = pendingLettersQuery.Count();
                        if (pendingLettersCount > 0)
                        {
                            // Don't need to wrap in a using because it is sharing the same UnitOfWork
                            var authorManager = ManagerFactory.CreateInstance<AuthorManager>();
                            authorManager.UnitOfWork = UnitOfWork;

                            _logger.DebugFormat("Found {0} pending letters awaiting letter type determination", pendingLettersCount);

                            IList<Letter> letters;

                            letters = pendingLettersQuery
                                .OrderBy(ltr => ltr.LetterId)
                                .Select(ltr => new { Letter = ltr, Authors = ltr.Authors, FormSet = ltr.FormSet }) // Eager loads authors and formset in one query
                                .Select(laf => laf.Letter)
                                .ToList();

                            List<LetterSenderComposite> metaDatas = new List<LetterSenderComposite>();
                            foreach (Letter l in letters)
                            {
                                try
                                {
                                    metaDatas.Add(new LetterSenderComposite(l, _formLetterDetector.FragmentKeys(l.Text)));
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(string.Format("Error determining the letter type:: Phase ID: {0}, Letter ID/#: {1}/{2}",
                                            l.PhaseId,
                                            l.LetterId,
                                            l.LetterSequence),
                                        ex);
                                }
                            }

                            StoreLetterInfo(metaDatas);

                            // Compare the pending letters against non-pending letters
                            foreach (var newLetter in letters)
                            {
                                try
                                {
                                    #region Compare pending letter against existing letters
                                    List<string> newLetterFragKeys = metaDatas
                                        .FirstOrDefault(x => x.LetterId == newLetter.LetterId)
                                        .FragmentKeys;

                                    if (!_formLetterDetector.GetRelevantFragmentKeys(newLetterFragKeys).Any())
                                    {
                                        letterType = _uniqueLetterType;
                                    }
                                    else
                                    {
                                        letterType = DetermineLetterType(newLetter, newLetterFragKeys);

                                        if (letterType.Equals(_formLetterType))
                                        {
                                            newLetter.LetterStatusId = _notNeededLetterStatusId;
                                        }

                                        // CARA-1283 form letters should inherit the publishing status of the master
                                        if (letterType.Equals(_formLetterType) || letterType.Equals(_formPlusLetterType))
                                        {
                                            InheritPropertiesFromMasterForm(newLetter, letterType.Equals(_formLetterType));
                                        }

                                        // Duplicate letters
                                        else if (letterType.Equals(_duplicateLetterType))
                                        {
                                            newLetter.EarlyActionStatusId = _notRequiredEarlyActionStatusId;
                                            newLetter.LetterStatusId = _notNeededLetterStatusId;
                                        }
                                    }

                                    newLetter.LetterTypeId = letterType.LetterTypeId;
                                    if (newLetter.LetterTypeId == _pendingLetterTypeId)
                                    {
                                        throw new Exception("Form detection: letter type was not set for letter id: " + newLetter.LetterId.ToString());
                                    }

                                    if (letterType != _uniqueLetterType)
                                    {
                                        _logger.DebugFormat("Type Determined:: Phase ID: {0}, Letter ID/#: {1}/{2}, Letter Type: {3}",
                                            newLetter.PhaseId,
                                            newLetter.LetterId,
                                            newLetter.LetterSequence,
                                            letterType.Name);
                                    }
                                    #endregion Compare pending letter against existing letters
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(string.Format("Error determining the letter type:: Phase ID: {0}, Letter ID/#: {1}/{2}",
                                            newLetter.PhaseId,
                                            newLetter.LetterId,
                                            newLetter.LetterSequence),
                                        ex);
                                }
                            }

                            // Save the changes made to each batch of letters
                            UnitOfWork.Save();

                            // Remove the entries from the queue
                            var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrEntries };
                            sqs.DeleteMessageBatch(deleteRequest);
                        }
                    }
                } while (numberOfMessages > 0);
            }
            catch (AmazonSQSException ase)
            {
                _logger.Error("An error occurred trying to interact with the queue", ase);
            }
        }

        public void InheritPropertiesFromMasterForm(Letter newLetter, bool isForm)
        {
            var formSetManager = ManagerFactory.CreateInstance<FormSetManager>();
            formSetManager.UnitOfWork = UnitOfWork;

            var publishInfo = (
                               from l in All
                               join f in formSetManager.All on l.LetterId equals f.MasterFormId
                               where f.FormSetId == newLetter.FormSetId
                               select new
                               {
                                   l.PublishToReadingRoom,
                                   l.UnpublishedReasonId,
                                   l.UnpublishedReasonOther,
                                   l.LetterStatusId,
                                   l.EarlyActionStatusId
                               }).SingleOrDefault();

            if (publishInfo != null)
            {
                newLetter.PublishToReadingRoom = publishInfo.PublishToReadingRoom;
                newLetter.UnpublishedReasonId = publishInfo.UnpublishedReasonId;
                newLetter.UnpublishedReasonOther = publishInfo.UnpublishedReasonOther;
             
                if (isForm)
                {
                    newLetter.EarlyActionStatusId = publishInfo.EarlyActionStatusId;
                }
            }
        }

		private string ReplaceInvalidCharacters(string text)
        {
            string replacedText = text;
            if (replacedText != null)
            {
                foreach (TextReplacementItem txtReplacement in txtReplacementConfig.ReplacementItems)
                {
                    replacedText = replacedText.Replace(txtReplacement.Exception, txtReplacement.Replacement);
                }
            }

			return replacedText;
		}

        public void FixLetterText(Letter letter)
        {
            letter.CodedText = ReplaceInvalidCharacters(letter.CodedText);
            letter.HtmlCodedText = ReplaceInvalidCharacters(letter.HtmlCodedText);
            letter.Text = ReplaceInvalidCharacters(letter.Text);
            letter.HtmlText = ReplaceInvalidCharacters(letter.HtmlText);
        }


        /// <summary>
        /// Returns next Form Letter with the given letter ID
        /// </summary>
        /// <param name="name">The ID of the letter</param>
        /// <returns>Letter or null</returns>
        public Letter GetNextFormLetter(FormSetSearchResultFilter filter, int sequence)
        {
            var query = NavigationQuery(filter);

            return query.Where(x => x.LetterSequence > sequence)
                .OrderBy(x => x.LetterSequence)
                .FirstOrDefault();
        }
  
        /// <summary>
        /// Returns Prev Form Letter with the given letter ID
        /// </summary>
        /// <param name="name">The ID of the letter</param>
        /// <returns>Letter or null</returns>
        public Letter GetPrevFormLetter(FormSetSearchResultFilter filter, int sequence)
        {
            var query = NavigationQuery(filter);

            return query.Where(x => x.LetterSequence < sequence)
                .OrderByDescending(x => x.LetterSequence)
                .FirstOrDefault();
        }

        private IQueryable<Letter> NavigationQuery(FormSetSearchResultFilter filter)
        {


            var query = All.Where(x => x.PhaseId == filter.PhaseId
                                        && x.LetterTypeId != _masterFormLetterType.LetterTypeId);

            if (filter.FormSetId.HasValue)
            {
                query = query.Where(x => x.FormSetId == filter.FormSetId);
            }
            else
            {
                query = query.Where(x => x.FormSetId == null);
            }

            if (filter.LetterStatusId != null && filter.LetterStatusId.HasValue)
            {
                query = query.Where(x => x.LetterStatusId == filter.LetterStatusId);
            }

            if (filter.LetterTypeId != null && filter.LetterTypeId.HasValue)
            {
                if (filter.LetterTypeId == 6)
                {
                    query = query.Where(x => LookupLetterType.NotPending.Contains(x.LetterTypeId));
                }
                else if (filter.LetterTypeId == 7)
                {
                    query = query.Where(x => LookupLetterType.AllForms.Contains(x.LetterTypeId));
                }
                else if (filter.LetterTypeId == 8)
                {
                    query = query.Where(x => LookupLetterType.MasterFormOrUnique.Contains(x.LetterTypeId));
                }
                else if (filter.LetterTypeId == 10)
                {
                    query = query.Where(x => LookupLetterType.UniqueOrForms.Contains(x.LetterTypeId));
                }
                else
                {
                    query = query.Where(x => x.LetterTypeId == filter.LetterTypeId);
                }
            }

            return query;
        }

        public void UpdatePublishingStatus(int formSetId, bool publish, int? unpublishReasonId, string unpublishReason)
        {
            (Repository as ILetterRepository).UpdatePublishingStatus(formSetId, publish, unpublishReasonId, unpublishReason);
        }

        public void UpdateLetterStatus(int formSetId, int letterStatusId)
        {
            (Repository as ILetterRepository).UpdateLetterStatus(formSetId, letterStatusId);
        }

        public void UpdateEarlyAttentionStatus(int formSetId, int earlyAttentionStatusId)
        {
            (Repository as ILetterRepository).UpdateFormSetEarlyAttentionStatus(formSetId, earlyAttentionStatusId);
        }

        public int TransitionLetterStatusByLetterType(LetterType oldType, LetterType newType, int currentLetterStatusId)
        {
            int letterStatusId = currentLetterStatusId;
            if (newType.Equals(_duplicateLetterType) || newType.Equals(_formLetterType))
            {
                letterStatusId = _notNeededLetterStatusId;
            }
            else if (oldType.Equals(_duplicateLetterType) || oldType.Equals(_formLetterType))
            {
                letterStatusId = _inProgressLetterStatusId;
            }

            return letterStatusId;
        }

        public int? TransitionEarlyActionStatusByLetterType(LetterType oldType, LetterType newType, Letter letter, int? ignoreCommentId = null)
        {
            int? earlyAttentionStatusId = letter.EarlyActionStatusId.Value;

            if (newType.Equals(_duplicateLetterType))
            {
                earlyAttentionStatusId = _notRequiredEarlyActionStatusId;
            }
            else if (newType.Equals(_masterFormLetterType))
            {
                earlyAttentionStatusId = DetermineEarlyAttentionStatus(letter, ignoreCommentId);
                UpdateEarlyAttentionStatus(letter.FormSetId.Value, earlyAttentionStatusId.Value);
            }
            else if (newType.Equals(_formPlusLetterType) || newType.Equals(_uniqueLetterType))
            {
                earlyAttentionStatusId = DetermineEarlyAttentionStatus(letter);
            }
            else if (newType.Equals(_formLetterType) && letter.FormSetId.HasValue)
            {
                var master = letter.FormSet.MasterFormLetter;
                earlyAttentionStatusId = master.EarlyActionStatusId;
            }

            return earlyAttentionStatusId;
        }

        public int DetermineEarlyAttentionStatus(Letter letter, int? ignoreCommentId = null)
        {
            int earlyAttentionStatusId = _notRequiredEarlyActionStatusId;
            bool earlyAttnCommentCodeExists = false;

            #region Determine if letter has an early attention code
            var comments = letter.Comments;
            if (ignoreCommentId.HasValue)
            {
                comments = comments.Where(x => x.CommentId != ignoreCommentId.Value).ToList();
            }
            int commentCount = comments == null ? 0 : comments.Count();
            if (commentCount > 0)
            {
                using (var commentCodeManager = ManagerFactory.CreateInstance<CommentCodeManager>())
                {
                    using (var phasecodeManager = ManagerFactory.CreateInstance<PhaseCodeManager>())
                    {
                        foreach (var comment in comments)
                        {
                            var phaseCodeIds = commentCodeManager.All.Where(x => x.CommentId == comment.CommentId).Select(x => x.PhaseCodeId).ToList();

                            foreach (int phaseCodeId in phaseCodeIds)
                            {
                                int codeCat = phasecodeManager.GetCodeCategoryId(phaseCodeId);
                                if (codeCat == LookupManager.GetLookup<LookupCodeCategory>().Where(x => x.Name.Equals("Early Attention")).First().CodeCategoryId)
                                {
                                    earlyAttnCommentCodeExists = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            #endregion Determine if letter has an early attention code

            var lookupEarlyActionStatus = LookupManager.GetLookup<LookupEarlyActionStatus>();
            var lookupLetterTermConfirmationStatus = LookupManager.GetLookup<LookupLetterTermConfirmationStatus>();
            int needsAttentionId = lookupLetterTermConfirmationStatus.First(y => y.Name.Equals("Needs Attention")).ConfirmationStatusId;
            int notRequiredId = lookupLetterTermConfirmationStatus.First(y => y.Name.Equals("Not Required")).ConfirmationStatusId;
            int completedId = lookupLetterTermConfirmationStatus.First(y => y.Name.Equals("Completed")).ConfirmationStatusId;

            #region Determine letter's early attention status
            var eaConfirmations = letter.LetterTermConfirmations
                .Where(x => x.Term.TermList.EarlyAction.HasValue && x.Term.TermList.EarlyAction.Value)
                .Select(x => x);

            bool earlyAttentionTermExists = eaConfirmations != null ? eaConfirmations.Count() > 0 : false;

            if (!earlyAttentionTermExists || eaConfirmations.All(x => x.StatusId == notRequiredId))
            {
                earlyAttentionStatusId = lookupEarlyActionStatus.First(x => x.Name.Equals("Not Required")).StatusId;
            }
            else if (eaConfirmations.Any(x => x.StatusId == null || x.StatusId == needsAttentionId))
            {
                earlyAttentionStatusId = lookupEarlyActionStatus.First(x => x.Name.Equals("Auto-Marked")).StatusId;
            }
            else if (eaConfirmations.Any(x => x.StatusId == completedId) && eaConfirmations.All(x => x.StatusId == notRequiredId || x.StatusId == completedId))
            {
                earlyAttentionStatusId = lookupEarlyActionStatus.First(x => x.Name.Equals("Completed")).StatusId;
            }
            
            // Coded takes precedent over all
            if (earlyAttnCommentCodeExists && !eaConfirmations.Any(x => x.StatusId == null || x.StatusId == needsAttentionId))
            {
                earlyAttentionStatusId = lookupEarlyActionStatus.First(x => x.Name.Equals("Coded")).StatusId;
            }
            #endregion Determine letter's early attention status

            return earlyAttentionStatusId;
        }

        public bool MakeDuplicate(int phaseId, int letterId, Letter masterDuplicate)
        {
            // If the masterDuplicate is also a duplicate, use its masterDuplicateId
            var duplicateLetterId = masterDuplicate.MasterDuplicateId.HasValue ? masterDuplicate.MasterDuplicateId.Value : masterDuplicate.LetterId;
            var duplicate = Get(letterId);

            // Update DynamoDB
            bool success = dynamo.Update(
                new List<KeyValuePair<string, AttributeValue>>()
                {
                    //indexes
                    new KeyValuePair<string,AttributeValue>
                        ("PhaseId",DynamoDBHelper.GetAttributeValue(duplicate.PhaseId)),
                    new KeyValuePair<string,AttributeValue>
                        ("LetterId",DynamoDBHelper.GetAttributeValue(duplicate.LetterId))
                },
                new List<KeyValuePair<string, AttributeValue>>()
                {
                    //new values
                    new KeyValuePair<string,AttributeValue>
                        ("FormSetId",DynamoDBHelper.GetAttributeValue(0)), // Wipe out FormSetId
                    new KeyValuePair<string,AttributeValue>
                        ("LetterTypeId",DynamoDBHelper.GetAttributeValue(1)) // Make duplicate
                }
            );

            //check for successful result
            if (!success)
            {
                _logger.Error(String.Format(
                    "An error occurred making letter {1} in phase {0} a duplicate of letter {2} in Dynamo",
                    phaseId,
                    letterId,
                    masterDuplicate.LetterId
                ));
            }
            else
            {
                duplicate.MasterDuplicateId = duplicateLetterId;
                duplicate.LetterTypeId = 1;
                duplicate.EarlyActionStatusId = 1; /* Not Required */
                duplicate.LetterStatusId = 4; /* Not Needed */
                duplicate.FormSetId = null;
            }

            return success;
        }

        /// <summary>
        /// Html Encodes the text and converts linebreaks
        /// </summary>
        /// <param name="text">plain text to encode</param>
        /// <returns></returns>
        public static string HtmlEncodeTextAndLineBreaks(string text)
        {
            string retVal = string.Empty;
            if (!string.IsNullOrWhiteSpace(text))
            {
                text = text.Replace("&", "&amp;").Replace(">", "&gt;").Replace("<", "&lt;");
                string[] arrParagraghs = text.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string paragragh in arrParagraghs)
                {
                    retVal = string.Format("{0}<p>{1}</p>", retVal, paragragh);
                }
            }
            return retVal;
        }

        /// <summary>
        /// converts html text to plain text (removes codes and adds line breaks
        /// </summary>
        /// <param name="htmlText">html text to convert</param>
        /// <returns></returns>
        public static string ConvertHtmlToPlainText(string htmlText)
        {
            string retVal = string.Empty;
            try
            {
                htmlText = htmlText.Replace("<br>", "<br/>").Replace("<BR>", "<BR/>").Replace("&nbsp;", " ");
                Regex regex = new Regex(@"\&(?!(amp(?i);|gt(?i);|lt(?i);))[^;]*;");
                MatchCollection matches = regex.Matches(htmlText);
                foreach (Match match in matches)
                {
                    htmlText = htmlText.Replace(match.Value, "[" + match.Value.Substring(1, match.Value.Length - 2) + "]");
                }
                XDocument xDoc = XDocument.Parse(@"<div id='root'>" + htmlText + @"</div>");
                XElement element = xDoc.Elements().FirstOrDefault();
                retVal = GetText(element, true).Trim();
            }
            catch (Exception ex)
            {
                retVal = string.Empty;
            }
            return retVal;

        }

        /// <summary>
        /// Gets the plain text version of the html node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static string GetText(XElement element, bool lineBreakOnP = true)
        {

            string retVal = "";
            XNode node = element.FirstNode;
            int liCount = 1;
            while (node != null)
            {
                if (node.NodeType == System.Xml.XmlNodeType.Text)
                {
                    retVal = retVal + node.ToString().Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&");
                }
                else if (node.NodeType == System.Xml.XmlNodeType.Element)
                {
                    XElement childElement = node as XElement;
                    string elementName = childElement.Name.ToString().ToUpper();
                    List<string> breakBefore = new List<string> { "BR", "P", "TABLE", "OL", "UL" };
                    List<string> breakAfter = new List<string> { "P", "LI", "TR" };
                    if (!lineBreakOnP)
                    {
                        breakBefore.Remove("P");
                        breakAfter.Remove("P");
                    }
                    if (breakBefore.Contains(elementName))
                    {
                        retVal = retVal + "\r\n";
                    }
                    if (elementName == "LI")
                    {
                        if (element.Name.ToString().ToUpper() == "UL")
                        {
                            retVal = retVal + "* ";
                        }
                        else if (element.Name.ToString().ToUpper() == "OL")
                        {
                            retVal = retVal + liCount.ToString() + ". ";
                            liCount++;
                        }
                    }
                    if (elementName != "TD")
                    {
                        retVal = retVal + GetText(childElement, lineBreakOnP);
                    }
                    else
                    {
                        retVal = retVal + GetText(childElement, false);
                        if (childElement.ElementsAfterSelf("td").Count() > 0)
                        {
                            retVal = retVal + "\t";
                        }
                    }
                    if (breakAfter.Contains(elementName))
                    {
                        retVal = retVal + "\r\n";
                    }
                }
                node = node.NextNode;
            }
            return retVal;
        }
        #endregion
    }
}
