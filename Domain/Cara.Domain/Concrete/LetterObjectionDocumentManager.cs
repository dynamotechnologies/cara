﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class LetterObjectionDocumentManager : AbstractEntityManager<LetterObjectionDocument>
	{
        public LetterObjectionDocumentManager(IRepository<LetterObjectionDocument> repository)
			: base(repository)
		{
		}

        #region Override AbstractEntityManager methods
        public override LetterObjectionDocument Get(int uniqueId)
        {
            throw new NotImplementedException();
        }

        public override LetterObjectionDocument Get(string uniqueName)
        {
            throw new NotImplementedException();
        }
        #endregion Override AbstractEntityManager methods
    }
}
