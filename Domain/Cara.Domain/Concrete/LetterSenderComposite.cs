﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aquilent.Cara.Domain.Aws.DynamoDB;
using Amazon.DynamoDBv2.Model;
using System.Text;

namespace Aquilent.Cara.Domain
{
	public class LetterSenderComposite
    {
        #region Properties
        public int LetterId { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorLastName { get; set; }
		public int FormSetId { get; set; }
        public int LetterTypeId { get; set; }
        public int PhaseId { get; set; }
        public int Size { get; set; }

        public string AuthorName
        {
            get
            {
                return string.Format("{0}|{1}", AuthorFirstName.Replace("|", "\\|"), AuthorLastName.Replace("|", "\\|"));
            }

            set
            {
                string[] splitToken = new string[] { "\\|" };
                string[] nameFragments = value.Split(splitToken, StringSplitOptions.None);
                string firstName = string.Empty;
                string lastName = string.Empty;
                bool foundPipe = false;
                foreach (string nameFragment in nameFragments)
                {
                    if (!foundPipe)
                    {
                        if (firstName.Length > 0)
                        {
                            firstName = firstName + "|";
                        }

                        if (nameFragment.Contains('|'))
                        {
                            firstName = firstName + nameFragment.Split('|')[0];
                            lastName = nameFragment.Split('|')[1];
                            foundPipe = true;
                        }
                        else
                        {
                            firstName = firstName + nameFragment;
                        }
                    }
                    else
                    {
                        lastName = string.Format("{0}|{1}", lastName, nameFragment);
                    }
                }

                AuthorFirstName = firstName;
                AuthorLastName = lastName;
            }
        }

        public List<string> FragmentKeys { get; set; }

        public List<KeyValuePair<string, AttributeValue>> Item
        {
            get
            {
                List<KeyValuePair<string, AttributeValue>> returnValue = new List<KeyValuePair<string, AttributeValue>>()
                {
                    new KeyValuePair<string, AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(LetterId)),
                    new KeyValuePair<string, AttributeValue>("FormSetId", DynamoDBHelper.GetAttributeValue(FormSetId)),
                    new KeyValuePair<string, AttributeValue>("AuthorName", DynamoDBHelper.GetAttributeValue(AuthorName)),
                    new KeyValuePair<string, AttributeValue>("LetterTypeId", DynamoDBHelper.GetAttributeValue(LetterTypeId)),
                    new KeyValuePair<string, AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(PhaseId)),
                    new KeyValuePair<string, AttributeValue>("Size", DynamoDBHelper.GetAttributeValue(Size))
                };

                if (FragmentKeys.Any())
                {
                    returnValue.Add(new KeyValuePair<string, AttributeValue>
                        ("FragmentKeys", DynamoDBHelper.GetAttributeValue(FragmentKeys)));
                }

                return returnValue;
            }
        }
        #endregion Properties

        #region Constructors
        public LetterSenderComposite() { }

        public LetterSenderComposite(List<KeyValuePair<string, AttributeValue>> item)
        {
            LetterId = DynamoDBHelper.GetIntAttribute(item, "LetterId");
            FormSetId = DynamoDBHelper.GetIntAttribute(item, "FormSetId");
            AuthorName = DynamoDBHelper.GetStringAttribute(item, "AuthorName");
            LetterTypeId = DynamoDBHelper.GetIntAttribute(item, "LetterTypeId");
            PhaseId = DynamoDBHelper.GetIntAttribute(item, "PhaseId");
            Size = DynamoDBHelper.GetIntAttribute(item, "Size");
            FragmentKeys = DynamoDBHelper.GetStringSetAttribute(item, "FragmentKeys");
        }

        public LetterSenderComposite(Letter letter)
        {
            Author sender = letter.Authors.First(x => x.Sender == true);

            LetterId = letter.LetterId;
            PhaseId = letter.PhaseId;
            AuthorFirstName = sender.FirstName;
            AuthorLastName = sender.LastName;
            LetterTypeId = letter.LetterTypeId;
            Size = letter.Size.HasValue ? letter.Size.Value : 0;
            FormSetId = letter.FormSetId.HasValue ? letter.FormSetId.Value : 0;
        }

        public LetterSenderComposite(Letter letter, List<string> fragmentKeys)
        {
            Author sender = letter.Authors.First(x => x.Sender == true);

            LetterId = letter.LetterId;
            PhaseId = letter.PhaseId;
            AuthorFirstName = sender.FirstName;
            AuthorLastName = sender.LastName;
            LetterTypeId = letter.LetterTypeId;
            Size = letter.Size.HasValue ? letter.Size.Value : 0;
            FormSetId = letter.FormSetId.HasValue ? letter.FormSetId.Value : 0;
            FragmentKeys = fragmentKeys;
        }
        #endregion Constructors
    }
}
