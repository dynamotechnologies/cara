﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Cara.Configuration;
using System.Data.Objects.SqlClient;

namespace Aquilent.Cara.Domain.Report
{
	public class ReportManager : AbstractEntityManager<Report>
	{
		#region Constructor
		public ReportManager(IReportRepository repository)
			: base(repository)
		{
		}
		#endregion Constructor

		#region Properties
		public IReportRepository ReportRepository
		{
			get { return (Repository as IReportRepository); }
		}
        private static readonly TextReplacementConfiguration txtReplacementConfig = ConfigUtilities.GetSection<TextReplacementConfiguration>("textReplacementConfiguration");
        
		#endregion Properties

		#region Override AbstractEntityService members
		public override IQueryable<Report> All
		{
			get
			{
				return Repository.ObjectQuery.Include("ReportOptions").AsQueryable();
			}
		}

		public override Report Get(int uniqueId)
		{
			return All.FirstOrDefault(x => x.ReportId == uniqueId);
		}

		public override Report Get(string uniqueName)
		{
			return All.FirstOrDefault(x => x.Name == uniqueName);
		}
		#endregion Override AbstractEntityService members

		#region Report Methods
		public ReportHeaderData GetReportHeader(int phaseId)
		{
			return ReportRepository.ReportContext.GetReportHeader(phaseId);
		}

		#region Standard Report Methods

        public IList<TeamMemberReportData> GetTeamMemberReport(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<TeamMemberReportData> list = (Repository as IReportRepository).GetTeamMemberReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        public IList<CommentReportData> GetCommentReport(int phaseId, int? pageNum, int? numRows, out int total)
		{
			var output = new ObjectParameter("totalRows", typeof(int));

            IList<CommentReportData> list = (Repository as IReportRepository).GetCommentReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);
            // CR-296 Handling new line characters in comment text
            foreach (var entry in list)
            {
                entry.CommentText = CleanCommentText(entry.CommentText);
            }
            
            return list;
		}

        public IList<CommentWithCRInfoReportData> GetCommentWithCRInfoReportData(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<CommentWithCRInfoReportData> list = (Repository as IReportRepository).GetCommentWithCRInfoReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);
            // CR-296 Handling new line characters in comment text
            foreach (var entry in list)
            {
                entry.CommentText = CleanCommentText(entry.CommentText);
            }
            return list;
        }

        public IList<ConcernResponseWithAssignedToReportData> GetConcernResponseWithAssignedToReportData(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<ConcernResponseWithAssignedToReportData> list = (Repository as IReportRepository).GetConcernResponseWithAssignedToReportData(phaseId, pageNum, numRows, output);
            foreach (ConcernResponseWithAssignedToReportData crwatrd in list)
            {
                //try
                //{
                    crwatrd.ConcernText = ReplaceInvalidCharacters(crwatrd.ConcernText).Trim();
                //}
                //catch (NullReferenceException ex)
                //{
                //    Console.WriteLine(ex.GetType().FullName);
                //    Console.WriteLine(ex.Message);
                //}
                crwatrd.ResponseText = ReplaceInvalidCharacters(crwatrd.ResponseText).Trim();
            }
            int.TryParse(output.Value.ToString(), out total);           
            return list;
        }

        public IList<CommentWithAnnotationReportData> GetCommentWithAnnotationReportData(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<CommentWithAnnotationReportData> list = (Repository as IReportRepository).GetCommentWithAnnotationReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);
            // CR-296 Handling new line characters in comment text
            foreach (var entry in list)
            {
                entry.CommentText = CleanCommentText(entry.CommentText);
            }
            return list;
        }

        public IList<CommenterOrganizationTypeData> GetCommenterOrganizationTypeReport(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<CommenterOrganizationTypeData> list = (Repository as IReportRepository).GetCommenterOrganizationTypeReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

		public IList<CodingStructureReportData> GetCodingStructureReport(int phaseId)
		{
			return ReportRepository.ReportContext.GetCodingStructureReport(phaseId);
		}

        public IList<ResponseStatusReportData> GetResponseStatusReport(int phaseId, int? pageNum, int? numRows, out int total)
        {
           // return ReportRepository.ReportContext.GetResponseStatusReport(phaseId);
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<ResponseStatusReportData> list = (Repository as IReportRepository).GetResponseStatusReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        public IList<MailingListReportData> GetMailingListReport(int phaseId, int? pageNum, int? numRows, out int total)
		{
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<MailingListReportData> list = (Repository as IReportRepository).GetMailingListReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        public IList<UntimelyCommentsReportData> GetUntimelyCommentsReport(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<UntimelyCommentsReportData> list = (Repository as IReportRepository).GetUntimelyCommentsReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        public IList<ConcernResponseByCommenterReportData> GetConcernResponseByCommenterReport(int phaseId, int? option, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<ConcernResponseByCommenterReportData> list = (Repository as IReportRepository).GetConcernResponseByCommenterReport(phaseId, option, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        public IList<CommentsAndResponsesByLetterReportData> GetCommentsAndResponsesByLetterReport(int phaseId)
        {
            int total;

            return GetCommentsAndResponsesByLetterReport(phaseId, 1067, -1, 0, out total);
        }

        public IList<CommentsAndResponsesByLetterReportData> GetCommentsAndResponsesByLetterReport(int phaseId, int? letterId, int pageNum, int numRows, out int total)
        {
            var letterManager = ManagerFactory.CreateInstance<LetterManager>();
            var authorManager = ManagerFactory.CreateInstance<AuthorManager>();
            var commentManager = ManagerFactory.CreateInstance<CommentManager>();
            var responseManager = ManagerFactory.CreateInstance<ConcernResponseManager>();

            authorManager.UnitOfWork = letterManager.UnitOfWork;
            commentManager.UnitOfWork = letterManager.UnitOfWork;
            responseManager.UnitOfWork = letterManager.UnitOfWork;

            IList<CommentsAndResponsesByLetterReportData> reportData = null;

            try
            {
                var initialQuery = from l in letterManager.All
                                   join c in commentManager.All on l.LetterId equals c.LetterId
                                   join a in authorManager.All on l.LetterId equals a.LetterId
                                   where l.PhaseId == phaseId
                                      && a.Sender == true
                                   select new
                                   {
                                       LetterId = l.LetterId,
                                       CommentId = c.CommentId,
                                       AuthorLastName = a.LastName,
                                       AuthorFirstName = a.FirstName
                                   };

                if (letterId != null && letterId.HasValue)
                {
                    initialQuery = initialQuery.Where(x => x.LetterId == letterId.Value);
                }

                // Get the total number of comments in the phase
                total = initialQuery.Count();

                initialQuery = initialQuery
                        .OrderBy(x => x.AuthorLastName)
                        .ThenBy(x => x.AuthorFirstName);

                if (pageNum > 0)
                {
                    initialQuery = initialQuery
                        .Skip((pageNum - 1) * numRows)
                        .Take(numRows);
                }

                // Find the comment IDs on the requested page
                var commentIds = initialQuery
                    .Select(x => x.CommentId).ToList();

                // Build query to get all of the data associated with those comments
                var dataQuery = from l in letterManager.All
                                join c in commentManager.All on l.LetterId equals c.LetterId
                                join a in authorManager.All on l.LetterId equals a.LetterId
                                orderby c.CommentId, a.Sender descending, a.LastName, a.FirstName
                                where commentIds.Contains(c.CommentId)
                                select new
                                {
                                    Comment = c,
                                    AuthorLastName = a.LastName,
                                    AuthorFirstName = a.FirstName,
                                    IsSender = a.Sender,
                                    ResponseNumber = c.ConcernResponse.ConcernResponseNumber,
                                    ResponseText = c.ConcernResponse.ResponseText.Trim().Replace("&nbsp;","")
                                };

                // Build data structure to return for the report
                reportData = new List<CommentsAndResponsesByLetterReportData>();
                CommentsAndResponsesByLetterReportData reportRow = null;
                foreach (var entry in dataQuery)
                {
                    if (entry.IsSender)
                    {
                        //CARA-925 Moved the reportData.Add statement to after reportRow was instantiated and removed the
                        //addtional insertion that was after the for loop. -JRhoadhouse 9/24/12
                        // CR-296 Handling new line characters in comment text
                        reportRow = new CommentsAndResponsesByLetterReportData
                        {
                            SenderLast = entry.AuthorLastName,
                            SenderFirst = entry.AuthorFirstName,
                            // CR-296 Handling new line characters in comment text
                            CommentText = CleanCommentText(entry.Comment.CommentText),
                            ResponseNumber = entry.ResponseNumber,
                            ResponseText = entry.ResponseText
                        };

                        if (reportRow != null)
                        {
                            reportData.Add(reportRow);
                        }
                    }

                    reportRow.Authors.Add(string.Format("{0}, {1}", entry.AuthorLastName, entry.AuthorFirstName));
                }
            }
            finally
            {
                // Only need to dispose the letterManager, since the other managers
                // are sharing the same unit of work
                letterManager.Dispose();
            }

            if (reportData != null)
            {
                reportData = reportData.OrderBy(x => x.SenderLast).ThenBy(x => x.SenderFirst).ToList();
                foreach (CommentsAndResponsesByLetterReportData data in reportData)
                {
                    data.ResponseText = ReplaceInvalidCharacters(data.ResponseText);
                }
            }

            return reportData;
        }

        public IList<ResponseToCommentReportData> GetResponseToCommentReport(int phaseId, int option, int pageNum, int numRows, out int total)
        {
            var letterManager = ManagerFactory.CreateInstance<LetterManager>();
            var commentManager = ManagerFactory.CreateInstance<CommentManager>();
            var responseManager = ManagerFactory.CreateInstance<ConcernResponseManager>();

            commentManager.UnitOfWork = letterManager.UnitOfWork;
            responseManager.UnitOfWork = letterManager.UnitOfWork;

            IList<ResponseToCommentReportData> reportData = null;

            try
            {
                IQueryable<ResponseToCommentReportData> initialQuery;
                if (option == 1)
                {
                    // All concerns/responses not including comments
                    initialQuery = (
                        from l in letterManager.All
                        join c in commentManager.All on l.LetterId equals c.LetterId
                        join r in responseManager.All on c.ConcernResponseId equals r.ConcernResponseId
                        where l.PhaseId == phaseId
                        select new 
                        {
                            ConcernText = r.ConcernText,
                            ResponseText = r.ResponseText,
                            ResponseSequence = r.Sequence.Value,
                            ResponseNumber = r.ConcernResponseNumber,
                            ResponseId = r.ConcernResponseId
                        }
                        into data
                        group data by new
                        {
                            data.ResponseId,
                            data.ResponseSequence,
                            data.ResponseNumber,
                            data.ResponseText,
                            data.ConcernText
                        } into rollup
                        select new ResponseToCommentReportData
                        {
                            ConcernText = rollup.Key.ConcernText,
                            ResponseText = rollup.Key.ResponseText,
                            ResponseSequence = rollup.Key.ResponseSequence,
                            ResponseNumber = rollup.Key.ResponseNumber.Value,
                            ResponseId = rollup.Key.ResponseId,
                        }).Distinct();
                }
                else
                {

                    // All concerns/responses with all comments
                    initialQuery =
                        from l in letterManager.All
                        join c in commentManager.All on l.LetterId equals c.LetterId
                        join r in responseManager.All on c.ConcernResponseId equals r.ConcernResponseId
                        where l.PhaseId == phaseId
                        select new
                        {
                            LetterNumber = l.LetterSequence.Value,
                            CommentNumber = c.CommentNumber.Value,
                            CommentText = c.CommentTexts,
                            ConcernText = r.ConcernText,
                            ResponseText = r.ResponseText,
                            ResponseSequence = r.Sequence.Value,
                            ResponseNumber = r.ConcernResponseNumber.Value,
                            ResponseId = r.ConcernResponseId,
                            SampleStatement = c.SampleStatement
                        }
                            into data
                            group data by new
                            {
                                data.ResponseId,
                                data.ResponseSequence,
                                data.ResponseNumber,
                                data.ResponseText,
                                data.ConcernText
                            }
                                into rollup
                                select new ResponseToCommentReportData
                                {
                                    ConcernText = rollup.Key.ConcernText,
                                    ResponseText = rollup.Key.ResponseText,
                                    ResponseSequence = rollup.Key.ResponseSequence,
                                    ResponseNumber = rollup.Key.ResponseNumber,
                                    ResponseId = rollup.Key.ResponseId,
                                    HasSampleStatement = rollup.Any(y => y.SampleStatement),
                                    Comments = rollup
                                        .Where(x => option == 2 ? (x.SampleStatement == rollup.Any(y => y.SampleStatement)) : true)
                                        .Select(x =>
                                            new ResponseToCommentReportData.Comment
                                            {
                                                LetterNumber = x.LetterNumber,
                                                CommentNumber = x.CommentNumber,
                                                CommentText = x.CommentText,
                                                SampleStatement = x.SampleStatement
                                            })
                                        .OrderBy(x => x.LetterNumber)
                                        .ThenBy(x => x.CommentNumber)
                                };
                }

                // Get the total number of comments in the phase
                total = initialQuery.Count();
                initialQuery = initialQuery.OrderBy(x => x.ResponseSequence);

                if (pageNum > 0)
                {
                    initialQuery = initialQuery
                        .Skip((pageNum - 1) * numRows)
                        .Take(numRows);
                }

                reportData = initialQuery.ToList();
                foreach (ResponseToCommentReportData data in reportData)
                {
                    data.ConcernText = ReplaceInvalidCharacters(data.ConcernText);
                    data.ResponseText = ReplaceInvalidCharacters(data.ResponseText);
                }
            }
            finally
            {
                // Only need to dispose the letterManager, since the other managers
                // are sharing the same unit of work
                letterManager.Dispose();
            }

            return reportData;
        }

        public IList<CommentResponseNoConcernReportData> GetCommentResponseNoConcernReport(int phaseId, int pageNum, int numRows, out int total)
        {
            var letterManager = ManagerFactory.CreateInstance<LetterManager>();
            var commentManager = ManagerFactory.CreateInstance<CommentManager>();
            var responseManager = ManagerFactory.CreateInstance<ConcernResponseManager>();

            commentManager.UnitOfWork = letterManager.UnitOfWork;
            responseManager.UnitOfWork = letterManager.UnitOfWork;

            IList<CommentResponseNoConcernReportData> reportData = null;

            try
            {
                var initialQuery = from l in letterManager.All
                                   join c in commentManager.All on l.LetterId equals c.LetterId
                                   join r in responseManager.All on c.ConcernResponseId equals r.ConcernResponseId
                                   where l.PhaseId == phaseId
                                   select new CommentResponseNoConcernReportData
                                   {
                                       LetterNumber = l.LetterSequence.Value,
                                       CommentNumber = c.CommentNumber.Value,
                                       CommentText = c.CommentTexts,                                                                               
                                       ResponseNumber = r.Sequence.Value,
                                       ResponseText = r.ResponseText
                                   } ;

                // Get the total number of comments in the phase
                total = initialQuery.Count();

                initialQuery = initialQuery
                        .OrderBy(x => x.ResponseNumber);

                if (pageNum > 0)
                {
                    initialQuery = initialQuery
                        .Skip((pageNum - 1) * numRows)
                        .Take(numRows);
                }

                reportData = initialQuery.ToList();
            }
            finally
            {
                // Only need to dispose the letterManager, since the other managers
                // are sharing the same unit of work
                letterManager.Dispose();
            }

            return reportData;
        }

		#region Demographic Subreport Methods
        public IList<EarlyActionReportData> GetEarlyActionReport(int phaseId, int? pageNum, int? numRows, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<EarlyActionReportData> list = (Repository as IReportRepository).GetEarlyActionReport(phaseId, pageNum, numRows, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

		public IList<DeliveryTypeReportData> GetDeliveryTypeReport(int phaseId)
		{
			return ReportRepository.ReportContext.GetDeliveryTypeReport(phaseId);
		}

		public IList<OrganizationTypeReportData> GetOrganizationTypeReport(int phaseId)
		{
			return ReportRepository.ReportContext.GetOrganizationTypeReport(phaseId);
		}

		public IList<LetterTypeReportData> GetLetterTypeReport(int phaseId)
		{
			return ReportRepository.ReportContext.GetLetterTypeReport(phaseId);
		}

		public IList<OrganizedLetterCampaignReportData> GetOrganizedLetterCampaignReport(int phaseId)
		{
			return ReportRepository.ReportContext.GetOrganizedLetterCampaignReport(phaseId);
		}

		public IList<RespondingOrgReportData> GetRespondingOrgReport(int phaseId)
		{
			return ReportRepository.ReportContext.GetRespondingOrgReport(phaseId);
		}
		#endregion Demographic Subreport Methods

		#endregion Standard Report Methods

        public static string CleanCommentText(string CommentText)
        {
            string retVal = CommentText.Replace("&#x0D;", "\n").TrimStart('\n', '\r').Trim();
            retVal = retVal.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&");              
                
            return retVal;
        }

        public static string ReplaceInvalidCharacters(string text)
        {
            string replacedText = txtReplacementConfig.ReplaceInvalidCharacters(text);

            return replacedText;
        }
        
		#endregion Report Methods
	}
}
