﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// catergories for custom report fields
    /// </summary>
    public static class CustomReportFieldType
    {
        public const string All = "All";
        public const string Commenter = "Commenter";
        public const string Letter = "Letter";
        public const string Comment = "Comment";
        public const string Project = "Project";
        public const string CommentPeriod = "CommentPeriod";

        public static int GetListIdforFieldType(string fieldType)
        {
            switch (fieldType)
            {
                    //-1 is for the Search Parameters node
                case Project:
                case CommentPeriod:
                    return -2;
                case Commenter:
                    return -3;
                case Letter:
                    return -4;
                    //other values are nto used so return 0
                default:
                    return 0;
            }
        }
    }
}