﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class CodingTemplateManager : AbstractEntityManager<CodingTemplate>
	{
		public CodingTemplateManager(ICodingTemplateRepository repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a coding template with the given coding template ID
		/// </summary>
        /// <param name="name">The ID of the coding template</param>
		/// <returns>CodingTemplate or null</returns>
		public override CodingTemplate Get(int uniqueId)
		{
			return All.Where(x => x.CodingTemplateId == uniqueId).FirstOrDefault();
		}

		public override CodingTemplate Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

        #region Custom Methods
        /// <summary>
        /// Returns a list of template codes with the given coding template ID
        /// </summary>
        /// <param name="codingTemplateId">The ID of the coding template</param>
        /// <param name="templateName">Output of the template name</param>
        /// <returns>CodingTemplate or null</returns>
        public IList<TemplateCodeSelect> GetTemplateCodes(int codingTemplateId, out string templateName)
        {
            var output = new ObjectParameter("templateName", typeof(string));

            IList<TemplateCodeSelect> list = (Repository as ICodingTemplateRepository).GetTemplateCodeSelects(codingTemplateId, output);
            templateName = output.Value.ToString();
            
            return list;
        }

        /// <summary>
        /// Update an existing coding template
        /// </summary>
        /// <param name="codingTemplateId">The ID of the coding template</param>
        /// <param name="templateName">The Name of the coding template</param>
        /// <param name="codeIds">List of code IDs</param>
        public void UpdateCodingTemplate(int codingTemplateId, string templateName, string codeIds)
        {
            (Repository as ICodingTemplateRepository).UpdateCodingTemplate(codingTemplateId, templateName, codeIds);
        }
        #endregion
    }
}
