﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using log4net;


namespace Aquilent.Cara.Domain
{
    public class TaskItem
    {
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectNumber { get; set; }
        public int PhaseId { get; set; }
        public string PhaseName { get; set; }
        public int CommentCount { get; set; }
        public int ConcernCount { get; set; }
        public int EarlyAttentionCount { get; set; }
        public int UnpublishedLetterCount { get; set; }
    }
}