﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    class CustomReportLogManager: AbstractEntityManager<CustomReportLog>
	{
		#region Constructor
        public CustomReportLogManager(IRepository<CustomReportLog> repository)
			: base(repository)
		{
		}
		#endregion Constructor
        
		#region Override AbstractEntityService members
        public override CustomReportLog Get(int uniqueId)
        {
            throw new NotImplementedException();
		}

        public override CustomReportLog Get(string uniqueName)
		{
            throw new NotImplementedException();
		}
		#endregion Override AbstractEntityService members        
    }
}
