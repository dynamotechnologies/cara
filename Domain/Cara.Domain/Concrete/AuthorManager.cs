﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using log4net;

namespace Aquilent.Cara.Domain
{
	public class AuthorManager : AbstractEntityManager<Author>
    {
        private ILog _logger = LogManager.GetLogger(typeof(AuthorManager));

		public AuthorManager(IRepository<Author> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given author ID
		/// </summary>
        /// <param name="name">The ID of the author</param>
		/// <returns>Author or null</returns>
		public override Author Get(int uniqueId)
		{
			return All.Where(x => x.AuthorId == uniqueId).FirstOrDefault();
		}

		public override Author Get(string uniqueName)
		{
            throw new NotImplementedException();
		}

        /// <summary>
        /// Deletes an existing author
        /// </summary>
        /// <param name="entity"></param>
        public override void Delete(Author entity)
        {
            try
            {
                // delete the author
                base.Delete(entity);
            }
            catch (Exception e)
            {
                _logger.Debug("An error occurred deleting an author", e);
                throw new DomainException("An error occurred deleting an author", e);
            }
        }
		#endregion Override AbstractEntityService members
	}
}
