﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using Aquilent.Cara.Domain;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.SQS;
using Amazon;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.Aws.Sqs;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.Cara.Reference;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Framework.Documents;

namespace Aquilent.Cara.Domain
{
    public class LetterObjectionQueueServiceTask : IServiceTask
	{
		#region Member Variables
        private ILog _logger = LogManager.GetLogger(typeof(LetterObjectionQueueServiceTask));
		private static readonly string queueNameKey = "cara.queue.letterobjection";
        private static readonly int minimumHoursOld = int.Parse(ConfigurationManager.AppSettings["minimumAgeInHours"]);
		#endregion Member Variables

		#region Properties
		public string Name
		{
			get { return "Letter Objection Queue"; }
		}
		#endregion Properties

        #region IServiceTask Methods
        public void RunServiceTask()
        {
            ProcessLetterObjectionQueue();
        }
        #endregion IServiceTask Methods

        private void ProcessLetterObjectionQueue()
        {
            using (var loManager = ManagerFactory.CreateInstance<LetterObjectionManager>())
            {
                var dmManager = new DataMartManager();
                try
                {
                    // Create a reference to the queue
                    string queueName;
                    var sqs = SqsHelper.GetQueue(queueNameKey, out queueName);

                    var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                    var createQueueResponse = sqs.CreateQueue(sqsRequest);
                    string queueUrl = createQueueResponse.QueueUrl;

                    bool datamartObjectionResult = false;
                    bool removeObjection = false;
                    int numberOfMessages = 0;
                    do
                    {
                        IList<int> idList;
                        List<DeleteMessageBatchRequestEntry> dmbrEntries = new List<DeleteMessageBatchRequestEntry>(numberOfMessages);
                        var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = 10 };
                        var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                        numberOfMessages = 0;

                        if (receiveMessageResponse.Messages != null)
                        {
                            numberOfMessages = receiveMessageResponse.Messages.Count;
                            idList = new List<int>(numberOfMessages);
                            foreach (var message in receiveMessageResponse.Messages)
                            {
                                idList.Add(int.Parse(message.Body));
                                dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                            }

                            using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                            {
                                using (var docManager = ManagerFactory.CreateInstance<DocumentManager>())
                                {
                                    // Get all of the custom reports that are queued up
                                    var queuedLetterObjections = loManager.All.Where(x => idList.Contains(x.LetterObjectionId)).ToList();
                                    var lookupCommentRule = LookupManager.GetLookup<LookupCommentRule>();
                                    var dmdManager = new DmdManager();
                                    foreach (var qlo in queuedLetterObjections)
                                    {
                                        // Check the last updated date
                                        if (qlo.LastUpdated.AddHours(minimumHoursOld) <= DateTime.UtcNow)
                                        {
                                            var entity = letterManager.All.Where(x => x.LetterObjectionId == qlo.LetterObjectionId)
                                                .Select(x => new
                                                {
                                                    Letter = x,
                                                    Author = x.Authors.FirstOrDefault(y => y.Sender),
                                                    Phase = x.Phase,
                                                    Project = x.Phase.Project
                                                }).First();

                                            // Get the Datamart record
                                            Objection objection = dmManager.GetObjection(qlo.LetterObjectionId);

                                            bool noPdf = false;
                                            string dmdId = null;
                                            var docIds = qlo.LetterObjectionDocuments.Select(x => x.DocumentId).ToList();
                                            if (docIds.Count > 1)
                                            {
                                                // If more than one attachment, merge them
                                                var documents = docManager.All.Where(x => docIds.Contains(x.DocumentId));

                                                try
                                                {
                                                    dmdId = dmdManager.MergeObjectionAttachments(
                                                        qlo,
                                                        entity.Letter,
                                                        entity.Author.FullNameLastFirst,
                                                        entity.Project.ProjectNumber,
                                                        lookupCommentRule[entity.Project.CommentRuleId.Value].Name,
                                                        documents.Select(x => x.DmdId).ToList());
                                                    
                                                    if (dmdId == null)
                                                    {
                                                        _logger.Warn("Merge Objection Attachments did not return a valid DMD ID");
                                                        noPdf = true;
                                                    }
                                                }
                                                catch (NoPdfExistsException npee)
                                                {
                                                    _logger.Warn(npee.Message);
                                                    noPdf = true;
                                                }
                                            }
                                            else if (docIds.Count == 1)
                                            {
                                                // If only one document, use the document
                                                dmdId = docManager.Get(docIds[0]).DmdId;
                                                var fileMetadata = dmdManager.GetFileMetadata(dmdId, "PDF");
                                                if (fileMetadata == null)
                                                {
                                                    _logger.Warn(string.Format("The PDF for DMD ID {0} is not ready.", dmdId));
                                                    noPdf = true;
                                                }
                                            }
                                            else
                                            {
                                                // If no documents, remove the objection record from the Datamart
                                                removeObjection = true;
                                                noPdf = true;
                                            }

                                            if (noPdf)
                                            {
                                                datamartObjectionResult = false;
                                            }
                                            else
                                            {
                                                // Update the LetterObjection objection with the new merged DmdId
                                                qlo.DmdId = dmdId;

                                                string commentRuleName = "Unknown";
                                                if (entity.Project.CommentRuleId.HasValue)
                                                {
                                                    var commentRule = lookupCommentRule[entity.Project.CommentRuleId.Value];

                                                    if (commentRuleName != null)
                                                    {
                                                        commentRuleName = commentRule.Name;
                                                    }
                                                }

                                                if (entity.Phase.ObjectionResponseExtendedBy != null && entity.Phase.ObjectionResponseExtendedBy > 0)
                                                {
                                                    var project = entity.Phase.Project;
                                                    var objectionDueDays = 0;

                                                    switch (project.CommentRuleId)
                                                    {
                                                        case 13: /* 219 (2012) */
                                                            objectionDueDays = 90;
                                                            break;
                                                        case 14: /* 218 (2013) HFRA */
                                                            objectionDueDays = 30;
                                                            break;
                                                        case 15: /* 218 (2013) Non-HFRA */
                                                            objectionDueDays = 45;
                                                            break;
                                                        default:
                                                            objectionDueDays = 30;
                                                            break;
                                                    }

                                                    var objectionWeekendCheck = entity.Phase.CommentEnd.AddDays(objectionDueDays).DayOfWeek;

                                                    if (objectionWeekendCheck == DayOfWeek.Sunday)
                                                    {
                                                        entity.Phase.ObjectionResponseExtendedBy = entity.Phase.ObjectionResponseExtendedBy - 1;
                                                    }

                                                    else if (objectionWeekendCheck == DayOfWeek.Saturday)
                                                    {
                                                        entity.Phase.ObjectionResponseExtendedBy = entity.Phase.ObjectionResponseExtendedBy - 2;
                                                    }
                                                }

                                                // Add/Update the Datamart record
                                                var updatedObjection = new Objection
                                                {
                                                    ObjectionId = qlo.LetterObjectionId,
                                                    Objector = entity.Author.FullNameLastFirst,
                                                    PalsId = int.Parse(entity.Project.ProjectNumber),
                                                    ProjectType = "nepa",
                                                    ResponseDate = qlo.ResponseDate.Value.ToString("yyyy-MM-dd"),
                                                    CommentRule = commentRuleName,
                                                    DmdId = dmdId,
                                                    Status = entity.Letter.LetterStatusName,
                                                    Outcome = qlo.OutcomeName,
                                                    StartDate = entity.Phase.CommentStart.ToString("yyyy-MM-dd"),
                                                    EndDate = entity.Phase.CommentEnd.ToString("yyyy-MM-dd"),
                                                    DueDate = entity.Phase.ObjectionResponseDue.Value.ToString("yyyy-MM-dd"),
                                                    ExtendedDueDate = entity.Phase.ObjectionResponseDue.Value
                                                        .AddDays(entity.Phase.ObjectionResponseExtendedBy.HasValue ? entity.Phase.ObjectionResponseExtendedBy.Value : 0)
                                                        .ToString("yyyy-MM-dd"),
                                                    Title = entity.Author.FullNameLastFirst
                                                };

                                                // Remove the existing objection attachment and update
                                                if (objection != null)
                                                {
                                                    dmdManager.DeleteFile(objection.DmdId);
                                                    datamartObjectionResult = dmManager.UpdateObjection(updatedObjection);
                                                }
                                                else
                                                {
                                                    datamartObjectionResult = dmManager.AddObjection(updatedObjection);
                                                }

                                                // Publish the file
                                                dmdManager.PublishFile(dmdId);
                                            }
                                        }

                                        // Remove the entries from the queue
                                        var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrEntries };
                                        sqs.DeleteMessageBatch(deleteRequest);

                                        if (!datamartObjectionResult && !removeObjection)
                                        {
                                            // Re-add the LetterObjection to the queue
                                            var sendMessageRequest = new SendMessageRequest
                                                {
                                                    QueueUrl = queueUrl,
                                                    MessageBody = qlo.LetterObjectionId.ToString()
                                                };
                                            sqs.SendMessage(sendMessageRequest);
                                        }
                                        else if (removeObjection)
                                        {
                                            dmManager.DeleteObjection(qlo.LetterObjectionId);
                                        }
                                    }
                                }
                            }
                        }
                    } while (numberOfMessages > 0);
                }
                finally
                {
                    loManager.UnitOfWork.Save();
                    dmManager.Dispose();
                }
            }
        }
	}
}
