﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;

namespace Aquilent.Cara.Domain
{
    public class HarmfulContentNotificationServiceTask : IServiceTask
	{
		#region Member Variables
		private ILog _logger = LogManager.GetLogger(typeof(PhaseStartingServiceTask));
		private const string _lastRunSettingName = "cara.sys.harmfulcontent.lastrun";
		#endregion Member Variables

		#region Properties
		public string Name
		{
			get { return "Harmful Content Notification"; }
		}

		/// <summary>
		/// Encapsulates the cara.sys.phasestart.lastrun setting
		/// </summary>
        private CaraSetting LastRunSetting(CaraSettingManager settingsManager)
        {
            CaraSetting lastRun;

            lastRun = settingsManager.Get(_lastRunSettingName);
            if (lastRun == null)
            {
                lastRun = new CaraSetting
                {
                    Name = _lastRunSettingName,
                    Value = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss"), // Forces the service to run the first time
                    Description = "The date the harmful content notification service was last run"
                };

                settingsManager.Add(lastRun);
            }

            return lastRun;
        }
		#endregion Properties

		#region IServiceTask Methods
        public void RunServiceTask()
        {
            using (var settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                var now = DateTime.UtcNow;
                var lastRun = LastRunSetting(settingsManager);
                var lastRunDate = DateTime.Parse(lastRun.Value);

                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    // Get letters that weren't published to the reading room
                    var harmfulLettersByPhase = letterManager.All
                        .Where(x => x.DateEntered >= lastRunDate)
                        .Where(x => !x.PublishToReadingRoom)
                        .GroupBy(x => x.PhaseId)
                        .ToList();

                    foreach (var phaseGroup in harmfulLettersByPhase)
                    {
                        using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            // Get all of the relevant phase information
                            var phaseInfo = phaseManager
                                .All
                                .Where(x => x.PhaseId == phaseGroup.Key)
                                .Select(x => new { Phase = x, Project = x.Project, TeamMembers = x.PhaseMemberRoles })
                                .First();

                            if (phaseInfo.Phase.ReadingRoomActive)
                            {
                                // Get the list of letter numbers to include in the e-mail
                                var letterNumbers = phaseGroup.Select(x => x.LetterSequence).ToList();

                                _logger.Info(string.Format("From HarmfulContentNotificationServiceTask to NotificationManager: phaseId - {0}", phaseInfo.Phase.PhaseId));
                                _logger.Info(string.Format("From HarmfulContentNotificationServiceTask to NotificationManager: phaseReadingRoomActive - {0}", phaseInfo.Phase.ReadingRoomActive));
                                NotificationManager.SendHarmfulContentNotification(
                                    phaseInfo.Phase, phaseInfo.Project,
                                    phaseInfo.TeamMembers, letterNumbers);
                            }
                        }
                    }
                }

                // Record the last time it ran
                lastRun.Value = now.ToString("yyyy-MM-dd HH:mm:ss");
                settingsManager.UnitOfWork.Save();
            }
        }
		#endregion IServiceTask Methods
	}
}
