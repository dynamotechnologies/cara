﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;

namespace Aquilent.Cara.Domain
{
    class TaskAssignmentNotificationServiceTask : IServiceTask
    {
        #region Member Variables
        private ILog _logger = LogManager.GetLogger(typeof(PhaseStartingServiceTask));
        private const string _lastRunSettingName = "cara.sys.taskassignment.lastrun";
        #endregion Member Variables

        #region Properties
        public string Name
        {
            get { return "Task Assignment Notification"; }
        }
        #endregion //properties

        /// <summary>
        /// Encapsulates the cara.sys.phasestart.lastrun setting
        /// </summary>
        private CaraSetting LastRunSetting(CaraSettingManager settingsManager)
        {
            CaraSetting lastRun;
            lastRun = settingsManager.Get(_lastRunSettingName);
            if (lastRun == null)
            {
                lastRun = new CaraSetting
                {
                    Name = _lastRunSettingName,
                    Value = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd"),
                    Description = "The date the Task Assignment Notification service was last run"
                };

                settingsManager.Add(lastRun);
            }

            return lastRun;
        }

        #region IServiceTask Methods
        public void RunServiceTask()
        {
            CaraSetting lastRun = null;
            var today = DateTime.Today;

            using (var settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                try
                {
                    lastRun = LastRunSetting(settingsManager);

                    // No need to perform the work more than once/day
                    if (lastRun.Value != today.ToString("yyyy-MM-dd"))
                    {
                        //Get list of all concerns and comments that were created since the last time notifications were sent
                        List<Comment> comments = null;
                        List<ConcernResponse> concerns = null;
                        DateTime dateLastRun = DateTime.Parse(lastRun.Value);
                        using (CommentManager commentManager = ManagerFactory.CreateInstance<CommentManager>())
                        {
                            comments = commentManager.All.Where
                                (x => x.UserId != null &&
                                    x.DateAssigned != null &&
                                    dateLastRun < x.DateAssigned.Value)
                                .ToList();
                            ConcernResponseManager concernResponseManager
                                = ManagerFactory.CreateInstance<ConcernResponseManager>();
                            concernResponseManager.UnitOfWork = commentManager.UnitOfWork;

                            concerns = concernResponseManager.All.Where
                                (x => x.UserId != null &&
                                    x.DateAssigned != null &&
                                    dateLastRun < x.DateAssigned.Value)
                                .ToList();

                            NotificationManager.SendTaskAssignmentNotifications(comments, concerns);
                        }
                    }
                }
                catch (FormatException fe)
                {
                    _logger.Fatal("An error occurred fetching the setting for 'cara.sys.phaseend.threshold'", fe);
                }
                finally
                {
                    if (lastRun != null)
                    {
                        // Record the last time it ran
                        lastRun.Value = today.ToString("yyyy-MM-dd");
                        settingsManager.UnitOfWork.Save();
                    }
                }
            }
        }
        #endregion IServiceTask Methods
    }
}

