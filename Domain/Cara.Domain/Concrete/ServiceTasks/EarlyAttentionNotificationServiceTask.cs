﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;

namespace Aquilent.Cara.Domain
{
    public class EarlyAttentionNotificationServiceTask : IServiceTask
    {
        #region Member Variables
        private ILog _logger = LogManager.GetLogger(typeof(PhaseStartingServiceTask));

        private const string _lastRunSettingName = "cara.sys.earlyattention.lastrun";
        #endregion Memter Variables

        #region Properties
        public string Name
        {
            get { return "Early Attention Notification"; }
        }

        /// <summary>
        /// Encapsulates the cara.sys.earlyattention.lastrun setting                                                                      
        /// </summary>
        private CaraSetting LastRunSetting(CaraSettingManager settingsManager)
        {
            CaraSetting lastRun;

            lastRun = settingsManager.Get(_lastRunSettingName);
            if (lastRun == null)
            {
                lastRun = new CaraSetting
                {
                    Name = _lastRunSettingName,
                    Value = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss"), // Forces the service to run the first time
                    //                    Description = "The date the early attention notification service was last run"
                    Description = "The date the early attention notification service was last run"
                };

                settingsManager.Add(lastRun);
            }

            return lastRun;
        }
        #endregion Properties

        #region IServiceTask Methods
        public void RunServiceTask()
        {
            using (var settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                var now = DateTime.UtcNow;
                var lastRun = LastRunSetting(settingsManager);
                var lastRunDate = DateTime.Parse(lastRun.Value);

                using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                {
                    // EarlyActionStatusId == 2 means it is auto marked for Early Attention
                    var earlyAttentionLettersByPhase = letterManager.All
                        .Where(x => x.DateEntered >= lastRunDate)
                        .Where(x => x.EarlyActionStatusId == 2)
                        .GroupBy(x => x.PhaseId)
                        .ToList();

                    foreach (var phaseGroup in earlyAttentionLettersByPhase)
                    {
                        using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            // Get all of the relevant phase information
                            var phaseInfo = phaseManager
                                .All
                                .Where(x => x.PhaseId == phaseGroup.Key)
                                .Select(x => new { Phase = x, Project = x.Project })
                                .First();

                            if (phaseInfo.Phase.ReadingRoomActive)
                            {
                                // Get the list of letter numbers to include in the e-mail
                                var letterNumbers = phaseGroup.Select(x => x.LetterSequence).ToList();

                                _logger.Info(string.Format("From EarlyAttentionNotificationServiceTask to NotificationManager: phaseId - {0}", phaseInfo.Phase.PhaseId));
                                _logger.Info(string.Format("From EarlyAttentionNotificationServiceTask to NotificationManager: phaseReadingRoomActive - {0}", phaseInfo.Phase.ReadingRoomActive));
                                NotificationManager.SendEarlyAttentionNotification(
                                    phaseInfo.Phase, phaseInfo.Project, letterNumbers);
                            }
                        }
                    }
                }

                // Record the last time it ran
                lastRun.Value = now.ToString("yyyy-MM-dd HH:mm:ss");
                settingsManager.UnitOfWork.Save();
            }
        }
        #endregion IServiceTask Methods
    }
}