﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;

namespace Aquilent.Cara.Domain
{
	public class PhaseStartingServiceTask : IServiceTask
	{
		#region Member Variables
		private ILog _logger = LogManager.GetLogger(typeof(PhaseStartingServiceTask));
		private const string _lastRunSettingName = "cara.sys.phasestart.lastrun";
		#endregion Member Variables

		#region Properties
		public string Name
		{
			get { return "Phase Starting Notification"; }
		}

		/// <summary>
		/// Encapsulates the cara.sys.phasestart.lastrun setting
		/// </summary>
        private CaraSetting LastRunSetting(CaraSettingManager settingsManager)
        {
            CaraSetting lastRun;

            lastRun = settingsManager.Get(_lastRunSettingName);
            if (lastRun == null)
            {
                lastRun = new CaraSetting
                {
                    Name = _lastRunSettingName,
                    Value = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd"), // Forces the service to run the first time
                    Description = "The date the phaseend service was last run"
                };

                settingsManager.Add(lastRun);
            }

            return lastRun;
        }
		#endregion Properties

		#region IServiceTask Methods
		public void RunServiceTask()
		{
            using (var settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
            {
                var today = DateTime.Today;
                var lastRun = LastRunSetting(settingsManager);

                // No need to perform the work more than once/day
                if (lastRun.Value != today.ToString("yyyy-MM-dd"))
                {
                    using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                    {
                        var phasesStarting = phaseManager.Find(x => x.CommentStart.ToString("yyyy-MM-dd") == today.ToString("yyyy-MM-dd"));

                        NotificationManager.SendPhaseStartingNotifications(phasesStarting);
                    }

                    // Record the last time it ran
                    lastRun.Value = today.ToString("yyyy-MM-dd");
                    settingsManager.UnitOfWork.Save();
                }
            }
		}
		#endregion IServiceTask Methods
	}
}
