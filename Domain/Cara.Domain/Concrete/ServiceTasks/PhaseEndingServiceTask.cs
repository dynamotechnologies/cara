﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;

namespace Aquilent.Cara.Domain
{
	public class PhaseEndingServiceTask : IServiceTask
	{
		#region Member Variables
		private ILog _logger = LogManager.GetLogger(typeof(PhaseStartingServiceTask));
		private const string _lastRunSettingName = "cara.sys.phaseend.lastrun";
		private const string _thresholdSettingName = "cara.sys.phaseend.threshold";
		private const int _thresholdDefault = 5;
		#endregion Member Variables

		#region Properties
		public string Name
		{
			get { return "Phase Ending Notification"; }
		}

		/// <summary>
		/// Encapsulates the cara.sys.phasestart.lastrun setting
		/// </summary>
        private CaraSetting LastRunSetting(CaraSettingManager settingsManager)
        {
            CaraSetting lastRun;
            lastRun = settingsManager.Get(_lastRunSettingName);
            if (lastRun == null)
            {
                lastRun = new CaraSetting
                {
                    Name = _lastRunSettingName,
                    Value = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd"),
                    Description = "The date the phaseend service was last run"
                };

                settingsManager.Add(lastRun);
            }

            return lastRun;
        }

		/// <summary>
		/// Encapsulates the cara.sys.phasestart.threshold setting
		/// </summary>
        private CaraSetting ThresholdSetting(CaraSettingManager settingsManager)
        {
            CaraSetting threshold;
            threshold = settingsManager.Get(_thresholdSettingName);
            if (threshold == null)
            {
                threshold = new CaraSetting
                {
                    Name = _thresholdSettingName,
                    Value = _thresholdDefault.ToString(),
                    Description = "Days prior to a phase ending to notify team members"
                };

                settingsManager.Add(threshold);
            }

            return threshold;
        }
		#endregion Properties

		#region IServiceTask Methods
		public void RunServiceTask()
		{
			try
			{
                using (var settingsManager = ManagerFactory.CreateInstance<CaraSettingManager>())
                {
                    var threshold = ThresholdSetting(settingsManager);
                    var lastRun = LastRunSetting(settingsManager);
                    var today = DateTime.Today;

                    // No need to perform the work more than once/day
                    if (lastRun.Value != today.ToString("yyyy-MM-dd"))
                    {
                        var phaseEndThreshold = Convert.ToInt32(threshold.Value);
                        using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
                        {
                            var phasesEndingSoon = phaseManager.Find(x => x.CommentEnd.AddDays(-1 * phaseEndThreshold).ToString("yyyy-MM-dd") == today.ToString("yyyy-MM-dd"));

                            // Send the messages
                            NotificationManager.SendPhaseEndingSoonNotifications(phasesEndingSoon);
                        }

                        // Record the last time it ran
                        lastRun.Value = today.ToString("yyyy-MM-dd");
                        settingsManager.UnitOfWork.Save();
                    }
                }
			}
			catch (FormatException fe)
			{
				_logger.Fatal("An error occurred fetching the setting for 'cara.sys.phaseend.threshold'", fe);
			}
		}
		#endregion IServiceTask Methods
	}
}
