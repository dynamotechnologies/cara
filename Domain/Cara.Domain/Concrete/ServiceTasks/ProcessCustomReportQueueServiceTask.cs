﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Services;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Framework.Resource;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using Aquilent.Cara.Domain;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.SQS;
using Amazon;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.Aws.Sqs;

namespace Aquilent.Cara.Domain.Report.Personal
{
    public class ProcessCustomReportQueueServiceTask : IServiceTask
	{
		#region Member Variables
        private ILog _logger = LogManager.GetLogger(typeof(ProcessCustomReportQueueServiceTask));
        private static string csvFolder = ConfigurationManager.AppSettings["customReportFolder"];
		private static readonly string queueNameKey = "cara.queue.customreportexport";
		#endregion Member Variables

		#region Properties
		public string Name
		{
			get { return "Custom Report Queue"; }
		}
		#endregion Properties

        #region Static Constructor
        static ProcessCustomReportQueueServiceTask()
        {
            using (var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>())
            {
                var setting = resourceManager.Get("resource.customreport.folder");

                if (setting == null)
                {
                    throw new ApplicationException("Missing resource in the database:  resource.customreport.folder");
                }

                csvFolder = setting.Value;
            }
        }
        #endregion Static Constructor

        #region IServiceTask Methods
        public void RunServiceTask()
        {
            ProcessCustomReportQueue();

            // This is unnecessary when using S3 because the defined
            // Lifecycle Policy will remove files automatically
            //RemoveExpiredCustomReportFiles();
        }
        #endregion IServiceTask Methods

        private void ProcessCustomReportQueue()
        {
            using (var crqManager = ManagerFactory.CreateInstance<CustomReportQueueManager>())
            {
                try
                {
                    // Create a reference to the queue
                    string queueName;
                    var sqs = SqsHelper.GetQueue(queueNameKey, out queueName);

                    var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                    var createQueueResponse = sqs.CreateQueue(sqsRequest);
                    string queueUrl = createQueueResponse.QueueUrl;

                    int numberOfMessages = 0;
                    do
                    {
                        IList<int> idList;
                        List<DeleteMessageBatchRequestEntry> dmbrEntries = new List<DeleteMessageBatchRequestEntry>(numberOfMessages);
                        var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = queueUrl, MaxNumberOfMessages = 10 };
                        var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                        numberOfMessages = 0;

                        if (receiveMessageResponse.Messages != null)
                        {
                            numberOfMessages = receiveMessageResponse.Messages.Count;
                            idList = new List<int>(numberOfMessages);
                            foreach (var message in receiveMessageResponse.Messages)
                            {
                                idList.Add(int.Parse(message.Body));
                                dmbrEntries.Add(new DeleteMessageBatchRequestEntry { Id = message.MessageId, ReceiptHandle = message.ReceiptHandle });
                            }

                            // Get all of the custom reports that are queued up
                            var queuedCustomReports = crqManager.All.Where(x => idList.Contains(x.CustomReportQueueId)).ToList();

                            // Get all of the users at once to limit calls to the database
                            IList<User> users = null;
                            if (queuedCustomReports.Count > 0)
                            {
                                using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                                {
                                    var userIds = queuedCustomReports.Select(x => x.UserId).ToList();
                                    users = userManager.All.Where(u => userIds.Contains(u.UserId)).ToList();
                                }
                            }

                            foreach (var qcr in queuedCustomReports)
                            {
                                // Get the user who queued the report
                                var user = users.Single(u => u.UserId == qcr.UserId);

                                // Run the report
                                // (CARA-1052) 
                                string provider = ConfigurationManager.ConnectionStrings["CaraCustomReportConnection"].ProviderName;
                                string constring = ConfigurationManager.ConnectionStrings["CaraCustomReportConnection"].ConnectionString;
                                int userId = user.UserId;
                                List<CustomReportData> retVal = null;
                                using (CustomReportManager reportManager = new Domain.Report.CustomReportManager())
                                {
                                    reportManager.Open(provider, constring);
                                    int total;
                                    retVal = reportManager.RunReport(qcr.ReportDefinition, out total, userId, 0, 0, 0, true);
                                    reportManager.Close();
                                }

                                // Build & Save the CSV file
                                string filename = SaveAsCsvFile(retVal, user.Username);

                                if (filename != null)
                                {
                                    string reportSummary = GetCustomReportSummary(qcr.ReportDefinition);

                                    // Send the e-mail
                                    NotificationManager.SendCustomReportCsvNotification(user, reportSummary, filename);

                                    // Remove the report from the queue
                                    crqManager.Delete(qcr);

                                    // Remove the entries from the queue
                                    var deleteRequest = new DeleteMessageBatchRequest { QueueUrl = queueUrl, Entries = dmbrEntries };
                                    sqs.DeleteMessageBatch(deleteRequest);
                                }
                            }
                        }
                    } while (numberOfMessages > 0);
                }
                catch (FormatException fe)
                {
                    _logger.Fatal("An error occurred fetching the setting for 'cara.sys.phaseend.threshold'", fe);
                }
                finally
                {
                    crqManager.UnitOfWork.Save();
                }
            }
        }

        //Generates Custom Report Parameter summary for e-mail body
        //string xml: the xml formated parameters (non-encoded)
        private string GetCustomReportSummary(string xml)
        {
            try
            {
                CustomReportParameters parameters = new CustomReportParameters(xml, false);
                return String.Format("Search Parameters\n{0}\n{1}\n{2}\nCustom Report Presentation\n{3}",
                    GetSummaryForFieldType(CustomReportFieldType.Project, parameters),
                    GetSummaryForFieldType(CustomReportFieldType.Commenter, parameters),
                    GetSummaryForFieldType(CustomReportFieldType.Letter, parameters),
                    GetSummaryForColumns(parameters));
            }
            catch (Exception ex)
            {
                _logger.Error("An error occurred while generating summary for the follwing parameters: " + xml, ex);
                return "An error occurred while generating summary.";
            }
        }

        private string GetSummaryForFieldType(string fieldType, CustomReportParameters parameters)
        {
            string retVal = "\t" + fieldType;
            List<CustomReportDataItem> relevantDataItems = parameters.DataItems
                    .Where(x => x.FieldType == fieldType || (x.FieldType == CustomReportFieldType.CommentPeriod && fieldType == CustomReportFieldType.Project))
                    .OrderBy(x => x.FieldName)
                    .ToList();
            if (!relevantDataItems.Any())
            { return retVal + " (All)"; }
            foreach (CustomReportDataItem dataItem in relevantDataItems)
            {
                retVal += "\n\t\t" + dataItem.FieldName;
                foreach (CustomReportValueItem valueItem in dataItem.Values)
                {
                    #region construct value text
                    string text = "";
                    if (valueItem.Not)
                    { text = "excluding "; }
                    switch (dataItem.FieldDataType)
                    {
                        case "date":
                            if (valueItem.DateFromValue == DateTime.MinValue)
                            { text = String.Format("{0}Before {1}", text, valueItem.DateToValue.ToShortDateString()); }
                            else if (valueItem.DateToValue == DateTime.MaxValue)
                            { text = String.Format("{0}After {1}", text, valueItem.DateFromValue.ToShortDateString()); }
                            else
                            {
                                text = String.Format("{0}Between {1} and {2}",
                                    text,
                                    valueItem.DateFromValue.ToShortDateString(),
                                    valueItem.DateToValue.ToShortDateString());
                            }
                            break;
                        case "dropdown":
                        case "treedropdown":
                            text += valueItem.Name;
                            break;
                        case "string":
                        case "number":
                            text += valueItem.Value;
                            break;
                        default:
                            text += valueItem.Value;
                            break;
                    }
                    #endregion
                    retVal += "\n\t\t\t" + text;
                }
            }
            return retVal;
        }
        private string GetSummaryForColumns(CustomReportParameters parameters)
        {
            string retVal = "\tColumns";
            parameters.ColumnDataItems.ForEach(x => retVal += "\n\t\t" + x.ColumnName);
            retVal += "\n\tSort Order";
            parameters.ColumnDataItems.Where(x => x.SortOrder > 0).OrderBy(x => x.SortOrder).ToList()
                .ForEach(x => retVal += String.Format("\n\t\t{0} {1}", x.ColumnName, x.Ascending ? "ASC" : "DESC"));
            return retVal;
        }

        private void RemoveExpiredCustomReportFiles()
        {
            int maxAge = int.Parse(ConfigurationManager.AppSettings["customReportMaxAgeInDays"]);
            TimeSpan now = new TimeSpan(DateTime.Now.Ticks);

            string[] csvFiles = Directory.GetFiles(csvFolder);
            foreach (var csvFile in csvFiles)
            {
                TimeSpan creationTime = new TimeSpan(File.GetCreationTime(csvFile).Ticks);

                if (now.Subtract(creationTime).Days > maxAge)
                {
                    try
                    {
                        File.Delete(csvFile);
                    }
                    catch (Exception ex)
                    {
                        _logger.Warn(string.Format("An error occurred trying to delete {0}", csvFile), ex);
                    }
                }
            }
        }

        /// <summary>
        /// Transforms the results into a CSV file
        /// </summary>
        /// <returns>Filename of the CSV File</returns>
        private string SaveAsCsvFile(List<CustomReportData> reportResults, string username) 
        {
            // Write to a file
            //    Format:  yyyymmddhhmm_username_xxx.csv
            string filename = string.Format("{0:yyyyMMddhhmm}_{1}_{2}.csv", DateTime.Now, username, new Random().Next(100, 999));

            StreamWriter writer = null;
            try
            {
                using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, csvFolder);
                    var csvFile = rootDirectory.GetFile(filename);

                    using (writer = new StreamWriter(csvFile.OpenWrite(), Encoding.UTF8))
                    {
                        foreach (CustomReportData result in reportResults)
                        {
                            writer.WriteLine(result.Csv);
                        }
                    }
                }
            }
            catch (AmazonS3Exception as3e)
            {
                _logger.Error(string.Format("An error occurred attempting to write the file {0} to {1}", filename, csvFolder), as3e);
                filename = null;
            }
            catch (ApplicationException ae)
            {
                _logger.Error(string.Format("An error occurred attempting to write the file {0}", filename), ae);
                filename = null;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }

            return filename;
        }
	}
}
