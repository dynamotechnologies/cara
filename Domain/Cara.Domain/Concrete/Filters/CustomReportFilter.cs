﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for CustomReportSearchResult
    /// </summary>
    [Serializable]
    public class CustomReportFilter : IFilter
    {
		public enum DisplayFormat { Html, Report };

        #region Public Members
        public int? ReportUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string ZipList { get; set; }
        public int? CountryId { get; set; }
        public int? OrganizationId { get; set; }
        public int? OrganizationTypeId { get; set; }
        public int? OfficialRepresentativeTypeId { get; set; }

        public int? DeliveryTypeId { get; set; }
        public DateTime? DateSubmittedFrom { get; set; }
        public DateTime? DateSubmittedTo { get; set; }
        public int? LetterSequence { get; set; }
        public CodeSearch CodeSearch { get; set; }
        public string LetterText { get; set; }
        public string ConcernText { get; set; }
        public string ResponseText { get; set; }
        
        public string ProjectNameOrId { get; set; }
        public int? ProjectActivityId { get; set; }
        public int? PhaseTypeId { get; set; }
        public int? AnalysisTypeId { get; set; }
        public string PhaseName { get; set; }
        public string UnitCode { get; set; }
        public int? ProjectTypeId { get; set; }
        public string ProjectStateId { get; set; }
        public string ProjectCountyName { get; set; }

        public bool SubmitterView { get; set; }
        public bool LetterView { get; set; }
        public bool ProjectView { get; set; }

        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }

        /// <summary>
        /// Returns a comma separated list of quoted zip codes
        /// </summary>
        public string QuotedZipList
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (!string.IsNullOrWhiteSpace(ZipList))
                {
                    string[] zips = ZipList.Split(',');

                    if (zips != null)
                    {
                        foreach (string zip in zips)
                        {
                            if (!string.IsNullOrWhiteSpace(zip))
                            {
                                sb.Append((sb.ToString().Length > 0 ? ", " : string.Empty) + "'" + zip.Trim() + "'");
                            }
                        }
                    }
                }

                return sb.ToString();
            }
        }
        #endregion

        #region Parameter Methods
        /// <summary>
        /// Returns the stored parameter list in html string
        /// </summary>
		public string ParameterListDisplay(DisplayFormat format)
		{
			StringBuilder sb = new StringBuilder();

			#region Parameters
			if (!string.IsNullOrEmpty(FirstName))
			{
				AppendParameter(sb, "First Name", FirstName, format);
			}

			if (!string.IsNullOrEmpty(LastName))
			{
				AppendParameter(sb, "Last Name", LastName, format);
			}

			if (!string.IsNullOrEmpty(City))
			{
				AppendParameter(sb, "City", City, format);
			}

			if (!string.IsNullOrEmpty(StateId))
			{
				AppendParameter(sb, "State", StateId, format);
			}

			if (!string.IsNullOrEmpty(ZipList))
			{
                AppendParameter(sb, "Zip Codes", ZipList, format);
			}

			if (CountryId.HasValue)
			{
				AppendParameter(sb, "Country", LookupManager.GetLookup<LookupCountry>()[CountryId.Value].Name, format);
			}

			if (OrganizationId.HasValue)
			{
                using (var orgManager = ManagerFactory.CreateInstance<OrganizationManager>())
                {
                    AppendParameter(sb, "Organization", orgManager.Get(OrganizationId.Value).Name, format);
                }
			}

			if (OrganizationTypeId.HasValue)
			{
				AppendParameter(sb, "Organization Type", LookupManager.GetLookup<LookupOrganizationType>()[OrganizationTypeId.Value].Name, format);
			}

			if (OfficialRepresentativeTypeId.HasValue)
			{
				AppendParameter(sb, "Official Representative Type", LookupManager.GetLookup<LookupOfficialRepresentativeType>()[OfficialRepresentativeTypeId.Value].Name, format);
			}

			if (DeliveryTypeId.HasValue)
			{
                AppendParameter(sb, "Delivery Type", LookupManager.GetLookup<LookupDeliveryType>()[DeliveryTypeId.Value].Name, format);
			}

			if (DateSubmittedFrom.HasValue)
			{
				AppendParameter(sb, "Date Submitted From", DateSubmittedFrom.Value.ToShortDateString(), format);
			}

			if (DateSubmittedTo.HasValue)
			{
				AppendParameter(sb, "Date Submitted To", DateSubmittedTo.Value.ToShortDateString(), format);
			}

			if (LetterSequence.HasValue)
			{
				AppendParameter(sb, "Letter #", LetterSequence.Value.ToString(), format);
			}

			if (CodeSearch != null && !string.IsNullOrEmpty(CodeSearch.CodeSearchText))
			{
				AppendParameter(sb, "Comment Code", CodeSearch.CodeSearchText, format);
			}

			if (!string.IsNullOrEmpty(LetterText))
			{
				AppendParameter(sb, "Letter Text", LetterText, format);
			}

			if (!string.IsNullOrEmpty(ConcernText))
			{
				AppendParameter(sb, "Concerns", ConcernText, format);
			}

			if (!string.IsNullOrEmpty(ResponseText))
			{
				AppendParameter(sb, "Responses", ResponseText, format);
			}

			if (!string.IsNullOrEmpty(ProjectNameOrId))
			{
				AppendParameter(sb, "Project Name or ID", ProjectNameOrId, format);
			}

			if (ProjectActivityId.HasValue)
			{
				AppendParameter(sb, "Activity", LookupManager.GetLookup<LookupActivity>()[ProjectActivityId.Value].Name, format);
			}

			if (PhaseTypeId.HasValue)
			{
				AppendParameter(sb, "Comment Period", LookupManager.GetLookup<LookupPhaseType>()[PhaseTypeId.Value].Name, format);
			}

			if (AnalysisTypeId.HasValue)
			{
				AppendParameter(sb, "Analysis Type", LookupManager.GetLookup<LookupAnalysisType>()[AnalysisTypeId.Value].Name, format);
			}

			if (!string.IsNullOrEmpty(PhaseName))
			{
				AppendParameter(sb, "Comment Period Description", PhaseName, format);
			}

			if (!string.IsNullOrEmpty(UnitCode))
			{
				AppendParameter(sb, "Region/Forest/District", UnitCode, format);
			}

			if (ProjectTypeId.HasValue)
			{
				AppendParameter(sb, "Project Type", LookupManager.GetLookup<LookupProjectType>()[ProjectTypeId.Value].Name, format);
			}

			if (!string.IsNullOrEmpty(ProjectStateId))
			{
				AppendParameter(sb, "Project State", ProjectStateId, format);
			}

			if (!string.IsNullOrEmpty(ProjectCountyName))
			{
				AppendParameter(sb, "Project County", ProjectCountyName, format);
			}

			if (SubmitterView)
			{
				AppendParameter(sb, "Submitter View", "Yes", format);
			}

			if (LetterView)
			{
				AppendParameter(sb, "Letter/Comment View", "Yes", format);
			}

			if (ProjectView)
			{
				AppendParameter(sb, "Project View", "Yes", format);
			}
			#endregion

			return sb.ToString();
		}

        private void AppendParameter(StringBuilder sb, string key, string value, DisplayFormat format)
        {
            sb.Append(sb.Length == 0 ? string.Empty : ", ");

			string formatString = string.Empty;
			if (format == DisplayFormat.Html)
			{
				formatString = "<b>{0}</b>: {1}";
			}
			else if (format == DisplayFormat.Report)
			{
				formatString = "{0}: {1}";
				key = key.ToUpper();
			}

            sb.Append(string.Format(formatString, key, value));
        }
        #endregion
    }
}
