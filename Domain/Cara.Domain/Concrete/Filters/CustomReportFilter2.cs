﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for CustomReportSearchResult
    /// </summary>
    [Serializable]
    public class CustomReportFilter2 : IFilter
    {
        public string ParameterXML { get; set; }
        public int ReportUserId { get; set; }

        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
    }
}
