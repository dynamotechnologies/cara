﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for CommentSearchResult
    /// </summary>
    public class CommentSearchResultFilter : IFilter
    {
        #region Public Members
        public int PhaseId { get; set; }
        public string LetterSequence { get; set; }
        public string CommentNumber { get; set; }
        public string Code { get; set; }
        public string CodeFrom { get; set; }
        public string CodeTo { get; set; }
        public string CodeName { get; set; }
        public CodeSearch CodeSearch { get; set; }
        public int? CodeCategoryId { get; set; }
        public string Keyword { get; set; }
        public bool? Association { get; set; }
        public bool? SampleStatement { get; set; }
        public bool? NoResponseRequired { get; set; }
        public int? NoResponseReasonId { get; set; }
        public int? UserId { get; set; }
        public bool? Annotation { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set the filter values based on filter type
        /// </summary>
        /// <param name="filterType"></param>
        public void SetFilterByType(string filterType)
        {
            if (!string.IsNullOrEmpty(filterType))
            {
                // clear all filters
                Code = CodeFrom = CodeTo = CodeName = Keyword = null;
                Association = SampleStatement = NoResponseRequired = Annotation = null;
                CodeCategoryId = null;

                int userId = 0;
                if (filterType.StartsWith("AssignedTo"))
                {
                    var parsed = filterType.Split('|');
                    filterType = parsed[0];
                    if (!int.TryParse(parsed[1], out userId))
                    {
                        userId = 0;
                    }
                }

                switch (filterType)
                {
                    case "IssueAction":
                        CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
                            .Where(x => x.Value == "Issue/Action")
                            .Select(y => y.Key).First();
                        break;
                    case "Rationale":
                        CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
                            .Where(x => x.Value == "Resource/Rationale")
                            .Select(y => y.Key).First();
                        break;
                    case "Location":
                        CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
                            .Where(x => x.Value == "Location")
                            .Select(y => y.Key).First();
                        break;
					case "Document":
						CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
							.Where(x => x.Value == "Document")
							.Select(y => y.Key).First();
						break;
					case "EarlyAttention":
						CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
							.Where(x => x.Value == "Early Attention")
							.Select(y => y.Key).First();
						break;
					case "Objection":
						CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
							.Where(x => x.Value == "Objection")
							.Select(y => y.Key).First();
						break;
					case "Other":
						CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
							.Where(x => x.Value == "Other")
							.Select(y => y.Key).First();
						break;
					case "LegalAppeals":
                        CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
                            .Where(x => x.Value == "Appeals (Legal)")
                            .Select(y => y.Key).First();
                        break;
                    case "ResourceAppeals":
                        CodeCategoryId = LookupManager.GetLookup<LookupCodeCategory>().SelectionList
                            .Where(x => x.Value == "Appeals (Resource)")
                            .Select(y => y.Key).First();
                        break;
                    case "AssignedTo":
                        UserId = userId;
                        break;
                    case "Total":
                        break;
                }
            }
        }
        #endregion
    }
}
