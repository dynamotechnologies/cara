﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for ProjectSearchResult
    /// </summary>
    public class ProjectSearchResultFilter : IFilter
    {
        #region Public Members
        public string Keyword { get; set; }
        public string UnitId { get; set; }
        public int? AnalysisTypeId { get; set; }
        public int? ProjectStatusId { get; set; }
        public int? ProjectActivityId { get; set; }
        public string StateId { get; set; }
        public bool? MyProjects { get; set; }
        public bool? Archived { get; set; }        
        public int? UserId { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        public int? PhaseTypeId { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set the filter values based on filter type
        /// </summary>
        /// <param name="filterType"></param>
        /// <param name="keyword"></param>
        /// <param name="userId"></param>
        public void SetFilterByType(string filterType, string keyword = null, int? userId = null)
        {
            if (!string.IsNullOrEmpty(filterType))
            {
                // clear all filters
                Keyword = UnitId = StateId = null;
                MyProjects = Archived = null;
                AnalysisTypeId = ProjectStatusId = ProjectActivityId = UserId = null;
                PhaseTypeId = null;

                UserId = userId.HasValue ? userId.Value : new Nullable<int>();
                switch (filterType)
                {
                    case "Keyword":
                        Keyword = keyword;
                        break;
                    case "MyProjects":
                        MyProjects = true;
                        Archived = false;
                        break;
                }
            }
        }

        public string ParentUnitId
        {
            get
            {
                return Unit.GetParentUnitId(UnitId);
            }
        }
        #endregion
    }
}
