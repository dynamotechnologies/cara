﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
    /// <summary>
	/// Filter class for FormSetSearchResultFilter
    /// </summary>
    public class FormSetSearchResultFilter : IFilter
    {
        #region Public Members
        public int PhaseId { get; set; }
		public int? FormSetId { get; set; }
		public int? LetterTypeId { get; set; }
		public int? LetterStatusId { get; set; }
		public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        #endregion

        public FormSetSearchResultFilter() { }

        public FormSetSearchResultFilter(string formSetId)
        {
            SetFormSet(formSetId);
        }

        #region Public Methods
        /// <summary>
        /// Set the filter values based on filter type
        /// </summary>
        /// <param name="filterType"></param>
        public void SetFormSet(string formSetId)
        {
            int fsId;
            bool isValid = Int32.TryParse(formSetId, out fsId);

            // Clear the filter
            FormSetId = null;
            LetterTypeId = null;
            LetterStatusId = null;

            // Set the form set id (if 0, set it to -1 to get the unique forms)
            FormSetId = isValid ? Convert.ToInt32(formSetId) : -1;
        }
		#endregion Public Methods
	}
}
