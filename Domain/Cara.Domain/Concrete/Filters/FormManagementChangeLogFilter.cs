﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for FormManagementChangeLog
    /// </summary>
    public class FormManagementChangeLogFilter : IFilter
    {
        #region Public Members
        public int PhaseId { get; set; }
		public int? PageNum { get; set; }
        #endregion
    }
}
