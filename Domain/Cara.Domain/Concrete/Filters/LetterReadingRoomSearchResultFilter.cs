﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for LetterReadingRoomSearchResult
    /// </summary>
    public class LetterReadingRoomSearchResultFilter : IFilter
    {
        #region Public Members
        public int? ProjectId { get; set; }
        public int? PhaseId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? LetterTypeId { get; set; }
        public string OrganizationId { get; set; }
        public string Keyword { get; set; }
        public bool? PublishToReadingRoom { get; set; }
        public bool? ShowForms { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set the filter values based on filter type
        /// </summary>
        /// <param name="filterType"></param>
        public void SetFilterByType(string filterType)
        {
            if (!string.IsNullOrEmpty(filterType))
            {
                // clear all filters
                PublishToReadingRoom = null;
                FirstName = null;
                LastName = null;
                LetterTypeId = null;
                OrganizationId = null;
                Keyword = null;

                switch (filterType)
                {
                    case "Unpublished":
                        PublishToReadingRoom = false;
                        break;
                }
            }
        }
        #endregion
    }
}
