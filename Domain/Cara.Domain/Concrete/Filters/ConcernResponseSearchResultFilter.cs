﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for ConcernResponseSearchResult
    /// </summary>
    public class ConcernResponseSearchResultFilter : IFilter
    {
        #region Public Members
        public int PhaseId { get; set; }
        public int? ConcernResponseStatusId { get; set; }
        public int? ConcernResponseNumber { get; set; }
        public DateTime? DateUpdatedFrom { get; set; }
        public DateTime? DateUpdatedTo { get; set; }
        public string Keyword { get; set; }
        public int? CodeCategoryId { get; set; }
        public string Code { get; set; }
        public string CodeFrom { get; set; }
        public string CodeTo { get; set; }
        public string CodeName { get; set; }
        public CodeSearch CodeSearch { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        public int? UserId { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set the filter values based on filter type
        /// </summary>
        /// <param name="filterType"></param>
        public void SetFilterByType(string filterType)
        {
            if (!string.IsNullOrEmpty(filterType))
            {
                // clear all filters
                ConcernResponseStatusId = null;

                Code = CodeFrom = CodeTo = CodeName = Keyword = null;
                DateUpdatedFrom = DateUpdatedTo = null;

                int userId = 0;
                if (filterType.StartsWith("AssignedTo"))
                {
                    var parsed = filterType.Split('|');
                    filterType = parsed[0];
                    if (!int.TryParse(parsed[1], out userId))
                    {
                        userId = 0;
                    }
                }

                switch (filterType)
                {
                    case "InProgressConcerns":
                        ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList
                            .Where(x => x.Value == "Concern In Progress")
                            .Select(y => y.Key).First();
                        break;
                    case "ReadyResponses":
                        ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList
                            .Where(x => x.Value == "Ready for Response")
                            .Select(y => y.Key).First();
                        break;
                    case "InProgressResponses":
                        ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList
                            .Where(x => x.Value == "Response In Progress")
                            .Select(y => y.Key).First();
                        break;
                    case "CompleteResponses":
                        ConcernResponseStatusId = LookupManager.GetLookup<LookupConcernResponseStatus>().SelectionList
                            .Where(x => x.Value == "Response Complete")
                            .Select(y => y.Key).First();
                        break;
                    case "AssignedTo":
                        UserId = userId;
                        break;
                    case "Total":
                        break;
                }
            }
        }
        #endregion
    }
}
