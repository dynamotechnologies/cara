﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for LetterSearchResult
    /// </summary>
    public class LetterSearchResultFilter : IFilter
    {
        #region Public Members
        public int PhaseId { get; set; }
        public int? LetterTypeId { get; set; }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }
        public int? LetterSequence { get; set; }
        public int? LetterStatusId { get; set; }
        public string Keyword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? ViewAll { get; set; }
        public bool? WithinCommentPeriod { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        public int? EarlyActionStatusId { get; set; }
        public int? StartingLetterId { get; set; }
        public int AttachmentCount { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set the filter values based on filter type
        /// </summary>
        /// <param name="filterType"></param>
        public void SetFilterByType(string filterType)
        {
            if (!string.IsNullOrEmpty(filterType))
            {
                // clear all filters
                LetterTypeId = LetterStatusId = EarlyActionStatusId = null;
                DateReceivedFrom = DateReceivedTo = null;
                Keyword = null;
                
                ViewAll = WithinCommentPeriod = null;

                switch (filterType)
                {
                    case "New":
                        LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().SelectionList
                            .Where(x => x.Value == "New")
                            .Select(y => y.Key).First();
                        break;
                    case "RequiresEarlyAttention":
                        EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().SelectionList
                            .Where(x => x.Value == "Needs Attention")
                            .Select(y => y.Key).First();
                        break;
                    case "NotRequired":
                        EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().SelectionList
                            .Where(x => x.Value == "Not Required")
                            .Select(y => y.Key).First();
                        break;
                    case "NeedsReview":
                        EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().SelectionList
                            .Where(x => x.Value == "Auto-Marked")
                            .Select(y => y.Key).First();
                        break;
                    case "Completed":
                        EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().SelectionList
                            .Where(x => x.Value == "Completed")
                            .Select(y => y.Key).First();
                        break;
                    case "Coded":
                        EarlyActionStatusId = LookupManager.GetLookup<LookupEarlyActionStatus>().SelectionList
                            .Where(x => x.Value == "Coded")
                            .Select(y => y.Key).First();
                        break;
                    case "CodingInProgress":
                        LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().SelectionList
                            .Where(x => x.Value == "In Progress")
                            .Select(y => y.Key).First();                      
                        break;
                    case "CodingComplete":
                        LetterStatusId = LookupManager.GetLookup<LookupLetterStatus>().SelectionList
                            .Where(x => x.Value == "Complete")
                            .Select(y => y.Key).First();
                        break;
                    case "Unique":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Unique")
                            .Select(y => y.Key).First();
                        break;
                    case "MasterForm":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Master Form")
                            .Select(y => y.Key).First();
                        break;
                    case "Form":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Form")
                            .Select(y => y.Key).First();
                        break;
                    case "FormPlus":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Form Plus")
                            .Select(y => y.Key).First();
                        break;
                    case "Duplicate":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Duplicate")
                            .Select(y => y.Key).First();
                        break;
                    case "Forms":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "All Forms")
                            .Select(y => y.Key).First();
                        break;
                    case "Total":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Not Pending")
                            .Select(y => y.Key).First();
                        break;
                    case "Pending":
                        LetterTypeId = LookupManager.GetLookup<LookupLetterType>().SelectionList
                            .Where(x => x.Value == "Pending")
                            .Select(y => y.Key).First();
                        break;
                    case "All":
                        LetterTypeId = null;
                        break;
                }
            }
        }
        #endregion
    }
}
