﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for PhaseCodeSelect
    /// </summary>
    public class PhaseCodeSelectFilter : IFilter
    {
        #region Public Members
        public int? PhaseId { get; set; }
        public CodeSearch CodeSearch { get; set; }
        #endregion                
    }
}
