﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Filter class for PhaseSearchResult
    /// </summary>
    public class PhaseSearchResultFilter : IFilter
    {
        #region Public Members
        public int? UserId { get; set; }
        public int? ProjectId { get; set; }
        public int? PageNum { get; set; }
        public int? NumRows { get; set; }
        public string SortKey { get; set; }
        #endregion
    }
}
