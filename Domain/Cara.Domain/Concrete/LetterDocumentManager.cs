﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class LetterDocumentManager : AbstractEntityManager<LetterDocument>
	{
        public LetterDocumentManager(IRepository<LetterDocument> repository)
			: base(repository)
		{
		}

        #region Override AbstractEntityManager methods
        public override LetterDocument Get(int uniqueId)
        {
            throw new NotImplementedException();
        }

        public override LetterDocument Get(string uniqueName)
        {
            throw new NotImplementedException();
        }
        #endregion Override AbstractEntityManager methods
    }
}
