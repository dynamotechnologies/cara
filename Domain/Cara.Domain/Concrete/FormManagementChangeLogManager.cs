﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Cara.Reference;

namespace Aquilent.Cara.Domain
{
	public class FormManagementChangeLogManager : AbstractEntityManager<FormManagementChangeLog>
	{
		public FormManagementChangeLogManager(IRepository<FormManagementChangeLog> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a form set with the given ID
		/// </summary>
        /// <param name="name">The ID of the formset</param>
		/// <returns>formset or null</returns>
		public override FormManagementChangeLog Get(int uniqueId)
		{
			throw new NotImplementedException("You cannot retrieve individual FormManagementChangeLog entries");
		}

		public override FormManagementChangeLog Get(string uniqueName)
		{
			throw new NotImplementedException("You cannot retrieve individual FormManagementChangeLog entries");
		}
		#endregion Override AbstractEntityService members

		#region Methods
		/// <summary>
		/// Logs when the letter type of a letter is modified.
		/// 
		/// Example messages:
		///		Re-designated Letter #58 from 'Form Plus' to 'Form' in 'Form Set 15'
		///		Re-designated Letter #75 from 'Form Plus' to 'Master Form' in 'Form Set 22'
		///		Re-designated Letter #33 from 'Master Form' to 'Form' in 'Form Set 22'
		/// </summary>
		/// <param name="letter">The updated letter</param>
		/// <param name="oldLetterTypeId">The old letter type id</param>
		/// <param name="username">The username of the person making the change</param>
		public void LogChangeLetterType(Letter letter, int oldLetterTypeId, string username)
		{
			var lookupLetterType = LookupManager.GetLookup<LookupLetterType>();
			var oldLetterType = lookupLetterType[oldLetterTypeId];
			var newLetterType = lookupLetterType[letter.LetterTypeId];

			var formSetName = (letter.FormSet == null || string.IsNullOrEmpty(letter.FormSet.Name))  ? "Unique Set" : letter.FormSet.Name;

			var logEntry = new FormManagementChangeLog
			{
				PhaseId = letter.PhaseId,
				LetterNumber = letter.LetterSequence.Value,
				Date = DateTime.UtcNow,
				Username = username,
				Description = string.Format("Re-designated Letter #{0} from '{1}' to '{2}' in '{3}'",
					letter.LetterSequence,
					oldLetterType.Name,
					newLetterType.Name,
					formSetName)
			};

			Add(logEntry);
		}

		/// <summary>
		/// Logs when a letter moves from one form set to another
		/// 
		/// Example message:
		///		Moved Letter #58 from 'Unique Set' to 'Form Set 22'
		/// </summary>
		/// <param name="letter">The updated letter</param>
		/// <param name="oldFormSet">The form set the letter used to belong to.  Can be null, if the letter was unique.</param>
		/// <param name="username">The username of the person making the change</param>
		public void LogChangeFormSet(Letter letter, FormSet oldFormSet, string username)
		{
			string oldFormSetName = oldFormSet != null ? oldFormSet.Name : "Unique Set";
			string newFormSetName = letter.FormSetId != null ? letter.FormSet.Name : "Unique Set";

			var logEntry = new FormManagementChangeLog
			{
				PhaseId = letter.PhaseId,
				LetterNumber = letter.LetterSequence.Value,
				Date = DateTime.UtcNow,
				Username = username,
				Description = string.Format("Moved Letter #{0} from '{1}' to '{2}'",
					letter.LetterSequence,
					oldFormSetName,
					newFormSetName)
			};

			Add(logEntry);
		}

		/// <summary>
		/// Logs when a FormSet is renamed
		/// 
		/// Example message:
		///		Renamed FormSet from 'Form Set 22' to 'Sierra Club Form Letter'
		/// </summary>
		/// <param name="formSet">The updated FormSet</param>
		/// <param name="oldFormSetName">The previous name of the FormSet</param>
		/// <param name="username">The username of the person making the change</param>
		public void LogChangeFormSetName(FormSet formSet, string oldFormSetName, string username)
		{
			var logEntry = new FormManagementChangeLog
			{
				PhaseId = formSet.PhaseId,
				LetterNumber = 0,
				Date = DateTime.UtcNow,
				Username = username,
				Description = string.Format("Renamed FormSet from '{0}' to '{1}'",
					oldFormSetName,
					formSet.Name)
			};

			Add(logEntry);
		}
		#endregion Methods
	}
}
