﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    public class CustomReportData
    {
        public List<string> Columns = new List<string>();
        public string Column1
        {
            get
            {
                return GetColumn(1);
            }
            set
            {
                SetColumn(1, value);
            }
        }
        public string Column2
        {
            get
            {
                return GetColumn(2);
            }
            set
            {
                SetColumn(2, value);
            }
        }
        public string Column3
        {
            get
            {
                return GetColumn(3);
            }
            set
            {
                SetColumn(3, value);
            }
        }
        public string Column4
        {
            get
            {
                return GetColumn(4);
            }
            set
            {
                SetColumn(4, value);
            }
        }
        public string Column5
        {
            get
            {
                return GetColumn(5);
            }
            set
            {
                SetColumn(5, value);
            }
        }
        public string Column6
        {
            get
            {
                return GetColumn(6);
            }
            set
            {
                SetColumn(6, value);
            }
        }
        public string Column7
        {
            get
            {
                return GetColumn(7);
            }
            set
            {
                SetColumn(7, value);
            }
        }
        public string Column8
        {
            get
            {
                return GetColumn(8);
            }
            set
            {
                SetColumn(8, value);
            }
        }
        public string Column9
        {
            get
            {
                return GetColumn(9);
            }
            set
            {
                SetColumn(9, value);
            }
        }
        public string Column10
        {
            get
            {
                return GetColumn(10);
            }
            set
            {
                SetColumn(10, value);
            }
        }
        public string Column11
        {
            get
            {
                return GetColumn(11);
            }
            set
            {
                SetColumn(11, value);
            }
        }
        public string Column12
        {
            get
            {
                return GetColumn(12);
            }
            set
            {
                SetColumn(12, value);
            }
        }
        public string Column13
        {
            get
            {
                return GetColumn(13);
            }
            set
            {
                SetColumn(13, value);
            }
        }
        public string Column14
        {
            get
            {
                return GetColumn(14);
            }
            set
            {
                SetColumn(14, value);
            }
        }
        public string Column15
        {
            get
            {
                return GetColumn(15);
            }
            set
            {
                SetColumn(15, value);
            }
        }
        public string Column16
        {
            get
            {
                return GetColumn(16);
            }
            set
            {
                SetColumn(16, value);
            }
        }
        public string Column17
        {
            get
            {
                return GetColumn(17);
            }
            set
            {
                SetColumn(17, value);
            }
        }
        public string Column18
        {
            get
            {
                return GetColumn(18);
            }
            set
            {
                SetColumn(18, value);
            }
        }
        public string Column19
        {
            get
            {
                return GetColumn(19);
            }
            set
            {
                SetColumn(19, value);
            }
        }
        public string Column20
        {
            get
            {
                return GetColumn(20);
            }
            set
            {
                SetColumn(20, value);
            }
        }
        public string Csv
        {
            get
            {
                string retVal = "";
                foreach (string column in Columns)
                {
                    if (string.IsNullOrWhiteSpace(retVal))
                    {
                        retVal = QuoteString(column);
                    }
                    else
                    {
                        retVal = String.Format("{0},{1}", retVal, QuoteString(column));
                    }
                }
                return retVal; 
            }
        }

        private string GetColumn(int index)
        {
            return index > Columns.Count() ? null : Columns[index - 1];
        }

        private void SetColumn(int index, string value)
        {
            while (index > Columns.Count())
            {
                Columns.Add(string.Empty);
            }
            Columns[index - 1] = value;
        }
        /// <summary>
        /// Formats the data value for CSV
        /// </summary>
        /// <param name="value">The data value</param>
        /// <returns>The formated data value</returns>
        private string QuoteString(string value)
        {
            //CARA-1128 remove line breaks from data
            //CARA-1143 escape double quotes in data

            value = value.Length > 32000 ? value.Substring(0, 32000) + "...[TEXT TRUNCATED, EXCEEDS LIMIT]" : value;
            value = string.Format("\"{0}\"", value.Replace('\n', ' ').Replace('\r', ' ').Replace("\"", "\"\""));
            return value;
        }

    }
}
