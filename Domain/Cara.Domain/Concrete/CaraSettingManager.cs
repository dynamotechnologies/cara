﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class CaraSettingManager : AbstractEntityManager<CaraSetting>
	{
		public CaraSettingManager(IRepository<CaraSetting> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given term list ID
		/// </summary>
        /// <param name="name">The ID of the system config</param>
		/// <returns>CaraSetting or null</returns>
		public override CaraSetting Get(int uniqueId)
		{
            throw new NotImplementedException();
		}

        /// <summary>
        /// Returns a setting with the given name.  Note the setting name must start with "cara."
        /// </summary>
        /// <param name="uniqueName">The setting</param>
        /// <returns>SecuritySetting or null</returns>
        public override CaraSetting Get(string uniqueName)
        {
            return All.Where(x => x.Name.ToLower().StartsWith("cara.") && x.Name == uniqueName).FirstOrDefault();
        }
		#endregion Override AbstractEntityService members
	}
}
