﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain.Report
{
	public class LookupReport : AbstractLookup<int, Report>
    {
        #region Public Members
        // holds a list of report for listing purpose
        public IList<Report> Reports = new List<Report>();
        #endregion

        #region Override Methods
        protected override IList<Report> LoadValues()
		{
			return DataLoader.Load().OfType<Report>().ToList();
		}

		protected override KeyValuePair<int, Report> PrepareForLookup(Report item)
		{
			return new KeyValuePair<int, Report>(item.ReportId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(Report item)
		{
            // add the item to the reports
            Reports.Add(item);

			return new KeyValuePair<int, string>(item.ReportId, item.Name);
        }
        #endregion
    }
}
