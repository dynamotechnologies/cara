﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using System.Configuration;
using Amazon.SQS;
using Amazon;
using Amazon.SQS.Model;
using Aquilent.Cara.Domain.Aws.Sqs;

namespace Aquilent.Cara.Domain.Report.Personal
{
	public class CustomReportQueueManager : AbstractEntityManager<CustomReportQueue>
	{
        private bool hasAddedCustomReportQueue = false;
		private static readonly string queueNameKey = "cara.queue.customreportexport";

		#region Constructor
        public CustomReportQueueManager(IRepository<CustomReportQueue> repository)
			: base(repository)
		{
		}
		#endregion Constructor
        
		#region Override AbstractEntityService members
        public override void Add(CustomReportQueue entity)
        {
            base.Add(entity);

            if (!hasAddedCustomReportQueue)
            {
                UnitOfWork.Saved += new UnitOfWorkEventHandler(QueueCustomReportForExport);
                hasAddedCustomReportQueue = true;
            }
        }
        public override CustomReportQueue Get(int uniqueId)
		{
            return All.FirstOrDefault(x => x.CustomReportQueueId == uniqueId);
		}

        public override CustomReportQueue Get(string uniqueName)
		{
            throw new NotImplementedException();
		}
		#endregion Override AbstractEntityService members        

        internal void QueueCustomReportForExport(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Added"))
            {
                IList<object> entries = (IList<object>) e["Added"];
                foreach (var entry in entries)
                {
                    CustomReportQueue entity;
                    if (entry is CustomReportQueue)
                    {
                        entity = (CustomReportQueue)entry;

                        string queueName;
                        var sqs = SqsHelper.GetQueue(queueNameKey, out queueName);

                        var sqsRequest = new CreateQueueRequest { QueueName = queueName };
                        var createQueueResponse = sqs.CreateQueue(sqsRequest);
                        string queueUrl = createQueueResponse.QueueUrl;

                        var sendMessageRequest = new SendMessageRequest
                            {
                                QueueUrl = queueUrl,
                                MessageBody = entity.CustomReportQueueId.ToString()
                            };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
        }
    }
}
