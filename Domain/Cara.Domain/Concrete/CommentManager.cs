﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using System.Xml.Linq;

namespace Aquilent.Cara.Domain
{
	public class CommentManager : AbstractEntityManager<Comment>
	{
        public CommentManager(ICommentRepository repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a comment with the given comment ID
		/// </summary>
        /// <param name="name">The ID of the comment</param>
		/// <returns>Comment or null</returns>
        public override Comment Get(int uniqueId)
        {
            return Repository.ObjectQuery.Include("Letter").Where(x => x.CommentId == uniqueId).FirstOrDefault();
        }

        public IList<Comment> GetAllComments(int letterId)
        {
            return Repository.ObjectQuery.Where(x => x.LetterId == letterId).ToList();
            //return All.Where(x => x.LetterId == letterId).FirstOrDefault();
        }

  		/// <summary>
        /// Returns a comment with the uniqueName
		/// </summary>
		/// <param name="uniqueName"></param>
		/// <returns></returns>
        public override Comment Get(string uniqueName)
        {
            throw new NotImplementedException();
        }
		#endregion Override AbstractEntityService members

        #region Custom Methods
        /// <summary>
        /// Returns a comment where the FullCommentNumber and Phase ID is the unique
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="fullCommentNumber">Full comment number</param>
        /// <returns></returns>
        public Comment Get(int phaseId, string fullCommentNumber)
        {
            string[] pieces = fullCommentNumber.Split('-');
            int commentNumber;
            int letterNumber;
            if (pieces.Length != 2)
            {
                throw new ArgumentException("The format for 'uniqueName' should be X-Y, where X is the letter number and Y is the comment number");
            }
            else
            {
                try
                {
                    letterNumber = Convert.ToInt32(pieces[0]);
                    commentNumber = Convert.ToInt32(pieces[1]);
                }
                catch (FormatException)
                {
                    throw new ArgumentException("The format for 'uniqueName' should be X-Y, where X is the letter number and Y is the comment number");
                }
            }

            return Repository.ObjectQuery
                .Include("Letter")
                .Include("CommentCodes.PhaseCode")
                .Include("ConcernResponse")
                .Where(x => x.CommentNumber == commentNumber && x.Letter.LetterSequence == letterNumber && x.Letter.PhaseId == phaseId).FirstOrDefault();
        }


        /// <summary>
        /// Returns the comments based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <returns>A list of matching comment search results</returns>
        public IList<CommentSearchResult> GetComments(CommentSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<CommentSearchResult> list = (Repository as ICommentRepository).GetCommentSearchResults(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        /// <summary>
        /// Returns the comments based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <returns>A list of matching comment search results</returns>
        public IList<CommentDetails> CommentDetails(XDocument xdoc)
        {
            IList<CommentDetails> list = (Repository as ICommentRepository).CommentDetails(xdoc);
            
            return list;
        }

        public IQueryable<Comment> GetCommentsForConcernResponse(int concernResponseId)
        {
            return Repository.ObjectQuery
                .Include("Letter")
                .Include("CommentCodes.PhaseCode")
                .Include("CommentTexts")
                .Where(r => r.ConcernResponseId == concernResponseId).AsQueryable();
        }


        #endregion

       
	}
}
