﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;



namespace Aquilent.Cara.Domain
{
    public class CustomReportParameters
    {

        private int maxListId = 0;
        public List<CustomReportDataItem> DataItems { get; set; }
        public List<CustomReportColumnDataItem> ColumnDataItems { get; set; }
        public List<int> CollapsedListIds {get; set;}
        public CustomReportParameters()
        {
            Clear();
        }
        public CustomReportParameters(string xml, bool encoded = true)
        {
            if (encoded)
            { xml = HttpUtility.HtmlDecode(xml); }
            PopulateFromXml(xml);
        }
        public void Clear()
        {
            DataItems = new List<CustomReportDataItem>();
            ColumnDataItems = new List<CustomReportColumnDataItem>();
            ResetListIds();
        }
        public string GetXml(bool forStoredProcedure = false)
        {
            XElement reportData = new XElement("CustomReportData",
                GetXmlForFieldType(CustomReportFieldType.CommentPeriod),
                GetXmlForFieldType(CustomReportFieldType.Project),
                GetXmlForFieldType(CustomReportFieldType.Commenter),
                GetXmlForFieldType(CustomReportFieldType.Letter),
                GetXmlForFieldType(CustomReportFieldType.Comment),
                GetXmlForColumns());
            if (forStoredProcedure)
            {
                string retVal = reportData.ToString(System.Xml.Linq.SaveOptions.DisableFormatting);
                retVal = retVal.Replace("'", "''");
                return retVal;
            }
            return HttpUtility.HtmlEncode(reportData.ToString());
        }
        public bool Add(int fieldId, string fieldDataType, string fieldName, string fieldType, bool not, string value, string name = null)
        {
            if (fieldDataType == "date")
            {
                return false;
            }
            if ((fieldDataType == "dropdown" || fieldDataType == "treedropdown") && name == null)
            {
                return false;
            }
            if (!DataItems.Any(x => x.FieldId == fieldId))
            {
                //add field entry
                maxListId++;
                CustomReportDataItem newDataItem = new CustomReportDataItem
                {
                    FieldDataType = fieldDataType,
                    FieldId = fieldId,
                    FieldName = fieldName,
                    FieldType = fieldType,
                    ListId = maxListId,
                    Values = new List<CustomReportValueItem>()
                };
                DataItems.Add(newDataItem);
            }
            CustomReportDataItem dataItem = DataItems.FirstOrDefault(x => x.FieldId == fieldId);


            if (fieldDataType == "string" || fieldDataType == "number")
            {
                string[] commaValues = value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                List<string> values = new List<string>();
                foreach (string commaValue in commaValues)
                {
                    int orIndex = commaValue.IndexOf(" OR ");
                    int startIndex = 0;
                    while (orIndex > startIndex)
                    {
                        values.Add(commaValue.Substring(startIndex, orIndex));
                        startIndex = orIndex + 4;
                        orIndex = commaValue.IndexOf(" OR ", startIndex);
                    }
                    values.Add(commaValue.Substring(startIndex));
                }
                foreach (string stringValue in values)
                {
                    if (string.IsNullOrWhiteSpace(stringValue.Trim()))
                    {
                        continue;
                    }
                    //CARA-1082 add custom filtering for comment codes
                    if (fieldId == 17 || fieldId == 27) //comment Code Number
                    {
                        CodeSearch codeSearch = new CodeSearch(stringValue.Trim());
                        if (codeSearch.CodeNumber != null)
                        {
                            maxListId++;
                            dataItem.Values.Add(new CustomReportValueItem
                            {
                                ListId = maxListId,
                                Name = GetCodeNumberShortForm(codeSearch.CodeNumber),
                                Not = not,
                                Value = codeSearch.CodeNumber
                            });
                        }
                        else if (codeSearch.CodeRanges.Any())
                        {
                            foreach (CodeRange codeRange in codeSearch.CodeRanges)
                            {
                                maxListId++;
                                dataItem.Values.Add(new CustomReportValueItem
                                {
                                    ListId = maxListId,
                                    Name = String.Format("{0}-{1}",
                                        GetCodeNumberShortForm(codeRange.CodeRangeFrom),
                                        GetCodeNumberShortForm(codeRange.CodeRangeTo)),
                                    Not = not,
                                    Value = String.Format("{0}-{1}", codeRange.CodeRangeFrom, codeRange.CodeRangeTo)
                                });
                            }
                        }
                    }
                    else
                    {
                        maxListId++;
                        CustomReportValueItem stringValueItem = new CustomReportValueItem
                        {
                            ListId = maxListId,
                            Not = not,
                            Value = stringValue.Trim(),
                            Name = stringValue.Trim()
                        };
                        dataItem.Values.Add(stringValueItem);
                    }
                }
                if (!dataItem.Values.Any())
                {
                    Remove(dataItem.ListId);
                    return false;
                }
                return true;
            }
            maxListId++;
            CustomReportValueItem valueItem = new CustomReportValueItem
            {
                ListId = maxListId,
                Not = not,
                Value = value
            };
            if (fieldDataType == "dropdown" || fieldDataType == "treedropdown")
            {
                valueItem.Name = name;
            }
            dataItem.Values.Add(valueItem);
            return true;
        }
        public bool Add(int fieldId, string fieldDataType, string fieldName, string fieldType, bool not, DateTime to, DateTime from)
        {
            if (fieldDataType != "date")
            {
                return false;
            }
            if (!DataItems.Any(x => x.FieldId == fieldId))
            {
                //add field entry
                maxListId++;
                CustomReportDataItem newDataItem = new CustomReportDataItem
                {
                    FieldDataType = fieldDataType,
                    FieldId = fieldId,
                    FieldName = fieldName,
                    FieldType = fieldType,
                    ListId = maxListId,
                    Values = new List<CustomReportValueItem>()
                };
                DataItems.Add(newDataItem);
            }
            CustomReportDataItem dataItem = DataItems.FirstOrDefault(x => x.FieldId == fieldId);

            //remove field listId from collapsed listIds so the new value will be displayed in the tree
            CollapsedListIds.Remove(dataItem.ListId);
            CollapsedListIds.Remove(CustomReportFieldType.GetListIdforFieldType(dataItem.FieldType));
            CollapsedListIds.Remove(-1);

            maxListId++;
            string valueName = string.Empty;
            if (from == DateTime.MinValue)
            { valueName = String.Format("Before {0}", to.ToShortDateString()); }
            else if (to == DateTime.MaxValue)
            { valueName = String.Format("After {0}", from.ToShortDateString()); }
            else
            {
                valueName = String.Format("Between {0} and {1}",
                    from.ToShortDateString(),
                    to.ToShortDateString());
            }

            //to = to.AddHours(23 + 7).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

            CustomReportValueItem valueItem = new CustomReportValueItem
            {
                ListId = maxListId,
                Not = not,
                DateFromValue = from,
                DateToValue = to,
                Name = valueName
            };
            dataItem.Values.Add(valueItem);
            return true;
        }
        public void AddColumn(string columnName, int columnId)
        {
            if (ColumnDataItems.Any(x => x.ColumnId == columnId))
            { return; }
            maxListId++;
            ColumnDataItems.Add(new CustomReportColumnDataItem
            {
                ListId = maxListId,
                ColumnId = columnId,
                ColumnName = columnName,
                Ascending = true,
                SortOrder = -1
            });
            //expand presentation node and column node
            CollapsedListIds.Remove(-5);
            CollapsedListIds.Remove(-6);
        }
        public void Remove(int listId)
        {

            //remove field values
            DataItems.ForEach(field =>
                {
                    if (field.Values.Any(value => value.ListId == listId))
                    {
                        field.Values.RemoveAll(value => value.ListId == listId);

                        //remove the field and all parent listIds from collapsed list
                        CollapsedListIds.Remove(field.ListId);
                        CollapsedListIds.Remove(CustomReportFieldType.GetListIdforFieldType(field.FieldType));
                        //-1 for search parameters nodes
                        CollapsedListIds.Remove(-1);
                    }
                });

            //remove from collapsed listids that are removed directly
            CollapsedListIds.Remove(listId);

            //remove fields if listid matches or has no values
            DataItems.RemoveAll(x => x.ListId == listId || !x.Values.Any());
            
            //if a column is removed expand columns node and remvoe the column
            if (ColumnDataItems.Any(column => column.ListId == listId))
            {
                //expand report presentation node
                CollapsedListIds.Remove(-5);
                //expand columns node
                CollapsedListIds.Remove(-6);
                // Remove Columns
                ColumnDataItems.RemoveAll(x => x.ListId == listId);
            }

            ResetListIds();
        }
        public void Remove(List<int> listIds)
        {
            //remove filed values
            DataItems.ForEach(field =>
                {
                    if (field.Values.Any(value => listIds.Contains(value.ListId)))
                    {
                        field.Values.RemoveAll(value => listIds.Contains(value.ListId));

                        //remove the field and all parent listIds from collapsed list
                        CollapsedListIds.Remove(field.ListId);
                        CollapsedListIds.Remove(CustomReportFieldType.GetListIdforFieldType(field.FieldType));
                        //-1 for search parameters nodes
                        CollapsedListIds.Remove(-1);
                    }
                    
                });

            //remove from collapsed listids that are removed directly
            CollapsedListIds.RemoveAll(x => listIds.Contains(x));

            //remove fields if listid matches or has no values
            DataItems.RemoveAll(x => listIds.Contains(x.ListId) || !x.Values.Any());

            //remove columns
            //if a column is removed expand columns node and remvoe the column
            if (ColumnDataItems.Any(column => listIds.Contains(column.ListId)))
            {
                //expand report presentation node
                CollapsedListIds.Remove(-5);
                //expand columns node
                CollapsedListIds.Remove(-6);
                // Remove Columns
                ColumnDataItems.RemoveAll(x => listIds.Contains(x.ListId));
            }

            ResetListIds();
        }
        public void MoveColumns(List<int> listIds, bool direction) 
        {
            if (direction) { listIds = listIds.OrderBy(x => x).ToList(); }
            else { listIds = listIds.OrderByDescending(x => x).ToList(); }
            foreach (int listId in listIds)
            {
                CustomReportColumnDataItem currentColumn = ColumnDataItems.FirstOrDefault(x => x.ListId == listId);
                CustomReportColumnDataItem swapItem = currentColumn;
                if (direction)
                {
                    if (ColumnDataItems.Any(x => x.ListId < listId))
                    { swapItem = ColumnDataItems.OrderBy(x => x.ListId).LastOrDefault(x => x.ListId < listId); }
                }
                else
                {
                    if (ColumnDataItems.Any(x => x.ListId > listId))
                    { swapItem = ColumnDataItems.OrderBy(x => x.ListId).FirstOrDefault(x => x.ListId > listId); }
                }
                currentColumn.ListId = swapItem.ListId;
                swapItem.ListId = listId;
            }

            //expand presentation node and column node
            CollapsedListIds.Remove(-5);
            CollapsedListIds.Remove(-6);

            ResetListIds();
        }
        public void SetSortOrder(List<KeyValuePair<int, bool>> sortColumns)
        {
            int priority = 1;
            ColumnDataItems.ForEach(x => x.SortOrder = -1);
            foreach (KeyValuePair<int, bool> sortColumn in sortColumns)
            {
                CustomReportColumnDataItem colData = ColumnDataItems.FirstOrDefault(x => x.ListId == sortColumn.Key);
                colData.SortOrder = priority;
                colData.Ascending = sortColumn.Value;
                priority++;
            }

            //expand presentation node and sort order node
            CollapsedListIds.Remove(-5);
            CollapsedListIds.Remove(-7);
        }

        private void ResetListIds()
        {
            if (CollapsedListIds == null)
            { CollapsedListIds = new List<int>(); }
            maxListId = 0;
            foreach (CustomReportDataItem dataItem in DataItems)
            {
                maxListId++;
                if (CollapsedListIds.Contains(dataItem.ListId))
                {
                    CollapsedListIds.Remove(dataItem.ListId);
                    CollapsedListIds.Add(maxListId);
                }
                dataItem.ListId = maxListId;
                dataItem.Values = dataItem.Values.OrderBy(x => x.Not).ToList();
                foreach (CustomReportValueItem valueItem in dataItem.Values)
                {
                    maxListId++;
                    valueItem.ListId = maxListId;
                }
            }
            ColumnDataItems = ColumnDataItems.OrderBy(x => x.ListId).ToList();
            foreach (CustomReportColumnDataItem columnDataItem in ColumnDataItems)
            {
                maxListId++;
                columnDataItem.ListId = maxListId;
            }
            List<CustomReportColumnDataItem> sortList = ColumnDataItems.Where(x => x.SortOrder > 0).OrderBy(x => x.SortOrder).ToList();
            int sortOrderNum = 0;
            foreach (CustomReportColumnDataItem sortColumn in sortList)
            {
                sortOrderNum++;
                int listId = sortColumn.ListId;
                ColumnDataItems.FirstOrDefault(x => x.ListId == listId).SortOrder = sortOrderNum;
            }

        }
        private void PopulateFromXml(string xml)
        {
            DataItems = new List<CustomReportDataItem>();
            ColumnDataItems = new List<CustomReportColumnDataItem>();
            XDocument xDoc = XDocument.Parse(xml);
            XElement root = xDoc.Element("CustomReportData");
            List<XElement> xfieldTypes = root.Elements("fieldType").ToList();
            foreach (XElement xfieldType in xfieldTypes)
            {
                List<XElement> xfields = xfieldType.Elements("field").ToList();
                foreach (XElement xfield in xfields)
                {
                    string fieldDataType = xfield.Attribute("fieldDataType").Value;
                    int fieldId = Int32.Parse(xfield.Attribute("fieldId").Value);
                    string fieldName = xfield.Attribute("fieldName").Value;
                    string fieldType = xfield.Attribute("fieldType").Value;

                    int listId = Int32.Parse(xfield.Attribute("listId").Value);
                    if (listId > maxListId) { maxListId = listId; }
                    CustomReportDataItem dataItem = new CustomReportDataItem
                    {
                        FieldDataType = fieldDataType,
                        FieldId = fieldId,
                        FieldName = fieldName,
                        FieldType = fieldType,
                        ListId = listId,
                        Values = new List<CustomReportValueItem>()
                    };

                    List<XElement> xValues = xfield.Elements("value").ToList();
                    foreach (XElement xValue in xValues)
                    {
                        bool not = bool.Parse(xValue.Attribute("not").Value);
                        listId = Int32.Parse(xValue.Attribute("listId").Value);
                        if (listId > maxListId) { maxListId = listId; }
                        CustomReportValueItem valueItem = new CustomReportValueItem { Not = not, ListId = listId };
                        switch (fieldDataType)
                        {
                            case "date":
                                if (xValue.Attributes("from").Count() > 0)
                                { valueItem.DateFromValue = DateTime.Parse(xValue.Attribute("from").Value); }
                                else
                                { valueItem.DateFromValue = DateTime.MinValue; }
                                if (xValue.Attributes("to").Count() > 0)
                                { valueItem.DateToValue = DateTime.Parse(xValue.Attribute("to").Value); }
                                else
                                { valueItem.DateToValue = DateTime.MaxValue; }
                                valueItem.Name = xValue.Attribute("name").Value;
                                break;
                            case "string":
                                valueItem.Value = xValue.Value;
                                valueItem.Name = xValue.Value;
                                if (xValue.Attribute("name") != null && !String.IsNullOrWhiteSpace(xValue.Attribute("name").Value))
                                {
                                    valueItem.Name = xValue.Attribute("name").Value;
                                }
                                break;
                            case "number":
                                valueItem.Value = xValue.Attribute("value").Value;
                                valueItem.Name = xValue.Attribute("value").Value;
                                break;
                            case "dropdown":
                            case "treedropdown":
                                valueItem.Value = xValue.Attribute("value").Value;
                                valueItem.Name = xValue.Attribute("name").Value;
                                break;
                            default:
                                valueItem.Value = xValue.Attribute("value").Value;
                                break;
                        }
                        dataItem.Values.Add(valueItem);
                    }
                    DataItems.Add(dataItem);
                }
            }
            XElement xColumnsElement = root.Element("columns");
            List<XElement> xColumns = xColumnsElement.Elements("column").ToList();
            foreach (XElement xColumn in xColumns)
            {
                int listId = Int32.Parse(xColumn.Attribute("listId").Value);
                if (listId > maxListId) { maxListId = listId; }
                int columnId = Int32.Parse(xColumn.Attribute("columnId").Value);
                string columnName = xColumn.Attribute("columnName").Value;
                int sortOrder = Int32.Parse(xColumn.Attribute("sortOrder").Value);
                bool ascending = bool.Parse(xColumn.Attribute("ascending").Value);
                ColumnDataItems.Add(new CustomReportColumnDataItem
                {
                    ListId = listId,
                    ColumnId = columnId,
                    ColumnName = columnName,
                    SortOrder = sortOrder,
                    Ascending = ascending
                });

            }
        }
        private XElement GetXmlForFieldType(string fieldType)
        {
            XElement root = new XElement("fieldType", new XAttribute("name", fieldType.ToString()));
            List<CustomReportDataItem> fieldDataItems = DataItems.Where(x => x.FieldType == fieldType).ToList();
            foreach (CustomReportDataItem fieldDataItem in fieldDataItems)
            {
                XElement xField = new XElement("field",
                    new XAttribute("name", fieldDataItem.FieldName),
                    new XAttribute("fieldId", fieldDataItem.FieldId),
                    new XAttribute("fieldDataType", fieldDataItem.FieldDataType),
                    new XAttribute("fieldName", fieldDataItem.FieldName),
                    new XAttribute("fieldType", fieldDataItem.FieldType),
                    new XAttribute("listId", fieldDataItem.ListId));
                foreach (CustomReportValueItem valueItem in fieldDataItem.Values)
                {
                    XElement xValue = new XElement("value",
                        new XAttribute("listId", valueItem.ListId),
                        new XAttribute("not", valueItem.Not));
                    switch (fieldDataItem.FieldDataType)
                    {
                        case "date":
                            if (valueItem.DateFromValue != DateTime.MinValue)
                            {
                                //  xValue.SetAttributeValue("from", "6/1/2011");
                                xValue.SetAttributeValue("from", valueItem.DateFromValue.ToShortDateString());
                            }
                            if (valueItem.DateToValue != DateTime.MaxValue)
                            {
//                                var mtDiff = 7;
                                // var tempTime = valueItem.DateToValue.AddHours(23 + mtDiff).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                                // var stringMode = tempTime.ToString("dd/MM/yyyy ");
                                // var stringMode = tempTime.ToString("g");

                                // xValue.SetAttributeValue("to", valueItem.DateToValue.AddHours(23 + mtDiff).AddMinutes(59).AddSeconds(59).AddMilliseconds(999));
                                xValue.SetAttributeValue("to", valueItem.DateToValue.ToString("g")); //add milliseconds and stuff
//                                xValue.SetAttributeValue("to", valueItem.DateToValue.ToShortDateString());
                            }
                            xValue.SetAttributeValue("name", valueItem.Name);
                            break;
                        case "string":
                            xValue.Value = valueItem.Value;
                            xValue.SetAttributeValue("name", valueItem.Name);
                            break;
                        case "dropdown":
                        case "treedropdown":
                            xValue.SetAttributeValue("value", valueItem.Value);
                            xValue.SetAttributeValue("name", valueItem.Name);
                            break;
                        default:
                            xValue.SetAttributeValue("value", valueItem.Value);
                            break;
                    }
                    xField.Add(xValue);
                }
                root.Add(xField);
            }
            return root;
        }
        private XElement GetXmlForColumns()
        {
            XElement root = new XElement("columns");
            foreach (CustomReportColumnDataItem column in ColumnDataItems)
            {
                XElement xColumn = new XElement("column",
                    new XAttribute("listId", column.ListId),
                    new XAttribute("columnId", column.ColumnId),
                    new XAttribute("columnName", column.ColumnName),
                    new XAttribute("sortOrder", column.SortOrder),
                    new XAttribute("ascending", column.Ascending));
                root.Add(xColumn);
            }
            return root;
        }
        private string GetCodeNumberShortForm(string codeNumberLongForm)
        {
            if (string.IsNullOrWhiteSpace(codeNumberLongForm))
            {
                return "";
            }
            return codeNumberLongForm.Replace(".0000.00", "").Replace("00.00", "").Replace(".00", "");
        }

    }
}
