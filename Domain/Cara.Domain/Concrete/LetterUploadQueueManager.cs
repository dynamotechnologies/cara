﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.SQS;
using Amazon.SQS.Model;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using log4net;
using Aquilent.Cara.Domain.Aws.Sqs;
using System.Net;   // I forget why I added this

namespace Aquilent.Cara.Domain
{
	public class LetterUploadQueueManager : AbstractEntityManager<LetterUploadQueue>
	{
        #region Member Variables
        private ILog _logger = LogManager.GetLogger(typeof(LetterUploadQueueManager));
        private static string _letterUploadFolder = ConfigurationManager.AppSettings["letterUploadFolder"];
        private static readonly string _letterUploadQueueKey = "cara.queue.letterupload";
        #endregion Member Variables

        #region Constructors
        static LetterUploadQueueManager()
        {
            using (var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>())
            {
                var setting = resourceManager.Get("resource.letterupload.folder");

                if (setting == null)
                {
                    throw new ApplicationException("Missing resource in the database:  resource.letterupload.folder");
                }

                _letterUploadFolder = setting.Value;
            }
        }

        public LetterUploadQueueManager(IRepository<LetterUploadQueue> repository)
			: base(repository)
		{
		}
        #endregion Constructor

        #region Override AbstractEntityManager methods
        public override LetterUploadQueue Get(int uniqueId)
        {
            return Find(x => x.LetterUploadQueueId == uniqueId).SingleOrDefault();
        }

        public override LetterUploadQueue Get(string uniqueName)
        {
            throw new NotImplementedException();
        }

        public override void Add(LetterUploadQueue entity)
        {
 	        throw new NotImplementedException("Use 'Add(LetterUploadQueue, Stream)' instead");
        }
        #endregion Override AbstractEntityManager methods

        #region Public Methods
        /// <summary>
        /// Adds the entity to the repository and saves the file to the filesystem
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="fileStream"></param>
        public void Add(LetterUploadQueue entity, Stream fileStream)
        {
            entity.UploadDate = DateTime.UtcNow;
            entity.StoredFilename = SaveLetterUpload(entity.UserId, entity.Filename, fileStream);

            if (entity.StoredFilename == null)
            {
                throw new ApplicationException("An error occurred saving the uploaded file to the queue.");
            }

            base.Add(entity);
            UnitOfWork.Saved += new UnitOfWorkEventHandler(QueueLetterUpload);
        }

        public bool Requeue(LetterUploadQueue entity)
        {
            bool isRequeueable = false;
            if (entity != null)
            {
                try
                {
                    // Check to see if the file still exists on S3
                    using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                    {
                        S3FileInfo fileInfo = new S3FileInfo(client, _letterUploadFolder, entity.StoredFilename);
                        if (fileInfo.Exists)
                        {
                            AddLetterUploadToQueue(entity);
                            isRequeueable = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    isRequeueable = false;
                }
            }

            return isRequeueable;
        }

        /// <summary>
        /// Retrieves all of the items in the queue for the given phase
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public IList<LetterUploadQueue> GetByPhase(int phaseId)
        {
            return All.Where(x => x.PhaseId == phaseId).OrderByDescending(x => x.UploadDate).ToList();
        }

        public IList<LetterUploadQueue> GetPending()
        {
            string pending = LetterUploadStatus.Pending.ToString();
            return All.Where(x => x.Status == pending).OrderBy(x => x.LetterUploadQueueId).ToList();
        }

        #region Notifications
        /// <summary>
        /// Send email to user who uploaded the letters and let them know that
        /// their file has been processed
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="uploader"></param>
        public void SendQueueEntryProcessedNotification(LetterUploadQueue entry, User uploader)
        {
            var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
            var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
            var emailFrom = resourceManager.Get("resource.support.email");

            SmtpClient smtp = new SmtpClient();

            IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
            string emailSubject;
            string emailBody;
            var emailSubjectCannedText = cannedTextManager.Get("email.letteruploadqueue.processed.subject");
            var emailBodyCannedText = cannedTextManager.Get("email.letteruploadqueue.processed.body");

            supportingInfo["project.number"] = entry.Phase.Project.ProjectNumber;
            supportingInfo["project.name"] = entry.Phase.Project.Name;
            supportingInfo["phase.name"] = entry.Phase.PhaseTypeDesc;
            supportingInfo["entry.filename"] = entry.Filename;
            supportingInfo["entry.lettercount"] = entry.LetterCount.ToString();

            // Replace the tokens in the e-mail
            emailSubjectCannedText.SupportingInfo = supportingInfo;
            emailSubject = emailSubjectCannedText.Resolve();

            emailBodyCannedText.SupportingInfo = supportingInfo;
            emailBody = emailBodyCannedText.Resolve();

            try
            {
                // Build the e-mail and send it
                MailMessage email = new MailMessage(
                    emailFrom.Value, uploader.Email.Trim(),
                    emailSubject, emailBody);
                smtp.Send(email);
            }
            catch (System.Net.Mail.SmtpException se)
            {
                _logger.Error(string.Format("An error occurred sending '{1}' to {0}", uploader.Email, emailSubject), se);
            }

            cannedTextManager.Dispose();
            resourceManager.Dispose();
        }

        /// <summary>
        /// Send email to user who uploaded the letters and let them know that
        /// their file had an error
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="uploader"></param>
        /// <param name="additionalErrorMessage"></param>
        public void SendQueueEntryErrorNotification(LetterUploadQueue entry, User uploader, string additionalErrorMessage)
        {
            var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
            var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
            var emailFrom = resourceManager.Get("resource.support.email");

            SmtpClient smtp = new SmtpClient();

            IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
            string emailSubject;
            string emailBody;
            var emailSubjectCannedText = cannedTextManager.Get("email.letteruploadqueue.error.subject");
            var emailBodyCannedText = cannedTextManager.Get("email.letteruploadqueue.error.body");

            supportingInfo["project.number"] = entry.Phase.Project.ProjectNumber;
            supportingInfo["project.name"] = entry.Phase.Project.Name;
            supportingInfo["phase.name"] = entry.Phase.PhaseTypeDesc;
            supportingInfo["entry.filename"] = entry.Filename;
            supportingInfo["errorMessage"] = additionalErrorMessage == null ? string.Empty : additionalErrorMessage;

            // Replace the tokens in the e-mail
            emailSubjectCannedText.SupportingInfo = supportingInfo;
            emailSubject = emailSubjectCannedText.Resolve();

            emailBodyCannedText.SupportingInfo = supportingInfo;
            emailBody = emailBodyCannedText.Resolve();

            try
            {
                // Build the e-mail and send it
                MailMessage email = new MailMessage(
                    emailFrom.Value, uploader.Email.Trim(),
                    emailSubject, emailBody);
                smtp.Send(email);
            }
            catch (System.Net.Mail.SmtpException se)
            {
                _logger.Error(string.Format("An error occurred sending '{1}' to {0}", uploader.Email, emailSubject), se);
            }

            cannedTextManager.Dispose();
            resourceManager.Dispose();
        }
        #endregion Notifications

        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Saves the uploaded file to the filesystem for later retrieval
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="originalFileName"></param>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        private string SaveLetterUpload(int userId, string originalFileName, Stream inputStream)
        {
            // Write to a file
            //    Format:  yyyymmddhhmm_username_xxx.csv
            string filename = string.Format("{0:yyyyMMddhhmm}_{1}_{2}_{3}", DateTime.Now, userId, new Random().Next(100, 999), originalFileName);

            try
            {
                using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    S3DirectoryInfo rootDirectory = new S3DirectoryInfo(client, _letterUploadFolder);

                    using (var outputStream = rootDirectory.GetFile(filename).OpenWrite())
                    {
                        DocumentManager.WriteStreamToStream(inputStream, outputStream);
                    }
                }
            }
            catch (ApplicationException ae)
            {
                _logger.Error(string.Format("An error occurred attempting to write the file {0}", filename), ae);
                filename = null;
            }

            return filename;
        }
        #endregion Private Methods

        internal void QueueLetterUpload(object sender, UnitOfWorkEventArgs e)
        {
            if (e.ContainsKey("Added"))
            {
                IList<object> entries = (IList<object>) e["Added"];
                foreach (var entry in entries)
                {
                    LetterUploadQueue letterUploadQueue;
                    if (entry is LetterUploadQueue)
                    {
                        letterUploadQueue = (LetterUploadQueue)entry;

                        // Queue the letter
                        AddLetterUploadToQueue(letterUploadQueue);
                    }
                }
            }
        }
  
        private void AddLetterUploadToQueue(LetterUploadQueue letterUploadQueue)
        {
            // Queue the letter
            string queueName;
            var sqs = SqsHelper.GetQueue(_letterUploadQueueKey, out queueName);

            var sqsRequest = new CreateQueueRequest { QueueName = queueName };
            var createQueueResponse = sqs.CreateQueue(sqsRequest);
            string queueUrl = createQueueResponse.QueueUrl;

            var sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = queueUrl,
                MessageBody = letterUploadQueue.LetterUploadQueueId.ToString()
            };
            sqs.SendMessage(sendMessageRequest);
        }
    }
}
