﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Aquilent.Cara.Domain
{
    public class CustomReportDataItem
    {
        public string FieldName { get; set; }
        public int FieldId { get; set; }
        public string FieldType { get; set; }
        public string FieldDataType { get; set; }
        public List<CustomReportValueItem> Values { get; set; }
        public int ListId { get; set; }
    }
}