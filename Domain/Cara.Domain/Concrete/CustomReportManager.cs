﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Report.Personal;
using Aquilent.Cara.Domain;
using System.Text.RegularExpressions;

namespace Aquilent.Cara.Domain.Report
{
    public class CustomReportManager : IDisposable
    {
        private DbConnection connection;
        private bool connected = false;
        public void Open(string dbProvider, string dbConnectionString)
        {
            Close();
            DbProviderFactory dbProFac = DbProviderFactories.GetFactory(dbProvider);
            connection = dbProFac.CreateConnection();
            connection.ConnectionString = dbConnectionString;
            connection.Open();
            connected = true;
        }
        public void Close()
        {
            if (connected)
            {
                connection.Close();
                connected = false;
            }
        }

        public void Dispose()
        {
            Close();
        }

        public List<CustomReportData> RunReport(int reportId, out int total, int userId, int pageNum = 0, int numRows = 0, int isCount = 0)
        {
            total = 0;
            string parameterXml = "";
            if (!connected)
            {
                return null;
            }
            using (var reportManager = ManagerFactory.CreateInstance<MyReportManager>())
            {
                // get the report
                var myReport = reportManager.Get(reportId);

                // get my report parameter
                if (myReport == null)
                { return null; }
                parameterXml = myReport.Parameters;
            }
            return RunReport(parameterXml, out total, userId, pageNum, numRows, isCount) ;

        }
        public List<CustomReportData> RunReport(string parameterXml, out int total, int userId, int pageNum = 0, int numRows = 0, int isCount = 0, bool includeHeader = false)
        {
            if (!connected)
            {
                total = 0;
                return null;
            }
            List<CustomReportData> retVal = new List<CustomReportData>();
            DbCommand command = connection.CreateCommand();
            string commandText = String.Format("EXEC dbo.spsCustomReport @myDoc = '{0}', @pageNum = {1}, @numRows = {2}, @userId = {3}",
                parameterXml, pageNum, numRows, userId);
            command.CommandText = commandText + ", @isCount = 1";
            using (DbDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                total = Convert.ToInt32(reader[0].ToString());
                reader.Close();
            }
            if (isCount != 0)
            { return null; }
            command = connection.CreateCommand();
            command.CommandText = commandText;
            CustomReportParameters parameters = new CustomReportParameters(parameterXml, false);
            CustomReportData header = new CustomReportData();
            List<CustomReportColumnDataItem> columns = parameters.ColumnDataItems;
            if (includeHeader)
            {
                header.Columns = parameters.ColumnDataItems.Select(x => x.ColumnName).ToList();
                retVal.Add(header);
            }
            //CARA-1142 log in the database whenever a custom report is executed
            using (CustomReportLogManager logManager = ManagerFactory.CreateInstance<CustomReportLogManager>())
            {
                CustomReportLog logEntry = new CustomReportLog
                {
                    UserId = userId,
                    ExecutionDate = DateTime.UtcNow,
                    Parameters = parameterXml
                };
                logManager.Add(logEntry);
                logManager.UnitOfWork.Save();
            }
            using (DbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    CustomReportData row = new CustomReportData();
                    for (int i = 0; i < columns.Count; i++)
                    {
                        row.Columns.Add(GetValue(i, columns, reader));
                    }
                    retVal.Add(row);
                }
            }
            return retVal;
        }

        //formats string value according to column
        private string GetValue(int index, List<CustomReportColumnDataItem> columns, DbDataReader reader)
        {
            int fieldIndex = index + reader.VisibleFieldCount - columns.Count;
            string retVal = reader[fieldIndex].ToString();

            switch (columns[index].ColumnId)
            {
                case 36: //first name
                case 37: //last name
                    if (String.IsNullOrWhiteSpace(retVal))
                    { retVal = "<No Data>"; }
                    break;
                case 62: // Concern Text
                case 63: // Response Text
                    // Strip out HTML tags and normalize the leftover line breaks, etc.
                    retVal = Regex.Replace(Regex.Replace(retVal, "(<[^>]*(>|$))+", " "), @"[\s\r\n]+", " ").Replace("&nbsp;","").Trim();
                    // CR-296 Handle special characters
                    retVal = retVal.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&");
                    break;
                case 71: //Preferred Contact Method
                    if (String.IsNullOrWhiteSpace(retVal))
                    { retVal = "Do Not Contact"; }
                    break;
                // CR-296 Handling new line characters in comment text
                case 56: //comment text
                     retVal = ReportManager.CleanCommentText(retVal);
                     break;
            }
            return retVal;
        }
    }
}
