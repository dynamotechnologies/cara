﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class ConcernResponseManager : AbstractEntityManager<ConcernResponse>
    {
        #region Private Members
        private IRepository<ConcernResponsePhase> repositoryPhase;
        #endregion

        #region Constructor
        public ConcernResponseManager(IConcernResponseRepository repository, IRepository<ConcernResponsePhase> repositoryPhase)
			: base(repository)
		{
            // assign the Repository.UnitOfWork to the other IRepository, so that they are sharing the same instance of the objectcontext
            this.repositoryPhase = repositoryPhase;
            this.repositoryPhase.UnitOfWork.Dispose();
            this.repositoryPhase.UnitOfWork = this.UnitOfWork;
		}
        #endregion

        #region Override AbstractEntityService members
        /// <summary>
        /// Returns a concern response with the given concern response ID
		/// </summary>
        /// <param name="name">The ID of the concern response</param>
		/// <returns>ConcernResponse or null</returns>
		public override ConcernResponse Get(int uniqueId)
		{
			return All.Where(x => x.ConcernResponseId == uniqueId).FirstOrDefault();
		}

		public override ConcernResponse Get(string uniqueName)
		{
			throw new NotImplementedException();
		}
		#endregion Override AbstractEntityService members

        #region ConcernResponsePhase Methods
        /// <summary>
        /// Returns the entire repository for further querying
        /// </summary>
        public IQueryable<ConcernResponsePhase> AllConcernResponsePhase
        {
            get
            {
                return repositoryPhase.All;
            }
        }

        /// <summary>
        /// Returns a concern response phase with the given concern response ID
        /// </summary>
        /// <param name="name">The ID of the concern response</param>
        /// <returns>ConcernResponsePhase or null</returns>
        public ConcernResponsePhase GetConcernResponsePhase(int uniqueId)
        {
            return AllConcernResponsePhase.Where(x => x.ConcernResponseId == uniqueId).FirstOrDefault();
        }

        /// <summary>
        /// Returns a concern response phase with the given concern response ID
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <returns>ConcernResponsePhase or null</returns>
        public IQueryable<ConcernResponsePhase> AllConcernResponsePhaseByPhase(int phaseId)
        {
            return AllConcernResponsePhase.Where(x => x.PhaseId == phaseId);
        }
        #endregion

        #region Custom Methods
        /// <summary>
        /// Returns the concern/responses based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <returns>A list of matching concern/response search results</returns>
        public IList<ConcernResponseSearchResult> GetConcernResponses(ConcernResponseSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<ConcernResponseSearchResult> list = (Repository as IConcernResponseRepository).GetConcernResponseSearchResults(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        /// <summary>
        /// Update the sequence of concern response
        /// </summary>
        /// <param name="concernResponseId">The ID of the concern response</param>
        /// <param name="sequence">Sequence Number</param>
        /// <param name="position">Position Name (First, Last, Prev, and Next)</param>
        public void UpdateSequence(int concernResponseId, int? sequence, string position = null)
        {
            (Repository as IConcernResponseRepository).UpdateConcernResponseSequence(concernResponseId, sequence, position);
        }

        /// <summary>
        /// Update the phase sequence of concern response
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <param name="sequence">Sequence Number</param>
        public void UpdatePhaseSequence(int phaseId, int? sequence)
        {
            (Repository as IConcernResponseRepository).UpdateConcernResponsePhaseSequence(phaseId, sequence);
        }
        #endregion
    }
}
