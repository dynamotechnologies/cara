﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain.Report.Personal
{
	public class MyReportManager : AbstractEntityManager<MyReport>
	{
		#region Constructor
        public MyReportManager(IRepository<MyReport> repository)
			: base(repository)
		{
		}
		#endregion Constructor
        
		#region Override AbstractEntityService members
        /// <summary>
        /// Overrides the all to set the username property of the entity
        /// </summary>
        public override IQueryable<MyReport> All
        {
            get
            {
                IQueryable<MyReport> data = Repository.ObjectQuery.AsQueryable();

                if (data != null)
                {
                    // select all user ids from the my report data set
                    List<int> userIds = data.Where(x => x.UserId.HasValue).Select(x => x.UserId.Value).Distinct().ToList();

                    if (userIds != null && userIds.Count > 0)
                    {
                        // get all users in the system containing the user ids
                        IList<User> users;
                        using (var userManager = ManagerFactory.CreateInstance<UserManager>())
                        {
                            users = userManager.All.Where(x => userIds.Contains(x.UserId)).ToList();
                        }

                        if (users != null)
                        {
                            // populate the user name of each entry
                            foreach (MyReport myReport in data)
                            {
                                myReport.Username = users.Where(x => x.UserId == myReport.UserId).FirstOrDefault().Username;
                            }
                        }
                    }
                }

                return data;
            }
        }

		public override MyReport Get(int uniqueId)
		{
			return All.FirstOrDefault(x => x.MyReportId == uniqueId);
		}

		public override MyReport Get(string uniqueName)
		{
            throw new NotImplementedException();
		}
		#endregion Override AbstractEntityService members        
    }
}
