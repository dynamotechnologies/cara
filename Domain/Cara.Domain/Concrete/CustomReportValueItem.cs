﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Aquilent.Cara.Domain
{
    public class CustomReportValueItem
    {
        public string Value { get; set; }
        public string Name { get; set; }
        public DateTime DateFromValue { get; set; }
        public DateTime DateToValue { get; set; }
        public bool Not { get; set; }
        public int ListId { get; set; }
    }
}