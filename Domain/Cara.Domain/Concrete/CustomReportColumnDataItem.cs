﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aquilent.Cara.Domain
{
    public class CustomReportColumnDataItem
    {
        public int ColumnId {get; set;}
        public string ColumnName { get; set; }
        public int ListId {get; set;}
        public bool Ascending {get; set;}
        public int SortOrder {get; set;}
    }
}