﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;

namespace Aquilent.Cara.Domain
{
	public class LetterObjectionManager : AbstractEntityManager<LetterObjection>
	{
		public LetterObjectionManager(IRepository<LetterObjection> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given code ID
		/// </summary>
        /// <param name="name">The ID of the code</param>
		/// <returns>Code or null</returns>
		public override LetterObjection Get(int uniqueId)
		{
			return All.Where(x => x.LetterObjectionId == uniqueId).FirstOrDefault();
		}

		public override LetterObjection Get(string uniqueName)
		{
            throw new NotImplementedException();
		}
		#endregion Override AbstractEntityService members

        public void DeleteAttachment(LetterObjection letterObjection, int docId, DocumentManager documentManager)
        {
			// Remove the attachment for the letter from CARA
			var ltrdoc = letterObjection.LetterObjectionDocuments.FirstOrDefault(ld => ld.DocumentId == docId);
			if (ltrdoc != null)
			{
				letterObjection.LetterObjectionDocuments.Remove(ltrdoc);
			}

            // Delete the documents
           var document = documentManager.Get(docId);
           if (document != null)
           {
               documentManager.Delete(document);
           }
        }
	}
}
