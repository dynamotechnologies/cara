﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class TermListManager : AbstractEntityManager<TermList>
	{
		public TermListManager(IRepository<TermList> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given term list ID
		/// </summary>
        /// <param name="name">The ID of the term list</param>
		/// <returns>TermList or null</returns>
		public override TermList Get(int uniqueId)
		{
			return All.Where(x => x.TermListId == uniqueId).FirstOrDefault();
		}

		public override TermList Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members
	}
}
