﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class OrganizationManager : AbstractEntityManager<Organization>
	{
		public OrganizationManager(IRepository<Organization> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given code ID
		/// </summary>
        /// <param name="name">The ID of the code</param>
		/// <returns>Code or null</returns>
		public override Organization Get(int uniqueId)
		{
			return All.Where(x => x.OrganizationId == uniqueId).FirstOrDefault();
		}

		public override Organization Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

        #region Custom Methods
        /// <summary>
        /// Returns a list of key/value pair of organizations
        /// </summary>
        /// <returns></returns>
        public IList<KeyValuePair<int, string>> GetSelectList()
        {
            IList<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();

            foreach (Organization org in All.ToList().OrderBy(o => o.Name))
            {
                list.Add(new KeyValuePair<int, string>(org.OrganizationId, org.Name));
            }

            return list;
        }
        #endregion
    }
}
