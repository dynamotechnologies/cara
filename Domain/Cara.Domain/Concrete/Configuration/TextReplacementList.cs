﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// configuration class for the TextReplacementList
    /// </summary>
    public class TextReplacementList : ConfigurationElementCollection
    {
        public TextReplacementList()
        {
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new TextReplacementItem();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((TextReplacementItem)element).Exception;
        }

        public TextReplacementItem this[int index]
        {
            get
            {
                return (TextReplacementItem)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public int IndexOf(TextReplacementItem fqce)
        {
            return BaseIndexOf(fqce);
        }

        public void Add(TextReplacementItem fqce)
        {
            BaseAdd(fqce);
            // Add custom code here.
        }

        public void Remove(TextReplacementItem fqce)
        {
            if (BaseIndexOf(fqce) >= 0)
            {
                BaseRemove(fqce.Exception);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
            // Add custom code here.
        }
    }
}