﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// configuration class for the TextReplacementList
    /// </summary>
    public class TextReplacementConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("Items", IsDefaultCollection = false)]
        public TextReplacementList ReplacementItems
        {
            get
            {
                return (TextReplacementList)base["Items"];
            }
        }

        public string ReplaceInvalidCharacters(string text)
        {
            string replacedText = text;
            if (replacedText != null)
            {
                foreach (TextReplacementItem txtReplacement in ReplacementItems)
                {
                    replacedText = replacedText.Replace(txtReplacement.Exception, txtReplacement.Replacement);
                }
            }

            return replacedText;
        }
    }
}