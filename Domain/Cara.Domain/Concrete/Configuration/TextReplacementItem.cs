﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Domain
{
    public class TextReplacementItem : ConfigurationSection
    {
        #region Public Property
        [ConfigurationProperty("exception", IsRequired = true)]
        public string Exception
        {
            get
            {
                return (string)this["exception"];
            }
            set
            {
                this["exception"] = value;
            }
        }
        [ConfigurationProperty("replacement", IsRequired = true)]
        public string Replacement
        {
            get
            {
                return (string)this["replacement"];
            }
            set
            {
                this["replacement"] = value;
            }
        }
        #endregion
    }
}
