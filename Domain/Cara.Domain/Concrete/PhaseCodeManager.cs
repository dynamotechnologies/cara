﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class PhaseCodeManager : AbstractEntityManager<PhaseCode>
	{
		public PhaseCodeManager(IRepository<PhaseCode> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given phase code ID
		/// </summary>
        /// <param name="name">The ID of the code</param>
		/// <returns>PhaseCode or null</returns>
		public override PhaseCode Get(int uniqueId)
		{
			return All.Where(x => x.PhaseCodeId == uniqueId).FirstOrDefault();
		}

		public override PhaseCode Get(string uniqueName)
		{
			return All.Where(x => x.CodeNumber == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

        #region PhaseCode methods
        /// <summary>
        /// Returns CodeCategoryId for the given phase code ID
        /// </summary>
        /// <param name="name">The ID of the code</param>
        /// <returns>CodeCategoryId or 0</returns>
        public int GetCodeCategoryId(int phasecodeId)
        {
            int codeCatId = 0;
            PhaseCode pcode = Get(phasecodeId);

            if (pcode != null)
            {
                codeCatId = pcode.CodeCategoryId;
            }
            return codeCatId;
        }
        #endregion
    }
}
