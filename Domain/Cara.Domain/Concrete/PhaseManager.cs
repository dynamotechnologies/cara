﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using log4net;

using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Domain.Dmd;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Report;
using Aquilent.Framework.Security;

namespace Aquilent.Cara.Domain
{
	public class PhaseManager : AbstractEntityManager<Phase>
	{
        #region Private Members
        private IRepository<PhaseMemberRole> repositoryMember;
		private IRepository<ProjectDocument> repositoryProjectDocument;
        private ILog _logger = LogManager.GetLogger(typeof(PhaseManager));
        #endregion

        #region Constructor
		public PhaseManager(IPhaseRepository repository, IRepository<PhaseMemberRole> repositoryMember, IRepository<ProjectDocument> repositoryProjectDocument)
			: base(repository)
		{
            // assign the Repository.UnitOfWork to the other IRepository, so that they are sharing the same instance of the objectcontext
            this.repositoryMember = repositoryMember;
            this.repositoryMember.UnitOfWork.Dispose();
			this.repositoryMember.UnitOfWork = this.UnitOfWork;

			this.repositoryProjectDocument = repositoryProjectDocument;
            this.repositoryProjectDocument.UnitOfWork.Dispose();
            this.repositoryProjectDocument.UnitOfWork = this.UnitOfWork;
        }
        #endregion
        
        #region Override AbstractEntityService members
        /// <summary>
        /// Returns a phase with the given phase ID
		/// </summary>
        /// <param name="name">The ID of the phase</param>
		/// <returns>Phase or null</returns>
		public override Phase Get(int uniqueId)
		{
			return All.Where(x => x.PhaseId == uniqueId).FirstOrDefault();
		}

		public override Phase Get(string uniqueName)
		{
			throw new NotImplementedException();
		}

        /// <summary>
        /// Deletes an existing phase
        /// </summary>
        /// <param name="entity"></param>
        public override void Delete(Phase entity)
        {
            try
            {
                // Unlink the phase from data mart
                var dmManager = new DataMartManager();
                dmManager.UnlinkPhase(entity.ProjectId, entity.PhaseId, entity.ProjectDocuments);
                dmManager.Dispose();
                
                base.Delete(entity);
            }
            catch (Exception e)
            {
                _logger.Debug("An error occurred deleting a phase", e);
                throw new DomainException("An error occurred deleting a phase", e);
            }
        }

		#endregion Override AbstractEntityService members

        #region Phase Methods
        /// <summary>
        /// Returns all phass with the given phase search filter
        /// </summary>
        /// <param name="filter">filter</param>
        /// <returns>List of PhaseSearchResult or null</returns>
        public IList<PhaseSearchResult> GetPhases(PhaseSearchResultFilter filter)
        {
            return (Repository as IPhaseRepository).GetPhases(filter);
        }

        /// <summary>
        /// Returns a phase summary
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <returns></returns>
        public PhaseSummary GetPhaseSummary(int phaseId)
        {
            return (Repository as IPhaseRepository).GetPhaseSummary(phaseId);
        }
		
		/// <summary>
		/// Used to activate a phase that had been previously archived
		/// </summary>
		/// <param name="phaseId"></param>
		public void Reactivate(int phaseId)
		{
            Phase phase = Get(phaseId);

			if (phase != null)
			{
				phase.Locked = false;
			}
		}

        public IList<PhaseCodesMostUsed> GetMostUsedPhaseCodes(int phaseId)
        {
            return (Repository as IPhaseRepository).GetMostUsedPhaseCodes(phaseId);
        }

        public List<TaskItem> GetTaskAssignments(int userId)
        {
            IList<KeyValuePair<int, int>> userCommentCountByPhase = null;
            IList<KeyValuePair<int, int>> userConcernCountByPhase = null;


            using (CommentManager commentManager = ManagerFactory.CreateInstance<CommentManager>())
            {
                userCommentCountByPhase = commentManager.All
                    .Where(x => x.UserId == userId)
                    .GroupBy(x => x.Letter.PhaseId)
                    .Select(group => new { Key = group.Key, Value = group.Count() })
                    .ToList()
                    .Select(y => new KeyValuePair<int, int>(y.Key, y.Value))
                    .ToList();
            }

            using (ConcernResponseManager concernResponseManager = ManagerFactory.CreateInstance<ConcernResponseManager>())
            {
                userConcernCountByPhase = concernResponseManager.All
                    .Where(x => x.UserId == userId && x.ConcernResponseStatusId != 4)
                    .GroupBy(x => x.Comments.FirstOrDefault().Letter.PhaseId)
                    .Select(group => new { Key = group.Key, Value = group.Count() })
                    .ToList()
                    .Select(y => new KeyValuePair<int, int>(y.Key, y.Value))
                    .ToList();
            


                //var userConcernCountByPhaseTESTEST = concernResponseManager.All     Test code for when something disappears and ends up in the audit table with a duplicate (nonaudit) pk
                //    .Where(x => x.UserId == userId)                                 think it happens when a user interrupts a creation process  
                //    .GroupBy(x => x.Comments.FirstOrDefault().Letter.PhaseId)
                //    //.Select(group => new { Key = group.Key, Value = group.Count() })
                //    //    //.ToList()
                //    //    //    .Select(y => new KeyValuePair<int, int>(y.Key, y.Value))
                //    //    //    .ToList();
                //    //;

                ////var test = userConcernCountByPhaseTESTEST.GetType();
                //;
            }

            List<int> masterPhaseIdList = userCommentCountByPhase.Select(x => x.Key).ToList();
            masterPhaseIdList.AddRange(userConcernCountByPhase.Select(x => x.Key).ToList());


            masterPhaseIdList = masterPhaseIdList.Distinct().ToList();

            List<TaskItem> retVal = All.Where(x => masterPhaseIdList.Contains(x.PhaseId))
                .Select(x =>
                    new TaskItem
                    {
                        UserId = userId,
                        ProjectId = x.Project.ProjectId,
                        ProjectName = x.Project.Name,
                        ProjectNumber = x.Project.ProjectNumber,
                        PhaseId = x.PhaseId,
                        PhaseName = x.Name
                    }).ToList();

            foreach (var item in retVal)
            {
                item.CommentCount = userCommentCountByPhase.FirstOrDefault(y => y.Key == item.PhaseId).Value;
                item.ConcernCount = userConcernCountByPhase.FirstOrDefault(y => y.Key == item.PhaseId).Value;
            }

            return retVal;
        }

        public List<TaskItem> GetReviewableLetters(User user)
        {
            var tasks = new List<TaskItem>();

            var results = ((IPhaseRepository)Repository).GetReviewableLetterCounts(user.UserId);
            foreach (var phase in results)
            {
                if (user.HasSystemPrivilege("PURR") || user.HasUnitPrivilege("PURR", phase.UnitId))
                {
                    var taskItem = new TaskItem
                    {
                        ProjectId = phase.ProjectId,
                        ProjectName = phase.ProjectName,
                        ProjectNumber = phase.ProjectNumber,
                        PhaseId = phase.PhaseId,
                        PhaseName = phase.PhaseName,
                        EarlyAttentionCount = phase.EarlyActionCount.Value,
                        UnpublishedLetterCount = phase.UnpublishedLetterCount.Value,
                        UserId = user.UserId
                    };

                    tasks.Add(taskItem);
                }
            }

            return tasks;
        }

        /// <summary>
        /// Update the phase status (archive/active)
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="action"></param>
        /// <param name="sendToExternalSystem">Indicates whether or not to update external systems with comment period info</param>
		public void Archive(int phaseId, IList<Report.Report> reports, User archiver, bool sendToExternalSystem)
		{
			Phase phase = Get(phaseId);

			if (phase != null)
			{
                // lock the phase
				phase.Locked = true;
                // set it to public
                phase.Private = false;

                if (sendToExternalSystem)
                {
				    // actions after archiving the phase
				    PhaseSummary summary = GetPhaseSummary(phaseId);

                    if (phase != null && summary != null)
                    {
                        DmdManager dmdManager = new DmdManager();

                        try
                        {
                            var dataMartManager = new DataMartManager();
                            dataMartManager.SendPhaseLetterInfo(phase.ProjectId, new DataMartLetterInfo
                            {
                                PhaseId = phaseId,
                                Name = phase.PhaseTypeDesc,
                                StartDate = phase.CommentStart.ToString("yyyy-MM-dd"),
                                FinishDate = phase.CommentEnd.ToString("yyyy-MM-dd"),
                                DuplicateLetterCount = summary.DuplicateLetters,
                                FormLetterCount = summary.FormLetters,
                                FormPlusLetterCount = summary.FormPlusLetters,
                                MasterFormLetterCount = summary.MasterFormLetters,
                                TotalLetterCount = summary.TotalLetters,
                                UniqueLetterCount = summary.UniqueLetters
                            });
                            dataMartManager.Dispose();
                        }
                        catch (Exception e)
                        {
                            _logger.Debug("An error occurred sending the letter info to the Datamart", e);
                            throw new DomainException("An error occurred sending the letter info to the Datamart");
                        }

    #if !DEBUG
					    // Send the reports to the DMD
					    foreach (var report in reports)
					    {
						    try
						    {
							    dmdManager.PutReport(phase, report, archiver);

						    }
						    catch (Exception e)
						    {
							    _logger.Debug("An error occurred sending the report to the DMD", e);
							    throw new DomainException("An error occurred sending the report to the DMD", e);
						    }
					    }
    #endif
                    }
                }
			}
		}

        /// <summary>
        /// Update the phase accessibility status (all/team only)
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="action"></param>
        public void UpdateAccessStatus(int phaseId, string action)
        {
            Phase phase = All.Where(x => x.PhaseId == phaseId).FirstOrDefault();

            if (phase != null)
            {
                phase.Private = (action.Equals("All") ? false : true);
            }
        }

		public void UpdateReadingRoomStatus(Phase phase, bool isActive)
		{
            phase.ReadingRoomActive = isActive;

            using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
            {
                ICollection<Phase> phases = null;
                if (phase.Project != null)
                {
                    phases = phase.Project.Phases;
                }
                else
                {
                    phases = projectManager.Get(phase.ProjectId).Phases;
                }

                // Check to see if all the phases have the same reading room active status
                bool allTheSame = true;
                foreach (var p in phases)
                {
                    if (isActive != p.ReadingRoomActive)
                    {
                        allTheSame = false;
                        break;
                    }
                }

                // If all phases have the same reading room status, update the Datamart
                if (allTheSame)
                {
                    var dmManager = new DataMartManager();
                    dmManager.ProjectManager = projectManager;
                    dmManager.SetReadingRoomStatus(phase.ProjectId, isActive);
                    dmManager.Dispose();
                }
            }
		}
        #endregion

        #region PhaseMemberRole Methods
        /// <summary>
        /// Returns the entire repository for further querying
        /// </summary>
        public IQueryable<PhaseMemberRole> AllPhaseMemberRole
        {
            get
            {
                return repositoryMember.All;
            }
        }

        /// <summary>
        /// Returns a concern response phase with the given user ID
        /// </summary>
        /// <param name="name">The ID of the user</param>
        /// <returns>PhaseMemberRole or null</returns>
        public PhaseMemberRole GetPhaseMemberRole(int uniqueId)
        {
            return AllPhaseMemberRole.Where(x => x.UserId == uniqueId).FirstOrDefault();
        }

        /// <summary>
        /// Returns all phase member roles with the given phase ID
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <returns>List of PhaseMemberRole or null</returns>
        public IQueryable<PhaseMemberRole> AllPhaseMemberRoleByPhase(int phaseId)
        {
            return AllPhaseMemberRole.Where(x => x.PhaseId == phaseId);
        }

        /// <summary>
        /// Add a new phase member role
        /// </summary>
        /// <param name="pmr"></param>
        public void AddPhaseMemberRole(PhaseMemberRole pmr)
        {
            repositoryMember.Add(pmr);
        }

        /// <summary>
        /// Delete an existing phase member role
        /// </summary>
        /// <param name="pmr"></param>
        public void DeletePhaseMemberRole(PhaseMemberRole pmr)
        {
            repositoryMember.Delete(pmr);
        }
        #endregion

        #region PhaseCode Methods
        /// <summary>
        /// Returns all phase codes with the given phase ID
        /// </summary>
        /// <param name="phaseId">The ID of the phase</param>
        /// <returns>List of PhaseCodeSelect or null</returns>
        public IList<PhaseCodeSelect> GetPhaseCodes(PhaseCodeSelectFilter filter)
        {
            return (Repository as IPhaseRepository).GetPhaseCodeSelects(filter).OrderBy(x => x.CodeNumber.Length).ThenBy(x => x.CodeNumber).ToList();
        }

        /// <summary>
        /// Insert the phase codes from coding template or phase or ids
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="phaseId"></param>
        /// <param name="codeIds"></param>
        /// <param name="targetPhaseId"></param>
        public void InsertPhaseCodes(int? codingTemplateId, int? phaseId, string codeIds, int targetPhaseId)
        {
            (Repository as IPhaseRepository).InsertPhaseCodes(codingTemplateId, phaseId, codeIds, targetPhaseId);
        }
        #endregion

        #region TermList Methods
        /// <summary>
        /// Insert the phase term lists specified in the id list
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="termListIdList"></param>
        public void InsertTermLists(int phaseId, string termListIdList)
        {
            (Repository as IPhaseRepository).InsertPhaseTermLists(phaseId, termListIdList);
        }
        #endregion

		#region ProjectDocument Methods
		/// <summary>
		/// Retrieves all of the ProjectDocuments associated with a phase
		/// </summary>
		/// <param name="phaseId"></param>
		/// <returns></returns>
		public IQueryable<ProjectDocument> GetProjectDocuments(int phaseId)
		{
			return repositoryProjectDocument.All.Where(x => x.PhaseId.Value == phaseId);
		}

		/// <summary>
		/// Adds a ProjectDocument to the repository
		/// </summary>
		/// <param name="projectDocument"></param>
		public void AddProjectDocument(ProjectDocument projectDocument)
		{
			repositoryProjectDocument.Add(projectDocument);
		}

		/// <summary>
		/// Removes a ProjectDocument from the repository
		/// </summary>
		/// <param name="projectDocument"></param>
		public void DeleteProjectDocument(ProjectDocument projectDocument)
		{
			repositoryProjectDocument.Delete(projectDocument);
		}
		#endregion ProjectDocument Methods

        #region Sequence Methods
        /// <summary>
        /// Returns the next available sequence number
        /// </summary>
        /// <param name="name">The name of the key</param>
        /// <param name="phaseId">The ID of the phase</param>
        /// <returns>Next available sequence number</returns>
        public int GetNextSequence(string name, int? phaseId = null)
        {
            return (Repository as IPhaseRepository).GetNextSequence(name, phaseId).Value;
        }
        #endregion
    }
}
