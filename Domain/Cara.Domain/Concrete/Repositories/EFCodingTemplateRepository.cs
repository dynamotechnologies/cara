﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a project repository backed by the interface coding template repository.
    /// </summary>
    public class EFCodingTemplateRepository : EFRepository<CodingTemplate>, ICodingTemplateRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFCodingTemplateRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for domain context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region CodingTemplate Methods
        /// <summary>
        /// Implements the GetCodingTemplates interface
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public IList<TemplateCodeSelect> GetTemplateCodeSelects(int codingTemplateId, ObjectParameter templateName)
        {
            // call the method from unit of work context
            return DomainContext.GetTemplateCodeSelects(codingTemplateId, templateName);
        }

        /// <summary>
        /// Implements the UpdateCodingTemplate interface
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <param name="codeIds"></param>
        public void UpdateCodingTemplate(int codingTemplateId, string templateName, string codeIds)
        {
            DomainContext.UpdateCodingTemplate(codingTemplateId, templateName, codeIds);
        }
        #endregion
    }
}
