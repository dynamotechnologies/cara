﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a project repository backed by the interface project repository.
    /// </summary>
    public class EFProjectRepository : EFRepository<Project>, IProjectRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFProjectRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for project context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region Project Methods
        /// <summary>
        /// Implements the GetProjects interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ProjectSearchResult> GetProjectSearchResults(ProjectSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetProjectSearchResults(filter, total);
        }
        #endregion
    }
}
