﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain.Report
{
    /// <summary>
    /// Class for a report repository backed by the interface report repository.
    /// </summary>
    public class EFReportRepository : EFRepository<Report>, IReportRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
		public EFReportRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for letter context
        /// </summary>
        public IReportContext ReportContext
        {
            get
            {
				return (UnitOfWork.Context is IReportContext ? (UnitOfWork.Context as IReportContext) : null);
            }
        }

        #region Report Methods
        /// <summary>
        /// Implements the GetLetters interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        //public IList<TeamMemberReportData> GetTeamMemberReport(int phaseId)
        //{
        //    // call the method from unit of work context
        //    return ReportContext.GetTeamMemberReport(phaseId);
        //}
        #endregion

        #region Standard Report Methods
        public IList<MailingListReportData> GetMailingListReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total)
        {
            // call the method from unit of work context
            return ReportContext.GetMailingListReport(phaseId, PageNum, NumRows, total);
        }

        public IList<TeamMemberReportData> GetTeamMemberReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total)
        {
            // call the method from unit of work context
            return ReportContext.GetTeamMemberReport(phaseId, PageNum, NumRows, total);
        }

        public IList<CommentWithCRInfoReportData> GetCommentWithCRInfoReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total)
        {
            // call the method from unit of work context
            return ReportContext.GetCommentWithCRInfoReport(phaseId, PageNum, NumRows, total);
        }

        public IList<ConcernResponseWithAssignedToReportData> GetConcernResponseWithAssignedToReportData(int phaseId, int? PageNum, int? NumRows, ObjectParameter total)
        {
            // call the method from unit of work context
            return ReportContext.GetConcernResponseWithAssignedToReport(phaseId, PageNum, NumRows, total);
        }

        public IList<CommentWithAnnotationReportData> GetCommentWithAnnotationReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total)
        {
            // call the method from unit of work context
            return ReportContext.GetCommentWithAnnotationReport(phaseId, PageNum, NumRows, total);
        }

        public IList<UntimelyCommentsReportData> GetUntimelyCommentsReport(int phaseId, int? PageNum, int? NumRows, ObjectParameter total)
        {
            // call the method from unit of work context
            return ReportContext.GetUntimelyCommentsReport(phaseId, PageNum, NumRows, total);
        }

        public IList<CommenterOrganizationTypeData> GetCommenterOrganizationTypeReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return ReportContext.GetCommenterOrganizationTypeReport(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<EarlyActionReportData> GetEarlyActionReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return ReportContext.GetEarlyActionReport(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<CommentReportData> GetCommentReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return ReportContext.GetCommentReport(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<ResponseStatusReportData> GetResponseStatusReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return ReportContext.GetResponseStatusReport(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<ConcernResponseByCommenterReportData> GetConcernResponseByCommenterReport(int phaseId, int? option, int? pageNum, int? numRows, ObjectParameter total)
        {
            return ReportContext.GetConcernResponseByCommenterReport(phaseId, option, pageNum, numRows, total).ToList();
        }

        #endregion
    }
}
