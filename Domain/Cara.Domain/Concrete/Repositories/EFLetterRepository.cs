﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a letter repository backed by the interface letter repository.
    /// </summary>
    public class EFLetterRepository : EFRepository<Letter>, ILetterRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFLetterRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for letter context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region Letter Methods
        /// <summary>
        /// Implements the GetLetters interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<LetterSearchResult> GetLetterSearchResults(LetterSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetLetterSearchResults(filter, total);
        }

        /// <summary>
        /// Implements the GetLettersReadingRoom interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<LetterReadingRoomSearchResult> GetLetterReadingRoomSearchResults(LetterReadingRoomSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetLetterReadingRoomSearchResults(filter, total);
        }

        /// <summary>
        /// Sets the publishing status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="publish"></param>
        /// <param name="unpublishReasonId"></param>
        /// <param name="unpublishReason"></param>
        public void UpdatePublishingStatus(int formSetId, bool publish, int? unpublishReasonId, string unpublishReason)
        {
            int rowsUpdated = 1;
            while (rowsUpdated > 0)
            {
                rowsUpdated = DomainContext.UpdateFormSetPublishingStatus(formSetId, publish, unpublishReasonId, unpublishReason);
            }
        }

        /// <summary>
        /// Sets the letter status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="letterStatusId"></param>
        public void UpdateLetterStatus(int formSetId, int letterStatusId)
        {
            int rowsUpdated = 1;
            while (rowsUpdated > 0)
            {
                rowsUpdated = DomainContext.UpdateFormSetLetterStatus(formSetId, letterStatusId);
            }
        }

        /// <summary>
        /// Sets the early attention status for all of the letters in a form set
        /// </summary>
        /// <param name="formSetId"></param>
        /// <param name="earlyAttentionStatusId"></param>
        public void UpdateFormSetEarlyAttentionStatus(int formSetId, int earlyAttentionStatusId)
        {
            int rowsUpdated = 1;
            while (rowsUpdated > 0)
            {
                rowsUpdated = DomainContext.UpdateFormSetEarlyAttentionStatus(formSetId, earlyAttentionStatusId);
            }
        }
        #endregion
    }
}
