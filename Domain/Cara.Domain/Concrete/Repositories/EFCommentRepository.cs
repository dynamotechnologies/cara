﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using System.Xml.Linq;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a comment repository backed by the interface comment repository.
    /// </summary>
    public class EFCommentRepository : EFRepository<Comment>, ICommentRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFCommentRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for comment context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region Comment Methods
        /// <summary>
        /// Implements the GetComments interface
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<CommentSearchResult> GetCommentSearchResults(CommentSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetCommentSearchResults(filter, total);
        }


        public IList<CommentDetails> CommentDetails(XDocument xdoc)
        {
            return DomainContext.CommentDetails(xdoc);
        }

        #endregion
    }
}
