﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    public class EFCommentCodeRepository : EFRepository<CommentCode>,ICommentCodeRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFCommentCodeRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for comment context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        //*Added for comment code sort
        /// <summary>
        /// Update the sequence of comment code
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="phaseCodeId"></param>
        /// <param name="sequence"></param>
        public void UpdateCommentCodeSequence(int commentId, int phaseCodeId, int sequence)
        {
            DomainContext.UpdateCommentCodeSequence(commentId, phaseCodeId, sequence);
        }

    }
}
