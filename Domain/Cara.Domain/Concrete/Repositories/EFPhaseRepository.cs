﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a phase repository backed by the interface phase repository.
    /// </summary>
    public class EFPhaseRepository : EFRepository<Phase>, IPhaseRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFPhaseRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for phase context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region Phase Methods
        /// <summary>
        /// Implements the GetPhases interface
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<PhaseSearchResult> GetPhases(PhaseSearchResultFilter filter)
        {
            return DomainContext.GetPhaseSearchResults(filter);
        }

        /// <summary>
        /// Implements the GetPhaseSummary interface
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public PhaseSummary GetPhaseSummary(int phaseId)
        {
            return DomainContext.GetPhaseSummary(phaseId);
        }

        /// <summary>
        /// Implements the GetPhaseCodeSelects interface
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<PhaseCodeSelect> GetPhaseCodeSelects(PhaseCodeSelectFilter filter)
        {
            // call the method from unit of work context
            return DomainContext.GetPhaseCodeSelects(filter);
        }

        /// Implements the InsertPhaseCodes interface
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="phaseId"></param>
        /// <param name="codeIds"></param>
        /// <param name="targetPhaseId"></param>
        public void InsertPhaseCodes(int? codingTemplateId, int? phaseId, string codeIds, int targetPhaseId)
        {
            DomainContext.InsertPhaseCodes(codingTemplateId, phaseId, codeIds, targetPhaseId);
        }

        /// <summary>
        /// Implements the InsertPhaseTermLists interface
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="termListIdList"></param>
        public void InsertPhaseTermLists(int phaseId, string termListIdList)
        {
            DomainContext.InsertPhaseTermLists(phaseId, termListIdList);
        }

        /// <summary>
        /// Returns the next available sequence number
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public int? GetNextSequence(string name, int? phaseId)
        {
            return DomainContext.GetNextSequence(name, phaseId);
        }

        
        #endregion


        public IList<PhaseCodesMostUsed> GetMostUsedPhaseCodes(int phaseId)
        {
            return DomainContext.GetMostUsedPhaseCodes(phaseId);
        }

        public IList<ReviewableLetterCounts> GetReviewableLetterCounts(int userId)
        {
            return DomainContext.GetReviewableLetterCounts(userId);
        }
    }
}
