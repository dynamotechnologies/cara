﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a concern/response repository backed by the interface concern/response repository.
    /// </summary>
    public class EFConcernResponseRepository : EFRepository<ConcernResponse>, IConcernResponseRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFConcernResponseRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for concern/response context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region ConcernResponse Methods
        /// <summary>
        /// Implements the GetConcernResponses interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ConcernResponseSearchResult> GetConcernResponseSearchResults(ConcernResponseSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetConcernResponseSearchResults(filter, total);
        }

        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="concernResponseId"></param>
        /// <param name="sequence"></param>
        /// <param name="position"></param>
        public void UpdateConcernResponseSequence(int concernResponseId, int? sequence, string position)
        {
            DomainContext.UpdateConcernResponseSequence(concernResponseId, sequence, position);
        }

        /// <summary>
        /// Update the sequence of phase concern/response
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="sequence"></param>
        public void UpdateConcernResponsePhaseSequence(int phaseId, int? sequence)
        {
            DomainContext.UpdateConcernResponsePhaseSequence(phaseId, sequence);
        }
        #endregion
    }
}
