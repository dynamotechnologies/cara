﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Class for a formset repository backed by the interface letter repository.
    /// </summary>
    public class EFFormSetRepository : EFRepository<FormSet>, IFormSetRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public EFFormSetRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Property for letter context
        /// </summary>
        public IDomainContext DomainContext
        {
            get
            {
                return (UnitOfWork.Context is IDomainContext ? (UnitOfWork.Context as IDomainContext) : null);
            }
        }

        #region FormSet Methods
        /// <summary>
        /// Implements the GetFormSets interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        public IList<FormSetSearchResult> GetFormSetSearchResults(FormSetSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetFormSetSearchResults(filter, total);
        }

        /// <summary>
        /// Implements the GetFormSets interface
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        public IList<FormSetLetterSearchResult> GetFormSetLetterSearchResults(FormSetSearchResultFilter filter, ObjectParameter total)
        {
            // call the method from unit of work context
            return DomainContext.GetFormSetLetterSearchResults(filter, total);
        }
        #endregion
    }
}
