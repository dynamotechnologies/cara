﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using log4net;

using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.DataMart;

namespace Aquilent.Cara.Domain
{
	public class ProjectManager : AbstractEntityManager<Project>
	{
        #region Private Members
        private ILog _logger = LogManager.GetLogger(typeof(LetterManager));
        #endregion

        #region Constructors
        public ProjectManager(IProjectRepository repository)
			: base(repository)
		{
		}
        #endregion

        #region Override AbstractEntityService members
        /// <summary>
		/// Returns a user with the given user ID
		/// </summary>
		/// <param name="name">The ID of the user</param>
		/// <returns>User or null</returns>
		public override Project Get(int uniqueId)
		{
			return All.Where(x => x.ProjectId == uniqueId).FirstOrDefault();
		}

		/// <summary>
		/// Returns a user with the given username
		/// </summary>
		/// <param name="uniqueName">The username of the user</param>
		/// <returns>User or null</returns>
		public override Project Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}

        /// <summary>
        /// Deletes an existing project
        /// </summary>
        /// <param name="entity"></param>
        public override void Delete(Project entity)
        {
            try
            {
                var phaseManager = ManagerFactory.CreateInstance<PhaseManager>();  // Don't need to dispose, since it is sharing the unitofwork
                // set the context from the project manager so that the phases can be deleted
                phaseManager.UnitOfWork = this.UnitOfWork;

                // unlink the phases in the project
                if (entity != null && entity.Phases != null)
                {
                    for (int i = entity.Phases.Count - 1; i >= 0; i--)
                    {
                        // delete and unlink the phase
                        phaseManager.Delete(entity.Phases.ElementAt(i));
                    }
                }

                // delete the project
                base.Delete(entity);

                if (entity.IsPalsProject)
                {
                    // Unlink the PALS project from data mart
                    var dataMartManager = new DataMartManager();
                    dataMartManager.UnlinkProject(entity.ProjectId, int.Parse(entity.ProjectNumber));
                    dataMartManager.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.Debug("An error occurred deleting a project", e);
                throw new DomainException("An error occurred deleting a project", e);
            }
        }
		#endregion Override AbstractEntityService members

		#region Custom Methods
		/// <summary>
		/// Returns the projects based on input filterings
		/// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
		/// <returns>A list of matching project search results</returns>
        public IList<ProjectSearchResult> GetProjects(ProjectSearchResultFilter filter, out int total)
		{
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<ProjectSearchResult> list = (Repository as IProjectRepository).GetProjectSearchResults(filter, output);            
            int.TryParse(output.Value.ToString(), out total);

            return list;
		}

		/// <summary>
		/// Given a project number, find and return the corresponding project
		/// </summary>
		/// <param name="projectNumber"></param>
		/// <returns>the matching Project or Null</returns>
		public Project GetByProjectNumber(string projectNumber)
		{
			return All.Where(x => x.ProjectNumber == projectNumber).FirstOrDefault();
		}

        /// <summary>
        /// Given a filter text, find and return the corresponding projects
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IQueryable<Project> FindProjects(string filter)
        {
            return All.Where(x => x.ProjectNumber.Equals(filter) || x.Name.Contains(filter));
        }

        public IQueryable<Project> FindOrmsProjects(string active, string readingRoomActive, string commentInputActive)
        {
            var query = Repository.ObjectQuery.Include("CurrentPhase").AsQueryable();
            query = query.Where(x => x.ProjectTypeId == 3 && x.CurrentPhase != null);

            if (active.Equals("true"))
            {
                query = query.Where(x => x.CurrentPhase.Phase.CommentEnd >= DateTime.Today &&
                    x.CurrentPhase.Phase.CommentStart <= DateTime.Today);
            }

            if (readingRoomActive.Equals("true"))
            {
                query = query.Where(x => x.CurrentPhase.Phase.ReadingRoomActive);
            }

            else
            {
                query = query.Where(x => !x.CurrentPhase.Phase.ReadingRoomActive);
            }

            if (commentInputActive.Equals("true"))
            {
                query = query.Where(x => x.CurrentPhase.Phase.PublicCommentActive);
            }

            else
            {
                query = query.Where(x => !x.CurrentPhase.Phase.PublicCommentActive);
            }

            return query;
        }

        /// <summary>
        /// Returns information for a project based on two parameters.
        /// </summary>
        /// <param name="projectTypeId">if has value, limits projects by project type</param>
        /// <param name="phaseCommentEnd">
        ///     if no value, find projects with current phase's comment end is in the future
        ///     if has value, find projects with current phase's comment end is in the past and less than value
        /// </param>
        /// <returns>Matching projects</returns>
        public IQueryable<Project> FindProjects(int? projectTypeId, DateTime? phaseCommentEnd, string unitIdList)
        {
            var query = Repository.ObjectQuery.Include("CurrentPhase").AsQueryable();

            if (unitIdList != null)
            {
                //query = query.Where(x => x.CurrentPhase.Phase.Project.UnitId == unitIdList && x.CurrentPhase.Phase.PhaseTypeId == 3);
                //query = query.Where(x => (unitIdList.Contains((x.CurrentPhase.Phase.Project.UnitId)) && x.CurrentPhase.Phase.PhaseTypeId == 3));
               //return query;                                    

                string[] unitsArray = unitIdList.Split(',');
                query = query.Where(x => (unitsArray.Any(unit => x.CurrentPhase.Phase.Project.UnitId.StartsWith(unit))) && x.CurrentPhase.Phase.PhaseTypeId == 3);
                return query;
            
            }

            if (projectTypeId.HasValue)
            {
                query = query.Where(x => x.ProjectTypeId == projectTypeId.Value);
            }

            if (!phaseCommentEnd.HasValue)
            {
                query = query.Where(x => x.CurrentPhase.Phase.CommentEnd >= DateTime.UtcNow);
            }
            else
            {
                query = query.Where(x => x.CurrentPhase.Phase.CommentEnd <= DateTime.UtcNow && x.CurrentPhase.Phase.CommentEnd >= phaseCommentEnd.Value);
            }

            return query;
        }

        
		#endregion
	}
}
