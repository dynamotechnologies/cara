﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class WebCommentPageInfoManager : AbstractEntityManager<WebCommentPageInfo>
	{
		public WebCommentPageInfoManager(IRepository<WebCommentPageInfo> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
		/// Returns a user with the given user ID
		/// </summary>
		/// <param name="name">The ID of the user</param>
		/// <returns>User or null</returns>
		public override WebCommentPageInfo Get(int uniqueId)
		{
			return All.Where(x => x.WebCommentPageInfoId == uniqueId).FirstOrDefault();
		}

		/// <summary>
		/// Returns a user with the given username
		/// </summary>
		/// <param name="uniqueName">The username of the user</param>
		/// <returns>User or null</returns>
		public override WebCommentPageInfo Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

		#region Custom Methods
		/// <summary>
		/// Given a PALS project number, find and return the corresponding record
		/// </summary>
		/// <param name="palsId"></param>
		/// <returns>the matching Project or Null</returns>
		public WebCommentPageInfo GetByPalsId(int palsId)
		{
			return All.Where(x => x.PalsId == palsId).FirstOrDefault();
		}
		#endregion
	}
}
