﻿using Aquilent.Cara.Reference;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Objects;
using Aquilent.Cara.Domain.Aws.DynamoDB;
using Amazon.DynamoDBv2.Model;
using System.Configuration;
using log4net;

namespace Aquilent.Cara.Domain
{
	public class FormSetManager : AbstractEntityManager<FormSet>
	{
        private static ILog _logger = LogManager.GetLogger(typeof(FormSetManager));

        public FormSetManager(IFormSetRepository repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		public override IQueryable<FormSet> All
		{
			get
			{
				return Repository.ObjectQuery.Include("MasterFormLetter");
			}
		}

		/// <summary>
        /// Returns a form set with the given ID
		/// </summary>
        /// <param name="name">The ID of the formset</param>
		/// <returns>formset or null</returns>
		public override FormSet Get(int uniqueId)
		{
			return All.Where(x => x.FormSetId == uniqueId).FirstOrDefault();
		}

		public override FormSet Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members

		#region Methods
		/// <summary>
		/// Compares all of the letters in the source FormSet against the master
		/// in the destination FormSet.
		/// 
		/// NOTE:  UnitOfWork.Save IS NOT called in this method.
		/// </summary>
		/// <param name="srcFormSet">Will be null, if resorting Unique letters against destFormSet</param>
		/// <param name="destFormSet"></param>
		/// <returns>The number of letters affected</returns>
        public int Resort(FormSet srcFormSet, FormSet destFormSet, string username = null)
        {
            int numLettersAffected = 0;

            var changeLogManager = ManagerFactory.CreateInstance<FormManagementChangeLogManager>();
            changeLogManager.UnitOfWork = this.UnitOfWork;

            var letterManager = ManagerFactory.CreateInstance<LetterManager>();
            letterManager.UnitOfWork = this.UnitOfWork;

            var letterTypeLookup = LookupManager.GetLookup<LookupLetterType>();
            var formLetterType = letterTypeLookup.Where(x => x.Name == "Form").First();
            var formPlusLetterType = letterTypeLookup.Where(x => x.Name == "Form Plus").First();
            var uniqueLetterType = letterTypeLookup.Where(x => x.Name == "Unique").First();
            LetterType resortedLetterType = uniqueLetterType;

            IQueryable<Letter> letterQuery;
            if (srcFormSet != null)
            {
                letterQuery = letterManager.All
                    .Where(ltr => ltr.PhaseId == srcFormSet.PhaseId && ltr.FormSetId == srcFormSet.FormSetId)
                    .OrderBy(ltr => ltr.LetterId);
            }
            else
            {
                // Null srcFormSet implies all unique letters in the phase
                letterQuery = letterManager.All
                    .Where(ltr => ltr.PhaseId == destFormSet.PhaseId && ltr.LetterTypeId == uniqueLetterType.LetterTypeId)
                    .OrderBy(ltr => ltr.LetterId);
            }

            // Retrieve the destination master form letter metadata
            Letter destMaster = destFormSet.MasterFormLetter;
            DynamoDBHelper dynamo = new DynamoDBHelper();
            List<KeyValuePair<string, AttributeValue>> getItemResult = dynamo.GetItem(
                new List<KeyValuePair<string, AttributeValue>>()
                {
                    new KeyValuePair<string,AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(destMaster.PhaseId)),
                    new KeyValuePair<string,AttributeValue>("LetterId", DynamoDBHelper.GetAttributeValue(destMaster.LetterId))
                });

            // If the dynamo query worked create potential master from query result otherwise create it from the db.
            List<LetterSenderComposite> potentialMasters = null;
            if (getItemResult != null)
            {
                potentialMasters = new List<LetterSenderComposite>()
                {
                    new LetterSenderComposite(getItemResult)
                };
            }
            else
            {
                potentialMasters = new List<LetterSenderComposite>()
                {
                    new LetterSenderComposite(destMaster)
                };
            }

            // Get all of the letter metadata in the source form set
            IQueryable<List<KeyValuePair<string, AttributeValue>>> queryResult;
            if (srcFormSet != null)
            {
                queryResult = dynamo.Query("FormSetId-index",
                    new List<KeyValuePair<string, AttributeValue>>()
                    {
                        new KeyValuePair<string,AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(srcFormSet.PhaseId)),
                        new KeyValuePair<string,AttributeValue>("FormSetId", DynamoDBHelper.GetAttributeValue(srcFormSet.FormSetId))
                    }, null);
            }
            else
            {
                queryResult = dynamo.Query("LetterTypeId-index",
                    new List<KeyValuePair<string, AttributeValue>>()
                    {
                        new KeyValuePair<string,AttributeValue>("PhaseId", DynamoDBHelper.GetAttributeValue(destFormSet.PhaseId)),
                        new KeyValuePair<string,AttributeValue>("LetterTypeId", DynamoDBHelper.GetAttributeValue(uniqueLetterType.LetterTypeId))
                    }, null);
            }

            // If the query executed properly get the metadata from the result 
            // otherwise fragment keys will be retrieved from dynamo or the database as needed
            List<LetterSenderComposite> srcLetterMetaDatas = new List<LetterSenderComposite>();
            if (queryResult != null)
            {
                srcLetterMetaDatas = queryResult.Select(item => new LetterSenderComposite(item)).ToList();
            }

            var srcAndDestAreEqual = srcFormSet != null && srcFormSet.FormSetId == destFormSet.FormSetId;
            IList<Letter> srcFormLetters = new List<Letter>();
            int batchSize = 100;
            int batchNumber = 0;
            do
            {
                srcFormLetters = letterQuery.Skip(batchNumber * batchSize).Take(batchSize).ToList();

                foreach (var letter in srcFormLetters)
                {
                    bool updateDynamo = false;

                    // Make sure you don't compare the master to itself
                    // -- OR --
                    // Make sure you don't compare the master of the srcFormSet
                    if (letter.LetterId == destFormSet.MasterFormId || (srcFormSet != null && letter.LetterId == srcFormSet.MasterFormId))
                    {
                        continue;
                    }

                    LetterSenderComposite curLetterMetaData = srcLetterMetaDatas.FirstOrDefault(x => x.LetterId == letter.LetterId);
                    List<string> newLetterFragKeys = curLetterMetaData == null ? null : curLetterMetaData.FragmentKeys;

                    //resortedLetterType = letterManager.DetectForm(letter, potentialMasters);
                    int oldLetterTypeId = letter.LetterTypeId;
                    double? oldConfidence = letter.Confidence;
                    resortedLetterType = letterManager.DetectForm(letter, potentialMasters, newLetterFragKeys);

                    // Resorting a Form Set against itself
                    if (srcAndDestAreEqual)
                    {
                        // The letter type changed
                        if (resortedLetterType.LetterTypeId != letter.LetterTypeId)
                        {
                            // Set the letter type
                            letter.EarlyActionStatusId = letterManager.TransitionEarlyActionStatusByLetterType(letterTypeLookup[oldLetterTypeId], resortedLetterType, letter);
                            letter.LetterStatusId = letterManager.TransitionLetterStatusByLetterType(letterTypeLookup[oldLetterTypeId], resortedLetterType, letter.LetterStatusId);
                            letter.LetterTypeId = resortedLetterType.LetterTypeId;
                            updateDynamo = true;

                            // Log the change
                            changeLogManager.LogChangeLetterType(letter, oldLetterTypeId, username);

                            // Increment the letter count
                            numLettersAffected++;
                        }
                    }
                    else
                    {
                        // If the letter is a Form Plus in srcFormSet and would be a Form in the destFormSet
                        // -- OR --
                        // If a unique letter would be a Form or Form Plus in the destFormSet
                        if ((oldLetterTypeId == formPlusLetterType.LetterTypeId && resortedLetterType.LetterTypeId == formLetterType.LetterTypeId) ||
                            (oldLetterTypeId == uniqueLetterType.LetterTypeId && resortedLetterType.LetterTypeId != uniqueLetterType.LetterTypeId))
                        {
                            letter.FormSetId = destFormSet.FormSetId;
                            letter.EarlyActionStatusId = letterManager.TransitionEarlyActionStatusByLetterType(letterTypeLookup[oldLetterTypeId], resortedLetterType, letter);
                            letter.LetterStatusId = letterManager.TransitionLetterStatusByLetterType(letterTypeLookup[oldLetterTypeId], resortedLetterType, letter.LetterStatusId);
                            letter.LetterTypeId = resortedLetterType.LetterTypeId;
                            updateDynamo = true;

                            changeLogManager.LogChangeFormSet(letter, srcFormSet, username);
                            numLettersAffected++;
                        }
                        else
                        {
                            // It's possible that the DetectForm method reassigned the letter
                            // to the destFormSet.  We want to make sure that the letter
                            // stays in the srcFormSet with it's original confidence.
                            if (srcFormSet != null)
                            {
                                letter.FormSetId = srcFormSet.FormSetId;
                                letter.Confidence = oldConfidence;
                                updateDynamo = true;
                            }
                        }
                    }
                    if (updateDynamo)
                    {
                        bool success = dynamo.Update(
                            new List<KeyValuePair<string, AttributeValue>>()
                            {
                                new KeyValuePair<string, AttributeValue>
                                    ("PhaseId", DynamoDBHelper.GetAttributeValue(letter.PhaseId)),
                                new KeyValuePair<string, AttributeValue>
                                    ("LetterId", DynamoDBHelper.GetAttributeValue(letter.LetterId))
                            },
                            new List<KeyValuePair<string, AttributeValue>>()
                            {
                                new KeyValuePair<string, AttributeValue>
                                    ("LetterTypeId", DynamoDBHelper.GetAttributeValue(letter.LetterTypeId))
                            }
                        );

                        if (!success)
                        {
                            _logger.Error(string.Format(
                                "Could not update letter ({0}, {1}, {2}) in Dynamo",
                                letter.PhaseId,
                                letter.LetterId,
                                letter.LetterTypeId));
                        }
                    }
                }

                batchNumber++;
            } while (srcFormLetters.Count == batchSize);

            return numLettersAffected;
        }
		#endregion Methods

        #region Custom Methods
        /// <summary>
        /// Returns the letters based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <param name="totalFormSets">Total form set count</param>
        /// <returns>A list of matching letter search results</returns>
        public IList<FormSetSearchResult> GetFormSetSearchResults(FormSetSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<FormSetSearchResult> list = (Repository as IFormSetRepository).GetFormSetSearchResults(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }

        /// <summary>
        /// Returns the letters based on input filterings
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="total">Total record count</param>
        /// <param name="totalFormSets">Total form set count</param>
        /// <returns>A list of matching letter search results</returns>
        public IList<FormSetLetterSearchResult> GetFormSetLetterSearchResults(FormSetSearchResultFilter filter, out int total)
        {
            var output = new ObjectParameter("totalRows", typeof(int));

            IList<FormSetLetterSearchResult> list = (Repository as IFormSetRepository).GetFormSetLetterSearchResults(filter, output);
            int.TryParse(output.Value.ToString(), out total);

            return list;
        }
        #endregion
    }
}
