﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Domain
{
	public class CodeManager : AbstractEntityManager<Code>
	{
		public CodeManager(IRepository<Code> repository)
			: base(repository)
		{
		}

		#region Override AbstractEntityService members
		/// <summary>
        /// Returns a term list with the given code ID
		/// </summary>
        /// <param name="name">The ID of the code</param>
		/// <returns>Code or null</returns>
		public override Code Get(int uniqueId)
		{
			return All.Where(x => x.CodeId == uniqueId).FirstOrDefault();
		}

		public override Code Get(string uniqueName)
		{
			return All.Where(x => x.Name == uniqueName).FirstOrDefault();
		}
		#endregion Override AbstractEntityService members
	}
}
