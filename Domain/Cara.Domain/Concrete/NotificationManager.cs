﻿using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;
using Aquilent.Framework.Security;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using log4net;
using System;
using System.Configuration;
using System.Text;

namespace Aquilent.Cara.Domain
{
	public static class NotificationManager
	{
		public static ILog _logger = LogManager.GetLogger(typeof(NotificationManager));

		/// <summary>
		/// Sends notifications to active team members and comment period managers
		/// of ending phases.
		/// </summary>
		/// <param name="phasesEndingSoon">Phases that are ending soon</param>
		public static void SendPhaseEndingSoonNotifications(IQueryable<Phase> phasesEndingSoon)
		{
			var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
			var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
			var emailFrom = resourceManager.Get("resource.support.email");
			var userManager = ManagerFactory.CreateInstance<UserManager>();

			SmtpClient smtp = new SmtpClient();

			IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
			string emailSubject;
			string emailBody;
			var emailSubjectCannedText = cannedTextManager.Get("email.phaseend.subject");
			var emailBodyCannedText = cannedTextManager.Get("email.phaseend.body");
			foreach (Phase phase in phasesEndingSoon)
			{
				// Get the active team members
				var phaseMemberRoles = phase.PhaseMemberRoles.Where(x => x.Active == true).OrderBy(x => x.UserId).ToList();
				var teamMembers = userManager.Find(y => phaseMemberRoles.Select(x => x.UserId).Contains(y.UserId)).OrderBy(x => x.UserId);

				supportingInfo["project.name"] = phase.Project.Name;
				supportingInfo["phase.name"] = phase.PhaseTypeDesc;
				supportingInfo["phase.commentend"] = phase.CommentEnd.ToString("d");

				foreach (var teamMember in teamMembers)
				{
					if (teamMember.NotifyCommentPeriodEnd)
					{
						// Replace the tokens in the e-mail
						emailSubjectCannedText.SupportingInfo = supportingInfo;
						emailSubject = emailSubjectCannedText.Resolve();

						emailBodyCannedText.SupportingInfo = supportingInfo;
						emailBody = emailBodyCannedText.Resolve();

						try
						{
							// Build the e-mail and send it
							MailMessage email = new MailMessage(
								emailFrom.Value, teamMember.Email.Trim(),
								emailSubject, emailBody);
							smtp.Send(email);
						}
						catch (System.Net.Mail.SmtpException se)
						{
							_logger.Error(string.Format("An error occurred sending '{1}' to {0}", teamMember.Email, emailSubject), se);
						}
					}
				}
			}

            userManager.Dispose();
            cannedTextManager.Dispose();
            resourceManager.Dispose();
		}

		/// <summary>
		/// Sends notifications to active team members and comment period managers
		/// of starting phases.
		/// </summary>
		/// <param name="phasesStarting"></param>
		public static void SendPhaseStartingNotifications(IQueryable<Phase> phasesStarting)
		{
			var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
			var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
			var emailFrom = resourceManager.Get("resource.support.email");
			var userManager = ManagerFactory.CreateInstance<UserManager>();

			SmtpClient smtp = new SmtpClient();

			IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
			string emailSubject;
			string emailBody;
			var emailSubjectCannedText = cannedTextManager.Get("email.phasestart.subject");
			var emailBodyCannedText = cannedTextManager.Get("email.phasestart.body");
			foreach (Phase phase in phasesStarting)
			{
				// Get the active team members
				var phaseMemberRoles = phase.PhaseMemberRoles.Where(x => x.Active == true).OrderBy(x => x.UserId).ToList();
				var teamMembers = userManager.Find(y => phaseMemberRoles.Select(x => x.UserId).Contains(y.UserId)).OrderBy(x => x.UserId);

				// Add supporting info
				supportingInfo["project.name"] = phase.Project.Name;
				supportingInfo["phase.name"] = phase.PhaseTypeDesc;
				supportingInfo["phase.commentstart"] = phase.CommentStart.ToString("d");

				foreach (var teamMember in teamMembers)
				{
					if (teamMember.NotifyCommentPeriodOpen)
					{
						// Replace the tokens in the e-mail
						emailSubjectCannedText.SupportingInfo = supportingInfo;
						emailSubject = emailSubjectCannedText.Resolve();

						emailBodyCannedText.SupportingInfo = supportingInfo;
						emailBody = emailBodyCannedText.Resolve();

						try
						{
							// Build the e-mail and send it
							MailMessage email = new MailMessage(
								emailFrom.Value, teamMember.Email.Trim(),
								emailSubject, emailBody);
							smtp.Send(email);
						}
						catch (System.Net.Mail.SmtpException se)
						{
							_logger.Error(string.Format("An error occurred sending '{1}' to {0}", teamMember.Email, emailSubject), se);
						}
					}
				}
			}

            userManager.Dispose();
            cannedTextManager.Dispose();
            resourceManager.Dispose();
		}

		public static void SendAddedToPhaseNotification(PhaseMemberRole pmr)
		{
			var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
			var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
			var emailFrom = resourceManager.Get("resource.support.email");

			var user = pmr.User;

			if (user.NotifyAddedProject)
			{
				var phase = pmr.Phase;
				var project = phase.Project;

				SmtpClient smtp = new SmtpClient();

				IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
				string emailSubject;
				string emailBody;
				var emailSubjectCannedText = cannedTextManager.Get("email.addproject.subject");
				var emailBodyCannedText = cannedTextManager.Get("email.addproject.body");

				// Add supporting info
				supportingInfo["project.name"] = project.Name;
				supportingInfo["phase.name"] = phase.PhaseTypeDesc;
				supportingInfo["phase.rolename"] = pmr.RoleName;

				emailSubjectCannedText.SupportingInfo = supportingInfo;
				emailSubject = emailSubjectCannedText.Resolve();

				emailBodyCannedText.SupportingInfo = supportingInfo;
				emailBody = emailBodyCannedText.Resolve();

				// Build the e-mail and send it
				try
				{
					MailMessage email = new MailMessage(
						emailFrom.Value, user.Email.Trim(),
						emailSubject, emailBody);
					smtp.Send(email);
				}
				catch (System.Net.Mail.SmtpException se)
				{
					_logger.Error(string.Format("An error occurred sending '{1}' to {0}", user.Email, emailSubject), se);
				}
			}

            cannedTextManager.Dispose();
            resourceManager.Dispose();
		}

		public static void SendNewLettersArrivedNotification(Phase p, IList<int?> letterSequences)
		{
            var userManager = ManagerFactory.CreateInstance<UserManager>();
            var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
            var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
            var phaseManager = ManagerFactory.CreateInstance<PhaseManager>();
            var phase = phaseManager.Get(p.PhaseId);

            try
            {
                var pmrs = phase.PhaseMemberRoles.Where(x => x.Active == true).ToList();
                var users = userManager.Find(x => pmrs.Select(y => y.UserId).Contains(x.UserId) && x.NotifySubmissionsReceived == true);

                var emailFrom = resourceManager.Get("resource.support.email");
                var emailSubjectCannedText = cannedTextManager.Get("email.newletters.subject");
                var emailBodyCannedText = cannedTextManager.Get("email.newletters.body");

                IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
                string emailSubject;
                string emailBody;
                MailMessage email;
                SmtpClient smtp = new SmtpClient();

                // Add supporting info
                supportingInfo["project.name"] = phase.Project.Name;
                supportingInfo["phase.name"] = phase.PhaseTypeDesc;

                foreach (var letterSequence in letterSequences)
                {
                    foreach (var user in users)
                    {
                        // Not time to send a notification
                        if (letterSequence % user.SubmissionsRecv != 0)
                        {
                            continue;
                        }

                        // Add supporting info
                        supportingInfo["num.letters"] = user.SubmissionsRecv.ToString();

                        emailSubjectCannedText.SupportingInfo = supportingInfo;
                        emailSubject = emailSubjectCannedText.Resolve();

                        emailBodyCannedText.SupportingInfo = supportingInfo;
                        emailBody = emailBodyCannedText.Resolve();

                        try
                        {
                            // Build the e-mail and send it
                            email = new MailMessage(
                                emailFrom.Value, user.Email.Trim(),
                                emailSubject, emailBody);
                            smtp.Send(email);
                        }
                        catch (System.Net.Mail.SmtpException se)
                        {
                            _logger.Error(string.Format("An error occurred sending '{1}' to {0}", user.Email, emailSubject), se);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(string.Format(
                    "An error occurred sending the new letter notification for Phase ID: {0}, (First) LetterSequence: {1}",
                    phase.PhaseId, letterSequences[0], ex)
                );
            }
            finally
            {
                phaseManager.Dispose();
                userManager.Dispose();
                cannedTextManager.Dispose();
                resourceManager.Dispose();
            }
		}

        public static void SendEarlyAttentionNotification(Phase phase, Project project, IList<int?> letterNumbers)
        {
            var userManager = ManagerFactory.CreateInstance<UserManager>();
            var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
            var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();

            // getting POCs and subscribers
            var commentPeriodPocIds = phase.PhaseMemberRoles.Where(x => x.Active && x.IsPointOfContact).Select(x => x.UserId).ToList();
            var commentPeriodEarlyAtSubIds = phase.PhaseMemberRoles.Where(x => x.Active && x.IsEarlyAttentionNewsletterSubscriber).Select(x => x.UserId).ToList();

            var commentPeriodPoc = userManager.All.Where(x => commentPeriodPocIds.Contains(x.UserId)).ToList();
            var commentPeriodEarlyAtSubbers = userManager.All.Where(x => commentPeriodEarlyAtSubIds.Contains(x.UserId)).ToList();

            // for Notification log file
            string pocList = "";
            string subberList = "";
            foreach (var poc in commentPeriodPoc)
            {
                pocList += poc.Username + " ";
            }
            foreach (var earlyAtSubber in commentPeriodEarlyAtSubbers)
            {
                subberList += earlyAtSubber.Username + " ";
            }
            _logger.Info(string.Format("{0} POCs - {1}", commentPeriodPocIds.Count, pocList));
            _logger.Info(string.Format("{0} early-attention newsletter subscribers - {1}", commentPeriodEarlyAtSubIds.Count, subberList));
            _logger.Info(string.Format("For Phase.PhaseId {0}, Project.ProjectNumber {1}", phase.PhaseId, project.ProjectNumber));

            var emailFrom = resourceManager.Get("resource.support.email");
            var emailSubjectCannedText = cannedTextManager.Get("email.earlyattention.subject");
            var emailBodyCannedText = cannedTextManager.Get("email.earlyattention.body");

            StringBuilder numbersString = new StringBuilder();
            foreach (var number in letterNumbers)
            {
                numbersString.Append(string.Format("\t{0}\r\n", number));
            }

            IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
            string emailSubject;
            string emailBody;
            MailMessage email;
            SmtpClient smtp = new SmtpClient();

            // Add supporting info
            supportingInfo["project.name"] = project.Name;
            supportingInfo["phase.name"] = phase.PhaseTypeDesc;
            supportingInfo["letter.count"] = letterNumbers.Count.ToString();
            supportingInfo["letter.numbers"] = numbersString.ToString();
            supportingInfo["project.projectnumber"] = project.ProjectNumber;

            emailSubjectCannedText.SupportingInfo = supportingInfo;
            emailSubject = emailSubjectCannedText.Resolve();

            emailBodyCannedText.SupportingInfo = supportingInfo;
            emailBody = emailBodyCannedText.Resolve();

            try
            {
                if (commentPeriodEarlyAtSubbers.Count > 0)
                {
                    // Build the e-mail and send it
                    email = new MailMessage
                    {
                        From = new MailAddress(emailFrom.Value.Trim()),
                        Subject = emailSubject,
                        Body = emailBody
                    };

                    // TENTATIVELY OFF - PALS doesn't get POC anymore (CR-346). poc gets sent newsletter regardless
                    //foreach (var poc in commentPeriodPoc)
                    //{
                    //    email.To.Add(new MailAddress(poc.Email, poc.FullName));
                    //}
                    // newsletter subscribers
                    foreach (var earlyAtSubber in commentPeriodEarlyAtSubbers)
                    {
                        email.To.Add(new MailAddress(earlyAtSubber.Email, earlyAtSubber.FullName));
                    }

                    smtp.Send(email);
                }
            }
            catch (System.Net.Mail.SmtpException se)
            {
                _logger.Error(string.Format("An error occurred sending {0}", emailSubject), se);
            }
            finally
            {
                userManager.Dispose();
                cannedTextManager.Dispose();
                resourceManager.Dispose();
            }
        }

        public static void SendHarmfulContentNotification(Phase phase, Project project, ICollection<PhaseMemberRole> iCollection, IList<int?> letterNumbers)
        {
            var userManager = ManagerFactory.CreateInstance<UserManager>();
            var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
            var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();

            // changed to POC bcs phase manager is deprecated
            var commentPeriodPocIds = phase.PhaseMemberRoles.Where(x => x.Active && x.IsPointOfContact).Select(x => x.UserId).ToList();
            var commentPeriodPocs = userManager.All.Where(x => commentPeriodPocIds.Contains(x.UserId)).ToList();

            // for Notification log file
            string pocList = "";
            foreach (var poc in commentPeriodPocs)
            {
                pocList += poc.Username + " ";
            }
            _logger.Info(string.Format("{0} POCs - {1}", commentPeriodPocIds.Count, pocList));
            _logger.Info(string.Format("For Phase.PhaseId {0}, Project.ProjectNumber {1}", phase.PhaseId, project.ProjectNumber));
  
            var emailFrom = resourceManager.Get("resource.support.email");
            var emailSubjectCannedText = cannedTextManager.Get("email.harmfulcontent.subject");
            var emailBodyCannedText = cannedTextManager.Get("email.harmfulcontent.body");

            StringBuilder numbersString = new StringBuilder();
            foreach (var number in letterNumbers)
            {
                numbersString.Append(string.Format("\t{0}\r\n", number));
            }

            IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
            string emailSubject;
            string emailBody;
            MailMessage email;
            SmtpClient smtp = new SmtpClient();

            // Add supporting info
            supportingInfo["project.name"] = project.Name;
            supportingInfo["phase.name"] = phase.PhaseTypeDesc;
            supportingInfo["letter.count"] = letterNumbers.Count.ToString();
            supportingInfo["letter.numbers"] = numbersString.ToString();
            supportingInfo["project.projectnumber"] = project.ProjectNumber;

            emailSubjectCannedText.SupportingInfo = supportingInfo;
            emailSubject = emailSubjectCannedText.Resolve();

            emailBodyCannedText.SupportingInfo = supportingInfo;
            emailBody = emailBodyCannedText.Resolve();

            try
            {
                // 06-20-2019 added if for count so it won't break the notifcation scan if thre are no POCs - harmful content notifications only get sent to POCs
                if (commentPeriodPocs.Count > 0)
                {                // Build the e-mail and send it
                    email = new MailMessage
                    {
                        From = new MailAddress(emailFrom.Value.Trim()),
                        Subject = emailSubject,
                        Body = emailBody
                    };

                    foreach (var cpm in commentPeriodPocs)
                    {
                        email.To.Add(new MailAddress(cpm.Email, cpm.FullName));
                    }

                    smtp.Send(email);
                }
            }
            catch (System.Net.Mail.SmtpException se)
            {
                _logger.Error(string.Format("An error occurred sending {0}", emailSubject), se);
            }
            finally
            {
                userManager.Dispose();
                cannedTextManager.Dispose();
                resourceManager.Dispose();
            }
        }

        public static void SendWebLinksNotification(int phaseId, User user)
        {
            PhaseManager phaseManager = null;

            try
            {
                phaseManager = ManagerFactory.CreateInstance<PhaseManager>();
                var phase = phaseManager.Get(phaseId);
                var project = phase.Project;

                var userManager = ManagerFactory.CreateInstance<UserManager>();
                var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
                var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();

                var emailFrom = resourceManager.Get("resource.support.email");
                var emailSubjectCannedText = cannedTextManager.Get("email.newcommentperiod.subject");
                var emailBodyCannedText = cannedTextManager.Get("email.newcommentperiod.body");

                IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
                string emailSubject;
                string emailBody;
                MailMessage email;
                SmtpClient smtp = new SmtpClient();

                // Add supporting info
                supportingInfo["project.number"] = project.ProjectNumber;
                supportingInfo["phase.name"] = phase.PhaseTypeDesc;

                emailSubjectCannedText.SupportingInfo = supportingInfo;
                emailSubject = emailSubjectCannedText.Resolve();

                emailBodyCannedText.SupportingInfo = supportingInfo;
                emailBody = emailBodyCannedText.Resolve();

                try
                {
                    // Build the e-mail and send it
                    email = new MailMessage(
                        emailFrom.Value, user.Email.Trim(),
                        emailSubject, emailBody);
                    smtp.Send(email);
                }
                catch (System.Net.Mail.SmtpException se)
                {
                    _logger.Error(string.Format("An error occurred sending '{1}' to {0}", user.Email, emailSubject), se);
                }
                finally
                {
                    userManager.Dispose();
                    cannedTextManager.Dispose();
                    resourceManager.Dispose();
                }
            }
            finally
            {
                if (phaseManager != null)
                {
                    phaseManager.Dispose();
                }
            }
		}

        public static void SendCustomReportCsvNotification(User recipient, string reportSummary, string reportFilename)
        {
            using (var cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>())
            {
                var resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
                var emailFrom = resourceManager.Get("resource.support.email");
                var emailSubjectCannedText = cannedTextManager.Get("email.customreport.csv.subject");
                var emailBodyCannedText = cannedTextManager.Get("email.customreport.csv.body");

                IDictionary<string, string> supportingInfo = new Dictionary<string, string>();
                string emailSubject;
                string emailBody;
                MailMessage email;
                SmtpClient smtp = new SmtpClient();

                // Add supporting info
                supportingInfo["report.summary"] = reportSummary;
                supportingInfo["report.filename"] = reportFilename;
                supportingInfo["report.maxAge"] = ConfigurationManager.AppSettings["customReportMaxAgeInDays"];

                emailSubjectCannedText.SupportingInfo = supportingInfo;
                emailSubject = emailSubjectCannedText.Resolve();

                emailBodyCannedText.SupportingInfo = supportingInfo;
                emailBody = emailBodyCannedText.Resolve();

                try
                {
                    // Build the e-mail and send it
                    email = new MailMessage(
                        emailFrom.Value, recipient.Email,
                        emailSubject, emailBody);
                    smtp.Send(email);
                }
                catch (System.Net.Mail.SmtpException se)
                {
                    _logger.Error(string.Format("An error occurred sending '{1}' to {0}", recipient.Email, emailSubject), se);
                }
                finally
                {
                    cannedTextManager.Dispose();
                    resourceManager.Dispose();
                }
            }
		}

		/// <summary>
		/// Sends notifications to team members when they have been assigned tasks
		/// </summary>
		/// <param name="phasesEndingSoon">Phases that are ending soon</param>
        public static void SendTaskAssignmentNotifications(List<Comment> comments, List<ConcernResponse> concerns)
        {
            #region initialize mail resources
            ResourceSettingManager resourceManager = ManagerFactory.CreateInstance<ResourceSettingManager>();
            CannedTextManager cannedTextManager = ManagerFactory.CreateInstance<CannedTextManager>();
            ResourceSetting emailFrom = resourceManager.Get("resource.support.email");
            UserManager userManager = ManagerFactory.CreateInstance<UserManager>();

            SmtpClient smtp = new SmtpClient();

            IDictionary<string, string> supportingInfo = null; //new Dictionary<string, string>();
            string emailSubject;
            string emailBody;
            CannedText emailSubjectCannedText = cannedTextManager.Get("email.TaskAssignment.subject");
            CannedText emailBodyCannedText = cannedTextManager.Get("email.TaskAssignment.body");
            #endregion initialize mail resources

            #region Get list of all users to E-mail
            List<int> userIds = concerns.Select(x => x.UserId.Value).Union(comments.Select(x => x.UserId.Value)).ToList();
            List<User> users = userManager.All
                .Where(user => userIds.Contains(user.UserId))
                .ToList();
            #endregion Get list of all users to E-mail


            foreach (User user in users)
            {
                supportingInfo = new Dictionary<string, string>();

                #region Get a list of all the projects for this user and generate message
                List<Project> projects = concerns
                    .Where(x => x.UserId == user.UserId)
                    .Select(x => x.Comments.FirstOrDefault().Letter.Phase.Project)
                    .Union(comments
                        .Where(x => x.UserId == user.UserId)
                        .Select(x => x.Letter.Phase.Project))
                    .Distinct()
                    .ToList();
                StringBuilder projectInfo = new StringBuilder();
                foreach (Project project in projects)
                {
                    projectInfo.AppendFormat("{0} ({1})\n\tConcerns: {2}\n\tComments: {3}\n\n",
                        project.Name,
                        project.ProjectNumber,
                        concerns.Where(x => x.UserId == user.UserId &&
                            x.Comments.FirstOrDefault().Letter.Phase.Project.ProjectId == project.ProjectId)
                            .Count(),
                        comments.Where(x => x.UserId == user.UserId &&
                            x.Letter.Phase.Project.ProjectId == project.ProjectId)
                            .Count());
                }
                supportingInfo["projectinfo"] = projectInfo.ToString();
                #endregion Get a list of all the projects for this user

                // Replace the tokens in the e-mail
                emailSubjectCannedText.SupportingInfo = supportingInfo;
                emailSubject = emailSubjectCannedText.Resolve();

                emailBodyCannedText.SupportingInfo = supportingInfo;
                emailBody = emailBodyCannedText.Resolve();

                try
                {
                    // Build the e-mail and send it
                    MailMessage email = new MailMessage(
                        emailFrom.Value, user.Email.Trim(),
                        emailSubject, emailBody);
                    smtp.Send(email);
                }
                catch (System.Net.Mail.SmtpException se)
                {
                    _logger.Error(string.Format("An error occurred sending '{1}' to {0}", user.Email, emailSubject), se);
                }
            }

            userManager.Dispose();
            cannedTextManager.Dispose();
            resourceManager.Dispose();
        }
    }
}
