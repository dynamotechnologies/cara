namespace Aquilent.Cara.Reporting
{
	partial class CodingStructureReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule6 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule7 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule8 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule9 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule10 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule11 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule12 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule13 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.currentTimeTextBox = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
			this.subReport1 = new Telerik.Reporting.SubReport();
			this.reportHeader1 = new Aquilent.Cara.Reporting.ReportHeader();
			this.group1 = new Telerik.Reporting.Group();
			this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
			this.letterIdCaptionTextBox = new Telerik.Reporting.TextBox();
			this.codeCategoryCaptionTextBox = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.codeCategory = new Telerik.Reporting.Group();
			this.groupFooterSection2 = new Telerik.Reporting.GroupFooterSection();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.groupHeaderSection2 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.group3 = new Telerik.Reporting.Group();
			this.groupFooterSection3 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection3 = new Telerik.Reporting.GroupHeaderSection();
			((System.ComponentModel.ISupportInitialize) (this.reportHeader1)).BeginInit();
			((System.ComponentModel.ISupportInitialize) (this)).BeginInit();
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			this.pageHeaderSection1.PrintOnFirstPage = false;
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.4166665077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Coding Structure Report";
			// 
			// detail
			// 
			this.detail.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox6,
            this.textBox4});
			this.detail.Name = "detail";
			this.detail.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// textBox2
			// 
			this.textBox2.CanGrow = true;
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4800001382827759D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.StyleName = "Data";
			this.textBox2.Value = "= Aquilent.Cara.Reporting.ReportUtilities.TrimCode(Fields.CodeNumber)";
			// 
			// textBox6
			// 
			this.textBox6.CanGrow = true;
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3366665840148926D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox6.StyleName = "Data";
			this.textBox6.Value = "=Fields.CommentCount";
			// 
			// textBox4
			// 
			this.textBox4.CanGrow = true;
			formattingRule1.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "1")});
			formattingRule1.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.15000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule2.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "2")});
			formattingRule2.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.25D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule3.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "3")});
			formattingRule3.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.34999999403953552D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule4.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "0")});
			formattingRule4.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.05000000074505806D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.textBox4.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1,
            formattingRule2,
            formattingRule3,
            formattingRule4});
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.5000787973403931D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.8365085124969482D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.StyleName = "Data";
			this.textBox4.Value = "=Fields.CodeName";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.currentTimeTextBox,
            this.pageInfoTextBox});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox1.StyleName = "PageInfo";
			this.textBox1.Value = "Coding Structure Report";
			// 
			// currentTimeTextBox
			// 
			this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Name = "currentTimeTextBox";
			this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.currentTimeTextBox.StyleName = "PageInfo";
			this.currentTimeTextBox.Value = "=NOW()";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.5099999904632568D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
			// 
			// reportHeaderSection1
			// 
			this.reportHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.81000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
			this.reportHeaderSection1.Name = "reportHeaderSection1";
			// 
			// subReport1
			// 
			this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.subReport1.Name = "subReport1";
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "25"));
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Coding Structure Report"));
			this.subReport1.ReportSource = this.reportHeader1;
			this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.81000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch));
			// 
			// group1
			// 
			this.group1.GroupFooter = this.groupFooterSection1;
			this.group1.GroupHeader = this.groupHeaderSection1;
			this.group1.Name = "group1";
			// 
			// groupFooterSection1
			// 
			this.groupFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection1.Name = "groupFooterSection1";
			this.groupFooterSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Double;
			this.groupFooterSection1.Style.BorderWidth.Top = new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Point);
			this.groupFooterSection1.Style.Visible = false;
			// 
			// groupHeaderSection1
			// 
			this.groupHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.56000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.letterIdCaptionTextBox,
            this.codeCategoryCaptionTextBox,
            this.textBox8});
			this.groupHeaderSection1.Name = "groupHeaderSection1";
			this.groupHeaderSection1.PrintOnEveryPage = true;
			this.groupHeaderSection1.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int) (((byte) (51)))), ((int) (((byte) (62)))), ((int) (((byte) (30)))));
			this.groupHeaderSection1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.groupHeaderSection1.Style.BorderWidth.Bottom = new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Point);
			// 
			// letterIdCaptionTextBox
			// 
			this.letterIdCaptionTextBox.CanGrow = true;
			this.letterIdCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3366665840148926D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.letterIdCaptionTextBox.Name = "letterIdCaptionTextBox";
			this.letterIdCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.letterIdCaptionTextBox.StyleName = "Caption";
			this.letterIdCaptionTextBox.Value = "Comment Count";
			// 
			// codeCategoryCaptionTextBox
			// 
			this.codeCategoryCaptionTextBox.CanGrow = true;
			this.codeCategoryCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeCategoryCaptionTextBox.Name = "codeCategoryCaptionTextBox";
			this.codeCategoryCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4800001382827759D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeCategoryCaptionTextBox.StyleName = "Caption";
			this.codeCategoryCaptionTextBox.Value = "Code #";
			// 
			// textBox8
			// 
			this.textBox8.CanGrow = true;
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1100001335144043D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.8365871906280518D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox8.StyleName = "Caption";
			this.textBox8.Value = "Code Name";
			// 
			// textBox9
			// 
			this.textBox9.CanGrow = true;
			formattingRule5.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "1")});
			formattingRule5.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule6.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "2")});
			formattingRule6.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule7.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "3")});
			formattingRule7.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.30000001192092896D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.textBox9.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule5,
            formattingRule6,
            formattingRule7});
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.020000139251351357D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.9799997806549072D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.StyleName = "Data";
			this.textBox9.Value = "Total Comments";
			// 
			// textBox10
			// 
			this.textBox10.CanGrow = true;
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3366665840148926D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.70000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox10.Style.Font.Bold = true;
			this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox10.StyleName = "Data";
			this.textBox10.Value = "=Sum(Fields.CommentCount)";
			// 
			// codeCategory
			// 
			this.codeCategory.GroupFooter = this.groupFooterSection2;
			this.codeCategory.GroupHeader = this.groupHeaderSection2;
			this.codeCategory.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.CodeCategoryId")});
			this.codeCategory.Name = "group2";
			// 
			// groupFooterSection2
			// 
			this.groupFooterSection2.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox7});
			this.groupFooterSection2.Name = "groupFooterSection2";
			this.groupFooterSection2.PageBreak = Telerik.Reporting.PageBreak.None;
			this.groupFooterSection2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.groupFooterSection2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.groupFooterSection2.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// textBox5
			// 
			this.textBox5.CanGrow = true;
			formattingRule8.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "1")});
			formattingRule8.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule9.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "2")});
			formattingRule9.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule10.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "3")});
			formattingRule10.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.30000001192092896D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.textBox5.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule8,
            formattingRule9,
            formattingRule10});
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079960823059082031D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.9799997806549072D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.StyleName = "Data";
			this.textBox5.Value = "=\"Total Comments for \" + Fields.CodeCategoryName";
			// 
			// textBox7
			// 
			this.textBox7.CanGrow = true;
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3366665840148926D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079960502684116364D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.70000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox7.StyleName = "Data";
			this.textBox7.Value = "=RunningValue(\"group2\", Sum(Fields.CommentCount))";
			// 
			// groupHeaderSection2
			// 
			this.groupHeaderSection2.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3});
			this.groupHeaderSection2.Name = "groupHeaderSection2";
			this.groupHeaderSection2.PrintOnEveryPage = true;
			// 
			// textBox3
			// 
			this.textBox3.CanGrow = true;
			formattingRule11.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "1")});
			formattingRule11.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule12.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "2")});
			formattingRule12.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch);
			formattingRule13.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.Level", Telerik.Reporting.Data.FilterOperator.Equal, "3")});
			formattingRule13.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0.30000001192092896D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.textBox3.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule11,
            formattingRule12,
            formattingRule13});
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.StyleName = "Data";
			this.textBox3.Value = "=Fields.CodeCategoryName";
			// 
			// group3
			// 
			this.group3.GroupFooter = this.groupFooterSection3;
			this.group3.GroupHeader = this.groupHeaderSection3;
			this.group3.Name = "group3";
			// 
			// groupFooterSection3
			// 
			this.groupFooterSection3.Height = new Telerik.Reporting.Drawing.Unit(0.25999990105628967D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection3.Name = "groupFooterSection3";
			this.groupFooterSection3.Style.Visible = false;
			// 
			// groupHeaderSection3
			// 
			this.groupHeaderSection3.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox9});
			this.groupHeaderSection3.Name = "groupHeaderSection3";
			this.groupHeaderSection3.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int) (((byte) (51)))), ((int) (((byte) (62)))), ((int) (((byte) (30)))));
			this.groupHeaderSection3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.groupHeaderSection3.Style.BorderWidth.Bottom = new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Point);
			// 
			// CodingStructureReport
			// 
			this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Aquilent.Cara.Reporting.Resources.Cara.Reporting.StyleSheet.xml"));
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.group1,
            this.group3,
            this.codeCategory});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.groupHeaderSection3,
            this.groupFooterSection3,
            this.groupHeaderSection2,
            this.groupFooterSection2,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			reportParameter1.Name = "phaseId";
			reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
			reportParameter1.Value = "25";
			this.ReportParameters.Add(reportParameter1);
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.Width = new Telerik.Reporting.Drawing.Unit(6.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			((System.ComponentModel.ISupportInitialize) (this.reportHeader1)).EndInit();
			((System.ComponentModel.ISupportInitialize) (this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.PageFooterSection pageFooterSection1;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
		private Telerik.Reporting.SubReport subReport1;
		private Telerik.Reporting.Group group1;
		private Telerik.Reporting.GroupFooterSection groupFooterSection1;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
		private Telerik.Reporting.TextBox letterIdCaptionTextBox;
		private Telerik.Reporting.TextBox codeCategoryCaptionTextBox;
		private Telerik.Reporting.TextBox textBox2;
		private ReportHeader reportHeader1;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.Group codeCategory;
		private Telerik.Reporting.GroupFooterSection groupFooterSection2;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.Group group3;
		private Telerik.Reporting.GroupFooterSection groupFooterSection3;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection3;
	}
}