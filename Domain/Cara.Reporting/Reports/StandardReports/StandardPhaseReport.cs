﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Domain.Report;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.Report.Ninject;

namespace Aquilent.Cara.Reporting
{
	public class StandardPhaseReport : Telerik.Reporting.Report
	{
        protected int CommandTimeout = ReportUtilities.GetCommandTimeout();

		protected virtual object GetStandardReportData(int phaseId)
		{
			throw new NotImplementedException();
		}

		public int PhaseId
		{
			set
			{
				ReportParameters["phaseId"].Value = value;
			}
		}

		public string Option
		{
			set
			{
				ReportParameters["option"].Value = value;
			}
		}

        public void AddParameter(KeyValuePair<string, object> kvp)
        {
            ReportParameters[kvp.Key].Value = kvp.Value;
        }

		protected void report_NeedDataSource(object sender, EventArgs e)
		{
			this.DataSource = GetStandardReportData(Convert.ToInt32(ReportParameters["phaseId"].Value));
		}
	}
}
