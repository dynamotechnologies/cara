namespace Aquilent.Cara.Reporting
{
    partial class ConcernResponseWithAssignedTo
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.CRNumberCaptionTextBox = new Telerik.Reporting.TextBox();
            this.ConcernTextCaptionTextBox = new Telerik.Reporting.TextBox();
            this.ResponseTextCaptionTextBox = new Telerik.Reporting.TextBox();
            this.AssignedToTextBox = new Telerik.Reporting.TextBox();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.reportNameTextBox = new Telerik.Reporting.TextBox();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.reportHeader1 = new Aquilent.Cara.Reporting.ReportHeader();
            this.detail = new Telerik.Reporting.DetailSection();
            this.CRNumberDataTextBox = new Telerik.Reporting.TextBox();
            this.AssignedToDataTextBox = new Telerik.Reporting.TextBox();
            this.ConcernTextDataHtmlTextBox = new Telerik.Reporting.HtmlTextBox();
            this.ResponseTextDataHtmlTextBox = new Telerik.Reporting.HtmlTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportHeader1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.60003918409347534D);
            this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.CRNumberCaptionTextBox,
            this.ConcernTextCaptionTextBox,
            this.ResponseTextCaptionTextBox,
            this.AssignedToTextBox});
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // CRNumberCaptionTextBox
            // 
            this.CRNumberCaptionTextBox.CanGrow = true;
            this.CRNumberCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.CRNumberCaptionTextBox.Name = "CRNumberCaptionTextBox";
            this.CRNumberCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.CRNumberCaptionTextBox.StyleName = "Caption";
            this.CRNumberCaptionTextBox.Value = "C/R Number";
            // 
            // ConcernTextCaptionTextBox
            // 
            this.ConcernTextCaptionTextBox.CanGrow = true;
            this.ConcernTextCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000789642333984D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.ConcernTextCaptionTextBox.Name = "ConcernTextCaptionTextBox";
            this.ConcernTextCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7998818159103394D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.ConcernTextCaptionTextBox.StyleName = "Caption";
            this.ConcernTextCaptionTextBox.Value = "Concern Text";
            // 
            // ResponseTextCaptionTextBox
            // 
            this.ResponseTextCaptionTextBox.CanGrow = true;
            this.ResponseTextCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0000393390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.ResponseTextCaptionTextBox.Name = "ResponseTextCaptionTextBox";
            this.ResponseTextCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6998816728591919D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.ResponseTextCaptionTextBox.StyleName = "Caption";
            this.ResponseTextCaptionTextBox.Value = "Response Text";
            // 
            // AssignedToTextBox
            // 
            this.AssignedToTextBox.CanGrow = true;
            this.AssignedToTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.AssignedToTextBox.Name = "AssignedToTextBox";
            this.AssignedToTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.600000262260437D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.AssignedToTextBox.StyleName = "Caption";
            this.AssignedToTextBox.Value = "CR Assigned To";
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.PrintOnFirstPage = false;
            this.pageHeader.Style.Visible = true;
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2791671752929688D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.reportNameTextBox.StyleName = "PageInfo";
            this.reportNameTextBox.Value = "Concern Response With AssignedTo";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.91863220930099487D);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox1});
            this.pageFooter.Name = "pageFooter";
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.61863201856613159D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999606847763062D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.61863201856613159D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1791667938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.49779847264289856D));
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "PageInfo";
            this.textBox1.Value = "Concern Response With AssignedTo Report";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.9187501072883606D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.subReport1.Name = "subReport1";
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "=Parameters.phaseId.Value"));
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Concern Response With AssignedTo"));
            this.subReport1.ReportSource = this.reportHeader1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.88117116689682007D));
            // 
            // reportHeader1
            // 
            this.reportHeader1.Name = "reportHeader1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40007880330085754D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.CRNumberDataTextBox,
            this.AssignedToDataTextBox,
            this.ConcernTextDataHtmlTextBox,
            this.ResponseTextDataHtmlTextBox});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // CRNumberDataTextBox
            // 
            this.CRNumberDataTextBox.CanGrow = true;
            this.CRNumberDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9259593904716894E-05D));
            this.CRNumberDataTextBox.Name = "CRNumberDataTextBox";
            this.CRNumberDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.CRNumberDataTextBox.StyleName = "Data";
            this.CRNumberDataTextBox.Value = "= Fields.ConcernResponseNumber";
            // 
            // AssignedToDataTextBox
            // 
            this.AssignedToDataTextBox.CanGrow = true;
            this.AssignedToDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7001185417175293D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.AssignedToDataTextBox.Name = "AssignedToDataTextBox";
            this.AssignedToDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5998821258544922D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.AssignedToDataTextBox.StyleName = "Data";
            this.AssignedToDataTextBox.Value = "=Fields.CRAssignedTo";
            // 
            // ConcernTextDataHtmlTextBox
            // 
            this.ConcernTextDataHtmlTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000789642333984D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.ConcernTextDataHtmlTextBox.Name = "ConcernTextDataHtmlTextBox";
            this.ConcernTextDataHtmlTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7998815774917603D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.ConcernTextDataHtmlTextBox.Value = "{Fields.ConcernText}";
            // 
            // ResponseTextDataHtmlTextBox
            // 
            this.ResponseTextDataHtmlTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0000393390655518D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.ResponseTextDataHtmlTextBox.Name = "ResponseTextDataHtmlTextBox";
            this.ResponseTextDataHtmlTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.699881911277771D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.ResponseTextDataHtmlTextBox.Value = "{Fields.ResponseText}";
            // 
            // ConcernResponseWithAssignedTo
            // 
            this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Data\\Cara.Reporting.StyleSheet.xml"));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail});
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "phaseId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "24";
            this.ReportParameters.Add(reportParameter1);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule1.Style.BackgroundColor = System.Drawing.Color.Empty;
            styleRule1.Style.Color = System.Drawing.Color.Black;
            styleRule1.Style.Font.Bold = true;
            styleRule1.Style.Font.Italic = true;
            styleRule1.Style.Font.Name = "Tahoma";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Bold = true;
            styleRule2.Style.Font.Italic = false;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            styleRule2.Style.Font.Strikeout = false;
            styleRule2.Style.Font.Underline = false;
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule4.Style.Color = System.Drawing.Color.Black;
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.3000006675720215D);
            ((System.ComponentModel.ISupportInitialize)(this.reportHeader1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
        private Telerik.Reporting.TextBox CRNumberCaptionTextBox;
        private Telerik.Reporting.TextBox ConcernTextCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox CRNumberDataTextBox;
		private Telerik.Reporting.SubReport subReport1;
		private ReportHeader reportHeader1;
		private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox ResponseTextCaptionTextBox;
        private Telerik.Reporting.TextBox AssignedToTextBox;
        private Telerik.Reporting.TextBox AssignedToDataTextBox;
        private Telerik.Reporting.HtmlTextBox ConcernTextDataHtmlTextBox;
        private Telerik.Reporting.HtmlTextBox ResponseTextDataHtmlTextBox;
	}
}