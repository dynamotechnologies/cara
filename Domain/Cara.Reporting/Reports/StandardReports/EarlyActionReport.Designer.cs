namespace Aquilent.Cara.Reporting
{
	partial class EarlyActionReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.currentTimeTextBox = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
			this.subReport1 = new Telerik.Reporting.SubReport();
			this.reportHeaderLandscape1 = new Aquilent.Cara.Reporting.ReportHeaderLandscape();
			this.group1 = new Telerik.Reporting.Group();
			this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
			this.codeNameCaptionTextBox = new Telerik.Reporting.TextBox();
			this.codeCategoryCaptionTextBox = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.authorCaptionTextBox = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.group2 = new Telerik.Reporting.Group();
			this.groupFooterSection2 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection2 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.group3 = new Telerik.Reporting.Group();
			this.groupFooterSection3 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection3 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.group4 = new Telerik.Reporting.Group();
			this.groupFooterSection4 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection4 = new Telerik.Reporting.GroupHeaderSection();
			this.group5 = new Telerik.Reporting.Group();
			this.groupFooterSection5 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection5 = new Telerik.Reporting.GroupHeaderSection();
			((System.ComponentModel.ISupportInitialize) (this.reportHeaderLandscape1)).BeginInit();
			((System.ComponentModel.ISupportInitialize) (this)).BeginInit();
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			this.pageHeaderSection1.PrintOnFirstPage = false;
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.4166665077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Early Attention Report";
			// 
			// detail
			// 
			formattingRule1.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.CommentId", Telerik.Reporting.Data.FilterOperator.Equal, null)});
			formattingRule1.Style.Visible = false;
			this.detail.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
			this.detail.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox9,
            this.textBox10});
			this.detail.Name = "detail";
			this.detail.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// textBox9
			// 
			this.textBox9.CanGrow = true;
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.4200783967971802D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1798423528671265D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox9.StyleName = "Data";
			this.textBox9.Value = "#{Fields.LetterSequence}-{Fields.CommentNumber}";
			// 
			// textBox10
			// 
			this.textBox10.CanGrow = true;
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.5999996662139893D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(4.7799210548400879D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox10.StyleName = "Data";
			this.textBox10.Value = "= Aquilent.Cara.Reporting.ReportUtilities.StripInvalidCharacters(Fields.CommentTe" +
				"xt)";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.currentTimeTextBox,
            this.pageInfoTextBox});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox1.StyleName = "PageInfo";
			this.textBox1.Value = "Early Attention Report";
			// 
			// currentTimeTextBox
			// 
			this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Name = "currentTimeTextBox";
			this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.currentTimeTextBox.StyleName = "PageInfo";
			this.currentTimeTextBox.Value = "=NOW()";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.2500195503234863D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
			// 
			// reportHeaderSection1
			// 
			this.reportHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.81000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
			this.reportHeaderSection1.Name = "reportHeaderSection1";
			// 
			// subReport1
			// 
			this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.subReport1.Name = "subReport1";
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "25"));
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Early Attention Report"));
			this.subReport1.ReportSource = this.reportHeaderLandscape1;
			this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(9.9999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.81000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch));
			// 
			// group1
			// 
			this.group1.GroupFooter = this.groupFooterSection1;
			this.group1.GroupHeader = this.groupHeaderSection1;
			this.group1.Name = "group1";
			// 
			// groupFooterSection1
			// 
			this.groupFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection1.Name = "groupFooterSection1";
			this.groupFooterSection1.Style.Visible = false;
			// 
			// groupHeaderSection1
			// 
			this.groupHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.81000012159347534D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.codeNameCaptionTextBox,
            this.codeCategoryCaptionTextBox,
            this.textBox6,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox4});
			this.groupHeaderSection1.Name = "groupHeaderSection1";
			this.groupHeaderSection1.PrintOnEveryPage = true;
			// 
			// codeNameCaptionTextBox
			// 
			this.codeNameCaptionTextBox.CanGrow = true;
			this.codeNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.900118350982666D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeNameCaptionTextBox.Name = "codeNameCaptionTextBox";
			this.codeNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeNameCaptionTextBox.StyleName = "Caption";
			this.codeNameCaptionTextBox.Value = "Date Submitted";
			// 
			// codeCategoryCaptionTextBox
			// 
			this.codeCategoryCaptionTextBox.CanGrow = true;
			this.codeCategoryCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeCategoryCaptionTextBox.Name = "codeCategoryCaptionTextBox";
			this.codeCategoryCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.87999999523162842D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeCategoryCaptionTextBox.StyleName = "Caption";
			this.codeCategoryCaptionTextBox.Value = "Category";
			// 
			// textBox6
			// 
			this.textBox6.CanGrow = true;
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.900118350982666D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.StyleName = "Caption";
			this.textBox6.Value = "Letter #";
			// 
			// textBox17
			// 
			this.textBox17.CanGrow = true;
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.9001972675323486D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8598031997680664D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox17.StyleName = "Caption";
			this.textBox17.Value = "Name";
			// 
			// textBox18
			// 
			this.textBox18.CanGrow = true;
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.7600798606872559D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4399194717407227D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox18.StyleName = "Caption";
			this.textBox18.Value = "Address";
			// 
			// textBox19
			// 
			this.textBox19.CanGrow = true;
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.20007848739624D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3998817205429077D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox19.StyleName = "Caption";
			this.textBox19.Value = "City";
			// 
			// textBox20
			// 
			this.textBox20.CanGrow = true;
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.6001176834106445D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.49984201788902283D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox20.StyleName = "Caption";
			this.textBox20.Value = "State";
			// 
			// textBox21
			// 
			this.textBox21.CanGrow = true;
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.1000394821167D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89992111921310425D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox21.StyleName = "Caption";
			this.textBox21.Value = "ZIP";
			// 
			// textBox4
			// 
			this.textBox4.CanGrow = true;
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999822601675987D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.51007890701293945D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(9.9799594879150391D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.29992124438285828D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int) (((byte) (251)))), ((int) (((byte) (248)))), ((int) (((byte) (221)))));
			this.textBox4.Style.Font.Bold = true;
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox4.StyleName = "Data";
			this.textBox4.Value = "=\"Total Early Attention Letters: \" + Count(Fields.LetterSequence)";
			// 
			// authorCaptionTextBox
			// 
			this.authorCaptionTextBox.CanGrow = true;
			this.authorCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.6000001430511475D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.authorCaptionTextBox.Name = "authorCaptionTextBox";
			this.authorCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(4.7799210548400879D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.authorCaptionTextBox.StyleName = "Caption";
			this.authorCaptionTextBox.Value = "Comment";
			// 
			// textBox7
			// 
			this.textBox7.CanGrow = true;
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.4200783967971802D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1798429489135742D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.StyleName = "Caption";
			this.textBox7.Value = "Comment #";
			// 
			// group2
			// 
			this.group2.GroupFooter = this.groupFooterSection2;
			this.group2.GroupHeader = this.groupHeaderSection2;
			this.group2.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.EaType")});
			this.group2.Name = "group2";
			// 
			// groupFooterSection2
			// 
			this.groupFooterSection2.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection2.Name = "groupFooterSection2";
			this.groupFooterSection2.Style.Visible = false;
			// 
			// groupHeaderSection2
			// 
			this.groupHeaderSection2.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15});
			this.groupHeaderSection2.Name = "groupHeaderSection2";
			this.groupHeaderSection2.PrintOnEveryPage = true;
			// 
			// textBox15
			// 
			this.textBox15.CanGrow = true;
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.3800003528594971D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox15.Style.Font.Bold = false;
			this.textBox15.Style.Font.Italic = true;
			this.textBox15.StyleName = "Data";
			this.textBox15.Value = "=Fields.EaType";
			// 
			// textBox2
			// 
			this.textBox2.CanGrow = true;
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.3800003528594971D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.StyleName = "Data";
			this.textBox2.Value = "=Aquilent.Cara.Reporting.ReportUtilities.TrimCodeInString(Fields.TermListName)";
			// 
			// group3
			// 
			this.group3.GroupFooter = this.groupFooterSection3;
			this.group3.GroupHeader = this.groupHeaderSection3;
			this.group3.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.TermListName")});
			this.group3.Name = "group3";
			// 
			// groupFooterSection3
			// 
			this.groupFooterSection3.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection3.Name = "groupFooterSection3";
			this.groupFooterSection3.Style.Visible = false;
			// 
			// groupHeaderSection3
			// 
			this.groupHeaderSection3.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox16});
			this.groupHeaderSection3.Name = "groupHeaderSection3";
			this.groupHeaderSection3.PrintOnEveryPage = true;
			// 
			// textBox16
			// 
			this.textBox16.CanGrow = true;
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(6.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.49996018409729D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox16.StyleName = "Data";
			this.textBox16.Value = "=\"Total \'\" + Aquilent.Cara.Reporting.ReportUtilities.TrimCodeInString(Fields.Term" +
				"ListName) + \"\' Letters: \" + Exec(\"group3\", Count(Fields.LetterSequence))";
			// 
			// textBox3
			// 
			this.textBox3.CanGrow = true;
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.900118350982666D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0800000429153442D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.400079607963562D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.StyleName = "Data";
			this.textBox3.Value = "=Fields.DateSubmitted";
			// 
			// textBox8
			// 
			this.textBox8.CanGrow = true;
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.9801973104476929D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox8.StyleName = "Data";
			this.textBox8.Value = "=Fields.LetterSequence";
			// 
			// textBox5
			// 
			this.textBox5.CanGrow = true;
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.8601973056793213D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9736431062920019E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8998031616210938D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.StyleName = "Data";
			this.textBox5.Value = "=IsNull(Fields.LastName, \"\") + \", \" + IsNull(Fields.FirstName, \"\")";
			// 
			// textBox11
			// 
			this.textBox11.CanGrow = true;
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.7600789070129395D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.4399206638336182D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox11.StyleName = "Data";
			this.textBox11.Value = "{Fields.Address1}\r\n{Fields.Address2}";
			// 
			// textBox12
			// 
			this.textBox12.CanGrow = true;
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.20007848739624D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(4.00543212890625E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3998817205429077D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox12.StyleName = "Data";
			this.textBox12.Value = "=Fields.City";
			// 
			// textBox13
			// 
			this.textBox13.CanGrow = true;
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.6000394821167D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.49992084503173828D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox13.StyleName = "Data";
			this.textBox13.Value = "=Fields.StateId";
			// 
			// textBox14
			// 
			this.textBox14.CanGrow = true;
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.1000394821167D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89992111921310425D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox14.StyleName = "Data";
			this.textBox14.Value = "=Fields.ZipPostal";
			// 
			// group4
			// 
			this.group4.GroupFooter = this.groupFooterSection4;
			this.group4.GroupHeader = this.groupHeaderSection4;
			this.group4.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.LetterSequence")});
			this.group4.Name = "group4";
			// 
			// groupFooterSection4
			// 
			this.groupFooterSection4.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection4.Name = "groupFooterSection4";
			this.groupFooterSection4.Style.Visible = false;
			// 
			// groupHeaderSection4
			// 
			this.groupHeaderSection4.Height = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.textBox8,
            this.textBox5,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14});
			this.groupHeaderSection4.Name = "groupHeaderSection4";
			this.groupHeaderSection4.PrintOnEveryPage = true;
			// 
			// group5
			// 
			this.group5.GroupFooter = this.groupFooterSection5;
			this.group5.GroupHeader = this.groupHeaderSection5;
			this.group5.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.LetterSequence")});
			this.group5.Name = "group5";
			// 
			// groupFooterSection5
			// 
			this.groupFooterSection5.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection5.Name = "groupFooterSection5";
			this.groupFooterSection5.Style.Visible = false;
			// 
			// groupHeaderSection5
			// 
			formattingRule2.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("Fields.CommentId", Telerik.Reporting.Data.FilterOperator.Equal, null)});
			formattingRule2.Style.Visible = false;
			this.groupHeaderSection5.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
			this.groupHeaderSection5.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.authorCaptionTextBox});
			this.groupHeaderSection5.Name = "groupHeaderSection5";
			this.groupHeaderSection5.PrintOnEveryPage = true;
			this.groupHeaderSection5.Style.Padding.Top = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// EarlyActionReport
			// 
			this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Aquilent.Cara.Reporting.Resources.Cara.Reporting.StyleSheet.xml"));
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.group1,
            this.group2,
            this.group3,
            this.group4,
            this.group5});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.groupHeaderSection2,
            this.groupFooterSection2,
            this.groupHeaderSection3,
            this.groupFooterSection3,
            this.groupHeaderSection4,
            this.groupFooterSection4,
            this.groupHeaderSection5,
            this.groupFooterSection5,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
			this.PageSettings.Landscape = true;
			this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			reportParameter1.Name = "phaseId";
			reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
			reportParameter1.Value = "25";
			this.ReportParameters.Add(reportParameter1);
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch);
			((System.ComponentModel.ISupportInitialize) (this.reportHeaderLandscape1)).EndInit();
			((System.ComponentModel.ISupportInitialize) (this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.PageFooterSection pageFooterSection1;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
		private Telerik.Reporting.SubReport subReport1;
		private Telerik.Reporting.Group group1;
		private Telerik.Reporting.GroupFooterSection groupFooterSection1;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
		private Telerik.Reporting.TextBox authorCaptionTextBox;
		private Telerik.Reporting.TextBox codeNameCaptionTextBox;
		private Telerik.Reporting.TextBox codeCategoryCaptionTextBox;
		private Telerik.Reporting.Group group2;
		private Telerik.Reporting.GroupFooterSection groupFooterSection2;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection2;
		private ReportHeaderLandscape reportHeaderLandscape1;
		private Telerik.Reporting.Group group3;
		private Telerik.Reporting.GroupFooterSection groupFooterSection3;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection3;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox11;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox14;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.Group group4;
		private Telerik.Reporting.GroupFooterSection groupFooterSection4;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection4;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox16;
		private Telerik.Reporting.Group group5;
		private Telerik.Reporting.GroupFooterSection groupFooterSection5;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection5;
		private Telerik.Reporting.TextBox textBox17;
		private Telerik.Reporting.TextBox textBox18;
		private Telerik.Reporting.TextBox textBox19;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox21;
	}
}