namespace Aquilent.Cara.Reporting
{
	partial class UntimelyCommentsReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
			this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
			this.teamMemberFirstNameCaptionTextBox = new Telerik.Reporting.TextBox();
			this.roleNameCaptionTextBox = new Telerik.Reporting.TextBox();
			this.teamMemberPhoneCaptionTextBox = new Telerik.Reporting.TextBox();
			this.teamMemberEmailCaptionTextBox = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
			this.labelsGroup = new Telerik.Reporting.Group();
			this.pageHeader = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.pageFooter = new Telerik.Reporting.PageFooterSection();
			this.currentTimeTextBox = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
			this.subReport1 = new Telerik.Reporting.SubReport();
			this.reportHeaderLandscape1 = new Aquilent.Cara.Reporting.ReportHeaderLandscape();
			this.detail = new Telerik.Reporting.DetailSection();
			this.teamMemberFirstNameDataTextBox = new Telerik.Reporting.TextBox();
			this.roleNameDataTextBox = new Telerik.Reporting.TextBox();
			this.teamMemberPhoneDataTextBox = new Telerik.Reporting.TextBox();
			this.teamMemberEmailDataTextBox = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize) (this.reportHeaderLandscape1)).BeginInit();
			((System.ComponentModel.ISupportInitialize) (this)).BeginInit();
			// 
			// labelsGroupHeader
			// 
			this.labelsGroupHeader.Height = new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.teamMemberFirstNameCaptionTextBox,
            this.roleNameCaptionTextBox,
            this.teamMemberPhoneCaptionTextBox,
            this.teamMemberEmailCaptionTextBox,
            this.textBox2,
            this.textBox4,
            this.textBox3});
			this.labelsGroupHeader.Name = "labelsGroupHeader";
			this.labelsGroupHeader.PrintOnEveryPage = true;
			// 
			// teamMemberFirstNameCaptionTextBox
			// 
			this.teamMemberFirstNameCaptionTextBox.CanGrow = true;
			this.teamMemberFirstNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberFirstNameCaptionTextBox.Name = "teamMemberFirstNameCaptionTextBox";
			this.teamMemberFirstNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3791667222976685D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberFirstNameCaptionTextBox.StyleName = "Caption";
			this.teamMemberFirstNameCaptionTextBox.Value = "Name";
			// 
			// roleNameCaptionTextBox
			// 
			this.roleNameCaptionTextBox.CanGrow = true;
			this.roleNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.4000788927078247D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameCaptionTextBox.Name = "roleNameCaptionTextBox";
			this.roleNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999209642410278D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameCaptionTextBox.StyleName = "Caption";
			this.roleNameCaptionTextBox.Value = "Organization";
			// 
			// teamMemberPhoneCaptionTextBox
			// 
			this.teamMemberPhoneCaptionTextBox.CanGrow = true;
			this.teamMemberPhoneCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.9000787734985352D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneCaptionTextBox.Name = "teamMemberPhoneCaptionTextBox";
			this.teamMemberPhoneCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.2998425960540771D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneCaptionTextBox.StyleName = "Caption";
			this.teamMemberPhoneCaptionTextBox.Value = "Address";
			// 
			// teamMemberEmailCaptionTextBox
			// 
			this.teamMemberEmailCaptionTextBox.CanGrow = true;
			this.teamMemberEmailCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(6.2000007629394531D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberEmailCaptionTextBox.Name = "teamMemberEmailCaptionTextBox";
			this.teamMemberEmailCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5999999046325684D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberEmailCaptionTextBox.StyleName = "Caption";
			this.teamMemberEmailCaptionTextBox.Value = "Email";
			// 
			// textBox2
			// 
			this.textBox2.CanGrow = true;
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.2000002861022949D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.999921977519989D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.StyleName = "Caption";
			this.textBox2.Value = "Phone";
			// 
			// textBox4
			// 
			this.textBox4.CanGrow = true;
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.800079345703125D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.799842357635498D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.StyleName = "Caption";
			this.textBox4.Value = "Letter #";
			// 
			// textBox3
			// 
			this.textBox3.CanGrow = true;
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.6000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3999603986740112D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.StyleName = "Caption";
			this.textBox3.Value = "Submitted";
			// 
			// labelsGroupFooter
			// 
			this.labelsGroupFooter.Height = new Telerik.Reporting.Drawing.Unit(0.28125D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.labelsGroupFooter.Name = "labelsGroupFooter";
			this.labelsGroupFooter.Style.Visible = false;
			// 
			// labelsGroup
			// 
			this.labelsGroup.GroupFooter = this.labelsGroupFooter;
			this.labelsGroup.GroupHeader = this.labelsGroupHeader;
			this.labelsGroup.Name = "labelsGroup";
			// 
			// pageHeader
			// 
			this.pageHeader.Height = new Telerik.Reporting.Drawing.Unit(0.28125D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeader.Name = "pageHeader";
			this.pageHeader.PrintOnFirstPage = false;
			this.pageHeader.Style.Visible = true;
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.4791665077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Untimely Comments Report";
			// 
			// pageFooter
			// 
			this.pageFooter.Height = new Telerik.Reporting.Drawing.Unit(0.28125D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox1});
			this.pageFooter.Name = "pageFooter";
			// 
			// currentTimeTextBox
			// 
			this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Name = "currentTimeTextBox";
			this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.currentTimeTextBox.StyleName = "PageInfo";
			this.currentTimeTextBox.Value = "=NOW()";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.2604165077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox1.StyleName = "PageInfo";
			this.textBox1.Value = "Untimely Comments Report";
			// 
			// reportHeader
			// 
			this.reportHeader.Height = new Telerik.Reporting.Drawing.Unit(0.9187501072883606D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
			this.reportHeader.Name = "reportHeader";
			this.reportHeader.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int) (((byte) (242)))), ((int) (((byte) (235)))), ((int) (((byte) (163)))));
			// 
			// subReport1
			// 
			this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.subReport1.Name = "subReport1";
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "=Parameters.phaseId.Value"));
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Untimely Comments Report"));
			this.subReport1.ReportSource = this.reportHeaderLandscape1;
			this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.87999999523162842D, Telerik.Reporting.Drawing.UnitType.Inch));
			// 
			// detail
			// 
			this.detail.Height = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.teamMemberFirstNameDataTextBox,
            this.roleNameDataTextBox,
            this.teamMemberPhoneDataTextBox,
            this.teamMemberEmailDataTextBox,
            this.textBox5,
            this.textBox7,
            this.textBox6,
            this.textBox9});
			this.detail.Name = "detail";
			this.detail.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0.05000000074505806D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// teamMemberFirstNameDataTextBox
			// 
			this.teamMemberFirstNameDataTextBox.CanGrow = true;
			formattingRule1.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {
            new Telerik.Reporting.Data.Filter("=Fields.LastName + \", \" + Fields.FirstName", Telerik.Reporting.Data.FilterOperator.Equal, ",")});
			formattingRule1.Style.Visible = false;
			this.teamMemberFirstNameDataTextBox.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
			this.teamMemberFirstNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9259593904716894E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberFirstNameDataTextBox.Name = "teamMemberFirstNameDataTextBox";
			this.teamMemberFirstNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3791667222976685D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40003955364227295D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberFirstNameDataTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.teamMemberFirstNameDataTextBox.StyleName = "Data";
			this.teamMemberFirstNameDataTextBox.Value = "=IsNull(Fields.LastName, \"\") + \', \' + IsNull(Fields.FirstName, \"\")";
			// 
			// roleNameDataTextBox
			// 
			this.roleNameDataTextBox.CanGrow = true;
			this.roleNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.4000791311264038D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameDataTextBox.Name = "roleNameDataTextBox";
			this.roleNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999207258224487D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40007877349853516D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameDataTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.roleNameDataTextBox.StyleName = "Data";
			this.roleNameDataTextBox.Value = "=Fields.OrgName";
			// 
			// teamMemberPhoneDataTextBox
			// 
			this.teamMemberPhoneDataTextBox.CanGrow = true;
			this.teamMemberPhoneDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.9000787734985352D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneDataTextBox.Name = "teamMemberPhoneDataTextBox";
			this.teamMemberPhoneDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.2998425960540771D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneDataTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.teamMemberPhoneDataTextBox.StyleName = "Data";
			this.teamMemberPhoneDataTextBox.Value = "{Fields.Address1}\r\n{Fields.Address2}";
			// 
			// teamMemberEmailDataTextBox
			// 
			this.teamMemberEmailDataTextBox.CanGrow = true;
			this.teamMemberEmailDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(6.2000007629394531D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberEmailDataTextBox.Name = "teamMemberEmailDataTextBox";
			this.teamMemberEmailDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5999999046325684D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40007880330085754D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberEmailDataTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.teamMemberEmailDataTextBox.StyleName = "Data";
			this.teamMemberEmailDataTextBox.Value = "=Fields.Email";
			// 
			// textBox5
			// 
			this.textBox5.CanGrow = true;
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.2000002861022949D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.99992161989212036D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40007877349853516D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox5.StyleName = "Data";
			this.textBox5.Value = "=Fields.Phone";
			// 
			// textBox7
			// 
			this.textBox7.CanGrow = true;
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.800079345703125D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40007874369621277D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.Style.Padding.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox7.StyleName = "Data";
			this.textBox7.Value = "=Fields.LetterSequence";
			// 
			// textBox6
			// 
			this.textBox6.CanGrow = true;
			this.textBox6.Format = "{0:g}";
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.6000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3999603986740112D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40007877349853516D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox6.StyleName = "Data";
			this.textBox6.Value = "=Fields.DateSubmitted";
			// 
			// textBox9
			// 
			this.textBox9.CanGrow = true;
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.9000787734985352D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20007880032062531D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.2998425960540771D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox9.StyleName = "Data";
			this.textBox9.Value = "=Fields.City + \", \" + Fields.StateId + \"  \" + Fields.ZipPostal";
			// 
			// UntimelyCommentsReport
			// 
			this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Data\\Cara.Reporting.StyleSheet.xml"));
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail});
			this.PageSettings.Landscape = true;
			this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			reportParameter1.Name = "phaseId";
			reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
			reportParameter1.Value = "24";
			this.ReportParameters.Add(reportParameter1);
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Style.Padding.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
			styleRule1.Style.BackgroundColor = System.Drawing.Color.Empty;
			styleRule1.Style.Color = System.Drawing.Color.Black;
			styleRule1.Style.Font.Bold = true;
			styleRule1.Style.Font.Italic = true;
			styleRule1.Style.Font.Name = "Tahoma";
			styleRule1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(18D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
			styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int) (((byte) (242)))), ((int) (((byte) (235)))), ((int) (((byte) (163)))));
			styleRule2.Style.Color = System.Drawing.Color.Black;
			styleRule2.Style.Font.Bold = true;
			styleRule2.Style.Font.Italic = false;
			styleRule2.Style.Font.Name = "Tahoma";
			styleRule2.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule2.Style.Font.Strikeout = false;
			styleRule2.Style.Font.Underline = false;
			styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
			styleRule3.Style.Color = System.Drawing.Color.Black;
			styleRule3.Style.Font.Name = "Tahoma";
			styleRule3.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
			styleRule4.Style.Color = System.Drawing.Color.Black;
			styleRule4.Style.Font.Name = "Tahoma";
			styleRule4.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
			this.Width = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch);
			((System.ComponentModel.ISupportInitialize) (this.reportHeaderLandscape1)).EndInit();
			((System.ComponentModel.ISupportInitialize) (this)).EndInit();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
		private Telerik.Reporting.TextBox teamMemberFirstNameCaptionTextBox;
		private Telerik.Reporting.TextBox roleNameCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberEmailCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.TextBox teamMemberFirstNameDataTextBox;
		private Telerik.Reporting.TextBox roleNameDataTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneDataTextBox;
		private Telerik.Reporting.TextBox teamMemberEmailDataTextBox;
		private Telerik.Reporting.SubReport subReport1;
		private Telerik.Reporting.TextBox textBox1;
		private ReportHeaderLandscape reportHeaderLandscape1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox9;
	}
}