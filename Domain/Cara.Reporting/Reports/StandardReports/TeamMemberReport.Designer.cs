namespace Aquilent.Cara.Reporting
{
	partial class TeamMemberReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.teamMemberFirstNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberPhoneCaptionTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberEmailCaptionTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberStatusCaptionTextBox = new Telerik.Reporting.TextBox();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.reportNameTextBox = new Telerik.Reporting.TextBox();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.reportHeader1 = new Aquilent.Cara.Reporting.ReportHeader();
            this.detail = new Telerik.Reporting.DetailSection();
            this.teamMemberFirstNameDataTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberPhoneDataTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberEmailDataTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberStatusDataTextBox = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportHeader1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D);
            this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.teamMemberFirstNameCaptionTextBox,
            this.teamMemberPhoneCaptionTextBox,
            this.teamMemberEmailCaptionTextBox,
            this.teamMemberStatusCaptionTextBox});
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // teamMemberFirstNameCaptionTextBox
            // 
            this.teamMemberFirstNameCaptionTextBox.CanGrow = true;
            this.teamMemberFirstNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberFirstNameCaptionTextBox.Name = "teamMemberFirstNameCaptionTextBox";
            this.teamMemberFirstNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2791669368743896D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberFirstNameCaptionTextBox.StyleName = "Caption";
            this.teamMemberFirstNameCaptionTextBox.Value = "Name";
            // 
            // teamMemberPhoneCaptionTextBox
            // 
            this.teamMemberPhoneCaptionTextBox.CanGrow = true;
            this.teamMemberPhoneCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.300079345703125D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberPhoneCaptionTextBox.Name = "teamMemberPhoneCaptionTextBox";
            this.teamMemberPhoneCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberPhoneCaptionTextBox.StyleName = "Caption";
            this.teamMemberPhoneCaptionTextBox.Value = "Phone";
            // 
            // teamMemberEmailCaptionTextBox
            // 
            this.teamMemberEmailCaptionTextBox.CanGrow = true;
            this.teamMemberEmailCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3001582622528076D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberEmailCaptionTextBox.Name = "teamMemberEmailCaptionTextBox";
            this.teamMemberEmailCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1998419761657715D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberEmailCaptionTextBox.StyleName = "Caption";
            this.teamMemberEmailCaptionTextBox.Value = "Email";
            // 
            // teamMemberStatusCaptionTextBox
            // 
            this.teamMemberStatusCaptionTextBox.CanGrow = true;
            this.teamMemberStatusCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberStatusCaptionTextBox.Name = "teamMemberStatusCaptionTextBox";
            this.teamMemberStatusCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91956406831741333D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberStatusCaptionTextBox.StyleName = "Caption";
            this.teamMemberStatusCaptionTextBox.Value = "Status";
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.PrintOnFirstPage = false;
            this.pageHeader.Style.Visible = true;
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4791665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.reportNameTextBox.StyleName = "PageInfo";
            this.reportNameTextBox.Value = "Team Member Report";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox1});
            this.pageFooter.Name = "pageFooter";
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "PageInfo";
            this.textBox1.Value = "Team Member Report";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.9187501072883606D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.subReport1.Name = "subReport1";
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "=Parameters.phaseId.Value"));
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Team Member Report"));
            this.subReport1.ReportSource = this.reportHeader1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.88117116689682007D));
            // 
            // reportHeader1
            // 
            this.reportHeader1.Name = "reportHeader1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.teamMemberFirstNameDataTextBox,
            this.teamMemberPhoneDataTextBox,
            this.teamMemberEmailDataTextBox,
            this.teamMemberStatusDataTextBox});
            this.detail.Name = "detail";
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // teamMemberFirstNameDataTextBox
            // 
            this.teamMemberFirstNameDataTextBox.CanGrow = true;
            this.teamMemberFirstNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(3.9259593904716894E-05D));
            this.teamMemberFirstNameDataTextBox.Name = "teamMemberFirstNameDataTextBox";
            this.teamMemberFirstNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2791674137115479D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberFirstNameDataTextBox.StyleName = "Data";
            this.teamMemberFirstNameDataTextBox.Value = "= Fields.TeamMemberLastName + \", \" + Fields.TeamMemberFirstName";
            // 
            // teamMemberPhoneDataTextBox
            // 
            this.teamMemberPhoneDataTextBox.CanGrow = true;
            this.teamMemberPhoneDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.300079345703125D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.teamMemberPhoneDataTextBox.Name = "teamMemberPhoneDataTextBox";
            this.teamMemberPhoneDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99999982118606567D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberPhoneDataTextBox.StyleName = "Data";
            this.teamMemberPhoneDataTextBox.Value = "=Fields.TeamMemberPhone";
            // 
            // teamMemberEmailDataTextBox
            // 
            this.teamMemberEmailDataTextBox.CanGrow = true;
            this.teamMemberEmailDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3001582622528076D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.teamMemberEmailDataTextBox.Name = "teamMemberEmailDataTextBox";
            this.teamMemberEmailDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1998419761657715D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberEmailDataTextBox.StyleName = "Data";
            this.teamMemberEmailDataTextBox.Value = "=Fields.TeamMemberEmail";
            // 
            // teamMemberStatusDataTextBox
            // 
            this.teamMemberStatusDataTextBox.CanGrow = true;
            this.teamMemberStatusDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.teamMemberStatusDataTextBox.Name = "teamMemberStatusDataTextBox";
            this.teamMemberStatusDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91960352659225464D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberStatusDataTextBox.StyleName = "Data";
            this.teamMemberStatusDataTextBox.Value = "=IIF(Fields.TeamMemberStatus, \"Active\", \"Inactive\")";
            // 
            // TeamMemberReport
            // 
            this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Data\\Cara.Reporting.StyleSheet.xml"));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail});
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "phaseId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "24";
            this.ReportParameters.Add(reportParameter1);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule1.Style.BackgroundColor = System.Drawing.Color.Empty;
            styleRule1.Style.Color = System.Drawing.Color.Black;
            styleRule1.Style.Font.Bold = true;
            styleRule1.Style.Font.Italic = true;
            styleRule1.Style.Font.Name = "Tahoma";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Bold = true;
            styleRule2.Style.Font.Italic = false;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            styleRule2.Style.Font.Strikeout = false;
            styleRule2.Style.Font.Underline = false;
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule4.Style.Color = System.Drawing.Color.Black;
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.5D);
            ((System.ComponentModel.ISupportInitialize)(this.reportHeader1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
        private Telerik.Reporting.TextBox teamMemberFirstNameCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberEmailCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberStatusCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox teamMemberFirstNameDataTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneDataTextBox;
		private Telerik.Reporting.TextBox teamMemberEmailDataTextBox;
		private Telerik.Reporting.TextBox teamMemberStatusDataTextBox;
		private Telerik.Reporting.SubReport subReport1;
		private ReportHeader reportHeader1;
		private Telerik.Reporting.TextBox textBox1;
	}
}