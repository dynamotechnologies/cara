using Aquilent.EntityAccess;
using System;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using System.Data.Objects;
using System.Configuration;

namespace Aquilent.Cara.Reporting
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class TeamMemberReport : StandardPhaseReport
	{
		public TeamMemberReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));

			IList<TeamMemberReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;
                //reportData = context.GetTeamMemberReport(phaseId,1,0,output);var output = new ObjectParameter("totalRows", typeof(int));
                //IList<TeamMemberReportData> reportData;
                IList<TeamMemberReportData> batchData;

                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetTeamMemberReport(phaseId, pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetTeamMemberReport(phaseId, pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<TeamMemberReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
			}

			return reportData;
		}
	}
}