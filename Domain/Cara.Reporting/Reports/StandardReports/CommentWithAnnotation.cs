using Aquilent.EntityAccess;
using System;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using System.Data.Objects;
using System.Configuration;

namespace Aquilent.Cara.Reporting
{
	/// <summary>
    /// Summary description for CommentWithCRInfoReport.
	/// </summary>
	public partial class CommentWithAnnotationReport : StandardPhaseReport
	{
        public CommentWithAnnotationReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));

			IList<CommentWithAnnotationReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;
                //reportData = context.GetTeamMemberReport(phaseId,1,0,output);var output = new ObjectParameter("totalRows", typeof(int));
                //IList<TeamMemberReportData> reportData;
                IList<CommentWithAnnotationReportData> batchData;

                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetCommentWithAnnotationReport(phaseId, pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetCommentWithAnnotationReport(phaseId, pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<CommentWithAnnotationReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
            }
            foreach (CommentWithAnnotationReportData row in reportData)
            {
                row.CommentText = ReportManager.CleanCommentText(row.CommentText);
            }

			return reportData;
		}
	}
}