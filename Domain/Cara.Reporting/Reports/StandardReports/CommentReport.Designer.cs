namespace Aquilent.Cara.Reporting
{
	partial class CommentReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.codeCategoryCaptionTextBox = new Telerik.Reporting.TextBox();
            this.codeNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.letterIdCaptionTextBox = new Telerik.Reporting.TextBox();
            this.commentTextCaptionTextBox = new Telerik.Reporting.TextBox();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.codeCategoryGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.codeCategoryDataTextBox = new Telerik.Reporting.TextBox();
            this.codeCategoryGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.codeCategoryGroup = new Telerik.Reporting.Group();
            this.codeNameGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.codeNameDataTextBox = new Telerik.Reporting.TextBox();
            this.codeNameGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.codeNameGroup = new Telerik.Reporting.Group();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.reportNameTextBox = new Telerik.Reporting.TextBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.reportHeaderLandscape1 = new Aquilent.Cara.Reporting.ReportHeaderLandscape();
            this.detail = new Telerik.Reporting.DetailSection();
            this.letterIdDataTextBox = new Telerik.Reporting.TextBox();
            this.commentTextDataTextBox = new Telerik.Reporting.TextBox();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportHeaderLandscape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D);
            this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.codeCategoryCaptionTextBox,
            this.codeNameCaptionTextBox,
            this.letterIdCaptionTextBox,
            this.commentTextCaptionTextBox});
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // codeCategoryCaptionTextBox
            // 
            this.codeCategoryCaptionTextBox.CanGrow = true;
            this.codeCategoryCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.11051527410745621D));
            this.codeCategoryCaptionTextBox.Name = "codeCategoryCaptionTextBox";
            this.codeCategoryCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2666666507720947D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.codeCategoryCaptionTextBox.StyleName = "Caption";
            this.codeCategoryCaptionTextBox.Value = "Code Category";
            // 
            // codeNameCaptionTextBox
            // 
            this.codeNameCaptionTextBox.CanGrow = true;
            this.codeNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.287578821182251D), Telerik.Reporting.Drawing.Unit.Inch(0.11051527410745621D));
            this.codeNameCaptionTextBox.Name = "codeNameCaptionTextBox";
            this.codeNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2874211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.codeNameCaptionTextBox.StyleName = "Caption";
            this.codeNameCaptionTextBox.Value = "Code";
            // 
            // letterIdCaptionTextBox
            // 
            this.letterIdCaptionTextBox.CanGrow = true;
            this.letterIdCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5750787258148193D), Telerik.Reporting.Drawing.Unit.Inch(0.11051527410745621D));
            this.letterIdCaptionTextBox.Name = "letterIdCaptionTextBox";
            this.letterIdCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1248422861099243D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.letterIdCaptionTextBox.StyleName = "Caption";
            this.letterIdCaptionTextBox.Value = "Comment #";
            // 
            // commentTextCaptionTextBox
            // 
            this.commentTextCaptionTextBox.CanGrow = true;
            this.commentTextCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.11051527410745621D));
            this.commentTextCaptionTextBox.Name = "commentTextCaptionTextBox";
            this.commentTextCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.299959659576416D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.commentTextCaptionTextBox.StyleName = "Caption";
            this.commentTextCaptionTextBox.Value = "Comment Text";
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // codeCategoryGroupHeader
            // 
            this.codeCategoryGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.codeCategoryGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.codeCategoryDataTextBox});
            this.codeCategoryGroupHeader.Name = "codeCategoryGroupHeader";
            this.codeCategoryGroupHeader.PrintOnEveryPage = true;
            // 
            // codeCategoryDataTextBox
            // 
            this.codeCategoryDataTextBox.CanGrow = true;
            this.codeCategoryDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.codeCategoryDataTextBox.Name = "codeCategoryDataTextBox";
            this.codeCategoryDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.879166841506958D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.codeCategoryDataTextBox.StyleName = "Data";
            this.codeCategoryDataTextBox.Value = "=Fields.CodeCategory";
            // 
            // codeCategoryGroupFooter
            // 
            this.codeCategoryGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.codeCategoryGroupFooter.Name = "codeCategoryGroupFooter";
            // 
            // codeCategoryGroup
            // 
            this.codeCategoryGroup.GroupFooter = this.codeCategoryGroupFooter;
            this.codeCategoryGroup.GroupHeader = this.codeCategoryGroupHeader;
            this.codeCategoryGroup.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.CodeCategory")});
            this.codeCategoryGroup.Name = "codeCategoryGroup";
            // 
            // codeNameGroupHeader
            // 
            this.codeNameGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.codeNameGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.codeNameDataTextBox});
            this.codeNameGroupHeader.Name = "codeNameGroupHeader";
            this.codeNameGroupHeader.PrintOnEveryPage = true;
            // 
            // codeNameDataTextBox
            // 
            this.codeNameDataTextBox.CanGrow = true;
            this.codeNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.287578821182251D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.codeNameDataTextBox.Name = "codeNameDataTextBox";
            this.codeNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.9124212265014648D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.codeNameDataTextBox.StyleName = "Data";
            this.codeNameDataTextBox.Value = "= Aquilent.Cara.Reporting.ReportUtilities.TrimCode(Fields.CodeNumber) + \" - \" + F" +
                "ields.CodeName";
            // 
            // codeNameGroupFooter
            // 
            this.codeNameGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.codeNameGroupFooter.Name = "codeNameGroupFooter";
            this.codeNameGroupFooter.Style.Visible = false;
            // 
            // codeNameGroup
            // 
            this.codeNameGroup.GroupFooter = this.codeNameGroupFooter;
            this.codeNameGroup.GroupHeader = this.codeNameGroupHeader;
            this.codeNameGroup.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.CodeName + \'-\' + Fields.CodeNumber")});
            this.codeNameGroup.Name = "codeNameGroup";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.PrintOnFirstPage = false;
            this.pageHeader.Style.Visible = true;
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4166665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.reportNameTextBox.StyleName = "PageInfo";
            this.reportNameTextBox.Value = "Comment Report";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.81000000238418579D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
            this.reportHeader.Name = "reportHeader";
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.subReport1.Name = "subReport1";
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "24"));
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Comment Report"));
            this.subReport1.ReportSource = this.reportHeaderLandscape1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(9.9999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.81000000238418579D));
            // 
            // reportHeaderLandscape1
            // 
            this.reportHeaderLandscape1.Name = "reportHeaderLandscape1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.letterIdDataTextBox,
            this.commentTextDataTextBox});
            this.detail.Name = "detail";
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            // 
            // letterIdDataTextBox
            // 
            this.letterIdDataTextBox.CanGrow = true;
            this.letterIdDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5958333015441895D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.letterIdDataTextBox.Name = "letterIdDataTextBox";
            this.letterIdDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.letterIdDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.letterIdDataTextBox.StyleName = "Data";
            this.letterIdDataTextBox.Value = "#{Fields.LetterSequence}-{Fields.CommentNumber}";
            // 
            // commentTextDataTextBox
            // 
            this.commentTextDataTextBox.CanGrow = true;
            this.commentTextDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.commentTextDataTextBox.Name = "commentTextDataTextBox";
            this.commentTextDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.commentTextDataTextBox.StyleName = "Data";
            this.commentTextDataTextBox.Value = "= Aquilent.Cara.Reporting.ReportUtilities.StripInvalidCharacters(Fields.CommentTe" +
                "xt)";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageInfoTextBox,
            this.currentTimeTextBox,
            this.textBox1});
            this.pageFooter.Name = "pageFooter";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2604165077209473D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.5D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "PageInfo";
            this.textBox1.Value = "Comment Report";
            // 
            // CommentReport
            // 
            this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Aquilent.Cara.Reporting.Resources.Cara.Reporting.StyleSheet.xml"));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup,
            this.codeCategoryGroup,
            this.codeNameGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.codeCategoryGroupHeader,
            this.codeCategoryGroupFooter,
            this.codeNameGroupHeader,
            this.codeNameGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail});
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "phaseId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "24";
            this.ReportParameters.Add(reportParameter1);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(10D);
            ((System.ComponentModel.ISupportInitialize)(this.reportHeaderLandscape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
		private Telerik.Reporting.TextBox codeCategoryCaptionTextBox;
		private Telerik.Reporting.TextBox codeNameCaptionTextBox;
		private Telerik.Reporting.TextBox letterIdCaptionTextBox;
		private Telerik.Reporting.TextBox commentTextCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.GroupHeaderSection codeCategoryGroupHeader;
		private Telerik.Reporting.TextBox codeCategoryDataTextBox;
		private Telerik.Reporting.GroupFooterSection codeCategoryGroupFooter;
		private Telerik.Reporting.Group codeCategoryGroup;
		private Telerik.Reporting.GroupHeaderSection codeNameGroupHeader;
		private Telerik.Reporting.TextBox codeNameDataTextBox;
		private Telerik.Reporting.GroupFooterSection codeNameGroupFooter;
		private Telerik.Reporting.Group codeNameGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.TextBox letterIdDataTextBox;
		private Telerik.Reporting.TextBox commentTextDataTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.SubReport subReport1;
		private ReportHeaderLandscape reportHeaderLandscape1;
	}
}