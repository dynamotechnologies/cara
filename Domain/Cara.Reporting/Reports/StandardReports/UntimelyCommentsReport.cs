using Aquilent.EntityAccess;
using System;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using System.Data.Objects;
using System.Configuration;

namespace Aquilent.Cara.Reporting
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class UntimelyCommentsReport : StandardPhaseReport
	{
		public UntimelyCommentsReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));

			IList<UntimelyCommentsReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;  

                IList<UntimelyCommentsReportData> batchData;
                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetUntimelyCommentsReport(phaseId, pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetUntimelyCommentsReport(phaseId, pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<UntimelyCommentsReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
			}

            if (reportData != null && reportData.Count > 0)
            {
                TimeZoneInfo tzi;
                foreach (var item in reportData)
                {
                    if (!string.IsNullOrEmpty(item.TimeZone))
                    {
                        tzi = TimeZoneInfo.FindSystemTimeZoneById(item.TimeZone);
                        item.DateSubmitted = TimeZoneInfo.ConvertTimeFromUtc(item.DateSubmitted, tzi);
                    }
                }
            }

			return reportData;
		}
	}
}