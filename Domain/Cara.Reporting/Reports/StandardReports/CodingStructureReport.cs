namespace Aquilent.Cara.Reporting
{
	using System;
	using System.ComponentModel;
	using System.Drawing;
	using System.Windows.Forms;
	using Telerik.Reporting;
	using Telerik.Reporting.Drawing;
	using Aquilent.Cara.Domain.Report;
	using System.Collections.Generic;

	/// <summary>
	/// Summary description for CommenterOrganizationReport.
	/// </summary>
	public partial class CodingStructureReport : StandardPhaseReport
	{
		public CodingStructureReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

			IList<CodingStructureReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;
				reportData = context.GetCodingStructureReport(phaseId);
			} 

			return reportData;
		}
	}
}