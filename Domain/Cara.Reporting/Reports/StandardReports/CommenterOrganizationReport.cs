namespace Aquilent.Cara.Reporting
{
	using System;
	using System.ComponentModel;
	using System.Drawing;
	using System.Windows.Forms;
	using Telerik.Reporting;
	using Telerik.Reporting.Drawing;
	using Aquilent.Cara.Domain.Report;
	using System.Collections.Generic;
    using System.Data.Objects;
    using System.Configuration;

	/// <summary>
	/// Summary description for CommenterOrganizationReport.
	/// </summary>
	public partial class CommenterOrganizationReport : StandardPhaseReport
	{
		public CommenterOrganizationReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));

			IList<CommenterOrganizationTypeData> reportData;
			using (var context = new CaraReportEntities()) 
			{
                context.CommandTimeout = CommandTimeout;

                IList<CommenterOrganizationTypeData> batchData;
                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetCommenterOrganizationTypeReport(phaseId, pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetCommenterOrganizationTypeReport(phaseId, pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<CommenterOrganizationTypeData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
			}

			return reportData;
		}
	}
}