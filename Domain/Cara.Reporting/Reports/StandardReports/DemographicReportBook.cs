﻿using Aquilent.Cara.Reporting.Demographic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Reporting;

namespace Aquilent.Cara.Reporting
{
	public class DemographicReportBook : ReportBook
	{
		public DemographicReportBook(int phaseId)
		{
			DocumentName = "Demographic Report";

			DeliveryTypeReport dtr = new Demographic.DeliveryTypeReport();
			Reports.Add(dtr);

			LetterTypeReport ltr = new Demographic.LetterTypeReport();
			Reports.Add(ltr);

			//OrganizationTypeReport otr = new Demographic.OrganizationTypeReport();
			//Reports.Add(otr);

			OrganizedLetterCampaignReport olcr = new Demographic.OrganizedLetterCampaignReport();
			Reports.Add(olcr);

			OriginOfLettersReport oolr = new Demographic.OriginOfLettersReport();
			Reports.Add(oolr);

			RespondingOrganizationReport ror = new Demographic.RespondingOrganizationReport();
			Reports.Add(ror);

			foreach (var report in Reports)
			{
				var spr = (StandardPhaseReport) report;
				spr.PhaseId = phaseId;
			}
		}
	}
}
