namespace Aquilent.Cara.Reporting
{
	partial class CommenterOrganizationReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.currentTimeTextBox = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.entityDataSource1 = new Telerik.Reporting.EntityDataSource();
			this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
			this.subReport1 = new Telerik.Reporting.SubReport();
			this.reportHeaderLandscape1 = new Aquilent.Cara.Reporting.ReportHeaderLandscape();
			this.group1 = new Telerik.Reporting.Group();
			this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
			this.letterIdCaptionTextBox = new Telerik.Reporting.TextBox();
			this.authorCaptionTextBox = new Telerik.Reporting.TextBox();
			this.codeNameCaptionTextBox = new Telerik.Reporting.TextBox();
			this.codeCategoryCaptionTextBox = new Telerik.Reporting.TextBox();
			this.group2 = new Telerik.Reporting.Group();
			this.groupFooterSection2 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection2 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.group3 = new Telerik.Reporting.Group();
			this.groupFooterSection3 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection3 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox3 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize) (this.reportHeaderLandscape1)).BeginInit();
			((System.ComponentModel.ISupportInitialize) (this)).BeginInit();
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			this.pageHeaderSection1.PrintOnFirstPage = false;
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.4166665077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Organization Type Report";
			// 
			// detail
			// 
			this.detail.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5});
			this.detail.Name = "detail";
			this.detail.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// textBox4
			// 
			this.textBox4.CanGrow = true;
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.8000006675720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.9999210834503174D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.StyleName = "Data";
			this.textBox4.Value = "=IsNull(Fields.LastName, \"\") + \", \" + IsNull(Fields.FirstName, \"\")";
			// 
			// textBox5
			// 
			this.textBox5.CanGrow = true;
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.8000006675720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox5.StyleName = "Data";
			this.textBox5.Value = "=Fields.LetterSequence";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.currentTimeTextBox,
            this.pageInfoTextBox});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999999552965164D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox1.StyleName = "PageInfo";
			this.textBox1.Value = "Organization Type Report";
			// 
			// currentTimeTextBox
			// 
			this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Name = "currentTimeTextBox";
			this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.currentTimeTextBox.StyleName = "PageInfo";
			this.currentTimeTextBox.Value = "=NOW()";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.2599997520446777D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
			// 
			// entityDataSource1
			// 
			this.entityDataSource1.ConnectionString = "CaraReportEntities";
			this.entityDataSource1.Name = "entityDataSource1";
			this.entityDataSource1.ObjectContext = typeof(Aquilent.Cara.Domain.Report.CaraReportEntities);
			this.entityDataSource1.ObjectContextMember = "GetCommenterOrganizationTypeReport";
			this.entityDataSource1.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("phaseId", typeof(int), "25")});
			// 
			// reportHeaderSection1
			// 
			this.reportHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.81000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
			this.reportHeaderSection1.Name = "reportHeaderSection1";
			// 
			// subReport1
			// 
			this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.subReport1.Name = "subReport1";
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "25"));
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Organization Type Report"));
			this.subReport1.ReportSource = this.reportHeaderLandscape1;
			this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(9.9999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.81000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch));
			// 
			// group1
			// 
			this.group1.GroupFooter = this.groupFooterSection1;
			this.group1.GroupHeader = this.groupHeaderSection1;
			this.group1.Name = "group1";
			// 
			// groupFooterSection1
			// 
			this.groupFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection1.Name = "groupFooterSection1";
			this.groupFooterSection1.Style.Visible = false;
			// 
			// groupHeaderSection1
			// 
			this.groupHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.letterIdCaptionTextBox,
            this.authorCaptionTextBox,
            this.codeNameCaptionTextBox,
            this.codeCategoryCaptionTextBox});
			this.groupHeaderSection1.Name = "groupHeaderSection1";
			this.groupHeaderSection1.PrintOnEveryPage = true;
			// 
			// letterIdCaptionTextBox
			// 
			this.letterIdCaptionTextBox.CanGrow = true;
			this.letterIdCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.8000006675720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.letterIdCaptionTextBox.Name = "letterIdCaptionTextBox";
			this.letterIdCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.letterIdCaptionTextBox.StyleName = "Caption";
			this.letterIdCaptionTextBox.Value = "Letter #";
			// 
			// authorCaptionTextBox
			// 
			this.authorCaptionTextBox.CanGrow = true;
			this.authorCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.8000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.authorCaptionTextBox.Name = "authorCaptionTextBox";
			this.authorCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.9999215602874756D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.authorCaptionTextBox.StyleName = "Caption";
			this.authorCaptionTextBox.Value = "Author";
			// 
			// codeNameCaptionTextBox
			// 
			this.codeNameCaptionTextBox.CanGrow = true;
			this.codeNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.8000000715255737D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeNameCaptionTextBox.Name = "codeNameCaptionTextBox";
			this.codeNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.9999215602874756D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeNameCaptionTextBox.StyleName = "Caption";
			this.codeNameCaptionTextBox.Value = "Organization";
			// 
			// codeCategoryCaptionTextBox
			// 
			this.codeCategoryCaptionTextBox.CanGrow = true;
			this.codeCategoryCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10999997705221176D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeCategoryCaptionTextBox.Name = "codeCategoryCaptionTextBox";
			this.codeCategoryCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7799211740493774D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.codeCategoryCaptionTextBox.StyleName = "Caption";
			this.codeCategoryCaptionTextBox.Value = "Organization Type";
			// 
			// group2
			// 
			this.group2.GroupFooter = this.groupFooterSection2;
			this.group2.GroupHeader = this.groupHeaderSection2;
			this.group2.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.OrgTypeName")});
			this.group2.Name = "group2";
			// 
			// groupFooterSection2
			// 
			this.groupFooterSection2.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection2.Name = "groupFooterSection2";
			this.groupFooterSection2.Style.Visible = false;
			// 
			// groupHeaderSection2
			// 
			this.groupHeaderSection2.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2});
			this.groupHeaderSection2.Name = "groupHeaderSection2";
			this.groupHeaderSection2.PrintOnEveryPage = true;
			// 
			// textBox2
			// 
			this.textBox2.CanGrow = true;
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.019999980926513672D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.3800003528594971D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.StyleName = "Data";
			this.textBox2.Value = "=Fields.OrgTypeName";
			// 
			// group3
			// 
			this.group3.GroupFooter = this.groupFooterSection3;
			this.group3.GroupHeader = this.groupHeaderSection3;
			this.group3.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.OrgName")});
			this.group3.Name = "group3";
			// 
			// groupFooterSection3
			// 
			this.groupFooterSection3.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupFooterSection3.Name = "groupFooterSection3";
			this.groupFooterSection3.Style.Visible = false;
			// 
			// groupHeaderSection3
			// 
			this.groupHeaderSection3.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.groupHeaderSection3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3});
			this.groupHeaderSection3.Name = "groupHeaderSection3";
			this.groupHeaderSection3.PrintOnEveryPage = true;
			// 
			// textBox3
			// 
			this.textBox3.CanGrow = true;
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.7999998331069946D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(5.0000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.StyleName = "Data";
			this.textBox3.Value = "=Fields.OrgName";
			// 
			// CommenterOrganizationReport
			// 
			this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Aquilent.Cara.Reporting.Resources.Cara.Reporting.StyleSheet.xml"));
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.group1,
            this.group2,
            this.group3});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.groupHeaderSection2,
            this.groupFooterSection2,
            this.groupHeaderSection3,
            this.groupFooterSection3,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
			this.PageSettings.Landscape = true;
			this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			reportParameter1.Name = "phaseId";
			reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
			reportParameter1.Value = "25";
			this.ReportParameters.Add(reportParameter1);
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch);
			((System.ComponentModel.ISupportInitialize) (this.reportHeaderLandscape1)).EndInit();
			((System.ComponentModel.ISupportInitialize) (this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.PageFooterSection pageFooterSection1;
		private Telerik.Reporting.EntityDataSource entityDataSource1;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
		private Telerik.Reporting.SubReport subReport1;
		private Telerik.Reporting.Group group1;
		private Telerik.Reporting.GroupFooterSection groupFooterSection1;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
		private Telerik.Reporting.TextBox letterIdCaptionTextBox;
		private Telerik.Reporting.TextBox authorCaptionTextBox;
		private Telerik.Reporting.TextBox codeNameCaptionTextBox;
		private Telerik.Reporting.TextBox codeCategoryCaptionTextBox;
		private Telerik.Reporting.Group group2;
		private Telerik.Reporting.GroupFooterSection groupFooterSection2;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection2;
		private ReportHeaderLandscape reportHeaderLandscape1;
		private Telerik.Reporting.Group group3;
		private Telerik.Reporting.GroupFooterSection groupFooterSection3;
		private Telerik.Reporting.GroupHeaderSection groupHeaderSection3;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
	}
}