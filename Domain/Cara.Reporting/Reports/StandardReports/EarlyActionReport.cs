namespace Aquilent.Cara.Reporting
{
	using System;
	using System.ComponentModel;
	using System.Drawing;
	using System.Windows.Forms;
	using Telerik.Reporting;
	using Telerik.Reporting.Drawing;
	using Aquilent.Cara.Domain.Report;
	using System.Collections.Generic;
	using System.Linq;
    using System.Data.Objects;
    using System.Configuration;

	/// <summary>
	/// Summary description for CommenterOrganizationReport.
	/// </summary>
	public partial class EarlyActionReport : StandardPhaseReport
	{
		private Dictionary<string, int> CategoryCount = new Dictionary<string, int>();

		public EarlyActionReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));

			IList<EarlyActionReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;

                IList<EarlyActionReportData> batchData;
                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetEarlyActionReport(phaseId, pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetEarlyActionReport(phaseId, pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<EarlyActionReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
			}

			var categories = reportData.Select(x => x.TermListName).Distinct();
			foreach (string cat in categories)
			{
				CategoryCount.Add(cat, reportData.Where(x => x.TermListName == cat).Count());
			}

			var eaTypes = reportData.Select(x => x.EaType).Distinct();
			foreach (string eaType in eaTypes)
			{
				CategoryCount.Add(eaType, reportData.Where(x => x.EaType == eaType).Count());
			}
 
			return reportData;
		}

		public int GetCategoryCount(string categoryName)
		{
			return CategoryCount[categoryName];
		}
	}
}