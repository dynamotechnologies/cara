namespace Aquilent.Cara.Reporting
{
	using Aquilent.Cara.Domain.Report;
	using System;
	using System.Collections.Generic;
    using System.Data.Objects;
    using System.Configuration;

	/// <summary>
	/// Summary description for CommentReport.
	/// </summary>
	public partial class ResponseStatusReport : StandardPhaseReport
	{
		public ResponseStatusReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));

			IList<ResponseStatusReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;

                IList<ResponseStatusReportData> batchData;
                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetResponseStatusReport(phaseId, pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetResponseStatusReport(phaseId, pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<ResponseStatusReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
            }
            foreach (ResponseStatusReportData row in reportData)
            {
                row.CommentText = ReportManager.CleanCommentText(row.CommentText);
            }

			return reportData;
		}
	}
}