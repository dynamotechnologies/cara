using Aquilent.EntityAccess;
using System;
using System.Data;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using System.Data.Objects;
using System.Configuration;

namespace Aquilent.Cara.Reporting
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class ConcernResponseByCommenterReport : StandardPhaseReport
	{
		public ConcernResponseByCommenterReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

        protected override object GetStandardReportData(int phaseId)
        {
            (this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            var output = new ObjectParameter("totalRows", typeof(int));
           
            IList<ConcernResponseByCommenterReportData> reportData;
            using (var context = new CaraReportEntities())
            {
                context.CommandTimeout = CommandTimeout;

                IList<ConcernResponseByCommenterReportData> batchData;
                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = context.GetConcernResponseByCommenterReport(phaseId, Convert.ToInt32(ReportParameters["option"].Value), pageNumber, batchAmount, output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = context.GetConcernResponseByCommenterReport(phaseId, Convert.ToInt32(ReportParameters["option"].Value), pageNumber, batchAmount, output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<ConcernResponseByCommenterReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
            }

            return reportData;
        }
	}
}