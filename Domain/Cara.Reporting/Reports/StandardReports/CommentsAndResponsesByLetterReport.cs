using Aquilent.EntityAccess;
using System;
using System.Data;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using System.Data.Objects;
using Aquilent.Cara.Domain;
using System.Configuration;
using System.Linq;

namespace Aquilent.Cara.Reporting
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class CommentsAndResponsesByLetterReport : StandardPhaseReport
	{
        public CommentsAndResponsesByLetterReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
            //CARA-927 Change the name of this report
            this.Name = "Response to Comment By Author";

		}

        protected override object GetStandardReportData(int phaseId)
        {
            (this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

            int output = 0;

            IList<CommentsAndResponsesByLetterReportData> reportData;
            int? letterId = null;
            using (var manager = ManagerFactory.CreateInstance<ReportManager>())
            {
                manager.UnitOfWork.Context.CommandTimeout = CommandTimeout;

                if (ReportParameters["option"].Value != null && int.Parse(ReportParameters["option"].Value.ToString()) > 0)
                {
                    letterId = int.Parse(ReportParameters["option"].Value.ToString());
                    this.Name = "Response to Comment By Author (Objection Summary - Letter #" + ReportParameters["letternumber"].Value.ToString() + ")";
                }

                IList<CommentsAndResponsesByLetterReportData> batchData;
                int batchAmount = 5000;
                string batchAmountSetting = ConfigurationManager.AppSettings["BatchAmount"];
                if (batchAmountSetting != null)
                {
                    batchAmount = Convert.ToInt32(batchAmountSetting);
                }
                int pageNumber = 1;
                reportData = manager.GetCommentsAndResponsesByLetterReport(phaseId, letterId, pageNumber, batchAmount, out output);
                pageNumber++;
                int recordsRetrieved = reportData.Count;
                while (batchAmount == recordsRetrieved)
                {
                    batchData = manager.GetCommentsAndResponsesByLetterReport(phaseId, letterId, pageNumber, batchAmount, out output);
                    if (batchData.Count > 0)
                    {
                        (reportData as List<CommentsAndResponsesByLetterReportData>).AddRange(batchData);
                    }
                    recordsRetrieved = batchData.Count;
                    pageNumber++;
                }
            }
            foreach (CommentsAndResponsesByLetterReportData row in reportData)
            {
                row.CommentText = ReportManager.CleanCommentText(row.CommentText);
            }

            return reportData;
        }
	}
}