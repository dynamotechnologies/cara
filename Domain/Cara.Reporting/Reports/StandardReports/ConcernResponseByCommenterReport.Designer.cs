namespace Aquilent.Cara.Reporting
{
	partial class ConcernResponseByCommenterReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.teamMemberFirstNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.roleNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberPhoneCaptionTextBox = new Telerik.Reporting.TextBox();
            this.teamMemberEmailCaptionTextBox = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.reportNameTextBox = new Telerik.Reporting.TextBox();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.reportHeaderLandscape1 = new Aquilent.Cara.Reporting.ReportHeaderLandscape();
            this.detail = new Telerik.Reporting.DetailSection();
            this.lastName = new Telerik.Reporting.TextBox();
            this.firstName = new Telerik.Reporting.TextBox();
            this.organizationName = new Telerik.Reporting.TextBox();
            this.concernResponseNumbers = new Telerik.Reporting.TextBox();
            this.letterNumber = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportHeaderLandscape1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D);
            this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.teamMemberFirstNameCaptionTextBox,
            this.roleNameCaptionTextBox,
            this.teamMemberPhoneCaptionTextBox,
            this.teamMemberEmailCaptionTextBox,
            this.textBox2});
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // teamMemberFirstNameCaptionTextBox
            // 
            this.teamMemberFirstNameCaptionTextBox.CanGrow = true;
            this.teamMemberFirstNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberFirstNameCaptionTextBox.Name = "teamMemberFirstNameCaptionTextBox";
            this.teamMemberFirstNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3791667222976685D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberFirstNameCaptionTextBox.StyleName = "Caption";
            this.teamMemberFirstNameCaptionTextBox.Value = "Last Name";
            // 
            // roleNameCaptionTextBox
            // 
            this.roleNameCaptionTextBox.CanGrow = true;
            this.roleNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.4000788927078247D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.roleNameCaptionTextBox.Name = "roleNameCaptionTextBox";
            this.roleNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7998423576354981D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.roleNameCaptionTextBox.StyleName = "Caption";
            this.roleNameCaptionTextBox.Value = "First Name";
            // 
            // teamMemberPhoneCaptionTextBox
            // 
            this.teamMemberPhoneCaptionTextBox.CanGrow = true;
            this.teamMemberPhoneCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberPhoneCaptionTextBox.Name = "teamMemberPhoneCaptionTextBox";
            this.teamMemberPhoneCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0999214649200439D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberPhoneCaptionTextBox.StyleName = "Caption";
            this.teamMemberPhoneCaptionTextBox.Value = "Organization";
            // 
            // teamMemberEmailCaptionTextBox
            // 
            this.teamMemberEmailCaptionTextBox.CanGrow = true;
            this.teamMemberEmailCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.8005166053771973D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.teamMemberEmailCaptionTextBox.Name = "teamMemberEmailCaptionTextBox";
            this.teamMemberEmailCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1994438171386719D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.teamMemberEmailCaptionTextBox.StyleName = "Caption";
            this.teamMemberEmailCaptionTextBox.Value = "Concern Response Sequence";
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = true;
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5004376173019409D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.StyleName = "Caption";
            this.textBox2.Value = "Letter #";
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.PrintOnFirstPage = false;
            this.pageHeader.Style.Visible = true;
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4791665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.reportNameTextBox.StyleName = "PageInfo";
            this.reportNameTextBox.Value = "Concern Response By Commenter Report";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox1});
            this.pageFooter.Name = "pageFooter";
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.5D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2604165077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1791667938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "PageInfo";
            this.textBox1.Value = "Concern Response By Commenter Report";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.9187501072883606D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.subReport1.Name = "subReport1";
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "=Parameters.phaseId.Value"));
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Concern Response By Commenter Report"));
            this.subReport1.ReportSource = this.reportHeaderLandscape1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(10D), Telerik.Reporting.Drawing.Unit.Inch(0.87999999523162842D));
            // 
            // reportHeaderLandscape1
            // 
            this.reportHeaderLandscape1.Name = "reportHeaderLandscape1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lastName,
            this.firstName,
            this.organizationName,
            this.concernResponseNumbers,
            this.letterNumber});
            this.detail.Name = "detail";
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            // 
            // lastName
            // 
            this.lastName.CanGrow = true;
            this.lastName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(3.9259593904716894E-05D));
            this.lastName.Name = "lastName";
            this.lastName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2791666984558106D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.lastName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.lastName.StyleName = "Data";
            this.lastName.Value = "{Fields.LastName}";
            // 
            // firstName
            // 
            this.firstName.CanGrow = true;
            this.firstName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.4000791311264038D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.firstName.Name = "firstName";
            this.firstName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6999207735061646D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.firstName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.firstName.StyleName = "Data";
            this.firstName.Value = "{Fields.FirstName}";
            // 
            // organizationName
            // 
            this.organizationName.CanGrow = true;
            this.organizationName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.organizationName.Name = "organizationName";
            this.organizationName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.organizationName.StyleName = "Data";
            this.organizationName.Value = "{Fields.OrganizationName}";
            // 
            // concernResponseNumbers
            // 
            this.concernResponseNumbers.CanGrow = true;
            this.concernResponseNumbers.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.8005166053771973D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.concernResponseNumbers.Name = "concernResponseNumbers";
            this.concernResponseNumbers.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0994834899902344D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.concernResponseNumbers.StyleName = "Data";
            this.concernResponseNumbers.Value = "= IsNull(Fields.ConcernResponseNumber, \"\")";
            // 
            // letterNumber
            // 
            this.letterNumber.CanGrow = true;
            this.letterNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.letterNumber.Name = "letterNumber";
            this.letterNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5004372596740723D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.letterNumber.StyleName = "Data";
            this.letterNumber.Value = "{Fields.LetterSequence}";
            // 
            // ConcernResponseByCommenterReport
            // 
            this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Data\\Cara.Reporting.StyleSheet.xml"));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail});
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "phaseId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "24";
            reportParameter2.Name = "option";
            reportParameter2.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.Value = "1";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule1.Style.BackgroundColor = System.Drawing.Color.Empty;
            styleRule1.Style.Color = System.Drawing.Color.Black;
            styleRule1.Style.Font.Bold = true;
            styleRule1.Style.Font.Italic = true;
            styleRule1.Style.Font.Name = "Tahoma";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Bold = true;
            styleRule2.Style.Font.Italic = false;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            styleRule2.Style.Font.Strikeout = false;
            styleRule2.Style.Font.Underline = false;
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule4.Style.Color = System.Drawing.Color.Black;
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(10D);
            ((System.ComponentModel.ISupportInitialize)(this.reportHeaderLandscape1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
		private Telerik.Reporting.TextBox teamMemberFirstNameCaptionTextBox;
		private Telerik.Reporting.TextBox roleNameCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberEmailCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.TextBox lastName;
		private Telerik.Reporting.TextBox firstName;
		private Telerik.Reporting.TextBox organizationName;
		private Telerik.Reporting.TextBox concernResponseNumbers;
		private Telerik.Reporting.SubReport subReport1;
		private Telerik.Reporting.TextBox textBox1;
		private ReportHeaderLandscape reportHeaderLandscape1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox letterNumber;
	}
}