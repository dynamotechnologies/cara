namespace Aquilent.Cara.Reporting
{
    partial class CommentWithCRInfoReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.CRNumberCaptionTextBox = new Telerik.Reporting.TextBox();
            this.CRCodesCaptionTextBox = new Telerik.Reporting.TextBox();
            this.CommentNumberCaptionTextBox = new Telerik.Reporting.TextBox();
            this.CommentCodesCaptionTextBox = new Telerik.Reporting.TextBox();
            this.AssignedToTextBox = new Telerik.Reporting.TextBox();
            this.SampleStatementTextBox = new Telerik.Reporting.TextBox();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.reportNameTextBox = new Telerik.Reporting.TextBox();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.reportHeader1 = new Aquilent.Cara.Reporting.ReportHeader();
            this.detail = new Telerik.Reporting.DetailSection();
            this.CRNumberDataTextBox = new Telerik.Reporting.TextBox();
            this.CRCodesDataTextBox = new Telerik.Reporting.TextBox();
            this.CommentNumberDataTextBox = new Telerik.Reporting.TextBox();
            this.CommentTextDataTextBox = new Telerik.Reporting.TextBox();
            this.CommentCodesDataTextBox = new Telerik.Reporting.TextBox();
            this.SampleStatementDataTextBox = new Telerik.Reporting.TextBox();
            this.AssignedToDataTextBox = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportHeader1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.60003918409347534D);
            this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.CRNumberCaptionTextBox,
            this.CRCodesCaptionTextBox,
            this.CommentNumberCaptionTextBox,
            this.CommentCodesCaptionTextBox,
            this.AssignedToTextBox,
            this.SampleStatementTextBox});
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // CRNumberCaptionTextBox
            // 
            this.CRNumberCaptionTextBox.CanGrow = true;
            this.CRNumberCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.CRNumberCaptionTextBox.Name = "CRNumberCaptionTextBox";
            this.CRNumberCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.CRNumberCaptionTextBox.StyleName = "Caption";
            this.CRNumberCaptionTextBox.Value = "C/R #";
            // 
            // CRCodesCaptionTextBox
            // 
            this.CRCodesCaptionTextBox.CanGrow = true;
            this.CRCodesCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60007888078689575D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.CRCodesCaptionTextBox.Name = "CRCodesCaptionTextBox";
            this.CRCodesCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3998817205429077D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.CRCodesCaptionTextBox.StyleName = "Caption";
            this.CRCodesCaptionTextBox.Value = "C/R Codes";
            // 
            // CommentNumberCaptionTextBox
            // 
            this.CommentNumberCaptionTextBox.CanGrow = true;
            this.CommentNumberCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0000393390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.CommentNumberCaptionTextBox.Name = "CommentNumberCaptionTextBox";
            this.CommentNumberCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.CommentNumberCaptionTextBox.StyleName = "Caption";
            this.CommentNumberCaptionTextBox.Value = "Comment #";
            // 
            // CommentCodesCaptionTextBox
            // 
            this.CommentCodesCaptionTextBox.CanGrow = true;
            this.CommentCodesCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0000393390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.CommentCodesCaptionTextBox.Name = "CommentCodesCaptionTextBox";
            this.CommentCodesCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999215364456177D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.CommentCodesCaptionTextBox.StyleName = "Caption";
            this.CommentCodesCaptionTextBox.Value = "Comment Codes";
            // 
            // AssignedToTextBox
            // 
            this.AssignedToTextBox.CanGrow = true;
            this.AssignedToTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3000397682189941D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.AssignedToTextBox.Name = "AssignedToTextBox";
            this.AssignedToTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999205350875855D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.AssignedToTextBox.StyleName = "Caption";
            this.AssignedToTextBox.Value = "Assigned To";
            // 
            // SampleStatementTextBox
            // 
            this.SampleStatementTextBox.CanGrow = true;
            this.SampleStatementTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4000396728515625D), Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D));
            this.SampleStatementTextBox.Name = "SampleStatementTextBox";
            this.SampleStatementTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8999212384223938D), Telerik.Reporting.Drawing.Unit.Inch(0.40000012516975403D));
            this.SampleStatementTextBox.StyleName = "Caption";
            this.SampleStatementTextBox.Value = "Sample Comment";
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.PrintOnFirstPage = false;
            this.pageHeader.Style.Visible = true;
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4791665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.reportNameTextBox.StyleName = "PageInfo";
            this.reportNameTextBox.Value = "Comment With C/R Information Report";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox1});
            this.pageFooter.Name = "pageFooter";
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "PageInfo";
            this.textBox1.Value = "Comment With C/R Information Report";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.9187501072883606D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.subReport1.Name = "subReport1";
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "=Parameters.phaseId.Value"));
            this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Comment With C/R Information Report"));
            this.subReport1.ReportSource = this.reportHeader1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.88117116689682007D));
            // 
            // reportHeader1
            // 
            this.reportHeader1.Name = "reportHeader1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40007880330085754D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.CRNumberDataTextBox,
            this.CRCodesDataTextBox,
            this.CommentNumberDataTextBox,
            this.CommentTextDataTextBox,
            this.CommentCodesDataTextBox,
            this.SampleStatementDataTextBox,
            this.AssignedToDataTextBox});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // CRNumberDataTextBox
            // 
            this.CRNumberDataTextBox.CanGrow = true;
            this.CRNumberDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9259593904716894E-05D));
            this.CRNumberDataTextBox.Name = "CRNumberDataTextBox";
            this.CRNumberDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.CRNumberDataTextBox.StyleName = "Data";
            this.CRNumberDataTextBox.Value = "= Fields.CRNumber";
            // 
            // CRCodesDataTextBox
            // 
            this.CRCodesDataTextBox.CanGrow = true;
            this.CRCodesDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60007888078689575D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.CRCodesDataTextBox.Name = "CRCodesDataTextBox";
            this.CRCodesDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3998817205429077D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.CRCodesDataTextBox.StyleName = "Data";
            this.CRCodesDataTextBox.Value = "=Fields.CRCodes";
            // 
            // CommentNumberDataTextBox
            // 
            this.CommentNumberDataTextBox.CanGrow = true;
            this.CommentNumberDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0000393390655518D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.CommentNumberDataTextBox.Name = "CommentNumberDataTextBox";
            this.CommentNumberDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992132186889648D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.CommentNumberDataTextBox.StyleName = "Data";
            this.CommentNumberDataTextBox.Value = "=Fields.CommentNumber";
            // 
            // CommentTextDataTextBox
            // 
            this.CommentTextDataTextBox.CanGrow = true;
            this.CommentTextDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.20007880032062531D));
            this.CommentTextDataTextBox.Name = "CommentTextDataTextBox";
            this.CommentTextDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.CommentTextDataTextBox.StyleName = "Data";
            this.CommentTextDataTextBox.Value = "=fields.CommentText";
            // 
            // CommentCodesDataTextBox
            // 
            this.CommentCodesDataTextBox.CanGrow = true;
            this.CommentCodesDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0000393390655518D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.CommentCodesDataTextBox.Name = "CommentCodesDataTextBox";
            this.CommentCodesDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999215364456177D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.CommentCodesDataTextBox.StyleName = "Data";
            this.CommentCodesDataTextBox.Value = "=Fields.CommentCodes";
            // 
            // SampleStatementDataTextBox
            // 
            this.SampleStatementDataTextBox.CanGrow = true;
            this.SampleStatementDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4000396728515625D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.SampleStatementDataTextBox.Name = "SampleStatementDataTextBox";
            this.SampleStatementDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89992159605026245D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.SampleStatementDataTextBox.StyleName = "Data";
            this.SampleStatementDataTextBox.Value = "=Fields.SampleStatement";
            // 
            // AssignedToDataTextBox
            // 
            this.AssignedToDataTextBox.CanGrow = true;
            this.AssignedToDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3000397682189941D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.AssignedToDataTextBox.Name = "AssignedToDataTextBox";
            this.AssignedToDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999207735061646D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.AssignedToDataTextBox.StyleName = "Data";
            this.AssignedToDataTextBox.Value = "=Fields.AssignedTo";
            // 
            // CommentWithCRInfoReport
            // 
            this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Data\\Cara.Reporting.StyleSheet.xml"));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail});
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "phaseId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "24";
            this.ReportParameters.Add(reportParameter1);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule1.Style.BackgroundColor = System.Drawing.Color.Empty;
            styleRule1.Style.Color = System.Drawing.Color.Black;
            styleRule1.Style.Font.Bold = true;
            styleRule1.Style.Font.Italic = true;
            styleRule1.Style.Font.Name = "Tahoma";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(235)))), ((int)(((byte)(163)))));
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Bold = true;
            styleRule2.Style.Font.Italic = false;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            styleRule2.Style.Font.Strikeout = false;
            styleRule2.Style.Font.Underline = false;
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule4.Style.Color = System.Drawing.Color.Black;
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.5D);
            ((System.ComponentModel.ISupportInitialize)(this.reportHeader1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
        private Telerik.Reporting.TextBox CRNumberCaptionTextBox;
		private Telerik.Reporting.TextBox CRCodesCaptionTextBox;
        private Telerik.Reporting.TextBox CommentNumberCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox CRNumberDataTextBox;
		private Telerik.Reporting.TextBox CRCodesDataTextBox;
		private Telerik.Reporting.TextBox CommentNumberDataTextBox;
		private Telerik.Reporting.TextBox CommentTextDataTextBox;
		private Telerik.Reporting.SubReport subReport1;
		private ReportHeader reportHeader1;
		private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox CommentCodesCaptionTextBox;
        private Telerik.Reporting.TextBox CommentCodesDataTextBox;
        private Telerik.Reporting.TextBox AssignedToTextBox;
        private Telerik.Reporting.TextBox SampleStatementTextBox;
        private Telerik.Reporting.TextBox SampleStatementDataTextBox;
        private Telerik.Reporting.TextBox AssignedToDataTextBox;
	}
}