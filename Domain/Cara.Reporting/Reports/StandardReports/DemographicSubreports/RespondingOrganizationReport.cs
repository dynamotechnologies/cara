using Aquilent.EntityAccess;
using System;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;

namespace Aquilent.Cara.Reporting.Demographic
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class RespondingOrganizationReport : StandardPhaseReport
	{
		public RespondingOrganizationReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

			IList<RespondingOrgReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;
                reportData = context.GetRespondingOrgReport(phaseId);
			}

			return reportData;
		}
	}
}