using Aquilent.EntityAccess;
using System;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using Telerik.Reporting.Charting;

namespace Aquilent.Cara.Reporting.Demographic
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class OriginOfLettersReport : StandardPhaseReport
	{
		public OriginOfLettersReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
			this.chart1.NeedDataSource += new EventHandler(this.chart1_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

			IList<OriginOfLettersReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;
                reportData = context.GetOriginOfLettersReport(phaseId);
			}

			return reportData;
		}

		private void chart1_NeedDataSource(object sender, EventArgs e)
		{
			var list = this.DataSource as IList<OriginOfLettersReportData>;
			
			Telerik.Reporting.Processing.Chart procChart = (Telerik.Reporting.Processing.Chart) sender;
			Telerik.Reporting.Chart defChart = (Telerik.Reporting.Chart) procChart.ItemDefinition;

			if (list.Count > 0)
			{
				if (defChart.Series.Count == 0)
				{
					defChart.Legend.Appearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Bottom;

					if (list.Count > 10)
					{
						defChart.Legend.Visible = false;
					}
					defChart.IntelligentLabelsEnabled = false;
					ChartSeries serie = new ChartSeries();
					serie.Type = ChartSeriesType.Pie;
					serie.Clear();
					serie.Appearance.LegendDisplayMode = Telerik.Reporting.Charting.ChartSeriesLegendDisplayMode.ItemLabels;
					foreach (var reportitem in list)
					{
						ChartSeriesItem item = new ChartSeriesItem();
						item.YValue = (int) reportitem.LetterCount;
						item.Name = (string) reportitem.StateName;
						//item.Label.TextBlock.Text = (string) reportitem.DeliveryTypeName + " - #%";
						serie.Items.Add(item);
					}
					defChart.Series.Add(serie);
				}
			}
			else
			{
				procChart.Visible = false;
				defChart.Visible = false;
			}
		}
	}
}