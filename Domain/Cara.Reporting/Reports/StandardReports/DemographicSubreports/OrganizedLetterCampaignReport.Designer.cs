namespace Aquilent.Cara.Reporting.Demographic
{
	partial class OrganizedLetterCampaignReport
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Telerik.Reporting.Charting.Styles.Corners corners1 = new Telerik.Reporting.Charting.Styles.Corners();
			Telerik.Reporting.Charting.GradientElement gradientElement1 = new Telerik.Reporting.Charting.GradientElement();
			Telerik.Reporting.Charting.GradientElement gradientElement2 = new Telerik.Reporting.Charting.GradientElement();
			Telerik.Reporting.Charting.GradientElement gradientElement3 = new Telerik.Reporting.Charting.GradientElement();
			Telerik.Reporting.Charting.Styles.ChartMargins chartMargins1 = new Telerik.Reporting.Charting.Styles.ChartMargins();
			Telerik.Reporting.Charting.Styles.ChartMargins chartMargins2 = new Telerik.Reporting.Charting.Styles.ChartMargins();
			Telerik.Reporting.Charting.Styles.ChartMargins chartMargins3 = new Telerik.Reporting.Charting.Styles.ChartMargins();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings1 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
			this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
			this.teamMemberFirstNameCaptionTextBox = new Telerik.Reporting.TextBox();
			this.roleNameCaptionTextBox = new Telerik.Reporting.TextBox();
			this.teamMemberPhoneCaptionTextBox = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
			this.labelsGroup = new Telerik.Reporting.Group();
			this.pageHeader = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.pageFooter = new Telerik.Reporting.PageFooterSection();
			this.currentTimeTextBox = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
			this.subReport1 = new Telerik.Reporting.SubReport();
			this.reportHeader1 = new Aquilent.Cara.Reporting.ReportHeader();
			this.detail = new Telerik.Reporting.DetailSection();
			this.roleNameDataTextBox = new Telerik.Reporting.TextBox();
			this.teamMemberPhoneDataTextBox = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.reportFooterSection1 = new Telerik.Reporting.ReportFooterSection();
			this.chart1 = new Telerik.Reporting.Chart();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize) (this.reportHeader1)).BeginInit();
			((System.ComponentModel.ISupportInitialize) (this)).BeginInit();
			// 
			// labelsGroupHeader
			// 
			this.labelsGroupHeader.Height = new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.teamMemberFirstNameCaptionTextBox,
            this.roleNameCaptionTextBox,
            this.teamMemberPhoneCaptionTextBox,
            this.textBox3,
            this.textBox4,
            this.textBox7});
			this.labelsGroupHeader.Name = "labelsGroupHeader";
			this.labelsGroupHeader.PrintOnEveryPage = true;
			// 
			// teamMemberFirstNameCaptionTextBox
			// 
			this.teamMemberFirstNameCaptionTextBox.CanGrow = true;
			this.teamMemberFirstNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberFirstNameCaptionTextBox.Name = "teamMemberFirstNameCaptionTextBox";
			this.teamMemberFirstNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8400000333786011D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberFirstNameCaptionTextBox.StyleName = "Caption";
			this.teamMemberFirstNameCaptionTextBox.Value = "Form Set Name";
			// 
			// roleNameCaptionTextBox
			// 
			this.roleNameCaptionTextBox.CanGrow = true;
			this.roleNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.6992855072021484D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameCaptionTextBox.Name = "roleNameCaptionTextBox";
			this.roleNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.83908778429031372D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameCaptionTextBox.StyleName = "Caption";
			this.roleNameCaptionTextBox.Value = "Letter Count";
			// 
			// teamMemberPhoneCaptionTextBox
			// 
			this.teamMemberPhoneCaptionTextBox.CanGrow = true;
			this.teamMemberPhoneCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3776187896728516D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneCaptionTextBox.Name = "teamMemberPhoneCaptionTextBox";
			this.teamMemberPhoneCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneCaptionTextBox.StyleName = "Caption";
			this.teamMemberPhoneCaptionTextBox.Value = "Signature Count";
			// 
			// textBox3
			// 
			this.textBox3.CanGrow = true;
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.5384521484375D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.83900898694992065D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox3.StyleName = "Caption";
			this.textBox3.Value = "Form Count";
			// 
			// textBox4
			// 
			this.textBox4.CanGrow = true;
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.377540111541748D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox4.StyleName = "Caption";
			this.textBox4.Value = "Form Plus Count";
			// 
			// labelsGroupFooter
			// 
			this.labelsGroupFooter.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.labelsGroupFooter.Name = "labelsGroupFooter";
			this.labelsGroupFooter.PageBreak = Telerik.Reporting.PageBreak.None;
			this.labelsGroupFooter.Style.Padding.Top = new Telerik.Reporting.Drawing.Unit(0.30000001192092896D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.labelsGroupFooter.Style.Visible = false;
			// 
			// labelsGroup
			// 
			this.labelsGroup.GroupFooter = this.labelsGroupFooter;
			this.labelsGroup.GroupHeader = this.labelsGroupHeader;
			this.labelsGroup.Name = "labelsGroup";
			// 
			// pageHeader
			// 
			this.pageHeader.Height = new Telerik.Reporting.Drawing.Unit(0.28125D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeader.Name = "pageHeader";
			this.pageHeader.Style.Visible = false;
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.BookmarkId = "";
			this.reportNameTextBox.DocumentMapText = "";
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.4791665077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Demographic Report > Organized Letter Campaign";
			// 
			// pageFooter
			// 
			this.pageFooter.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox1});
			this.pageFooter.Name = "pageFooter";
			this.pageFooter.Style.Visible = false;
			// 
			// currentTimeTextBox
			// 
			this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Name = "currentTimeTextBox";
			this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4999608993530273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.currentTimeTextBox.StyleName = "PageInfo";
			this.currentTimeTextBox.Value = "=NOW()";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.7000000476837158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber + \" of \" + PageCount";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.5791666507720947D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox1.StyleName = "PageInfo";
			this.textBox1.Value = "Demographic Report > Organized Letter Campaign";
			// 
			// reportHeader
			// 
			this.reportHeader.Height = new Telerik.Reporting.Drawing.Unit(0.9187501072883606D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1});
			this.reportHeader.Name = "reportHeader";
			this.reportHeader.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int) (((byte) (242)))), ((int) (((byte) (235)))), ((int) (((byte) (163)))));
			this.reportHeader.Style.Visible = true;
			// 
			// subReport1
			// 
			this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.subReport1.Name = "subReport1";
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("phaseId", "=Parameters.phaseId.Value"));
			this.subReport1.Parameters.Add(new Telerik.Reporting.Parameter("reportName", "Organized Letter Campaign Report"));
			this.subReport1.ReportSource = this.reportHeader1;
			this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.4999604225158691D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.88117116689682007D, Telerik.Reporting.Drawing.UnitType.Inch));
			// 
			// detail
			// 
			this.detail.Height = new Telerik.Reporting.Drawing.Unit(0.2800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.roleNameDataTextBox,
            this.teamMemberPhoneDataTextBox,
            this.textBox2,
            this.textBox5,
            this.textBox6,
            this.textBox8});
			this.detail.Name = "detail";
			this.detail.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
			// 
			// roleNameDataTextBox
			// 
			this.roleNameDataTextBox.CanGrow = true;
			this.roleNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.6992855072021484D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameDataTextBox.Name = "roleNameDataTextBox";
			this.roleNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.roleNameDataTextBox.StyleName = "DataCount";
			this.roleNameDataTextBox.Value = "=Fields.LetterCount";
			// 
			// teamMemberPhoneDataTextBox
			// 
			this.teamMemberPhoneDataTextBox.CanGrow = true;
			this.teamMemberPhoneDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3776187896728516D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneDataTextBox.Name = "teamMemberPhoneDataTextBox";
			this.teamMemberPhoneDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.teamMemberPhoneDataTextBox.StyleName = "DataCount";
			this.teamMemberPhoneDataTextBox.Value = "=Fields.SignatureCount";
			// 
			// textBox2
			// 
			this.textBox2.CanGrow = true;
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.5384521484375D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox2.StyleName = "DataCount";
			this.textBox2.Value = "=Fields.FormCount";
			// 
			// textBox5
			// 
			this.textBox5.CanGrow = true;
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.377540111541748D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox5.StyleName = "DataCount";
			this.textBox5.Value = "=Fields.FormPlusCount";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.8601188659667969D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.011687596328556538D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox6.StyleName = "Data";
			this.textBox6.Value = "= Fields.LetterSequence";
			// 
			// reportFooterSection1
			// 
			this.reportFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(6.320000171661377D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.reportFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.chart1});
			this.reportFooterSection1.Name = "reportFooterSection1";
			this.reportFooterSection1.PageBreak = Telerik.Reporting.PageBreak.Before;
			// 
			// chart1
			// 
			this.chart1.Appearance.Border.Color = System.Drawing.Color.FromArgb(((int) (((byte) (212)))), ((int) (((byte) (221)))), ((int) (((byte) (222)))));
			corners1.BottomLeft = Telerik.Reporting.Charting.Styles.CornerType.Round;
			corners1.BottomRight = Telerik.Reporting.Charting.Styles.CornerType.Round;
			corners1.RoundSize = 7;
			corners1.TopLeft = Telerik.Reporting.Charting.Styles.CornerType.Round;
			corners1.TopRight = Telerik.Reporting.Charting.Styles.CornerType.Round;
			this.chart1.Appearance.Corners = corners1;
			gradientElement1.Color = System.Drawing.Color.FromArgb(((int) (((byte) (243)))), ((int) (((byte) (253)))), ((int) (((byte) (255)))));
			gradientElement2.Color = System.Drawing.Color.White;
			gradientElement2.Position = 0.5F;
			gradientElement3.Color = System.Drawing.Color.FromArgb(((int) (((byte) (243)))), ((int) (((byte) (253)))), ((int) (((byte) (255)))));
			gradientElement3.Position = 1F;
			this.chart1.Appearance.FillStyle.FillSettings.ComplexGradient.AddRange(new Telerik.Reporting.Charting.GradientElement[] {
            gradientElement1,
            gradientElement2,
            gradientElement3});
			this.chart1.Appearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.ComplexGradient;
			this.chart1.AutoLayout = true;
			this.chart1.BitmapResolution = 96F;
			this.chart1.BookmarkId = "Chart";
			chartMargins1.Left = new Telerik.Reporting.Charting.Styles.Unit(6D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMargins1.Top = new Telerik.Reporting.Charting.Styles.Unit(3D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			this.chart1.ChartTitle.Appearance.Dimensions.Margins = chartMargins1;
			this.chart1.ChartTitle.Appearance.FillStyle.MainColor = System.Drawing.Color.Empty;
			this.chart1.ChartTitle.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (86)))), ((int) (((byte) (88)))), ((int) (((byte) (89)))));
			this.chart1.ChartTitle.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Verdana", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chart1.ChartTitle.TextBlock.Text = "Organized Letter Campaign";
			this.chart1.DocumentMapText = "Chart";
			this.chart1.ImageFormat = System.Drawing.Imaging.ImageFormat.Emf;
			this.chart1.Legend.Appearance.Border.Color = System.Drawing.Color.Empty;
			chartMargins2.Bottom = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMargins2.Right = new Telerik.Reporting.Charting.Styles.Unit(1D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			this.chart1.Legend.Appearance.Dimensions.Margins = chartMargins2;
			this.chart1.Legend.Appearance.FillStyle.MainColor = System.Drawing.Color.Empty;
			this.chart1.Legend.Appearance.ItemMarkerAppearance.Figure = "Rectangle";
			this.chart1.Legend.Appearance.ItemTextAppearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (86)))), ((int) (((byte) (88)))), ((int) (((byte) (89)))));
			this.chart1.Legend.Appearance.Visible = false;
			this.chart1.Legend.TextBlock.Appearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Top;
			this.chart1.Legend.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (86)))), ((int) (((byte) (88)))), ((int) (((byte) (89)))));
			this.chart1.Legend.Visible = false;
			this.chart1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.09375D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.010416666977107525D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.chart1.Name = "chart1";
			this.chart1.PlotArea.Appearance.Border.Color = System.Drawing.Color.WhiteSmoke;
			chartMargins3.Bottom = new Telerik.Reporting.Charting.Styles.Unit(12D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMargins3.Left = new Telerik.Reporting.Charting.Styles.Unit(9D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMargins3.Right = new Telerik.Reporting.Charting.Styles.Unit(90D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartMargins3.Top = new Telerik.Reporting.Charting.Styles.Unit(19D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			this.chart1.PlotArea.Appearance.Dimensions.Margins = chartMargins3;
			this.chart1.PlotArea.Appearance.FillStyle.MainColor = System.Drawing.Color.Transparent;
			this.chart1.PlotArea.Appearance.FillStyle.SecondColor = System.Drawing.Color.Transparent;
			this.chart1.PlotArea.EmptySeriesMessage.Appearance.Visible = true;
			this.chart1.PlotArea.EmptySeriesMessage.Visible = true;
			this.chart1.PlotArea.XAxis.Appearance.Color = System.Drawing.Color.FromArgb(((int) (((byte) (134)))), ((int) (((byte) (134)))), ((int) (((byte) (134)))));
			this.chart1.PlotArea.XAxis.Appearance.MajorGridLines.Color = System.Drawing.Color.FromArgb(((int) (((byte) (196)))), ((int) (((byte) (196)))), ((int) (((byte) (196)))));
			this.chart1.PlotArea.XAxis.Appearance.MajorGridLines.Width = 0F;
			this.chart1.PlotArea.XAxis.Appearance.MajorTick.Color = System.Drawing.Color.FromArgb(((int) (((byte) (134)))), ((int) (((byte) (134)))), ((int) (((byte) (134)))));
			this.chart1.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (89)))), ((int) (((byte) (89)))), ((int) (((byte) (89)))));
			chartPaddings1.Bottom = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			this.chart1.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.Paddings = chartPaddings1;
			this.chart1.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (51)))), ((int) (((byte) (51)))), ((int) (((byte) (51)))));
			this.chart1.PlotArea.XAxis.AxisLabel.TextBlock.Text = "Letter ID";
			this.chart1.PlotArea.YAxis.Appearance.Color = System.Drawing.Color.FromArgb(((int) (((byte) (134)))), ((int) (((byte) (134)))), ((int) (((byte) (134)))));
			this.chart1.PlotArea.YAxis.Appearance.MajorGridLines.Color = System.Drawing.Color.FromArgb(((int) (((byte) (196)))), ((int) (((byte) (196)))), ((int) (((byte) (196)))));
			this.chart1.PlotArea.YAxis.Appearance.MajorTick.Color = System.Drawing.Color.FromArgb(((int) (((byte) (196)))), ((int) (((byte) (196)))), ((int) (((byte) (196)))));
			this.chart1.PlotArea.YAxis.Appearance.MajorTick.Visible = false;
			this.chart1.PlotArea.YAxis.Appearance.MinorGridLines.Color = System.Drawing.Color.FromArgb(((int) (((byte) (196)))), ((int) (((byte) (196)))), ((int) (((byte) (196)))));
			this.chart1.PlotArea.YAxis.Appearance.MinorGridLines.Width = 0F;
			this.chart1.PlotArea.YAxis.Appearance.MinorTick.Color = System.Drawing.Color.FromArgb(((int) (((byte) (196)))), ((int) (((byte) (196)))), ((int) (((byte) (196)))));
			this.chart1.PlotArea.YAxis.Appearance.MinorTick.Visible = false;
			this.chart1.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (89)))), ((int) (((byte) (89)))), ((int) (((byte) (89)))));
			this.chart1.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.FromArgb(((int) (((byte) (220)))), ((int) (((byte) (158)))), ((int) (((byte) (119)))));
			this.chart1.PlotArea.YAxis.AxisLabel.TextBlock.Text = "Total Form Count";
			this.chart1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6.3000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(6.3000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.chart1.Skin = "Vista";
			// 
			// textBox7
			// 
			this.textBox7.CanGrow = true;
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.8601188659667969D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.83908778429031372D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox7.StyleName = "Caption";
			this.textBox7.Value = "Master Letter #";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7000000476837158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
			this.textBox8.StyleName = "Data";
			this.textBox8.Value = "= Fields.FormSetName";
			// 
			// OrganizedLetterCampaignReport
			// 
			this.BookmarkId = "Organized Letter Campaign";
			this.DocumentMapText = "Organized Letter Campaign";
			this.ExternalStyleSheets.Add(new Telerik.Reporting.Drawing.ExternalStyleSheet("Data\\Cara.Reporting.StyleSheet.xml"));
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.detail,
            this.reportFooterSection1});
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			reportParameter1.Name = "phaseId";
			reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
			reportParameter1.Value = "25";
			this.ReportParameters.Add(reportParameter1);
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Style.Padding.Top = new Telerik.Reporting.Drawing.Unit(0.30000001192092896D, Telerik.Reporting.Drawing.UnitType.Inch);
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
			styleRule1.Style.BackgroundColor = System.Drawing.Color.Empty;
			styleRule1.Style.Color = System.Drawing.Color.Black;
			styleRule1.Style.Font.Bold = true;
			styleRule1.Style.Font.Italic = true;
			styleRule1.Style.Font.Name = "Tahoma";
			styleRule1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(18D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
			styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int) (((byte) (242)))), ((int) (((byte) (235)))), ((int) (((byte) (163)))));
			styleRule2.Style.Color = System.Drawing.Color.Black;
			styleRule2.Style.Font.Bold = true;
			styleRule2.Style.Font.Italic = false;
			styleRule2.Style.Font.Name = "Tahoma";
			styleRule2.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule2.Style.Font.Strikeout = false;
			styleRule2.Style.Font.Underline = false;
			styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
			styleRule3.Style.Color = System.Drawing.Color.Black;
			styleRule3.Style.Font.Name = "Tahoma";
			styleRule3.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
			styleRule4.Style.Color = System.Drawing.Color.Black;
			styleRule4.Style.Font.Name = "Tahoma";
			styleRule4.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("DataCount")});
			styleRule5.Style.Font.Name = "Tahoma";
			styleRule5.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
			styleRule5.Style.Padding.Right = new Telerik.Reporting.Drawing.Unit(0.05000000074505806D, Telerik.Reporting.Drawing.UnitType.Inch);
			styleRule5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5});
			this.Width = new Telerik.Reporting.Drawing.Unit(6.4999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch);
			((System.ComponentModel.ISupportInitialize) (this.reportHeader1)).EndInit();
			((System.ComponentModel.ISupportInitialize) (this)).EndInit();

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
		private Telerik.Reporting.TextBox teamMemberFirstNameCaptionTextBox;
		private Telerik.Reporting.TextBox roleNameCaptionTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneCaptionTextBox;
		private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
		private Telerik.Reporting.Group labelsGroup;
		private Telerik.Reporting.PageHeaderSection pageHeader;
		private Telerik.Reporting.TextBox reportNameTextBox;
		private Telerik.Reporting.PageFooterSection pageFooter;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.ReportHeaderSection reportHeader;
		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.TextBox roleNameDataTextBox;
		private Telerik.Reporting.TextBox teamMemberPhoneDataTextBox;
		private Telerik.Reporting.ReportFooterSection reportFooterSection1;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox currentTimeTextBox;
		private Telerik.Reporting.Chart chart1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.SubReport subReport1;
		private ReportHeader reportHeader1;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox8;
	}
}