using Aquilent.EntityAccess;
using System;
using Aquilent.Cara.Domain.Report;
using System.Collections.Generic;
using Telerik.Reporting.Charting;
using System.Linq;

namespace Aquilent.Cara.Reporting.Demographic
{
	/// <summary>
	/// Summary description for TeamMemberReport.
	/// </summary>
	public partial class OrganizedLetterCampaignReport : StandardPhaseReport
	{
		public OrganizedLetterCampaignReport()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
			this.chart1.NeedDataSource += new EventHandler(this.chart1_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			(this.subReport1.ReportSource as StandardPhaseReport).PhaseId = phaseId;

			IList<OrganizedLetterCampaignReportData> reportData;
			using (var context = new CaraReportEntities())
			{
                context.CommandTimeout = CommandTimeout;
                reportData = context.GetOrganizedLetterCampaignReport(phaseId);
			}

			return reportData;
		}

		private void chart1_NeedDataSource(object sender, EventArgs e)
		{
			var list = this.DataSource as IList<OrganizedLetterCampaignReportData>;

			Telerik.Reporting.Processing.Chart procChart = (Telerik.Reporting.Processing.Chart) sender;
			Telerik.Reporting.Chart defChart = (Telerik.Reporting.Chart) procChart.ItemDefinition;

			if (list.Count > 0)
			{
				if (defChart.Series.Count == 0)
				{
					defChart.Legend.Appearance.Visible = false;
					defChart.IntelligentLabelsEnabled = false;
					defChart.PlotArea.XAxis.AxisLabel.TextBlock.Text = "Letter ID";
					defChart.PlotArea.XAxis.AutoScale = false;
					defChart.PlotArea.XAxis.AddRange(1, list.Count, 1);
					defChart.PlotArea.XAxis.AxisLabel.TextBlock.Visible = true;
					defChart.PlotArea.XAxis.AxisLabel.Visible = true;

					defChart.PlotArea.YAxis.AddRange(0, list.Max(x => (x.FormCount + x.FormPlusCount)).Value, 1);
					defChart.PlotArea.YAxis.AxisLabel.TextBlock.Text = "Total Form Count";
					defChart.PlotArea.YAxis.AxisLabel.TextBlock.Visible = true;
					defChart.PlotArea.YAxis.AxisLabel.Visible = true;

					ChartSeries serie = new ChartSeries();
					serie.Type = ChartSeriesType.Bar;
					serie.Clear();

					int i = 0;
					foreach (var reportitem in list)
					{
						ChartSeriesItem item = new ChartSeriesItem();
						item.YValue = (int) (reportitem.FormCount + reportitem.FormPlusCount);
						item.Label.TextBlock.Text = (reportitem.FormCount + reportitem.FormPlusCount).ToString();
						serie.Items.Add(item);
						defChart.PlotArea.XAxis[i].TextBlock.Text = reportitem.LetterSequence.ToString();
						i++;
					}
					defChart.Series.Add(serie);
				}
			}
			else
			{
				procChart.Visible = false;
				defChart.Visible = false;
			}
		}
	}
}