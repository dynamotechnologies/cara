namespace Aquilent.Cara.Reporting
{
	using System;
	using System.ComponentModel;
	using System.Drawing;
	using System.Windows.Forms;
	using Telerik.Reporting;
	using Telerik.Reporting.Drawing;
	using Aquilent.Cara.Domain.Report;

	/// <summary>
	/// Summary description for ReportHeader.
	/// </summary>
	public partial class ReportHeader : StandardPhaseReport
	{
		public ReportHeader()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.NeedDataSource += new EventHandler(this.report_NeedDataSource);
		}

		protected override object GetStandardReportData(int phaseId)
		{
			ReportHeaderData reportHeaderData;
			using (var context = new CaraReportEntities())
			{
				reportHeaderData = context.GetReportHeader(phaseId);
			}

			return reportHeaderData;
		}
	}
}