﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Domain.Report;
using NPOI.XWPF.UserModel;
using Aquilent.Cara.Reporting.Export.Utilities;

namespace Aquilent.Cara.Reporting.Export
{
    public class ResponseToCommentReportExport : IReportExport
    {
        private ICollection<ResponseToCommentReportData> reportData;
        public ResponseToCommentReportExport(ICollection<ResponseToCommentReportData> reportData)
        {
            if (reportData == null)
            {
                throw new ArgumentNullException("reportData");
            }

            this.reportData = reportData;
        }

        public XWPFDocument Export()
        {
            XWPFDocument doc = new XWPFDocument();

            foreach (var row in reportData)
            {
                var paragraph = doc.CreateParagraph();

                var concernHeader = paragraph.CreateRun();
                concernHeader.SetText(string.Format("Concern: [Seq#{0}] ", row.ResponseSequence));
                concernHeader.SetBold(true);
                concernHeader.AddCarriageReturn();

                if (!string.IsNullOrEmpty(row.ConcernText))
                {
                    HtmlToNpoiWord concernTextConverter = new HtmlToNpoiWord(row.ConcernText.Trim());
                    concernTextConverter.AddToExistingDocument(doc);

                    var concernIdRun = doc.GetLastParagraph().CreateRun();
                    concernIdRun.SetText(string.Format(" [ID#{0}]", row.ResponseNumber));
                    concernIdRun.AddCarriageReturn();
                }
                else
                {
                    concernHeader.AddCarriageReturn();
                }

                var responseHeader = doc.GetLastParagraph().CreateRun();
                responseHeader.SetText(string.Format("Response: [Seq#{0}]", row.ResponseSequence));
                responseHeader.SetBold(true);
                responseHeader.AddCarriageReturn();

                if (!string.IsNullOrEmpty(row.ResponseText))
                {
                    HtmlToNpoiWord responseTextConverter = new HtmlToNpoiWord(row.ResponseText.Trim());
                    responseTextConverter.AddToExistingDocument(doc);

                    var responseIdRun = doc.GetLastParagraph().CreateRun();
                    responseIdRun.SetText(string.Format(" [ID#{0}]", row.ResponseNumber));
                    responseIdRun.AddCarriageReturn();

                }
                else
                {
                    responseHeader.AddCarriageReturn();
                }

                if (row.Comments != null)
                {
                    var commentsHeader = doc.GetLastParagraph().CreateRun();
                    commentsHeader.SetText(string.Format("Associated Comments: [Seq#{0}]", row.ResponseSequence));
                    commentsHeader.SetBold(true);
                    commentsHeader.AddCarriageReturn();

                    var commentRun = doc.GetLastParagraph().CreateRun();
                    foreach (var comment in row.Comments)
                    {
                        commentRun.SetText(string.Format("{0} [{1}]", comment.ConcatenatedCommentText.Trim(), comment.UniqueNumber));
                        commentRun.AddCarriageReturn();
                        commentRun.AddCarriageReturn();
                    }
                }

                var separator = doc.GetLastParagraph().CreateRun();
                separator.AddCarriageReturn();
            }

            return doc;
        }
    }
}
