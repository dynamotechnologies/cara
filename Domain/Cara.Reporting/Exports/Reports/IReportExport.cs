﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.XWPF.UserModel;

namespace Aquilent.Cara.Reporting.Export
{
    public interface IReportExport
    {
        XWPFDocument Export();
    }
}
