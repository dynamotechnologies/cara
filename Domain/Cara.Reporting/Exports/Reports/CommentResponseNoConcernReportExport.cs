﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Cara.Domain.Report;
using NPOI.XWPF.UserModel;
using Aquilent.Cara.Reporting.Export.Utilities;

namespace Aquilent.Cara.Reporting.Export
{
    public class CommentResponseNoConcernReportExport : IReportExport
    {
        private ICollection<CommentResponseNoConcernReportData> reportData;
        public CommentResponseNoConcernReportExport(ICollection<CommentResponseNoConcernReportData> reportData)
        {
            if (reportData == null)
            {
                throw new ArgumentNullException("reportData");
            }

            this.reportData = reportData;
        }

        public XWPFDocument Export()
        {
            XWPFDocument doc = new XWPFDocument();

            foreach (var row in reportData)
            {
                var paragraph = doc.CreateParagraph();

                var commentHeader = paragraph.CreateRun();
                commentHeader.SetText("Comment: ");
                commentHeader.SetBold(true);

                var commentText = paragraph.CreateRun();
                commentText.SetText(string.Format("{0} [{1}]", row.ConcatenatedCommentText, row.UniqueNumber));
                commentText.AddCarriageReturn();

                var responseHeader = paragraph.CreateRun();
                responseHeader.SetText(string.Format("Response [Seq#{0}]: ", row.ResponseNumber));
                responseHeader.SetBold(true);

                if (!string.IsNullOrEmpty(row.ResponseText))
                {
                    HtmlToNpoiWord responseTextConverter = new HtmlToNpoiWord(row.ResponseText);
                    responseTextConverter.AddToExistingDocument(doc);
                }
                else
                {
                    responseHeader.AddCarriageReturn();
                }

                var separator = doc.CreateParagraph().CreateRun();
                separator.AddCarriageReturn();
            }

            return doc;
        }
    }
}
