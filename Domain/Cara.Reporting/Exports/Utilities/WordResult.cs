﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.XWPF.UserModel;
using System.IO;
using System.Configuration;

namespace Aquilent.Cara.Reporting.Export.Web.Mvc
{
    public class WordResult : ActionResult
    {
        private XWPFDocument document;
        private string filename;
        private static readonly string tempFolder = ConfigurationSettings.AppSettings["reportExportFolder"];

        public WordResult(XWPFDocument document, string filename)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            if (string.IsNullOrEmpty(filename))
            {
                filename = "unnamedDocument.docx";
            }

            this.document = document;
            this.filename = filename;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase response = context.HttpContext.Response;
            response.Clear();
            response.ContentType = "application/octet-stream";
            response.Headers.Add("content-disposition", "attachment;  filename=" + filename);

            var tempFilepath = WriteTempFile();
            response.WriteFile(tempFilepath);
            response.End();

            FileInfo fi = new FileInfo(tempFilepath);
            fi.Delete();
        }

        private string WriteTempFile()
        {
            var tempFile = string.Format("{0}{1}_{2}.docx", tempFolder, DateTime.Now.ToString("yyyyMMddHHmmss"), new Random().Next(1000));
            using (var fs = new FileStream(tempFile, FileMode.Create))
            {
                document.Write(fs);
            }

            return tempFile;
        }
    }
}