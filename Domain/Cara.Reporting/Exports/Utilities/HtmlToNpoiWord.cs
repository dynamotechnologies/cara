﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using NPOI.XWPF.UserModel;

namespace Aquilent.Cara.Reporting.Export.Utilities
{
    public class HtmlToNpoiWord
    {
        #region Member Variables
        private XWPFDocument wordDocument;
        private HtmlDocument htmlDocument;
        #endregion Member Variables

        #region Constructors
        public HtmlToNpoiWord(HtmlDocument html)
        {
            if (html == null)
            {
                throw new ArgumentNullException("html");
            }

            if (html.ParseErrors.Count() > 0)
            {
                throw new ArgumentException("The HTML contains parse errors.", "html");
            }

            htmlDocument = html;
        }

        public HtmlToNpoiWord(string htmlString)
        {
            if (string.IsNullOrEmpty(htmlString))
            {
                throw new ArgumentNullException("htmlString");
            }

            htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlString);

            if (htmlDocument.ParseErrors.Count() > 0)
            {
                throw new ArgumentException("The HTML contained in htmlString has parse errors.", "htmlString");
            }
        }
        #endregion Constructors

        public XWPFDocument GetDocument()
        {
            if (wordDocument != null)
            {
                return wordDocument;
            }

            wordDocument = new XWPFDocument();
            ConvertToWord(htmlDocument.DocumentNode);

            return wordDocument;
        }

        public void AddToExistingDocument(XWPFDocument document)
        {
            wordDocument = document;
            ConvertToWord(htmlDocument.DocumentNode);
        }

        private void ConvertToWord(HtmlNode node, string numId = null)
        {
            if (node.OriginalName == "p" || node.OriginalName == "span")
            {
                var paragraph = wordDocument.GetLastParagraph() == null ? wordDocument.GetLastParagraph() : wordDocument.CreateParagraph();
                if (node.HasAttributes)
                {
                    HtmlAttribute alignment = node.Attributes["style"];
                    if (alignment != null)
                    {
                        var styles = alignment.Value.Replace(" ", string.Empty).Split(';');

                        if (styles.Contains("text-align:right"))
                        {
                            paragraph.SetAlignment(ParagraphAlignment.RIGHT);
                        }
                        else if (styles.Contains("text-align:center"))
                        {
                            paragraph.SetAlignment(ParagraphAlignment.CENTER);
                        }
                        else if (styles.Contains("text-align:justify"))
                        {
                            paragraph.SetAlignment(ParagraphAlignment.DISTRIBUTE);
                        }
                        else
                        {
                            paragraph.SetAlignment(ParagraphAlignment.LEFT);
                        }
                    }
                }
            }
            else if (node.OriginalName == "ul" || node.OriginalName == "ol")
            {
                var numbering = wordDocument.CreateNumbering();
                string abstractNumId = numbering.AddAbstractNum();
                numId = numbering.AddNum(abstractNumId);
            }
            // TODO:  Figure out how to render an ordered list
            //else if (node.OriginalName == "ol")
            //{
            //    var numbering = wordDocument.CreateNumbering();
            //    string abstractNumId = numbering.AddAbstractNum();
            //    numId = numbering.AddNum(abstractNumId);
            //}
            else if (node.OriginalName == "#text")
            {
                XWPFRun run;
                if (wordDocument.Paragraphs.Count == 0)
                {
                    run = wordDocument.CreateParagraph().CreateRun();
                }
                else
                {
                    run = wordDocument.GetLastParagraph().CreateRun();
                }
                run.SetText(HtmlEntity.DeEntitize(node.InnerText));

                if (node.ParentNode.OriginalName == "strong" || node.ParentNode.OriginalName == "b")
                {
                    run.SetBold(true);
                }
                else if (node.ParentNode.OriginalName == "em" || node.ParentNode.OriginalName == "i")
                {
                    run.IsItalic = true;
                }
                else if (node.ParentNode.OriginalName == "span")
                {
                    if (node.ParentNode.HasAttributes && node.ParentNode.Attributes.AttributesWithName("style").Count() > 0)
                    {
                        var style = node.ParentNode.Attributes["style"];
                        var styles = style.Value.Replace(" ", string.Empty).Split(';');

                        if (styles.Contains("text-decoration:underline"))
                        {
                            run.SetUnderline(UnderlinePatterns.Single);
                        }
                    }
                }
            }

            foreach (var child in node.ChildNodes)
            {
                if (child.OriginalName != "li")
                {
                    ConvertToWord(child, numId);
                }
                else
                {
                    var paragraph = wordDocument.CreateParagraph();
                    ConvertToWord(child, numId);
                    paragraph.SetNumID(numId);
                }
            }
        }
    }
}
