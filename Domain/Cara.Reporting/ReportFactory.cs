﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Reporting;
using Aquilent.Cara.Domain;
using System.Configuration;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reporting
{
	public static class ReportFactory
	{
		public static IReportDocument GetReport(int reportId, int phaseId, string option = "1")
		{
			// TODO:  Use reflection to instantiate the report instead of hardcoding
			IReportDocument report;
			switch (reportId)
			{
				case 1:
					report = new CodingStructureReport();
					break;
				case 2:
					report = new CommentReport();
					break;
				case 3:
				    report = new DemographicReportBook(phaseId);
				    break;
				case 4:
					report = new EarlyActionReport();
					break;
				case 5:
					report = new CommenterOrganizationReport();
					break;
				case 7:
				    report = new ResponseStatusReport();
				    break;
				case 8:
					report = new MailingListReport();
					break;
				case 9:
					report = new UntimelyCommentsReport();
					break;
				case 10:
					report = new TeamMemberReport();
					break;
                case 12:
                    report = new CommentsAndResponsesByLetterReport();

                    if (option != "-1")
                    {
                        int letterId = Convert.ToInt32(option);
                        using (var letterManager = ManagerFactory.CreateInstance<LetterManager>())
                        {
                            var sequence = letterManager.All.Where(x => x.LetterId == letterId).Select(x => x.LetterSequence).First().Value;
                            ((StandardPhaseReport) report).AddParameter(new KeyValuePair<string, object>("letternumber", sequence.ToString()));
                        }
                    }

					((StandardPhaseReport) report).Option = (string.IsNullOrEmpty(option) || option == "-1") ? null : option;
                    break;
                case 13:
                    report = new ConcernResponseByCommenterReport();
                    ((StandardPhaseReport)report).Option = string.IsNullOrEmpty(option) ? "1" : option;
                    break;
                case 15:
                    report = new CommentWithCRInfoReport(); //Comment With CR Information Report
                    break;
                case 16:
                    report = new CommentWithAnnotationReport(); //Comment With CR Information Report
                    break;
                case 17:
                    report = new ConcernResponseWithAssignedTo(); //CR Information with assigned to Report
                    break;
                default:
					report = new TeamMemberReport();
					break;
			}

			if (report is StandardPhaseReport)
			{
				((StandardPhaseReport) report).PhaseId = phaseId;
			}

			return report;
		}
	}
}
