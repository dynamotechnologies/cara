﻿using System.Text.RegularExpressions;
using System.Web;
using System.Configuration;
using Telerik.Reporting.Processing;
using Aquilent.Cara.Domain.Report;
using Aquilent.Framework.Resource;
using Telerik.Reporting;
using System.Threading;

namespace Aquilent.Cara.Reporting
{
	public static class ReportUtilities
	{
        public static int GetCommandTimeout()
        {
            var commandTimeoutSetting = ConfigurationManager.AppSettings["ReportsCommandTimeout"];

            int commandTimeout;
            if (!int.TryParse(commandTimeoutSetting, out commandTimeout))
            {
                commandTimeout = 300;
            }

            return commandTimeout;
        }

		public static string TrimCode(string codeNumber)
		{
			string level1Suffix = ".0000.00";
			string level2Suffix = "00.00";
			string level3Suffix = ".00";

			int level1Length = 3;
			int level2Length = 6;
			int level3Length = 8;

			string trimmedCodeNumber = codeNumber;

			if (codeNumber.EndsWith(level1Suffix))
			{
				trimmedCodeNumber = codeNumber.Substring(0, level1Length);
			}
			else if (codeNumber.EndsWith(level2Suffix))
			{
				trimmedCodeNumber = codeNumber.Substring(0, level2Length);
			}
			else if (codeNumber.EndsWith(level3Suffix))
			{
				trimmedCodeNumber = codeNumber.Substring(0, level3Length);
			}

			return trimmedCodeNumber;
		}

		public static string TrimCodeInString(string containsCodeNumber)
		{
			string pattern = @"\d{3}.\d{4}.\d{2}";

			var regex = new Regex(@"\b" + pattern + @"\b", RegexOptions.IgnoreCase);
			var match = regex.Match(containsCodeNumber);

			if (match.Success)
			{
				var matchString = match.Value;
				var trimmedCode = TrimCode(match.Value);

				containsCodeNumber = containsCodeNumber.Replace(match.Value, trimmedCode);
			}

			return containsCodeNumber;
		}

		public static string StripInvalidCharacters(string text)
		{
			return (text != null ? text.Replace("&#x0D;", "\r") : null);
		}

		public static string UnescapeHtmlTags(string text)
		{
			return HttpUtility.HtmlDecode(text);
		}
        
        #region Generate the report in specified format
        
        public static RenderingResult ExportReport(IReportDocument reportDocument, string exportType)
        {
           ReportProcessor reportProcessor = new ReportProcessor();
           RenderingResult result = reportProcessor.RenderReport(exportType, reportDocument, null);
          
            return result;
                  
        }
        #endregion Generate the report

      }
}
