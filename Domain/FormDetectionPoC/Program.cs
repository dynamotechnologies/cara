﻿using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.FormDetection;
using Aquilent.Cara.Domain;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.EntityAccess;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace FormDetectionPoC
{
	class Program
	{
		static void Main(string[] args)
		{
			var letterManager = ManagerFactory.CreateInstance<LetterManager>();
			var batchSize = 25;
			var outerBatchNumber = 0;
			var innerBatchNumber = 0;
			IQueryable<Letter> outerLetters;
			IQueryable<Letter> innerLetters;
			FormDetectionConfiguration formDetectConfig = ConfigUtilities.GetSection<FormDetectionConfiguration>("formDetectionConfiguration");

			var runTimestamp = DateTime.Now.ToString("yyyyMMdd.HHmmss");
			var outputFolder = ConfigurationManager.AppSettings["outputFolder"];

			SaveFormDetectionConfig(outputFolder, runTimestamp, formDetectConfig);

			var letterCombos = new Dictionary<int, int>();

			var letterStatsCapture = new FileInfo(string.Format(@"{0}\{1}_formDetectionStats.csv", outputFolder, runTimestamp));

			using (var statsStream = new StreamWriter(letterStatsCapture.OpenWrite()))
			{
				// Write the header
				statsStream.WriteLine("ID1,SizeBytes1,ID2,SizeBytes2,Total Frags,Inserted,Deleted,Modified,Unchanged,Imaginary");

				do
				{
					outerLetters = letterManager.All.OrderBy(x => x.LetterId).Skip(outerBatchNumber++ * batchSize).Take(batchSize);
					foreach (var outerLetter in outerLetters)
					{
						Console.WriteLine("Processing {0}", outerLetter.LetterId);
						innerBatchNumber = 0;
						do
						{
							innerLetters = letterManager.Find(x => x.LetterId != outerLetter.LetterId).OrderBy(x => x.LetterId).Skip(innerBatchNumber++ * batchSize).Take(batchSize);
							foreach (var innerLetter in innerLetters)
							{
								var stats = new FormLetterDetectorStats(outerLetter.Text, innerLetter.Text, formDetectConfig);
								if (stats != null && stats.LetterSizeConfig != null)
								{
									statsStream.WriteLine("{0},{8},{1},{9},{7},{2},{3},{4},{5},{6}",
										outerLetter.LetterId,
										innerLetter.LetterId,
										stats.Inserted,
										stats.Deleted,
										stats.Modified,
										stats.Unchanged,
										stats.Imaginary,
										stats.TotalFragments,
										outerLetter.Text.Length,
										innerLetter.Text.Length);
								}
							}
						}
						while (innerLetters.Count() == batchSize);
					}
				}
				while (outerLetters.Count() == batchSize);
			}

			Console.WriteLine("Press 'c' to continue: ");
			var key = Console.ReadKey();
			while (key.KeyChar != 'c')
			{
				key = Console.ReadKey();
			}
		}

		private static void SaveFormDetectionConfig(string outputFolder, string runTimestamp, FormDetectionConfiguration formDetectConfig)
		{
			var configCapture = new FileInfo(string.Format(@"{0}\{1}_formDetectionConfig.xml", outputFolder, runTimestamp));

			// Write the configuration so we know what we ran with
			using (var xmlStream = new StreamWriter(configCapture.OpenWrite()))
			{
				XmlSerializer ser = new XmlSerializer(typeof(FormDetectionConfiguration));
				ser.Serialize(xmlStream, formDetectConfig);
			}
		}
	}
}
