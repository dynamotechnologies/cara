﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain.UnitTests.Datamart
{
	public class FakeProjectRepository : IProjectRepository
	{
		private IList<Project> _projects = new List<Project>();

		public FakeProjectRepository(IUnitOfWork unitOfWork)
		{
			Project[] temp = new Project[]
			{
				new Project {
					ProjectId = 1,
					AnalysisTypeId = 1,
					CommentRuleId = 1,
					CreateDate = new DateTime(2009, 01, 02),
					Description = "A Description",
					LastUpdated = DateTime.UtcNow,
					Locations = new List<Location>(),
					Name = "A Name",
					Phases = new List<Phase>(),
					ProjectActivities = new List<ProjectActivity>(),
					ProjectDocuments = new List<ProjectDocument>(),
					ProjectNumber = "10001",
					ProjectPurposes = new List<ProjectPurpose>(),
					ProjectStatusId = 1,
					ProjectTypeId = 1,
					SOPAUser = "Jed Sutton",
					UnitId = "1000001"
				}
			};

			_projects = temp.ToList();

			UnitOfWork = unitOfWork;
		}


		#region IRepository members
		public AbstractEntityManager<Project> Manager { get; set; }
		public IUnitOfWork UnitOfWork {	get; set; }
		public System.Data.Objects.IObjectSet<Project> ObjectSet
		{
			get { throw new NotImplementedException(); }
		}

		public System.Data.Objects.ObjectQuery<Project> ObjectQuery
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<Project> All
		{
			get
			{
				return _projects.AsQueryable();
			}
		}

		public void Add(Project entity)
		{
			_projects.Add(entity);
		}

		public void Delete(Project entity)
		{
			_projects.Remove(entity);
		}
		#endregion IRepository members

		public IList<ProjectSearchResult> GetProjectSearchResults(ProjectSearchResultFilter filter, System.Data.Objects.ObjectParameter total)
		{
			throw new NotImplementedException();
		}

		public IDomainContext DomainContext
		{
			get { throw new NotImplementedException(); }
		}
	}
}
