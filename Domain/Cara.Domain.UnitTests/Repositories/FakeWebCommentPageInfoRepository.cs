﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.EntityAccess;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain.UnitTests.Datamart
{
	public class FakeWebCommentPageInfoRepository : IRepository<WebCommentPageInfo>
	{
		private IList<WebCommentPageInfo> _WebCommentPageInfos = new List<WebCommentPageInfo>();

		public FakeWebCommentPageInfoRepository(IUnitOfWork unitOfWork)
		{
			UnitOfWork = unitOfWork;
		}


		#region IRepository members
		public AbstractEntityManager<WebCommentPageInfo> Manager { get; set; }
		public IUnitOfWork UnitOfWork {	get; set; }
		public System.Data.Objects.IObjectSet<WebCommentPageInfo> ObjectSet
		{
			get { throw new NotImplementedException(); }
		}

		public System.Data.Objects.ObjectQuery<WebCommentPageInfo> ObjectQuery
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<WebCommentPageInfo> All
		{
			get
			{
				return _WebCommentPageInfos.AsQueryable();
			}
		}

		public void Add(WebCommentPageInfo entity)
		{
			_WebCommentPageInfos.Add(entity);
		}

		public void Delete(WebCommentPageInfo entity)
		{
			_WebCommentPageInfos.Remove(entity);
		}
		#endregion IRepository members
	}
}
