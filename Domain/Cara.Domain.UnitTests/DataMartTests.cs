﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Framework.Security;
using Aquilent.EntityAccess;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.IO;
using Ninject;
using Ninject.Modules;
using Aquilent.Framework.Security.Ninject;
using System.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using log4net;

namespace Aquilent.Cara.Domain.UnitTests.Datamart
{
	[TestClass]
	public class DataMartTests
	{
		#region Ninject Setup Class
		/// <summary>
		/// Dependency Injection Setup via Ninject
		/// </summary>
		private class FakeModule : NinjectModule
		{
			public override void Load()
			{
				bool useCredentials;
				try
				{
					useCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["DataMartUseCredentials"]);
				}
				catch
				{
					useCredentials = false;
				}

				string endpoint = ConfigurationManager.AppSettings["DataMartEndpoint"];
				var factory = new ChannelFactory<IDataMart>(endpoint);

				if (useCredentials)
				{
					factory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["DataMartBasicUsername"];
					factory.Credentials.UserName.Password = ConfigurationManager.AppSettings["DataMartBasicPassword"];
				}

				//WebHttpBinding binding = new WebHttpBinding();
				//binding.Security.Mode = WebHttpSecurityMode.TransportCredentialOnly;
				//binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
				//binding.MaxReceivedMessageSize = Int32.MaxValue;
				//binding.ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
				//binding.ReaderQuotas.MaxArrayLength = Int32.MaxValue;

				//WebChannelFactory<IDataMart> factory = new WebChannelFactory<IDataMart>(binding, new Uri("http://suttonj-vm1:8081"));
				//factory.Credentials.SupportInteractive = false;
				//factory.Credentials.UserName.UserName = "myUserName";
				//factory.Credentials.UserName.Password = "myPassword";

				Bind<IUnitOfWork>()
					.To<FakeUnitOfWork>();
				Bind<IProjectRepository>()
					.To<FakeProjectRepository>();
				Bind<IRepository<WebCommentPageInfo>>()
					.To<FakeWebCommentPageInfoRepository>();
				Bind<IDataMart>()
					.ToConstant(factory.CreateChannel());
			}
		}
		#endregion Ninject Setup Class

		#region Member Variables
		private DataMartManager _dmManager;
		private UserManager _userManager;
		private ProjectManager _projectManager;
		private WebCommentPageInfoManager _webCommentPageInfoManager;

		private string _username = "jsutton";
		private string _lastname = "sutton";
		private string _lastnameFrag = "sut";
		private string _firstnameFrag = "je";

		private int _palsProjectId = 10001;
		private string _projectnameFrag = "Lorem";

		private int _caraProjectId = 8001;
		private Phase _caraPhase = new Phase
			{
				PhaseId = 50001,
				Name = "Test Phase",
				PhaseTypeId = 1,
				CommentStart = new DateTime(2010, 12, 20),
				CommentEnd = new DateTime(2010, 1, 30)
			};

		private string _dmdDocumentId = "FSPLT1_022738";
		#endregion Member Variables

		#region Initialize and Cleanup
		//[DeploymentItem(@"Data\UnitTests.log4net")]
		[TestInitialize]
		public void Initialize()
		{
			//IKernel kernel = new StandardKernel(new FakeModule());
			//_dmManager = kernel.Get<DataMartManager>();
			//_projectManager = kernel.Get<ProjectManager>();
			//_webCommentPageInfoManager = kernel.Get<WebCommentPageInfoManager>();

			//IKernel securityKernel = new StandardKernel(new EFSecurityModule());
			//_userManager = securityKernel.Get<UserManager>();

			_dmManager = ManagerFactory.CreateInstance<DataMartManager>();
			_projectManager = ManagerFactory.CreateInstance<ProjectManager>();
			_webCommentPageInfoManager = ManagerFactory.CreateInstance<WebCommentPageInfoManager>();
			_userManager = ManagerFactory.CreateInstance<UserManager>();

			_dmManager.UserManager = _userManager;
			_dmManager.ProjectManager = _projectManager;
			_dmManager.WebInfoManager = _webCommentPageInfoManager;

			string serviceName = ConfigurationManager.AppSettings["DataMartEndpoint"];
			if (!serviceName.StartsWith("Fake"))
			{
				_username = "acsmith";
				_lastname = "Smith";
				_lastnameFrag = "Smit";
				_firstnameFrag = "Alic";

				_palsProjectId = 25906;
				_projectnameFrag = "Alsea";
			}

			string log4netFile = ConfigurationManager.AppSettings["log4net.config"];
			log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netFile));

		}

		[TestCleanup]
		public void CleanUp()
		{
			//((IDisposable) _datamart).Dispose();
		}
		#endregion Initialize and Cleanup

		#region User Tests
		[TestMethod]
		public void Get_A_User_By_Username()
		{
			// Arrange:  Define the username to look for
			var username = _username;

			// Act: Look for a user
			var user = _dmManager.GetUser(username);

			// Act: Verify that the user also exists in the usermanager
			var caraUser = _userManager.Get(username);

			// Assert: Make sure the correct user was returned
			Assert.AreEqual(username, user.Username);
			Assert.AreEqual(caraUser.Username, user.Username);
		}

		[TestMethod]
		public void Sync_A_User_With_Known_Changes()
		{
			// Arrange:  Define the username to look for
			var username = _username;
			var notNumber = "NOT_A_NUMBER";

			// Arrange: Look for a user to change
			var caraUser = _userManager.Get(username);
			caraUser.Phone = notNumber;
			_userManager.UnitOfWork.Save();

			// Act: Get the user from the Datamart
			var user = _dmManager.GetUser(username);

			// Act: Verify that the user also exists in the usermanager
			var caraUser2 = _userManager.Get(username);

			// Assert: Make sure the phone number was updated
			Assert.AreNotEqual(notNumber, user.Phone);
			Assert.AreEqual(caraUser2.Phone, user.Phone);
		}

		[TestMethod]
		public void Find_User_Using_LastName_Fragment()
		{
			// Arrange:  Define the username to look for
			var lastnameFrag = _lastnameFrag;

			// Act: Look for a user
			var userSearchResults = _dmManager.FindUsers(new DataMartUserFilter { LastName = lastnameFrag });
			int count = userSearchResults.ResultMetaData.RowCount;
			int rightLastNameCount = userSearchResults.Users.Select(x => x.LastName.StartsWith(lastnameFrag)).Count();

			// Assert: Make sure all of the returned items match
			Assert.AreNotEqual(0, count);

			// When testing the dev database vs. real datamart, this may not always be equal
			//Assert.AreEqual(count, rightLastNameCount);
		}

		[TestMethod]
		public void Find_User_Using_FirstName_Fragment()
		{
			// Arrange:  Define the username to look for
			var firstnameFrag = _firstnameFrag;
			var lastname = _lastname;

			// Act: Look for a user
			var userSearchResults = _dmManager.FindUsers(new DataMartUserFilter { FirstName = firstnameFrag, LastName = lastname });
			int count = userSearchResults.ResultMetaData.RowCount;
			int rightFirstNameCount = userSearchResults.Users.Select(x => x.FirstName.StartsWith(firstnameFrag)).Count();

			// Assert: Make sure all of the returned items match
			Assert.AreNotEqual(0, count);
			Assert.AreEqual(count, rightFirstNameCount);
		}
		#endregion User Tests

		#region Project Tests
		[TestMethod]
		public void Find_Project_Using_Search_Parameters()
		{
			// Arrange:  Define the username to look for
			var nameFrag = _projectnameFrag;
			var filter = new DataMartProjectFilter
			{
				PartialName = nameFrag
			};

			filter.StatusIds.Add(0);
			filter.StatusIds.Add(1);
			filter.StatusIds.Add(2);

			// Act: Look for a user
			var projectList = _dmManager.FindProjects(filter);
			int count = projectList.ResultMetaData.RowCount;
			int nameCount = projectList.Projects.Select(x => x.Name.StartsWith(nameFrag)).Count();

			// Assert: Make sure all of the returned items match
			Assert.AreNotEqual(0, count);
			Assert.AreEqual(count, nameCount);
		}

		[TestMethod]
		public void Get_Project_Using_Pals_ProjectId()
		{
			// Arrange:  Define the project ID to look for
			var palsProjectId = _palsProjectId;

			// Act: Look for the project
			var project = _dmManager.GetProject(palsProjectId);

			// Assert: Make sure all of the returned items match
			Assert.IsNotNull(project);
			Assert.AreEqual(palsProjectId.ToString(), project.ProjectNumber);
		}

		[TestMethod]
		public void Link_And_Unlink_Cara_Project_And_Datamart_Project()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;

			// Act: Link the projects
			var linkProjectSuccess = _dmManager.LinkProject(_caraProjectId, palsProjectId);

			// Act: Unlink the projects
			var unlinkProjectSuccess = _dmManager.UnlinkProject(_caraProjectId, palsProjectId);

			// Assert: Project Linked Successfully
			Assert.IsTrue(linkProjectSuccess);

			// Assert: Project Unlinked Successfully
			Assert.IsTrue(unlinkProjectSuccess);
		}

		//[TestMethod]
		public void Sync_A_Project_With_Known_Changes()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;
			var notName = "NOT_A_NAME";

			// Arrange:  Get the corresponding CARA Project
			var caraProject = _projectManager.Find(x => x.ProjectNumber == palsProjectId.ToString()).First();
			caraProject.SOPAUser = notName;
			_projectManager.UnitOfWork.Save();

			// Arrange:  Get the corresponding PALS project
			var palsProject = _dmManager.GetProject(palsProjectId);

			// Act: Sync the projects from the Datamart
			_dmManager.SyncProject(caraProject, palsProject, true, _projectManager);

			// Act: Verify that the user also exists in the usermanager
			var caraProject2 = _projectManager.Find(x => x.ProjectNumber == palsProjectId.ToString()).First();

			// Assert: Make sure the phone number was updated
			Assert.AreNotEqual(notName, caraProject2.SOPAUser);
			Assert.AreEqual(caraProject2.SOPAUser, caraProject.SOPAUser);
		}

		[TestMethod]
		public void Get_Project_Documents()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;

			// Act: Get the project documents
			var documents = _dmManager.GetProjectDocuments(palsProjectId);

			// Assert: Ensure documents were returned
			Assert.AreNotEqual(0, documents.Count);
		}

		#endregion Project Tests

		#region Phase Tests
		[TestMethod]
		public void Test_Phase_Operations()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;

			// Arrange:  Get the corresponding CARA Project
			var phase = _caraPhase;

			// Arrange: Link the project
			var linkProjectSuccess = _dmManager.LinkProject(_caraProjectId, palsProjectId);

			// Act: Link the phase
			var linkPhaseSuccess = _dmManager.LinkPhase(_caraProjectId, phase);

			// Act: Link phase documents
			var linkPhaseDocumentsSuccess = _dmManager.LinkPhaseDocuments(_caraProjectId, _caraPhase.PhaseId, _dmdDocumentId);

			// Act: Send updated phase information
			var sendPhaseLetterInfoSuccess = _dmManager.SendPhaseLetterInfo(_caraProjectId,
					new DataMartLetterInfo
					{
						DuplicateLetterCount = 5,
						FinishDate = DateTime.UtcNow.ToString("yyyy-MM-dd"),
						FormLetterCount = 1,
						FormPlusLetterCount = 2,
						MasterFormLetterCount = 1,
						Name = "Formal - Test",
						PhaseId = phase.PhaseId,
						StartDate = new DateTime(2010, 12, 24).ToString("yyyy-MM-dd"),
						TotalLetterCount = 10,
						UniqueLetterCount = 7
					}
				);

			// Act: Link phase documents
			var unlinkPhaseDocumentsSuccess = _dmManager.UnlinkPhaseDocuments(_caraProjectId, _caraPhase.PhaseId, _dmdDocumentId);

			// Act: Unlink the phase
			var unlinkPhaseSuccess = _dmManager.UnlinkPhase(_caraProjectId, _caraPhase.PhaseId, null);

			// Unarrange: Unlink the projects
			var unlinkProjectSuccess = _dmManager.UnlinkProject(_caraProjectId, palsProjectId);

			// Assert: Project Linked Successfully
			Assert.IsTrue(linkProjectSuccess);

			// Assert: Phase Linked Successfully
			Assert.IsTrue(linkPhaseSuccess);

			// Assert: Phase documents linked succesfully 
			Assert.IsTrue(linkPhaseDocumentsSuccess);

			// Assert: Phase updated succesfully 
			Assert.IsTrue(sendPhaseLetterInfoSuccess);

			// Assert: Phase documents linked succesfully 
			Assert.IsTrue(unlinkPhaseDocumentsSuccess);

			// Assert: Phase Unlinked Successfully
			Assert.IsTrue(unlinkPhaseSuccess);

			// Assert: Project Unlinked Successfully
			Assert.IsTrue(unlinkProjectSuccess);
		}
		#endregion Phase Tests

		#region Subscriber Tests
		[TestMethod]
		public void Test_Subscriber_Operations()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;

			// Arrange: Define an author
			Author author = new Author
			{
				AuthorId = 1,
				Address1 = "1100 West Street",
				City = "Laurel",
				ContactMethod = "Email",
				Email = "jed.sutton@aquilent.com",
				FirstName = "Jed",
				LastName = "Sutton",
				OfficialRepresentativeTypeId = 1,
				OrganizationId = 1,
				Phone = "301-939-1100",
				StateId = "MD",
				ZipPostal = "20707"
			};

			// Arrange: Link the project
			var linkProjectSuccess = _dmManager.LinkProject(_caraProjectId, palsProjectId);

			// Act: Add the author
			var addSubscriberSuccess = _dmManager.AddSubscriber(_caraProjectId, author);

			// Arrange: Make changes to the author
			author.FirstName = "George";
			author.LastName = "Washington";
			author.Email = "george.washington@aquilent.com";

			// Act: Update the author
			var updateSubscriberSuccess = _dmManager.UpdateSubscriber(_caraProjectId, author);

			// Act: Delete the author
			var deleteSubscriberSuccess = _dmManager.DeleteSubscriber(_caraProjectId, author.AuthorId);

			// Unarrange: Unlink the projects
			var unlinkProjectSuccess = _dmManager.UnlinkProject(_caraProjectId, palsProjectId);

			// Assert: Project Linked Successfully
			Assert.IsTrue(linkProjectSuccess);

			// Assert: Author added Successfully
			Assert.IsTrue(addSubscriberSuccess);

			// Assert: Author updated Successfully
			Assert.IsTrue(updateSubscriberSuccess);

			// Assert: Author deleted Successfully
			Assert.IsTrue(deleteSubscriberSuccess);

			// Assert: Project Unlinked Successfully
			Assert.IsTrue(unlinkProjectSuccess);
		}
		#endregion Subscriber Tests

		#region Public Comment Page Tests
		[TestMethod]
		public void Get_Public_Comment_Page()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;

			// Act:  Get Public comment page
			var publicCommentPage = _dmManager.GetProjectCommentPage(palsProjectId.ToString());

			// Assert: Make sure something gets returned
			Assert.IsNotNull(publicCommentPage);
		}

		[TestMethod]
		public void Sync_Public_Comment_Page()
		{
			// Arrange:  Define the PALS project ID to look for
			var palsProjectId = _palsProjectId;

			// Arrange: Get the info from CARA and from the DataMart
			var caraWcpi = _webCommentPageInfoManager.GetByPalsId(palsProjectId);
			var dmWcpi = _dmManager.GetProjectCommentPage(palsProjectId.ToString());

			// Arrange:  Change the info in the dmWcpi
			dmWcpi.CommentEmail = "joe@aol.com";

			// Act:  Get Public comment page
			var publicCommentPage = _dmManager.SyncWebCommentPageInfo(caraWcpi, dmWcpi, true, _webCommentPageInfoManager);

			// Assert: Make sure something gets returned
			Assert.AreEqual(caraWcpi.CommentEmail, dmWcpi.CommentEmail);
		}

		[TestMethod]
		public void Sync_Public_Comment_Pages()
		{
			_dmManager.SyncWebCommentPageInfos();
		}

		#endregion Public Comment Page Tests
	}
}
