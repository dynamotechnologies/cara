﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aquilent.Cara.Domain.Dmd;
using System.IO;
using System;
using System.Text;

namespace Aquilent.Cara.Domain.UnitTests
{
	[TestClass]
	public class DmdTests
	{
		private DmdManager DmdManager { get; set; }

		[TestInitialize]
		public void Initialize()
		{
			DmdManager = new DmdManager();
		}

		[TestMethod]
		[DeploymentItem(@"Data\dmdtest.txt")]
		public void Add_File_To_Dmd()
		{
			// Arrange:  Prepare the file
			var testfile = new FileInfo("dmdtest.txt");

			// Act:  Send the file to the DMD
			var dmdId = DmdManager.PutFile(testfile, "Test File", "Jed Sutton");

			// Assert:  Check the DMD ID
			Assert.AreNotEqual(string.Empty, dmdId);
		}

		[TestMethod]
		[DeploymentItem(@"Data\dmdtest.txt")]
		public void Retrieve_File_Contents()
		{
			// Arrange:  Get the file to compare with
			string dmdId = "FSTST2_033640";
			var testfile = new FileInfo("dmdtest.txt");
			string testFileContents;
			using (var stream = testfile.OpenText())
			{
				testFileContents = stream.ReadToEnd();
			}

			// Act:  Get the file contents from the DMD
			var base64FileContents = DmdManager.GetFileData(dmdId);
			string dmdFileContents;
			using (var mem = new MemoryStream(base64FileContents))
			{
				using (var reader = new StreamReader(mem, Encoding.ASCII, true))
				{
					dmdFileContents = reader.ReadToEnd();
				}
			}

			// Assert:  Check the DMD ID
			Assert.AreEqual(testFileContents, dmdFileContents);
		}

		[TestMethod]
		public void Retrieve_File_Metadata()
		{
			// Arrange:  Select DMD ID
			string dmdId = "FSTST2_033640";

			// Act:  Get the file meta data from the DMD
			var fileMetadata = DmdManager.GetFileMetadata(dmdId);

			// Assert:  Check the DMD ID
			Assert.AreEqual("dmdTest.txt", fileMetadata.Filename);
		}

		[TestMethod]
		[DeploymentItem(@"Data\dmdtest.txt")]
		public void DeleteFile()
		{
			// Arrange:  Prepare the file
			var testfile = new FileInfo("dmdtest.txt");

			// Arrange:  Send the file to the DMD
			var dmdId = DmdManager.PutFile(testfile, "Test File", "Jed Sutton");

			// Act:  Delete the file
			var success = DmdManager.DeleteFile(dmdId);

			// Assert:  Check the DMD ID
			Assert.AreEqual("1", success);
		}

	}
}
