﻿using System.IO;
using System.Linq;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using GemBox.Spreadsheet;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Independentsoft.Pst;

namespace Aquilent.Cara.Domain.UnitTests
{
	[TestClass]
	public class IngressTests
	{
		private ZipFile _fdmsZipFile;
		private ZipFile _lotusZipFile;
		private PstFile _pstFile;
		private DocumentManager _documentManager;
		private OrganizationManager _organizationManager;

		[TestInitialize]
		public void Init()
		{
			SpreadsheetInfo.SetLicense("ETFV-FT6C-6R7H-ACFA");

			//var fdmsFile = new FileInfo("fdmstest.zip");
			//_fdmsZipFile = new ZipFile(fdmsFile.OpenRead());

			//var lotusFile = new FileInfo("lotustest.zip");
			//_lotusZipFile = new ZipFile(lotusFile.OpenRead());

			//var pstFile = new FileInfo("outlooktest.zip");
			//_pstFile = new PstFile(pstFile.OpenRead());

            //_documentManager = ManagerFactory.CreateInstance<DocumentManager>();
            //_organizationManager = ManagerFactory.CreateInstance<OrganizationManager>();
		}

		[TestMethod]
		public void Check_If_Fdms_File_Is_Valid()
		{
			// Arrange: set up the zip file 
			FdmsLetterExtractor extractor = new FdmsLetterExtractor("Username", _fdmsZipFile, _documentManager, _organizationManager);

			// Act:  Check if it is valid
			var isValid = extractor.IsValid;

			// Assert:  Ensure it is valid
			Assert.IsTrue(isValid);
		}

		[TestMethod]
		public void Check_If_Lotus_File_Is_Valid()
		{
			// Arrange: set up the zip file 
			LotusNotesLetterExtractor extractor = new LotusNotesLetterExtractor("Username", _lotusZipFile, _documentManager);

			// Act:  Check if it is valid
			var isValid = extractor.IsValid;

			// Assert:  Ensure it is valid
			Assert.IsTrue(isValid);
		}

		[TestMethod]
		public void Extract_Letters_And_Attachments_From_Fdms_Export_File()
		{
			// Arrange: set up the zip file 
			FdmsLetterExtractor extractor = new FdmsLetterExtractor("Username", _fdmsZipFile, _documentManager, _organizationManager);

			// Act:  Extract the letters
			var letters = extractor.Extract();

			// Assert:  The number of letters
			Assert.AreEqual(4, letters.Count);

			// Assert:  The number of documents (letters + attachments)
			Assert.AreEqual(7, letters.Sum(x => x.Documents.Count));
		}

		[TestMethod]
		public void Extract_Letters_And_Attachments_From_Lotus_Export_File()
		{
			// Arrange: set up the zip file 
			LotusNotesLetterExtractor extractor = new LotusNotesLetterExtractor("Username", _lotusZipFile, _documentManager);

			// Act:  Extract the letters
			var letters = extractor.Extract();

			// Assert:  The number of letters
			Assert.AreEqual(11, letters.Count);

			// Assert:  The number of documents (letters + attachments)
			Assert.AreEqual(13, letters.Sum(x => x.Documents.Count));
		}

		[TestMethod]
		[DeploymentItem(@"Data\outlooktest.pst")]
		public void Extract_Letters_And_Attachments_From_Outlook_Pst_File()
		{
			// Arrange: set up the pst file
			var pstFile = new FileInfo("outlooktest.pst");
			_pstFile = new PstFile(pstFile.OpenRead());
			OutlookPstLetterExtractor extractor = new OutlookPstLetterExtractor("Username", _pstFile, _documentManager);

			// Act:  Extract the letters
			var letters = extractor.Extract();

			// Assert:  The number of letters
			Assert.AreEqual(4, letters.Count);

			// Assert:  The number of documents (letters + attachments)
			Assert.AreEqual(9, letters.Sum(x => x.Documents.Count));
		}

        [TestMethod]
        public void Two_Contexts()
        {
            using (var phaseManager = ManagerFactory.CreateInstance<PhaseManager>())
            {
                var phase = phaseManager.Get(13);
                using (var projectManager = ManagerFactory.CreateInstance<ProjectManager>())
                {
                    var project = projectManager.Get(phase.ProjectId);
                }

                phase.Name = "Test Name";
                phaseManager.UnitOfWork.Save();
            }
        }
	}
}
