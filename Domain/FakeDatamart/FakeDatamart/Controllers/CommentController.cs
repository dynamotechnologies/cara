﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakeDatamart.Controllers
{
    public class CommentController : Controller
    {
		[HttpPost]
		public ContentResult Phases(int caraProjectId)
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpPut]
		[ActionName("Phases")]
		public ContentResult Phases_Put(int caraProjectId, int caraPhaseId)
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpDelete]
		[ActionName("Phases")]
		public ContentResult Phases_Delete(int caraProjectId, int caraPhaseId)
		{
			return Content(
				"<ok />",
				"application/xml");
		}
	}
}
