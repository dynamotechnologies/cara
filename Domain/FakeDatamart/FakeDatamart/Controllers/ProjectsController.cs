﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.XPath;

namespace FakeDatamart.Controllers
{
    public class ProjectsController : Controller
    {
        //
        // GET: /Projects/

        public ContentResult Index(int palsProjectId)
        {
			XElement projects = Utilities.LoadProjects(Request);
			XNamespace xn = projects.GetDefaultNamespace();

			var matchingProjects = from p in projects.Elements(xn + "project")
								   select p;

			matchingProjects = from p in matchingProjects
							   where p.Element(xn + "id").Value == palsProjectId.ToString()
							   select p;

			XElement returnElement = matchingProjects.FirstOrDefault();

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		public ContentResult Activities(int palsProjectId)
		{
			XElement returnElement = GetProjectRelatedData(palsProjectId, "projectactivities");

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		public ContentResult Milestones(int palsProjectId)
		{
			XElement returnElement = GetProjectRelatedData(palsProjectId, "projectmilestones");

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		public ContentResult Locations(int palsProjectId)
		{
			XElement returnElement = GetProjectRelatedData(palsProjectId, "locations");

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		public ContentResult Docs(int palsProjectId)
		{
			XElement returnElement = GetProjectRelatedData(palsProjectId, "projectdocuments");

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		private XElement GetProjectRelatedData(int palsProjectId, string elementName)
		{
			XElement projects = Utilities.LoadProjectRelatedData(Request);
			XNamespace xn = projects.GetDefaultNamespace();

			var matchingProjects = from p in projects.Elements(xn + "project")
								   where p.Attribute("id").Value == palsProjectId.ToString()
								   select p;

			return matchingProjects.Elements(xn + elementName).FirstOrDefault();
		}
	}
}
