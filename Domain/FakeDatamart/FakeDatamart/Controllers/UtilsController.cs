﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace FakeDatamart.Controllers
{
    public class UtilsController : Controller
    {
		public ContentResult FindUser()
		{
			XElement users = Utilities.LoadUsers(Request);
			XNamespace xn = users.GetDefaultNamespace();

			//var shortname = Request.Params["shortname"];
			var lastname = Request.Params["lastname"];
			var firstname = Request.Params["firstname"];
			int rows = 0;
            int.TryParse(Request.Params["rows"], out rows);
			
            int start = 0;
            int.TryParse(Request.Params["start"], out start);
            int count;
            IEnumerable<XElement> usersToReturn;

			var matchingUsers = from p in users.Descendants(xn + "user")
								select p;

            //if (!string.IsNullOrEmpty(shortname) && matchingUsers.Count() > 0)
            //{
            //    matchingUsers = from p in matchingUsers
            //                    where p.Element(xn + "shortname").Value.ToLower().StartsWith(shortname.ToLower())
            //                    select p;
            //}

			if (!string.IsNullOrEmpty(lastname) && matchingUsers.Count() > 0)
			{
				matchingUsers = from p in matchingUsers
								where p.Element(xn + "lastname").Value.ToLower().StartsWith(lastname.ToLower())
								select p;
			}

			if (!string.IsNullOrEmpty(firstname) && matchingUsers.Count() > 0)
			{
				matchingUsers = from p in matchingUsers
								where p.Element(xn + "firstname").Value.ToLower().StartsWith(firstname.ToLower())
								select p;
			}

			XElement returnElement;


            usersToReturn = matchingUsers.Skip(start);

            if (rows > 0)
            {
                usersToReturn = usersToReturn.Take(rows);
            }

            //if (!string.IsNullOrEmpty(shortname))
            //{
            //    returnElement = matchingUsers.FirstOrDefault();
            //}
			//else
			{
				returnElement = new XElement(xn + "users", usersToReturn);
			}

            count = matchingUsers.Count();

			returnElement = new XElement(xn + "usersearchresults",
								new XElement(xn + "resultmetadata",
									new XElement(xn + "request", Request.Url),
									new XElement(xn + "rowcount", usersToReturn.Count()),
									new XElement(xn + "totalcount", count)),
								returnElement);

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		public ContentResult FindNepaProject()
		{
			XElement projects = Utilities.LoadProjects(Request);
			XNamespace xn = projects.GetDefaultNamespace();

			string id = Request.Params["id"];
			string name = Request.Params["name"];
			string statusIds = Request.Params["status"];
			string unitcode = Request.Params["unit"];
            string rows = Request.Params["rows"];
            string startIndex = Request.Params["start"];

			var matchingProjects = from p in projects.Descendants(xn + "project")
								   select p;

			if (!string.IsNullOrEmpty(id))
			{
				matchingProjects = from p in matchingProjects
								   where p.Element(xn + "id").Value == id
								   select p;
			}

			if (!string.IsNullOrEmpty(name))
			{
				matchingProjects = from p in matchingProjects
								   where p.Element(xn + "name").Value.ToLower().Contains(name.ToLower())
								   select p;
			}

			if (!string.IsNullOrEmpty(unitcode) && matchingProjects.Count() > 0)
			{
                var unitcodes = unitcode.Split(new string[] { ", ", "," }, StringSplitOptions.RemoveEmptyEntries);
                var matches = new List<IEnumerable<XElement>>();
                string unit;
                foreach (var uc in unitcodes)
                {
                    if (uc.EndsWith("000000"))
                    {
                        unit = uc.Substring(0, uc.Length - 6);

                        matches.Add(from p in matchingProjects
								   where p.Element(xn + "unitcode").Value.StartsWith(unit)
								   select p);
                    }
                    else if (uc.EndsWith("0000"))
                    {
                        unit = uc.Substring(0, uc.Length - 4);

                        matches.Add(from p in matchingProjects
								   where p.Element(xn + "unitcode").Value.StartsWith(unit)
								   select p);
                    }
                    else if (uc.EndsWith("00"))
                    {
                        unit = uc.Substring(0, uc.Length - 2);

                        matches.Add(from p in matchingProjects
								   where p.Element(xn + "unitcode").Value.StartsWith(unit)
								   select p);
                    }
                    else
                    {
                        matches.Add(from p in matchingProjects
								   where p.Element(xn + "unitcode").Value.StartsWith(uc)
								   select p);
                    }
                }

				matchingProjects = matches.SelectMany(x => x).Distinct();
			}

            int totalCount = 0;
			if (!string.IsNullOrEmpty(statusIds) && matchingProjects.Count() > 0)
			{
				var statuses = statusIds.Split(new string[] { ", ", "," }, StringSplitOptions.RemoveEmptyEntries);

				matchingProjects = from p in matchingProjects
								   where statuses.Contains(p.Element(xn + "nepainfo").Element(xn + "statusid").Value)
                                   select p;

                totalCount = matchingProjects.Count();
                matchingProjects = matchingProjects.OrderBy(x => x.Name).Skip(int.Parse(startIndex)).Take(int.Parse(rows)).Select(p => p);
			}

			XElement returnElement = new XElement(xn + "projects", matchingProjects);
			returnElement = new XElement(xn + "projectsearchresults",
								new XElement(xn + "resultmetadata",
									new XElement(xn + "request", Request.Url),
									new XElement(xn + "rowcount", matchingProjects.Count()),
									new XElement(xn + "totalcount", totalCount)),
								returnElement);

			return Content(
				returnElement.ToString(),
				"application/xml");
		}

		[HttpPost]
		public ContentResult LinkProject()
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpPost]
		public ContentResult UnlinkProject()
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpPost]
		public ContentResult LinkDoc()
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpPost]
		public ContentResult UnlinkDoc()
		{
			return Content(
				"<ok />",
				"application/xml");
		}

        [HttpPost]
        public ContentResult projectbypalsid()
        {
			return Content(
				"<ok />",
				"application/xml");
        }
	}
}
