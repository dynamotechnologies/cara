﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace FakeDatamart.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/

		public ContentResult Index(string shortname)
        {
			XElement users = Utilities.LoadUsers(Request);
			XNamespace xn = users.GetDefaultNamespace();
	
			var matchingUsers = from p in users.Elements(xn + "user")
								select p;

			matchingUsers = from p in matchingUsers
							where p.Element(xn + "shortname").Value == shortname
							select p;

			XElement returnElement = matchingUsers.FirstOrDefault();

			return Content(
				returnElement.ToString(),
				"application/xml");
		}
    }
}
