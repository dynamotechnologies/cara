﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace FakeDatamart.Controllers
{
    public class RefController : Controller
    {
        public ContentResult Units(string unitcode)
        {
			XElement units = Utilities.LoadUnits(Request);
			XNamespace xn = units.GetDefaultNamespace();

			var matchingUnits = from p in units.Elements(xn + "unit")
								select p;

			matchingUnits = from p in matchingUnits
							where p.Element(xn + "code").Value == unitcode
							select p;

			XElement returnElement = new XElement(xn + "unit",
				new XElement(xn + "code", matchingUnits.Elements(xn + "code").First().Value),
				new XElement(xn + "name", matchingUnits.Elements(xn + "name").First().Value),
				new XElement(xn + "extendeddetails", false),
				new XElement(xn + "projectmap", false),
				new XElement(xn + "address1", "456 Main Street"),
				new XElement(xn + "address2", "P.O. Box 123"),
				new XElement(xn + "city", "Sumwearin"),
				new XElement(xn + "state", "MD"),
				new XElement(xn + "zip", "78901"),
				new XElement(xn + "phonenumber", "123-456-8790"),
				new XElement(xn + "wwwlink", "http://www.aquilent.com"),
				new XElement(xn + "commentemail", "bob@aol.com"),
				new XElement(xn + "newspaper", "The Washington Post"),
				new XElement(xn + "newspaperurl", "http://www.washingtonpost.com"),
				new XElement(xn + "unitboundary", "a string")
			);

			return Content(
				returnElement.ToString(),
				"application/xml");
		}
    }
}
