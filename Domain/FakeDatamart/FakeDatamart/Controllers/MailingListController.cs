﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakeDatamart.Controllers
{
    public class MailingListController : Controller
    {
		[HttpPost]
		public ContentResult Subscribers(int caraProjectId)
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpPut]
		[ActionName("Subscribers")]
		public ContentResult Subscribers_Put(int caraProjectId)
		{
			return Content(
				"<ok />",
				"application/xml");
		}

		[HttpDelete]
		[ActionName("Subscribers")]
		public ContentResult Subscribers_Delete(int caraProjectId)
		{
			return Content(
				"<ok />",
				"application/xml");
		}
	}
}
