﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FakeDatamart
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				null, // Route name
				"caraprojects/{caraProjectId}/{controller}/{action}/{phaseId}", // URL with parameters
				new { controller = "Comment", action = "Phases", phaseId = UrlParameter.Optional } // Parameter defaults
			);

			routes.MapRoute(
				null, // Route name
				"{controller}/findUser", // URL with parameters
				new { controller = "Utils", action = "FindUser" } // Parameter defaults
			);

			routes.MapRoute(
				null, // Route name
				"{controller}/cara/{action}", // URL with parameters
				new { controller = "Utils", action = "FindNepaProject" } // Parameter defaults
			);

			routes.MapRoute(
				null, // Route name
				"{controller}/nepa/{palsProjectId}/{action}", // URL with parameters
				new { controller = "Projects", action = "Index" } // Parameter defaults
			);

			routes.MapRoute(
				null, // Route name
				"ref/{action}/{unitcode}", // URL with parameters
				new { controller = "Ref", action = "Units" } // Parameter defaults
			);

			routes.MapRoute(
				null, // Route name
				"{controller}/{shortname}", // URL with parameters
				new { controller = "Users", action = "Index" } // Parameter defaults
			);

			routes.MapRoute(
				"Real Default", // Route name
				"{controller}/{*pathInfo}", // URL with parameters
				new { controller = "Home", action = "Default", id = UrlParameter.Optional } // Parameter defaults
			);


		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterRoutes(RouteTable.Routes);
		}
	}
}