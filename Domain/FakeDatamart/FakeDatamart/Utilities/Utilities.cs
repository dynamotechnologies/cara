﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Net;


namespace FakeDatamart
{
	public static class Utilities
	{
		public static XElement LoadUnits(HttpRequestBase request)
		{
			var fi = new System.IO.FileInfo(request.PhysicalApplicationPath + @"\Content\dmunits.xml");
			XElement elements;
			using (var s = fi.OpenText())
			{
				elements = XElement.Load(s);
			}

			return elements;
		}

		public static XElement LoadProjects(HttpRequestBase request)
		{
			var fi = new System.IO.FileInfo(request.PhysicalApplicationPath + @"\Content\dmprojects.xml");
			XElement projects;
			using (var s = fi.OpenText())
			{
				projects = XElement.Load(s);
			}

			return projects;
		}

		public static XElement LoadProjectRelatedData(HttpRequestBase request)
		{
			var fi = new System.IO.FileInfo(request.PhysicalApplicationPath + @"\Content\dmprojectdata.xml");
			XElement projects;
			using (var s = fi.OpenText())
			{
				projects = XElement.Load(s);
			}

			return projects;
		}

		public static XElement LoadUsers(HttpRequestBase request)
		{
			var fi = new System.IO.FileInfo(request.PhysicalApplicationPath + @"\Content\dmusers.xml");
			XElement users;
			using (var s = fi.OpenText())
			{
				users = XElement.Load(s);
			}
			return users;
		}
	}
}