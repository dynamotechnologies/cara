﻿using log4net.Config;
using System.IO;
using System.Configuration;
using Aquilent.EntityAccess;
using Aquilent.Cara.Domain.DataMart;
using Aquilent.Cara.Domain;

namespace DataMartCleanup
{
	class Program
	{
		static void Main(string[] args)
		{
			string log4netFile = ConfigurationManager.AppSettings["log4NetFile"];
			XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netFile));

			DataMartManager dmManager = new DataMartManager();
			PhaseManager phaseManager = ManagerFactory.CreateInstance<PhaseManager>();
			//AuthorManager authorManager = ManagerFactory.CreateInstance<AuthorManager>();
			Phase phase;
			//Author author;

            phase = phaseManager.Get(142);
            dmManager.LinkPhase(131, phase);

            phase = phaseManager.Get(218);
            dmManager.LinkPhase(189, phase);

            phase = phaseManager.Get(243);
            dmManager.LinkPhase(212, phase);

            phase = phaseManager.Get(259);
            dmManager.LinkPhase(211, phase);

            phase = phaseManager.Get(296);
            dmManager.LinkPhase(262, phase);

            phase = phaseManager.Get(275);
            dmManager.LinkPhase(242, phase);

            phase = phaseManager.Get(240);
            dmManager.LinkPhase(209, phase);

            phase = phaseManager.Get(217);
            dmManager.LinkPhase(188, phase);

            phase = phaseManager.Get(221);
            dmManager.LinkPhase(193, phase);

            phase = phaseManager.Get(235);
            dmManager.LinkPhase(205, phase);

            phase = phaseManager.Get(290);
            dmManager.LinkPhase(256, phase);

            phase = phaseManager.Get(291);
            dmManager.LinkPhase(257, phase);

            phase = phaseManager.Get(284);
            dmManager.LinkPhase(17, phase);

            phase = phaseManager.Get(242);
            dmManager.LinkPhase(211, phase);

            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365939");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365940");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365941");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365942");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365943");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365944");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365945");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365946");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365947");
            dmManager.LinkPhaseDocuments(189, 218, "FSPLT2_365948");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_032000");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_032001");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047919");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047920");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047921");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047922");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047923");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047924");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047925");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047926");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047927");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047928");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047929");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047930");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047931");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047932");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047934");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047935");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047936");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047937");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047938");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047939");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047940");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047941");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047942");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047944");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047945");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047946");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047947");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047948");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047949");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047950");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047951");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047952");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047953");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047954");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047955");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_047956");
            dmManager.LinkPhaseDocuments(212, 243, "FSPLT2_048179");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294073");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294074");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294076");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294077");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294078");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294079");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294080");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294081");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294082");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294083");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294084");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294085");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_294086");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_306676");
            dmManager.LinkPhaseDocuments(193, 221, "FSPLT2_369743");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_052107");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_052108");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_052109");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_052110");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_052111");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289027");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289406");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289407");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289408");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289409");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289410");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289411");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289412");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289413");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289414");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289415");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289416");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289417");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289418");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289419");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289420");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_289489");
            dmManager.LinkPhaseDocuments(205, 235, "FSPLT2_291583");
            dmManager.LinkPhaseDocuments(188, 217, "FSPLT2_057828");
            dmManager.LinkPhaseDocuments(188, 217, "FSPLT2_277682");
            dmManager.LinkPhaseDocuments(188, 217, "FSPLT2_291407");
            dmManager.LinkPhaseDocuments(188, 217, "FSPLT2_291408");
            dmManager.LinkPhaseDocuments(188, 217, "FSPLT2_291409"); 
            
            phaseManager.Dispose();
            dmManager.Dispose();

		}
	}
}
