﻿using Aquilent.Cara.Domain.DataModel;
using Aquilent.EntityAccess;
using Ninject.Modules;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.FormDetection;
using Aquilent.Cara.Domain.Report.Personal.DataModel;

namespace Aquilent.Cara.Domain.Report.Personal.Ninject
{
    public class CaraReportPersonalModule : NinjectModule
	{
		public override void Load()
		{
            Bind<CaraReportPersonalEntities>().ToSelf();

			Bind<IUnitOfWork>()
                .To<ReportPersonalEntitiesUnitOfWork>();

            #region Repository Bindings
            Bind<IRepository<MyReport>>()
                .To<EFRepository<MyReport>>();
            Bind<IRepository<CustomReportQueue>>()
                .To<EFRepository<CustomReportQueue>>();
            #endregion
		}
	}
}
