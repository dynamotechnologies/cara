﻿using Aquilent.Cara.Domain.DataModel;
using Aquilent.EntityAccess;
using Ninject.Modules;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.FormDetection;
using Aquilent.Cara.Domain.Report.DataModel;

namespace Aquilent.Cara.Domain.Report.Ninject
{
    public class CaraReportModule : NinjectModule
	{
		public override void Load()
		{
            Bind<CaraReportEntities>().ToSelf();

			Bind<IUnitOfWork>()
                .To<ReportEntitiesUnitOfWork>();

            #region Repository Bindings
            Bind<IReportRepository>()
                .To<EFReportRepository>();
            #endregion
		}
	}
}
