﻿using Aquilent.Cara.Domain.DataModel;
using Aquilent.EntityAccess;
using Ninject.Modules;
using Aquilent.Cara.Domain.Ingress;
using Aquilent.Cara.Configuration;
using Aquilent.Cara.Configuration.FormDetection;
using System;

namespace Aquilent.Cara.Domain.Ninject
{
    public class CaraDomainModule : NinjectModule
	{
		private static FormDetectionConfiguration FormDetectionConfiguration { get; set; }
		public override void Load()
		{
			try
			{
				if (FormDetectionConfiguration == null)
				{
					FormDetectionConfiguration = ConfigUtilities.GetSection<FormDetectionConfiguration>("formDetectionConfiguration");
				}
			}
			catch (Exception ex)
			{
                log4net.ILog logger = log4net.LogManager.GetLogger(typeof(FormDetectionConfiguration));
                logger.Fatal("An error occurred loading the FormDetectionConfiguration.", ex);
			}

            Bind<CaraDomainEntities>().ToSelf();

			Bind<IUnitOfWork>()
                .To<DomainEntitiesUnitOfWork>();

            #region Repository Bindings
            Bind<IRepository<Author>>()
                .To<EFRepository<Author>>();

            Bind<IRepository<Project>>()
                .To<EFRepository<Project>>();

            Bind<IRepository<ProjectSearchResult>>()
                .To<EFRepository<ProjectSearchResult>>();

            Bind<IRepository<PhaseCodeSelect>>()
                .To<EFRepository<PhaseCodeSelect>>();

			Bind<IRepository<Phase>>()
                .To<EFRepository<Phase>>();

            Bind<IRepository<PhaseCode>>()
                .To<EFRepository<PhaseCode>>();

            Bind<IRepository<PhaseSearchResult>>()
                .To<EFRepository<PhaseSearchResult>>();

            Bind<IRepository<Letter>>()
                .To<EFRepository<Letter>>();

            Bind<IRepository<LetterSearchResult>>()
                .To<EFRepository<LetterSearchResult>>();

            Bind<IRepository<FormSet>>()
                .To<EFRepository<FormSet>>();

            Bind<IRepository<FormSetSearchResult>>()
                .To<EFRepository<FormSetSearchResult>>();

            Bind<IRepository<Comment>>()
                .To<EFRepository<Comment>>();

            Bind<IRepository<CommentSearchResult>>()
                .To<EFRepository<CommentSearchResult>>();

            Bind<IRepository<ConcernResponse>>()
                .To<EFRepository<ConcernResponse>>();

            Bind<IRepository<ConcernResponseSearchResult>>()
                .To<EFRepository<ConcernResponseSearchResult>>();

            Bind<IRepository<TermList>>()
                .To<EFRepository<TermList>>();

            Bind<IRepository<CaraSetting>>()
                .To<EFRepository<CaraSetting>>();

            Bind<IRepository<Code>>()
                .To<EFRepository<Code>>();

            Bind<IRepository<CodingTemplate>>()
                .To<EFRepository<CodingTemplate>>();

            Bind<IRepository<ConcernResponsePhase>>()
                .To<EFRepository<ConcernResponsePhase>>();
			
			Bind<IRepository<Organization>>()
				.To<EFRepository<Organization>>();

            Bind<IRepository<LetterObjection>>()
				.To<EFRepository<LetterObjection>>();

            Bind<IProjectRepository>()
                .To<EFProjectRepository>();

            Bind<IPhaseRepository>()
                .To<EFPhaseRepository>();

            Bind<ILetterRepository>()
                .To<EFLetterRepository>();

            Bind<IFormSetRepository>()
                .To<EFFormSetRepository>();

            Bind<ICommentRepository>()
                .To<EFCommentRepository>();

            Bind<ICommentCodeRepository>()
                .To<EFCommentCodeRepository>();

            Bind<IConcernResponseRepository>()
                .To<EFConcernResponseRepository>();

            Bind<ICodingTemplateRepository>()
                .To<EFCodingTemplateRepository>();

            Bind<IRepository<PhaseMemberRole>>()
                .To<EFRepository<PhaseMemberRole>>();

            Bind<IRepository<LetterDocument>>()
                .To<EFRepository<LetterDocument>>();

			Bind<IRepository<ProjectDocument>>()
				.To<EFRepository<ProjectDocument>>();

            Bind<IRepository<TemplateCodeSelect>>()
                .To<EFRepository<TemplateCodeSelect>>();

            Bind<IRepository<WebCommentPageInfo>>()
                .To<EFRepository<WebCommentPageInfo>>();
			
			Bind<IRepository<FormSet>>()
				.To<EFRepository<FormSet>>();

            Bind<IRepository<FormManagementChangeLog>>()
                .To<EFRepository<FormManagementChangeLog>>();

            Bind<IRepository<CustomReportLog>>()
                .To<EFRepository<CustomReportLog>>();

            Bind<IRepository<LetterUploadQueue>>()
                .To<EFRepository<LetterUploadQueue>>();

            Bind<IRepository<LetterObjectionDocument>>()
                .To<EFRepository<LetterObjectionDocument>>();
            #endregion
		
			#region Other Bindings
			Bind<IFormLetterDetector>()
				.To<FormLetterDetector>()
				.WithConstructorArgument("config", FormDetectionConfiguration);
			#endregion
		}
	}
}
