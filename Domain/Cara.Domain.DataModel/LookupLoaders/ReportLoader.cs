﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Domain.Report
{
	public class ReportLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraReportEntities())
			{
				context.Reports.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                list = context.Reports.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
