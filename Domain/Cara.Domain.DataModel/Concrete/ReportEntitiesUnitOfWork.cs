﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Cara.Domain;

namespace Aquilent.Cara.Domain.Report.DataModel
{
    /// <summary>
    /// Unit of Work class for report entities
    /// </summary>
    public class ReportEntitiesUnitOfWork : EFUnitOfWork
    {
        #region Public Properties
        public CaraReportEntities ReportContext
        {
            get
            {
                return Context as CaraReportEntities;
            }
        }
        #endregion

        #region Constructor
		public ReportEntitiesUnitOfWork(CaraReportEntities context)
            : base(context)
        {
        }
        #endregion
    }
}
