﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Cara.Domain;

namespace Aquilent.Cara.Domain.DataModel
{
    /// <summary>
    /// Unit of Work class for domain entities
    /// </summary>
    public class DomainEntitiesUnitOfWork : EFUnitOfWork
    {
        #region Public Properties
        public CaraDomainEntities DomainContext
        {
            get
            {
                return Context as CaraDomainEntities;
            }
        }
        #endregion

        #region Constructor
        public DomainEntitiesUnitOfWork(CaraDomainEntities context)
            : base(context)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the projects from the object context
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ProjectSearchResult> GetProjects(ProjectSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsProjects(filter.Keyword, filter.ParentUnitId, filter.AnalysisTypeId, filter.ProjectStatusId,
                filter.ProjectActivityId, filter.StateId, filter.MyProjects, filter.Archived, filter.UserId, filter.PageNum, filter.NumRows, filter.SortKey,filter.PhaseTypeId, total
                ).ToList();
        }

        /// <summary>
        /// Get the phases from the object context
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<PhaseSearchResult> GetPhases(PhaseSearchResultFilter filter)
        {
            return DomainContext.spsPhases(filter.UserId, filter.ProjectId, filter.PageNum, filter.NumRows, filter.SortKey).ToList();
        }

        /// <summary>
        /// Get the phase count summary from the object context
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public PhaseSummary GetPhaseSummary(int phaseId)
        {
            return DomainContext.spsPhaseSummary(phaseId).FirstOrDefault();
        }

        /// <summary>
        /// Get the phase codes from the object context
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<PhaseCodeSelect> GetPhaseCodes(PhaseCodeSelectFilter filter)
        {
            return DomainContext.spsPhaseCodes(filter.PhaseId, (filter.CodeSearch == null ? null : filter.CodeSearch.CodeName),
                (filter.CodeSearch == null ? null : filter.CodeSearch.FullCodeNumber), (filter.CodeSearch == null ? null : filter.CodeSearch.CodeRangeListXml)).ToList();
        }

        /// <summary>
        /// Get the letters from the object context
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<LetterSearchResult> GetLetterSearchResults(LetterSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsLetters(filter.PhaseId, filter.LetterSequence,filter.LetterTypeId, filter.DateReceivedFrom, filter.DateReceivedTo, filter.LetterStatusId,
                filter.Keyword, filter.EarlyActionStatusId, filter.ViewAll, filter.WithinCommentPeriod, filter.FirstName, filter.LastName, filter.StartingLetterId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the letters for the reading room from the object context
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<LetterReadingRoomSearchResult> GetLetterReadingRoomSearchResults(LetterReadingRoomSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsLettersReadingRoom(filter.ProjectId, filter.PhaseId, filter.LetterTypeId, filter.FirstName, filter.LastName, filter.OrganizationId,
                filter.Keyword, filter.PublishToReadingRoom, filter.ShowForms, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the concern/response search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ConcernResponseSearchResult> GetConcernResponseSearchResults(ConcernResponseSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsConcernResponses(filter.PhaseId, (filter.CodeSearch == null ? null : filter.CodeSearch.CodeName),
                (filter.CodeSearch == null ? null : filter.CodeSearch.FullCodeNumber), (filter.CodeSearch == null ? null : filter.CodeSearch.CodeRangeListXml), 
                filter.DateUpdatedFrom, filter.DateUpdatedTo, filter.ConcernResponseStatusId, filter.ConcernResponseNumber, filter.Keyword,
                filter.CodeCategoryId, filter.UserId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the comment search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<CommentSearchResult> GetCommentSearchResults(CommentSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsComments(filter.PhaseId, filter.LetterSequence, filter.CommentNumber, (filter.CodeSearch == null ? null : filter.CodeSearch.CodeName),
                (filter.CodeSearch == null ? null : filter.CodeSearch.FullCodeNumber), (filter.CodeSearch == null ? null : filter.CodeSearch.CodeRangeListXml),
                filter.CodeCategoryId, filter.Keyword, filter.Association, filter.SampleStatement, filter.NoResponseRequired, filter.NoResponseReasonId, filter.Annotation,
                filter.UserId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the formset search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        public IList<FormSetSearchResult> GetFormSetSearchResults(FormSetSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsFormSets(filter.PhaseId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the formset search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        public IList<FormSetLetterSearchResult> GetFormSetLetterSearchResults(FormSetSearchResultFilter filter, ObjectParameter total)
        {
            return DomainContext.spsFormSetLetters(filter.PhaseId, filter.LetterTypeId, filter.LetterStatusId, filter.FormSetId,
                filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Insert the phase codes from coding template or phase
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="phaseId"></param>
        /// <param name="codeIds"></param>
        /// <param name="targetPhaseId"></param>
        public void InsertPhaseCodes(int? codingTemplateId, int? phaseId, string codeIds, int targetPhaseId)
        {
            DomainContext.spiPhaseCodes(codingTemplateId, phaseId, codeIds, targetPhaseId);
        }

        /// <summary>
        /// Insert the term lists specified in the id list
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="termListIdList"></param>
        public void InsertPhaseTermLists(int phaseId, string termListIdList)
        {
            DomainContext.spiPhaseTermLists(phaseId, termListIdList);
        }

        /// <summary>
        /// Get the template codes from the object context
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public IList<TemplateCodeSelect> GetTemplateCodes(int codingTemplateId, ObjectParameter templateName)
        {
            return DomainContext.spsCodingTemplateCodes(codingTemplateId, templateName).ToList();
        }

        /// <summary>
        /// Update an existing coding template
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <param name="codeIds"></param>
        public void UpdateCodingTemplate(int codingTemplateId, string templateName, string codeIds)
        {
            DomainContext.spuCodingTemplate(codingTemplateId, templateName, codeIds);
        }

        /// <summary>
        /// Returns the next available sequence number
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public int? GetNextSequence(string name, int? phaseId)
        {
            return DomainContext.spsSequenceNext(name, phaseId).SingleOrDefault();
        }

        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="concernResponseId"></param>
        /// <param name="sequence"></param>
        /// <param name="position"></param>
        public void UpdateConcernResponseSequence(int concernResponseId, int? sequence, string position)
        {
            DomainContext.spuConcernResponseSequence(concernResponseId, sequence, position);
        }

        //*Added for comment code sort
        /// <summary>
        /// Update the sequence of comment code
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="phaseCodeId"></param>
        /// <param name="sequence"></param>
        public void UpdateCommentCodeSequence(int commentId, int phaseCodeId, int sequence)
        {
            DomainContext.spuCommentCodeSequence(commentId, phaseCodeId, sequence);
            
        }

        public void UpdateFormSetPublishingStatus(int formSetId, bool publishToReadingRoom, int? unpublishedReasonId, string unpublishedReasonOther)
        {
            DomainContext.UpdateFormSetPublishingStatus(formSetId, publishToReadingRoom, unpublishedReasonId, unpublishedReasonOther);
        }

        public void UpdateFormSetLetterStatus(int formSetId, int letterStatusId)
        {
            DomainContext.UpdateFormSetLetterStatus(formSetId, letterStatusId);
        }

        public void UpdateFormSetEarlyAttentionStatus(int formSetId, int letterStatusId)
        {
            DomainContext.UpdateFormSetEarlyAttentionStatus(formSetId, letterStatusId);
        }

        public IList<ReviewableLetterCounts> GetReviewableLetterCounts(int userId)
        {
            return DomainContext.GetReviewableLetterCountByPhase(userId).ToList();
        }
        #endregion
    }
}
