﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Cara.Domain;

namespace Aquilent.Cara.Domain.Report.Personal.DataModel
{
    /// <summary>
    /// Unit of Work class for report entities
    /// </summary>
    public class ReportPersonalEntitiesUnitOfWork : EFUnitOfWork
    {
        #region Public Properties
        public CaraReportPersonalEntities ReportContext
        {
            get
            {
                return Context as CaraReportPersonalEntities;
            }
        }
        #endregion

        #region Constructor
        public ReportPersonalEntitiesUnitOfWork(CaraReportPersonalEntities context)
            : base(context)
        {
        }
        #endregion
    }
}
