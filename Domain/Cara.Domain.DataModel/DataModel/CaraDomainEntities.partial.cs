﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Aquilent.Cara.Domain
{
    /// <summary>
    /// Partial Class of the cara domain entities with custom interfaces
    /// </summary>
    public partial class CaraDomainEntities : IDomainContext
    {
        #region Interface Implementation Methods
        /// <summary>
        /// Get the project search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ProjectSearchResult> GetProjectSearchResults(ProjectSearchResultFilter filter, ObjectParameter total)
        {
            return spsProjects(filter.Keyword, filter.ParentUnitId, filter.AnalysisTypeId, filter.ProjectStatusId,
                filter.ProjectActivityId, filter.StateId, filter.MyProjects, filter.Archived, filter.UserId, filter.PageNum, filter.NumRows, filter.SortKey,filter.PhaseTypeId, total).ToList();
        }

        /// <summary>
        /// Get the phase search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<PhaseSearchResult> GetPhaseSearchResults(PhaseSearchResultFilter filter)
        {
            return spsPhases(filter.UserId, filter.ProjectId, filter.PageNum, filter.NumRows, filter.SortKey).ToList();
        }

        /// <summary>
        /// Get the phase summary counts
        /// </summary>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public PhaseSummary GetPhaseSummary(int phaseId)
        {
            return spsPhaseSummary(phaseId).FirstOrDefault();
        }

        /// <summary>
        /// Get the phase codes associated with the phase Id and code search
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<PhaseCodeSelect> GetPhaseCodeSelects(PhaseCodeSelectFilter filter)
        {
            return spsPhaseCodes(filter.PhaseId, (filter.CodeSearch == null ? null : filter.CodeSearch.CodeName), (filter.CodeSearch == null ? null : filter.CodeSearch.FullCodeNumber),
                (filter.CodeSearch == null ? null : filter.CodeSearch.CodeRangeListXml)).ToList();
        }

        /// <summary>
        /// Get the letter search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<LetterSearchResult> GetLetterSearchResults(LetterSearchResultFilter filter, ObjectParameter total)
        {
            return spsLetters(filter.PhaseId, filter.LetterSequence,filter.LetterTypeId, filter.DateReceivedFrom, filter.DateReceivedTo, filter.LetterStatusId, filter.Keyword,
                filter.EarlyActionStatusId, filter.ViewAll, filter.WithinCommentPeriod,filter.FirstName, filter.LastName, filter.StartingLetterId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the letter search results for the reading room based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<LetterReadingRoomSearchResult> GetLetterReadingRoomSearchResults(LetterReadingRoomSearchResultFilter filter, ObjectParameter total)
        {
            return spsLettersReadingRoom(filter.ProjectId, filter.PhaseId, filter.LetterTypeId, filter.FirstName, filter.LastName, filter.OrganizationId,
                filter.Keyword, filter.PublishToReadingRoom, filter.ShowForms, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the formset search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        public IList<FormSetSearchResult> GetFormSetSearchResults(FormSetSearchResultFilter filter, ObjectParameter total)
        {
            return spsFormSets(filter.PhaseId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the letters in a formset search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <param name="totalFormSets"></param>
        /// <returns></returns>
        public IList<FormSetLetterSearchResult> GetFormSetLetterSearchResults(FormSetSearchResultFilter filter, ObjectParameter total)
        {
            return spsFormSetLetters(filter.PhaseId, filter.LetterTypeId, filter.LetterStatusId, filter.FormSetId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the concern/response search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ConcernResponseSearchResult> GetConcernResponseSearchResults(ConcernResponseSearchResultFilter filter, ObjectParameter total)
        {
            return spsConcernResponses(filter.PhaseId, (filter.CodeSearch == null ? null : filter.CodeSearch.CodeName), (filter.CodeSearch == null ? null : filter.CodeSearch.FullCodeNumber),
                (filter.CodeSearch == null ? null : filter.CodeSearch.CodeRangeListXml), filter.DateUpdatedFrom, filter.DateUpdatedTo, filter.ConcernResponseStatusId, filter.ConcernResponseNumber, filter.Keyword,
                filter.CodeCategoryId, filter.UserId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }

        /// <summary>
        /// Get the comment search results based on filters
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<CommentSearchResult> GetCommentSearchResults(CommentSearchResultFilter filter, ObjectParameter total)
        {
            return spsComments(filter.PhaseId, filter.LetterSequence,filter.CommentNumber, (filter.CodeSearch == null ? null : filter.CodeSearch.CodeName), (filter.CodeSearch == null ? null : filter.CodeSearch.FullCodeNumber),
                (filter.CodeSearch == null ? null : filter.CodeSearch.CodeRangeListXml), filter.CodeCategoryId, filter.Keyword, filter.Association, filter.SampleStatement,
                filter.NoResponseRequired, filter.NoResponseReasonId, filter.Annotation, filter.UserId, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }


        /// <summary>
        /// Get the comment details based on CommentIds
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<CommentDetails> CommentDetails(XDocument xdoc)
        {
            return spsCommentDetails(xdoc.ToString()).ToList();
        }

        /// <summary>
        /// Insert the phase codes from coding template or phase
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="phaseId"></param>
        /// <param name="codeIds"></param>
        /// <param name="targetPhaseId"></param>
        public void InsertPhaseCodes(int? codingTemplateId, int? phaseId, string codeIds, int targetPhaseId)
        {
            spiPhaseCodes(codingTemplateId, phaseId, codeIds, targetPhaseId);
        }

        /// <summary>
        /// Insert the term lists specified in the id list
        /// </summary>
        /// <param name="phaseId"></param>
        /// <param name="termListIdList"></param>
        public void InsertPhaseTermLists(int phaseId, string termListIdList)
        {
            spiPhaseTermLists(phaseId, termListIdList);
        }

        /// <summary>
        /// Get the template codes associated with the coding template Id
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public IList<TemplateCodeSelect> GetTemplateCodeSelects(int codingTemplateId, ObjectParameter templateName)
        {
            return spsCodingTemplateCodes(codingTemplateId, templateName).ToList();
        }

        /// <summary>
        /// Update an existing coding template
        /// </summary>
        /// <param name="codingTemplateId"></param>
        /// <param name="templateName"></param>
        /// <param name="codeIds"></param>
        public void UpdateCodingTemplate(int codingTemplateId, string templateName, string codeIds)
        {
            spuCodingTemplate(codingTemplateId, templateName, codeIds);
        }

        /// <summary>
        /// Returns the next available sequence number
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public int? GetNextSequence(string name, int? phaseId)
        {
            return spsSequenceNext(name, phaseId).SingleOrDefault();
        }

        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="concernResponseId"></param>
        /// <param name="sequence"></param>
        /// <param name="position"></param>
        public void UpdateConcernResponsePhaseSequence(int phaseId, int? sequence)
        {
            spuConcernResponsePhaseSequence(phaseId, sequence);
        }

        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="concernResponseId"></param>
        /// <param name="sequence"></param>
        /// <param name="position"></param>
        public void UpdateConcernResponseSequence(int concernResponseId, int? sequence, string position)
        {
            spuConcernResponseSequence(concernResponseId, sequence, position);
        }

        //*Added for comment code sort
        /// <summary>
        /// Update the sequence of concern/response
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="phaseCodeId"></param>
        /// <param name="sequence"></param>
        public void UpdateCommentCodeSequence(int commentId, int phaseCodeId, int sequence)
        {
            spuCommentCodeSequence(commentId, phaseCodeId, sequence);
        }
        #endregion


        public IList<PhaseCodesMostUsed> GetMostUsedPhaseCodes(int phaseId)
        {
            return spsPhaseCodesMostUsed(phaseId).ToList();
        }

        public IList<ReviewableLetterCounts> GetReviewableLetterCounts(int userId)
        {
            return GetReviewableLetterCountByPhase(userId).ToList();
        }
    }
}
