﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Text;

namespace Aquilent.Cara.Domain.Report
{
    /// <summary>
    /// Partial Class of the cara report entities with custom interfaces
    /// </summary>
    public partial class CaraReportEntities : IReportContext
    {
        #region Interface Implementation Methods
		public ReportHeaderData GetReportHeader(int phaseId)
		{
			return GetReportHeaderData(phaseId).First();
		}

		#region Standard Report Methods
        public IList<TeamMemberReportData> GetTeamMemberReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetTeamMemberReportData(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<CommentWithCRInfoReportData> GetCommentWithCRInfoReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetCommentWithCRInfoReportData(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<ConcernResponseWithAssignedToReportData> GetConcernResponseWithAssignedToReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetConcernResponseWithAssignedToReportData(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<CommentWithAnnotationReportData> GetCommentWithAnnotationReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetCommentWithAnnotationReportData(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<CommentReportData> GetCommentReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
		{
            return GetCommentReportData(phaseId, pageNum, numRows, total).ToList();
		}

        public IList<CommenterOrganizationTypeData> GetCommenterOrganizationTypeReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
		{
            return GetCommenterOrganizationTypeData(phaseId, pageNum, numRows, total).ToList();
		}

		public IList<CodingStructureReportData> GetCodingStructureReport(int phaseId)
		{
			return GetCodingStructureReportData(phaseId).ToList();
		}

        public IList<EarlyActionReportData> GetEarlyActionReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetEarlyActionReportData(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<ResponseStatusReportData> GetResponseStatusReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
		{
            return GetResponseStatusReportData(phaseId, pageNum, numRows, total).ToList();
		}

        public IList<MailingListReportData> GetMailingListReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
		{
            return GetMailingListReportData(phaseId, pageNum, numRows, total).ToList();
		}

        public IList<UntimelyCommentsReportData> GetUntimelyCommentsReport(int phaseId, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetUntimelyCommentsReportData(phaseId, pageNum, numRows, total).ToList();
        }

        public IList<ConcernResponseByCommenterReportData> GetConcernResponseByCommenterReport(int phaseId, int? option, int? pageNum, int? numRows, ObjectParameter total)
        {
            return GetConcernResponseByCommenterReportData(phaseId, option, pageNum, numRows, total).ToList();
        }

		#region Demographic Report method calls
		public IList<DeliveryTypeReportData> GetDeliveryTypeReport(int phaseId)
		{
			return GetDeliveryTypeReportData(phaseId).ToList();
		}

		public IList<OrganizationTypeReportData> GetOrganizationTypeReport(int phaseId)
		{
			return GetOrganizationTypeReportData(phaseId).ToList();
		}

		public IList<OriginOfLettersReportData> GetOriginOfLettersReport(int phaseId)
		{
			return GetOriginOfLettersReportData(phaseId).ToList();
		}

		public IList<LetterTypeReportData> GetLetterTypeReport(int phaseId)
		{
			return GetLetterTypeReportData(phaseId).ToList();
		}

		public IList<OrganizedLetterCampaignReportData> GetOrganizedLetterCampaignReport(int phaseId)
		{
			return GetOrganizedLetterCampaignReportData(phaseId).ToList();
		}

		public IList<RespondingOrgReportData> GetRespondingOrgReport(int phaseId)
		{
			return GetRespondingOrgReportData(phaseId).ToList();
		}
		#endregion Demographic Report method calls

		#endregion Standard Report Methods

        #endregion
    }
}
