﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Aquilent.Framework.Services;

namespace Aquilent.Cara.Domain.Services
{
    public partial class CaraWindowsService : AbstractWindowsService
	{
		#region Main
        public CaraWindowsService()
        {
            Init("CARA Windows Service Shell");
        }

        public static void Main()
        {
            CaraWindowsService service = new CaraWindowsService();
#if !DEBUG
            ServiceBase[] ServicesToRun = new ServiceBase[] { service };
            ServiceBase.Run(ServicesToRun);
#else
            foreach (var schedule in service.Schedules)
            {
                schedule.PerformTask();
            }

            Console.Write("\nPress any key to exit... ");
            Console.ReadKey();
#endif      
        }
        #endregion

		protected void InitializeComponent()
		{
			// 
			// CaraWindowsService
			// 
			this.ServiceName = "CaraWindowsService";
		}
	}
}
