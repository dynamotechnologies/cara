﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Reference
{
    public partial class Unit
    {
        private static readonly LookupUnit lookupUnit = LookupManager.GetLookup<LookupUnit>();
        private string regionSuffix = "0000";
        private string forestSuffix = "00";

        private string RegionPrefix
        {
            get
            {
                return UnitId.Substring(0, 4);
            }
        }

        private string ForestPrefix
        {
            get
            {
                return UnitId.Substring(0, 6);
            }
        }

        public bool IsRegion
        {
            get
            {
                return UnitId.Length == 4;
            }
        }

        public bool IsForest
        {
            get
            {
                return UnitId.Length == 6;
            }
        }

        public bool IsDistrict
        {
            get
            {
                return UnitId.Length == 8;
            }
        }

        public IList<Unit> ActiveChildren
        {
            get
            {
                IList<Unit> children = null;
                if (IsRegion)
                {
                    children = lookupUnit.Where(unit => unit.IsForest && !unit.IsRegion && unit.UnitId.StartsWith(UnitId) && unit.Active).OrderBy(unit => unit.Name).ToList();
                }
                else if (IsForest)
                {
                    children = lookupUnit.Where(unit => !unit.IsForest && !unit.IsRegion && unit.UnitId.StartsWith(UnitId) && unit.Active).OrderBy(unit => unit.Name).ToList();
                }

                return children;
            }
        }

        public IList<Unit> Children
        {
            get
            {
                IList<Unit> children = null;
                if (IsRegion)
                {
                    children = lookupUnit.Where(unit => unit.IsForest && !unit.IsRegion && unit.UnitId.StartsWith(UnitId)).OrderBy(unit => unit.Name).ToList();
                }
                else if (IsForest)
                {
                    children = lookupUnit.Where(unit => !unit.IsForest && !unit.IsRegion && unit.UnitId.StartsWith(UnitId)).OrderBy(unit => unit.Name).ToList();
                }

                return children;
            }
        }

        public string ParentUnitId
        {
            get
            {
                return GetParentUnitId(UnitId);
            }
        }

        public static string GetParentUnitId(string unitId)
        {
            string parentUnitId = unitId;
            if (!string.IsNullOrWhiteSpace(unitId))
            {
                if (unitId.EndsWith("000000"))
                {
                    parentUnitId = unitId.Substring(0, unitId.Length - 6);
                }
                else if (unitId.EndsWith("0000"))
                {
                    parentUnitId = unitId.Substring(0, unitId.Length - 4);
                }
                else if (unitId.EndsWith("00"))
                {
                    parentUnitId = unitId.Substring(0, unitId.Length - 2);
                }
            }

            return parentUnitId;
        }
    }
}
