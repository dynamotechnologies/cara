﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;

namespace Aquilent.Cara.Reference
{
	public partial class PhaseType
	{
        #region PhaseType Constants
        private static readonly LookupPhaseType _lookupPhaseType = LookupManager.GetLookup<LookupPhaseType>();

        public static readonly PhaseType Formal = _lookupPhaseType[2];
        public static readonly PhaseType Informal = _lookupPhaseType[1];
        public static readonly PhaseType Other = _lookupPhaseType[6];
        public static readonly PhaseType Scoping = _lookupPhaseType[4];
        public static readonly PhaseType Objection = _lookupPhaseType[3];
        public static readonly PhaseType NoticeOfAvailability = _lookupPhaseType[5];
        #endregion 

	}
}
