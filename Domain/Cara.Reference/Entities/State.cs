//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Aquilent.Cara.Reference
{
    public partial class State
    {
        #region Primitive Properties
    
        public virtual string StateId
        {
            get;
            set;
        }
    
        public virtual string Name
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<County> Counties
        {
            get
            {
                if (_counties == null)
                {
                    var newCollection = new FixupCollection<County>();
                    newCollection.CollectionChanged += FixupCounties;
                    _counties = newCollection;
                }
                return _counties;
            }
            set
            {
                if (!ReferenceEquals(_counties, value))
                {
                    var previousValue = _counties as FixupCollection<County>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCounties;
                    }
                    _counties = value;
                    var newValue = value as FixupCollection<County>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCounties;
                    }
                }
            }
        }
        private ICollection<County> _counties;

        #endregion
        #region Association Fixup
    
        private void FixupCounties(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (County item in e.NewItems)
                {
                    item.State = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (County item in e.OldItems)
                {
                    if (ReferenceEquals(item.State, this))
                    {
                        item.State = null;
                    }
                }
            }
        }

        #endregion
    }
}
