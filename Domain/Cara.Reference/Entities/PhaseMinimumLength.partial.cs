﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aquilent.Cara.Reference
{
	public partial class PhaseMinimumLength
	{
		public bool AllowFormal
		{
			get	{ return PhaseMinimumDays.HasValue;	}
		}

		public bool AllowObjection
		{
			get { return ObjectionPeriodMinimumDays.HasValue; }
		}
	}
}
