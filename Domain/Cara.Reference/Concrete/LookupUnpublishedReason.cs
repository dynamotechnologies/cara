﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupUnpublishedReason : AbstractLookup<int, UnpublishedReason>
	{
		protected override IList<UnpublishedReason> LoadValues()
		{
			return DataLoader.Load().OfType<UnpublishedReason>().ToList();
		}

		protected override KeyValuePair<int, UnpublishedReason> PrepareForLookup(UnpublishedReason item)
		{
			return new KeyValuePair<int, UnpublishedReason>(item.UnpublishedReasonId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(UnpublishedReason item)
		{
			return new KeyValuePair<int, string>(item.UnpublishedReasonId, item.Name);
		}
	}
}
