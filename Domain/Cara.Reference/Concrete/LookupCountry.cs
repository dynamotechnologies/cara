﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupCountry : AbstractLookup<int, Country>
	{
		protected override IList<Country> LoadValues()
		{
			return DataLoader.Load().OfType<Country>().ToList();
		}

		protected override KeyValuePair<int, Country> PrepareForLookup(Country item)
		{
			return new KeyValuePair<int, Country>(item.CountryId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(Country item)
		{
			return new KeyValuePair<int, string>(item.CountryId, item.Name);
		}
	}
}
