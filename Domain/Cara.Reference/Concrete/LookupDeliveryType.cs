﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupDeliveryType : AbstractLookup<int, DeliveryType>
	{
		protected override IList<DeliveryType> LoadValues()
		{
			return DataLoader.Load().OfType<DeliveryType>().ToList();
		}

		protected override KeyValuePair<int, DeliveryType> PrepareForLookup(DeliveryType item)
		{
			return new KeyValuePair<int, DeliveryType>(item.DeliveryTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(DeliveryType item)
		{
			return new KeyValuePair<int, string>(item.DeliveryTypeId, item.Name);
		}
	}
}
