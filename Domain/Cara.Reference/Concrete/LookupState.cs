﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupState : AbstractLookup<string, State>
	{
		protected override IList<State> LoadValues()
		{
			return DataLoader.Load().OfType<State>().ToList();
		}

		protected override KeyValuePair<string, State> PrepareForLookup(State item)
		{
			return new KeyValuePair<string, State>(item.StateId, item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(State item)
		{
			return new KeyValuePair<string, string>(item.StateId, item.Name);
		}
	}
}
