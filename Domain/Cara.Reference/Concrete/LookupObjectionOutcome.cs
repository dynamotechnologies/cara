﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupObjectionOutcome: AbstractLookup<int, ObjectionOutcome>
	{
		protected override IList<ObjectionOutcome> LoadValues()
		{
			return DataLoader.Load().OfType<ObjectionOutcome>().ToList();
		}

		protected override KeyValuePair<int, ObjectionOutcome> PrepareForLookup(ObjectionOutcome item)
		{
			return new KeyValuePair<int, ObjectionOutcome>(item.ObjectionOutcomeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ObjectionOutcome item)
		{
			return new KeyValuePair<int, string>(item.ObjectionOutcomeId, item.Name);
		}
	}
}
