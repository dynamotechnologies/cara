﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupCounty : AbstractLookup<string, County>
	{
		protected override IList<County> LoadValues()
		{
			return DataLoader.Load().OfType<County>().ToList();
		}

		protected override KeyValuePair<string, County> PrepareForLookup(County item)
		{
			return new KeyValuePair<string, County>(item.CountyId, item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(County item)
		{
			return new KeyValuePair<string, string>(item.CountyId, item.Name);
		}
	}
}
