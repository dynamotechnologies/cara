﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupPhaseMinimumLength : AbstractLookup<string, PhaseMinimumLength>
	{
		protected override IList<PhaseMinimumLength> LoadValues()
		{
			return DataLoader.Load().OfType<PhaseMinimumLength>().ToList();
		}

		protected override KeyValuePair<string, PhaseMinimumLength> PrepareForLookup(PhaseMinimumLength item)
		{
			return new KeyValuePair<string, PhaseMinimumLength>(CreateKey(item.AnalysisTypeId, item.CommentRuleId), item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(PhaseMinimumLength item)
		{
			// This is not really useful for this lookup
			return new KeyValuePair<string, string>(CreateKey(item.AnalysisTypeId, item.CommentRuleId), CreateKey(item.AnalysisTypeId, item.CommentRuleId));
		}

		public string CreateKey(int analysisTypeId, int commentRuleId)
		{
			return string.Format("{0}_{1}", analysisTypeId, commentRuleId);
		}

		public PhaseMinimumLength GetValue(int analysisTypeId, int commentRuleId)
		{
			return this[CreateKey(analysisTypeId, commentRuleId)];
		}
	}
}
