﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupNoResponseReason : AbstractLookup<int, NoResponseReason>
	{
		protected override IList<NoResponseReason> LoadValues()
		{
			return DataLoader.Load().OfType<NoResponseReason>().ToList();
		}

		protected override KeyValuePair<int, NoResponseReason> PrepareForLookup(NoResponseReason item)
		{
			return new KeyValuePair<int, NoResponseReason>(item.NoResponseReasonId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(NoResponseReason item)
		{
			return new KeyValuePair<int, string>(item.NoResponseReasonId, item.Name);
		}
	}
}
