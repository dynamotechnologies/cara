﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupPhaseType : AbstractLookup<int, PhaseType>
	{
		protected override IList<PhaseType> LoadValues()
		{
			return DataLoader.Load().OfType<PhaseType>().ToList();
		}

		protected override KeyValuePair<int, PhaseType> PrepareForLookup(PhaseType item)
		{
			return new KeyValuePair<int, PhaseType>(item.PhaseTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(PhaseType item)
		{
			return new KeyValuePair<int, string>(item.PhaseTypeId, item.Name);
		}
	}
}
