﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupProjectStatus : AbstractLookup<int, ProjectStatus>
	{
		protected override IList<ProjectStatus> LoadValues()
		{
			return DataLoader.Load().OfType<ProjectStatus>().ToList();
		}

		protected override KeyValuePair<int, ProjectStatus> PrepareForLookup(ProjectStatus item)
		{
			return new KeyValuePair<int, ProjectStatus>(item.ProjectStatusId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ProjectStatus item)
		{
			return new KeyValuePair<int, string>(item.ProjectStatusId, item.Name);
		}
	}
}
