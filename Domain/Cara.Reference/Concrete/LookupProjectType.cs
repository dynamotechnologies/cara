﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupProjectType : AbstractLookup<int, ProjectType>
	{
		protected override IList<ProjectType> LoadValues()
		{
			return DataLoader.Load().OfType<ProjectType>().ToList();
		}

		protected override KeyValuePair<int, ProjectType> PrepareForLookup(ProjectType item)
		{
			return new KeyValuePair<int, ProjectType>(item.ProjectTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ProjectType item)
		{
			return new KeyValuePair<int, string>(item.ProjectTypeId, item.Name);
		}
	}
}
