﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupCodeCategory : AbstractLookup<int, CodeCategory>
	{
		protected override IList<CodeCategory> LoadValues()
		{
			return DataLoader.Load().OfType<CodeCategory>().ToList();
		}

		protected override KeyValuePair<int, CodeCategory> PrepareForLookup(CodeCategory item)
		{
			return new KeyValuePair<int, CodeCategory>(item.CodeCategoryId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(CodeCategory item)
		{
			return new KeyValuePair<int, string>(item.CodeCategoryId, item.Name);
		}
	}
}
