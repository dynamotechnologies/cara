﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupArtifactType : AbstractLookup<int, ArtifactType>
	{
		protected override IList<ArtifactType> LoadValues()
		{
			return DataLoader.Load().OfType<ArtifactType>().ToList();
		}

		protected override KeyValuePair<int, ArtifactType> PrepareForLookup(ArtifactType item)
		{
			return new KeyValuePair<int, ArtifactType>(item.ArtifactTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ArtifactType item)
		{
			return new KeyValuePair<int, string>(item.ArtifactTypeId, item.Name);
		}
	}
}
