﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupObjectionReviewStatus : AbstractLookup<int, ObjectionReviewStatus>
	{
		protected override IList<ObjectionReviewStatus> LoadValues()
		{
			return DataLoader.Load().OfType<ObjectionReviewStatus>().ToList();
		}

		protected override KeyValuePair<int, ObjectionReviewStatus> PrepareForLookup(ObjectionReviewStatus item)
		{
			return new KeyValuePair<int, ObjectionReviewStatus>(item.ObjectionReviewStatusId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ObjectionReviewStatus item)
		{
			return new KeyValuePair<int, string>(item.ObjectionReviewStatusId, item.Name);
		}
	}
}
