﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupCommentRuleByName : AbstractLookup<string, CommentRule>
	{
		protected override IList<CommentRule> LoadValues()
		{
			return DataLoader.Load().OfType<CommentRule>().ToList();
		}

		protected override KeyValuePair<string, CommentRule> PrepareForLookup(CommentRule item)
		{
			return new KeyValuePair<string, CommentRule>(item.Name, item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(CommentRule item)
		{
			return new KeyValuePair<string, string>(item.CommentRuleId.ToString(), item.Name);
		}
	}
}
