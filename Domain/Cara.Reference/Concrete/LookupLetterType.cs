﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupLetterType : AbstractLookup<int, LetterType>
	{
        public static int[] AllForms = new int[] { 3, 4, 5 };
        public static int[] MasterFormOrUnique = new int[] { 2, 3 };
        public static int[] NotPending = new int[] { 1, 2, 3, 4, 5 };
        public static int[] UniqueOrForms = new int[] { 2, 3, 4, 5 };

		protected override IList<LetterType> LoadValues()
		{
			return DataLoader.Load().OfType<LetterType>().ToList();
		}

		protected override KeyValuePair<int, LetterType> PrepareForLookup(LetterType item)
		{
			return new KeyValuePair<int, LetterType>(item.LetterTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(LetterType item)
		{
			return new KeyValuePair<int, string>(item.LetterTypeId, item.Name);
		}
	}
}
