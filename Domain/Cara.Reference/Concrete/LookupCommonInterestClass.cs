﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupCommonInterestClass : AbstractLookup<int, CommonInterestClass>
	{
		protected override IList<CommonInterestClass> LoadValues()
		{
			return DataLoader.Load().OfType<CommonInterestClass>().ToList();
		}

		protected override KeyValuePair<int, CommonInterestClass> PrepareForLookup(CommonInterestClass item)
		{
			return new KeyValuePair<int, CommonInterestClass>(item.CommonInterestClassId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(CommonInterestClass item)
		{
			return new KeyValuePair<int, string>(item.CommonInterestClassId, item.Name);
		}
	}
}
