﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
    public class LookupEarlyActionStatus : AbstractLookup<int, EarlyActionStatus>
    {
        protected override IList<EarlyActionStatus> LoadValues()
        {
            return DataLoader.Load().OfType<EarlyActionStatus>().ToList();
        }

        protected override KeyValuePair<int, EarlyActionStatus> PrepareForLookup(EarlyActionStatus item)
        {
            return new KeyValuePair<int, EarlyActionStatus>(item.StatusId, item);
        }

        protected override KeyValuePair<int, string> PrepareForSelection(EarlyActionStatus item)
        {
            return new KeyValuePair<int, string>(item.StatusId, item.Name);
        }
    }
}
