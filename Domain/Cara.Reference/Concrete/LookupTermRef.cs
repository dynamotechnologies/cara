﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupTermRef : AbstractLookup<int, TermRef>
	{
        protected override IList<TermRef> LoadValues()
		{
            return DataLoader.Load().OfType<TermRef>().ToList();
		}

        protected override KeyValuePair<int, TermRef> PrepareForLookup(TermRef item)
		{
            return new KeyValuePair<int, TermRef>(item.TermId, item);
		}

        protected override KeyValuePair<int, string> PrepareForSelection(TermRef item)
		{
            return new KeyValuePair<int, string>(item.TermId, item.Name);
		}
	}
}
