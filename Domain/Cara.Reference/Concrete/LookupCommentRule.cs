﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupCommentRule : AbstractLookup<int, CommentRule>
	{
		protected override IList<CommentRule> LoadValues()
		{
			return DataLoader.Load().OfType<CommentRule>().ToList();
		}

		protected override KeyValuePair<int, CommentRule> PrepareForLookup(CommentRule item)
		{
			return new KeyValuePair<int, CommentRule>(item.CommentRuleId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(CommentRule item)
		{
			return new KeyValuePair<int, string>(item.CommentRuleId, item.Name);
		}
	}
}
