﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
    public class LookupLetterTermConfirmationStatus : AbstractLookup<int, LetterTermConfirmationStatus>
    {
        protected override IList<LetterTermConfirmationStatus> LoadValues()
        {
            return DataLoader.Load().OfType<LetterTermConfirmationStatus>().ToList();
        }

        protected override KeyValuePair<int, LetterTermConfirmationStatus> PrepareForLookup(LetterTermConfirmationStatus item)
        {
            return new KeyValuePair<int, LetterTermConfirmationStatus>(item.ConfirmationStatusId, item);
        }

        protected override KeyValuePair<int, string> PrepareForSelection(LetterTermConfirmationStatus item)
        {
            return new KeyValuePair<int, string>(item.ConfirmationStatusId, item.Name);
        }
    }
}
