﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupObjectionDecisionMaker: AbstractLookup<int, ObjectionDecisionMaker>
	{
		protected override IList<ObjectionDecisionMaker> LoadValues()
		{
			return DataLoader.Load().OfType<ObjectionDecisionMaker>().ToList();
		}

		protected override KeyValuePair<int, ObjectionDecisionMaker> PrepareForLookup(ObjectionDecisionMaker item)
		{
			return new KeyValuePair<int, ObjectionDecisionMaker>(item.ObjectionDecisionMakerId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ObjectionDecisionMaker item)
		{
			return new KeyValuePair<int, string>(item.ObjectionDecisionMakerId, item.Name);
		}
	}
}
