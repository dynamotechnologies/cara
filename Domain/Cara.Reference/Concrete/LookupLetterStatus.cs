﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupLetterStatus : AbstractLookup<int, LetterStatus>
	{
		protected override IList<LetterStatus> LoadValues()
		{
			return DataLoader.Load().OfType<LetterStatus>().ToList();
		}

		protected override KeyValuePair<int, LetterStatus> PrepareForLookup(LetterStatus item)
		{
			return new KeyValuePair<int, LetterStatus>(item.LetterStatusId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(LetterStatus item)
		{
			return new KeyValuePair<int, string>(item.LetterStatusId, item.Name);
		}
	}
}
