﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupActivity : AbstractLookup<int, Activity>
	{
		protected override IList<Activity> LoadValues()
		{
			return DataLoader.Load().OfType<Activity>().ToList();
		}

		protected override KeyValuePair<int, Activity> PrepareForLookup(Activity item)
		{
			return new KeyValuePair<int, Activity>(item.ActivityId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(Activity item)
		{
			return new KeyValuePair<int, string>(item.ActivityId, item.Name);
		}
	}
}
