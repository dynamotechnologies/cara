﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupConcernResponseStatus : AbstractLookup<int, ConcernResponseStatus>
	{
		protected override IList<ConcernResponseStatus> LoadValues()
		{
			return DataLoader.Load().OfType<ConcernResponseStatus>().OrderBy(x => x.SortOrder).ToList();
		}

		protected override KeyValuePair<int, ConcernResponseStatus> PrepareForLookup(ConcernResponseStatus item)
		{
			return new KeyValuePair<int, ConcernResponseStatus>(item.ConcernResponseStatusId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(ConcernResponseStatus item)
		{
			return new KeyValuePair<int, string>(item.ConcernResponseStatusId, item.Name);
		}
	}
}
