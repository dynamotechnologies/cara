﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupAnalysisType : AbstractLookup<int, AnalysisType>
	{
		protected override IList<AnalysisType> LoadValues()
		{
			return DataLoader.Load().OfType<AnalysisType>().ToList();
		}

		protected override KeyValuePair<int, AnalysisType> PrepareForLookup(AnalysisType item)
		{
			return new KeyValuePair<int, AnalysisType>(item.AnalysisTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(AnalysisType item)
		{
			return new KeyValuePair<int, string>(item.AnalysisTypeId, item.Name);
		}
	}
}
