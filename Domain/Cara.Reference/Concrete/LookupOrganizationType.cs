﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupOrganizationType : AbstractLookup<int, OrganizationType>
	{
		protected override IList<OrganizationType> LoadValues()
		{
			return DataLoader.Load().OfType<OrganizationType>().ToList();
		}

		protected override KeyValuePair<int, OrganizationType> PrepareForLookup(OrganizationType item)
		{
			return new KeyValuePair<int, OrganizationType>(item.OrganizationTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(OrganizationType item)
		{
			return new KeyValuePair<int, string>(item.OrganizationTypeId, item.Name);
		}
	}
}
