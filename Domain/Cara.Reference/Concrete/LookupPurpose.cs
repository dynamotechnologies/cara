﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupPurpose : AbstractLookup<int, Purpose>
	{
		protected override IList<Purpose> LoadValues()
		{
			return DataLoader.Load().OfType<Purpose>().ToList();
		}

		protected override KeyValuePair<int, Purpose> PrepareForLookup(Purpose item)
		{
			return new KeyValuePair<int, Purpose>(item.PurposeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(Purpose item)
		{
			return new KeyValuePair<int, string>(item.PurposeId, item.Name);
		}
	}
}
