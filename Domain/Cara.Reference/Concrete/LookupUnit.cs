﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupUnit : AbstractLookup<string, Unit>
	{
		protected override IList<Unit> LoadValues()
		{
			return DataLoader.Load().OfType<Unit>().ToList();
		}

		protected override KeyValuePair<string, Unit> PrepareForLookup(Unit item)
		{
			return new KeyValuePair<string, Unit>(item.UnitId, item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(Unit item)
		{
			return new KeyValuePair<string, string>(item.UnitId, item.Name);
		}

        public IEnumerable<Unit> FullUnitHierarchy()
        {
            return this.Where(unit => unit.IsRegion);
        }
	}
}
