﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupOfficialRepresentativeType : AbstractLookup<int, OfficialRepresentativeType>
	{
		protected override IList<OfficialRepresentativeType> LoadValues()
		{
			return DataLoader.Load().OfType<OfficialRepresentativeType>().ToList();
		}

		protected override KeyValuePair<int, OfficialRepresentativeType> PrepareForLookup(OfficialRepresentativeType item)
		{
			return new KeyValuePair<int, OfficialRepresentativeType>(item.OfficialRepresentativeTypeId, item);
		}

		protected override KeyValuePair<int, string> PrepareForSelection(OfficialRepresentativeType item)
		{
			return new KeyValuePair<int, string>(item.OfficialRepresentativeTypeId, item.Name);
		}
	}
}
