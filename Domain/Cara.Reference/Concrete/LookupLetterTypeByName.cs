﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using Aquilent.EntityAccess;

namespace Aquilent.Cara.Reference
{
	public class LookupLetterTypeByName : AbstractLookup<string, LetterType>
	{
		public static LetterType DUPLICATE = LookupManager.GetLookup<LookupLetterTypeByName>()["Duplicate"];
		public static LetterType UNIQUE = LookupManager.GetLookup<LookupLetterTypeByName>()["Unique"];
		public static LetterType MASTERFORM = LookupManager.GetLookup<LookupLetterTypeByName>()["Master Form"];
		public static LetterType FORM = LookupManager.GetLookup<LookupLetterTypeByName>()["Form"];
		public static LetterType FORMPLUS = LookupManager.GetLookup<LookupLetterTypeByName>()["Form Plus"];

		protected override IList<LetterType> LoadValues()
		{
			return DataLoader.Load().OfType<LetterType>().ToList();
		}

		protected override KeyValuePair<string, LetterType> PrepareForLookup(LetterType item)
		{
			return new KeyValuePair<string, LetterType>(item.Name, item);
		}

		protected override KeyValuePair<string, string> PrepareForSelection(LetterType item)
		{
			return new KeyValuePair<string, string>(item.Name, item.Name);
		}
	}
}
