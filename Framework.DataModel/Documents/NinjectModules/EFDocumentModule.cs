﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using Aquilent.EntityAccess;
using Aquilent.Framework.Documents;
using System.Data.Objects;

namespace Aquilent.Framework.Documents.Ninject
{
	public class EFDocumentModule : NinjectModule
	{
		public override void Load()
		{
            Bind<ObjectContext>()
                .To<CaraDocumentsEntities>();

            Bind<IUnitOfWork>()
                .To<EFUnitOfWork>();

            Bind<IRepository<Document>>()
				.To<DocumentRepository>();

			Bind<IFileStore>()
				.To<AwsS3FileStore>()
				.WithConstructorArgument("configSectionName", "fileStoreConfiguration");
		}
	}
}
