﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

using Aquilent.EntityAccess;
using Aquilent.Framework.Security;

namespace Aquilent.Framework.DataModel.Security
{
    /// <summary>
    /// Unit of Work class for security entities
    /// </summary>
    public class SecurityEntitiesUnitOfWork : EFUnitOfWork
    {
        public CaraSecurityEntities SecurityContext
        {
            get
            {
                return Context as CaraSecurityEntities;
            }
        }

        public SecurityEntitiesUnitOfWork(CaraSecurityEntities context)
            : base(context)
        {
        }

        /// <summary>
        /// Get the user privileges from the object context
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<UserPrivilege> GetUserPrivileges(int userId)
        {
            return SecurityContext.spsUserPrivileges(userId).ToList();
        }

        /// <summary>
        /// Get the users from the object context
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<UserSearchResult> GetUsers(UserSearchResultFilter filter, ObjectParameter total)
        {
            return SecurityContext.spsUsers(filter.UserId, filter.Username, filter.LastName, filter.FirstName,
                filter.ParentUnitId, filter.ShowOnlyNonProjectRoles, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }
    }
}
