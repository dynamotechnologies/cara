﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;

namespace Aquilent.Framework.Security
{
    /// <summary>
    /// Partial Class of the cara security entities with custom interfaces
    /// </summary>
    public partial class CaraSecurityEntities : ISecurityContext
    {
        #region Interface Implementation Methods
        /// <summary>
        /// Return a list of user privileges
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<UserPrivilege> GetUserPrivileges(int userId)
        {
            return spsUserPrivileges(userId).ToList();
        }

        /// <summary>
        /// Return a list of users
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<UserSearchResult> GetUsers(UserSearchResultFilter filter, ObjectParameter total)
        {
            return spsUsers(filter.UserId, filter.Username, filter.LastName, filter.FirstName, filter.ParentUnitId,
                filter.ShowOnlyNonProjectRoles, filter.PageNum, filter.NumRows, filter.SortKey, total).ToList();
        }
        #endregion
    }
}
