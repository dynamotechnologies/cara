﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aquilent.Framework.Resource;
using System.Collections;

namespace Aquilent.Framework.Security
{
	public class RoleLoader : ILookupDataLoader
	{
		public IQueryable Load()
		{
			IQueryable list = null;
			using (var context = new CaraSecurityEntities())
			{
				context.Roles.MergeOption = System.Data.Objects.MergeOption.NoTracking;
				list = context.Roles.ToList<object>().AsQueryable();
			}

			return list;
		}
	}
}
