﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using Aquilent.EntityAccess;
using Aquilent.Framework.Security;
using Aquilent.Framework.DataModel.Security;

namespace Aquilent.Framework.Security.Ninject
{
	public class EFSecurityModule : NinjectModule
	{
		public override void Load()
		{
            Bind<CaraSecurityEntities>().ToSelf();

			Bind<IUnitOfWork>()
                .To<SecurityEntitiesUnitOfWork>();

			Bind<IRepository<User>>()
				.To<EFRepository<User>>();

            Bind<IRepository<UserSearchResult>>()
                .To<EFRepository<UserSearchResult>>();

			Bind<IRepository<SecurityToken>>()
				.To<EFRepository<SecurityToken>>();

			Bind<IRepository<SecuritySetting>>()
				.To<EFRepository<SecuritySetting>>();

			Bind<IRepository<Role>>()
				.To<EFRepository<Role>>();

            Bind<IRepository<UserPrivilege>>()
                .To<EFRepository<UserPrivilege>>();

            Bind<IUserRepository>()
                .To<EFUserRepository>();

            Bind<IPrivilegeRepository>()
                .To<EFPrivilegeRepository>();
		}
	}
}
