﻿using Aquilent.EntityAccess;
using Ninject.Modules;
using System.Data.Objects;

namespace Aquilent.Framework.Resource.Ninject
{
	public class EFResourceModule : NinjectModule
	{
		public override void Load()
		{
            Bind<ObjectContext>()
                .To<CaraResourceEntities>();

			Bind<IUnitOfWork>()
                .To<EFUnitOfWork>();

			Bind<IRepository<ResourceSetting>>()
				.To<EFRepository<ResourceSetting>>();

			Bind<IRepository<CannedText>>()
				.To<EFRepository<CannedText>>();
		}
	}
}
